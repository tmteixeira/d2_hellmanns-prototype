/*global define*/

define(function() {

    'use strict';

    var Navigation = IEA.module('UI.hero-launch-video', function(heroLaunchVideo, app, iea) {

        _.extend(heroLaunchVideo, {
            // Extendable hooks and API methods goes here

            onInit: function() {

                this.updateSetting({
                    enableHover: true
                });

            },

            clickHandler: function() {

            },

            // Hook for handling mouse click event
            onMouseclick: function() {

            },

            // Hook for handling mouse enter event
            onMouseenter: function(event) {

            },

            // Hook for handling mouse leave event
            onMouseleave: function() {

            },

            // Hook for handling load event
            onEnable: function () {

            $(".dig-hero-video").fancybox({
                width      : 1280,
                height      : 720,
                aspectRatio : true,
                scrolling   : 'no',
                preload   : true,
                closeClick  : true,
                openEffect  : 'fade',
                closeEffect : 'fade'
            });

        }

        });
    });

    return Navigation;
});
