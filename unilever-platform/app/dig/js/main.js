/*global require*/

'use strict';

require.config({
    enforceDefine: true,
    shim: {
        'iea': {
            deps: ['jquery', 'underscore']
        }
    },
    paths: {
        'jquery': '../js/libs/vendor/jquery/dist/jquery',
        'underscore': '../js/libs/vendor/underscore/underscore',
        'handlebars': '../js/libs/vendor/handlebars/handlebars',
        'svg4everybody': '../js/libs/vendor/svg4everybody/dist/svg4everybody.min',
        'iea': ['../js/libs/iea/core/js/iea'],
        'unilever-iea.components': ['../js/libs/iea/core/js/unilever-iea.components'],
        'iea.components': ['../js/libs/iea/core/js/iea.components'],
        'config': '../../../dig-config'
    }
});

define(['app'], function (app) {

    app.on('before:start', function () {
        // before start logic goes here
    });

    app.on('start', function (options) {
        // on application start logic goes here
    });

    app.on('application:ready', function () {
        app.start();
    });
});
