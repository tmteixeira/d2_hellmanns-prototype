// Created 'ratingReview' empty object
window.ratingReview = window.ratingReview || {};
ratingReview.config = ratingReview.config || {};
ratingReview.contents = ratingReview.contents || {};

/******** API URL ********/
ratingReview.server = {
    apiURL:"//ratingreviews-qa.unileversolutions.com/api/v1",
	apiSecureURL:"//secure.ratingreviews-qa.unileversolutions.com/api/v1"
};

/******** Get widget URL ********/
ratingReview.rrWidgetURL = {
    widgetURL: "//ratingreviews-qa.unileversolutions.com/BrandSite/resources",
    widgetSecureURL: "//ratingreviews-qa.unileversolutions.com/BrandSite/resources"
};



// Storing URL in global variable
function getWidgetURL(){
    return (window.location.protocol == 'https:') ? ratingReview.rrWidgetURL.widgetSecureURL : ratingReview.rrWidgetURL.widgetURL;
}
getRnRWidgetURL = getWidgetURL();

// Preload images
ratingReview.preloadImg = function(images) {
    if (document.images) {
        for(var i=0; i<=images.length-1; i++) {
            var imageObj = new Image();
            imageObj.src = images[i];
        }
    }
};

// Init widget
(function () {
    
    var jQuery; // Localize jQuery variable

    /******** Loading jQuery if not present *********/
    if (window.jQuery === undefined || parseFloat(window.jQuery.fn.jquery) <= 1.7) {
        var script_tag = document.createElement('script');
        script_tag.setAttribute("type", "text/javascript");
        script_tag.setAttribute("src","//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js");
        if (script_tag.readyState) {
            script_tag.onreadystatechange = function () { // For old versions of IE
                if (this.readyState == 'complete' || this.readyState == 'loaded') {
                    scriptLoadHandler();
                }
            };
        } else { // Other browsers
            script_tag.onload = scriptLoadHandler;
        }
        // Try to find the head, otherwise default to the documentElement
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);

    } else {
        // The jQuery version on the window is the one we want to use
        jQuery = window.jQuery;
        main();
    }

    /******** Called once jQuery has loaded ******/
    function scriptLoadHandler() {
        // Restore $ and window.jQuery to their previous values and store the
        // new jQuery in our local jQuery variable
        jQuery = window.jQuery.noConflict(true);
        // Call our main function        
        main();
    }
    
    /******** Our main function ********/
    function main() {
        
        /******* Load CSS *******/
        var RRcssLoaded = false,
            RRcssURL = getRnRWidgetURL + '/css/RR_widget.css',
            rrPreImgLoaded = false;

        ratingReview.rrPreLoadObj = {
            rrLoaderImg: getRnRWidgetURL + '/images/loader.gif'
        }

        ratingReview.rrPreLoadObj.rrConAvgRat = '';
        ratingReview.rrPreLoadObj.rrRatingStr = '';

        ratingReview.preLoadingStars = function(jQ) {
            var rrTypeRating = jQ('.rr-widget-container[data-widget-type="type-1"], .rr-widget-container[data-widget-type="type-2"], .rr-widget-container[data-widget-type="type-3"], .rr-widget-container[data-widget-type="type-4"]'),
                preLoadHTML = '';

            ratingReview.loadImg = window.setInterval(function() {

                if (ratingReview.config.starImageDisabled && ratingReview.config.starImageSelected) {
                    window.clearInterval(ratingReview.loadImg);

                    ratingReview.preloadImg([ratingReview.config.starImageDisabled, ratingReview.config.starImageSelected]);

                    if (ratingReview.config.rrLoaderImg && ratingReview.config.rrLoaderImg != '') {
                        ratingReview.rrPreLoadObj.rrLoaderImg = ratingReview.config.rrLoaderImg;
                    }

                    ratingReview.rrPreLoadObj.rrConAvgRat = '<span class="aggRtng"><img id="rrRatingLoader" title="" alt="" src="' + ratingReview.rrPreLoadObj.rrLoaderImg + '"></span><span class="tRtng"> </span><a href="javascript:void(0)" title="' + ratingReview.contents.writeReview + '" class="wRtng"/>';
                    ratingReview.rrPreLoadObj.rrRatingStr = '<div class="aggregateRating"><div class="agRatingDeSelect"><img title="review" alt=" review " src="' + ratingReview.config.starImageDisabled + '"></div><div class="agRatingSelect" style="width:0px;"><img title=" review " alt=" review " src="' + ratingReview.config.starImageSelected + '"></div></div>';

                    preLoadHTML = ratingReview.rrPreLoadObj.rrRatingStr + ratingReview.rrPreLoadObj.rrConAvgRat;
                    rrTypeRating.each(function() {
                        jQ(this).html(preLoadHTML);
                    });
                }
            }, 10);

            rrPreImgLoaded = true;

        };

        if (window.$ && document.head) {
            var RRcss_link = $('<link rel="stylesheet" type="text/css" href="'+RRcssURL+'">');
            RRcss_link.appendTo('head');
            RRcssLoaded = true;
        } 
        else if(!document.head){
            var RRcssObj  = document.createElement('link');
            RRcssObj.type = 'text/css';
            RRcssObj.rel  = 'stylesheet';
            RRcssObj.href = RRcssURL;
            document.getElementsByTagName("head")[0].appendChild(RRcssObj);
            RRcssLoaded = true;
        }

        /******* Load JS main file *******/
        jQuery(document).ready(function ($) {
            
            if (!RRcssLoaded) {
                var RRcss_link = $('<link rel="stylesheet" type="text/css" href="'+RRcssURL+'">');
                RRcss_link.appendTo('head');
            }
            if (!rrPreImgLoaded) {
                ratingReview.preLoadingStars($);
            }
        
            $.getScript(getRnRWidgetURL + '/js/RR_widget_core.js?callback=');
            
            return true;
        });
    }

})(); // We call our anonymous function immediately