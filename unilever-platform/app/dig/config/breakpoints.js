//Break Points for adaptive images.

var BREAKPOINTS = {
	desktopMin : 1200,
	tabLMin : 992,
	tabLMax : 1199,
	tabPMin : 768,
	tabPMax : 991,
	mobMin : 480,
	mobMax : 767
}