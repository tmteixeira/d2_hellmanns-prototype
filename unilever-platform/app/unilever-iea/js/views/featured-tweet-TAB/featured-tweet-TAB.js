/*global define*/

define(function() {
    'use strict';

    var FeaturedTweetTAB = IEA.module('UI.featured-tweet-TAB', function(featuredTweetTAB, app, iea) {

        _.extend(featuredTweetTAB, {

            defaultSettings: {
                tweetWrapper: '.c-featured-tweet__wrapper',
                template: 'featured-tweet-TAB/partial/featured-tweet-single.hbss'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');
                
                // call methods
                this._switchImageHandler();
                
                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/          
            _switchImageHandler: function() {
                var self = this,
                    componentJson = this.getModelJSON().featuredTweetTAB,
                    template = self.getTemplate('featured-tweet', self.defaultSettings.template),
                    $tweetWrapper = self.$el.find(self.defaultSettings.tweetWrapper),
                    arrayIndex = 0,
                    currentDate = new Date(),
                    startDate = new Date(componentJson.startDate),
                    rotationPeriod = componentJson.rotationPeriod,
                    rotationArrayLength = componentJson.tweetDetails.length,
                    customizedJson = null,                  
                    oneDay = 24*60*60*1000, // hours*minutes*seconds*milliseconds
                    diffDays = Math.floor(Math.abs((currentDate.getTime() - startDate.getTime())/(oneDay))),
                    arrayIndex = (diffDays / parseInt(rotationPeriod)) % rotationArrayLength,
					arrayIndexParsed;
                    
				if(componentJson.startDate === '' || componentJson.rotationPeriod === '') {
					arrayIndexParsed = 0;	
				}
				else{
					arrayIndexParsed = (rotationPeriod === '0') ? 0 : parseInt(arrayIndex);
				}
				
                var customizedJson = {
                    'featuredTweetTAB': {
                        'tweetDetails':componentJson.tweetDetails[arrayIndexParsed],
                        'ctaLabel': componentJson.ctaLabel
                    }
                };
                
                $tweetWrapper.append(template(customizedJson));
                
            }
            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */

        });
    });

    return FeaturedTweetTAB;
});