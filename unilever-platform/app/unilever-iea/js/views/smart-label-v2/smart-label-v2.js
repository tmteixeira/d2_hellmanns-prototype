/*global define*/

define(function() {
    'use strict';

    var SmartLabelV2 = IEA.module('UI.smart-label-v2', function(smartLabelV2, app, iea) {

        _.extend(smartLabelV2, {

            /*********************PUBLIC METHODS******************************************/
            defaultSettings: {
                smartLabelCta: '.js-smart-label-v2__btn'
            },

            events: {
                'change .js-smart-label-v2__select' : '_updateDataOnDropdownChange' //update params on dropdown change
            },

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
               this.triggerMethod('beforEnable');
               // functions to be wrirren here

               this.triggerMethod('enable');     
            },

            /**************************PRIVATE METHODS******************************************/ 

            _updateDataOnDropdownChange: function (evt) {
                evt.preventDefault();

                var self = this,
                    $this = $(evt.currentTarget),
                    componentJson = self.getModelJSON().smartLabelV2,
                    $parentWrapper = $this.parents('.c-smart-label-v2__wrapper'),
                    currentIndex = evt.currentTarget.selectedIndex,
                    listingIndex = parseInt($parentWrapper.data('index')),
                    currentData = componentJson.product.productsDetail[currentIndex],
                    $retail = $parentWrapper.find('.c-dropdown-label__retail'),
                    $unit = $parentWrapper.find('.c-dropdown-label__unit'),
                    $price = $parentWrapper.find('.c-dropdown-label__price'),
                    locale = componentJson.market,
                    ctaUrl;

                if (currentData) {
                    $price.html(currentData.price);
                    $unit.html(currentData.unit);
                    
                    if (currentData.price) {
                        $retail.html(currentData.retail);
                    } else {
                        $retail.html("");   
                    }

                    //update product data on dropdown change
                    $parentWrapper.attr('data-product-price', currentData.price)
                    .attr('data-product-id', currentData.skuid)
                    .attr('data-product-variant', currentData.unit)
                    .attr('data-product-name', currentData.name);

                    ctaUrl = componentJson.baseUrl + currentData.skuid + '?locale='+locale;
                    self.$el.find(self.defaultSettings.smartLabelCta).attr('href', ctaUrl);
                }                
            }
        });
    });

    return SmartLabelV2;
});
