/*global define*/

define(['formService', 'commonService', 'pwstrength', 'xregexp', 'postmessage'],function() {

    'use strict';
	var formService = null,
        commonService = null;
    
    var Form = IEA.module('UI.form', function(form, app, iea) {

        var self = this,
            fielUploadRequired = false;

        _.extend(form, {
            // Extendable hooks and API methods goes here

            onInit: function() {
                this.updateSetting({
                    calenderAutoclose: false
                });
                this.elementsHandlingEnquiryType();
				formService = new IEA.formService();
				commonService = new IEA.commonService();
            },               
			makeFieldMandatory: function(field, label, fieldVal) {
				var self = this,
					args = arguments, //cache arguments
					fielValArr = (function() {
					  var arr = [];
					  for (var i = 2, j = 0; i < args.length; i++) {
						arr[j] = args[i];
						j++;
					  }

					  return arr;
					})();
				$(document).on('change', field, function() {
					var labelId = "label[for='" + label + "']",
					  $this = $(this),
					  makeMandatory = (function() {
						var flag = false;
						// $(this).find("option[value='" + fieldVal + "']").is(":selected");
						fielValArr.forEach(function(item, index) {
						  flag = flag || $this.find("option[value='" + item + "']").is(":selected")
						});

						return flag;
					  })();
					if (makeMandatory) {
					  if ($(labelId).find('.c-form-mandatory').length === 0) {
						$(labelId).append('<span class="c-form-mandatory">*</span>');
					  } else {
						$(labelId).find('.c-form-mandatory').show();
					  }
					  self.requiredMethod(label, true);
                      self.validator.addRules(self, {
                        rules: self._createValidationRules()
                      });
					} else {
					  $(labelId).find('.c-form-mandatory').hide();
					  self.requiredMethod(label, false);
					}
				});
			},
            
            elementsHandlingEnquiryType: function() {
                var self = this,
					market=this.getModelJSON().form.market,
					locale=  $('#locale').val() || this.getModelJSON().form.locale,
					localeFlag = locale !== 'en-US',
					enquiryOnchangeElements =[$('#contactUs-upcCodeDetail'),$('#contactUs-expiryDate'),$('#contactUs-manufacturingCodeDetail'),$('#contactUs-manufacturingCode'),$('#contactUs-storeNamePurchasedFrom')];
					
				function enquiryFieldMandatory(name){
					var $label = $("label[for='" + name + "']");
					if ($label.find('.c-form-mandatory').length === 0) {
						$label.append('<span class="c-form-mandatory">*</span>');
					} else {
						$label.find('.c-form-mandatory').show();
					}
				}
				function removeMandatory(name) {
					$("label[for='" + name + "']").find('.c-form-mandatory').hide();
					self.requiredMethod(name, false);
				}
				
					
                $(document).on('change', '#contactUs-inquiryType', function() {
                    if ($(this).find("option[value='Product Concern']").is(":selected")) {
                      
						enquiryFieldMandatory('contactUs-product');
                        self.requiredMethod('contactUs-product', true);
                        
                        enquiryFieldMandatory('contactUs-upcCodeDetail');  
                        self.requiredMethod('contactUs-upcCodeDetail', true);
                        enquiryFieldMandatory('contactUs-manufacturingCodeDetail');
                        self.requiredMethod('contactUs-manufacturingCodeDetail', true);
                    }
					
					else {
						
                        removeMandatory('contactUs-product');     
                        removeMandatory('contactUs-upcCodeDetail');     
						removeMandatory('contactUs-manufacturingCodeDetail');     
                    }
                });
               
                //toggle UPC code required state based on upc detail dropdown value
                this.makeFieldMandatory('#contactUs-upcCodeDetail', 'contactUs-upcCode', 'Full');

                //toggle Manufacturing code required state based on manufacturing code detail dropdown value
                this.makeFieldMandatory('#contactUs-manufacturingCodeDetail', 'contactUs-manufacturingCode', 'Full','Partial');
				
                //onload disable the sms checkbox
                $('#optIn-smsBrand').attr("disabled", true);
                $('#optIn-smsAll').attr("disabled", true);
                $('#contact-phoneNumbers-0-value, #optIn-smsAll').on("keyup change blur", function() {
                    if($(this).val() !==''){
                        $('#optIn-smsBrand').attr("disabled", false);
                        $('#optIn-smsAll').attr("disabled", false);
                    }else{
                        $('#optIn-smsBrand').attr("disabled", true);
                        $('#optIn-smsAll').attr("disabled", true);

                        $('#optIn-smsAll').attr('checked', false);
                        $('#optIn-smsBrand').attr('checked', false);
                    }
                });           
            },
            
            /**
             * enable form
             * @method enable
             * @return
             */
            enable: function() {
                this.triggerMethod('beforEnable');

                var moduleName = this.moduleName.hyphenToCamelCase(),
                    modelData = this.getModelJSON()[moduleName],
                    allElementsConfig = modelData.formElementsConfig,
                    idx = 0,
                    item, $elem;
                
                this.$form = $('form', this.$el);
                this.$formElements = this.$form.find(':input'); 
				this.formMethod=this.$form.attr('method');
                this.popup = IEA.popup();
                this.form = {
                    isAjax: this.$form.data('ajax') || (modelData.isAjax) ? modelData.isAjax : false,
                    postURL: this.$form.attr('action'),
                    failureMessage: this.$form.data('fail') || (modelData.failureMessage) ? modelData.failureMessage : 'Submission Failed',
                    successMessage: this.$form.data('success') || (modelData.successMessage) ? modelData.successMessage : 'Thank you for your submission',
                    validation: this.$form.data('validation') || (modelData.validation) ? modelData.validation : false,
                    successRedirectURL: this.$form.data('redirect') || (modelData.redirect) ? modelData.redirect : false,
                    errorRedirectURL: this.$form.data('redirect') || (modelData.failureRedirect) ? modelData.failureRedirect : false
                };

                this.setElementActions();
                
                // add validation rules from the model into the validator engine.
                this.validator.addRules(this, {
                    rules: this._createValidationRules()
                });

                // Getting all the multivalue field together
                for(idx=0; idx < allElementsConfig.length; idx++) {
                    if(allElementsConfig[idx] !== null){
                        item = allElementsConfig[idx].formElement;
                        if(item && item.multivalue) {
                            // adding rule to the first elemnet of multivalue
                            this.multiValueFields[item.name] = {
                                'name': item.name,
                                rules: this.model.validation[item.name]
                            };

                            $elem = $('input[name^='+item.name+']');
                            this._setUpMultiValueField($elem, item.name, item.name+'_1' );

                            delete this.model.validation[item.name];
                        }
                    }   
                }
                
                // Signup form image upload size validation and base64 conversion
                this.fileUploadMethod('.c-form-file');

                this.signupDOBValidation(allElementsConfig);              
                
                // Contact Us en-us mutually exclusive checkboxes
                if(this.$form.find('[name="entity"]').val() === 'contactus_us' || this.$form.find('[name="entity"]').val() === 'signup_us' || this.$form.find('[name="entity"]').val() === 'sampling_us') {
                    this.exclusiveCheckboxes();
                }

                this.triggerMethod('enable');

                // Custom form handling goes here. -- User Account page and User Registration page.
                if($('body').hasClass('profile-page')){
                    var self = this, 
                        submitBtn = this.$form.find('button[type="submit"]'),
                        formDataElem = this.$form.find('[name="form-data-url"]'),
                        changeEvtElems = self.$form.find('input[type=radio], select, input[type=checkbox]'),
                        keyEvtElems = self.$form.find('input').not(changeEvtElems);
                    
                    submitBtn.addClass('hidden');
                    
                    // Bind password strength function for only one form
                    $('body').data('pwstrength-flag') === undefined && $('body').data('pwstrength-flag', true);
                    if(formDataElem.length > 0 && typeof formService.getCookie('token') !== 'undefined') {
                        var formDataUrl = formDataElem.val();
                        
                        if(formDataUrl === "") {
                            changeEvtElems.off('change.enableSave').on('change.enableSave', function(){
                                submitBtn.removeClass('hidden');
                            });

                            keyEvtElems.off('keyup.enableSave').on('keyup.enableSave', function(){
                                submitBtn.removeClass('hidden');
                            });
                        } else {
                            $.ajax({
                                url: formDataUrl,
                                type: 'GET',
                                cache: false,
                                dataType: 'json',
                                processData: false, 
                                contentType: false, 
                                success: function(data) {
                                    var mappingsObj = formService.jsonToForm(data);
                                   
                                  if(self.$form.find('#userSurveyId-submit').length == 0) {
                                        formService.populateFormData(mappingsObj,self.$form);
                                    }
                                   
                                    if(data.SurveyResponses && self.$form.find('#userSurveyId-submit').length >0) {
                                        self.profileSurveyFormHandling(data, self.$form);
                                    } 
                                    
                                    changeEvtElems.off('change.enableSave').on('change.enableSave', function(){
                                        submitBtn.removeClass('hidden');
                                    });

                                    keyEvtElems.off('keyup.enableSave').on('keyup.enableSave', function(){
                                        submitBtn.removeClass('hidden');
                                    });
                                },
                                error: function() {
                                }
                            });
                        }
                    } else {
                        changeEvtElems.off('change.enableSave').on('change.enableSave', function(){
                            submitBtn.removeClass('hidden');
                        });

                        keyEvtElems.off('keyup.enableSave').on('keyup.enableSave', function(){
                            submitBtn.removeClass('hidden');
                        });
                    }
                    
                    if($('body').data('pwstrength-flag')) {
                        $('[name="NewCredentials-Password"]').pwstrength();
                        $('[name="EmailAddress"]').attr('readonly', 'true').addClass('read-only-field');
                        $('body').data('pwstrength-flag', false);
                    }
                    
                    this.$form.find('#js-your-preferences').click(function(e) {
                        $('.c-preferences-content').stop().slideToggle(300);
                        e.preventDefault();
                    });
                    
                    if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                        this.$form.find('input[type="radio"][name*="SurveyResponses"]').on('change', function() {
                            var ev = {},
                                compName = $('input[type="radio"][name*="SurveyResponses"]').parents('form').data('componentname'),
                                compVariants = $('input[type="radio"][name*="SurveyResponses"]').parents('form').data('component-variants'),
                                compPositions = $('input[type="radio"][name*="SurveyResponses"]').parents('form').data('component-positions'),
                                surveyId = $('[type="hidden"][name*="SurveyId"][id*="SurveyId"]')[0].value;

                            digitalData.component = [];
                            digitalData.component.push({
                                'componentInfo' :{
                                    'componentID': compName,
                                    'name': compName
                                },
                                'attributes': {
                                    'position': compPositions,
                                    'variants': compVariants
                                }
                            });
                            ev.eventInfo={
                              'type':ctConstants.trackEvent,
                              'eventAction': ctConstants.survey,
                              'eventLabel' : surveyId + ' - Survey Edited'
                            };
                            ev.category ={'primaryCategory':ctConstants.custom};
                            digitalData.event.push(ev);
                        });
                    }
                    
                    this._successHandler = function(data, status, xhr) {
                        var msgElem = $('.profile-form-success-msg'),
                            sucessMsgCont = $('<div class="profile-form-success-msg"><span class="profile-success-msg-text">' + this.form.successMessage + '</span></div>');
                        if(msgElem.length  > 0) {
                            $('.profile-success-msg-text').text($(this.form.successMessage).text());
                            $('.profile-form-success-msg').fadeIn('fast');
                        } else {
                            $('.js-profile-tabs-cont').prepend(sucessMsgCont);
                        }
						
						if($('.form-error-msg').length){
							$('.form-error-msg').each(function(){
								$(this).remove();
							});
						}
						
						if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
							var ev = {},
								compName = this.$form.data('componentname'),
								compVariants = this.$form.data('component-variants'),
								compPositions = this.$form.data('component-positions');
							
							digitalData.component = [];
							digitalData.component.push({
								'componentInfo' :{
									'componentID': compName,
									'name': compName
								},
								'attributes': {
									'position': compPositions,
									'variants': compVariants
								}
							});
							
							ev.eventInfo={
							  'type': ctConstants.trackEvent,
							  'eventAction': ctConstants.profileEdit,
							  'eventLabel' : 'ACCOUNT | INFORMATION'
							};
							ev.category ={'primaryCategory':ctConstants.custom};
							digitalData.event.push(ev);
                            
                            //Subscribe Newsletter
                            if($('input[name*="NewsLetterOptIns-OptIns-0-OptIn"').is(':checked')){
                                digitalData.component = [];
                                digitalData.component.push({
                                    'componentInfo' :{
                                        'componentID': compName,
                                        'name': compName
                                    }
                                });

                                ev.eventInfo={
                                    'type':ctConstants.trackEvent,
                                    'eventAction': ctConstants.suscribeNewsletter,
                                    'eventLabel' : "Signup/Registration"
                                };
                                ev.category ={'primaryCategory':ctConstants.other};
                                digitalData.event.push(ev);
                            }

                            //Other OptIns
                            if($('input[name*="ChannelOptIns-0-OptIn"]').is(':checked')){
                                digitalData.component = [];
                                ev.eventInfo={
                                    'type': ctConstants.trackEvent,
                                    'eventAction': ctConstants.Acquisition,
                                    'eventLabel' : 'BRAND OPTIN & CORPORATE OPTIN'
                                };
                                ev.category ={'primaryCategory':ctConstants.conversion};
                                digitalData.event.push(ev);
                            }
                            
                            //Survey Submitted
                            if(this.$form.find('.c-user-survey-content').length) {
                                var ev = {},
                                    compName = this.$form.data('componentname'),
                                    compVariants = this.$form.data('component-variants'),
                                    compPositions = this.$form.data('component-positions'),
                                    surveyId = $('[type="hidden"][name*="SurveyId"][id*="SurveyId"]')[0].value;
                                
                                digitalData.component = [];
                                digitalData.component.push({
                                    'componentInfo' :{
                                        'componentID': compName,
                                        'name': compName
                                    },
                                    'attributes': {
                                        'position': compPositions,
                                        'variants': compVariants
                                    }
                                });
                                
                                ev.eventInfo={
                                  'type':ctConstants.trackEvent,
                                  'eventAction': ctConstants.survey,
                                  'eventLabel' : surveyId + ' : Survey Updated'
                                };
                                ev.category ={'primaryCategory':ctConstants.custom};
                                digitalData.event.push(ev);
                            }
                            
						}
						
						setTimeout(function() {
                            $('.profile-form-success-msg').fadeOut('fast');
                        }, 20000);
                        this.isSubmitted = false;
                    };
                    
                    if(submitBtn.length > 0 && submitBtn[0].id === 'profileEmailPassword-submit') {
                         this._errorHandler = function(xhr) {
                            try{
                                var res = JSON.parse(xhr.responseText);
                                if(xhr.status === 400 && res.ErrorCode !== '') {
                                    var $errorBlock = null,
                                        msg = $('[name="OldCredentials-Password"]').data('form-invalid-old-password-msg');
                                    if($('.c-profile-tabs-cont').find('.form-error-msg').length === 0) {
                                        $errorBlock = $('<div class="form-error-msg text-danger"><span class="glyphicon glyphicon-warning-sign"></span><span class="error-msg-text">' + msg + '</span></div>');
                                        this.$form.before($errorBlock);
                                    } else {
                                        this.$form.find('.error-msg-text').text(msg);
                                    }
                                    this.isSubmitted = false;
                                    return;
                                }
                                else {
                                    this.$form.prev('.form-error-msg').remove();
                                }
                            } catch (err){
                                console && console.log('User validation failed.', err);
                            }
                        }
                    }

                var self= this;
                 this.$form.find('#userSurveyId-submit').click(function() { 
                    self.surveyFormHandling();
                });
                
                } else if($('body').hasClass('registration-page')){
                    var forms = $('.registration-page .c-tab-element-wrapper-content form');
                    $('body').data('pwstrength-flag') === undefined && $('body').data('pwstrength-flag', true);
                    forms.removeClass('active');
                    if(typeof formService.getCookie('token') === 'undefined'){
                        forms.filter(':first').addClass('active');
                    } else if(forms.length > 3){
                        forms.filter(':eq(2)').addClass('active');
                    }
                    var submitBtn = this.$form.find('button[type="submit"]');
                    if(submitBtn.length > 0){
                        switch(submitBtn[0].id){
                            case 'userEmailId-submit': 
                                this._successHandler = function(data, status, xhr){
                                    var regStatus = data.UserRegistrationStatus;
                                    // var regStatus = 2;
                                    if(regStatus == 2){
                                        var emailId = this.$form.find('[name=Credentials-Username]').val();
                                        $('[name=Credentials-Username]').val(emailId);
                                        $('.js-btn-login').trigger('click');
                                        $('.c-login-wrap .c-already-registered-login').css("visibility","visible");
                                    } else if(regStatus == 1) {
                                        // regStatus == 1 is for user exists in Unilever but for other brand, remove password field and move to next tab
                                        var $pwdForm = $('#userPasswordId-submit').parents('form');
                                        $pwdForm.find('[name="Credentials-confirmPassword"]').parents('.form-group').hide();
                                        
                                        $pwdForm.find('label[for="Credentials-Password"]').text($pwdForm.find('[name="Credentials-Password"]').data('form-alreadyregisteredlogin-msg'));
                                        $pwdForm.find('[name="Credentials-Password"]').off('blur').on('blur', function() {
                                            $pwdForm.find('[name="Credentials-confirmPassword"]').val(this.value);
                                        }); 
                                        formService.registrationHelpers().moveNext();
                                        $('#userPreferencesId-submit').attr('actionType', 'update');
                                    }
                                    this.isSubmitted = false;
                                };
                                this._errorHandler = function(xhr){
                                    try{
                                        var res = JSON.parse(xhr.responseText);
                                        if(xhr.status === 401 && res.ErrorCode !== '') {
                                            var $errorBlock = null,
                                                msg = this.$form.find('[name="Credentials-Username"]').data('form-invaliddomain-msg');
                                            if($('.c-tab-element-wrapper-content').find('.form-error-msg').length === 0) {
                                                $errorBlock = $('<div class="form-error-msg text-danger"><span class="glyphicon glyphicon-warning-sign"></span><span class="error-msg-text">' + msg + '</span></div>');
                                                this.$form.before($errorBlock);
                                            } else {
                                                this.$form.find('.error-msg-text').text(msg);
                                            }
                                            this.isSubmitted = false;
                                            return;
                                        } else if(xhr.status === 404 && res.ErrorCode !== ''){
                                            $('#userPasswordId-submit').parents('form').data('customSubmitHandler', true);
                                            if($('body').data('pwstrength-flag')) {
                                                $('[name="Credentials-Password"]').pwstrength();
                                                $('body').data('pwstrength-flag', false);
                                            }
                                            formService.registrationHelpers().moveNext();
                                            this.isSubmitted = false;
                                            return;
                                        }
                                    } catch (err){
                                        console && console.log('User validation failed.', err);
                                    }
                                }
                                break;
                            case 'userPasswordId-submit':
                                submitBtn.parents('form').data('customSubmitHandler', true);
                                this.customSubmitHandler = function(){
                                    var self = this;
                                    if($('#userPreferencesId-submit').attr('actionType') === 'update'){
                                        // update flow, send login request
                                        var url = self.form.postURL,
                                            forms = $('.registration-page .c-tab-element-wrapper-content form'),
                                            formElems = $(forms[0]).add($(forms[1])).find(':input').not('[type=submit]').not('[data-exclude]').not('[name=Credentials-confirmPassword]'),
                                            loginData = formService.convertToJSON({}, formElems, formService),
                                            submitxhr = $.ajax({
                                                type: 'POST',
                                                url: url,
                                                contentType: 'application/json',
                                                dataType: 'json',
                                                data: loginData
                                            });

                                        submitxhr.done(function(data, status, xhr){
                                            if(!data.ErrorCode) {
                                                var allfieldsMappingsObj = formService.jsonToForm(data.User),
                                                forms = $('.registration-page .c-tab-element-wrapper-content form');
                                                formService.populateFormData(allfieldsMappingsObj,forms);
                                                formService.registrationHelpers().moveNext();
                                                self.isSubmitted = false;
                                            }
                                        });

                                        submitxhr.fail(function(err){
                                            var $errorBlock = null;
                                            if(err.errors !== undefined){
                                                for(var i=0; i<err.errors.length; i++){
                                                    $errorBlock = $('<div class="form-error-msg text-danger"><span class="glyphicon glyphicon-warning-sign"></span><span class="error-msg-text">' + err.errors[i].error + '</span></div>');
                                                    self.$form.before($errorBlock);
                                                }
                                            }
                                        });

                                    } else {
                                        // new user registration, get both passwords and move to next step
                                        var pwd = $('.c-tab-element-wrapper-content [name=Credentials-Password]').val(),
                                            cnfPwd = $('.c-tab-element-wrapper-content [name=Credentials-confirmPassword]').val(),
                                            isMatch = pwd !== '' && pwd === cnfPwd,
                                            $span = $("<span />").addClass('pwd-error-msg-text help-block-error').text('Passwords should be same.');

                                        if(isMatch){
                                            $('.pwd-error-msg-text').hide();
                                            formService.registrationHelpers().moveNext();
                                        } else {
                                            if($('.pwd-error-msg-text').length===0){
                                                $('.c-tab-element-wrapper-content [name=Credentials-confirmPassword]').addClass('').parent().append($span);
                                            }
                                            $('.pwd-error-msg-text').show();
                                        }
                                    }
                                };

                                break;
                            case 'userDetailsId-submit': 
                                if(typeof formService.getCookie('token') !== 'undefined'){
                                    $('#userPreferencesId-submit').attr('actionType', 'update');
                                    var formDataElem = this.$form.find('[name="form-data-url"]'),
                                    self = this;
                                    if(formDataElem.length > 0 && formDataElem.val() !== "") {
                                        $.ajax({
                                            url: formDataElem.val(),
                                            type: 'GET',
                                            cache: false,
                                            dataType: 'json',
                                            processData: false, 
                                            contentType: false, 
                                            success: function(data) {
                                                var mappingsObj = formService.jsonToForm(data);
                                                formService.populateFormData(mappingsObj,self.$form);
                                                
                                                var height = $('form.active .c-registration-tab-content').height() + 220 ; // adding 50px to fit in top bar plus 170px for registration header content.
                                                $('form.active').parents('.c-tab-element-wrapper-content').height(height);
                                                $('#Profile-Birthday').val($('#Profile-Birthday').val().split('T')[0]);
                                            }
                                        });
                                    }
                                }
                                this.$form.data('customSubmitHandler', true);
                                this.customSubmitHandler = function(){
                                    if(this.isFormValid){
                                        formService.registrationHelpers().moveNext();
                                    }
                                };
                                break;
                            case 'userSurveyId-submit':
                                var self = this;
                                this.$form.find('#userSurveyId-submit').click(function() { 
                                    self.surveyFormHandling();
                                });
                                
                                this.$form.data('customSubmitHandler', true);
                                this.customSubmitHandler = function(){
                                    if(this.isFormValid){
                                        formService.registrationHelpers().moveNext();
                                    }
                                };
                                break;
                            case 'userPreferencesId-submit':
                                this.$form.data('customSubmitHandler', true);
                                this.customSubmitHandler = function(){
                                    if(this.isFormValid){
                                        formService.registrationHelpers().registerUser(this.$form);
										
										if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
											if($('.form-error-msg').length === 0){
												var ev = {},
                                                    compName = this.$form.data('componentname');
                                                
                                                digitalData.component = [];
                                                digitalData.component.push({
                                                    'componentInfo' :{
                                                        'componentID': compName,
                                                        'name': compName
                                                    }
                                                });
												ev.eventInfo={
												  'type':ctConstants.trackEvent,
												  'eventAction': ctConstants.registration,
												  'eventLabel' : "Signup/Registration"
												};
												ev.category ={'primaryCategory':ctConstants.other};
												digitalData.event.push(ev);
												
												//Subscribe Newsletter
												if($('input[name*="NewsLetterOptIns-OptIns-0-OptIn"').is(':checked')){
													digitalData.component = [];
                                                    digitalData.component.push({
                                                        'componentInfo' :{
                                                            'componentID': compName,
                                                            'name': compName
                                                        }
                                                    });
                                                    
                                                    ev.eventInfo={
														'type':ctConstants.trackEvent,
														'eventAction': ctConstants.suscribeNewsletter,
														'eventLabel' : "Signup/Registration"
													};
													ev.category ={'primaryCategory':ctConstants.other};
													digitalData.event.push(ev);
												}
												
												//Other OptIns
												if($('input[name*="ChannelOptIns-0-OptIn"]').is(':checked')){
                                                    digitalData.component = [];
                                                    ev.eventInfo={
														'type': ctConstants.trackEvent,
														'eventAction': ctConstants.Acquisition,
														'eventLabel' : 'BRAND OPTIN & CORPORATE OPTIN'
													};
													ev.category ={'primaryCategory':ctConstants.conversion};
													digitalData.event.push(ev);
												}
												
											}
										}
                                        
                                        this.surveySubmitTracking();
                                    }
                                };
                                break;
                        } 
                    }
                } else if($('body').hasClass('forgot-password-page') || $('body').hasClass('reset-password-page')) {
                    var pageName = $('body').hasClass('forgot-password-page') ? 'forgot' : 'reset';
                    if(pageName === 'reset') {
                        $('body').data('pwstrength-flag') === undefined && $('body').data('pwstrength-flag', true);
                        
                        $('[name="token"]').val(commonService.getQueryParams('token'));
                        $('[name="userId"]').val(commonService.getQueryParams('userId'));
                        
                        if($('body').data('pwstrength-flag')) {
                            $('[name="NewCredentials-Password"]').pwstrength();
                            $('body').data('pwstrength-flag', false);
                        }
                    } else {
                        $('#js-remember-password').click(function() {
                            if($(this).attr('href') === '' || $(this).attr('href') === '#.html') {
                                if(document.referrer) {
                                    window.location.href = document.referrer;
                                } else {
                                    window.location.href = window.history.back(1);
                                }
                                return false;
                            } else {
                                return true;
                            }
                        });
                    }
                    this._successHandler = function(data, status, xhr){
                        var $sucessMsgCont = $('<div class="form-success-msg"><span class="' + pageName + '-success-msg-text">' + this.form.successMessage + '</span></div>');
                        this.$form.before($sucessMsgCont);                        
                        if(this.$form.parent().find('.form-error-msg').length) {
							$('.form-error-msg').remove();
						}
						this.$form.hide();
                        
						$('.' + pageName + '-page-redirect-content').find('.c-call-out-button').show();
                        var $redirectLink = $('.' + pageName + '-page-redirect-content').find('a');
                        
                        $redirectLink.click(function() {
                            if($(this).attr('href') === '' || $(this).attr('href') === '#.html') {
                                if(pageName === 'forgot') {
                                    if(document.referrer) {
                                        window.location.href = document.referrer;
                                    } else {
                                        window.location.href = window.history.back(1);
                                    }
                                } else {
                                    $('.js-btn-login').trigger('click');
                                }
                                return false;
                            } else {
                                return true;
                            }
                        });
                        this.isSubmitted = false;
                    };
                    this._errorHandler = function (xhr) {
                        try {
                            var res = JSON.parse(xhr.responseText),
                                $errorMsgCont = '',
                                msg = '';
                            
                            if (xhr.status === 403 && res.ErrorCode !== '') {
                                msg = this.form.failureMessage;
                                if(this.$form.parent().find('.form-error-msg').length === 0) {
                                    $errorMsgCont = $('<div class="form-error-msg"><span class="' + pageName + '-error-msg-text">' + msg + '</span></div>');
                                    this.$form.before($errorMsgCont);
                                } else {
                                    this.$form.find('.' + pageName + '-error-msg-text').text(msg);
                                }
                                this.isSubmitted = false;
                                return;
                            } else if(xhr.status === 404) {
                                msg = this.$form.find('[name="Credentials-Username"]').data('form-invalidemail-msg');
                                if(this.$form.find('.form-error-msg').length === 0) {
                                    $errorMsgCont = $('<div class="form-error-msg"><span class="' + pageName + '-error-msg-text">' + msg + '</span></div>');
                                    this.$form.before($errorMsgCont);
                                } else {
                                    this.$form.find('.' + pageName + '-error-msg-text').text(msg);
                                }
                                this.isSubmitted = false;
                                return;
                            }
                        } catch (err) {
                            console && console.log("email doesn't exist", err);
                        }
                    }
                } 
                else if($('body').hasClass('sign-up-page')){
                    $('.c-form-submit-btn').on('click', function(){
                        if($('#contact-birthday').val() !== ""){
                            var d = new Date($('#contact-birthday').val()),
                            newDobFormat = d.toISOString().split('.')[0];
                            $('#contact-birthday').val(newDobFormat) ;                            
                        }
                    })
                }
                
                $("input:radio[name^='surveyResponseList-']").click(function () {
					var indexOfHyphen = this.id.indexOf("-", 23);
					var fieldNamePrefix = this.id.substring(0, indexOfHyphen);
					fieldNamePrefix = fieldNamePrefix + "-";
					indexOfHyphen = this.id.indexOf("-", fieldNamePrefix.length + 1);
					fieldNamePrefix = this.id.substring(0, indexOfHyphen + 1);
					var questionIDFieldName = fieldNamePrefix + "id";
					$('input[name=' + questionIDFieldName + ']').removeAttr('data-exclude');
					var questionLabelFieldName = fieldNamePrefix + "question";
					$('input[name=' + questionLabelFieldName + ']').removeAttr('data-exclude');
					var answerFieldName = fieldNamePrefix + "answers-0-answer";
					$('input[name=' + answerFieldName + ']').removeAttr('data-exclude');
					$(this).removeAttr('data-exclude');
					$(this).attr("data-type","number");
					var answerFieldLabel = $('label[for=' + $(this).attr('id') + ']').data('label');
					$('input[name=' + answerFieldName + ']').val(answerFieldLabel);
					var surveyFieldName = this.id.split('-').slice(0,-6).join('-') + "-surveyId";
					$('input[name=' + surveyFieldName + ']').removeAttr('data-exclude');
				});

                if(window.name === 'secure-frame-login') {
                    this.tooltipEvent('.c-tooltip-info__icon', '.c-tooltip-info__copy', '.c-tooltip-info');
                }
                $("#rememberme").change(function() {
                    if(this.checked) { 
                        if($("#rememberMeExpiration").length > 0) {
                           $("#ExpirationDuration").val($("#rememberMeExpiration").val());
                        }
                    }
                    if(!this.checked) {
                        if($("#defaultExpiration").length > 0) {
                           $("#ExpirationDuration").val($("#defaultExpiration").val()); 
                       }                        
                    }
                });

            },
             tooltipEvent: function(selector, wrapper, parentWrapper) {
                var animationSpeed = 800,
                    isActive = 'is-active';
                    
                $(document).on('click', selector, function(e) {
                    e.preventDefault();
                    var $this = $(this),
                    contentWrapper = $(this).parents(parentWrapper).find(wrapper);

                    if ($(selector).hasClass(isActive) && !$this.hasClass(isActive)) {
                        $(selector).removeClass(isActive);
                        $(wrapper).slideUp(animationSpeed);
                    }
                    
                    $this.toggleClass(isActive, 200);

                    if ($this.hasClass(isActive)) {
                        contentWrapper.slideDown(animationSpeed);
                    } else {
                        contentWrapper.slideUp(animationSpeed);
                    }
                });
            },
            exclusiveCheckboxes : function() {
                $('[name*="optIn-"]').on('change', function() {
                    $('[name*="optIn-"]').not(this).prop('checked', false);  
                });
            },
            
            surveySubmitTracking: function() {
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    var ev = {},
                        eventLabelArray = [],
                        eventLabel,
                        surveyId = $('[type="hidden"][name*="SurveyId"][id*="SurveyId"]')[0].value,
                        compName = $('[type="radio"][name*="SurveyResponses"]').parents('form').attr('data-componentname'),
                        compVariants = $('[type="radio"][name*="SurveyResponses"]').parents('form').data('component-variants'),
                        compPositions = $('[type="radio"][name*="SurveyResponses"]').parents('form').data('component-positions');

                    $('[type="radio"][name*="SurveyResponses"]').each(function() {
                        if($(this).is(':checked')){
                            var ansName = $(this).parents('label').data('label'),
                                queName = $(this).parents('.form-group').find('.c-control-label_radio').text(),
                                queAns = queName + ' - ' + ansName;

                            eventLabelArray.push(queAns);
                            eventLabel = eventLabelArray.join(" | ");
                        }
                    });

                    digitalData.component = [];
                    digitalData.component.push({
                        'componentInfo' :{
                            'componentID': compName,
                            'name': compName
                        },
                        'attributes': {
                            'position': compPositions,
                            'variants': compVariants
                        }
                    });

                    ev.eventInfo={
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.survey,
                      'eventLabel' : surveyId + ' - ' + eventLabel
                    };
                    ev.category ={'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                }
            },
            
            surveyFormHandling: function() {
                var self= this;
                self.$form.find('[type="radio"][name*="SurveyResponses"]').each(function() {
                    if($(this).is(':checked')){
						 var ansNameAttr = $(this).attr('name').split('-'),
							 queNameAttr =  ansNameAttr.slice(0, -1).join('-') + '-QuestionId',
							 surveyNameAttr = ansNameAttr.slice(0,-3).join('-') + '-SurveyId';

						$('[name="' + queNameAttr + '"]').removeAttr('data-exclude');
						$('[name="' + surveyNameAttr + '"]').removeAttr('data-exclude');
					}
                });
            },
            profileSurveyFormHandling: function(data, form) {
            if(form.find('[name^="SurveyResponses"]').length >0) {
               var surveyObject = data,
                    questionIds = [],i,j;

                if (surveyObject.hasOwnProperty('SurveyResponses')) {
                    form.find('[name^="SurveyResponses"][name$="AnswerId"]').removeAttr('checked');
                    for ( i = 0; i < surveyObject.SurveyResponses.Responses.length; i++) {
                        for ( j = 0; j < surveyObject.SurveyResponses.Responses[i].QuestionAnswers.length; j++) {
                           var questionId = surveyObject.SurveyResponses.Responses[i].QuestionAnswers[j].QuestionId,
                             answerId = surveyObject.SurveyResponses.Responses[i].QuestionAnswers[j].AnswerId,
                             formElem = form.find('[name^="SurveyResponses"][name$="QuestionId"][value="' + questionId + '"]'),
                             questionIdAttr = formElem.attr('id'),
                             splitQuestion = questionIdAttr.split('-'),
                             surveyNameAttr = splitQuestion.slice(0, -3).join('-') + '-SurveyId',
                             AnswerIdAttr = splitQuestion.slice(0, -1).join('-') + '-AnswerId-' + answerId;
                            $('[name="' + questionIdAttr + '"]').removeAttr('data-exclude');
                            $('[name="' + surveyNameAttr + '"]').removeAttr('data-exclude').val(surveyObject.SurveyResponses.Responses[0].SurveyId);
                            $('#' + AnswerIdAttr).prop('checked', true);
                        }
                    }
                }
            }
            },
            signupDOBValidation: function(allElementsConfig) {
               //toggle age limit to 18years when state is "MAINE"
               var
                   idx = 0,
                   defaultAge = "",
                   countryDefaultAge = "",
                   stateAge = "",
                   country, errMsg, j,
                   item, elementRule = {};

               for (idx = 0; idx < allElementsConfig.length; idx++) {
                   if (allElementsConfig[idx] !== null) {
                       item = allElementsConfig[idx].formElement;
                       if (typeof item !== 'undefined' && typeof item.dateFormat !== 'undefined') {
                               if (typeof item.messageConfigs !== 'undefined' && item.messageConfigs[0].key !== "undefined") {
                                   if (item.messageConfigs[0].key === 'ageGateError') {
                                       errMsg = item.messageConfigs[0].value;
                                   }
                               }
                               if (typeof item.customValidation !== 'undefined') {

                                   for (j = 0; j < item.customValidation.countries.length; j++) {
                                       if (item.customValidation.countries[j].name === $('#contact-country').val()) {
                                           countryDefaultAge = item.customValidation.countries[j].defaultAge;
                                       }
                                   }

                                   defaultAge = item.customValidation.defaultAge;
                                   var result = $.grep(item.customValidation.countries, function(v, i) {
                                       if (v.name === $('#contact-country').val()) {
                                           country = v;
                                       }
                                   });

                               }
                       }
                   }
               }

               $('select#contact-locality').on('change', function() {
                    dateChange();                   
               });
                $('#contact-birthday').on('changeDate', function() {
                    dateChange();                   
               });
               function dateChange() {
                var dobInputFieldSelector = $("input[name='contact-birthday']"),
                       //  maineErrorMsg = dobInputFieldSelector.attr("data-msg-maine"),
                       maineErrorMsg = errMsg,
                       dobValue = dobInputFieldSelector.val(),
                       maxAge = "",
                       i;

                   //age = country.filter(function (states) { return states.name == $(this).val() }); contact-locality
                   for (i = 0; i < country.states.length; i++) {
                       if (country.states[i].name === $('#contact-locality').val()) {
                           stateAge = country.states[i].age;
                       }
                   }

                   if (stateAge !== "") {
                       maxAge = stateAge;
                   } else if (countryDefaultAge !== "") {
                       maxAge = countryDefaultAge;
                   } else {
                       maxAge = defaultAge;
                   }


                   if (dobValue !== '') {
                       var curr = new Date();
                       curr.setFullYear(curr.getFullYear() - maxAge);
                       var dob = Date.parse($("#contact-birthday").val());
                       if (curr < dob) {
                           dobInputFieldSelector.val("");

                           dobInputFieldSelector.parent().next('.help-block').text(maineErrorMsg).removeClass('hidden');
                       }
                       maxAge = "-" + maxAge + "Y";
                       dobInputFieldSelector.datepicker({ "maxDate": maxAge });
                       dobInputFieldSelector.focusin(function() {
                           dobInputFieldSelector.parent().next('.help-block').addClass('hidden');
                       });
                   }
               }
            },

     

            fileUploadMethod: function(selector) {
                var $selector = selector;
                // Signup form image upload size validation and base64 conversion
                $(document).on('change', $selector, function(event) {
                    var files,
                        servletUrl = $(this).parents('.form-group').data('servlet-url'),
                        sizeLimit = $(this).data('size-limit'),
                        sizeValMsg = $(this).data('size-msg'),
                        target = $(this),
                        exts = ['jpg','gif','png','jpeg'],
                        fileVal = target.val();
                                        
                    files = event.target.files;

                    var data = new FormData();
                    $.each(files, function(key, value) {
                        data.append(key, value);
                    });
                     
                    var getExt = fileVal.split('.');
                    getExt = getExt.reverse();

                    if ($.inArray(getExt[0].toLowerCase(), exts) > -1 ) {
                        target.parents('.form-group').removeClass('file-error');
                        target.next('.file-help-block').addClass('hidden');
                    } else {
                        target.parents('.form-group').addClass('file-error');
                        target.next('.file-help-block').removeClass('hidden').text(target.next('.file-help-block').data("format-msg"));
                    }
                    $.ajax({
                        url: servletUrl + '?sizelimit=' + sizeLimit,
                        type: 'POST',
                        data: data,
                        cache: false,
                        dataType: 'json',
                        processData: false, 
                        contentType: false, 
                        success: function(data) {
                            if(data.status == 'success') {
                                target.data('fileencoded', data.encodedString).data('filename', data.name).data('filesize', data.size).data('filepath', '');
                            } else {
                                if (target.next('.file-help-block').hasClass('hidden')) {
                                    target.next('.file-help-block').removeClass('hidden').text(sizeValMsg);
                                } else {
                                    target.next('.file-help-block').text(sizeValMsg);
                                }
                            }
                        },
                        error: function() {
                        }
                    });
                });
            },

            onFormSubmitTracking: function() {
                var ev = {}, compName = $('.c-form-submit-btn').parents('form').data('componentname');
                
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
					
					digitalData.component.push({'componentInfo' :{
					   	'componentID': compName,
						'name': compName
					}});
					
					var eventLabel = $('h1.c-form-copy-content__headline').text();
					ev.eventInfo={
						'type':ctConstants.trackEvent,
						'eventAction': ctConstants.suscribeNewsletter,
						'eventLabel': eventLabel
					};
					ev.category ={'primaryCategory':ctConstants.other};
					digitalData.event.push(ev);
					
                    if (($('#optIn-onlineBrand').is(':checked')) && ($('#optIn-onlineAll').is(':checked'))) {
                        ev.eventInfo={
                            'type':ctConstants.trackEvent,
                            'eventAction': ctConstants.Acquisition,
                            'eventLabel' : 'BRAND OPTIN & CORPORATE OPTIN'
                        };
                        ev.category ={'primaryCategory':ctConstants.conversion};
                        digitalData.event.push(ev);
                    } else if ($('#optIn-onlineBrand').is(':checked')) {
                        ev.eventInfo={
                          'type':ctConstants.trackEvent,
                          'eventAction': ctConstants.Acquisition,
                          'eventLabel' : 'BRAND OPTIN'
                        };
                        ev.category ={'primaryCategory':ctConstants.conversion};
                        digitalData.event.push(ev);                   
                    } else if ($('#optIn-onlineAll').is(':checked')) {
                        ev.eventInfo={
                          'type':ctConstants.trackEvent,
                          'eventAction': ctConstants.Acquisition,
                          'eventLabel' : 'CORPORATE OPTIN'
                        };
                        ev.category ={'primaryCategory':ctConstants.conversion};
                        digitalData.event.push(ev);                    
                    }
					
					digitalData.component = [];
                

					//Contact us tracking
					if ($('.contact-page').length) {
						var enqType = $('#contactUs-inquiryType option:selected').val(),
							prodToDiscuss = $('#contactUs-product').val(),
							contactMethod = [];

						$.each(
							$('input[class="c-input-checkbox"]:checked'), function() {contactMethod.push($(this).attr('id'));}
						);	

						digitalData.component.push({
							'componentInfo' :{
								'componentID': compName,
								'name': compName
							}
						});

						ev.eventInfo={
						  'type':ctConstants.trackEvent,
						  'eventAction': ctConstants.contactus,
						  'eventLabel' : 'Contact-Us - ' + enqType + ' | ' + prodToDiscuss + ' | ' + contactMethod  
						};
						ev.category ={'primaryCategory':ctConstants.other};
						digitalData.event.push(ev);

						digitalData.component = [];	
					}
				}
            },
            
             _successHandler: function(data) {
                if(window.name === 'secure-frame-login') {
                    var modelData = this.getModelJSON().form,                   
                        originURL = window.location.hostname,
                        resHeader = data.UserRegistrationStatus;

                    if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
						var ev = {};
						ev.eventInfo={
						'type': ctConstants.trackEvent,
						'eventAction': ctConstants.signIns,
						'eventLabel' : "Signup/Registration"
						};
						ev.category ={'primaryCategory':ctConstants.other};
						digitalData.event.push(ev);
					}
					if(location.protocol === "https:") { 
                        $.postMessage({ resHeader: resHeader },'http://'+originURL, parent); 
                    }
                    return; 
                }
                if(data.code === 200 || data.code === 201) {
                    if(typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                        this.onFormSubmitTracking();
                    }
                    if (typeof this.form.successRedirectURL !== 'undefined' && this.form.successRedirectURL !== '' && this.form.successRedirectURL !== false) {
                        // if the router is running, then make sure its using hash url change
                        // otherwise change the complete URL
                        if (IEA.History.started) {
                            Router.navigate(this.form.successRedirectURL, {
                                trigger: true
                            });
                        } else {
                            window.location.href = this.form.successRedirectURL;
                        }
                    } else {
                        // remove error msg
                        $('.form').find('.form-error-msg').each(function(){
                            $(this).remove();
                        })
                        
                        this.$sucessMsgBlock = $('<div class="form-success-msg"><span class="success-msg-text">' + this.form.successMessage + '</span></div>');
                        this.$form.before(this.$sucessMsgBlock);                        
                        this.$form.hide();
						$('.c-registration-tab-return-button-content').show();
						$('.c-registration-tab-return-button-content a').addClass('o-btn o-btn--secondary c-form-submit-btn input-submit');
                        $('.form-page .social-sharing').show();
                        if($('.username-success').length>0){
                             $('.username-success').html(this.$form.find('#contact-givenName').val());
                        }
                        if($('.email-success').length>0){
                             $('.email-success').html(this.$form.find('#contact-email').val());
                        }  
                    }
                    // UDMD ID PRM tracking
                    digitalData.trackingInfo.un = data.responseData.results[0].udmdID;
					
					//Form Submit Tracking
					var ev = {},
						compName = this.$form.data('componentname'),
                        compVariants = this.$form.data('component-variants'),
                        compPositions = this.$form.data('component-positions'),
						formName = this.$form.attr('name'),
						formId = this.$form.attr('id');
					
					digitalData.component = [];
					digitalData.component.push({
						'componentInfo' :{
							'componentID': compName,
							'name': compName
						},
                        'attributes': {
                            'position': compPositions,
                            'variants': compVariants
                        }
					});
					ev.eventInfo={
						'type':ctConstants.trackEvent,
						'eventAction': ctConstants.forms,
						'eventLabel' : formName + " - " + formId + " - " + "Form Successfully Submitted"
					};
					ev.category ={'primaryCategory':ctConstants.custom};
					digitalData.event.push(ev);
					
                } else {
                    this._errorHandler(data);
                }
                this.triggerMethod('success', this);
                this.isSubmitted = false;
            },
            
             _errorHandler: function(err) {
                if(window.name === 'secure-frame-login') { 
                    if(err.status === 403 && err.responseJSON){
                         var data = err.responseJSON,
                            errorCode = data.ErrorCode, //'error invalid user', 
                            $submit= this.$form.find('button[type="submit"]'),
                            msg;
                            if(errorCode === "InvalidInput"){
                                    msg = this.$form.find('[name="Credentials-Password"]').data('form-invalidinput-msg');
                                } else if(errorCode === "ItemNotFound"){
                                    msg = this.$form.find('[name="Credentials-Password"]').data('form-itemnotfound-msg');
                                } else {
                                    msg = data.ErrorMessage;
                                }
                             this.$form.find('.error-msg-text').remove();
                            $submit.parents('.form-group').addClass('has-error-block');
                            var $span = $("<span />").addClass('error-msg-text help-block-error').text(msg);
                            $submit.parent().prepend($span);
                    }
                    this.triggerMethod('error', this.form.failureMessage);
                    this.isSubmitted = false;
                    return;
                }

                if (typeof this.form.errorRedirectURL !== 'undefined' && this.form.errorRedirectURL !== '' && this.form.errorRedirectURL !== false) {
                    // if the router is running, then make sure its using hash url change
                    // otherwise change the complete URL
                    if (IEA.History.started) {
                        Router.navigate(this.form.errorRedirectURL, {
                            trigger: true
                        });
                    } else {
                        window.location.href = this.form.errorRedirectURL;
                    }
                } else {

                    if($('.form').find('.default-text').length == 0){
                         if(err.code != 867){
                              this.$errorBlock = $('<div class="form-error-msg text-danger default-text"><span class="glyphicon glyphicon-warning-sign"></span><span class="error-msg-text">' + this.form.failureMessage + '</span></div>');
                              this.$form.before(this.$errorBlock);
                         }                       
                    } else {
                        this.$errorBlock.show();
                    }
                                       
                    if(err.errors !== undefined){
                        for(var i=0; i<err.errors.length; i++){
                             this.$errorBlock = $('<div class="form-error-msg text-danger"><span class="glyphicon glyphicon-warning-sign"></span><span class="error-msg-text">' + err.errors[i].error + '</span></div>');
                             this.$form.before(this.$errorBlock);    
                        }
                    }
                }

                $('html, body').animate({
                    scrollTop : $('form').closest('.form').find('.form-error-msg').offset().top - 150
                }, 800);

                this.triggerMethod('error', this.form.failureMessage);
                this.isSubmitted = false;

                try{
                    grecaptcha.reset();
                }
                catch(e){
                   // console.log("error");
                }
            },

            requiredMethod: function(attrName, validateFlag) {
                var moduleName = this.moduleName.hyphenToCamelCase(),
                    modelData = this.getModelJSON()[moduleName],
                    allElementsConfig = modelData.formElementsConfig,
                    idx = 0, jdx = 0,
                    item, $elem, elementRule = {},
                    newRule = {},
                    validate = validateFlag;

                // Getting all the multivalue field together
                for (idx=0; idx < allElementsConfig.length; idx++) {
                    if(allElementsConfig[idx] !== null){
                        item = allElementsConfig[idx].formElement;
                        if(typeof item !== 'undefined'){
                            if (typeof item.rules !== 'undefined' && _.isArray(item.rules)) {
                                if (item.name === attrName) {                        
                                    elementRule = item.rules[jdx];
                                }
                            }
                        }
                    }       
                    if(item) {
                        // adding rule to the first elemnet of multivalue
                        this.multiValueFields[item.name] = {
                            'name': attrName,
                            rules: {
                                "required": validateFlag,
                                "msg": elementRule.msg
                            }
                        };
                    }
                }
                if(typeof this.multiValueFields[attrName] !== 'undefined'){
                    newRule[attrName] = this.multiValueFields[attrName].rules;

                    this.validator.addRules(this, {
                        rules: newRule
                    });
                }   
            },

             /* ----------------------------------------------------------------------------- *\
               Private Methods
            \* ----------------------------------------------------------------------------- */

            /**
             * create validation rules from the model data and then create a the validation rules which
             * then is set to the form validation engine
             * @method _createValidationRules
             * @return validationRules
             */
            _createValidationRules: function() {
                var self = this,
                    moduleName = this.moduleName.hyphenToCamelCase(),
                    formElements = this.getModelJSON()[moduleName].formElementsConfig,
                    element, elementRule = {},
                    validationRules = {},
                    idx = 0,
                    jdx = 0,
                    pattern,
                    XRegExp = require('xregexp');
                
                for(idx = 0; idx<formElements.length; idx++) {
                    element = formElements[idx];
                    if (element !== null) {
                        if (!_.isFunction(element.formElement) && element.formElement) {
                            validationRules[element.formElement.name] = [];
                            if (typeof element.formElement.rules !== 'undefined' && _.isArray(element.formElement.rules)) {

                                for (jdx = 0; jdx<element.formElement.rules.length; jdx++) {
                                    self.isvalidationRequired = true;
                                    elementRule = element.formElement.rules[jdx];
                                    validationRules[element.formElement.name].push({
                                        xss: true
                                    });
                                    if (_.has(elementRule, 'pattern')) {
                                        if(elementRule.pattern === 'creditcard') {
                                            validationRules[element.formElement.name].push({
                                                creditcard: true,
                                                msg: elementRule.msg
                                            });
                                        } else {
                                            pattern = decodeURIComponent(elementRule.pattern).replace(/%24/g, '$');
                                            validationRules[element.formElement.name].push({
                                                pattern: this.validator.patterns[pattern] || new XRegExp(pattern),
                                                msg: elementRule.msg
                                            });
                                        }
                                        
                                    } else {
                                        validationRules[element.formElement.name].push(elementRule);
                                    }
                                }

                            }
                        } else if (!_.isFunction(element.formcheckbox) && element.formcheckbox) {
                            validationRules[element.formcheckbox.name] = [];
                            if (element.formcheckbox.rules[0].required === true && typeof element.formcheckbox.rules !== 'undefined' && _.isArray(element.formcheckbox.rules)) {

                                for (jdx = 0; jdx<element.formcheckbox.rules.length; jdx++) {
                                    self.isvalidationRequired = true;
                                    elementRule = element.formcheckbox.rules[jdx];
                                    validationRules[element.formcheckbox.name].push({
                                        xss: true
                                    });                                   
                                    validationRules[element.formcheckbox.name].push(elementRule);
                                }

                            }
                        }                       
                    } 
                }
                return validationRules;
            },
            
            /**
             * handle submit
             * @method _handleFormSubmit
             * @param {} evt
             * @return
             */
            _handleFormSubmit: function(evt, regTabFlag) {   
                // regTabFlag is passed false to prevent form submission on next click on registration page.
                regTabFlag = (typeof regTabFlag === 'undefined') ? true : regTabFlag;
                
                if($('body').hasClass('profile-page') || $('body').hasClass('registration-page')) {
                    var checkBoxElem = $('.js-make-mob-req-content').find(':checkbox'),
                        mobNoElem = this.$form.find('input[name*="SmsOptIn-MobileNumber"]'),
                        smsOptinsElem = this.$form.find('[name*="SmsOptIn"]'),
                        profileSmsOptin = this.$form.find('input[type="checkbox"][name^="MarketingPreferences"][name$="SmsOptIn-OptIn"]');
                    
                    if(mobNoElem.length > 0) {
                        if(checkBoxElem.is(':checked')){
                            this.$form.find('input[name*="SmsOptIn-MobileNumber"]').addClass('mob-no-req');
                            smsOptinsElem.removeAttr('data-exclude');
                        } else {                           
                                this.$form.find('input[name*="SmsOptIn-MobileNumber"]').removeClass('mob-no-req');
                                this.$form.find('.mob-error-msg-text').remove();
                                smsOptinsElem.attr('data-exclude','true');   
                                mobNoElem.parents('.form-group').removeClass('has-error');
                                mobNoElem.next('.help-block').addClass('hidden');                           
                        }                       
                    }
                    if(this.$form.find('.has-error')) {
                        this.$form.prev('.form-error-msg').remove();
                    }

                }

                var formData = IEA.serializeFormObject(this.$form),
                    formDatatoSend = formService.formToJson(this.$form),
                    submitxhr, idx =0, key, item,
                    formText = JSON.stringify(formData);
					
                evt.preventDefault();

                this.model.unset(this.changedSet);
                this.model.set(formData);
                this.changedSet = this.model.changed;
                
                // form image file upload required field validation
                this.fielUploadRequired = (function() {
                    var inputFileSel = $('.c-form-file'),
                        reqMsg = inputFileSel.attr('data-primary-message'),
                        isreq = inputFileSel.attr('data-file-required'),
                        $formGrp = inputFileSel.parents('.form-group');

                    if (inputFileSel.val() !== "" && !$formGrp.hasClass('file-error')) {
                        $formGrp.removeClass('file-error');
                        inputFileSel.next('.file-help-block').addClass('hidden');
                        return true;
                    } else {
                        $formGrp.addClass('file-error');
                        inputFileSel.next('.file-help-block').removeClass('hidden');
                        return false;
                    }
                })();

                // Profile page password match validation
                this.confirmPassword = (function() {
                    if($('body').hasClass('profile-page') || $('body').hasClass('reset-password-page')) {
                        var passwordElem = $('[name="NewCredentials-Password"]'),
                            confirmPasswordElem = $('[name="NewCredentials-confirmPassword"]'),
                            msg = $('[name="profile-confirm-password-msg"]').val();

                        if (passwordElem.val() !== "" && confirmPasswordElem.val() !== "" && passwordElem.val() !== confirmPasswordElem.val()) {
                            $('.pwd-error-msg-text').remove();
                            confirmPasswordElem.parents('.form-group').addClass('has-error pwd-has-error-block');
                            var $span = $("<span />").addClass('pwd-error-msg-text help-block-error').text(msg);
                            confirmPasswordElem.parent().append($span); 
                            return false;
                        } else {
                            $('.pwd-error-msg-text').hide();
                            confirmPasswordElem.parents('.form-group').removeClass('has-error pwd-has-error-block');
                            return true;
                        }
                    } else {
                        return true;
                    }
                })();
               
                //Profile page old new password mismatch validation
                this.passwordMismatch = (function(self) {
                    if($('body').hasClass('profile-page') && self.$form.find('#profileEmailPassword-submit').length > 0) {
                        var oldPasswordElem = self.$form.find('[name="OldCredentials-Password"]'),
                            passwordElem = self.$form.find('[name="NewCredentials-Password"]'),
                            msg = self.$form.find('[name="NewCredentials-Password"]').data('form-oldnewpasswordmismatch-msg');

                        if (oldPasswordElem.val() !== "" && passwordElem.val() !== "" && oldPasswordElem.val() === passwordElem.val()) {
                            $('.pwd-error-msg-text1').remove();
                            passwordElem.parents('.form-group').addClass('has-error pwd-mismatch-has-error-block');
                            var $span = $("<span />").addClass('pwd-error-msg-text1 help-block-error').text(msg);
                            passwordElem.parent().append($span); 
                            return false;
                        } else {
                            $('.pwd-error-msg-text1').hide();
                            passwordElem.parents('.form-group').removeClass('has-error pwd-mismatch-has-error-block');
                            return true;
                        }
                    } else {
                        return true;
                    }
                })(this);
                
                this.mobileNoRequired = (function(self) {
                    if($('body').hasClass('profile-page') || $('body').hasClass('registration-page')) {
                        var mobNoElem = self.$form.find('input[name*="SmsOptIn-MobileNumber"]'),
                            msg = mobNoElem.data('form-mobnoreq-msg');
                        if(mobNoElem.length > 0 && mobNoElem.hasClass('mob-no-req')) {
                            if(mobNoElem.val() === '') {
                                $('.mob-error-msg-text').remove();
                                mobNoElem.parents('.form-group').addClass('has-error mob-has-error-block');
                                var $span = $("<span />").addClass('mob-error-msg-text help-block-error').text(msg);
                                mobNoElem.parent().append($span);
                                return false;
                            } else {
                                $('.mob-error-msg-text').hide();
                                mobNoElem.parents('.form-group').removeClass('has-error mob-has-error-block');
                                return true;
                            }
                        } else {
							return true;
                        }
                    } else {
                        return true;
                    }
                })(this);
            
                // Check if the model is valid before saving
                // See: http://thedersen.com/projects/backbone-validation/#methods/isvalid
                if ((!this.isvalidationRequired || (this.isvalidationRequired && this.model.isValid(true))) && this.fielUploadRequired && this.confirmPassword && this.passwordMismatch && this.mobileNoRequired) {
                    this.isFormValid = true;
                } else {
                    this.isFormValid = false;
                    $('html, body').animate({
                        scrollTop : $('form').find('.has-error, .file-error, .help-block-error').offset().top - 150
                    }, 800);
                    
                    if($('body').hasClass('registration-page')) {
                        formService.registrationHelpers().setFormHeight();
                    }
                }

                for(idx in this.multiValueFields) {
                    var multiValueArray = [],
                        match, elem;
                    item = this.multiValueFields[idx];

                    // Get the value from input field and put it back to its hidden field
                    for(key in formData) {
                        match = key.match(item.name);
                        if(match && match.length > 0) {
                            elem = $('input[name='+key+']');
                            elem.siblings('input[type=hidden]').val(elem.val());
                        }
                    }

                    formData[item] = multiValueArray;
                }

                if(this.$errorBlock) {
                    this.$errorBlock.hide();
                }

                if(this.isFormValid && this.$form.data('customSubmitHandler') == true){
                    if (typeof this.customSubmitHandler == 'function'){
                        this.customSubmitHandler();
                    } else {
                        var submitBtn = this.$form.find('button[type=submit]');
                        formService.registrationHelpers().moveNext();
                    }
                    return;  
                } 
                if (this.isFormValid && this.form.postURL !== '' && this.formMethod ==='POST' && this.isSubmitted === false) {

                    if (this.form.isAjax) {

                        var submitxhr = $.ajax({
                          type: "POST",
                          url: this.form.postURL,
                          contentType: "application/json",
                          dataType: "json",
                          data: formDatatoSend
                        });

                        submitxhr.done($.proxy(this._successHandler, this));
                        submitxhr.fail($.proxy(this._errorHandler, this));

                    } else {
                        this.$form[0].submit();
                    }
                    this.triggerMethod('formSubmit', this);
                    this.isSubmitted = true;
                }
				
            },

            onSuccess: function() {

            },

            onError: function(msg) {

            }

        });
    });

    return Form;
});