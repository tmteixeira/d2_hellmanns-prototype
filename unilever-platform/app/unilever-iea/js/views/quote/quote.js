/*global define*/

define(['socialSharing'],function() {
    'use strict';

    var quote = IEA.module('UI.quote', function(quote, app, iea) {

        var socialService = null;
        _.extend(quote, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {

                var options = {};
                options.socialSharingMenu = $('.c-social-sharing-stack');
                options.socialSharing = $('.js-social-sharing');

                socialService = new IEA.socialsharing(); // creates new instance
                socialService.enableSocial(options);

                this.triggerMethod('enable'); 

            }

        });
    });

    return quote;
});
