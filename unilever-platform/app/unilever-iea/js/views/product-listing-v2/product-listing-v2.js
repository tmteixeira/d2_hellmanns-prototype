/*global define*/

define(['TweenMax', 'jquery.hammer', 'commonService', 'quickPanelService', 'ratings', 'binWidgetService', 'accordion', 'postmessage', 'iframetracker'], function() {
    'use strict';

    var ProductListingV2 = IEA.module('UI.product-listing-v2', function (productListingV2, app, iea) {

        var commonService = null,
            ratingService = null,
            quickPanelService = null,
            binWidgetService = null;

        _.extend(productListingV2, {

            defaultSettings: {
                searchWrapper: '.c-product-listing-v2__wrapper',
                filterBlock: '.c-product-listing-v2-filter__wrapper',
                filterWrapper: '.c-product-listing-filter__filter',
                template: 'partials/product-listing-v2-product.hbss',
                paginationTemplate: 'pagination/defaultView.hbss',
                variantProductTemplate: 'product-listing-v2/partial/variant-product.hbss',
                binWidgetTemplate: 'partials/bin-widget-listing.hbss',
                filterTemplate: 'product-listing-v2/partial/filters.hbss',
                productListingList: '.c-product-listing-v2__list',
                listingItem: '.js-product-listing-v2__list-item',
                filterCount: '.js-filter-count',
                loadmoreWrapper: '.c-product-listing-v2__loadmore',
                loadmoreBtn: '.js-btn-loadmore',
                ctaAnchor: '.c-product-listing-v2-listing-item-cta__anchor',
                isLoadMore: false,
                isLoadMoreClicked: false,
                isPageLoad: true,
                preloader: '.c-product-listing-v2__loadmore .o-preloader',
                preloaderContent: '.o-preloader--content',
                quickPanelWrapper: '.c-product-listing-v2-quickview',
                quickPanelCta: '.c-product-listing-v2__quick-cta',
                quickPanelNav: '.js-product-listing-v2__arrow',
                startPageNum: 1,
                pageNum: 1,
                startIndex: 1,
                pagination: '.c-product-listing-v2-pagination',
                paginationBtn: '.js-btn-pagination',
                variantDropdown: '.js-variant-dropdown-select',
                binButtonCTA: '.js-bin-btn',
                binOvarlay: '.bin-widget-overlay',
                binWidgetWrapper: '.js-product-listing-v2-quickview__bin',
                itemMargin: 30
            },

            events: {
                'change .c-product-listing-filter__filter' : '_filterChangeHandler', //update params and render search results on dropdown change
                'click .js-btn-loadmore' : '_loadMorehandler', // render results on show all button click
                'click .js-btn-pagination' : '_paginationHandler',
                'click .c-product-listing-v2__quick-cta': '_showQuickPanel',
                'click .js-btn-product-listing-v2-panel-close' : '_closePanel', //hide quick panel
                'click .js-product-listing-v2__arrow' : '_navigateToItem', // navigate quick panel through next/prev button
                'click .js-btn-store-search' : '_findStores', // submit form
                'click .js-find-location' : '_findLocation', // find locations
                'click .js-store-search-pdp-header': '_toggleStoreSearch', //toggle store search
                'change .js-variant-dropdown-select': '_updatePanelOnDropdownChange'
            },
            
            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
                commonService = new IEA.commonService(); // creates new instance for common service
                ratingService = new IEA.ratings(); // creates new instance for ratings
                quickPanelService = new IEA.QuickPanel(); // creates new instance for Social tab service
                binWidgetService = new IEA.BinWidget(); // creates new instance for bin widget service
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                this.componentJson = this.getModelJSON().productListingV2;
                this.pageNum = this.defaultSettings.pageNum;
                this.pageScroll = false;
                this.pagination = this.$el.find(this.defaultSettings.pagination);
                this.loadmoreWrapper = this.$el.find(this.defaultSettings.loadmoreWrapper);
                this.isPageLoad = this.defaultSettings.isPageLoad;
                this.preloaderContent = this.$el.find(this.defaultSettings.preloaderContent);
                this.productList = this.$el.find(this.defaultSettings.productListingList);
                this.startIndex = this.defaultSettings.startIndex;
                this.listingItem = this.defaultSettings.listingItem;
                this.quickPanelWrapper = this.defaultSettings.quickPanelWrapper;
                this.selectedFiltersIndex = 0;
                this.activeState = false;

                var options = {},
                    binOptions = {};

                // intiate ratings
                if (this.componentJson.products.productList.length && this.componentJson.products.productList[0].ratings) {
                    options = {
                        ratingReview: this.componentJson.products.review,
                        productId: this.componentJson.products.productList,
                        brandName: this.componentJson.brandName,
                        ratingObj: this.componentJson.products.productList[0].ratings
                    };
                    
                    ratingService.initRatings(options);
                }

                //reinitiate lazy load method
                this._initiateLazyLoad();

                // show/hide load more on page scroll
                this._handleLoadMore();

                // render pagination
                this._renderPagination(this.componentJson.totalCount);

                if (this.componentJson.products.productList.length === 0){
                    self.$el.hide();
                    return;
                }

                //bin widget
                this._binWidgetInit();

                this.triggerMethod('enable');
            },

            /**************************PRIVATE METHODS******************************************/   

            _getServiceQueryParams: function () {
                var self = this,
                    serviceQueryParams = '',
                    filterParams = '',
                    pageNumParam;  
                
                $(self.defaultSettings.filterWrapper).each(function(key, val) {
                    var $this = $(this),
                        filterType = $this.data('filter-type'),
                        selectedVal = '';

                    switch (filterType) {
                        case 'dropdown':
                            selectedVal = $this.find('option:selected').val();
                            break;

                        case 'radioButton':
                            selectedVal = ($this.is(':checked')) ? $this.val() : '';
                            break;

                        case 'checkBox':
                            selectedVal = ($this.is(':checked')) ? $this.val() : '';
                            break;
                    }

                    if (key === self.selectedFiltersIndex && !self.activeState) {
                        selectedVal = '';
                    }
                    
                    filterParams += self.componentJson.filterConfig.filterParamName + '=' + selectedVal + '&';
                });
               
                pageNumParam = self.componentJson.filterConfig.pageNoParamName + '=' + self.pageNum;

                serviceQueryParams += '?' + filterParams + pageNumParam;

                return serviceQueryParams;
            },

            /**
            @description: update data on filter change
            @method: _filterChangeHandler
            @return: {null}
            **/

            _filterChangeHandler: function(evt) {
                var self = this,
                    $this = $(evt.currentTarget),
                    filterBlockClass = 'is-default-val';

                //self.$el.find(this.defaultSettings.filterBlock).addClass(filterBlockClass);
                if ($this.hasClass('active-dropdown')) {
                    self.activeState = false;
                }

                if (!self.activeState) {
                    $this.parents('.c-product-listing-v2-filter__container').find(this.defaultSettings.filterBlock).removeClass(filterBlockClass);

                    $this.addClass('active-dropdown');
                    self.selectedFiltersIndex = self.$el.find(this.defaultSettings.filterBlock+'.'+filterBlockClass).data('index');
                }

                // Start from the beginning again
                self.pageNum = self.defaultSettings.startPageNum;
                self.isPageLoad = true;

                $(self.preloaderContent).removeClass('hidden').show();
                
                if (self.defaultSettings.isLoadMoreClicked) {
                    self.defaultSettings.isLoadMore = false;
                }
                
                self._populateData();
            },

           /**
            @description: Backbone.js Bind Callback to Successful Model Fetch
            @method _searchView
            @return {null}
            **/

            _populateData: function() {
                /* Backbone.js Bind Callback to Successful Model Fetch */
                var self = this,
                    options = {},
                    isLocalEnv = $('.local-env').length; //this is for FE local build only.
                               
                if (isLocalEnv) {
                    options.queryParams = '';
                } else {
                    options.queryParams = self._getServiceQueryParams();    
                } 

                options.servletUrl = self.componentJson.filterConfig.ajaxURL;

                options.onCompleteCallback = function(response) {
                    if (response) {
                        var parsedJson = $.parseJSON(response);
                        self._updateProductListing(parsedJson.data, parsedJson.data.productListingV2.totalCount);
                    }                   
                };

                commonService.getDataAjaxMethod(options);
            },

            /**
            @description: handle load more click event
            @method: _updateProductListing
            @return: {null}
            **/

            _updateProductListing: function (data, totalCount) {
                var self = this,
                    settings = self.defaultSettings,
                    listing = '',
                    template = self.getTemplate('product-listing-v2-product', settings.template),
                    filterTemplate = self.getTemplate('product-listing-v2-filter', settings.filterTemplate),
                    filterJson = data.productListingV2.filterConfig.filterOptions[self.selectedFiltersIndex],
                    dataJson = {
                        productListingV2: data.productListingV2,
                        pageNum: (self.pageNum - 1)*self.componentJson.pagination.itemsPerPage
                    },
                    filterToUpdate = settings.filterBlock+'[data-index="'+self.selectedFiltersIndex+'"]',
                    filterContainer = '.c-product-listing-v2-filter__container';

                // update result count 
                self.$el.find(settings.filterCount).text(totalCount);

                //hide preloader
                $(self.preloaderContent).addClass('hidden');
                $(settings.preloader).addClass('hidden');

                //update filters               
                if (!self.activeState) {
                    if (filterJson && $(filterJson.filters).length) {
                        self.$el.find(filterToUpdate).html(filterTemplate(filterJson))
                        .parents(filterContainer).fadeIn();
                    } else {
                        self.$el.find(filterToUpdate).parents(filterContainer).fadeOut();
                    }
                    
                    self.activeState = true;
                }                

                // render listing                   
                listing = template(dataJson);

                if (self.pageNum > 1 && self.componentJson.pagination.paginationType === 'infiniteScroll') {
                    self.componentJson.products.productList = self.componentJson.products.productList.concat(data.productListingV2.products.productList);
                    $(self.productList).append(listing);
                } else {
                   $(self.productList).html(listing);
                }              

                // render pagination
                self._renderPagination(totalCount);

                // load more params
                self._updateParamsOnPageScroll(totalCount); 

                if (totalCount > self.componentJson.pagination.itemsPerPage) {
                       $(self.pagination).removeClass('hidden');
                       $(self.loadmoreWrapper).removeClass('hidden');
                       if (self.pageNum === 1) {
                            self.$el.find(settings.loadmoreBtn).fadeIn();
                       }
                       
                } else {
                    $(self.pagination).addClass('hidden');
                    $(self.loadmoreWrapper).addClass('hidden');
                }

                // Init rating 
                if (typeof ratingReview !== 'undefined' && typeof ratingReview.widget !== 'undefined') {
                    ratingService._kritiqueRatings(ratingReview);
                } 

                //reinitiate lazy load method
                self._initiateLazyLoad();                  
            },

            //render pagination
            _renderPagination: function(totalCount) {
                var self = this,
                    paginationTemplate = self.getTemplate('pagination', self.defaultSettings.paginationTemplate),
                    combinedJson = null,
                    totalPages = Math.ceil(totalCount/self.componentJson.pagination.itemsPerPage),
                    paginationList = [];

                for (var i=0; i < totalPages; i++) {
                    paginationList.push(i);
                }

                combinedJson = {
                    'componentJson': self.componentJson,
                    'pageNum': self.defaultSettings.pageNum,
                    'totalPages': totalPages,
                    'paginate': paginationList 
                };

                // render pagination
                if (self.isPageLoad) {
                    $(self.pagination).html(paginationTemplate(combinedJson));
                }
            },

            // trigger lazy load method
            _initiateLazyLoad: function() {
                //reinitiate lazy load method
                commonService.imageLazyLoadOnPageScroll(); // Init lazy load
                var throttledLazyLoad = _.throttle(commonService.imageLazyLoadOnPageScroll, 150);
                $(window).on('scroll.lazyLoad', throttledLazyLoad); // Window scroll lazy load 
            },

            /**
            @description: handle load more click event
            @method: _updateParamsOnPageScroll
            @return: {null}
            **/

            _updateParamsOnPageScroll: function (totalCount) {
                var self = this,
                    itemsPerPage = self.componentJson.filterConfig.itemsPerPage;

                self.pageScroll = (totalCount > itemsPerPage * self.pageNum);
            },

            /**
            @description: handle load more click event
            @method: _loadMorehandler
            @return: {null}
            **/

            _loadMorehandler: function(e) {
                var self = this,
                    $this = $(e.currentTarget),
                    settings = self.defaultSettings;

                e.preventDefault();

                self.pageNum++;

                $this.fadeOut(500);
                $(settings.preloader).removeClass('hidden');
                settings.isLoadMore = true;

                // render search listing
                self._populateData();
                
                settings.isLoadMoreClicked = true;
            },

            /**
             * _paginationHandler
             * @method _paginationHandler
             * @return 
            */
            _paginationHandler: function(evt) {
                var self = this,
                    $this = $(evt.currentTarget),
                    panelOffset;

                 if ($this.hasClass('o-btn--disabled') || $this.hasClass('o-btn--active')) {
                    evt.preventDefault();
                    return false;
                }

                $(self.preloaderContent).height($(self.productList).height()).removeClass('hidden').show();
                TweenMax.to(window, 0.5, {scrollTo: {y: $(self.$el).offset().top}});

                self.startIndex = self.startIndex + self.componentJson.pagination.itemsPerPage;
                self.isPageLoad = false;
                
                //pagination handler common service
                commonService.pagination(self.defaultSettings.paginationBtn, $this, self);

                // render search listing
                self._populateData(); 
            },

            /**
            @description: handle load more click event
            @method: _handleLoadMore
            @return: {null}
            **/
            _handleLoadMore: function() {               
                var self = this,
                    settings = self.defaultSettings;

                document.addEventListener('scroll', function (event) {
                    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop,
                        scrollOffset = scrollTop + window.innerHeight + $('.c-footer-container').height() - 50;

                    if ((document.body.scrollHeight <= scrollOffset) && (self.pageNum >= 2) && self.pageScroll === true) {
                        self.pageScroll = false;
                        self.$el.find(settings.loadmoreBtn).click();
                    }
                });
            },

            /**
             * show/hide quick panel
             * @method _showQuickPanel
             * @return 
            */
            _showQuickPanel: function(evt) {
                evt.preventDefault();

                var $this = $(evt.currentTarget),
                    self = this,
                    active = 'active',
                    $currentItem = $this.parents(self.listingItem),
                    currentIndex = $currentItem.data('index'),
                    $currentQuickPanel = self.$el.find(self.quickPanelWrapper+'[data-index='+currentIndex+']'),
                    options = {};

                options = {
                    nextButton: $(self.defaultSettings.quickPanelNav+'-next'),
                    prevButton: $(self.defaultSettings.quickPanelNav+'-prev'),
                    animateSpeed: 1000,
                    quickPanelWrapper: self.quickPanelWrapper,
                    listingItem: self.listingItem,
                    itemMargin: self.defaultSettings.itemMargin,
                    quickPanelCta: self.defaultSettings.quickPanelCta
                };
                
                quickPanelService.setActiveParams(options, parseInt(currentIndex));

                $currentItem.addClass(active);
                               
                self.$el.find(self.listingItem).each(function (index, el) {
                    var panelHeight = 0,
                        cols = 3,
                        row = currentIndex - currentIndex % cols;

                    if (index >= row && index <= row + (cols - 1)) {
                        panelHeight = $currentQuickPanel.height();
                    }
                    if (panelHeight !== 0) {
                        $(el).css("margin-bottom", panelHeight+ options.itemMargin*2);
                    }                   
                });
                
                $currentQuickPanel.addClass(active).css('top', $currentItem.position().top + $currentItem.height() + options.itemMargin);
                   

                TweenMax.to(window, 0.5, {scrollTo: {y: $currentItem.offset().top + $currentItem.height()/2}});

                self.defaultSettings.disableHotKey = false;

                //keyword keys handler
                self._navigateThroughKeys($currentItem, currentIndex, self.$el.find(self.listingItem).length);
            },

            /**
             * open quick panel on clicking on current item 
             * @method _navigateToItem
             * @return 
            */           
            _navigateToItem: function(evt) {
                evt.preventDefault();

                var $this = $(evt.currentTarget),
                    self = this,                  
                    active = 'active',
                    $currentItem = $this.parents(self.quickPanelWrapper),
                    currentIndex = $currentItem.data('index');

                if ($this.data('role') === 'next') {
                    currentIndex = currentIndex + 1;
                } else if ($this.data('role') === 'prev') {
                    currentIndex = currentIndex - 1;
                }
                self.$el.find(self.listingItem+'[data-index='+currentIndex+'] '+self.defaultSettings.quickPanelCta).click();
            },
            
            /**
             * close quick panel
             * @method _closePanel
             * @return 
            */
            _closePanel: function(evt) {
                evt.preventDefault();                
                
                var self = this,
                    $this = $(evt.currentTarget),
                    options = {};

                options = {
                    nextButton: self.$el.find(self.defaultSettings.quickPanelNav+'-next'),
                    prevButton: self.$el.find(self.defaultSettings.quickPanelNav+'-prev'),
                    animateSpeed: 1000,
                    quickPanelWrapper: $this.parents(self.quickPanelWrapper),
                    listingItem: self.listingItem,
                    itemMargin: self.defaultSettings.itemMargin,
                    quickPanelCta: self.defaultSettings.quickPanelCta
                };

                quickPanelService.closePanel(options);
            },

            /**
             * navigate quick panel through next/previous button
             * @method _closePanel
             * @return 
            */
            _navigateThroughKeys: function(item, index, last) {
                var self = this,
                    settings = self.defaultSettings,
                    options = {};

                options = {
                    nextButton: self.$el.find(self.defaultSettings.quickPanelNav+'-next'),
                    prevButton: self.$el.find(self.defaultSettings.quickPanelNav+'-prev'),
                    animateSpeed: 1000,
                    quickPanelWrapper: self.quickPanelWrapper,
                    listingItem: self.listingItem,
                    itemMargin: self.defaultSettings.itemMargin,
                    quickPanelCta: self.defaultSettings.quickPanelCta
                };
                
                // Hotkeys for navigation
                $(document).off('keyup').on('keyup', function(e) {
                    if (e.keyCode === 37 && !settings.disableHotKey && index > 0) {
                         quickPanelService.prevItem(options, index);
                    }
                    if (e.keyCode === 39 && !settings.disableHotKey && index < last) {                    
                        quickPanelService.nextItem(options, index);
                    }
                    if (e.keyCode === 27) {
                        TweenMax.to(window, 0.5, {scrollTo: {y: item.offset().top - item.height()/2}});
                    }
                });

                // Touch gestures with hammerjs for navigation
                self.$el.hammer().off('swipeleft').on('swipeleft', function(e) {                   
                    if (!settings.disableHotKey) {
                        quickPanelService.prevItem(options, index);
                    }
                }).off('swiperight').on('swiperight', function(e) {                 
                    if (!settings.disableHotKey) {
                        quickPanelService.nextItem(options, index);
                    }
                });
            },

            /**
            @bin widget init
            @method _binWidgetInit
            @return
            **/

            _binWidgetInit: function() {
                //intiate Bin Widget
                var options = {
                    shopNow: this.componentJson.products.shopNow,
                    modalCl: '.js-modal-etale',
                    view: this
                };

                if (options.shopNow) {
                    binWidgetService.injectBinJS(options);
                    this._binWidgetHandler(options);
                    $('.icon-cross').attr('brand', this.componentJson.brandName);

                    if (options.shopNow.serviceProviderName === 'etale') {
                        this._sendParametersToEtale();
                    }

                    // track constant commerce BIN button click
                    if (options.shopNow.serviceProviderName === 'constantcommerce') {
                        this._trackIframeMethod();
                    }                   
                }               
            },

            /**
            @bin widget handler
            @method _binWidgetHandler
            @return 
            **/

            _binWidgetHandler: function(options) {
                var self = this,
                    productId,
                    $variantDropdown = self.$el.find(self.defaultSettings.variantDropdown),
                    productData = self.componentJson.products,
                    selectedIndex = self.$el.find(self.defaultSettings.variantDropdown+' option:selected').index();

                $(self.defaultSettings.binButtonCTA).click(function(evt) {
                    evt.preventDefault();
                    var $this = $(this),
                        productListIndex = parseInt($this.parents(self.quickPanelWrapper).data('index'));

                   productId = $this.data('shopnow-productid');
                    
                    if (options.shopNow.serviceProviderName === 'cartwire') {
                        loadsWidget(options.productId, this,'retailPopup','en');
                    
                    } else if (options.shopNow.serviceProviderName === 'etale') {
                        $this.next(self.defaultSettings.binOvarlay).show();
                        self._etaleTracking($this);
                    }

                    // shop now tracking
                    self._trackShopNow($this);
                });
                
                $('.js-btn-close').click(function(evt) {
                    evt.preventDefault();
                    $(self.defaultSettings.binOvarlay).hide();
                });

            },
            /**
             * toggle store search wrapper on label click
             * @method _toggleStoreSearch
             * @return 
             */

            _toggleStoreSearch: function(evt) {
                var self = this,
                    $this = $(evt.currentTarget),
                    service = new IEA.accordion(), // creates new instance
                    options = {},
                    $currentQuickPanel = $this.parents(self.quickPanelWrapper),
                    currentIndex = $currentQuickPanel.data('index'),
                    $storeWrapper = $currentQuickPanel.find('.c-store-search-pdp-header__body');

                options.wrapper = this.defaultSettings.accordion;
                service._handleExpandCollapse(evt, options);
                
                // update listing margin
                self.$el.find(self.listingItem).each(function (index, el) {
                    var panelHeight = 0,
                        cols = 3,
                        row = currentIndex - currentIndex % cols;

                    if (index >= row && index <= row + (cols - 1)) {
                        panelHeight = $currentQuickPanel.height();
                    }
                    if (panelHeight !== 0) {
                        $(el).css("margin-bottom", panelHeight+ parseInt($storeWrapper.height()));
                    }                   
                });
            },

            /**
             * search zip code based on user location
             * @method _shareLoc
             * @return 
             */

            _findLocation: function() {             
                var self = this,
                    pos, reverseGeoCode;

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        }
                        reverseGeoCode(pos.lat, pos.lng, function(zip) {
                           self.$el.find('.active .input-zipcode').val(zip);
                        }); 
                    });
                    
                    reverseGeoCode = function(lat, lng, callback) {
                        var mapServiceUrl = '//maps.googleapis.com/maps/api/geocode/json?latlng=',
                            googleService = mapServiceUrl + lat + ',' + lng + '&sensor=false';
                        
                        $.getJSON(googleService, getMapData);

                        function getMapData(data) {
                            var addressComponents = data.results[0].address_components,
                                postalCode = '', 
                                countryCode = '', 
                                type = '';

                            if (addressComponents) {                                
                                _.each(addressComponents, function(item, index) { 
                                    if (typeof item.types == "string") {
                                        type = item.types;
                                    } else if (typeof item.types == "object") {
                                        type = item.types[0];
                                    }
                                    
                                    if (type === 'postal_code') {
                                        postalCode = item.long_name;
                                    }
                                    if (type === 'postal_town') {
                                        self.location = item.long_name;
                                    }
                                });
                            
                                if (typeof callback === 'function') {
                                    callback(postalCode);
                                }
                            }
                        }   
                    }
                }               
            },

            /**
             * submit buy in store form
             * @method _submitBuyStoreForm
             * @return 
             */

            _findStores: function(evt) {
                evt.preventDefault();

                var $this = $(evt.currentTarget),
                    $postalCode = $this.parents('form').find('.input-zipcode'),
                    isZipcodeValid = this._validateZipCode($postalCode.val(), $postalCode.data('regex')),                   
                    $formGroup = $this.parents('.form-group'),
                    $helpBlock = this.$el.find('.help-block');
                
                if (isZipcodeValid && $postalCode.val() !== '') {
                    $helpBlock.addClass('hidden');
                    $formGroup.removeClass('has-error');
                    $this.parents('form').submit();
                    // track store search analytics
                   this._ctStoreSearch($this);
                } else {
                    $formGroup.addClass('has-error');
                    $helpBlock.removeClass('hidden');
                    $postalCode.focus();
                    return false;
                }              
            },

            /**
             * Zip code validation based on regex provided
             * @method _validateZipCode
             * @return 
             */
            
            _validateZipCode: function(fieldVal, regExp) {
                var regEx = regExp, 
                    pattern = new RegExp(regEx), 
                    isValidZip = pattern.test(fieldVal);

                return isValidZip;
            },

            _ctStoreSearch: function(obj) {
                var ev = {},
                    $this = this.$el.find('.c-product-listing-v2'),
                    label = 'StoreSearchSubmitted < ' + $('#pc').val() + ' | ' + this.location;
            
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    this._trackComponentInfo();

                    this._trackProductInfo(obj);

                    ev.eventInfo = {
                      'type': ctConstants.trackEvent,
                      'eventAction': ctConstants.storesearch,
                      'eventLabel' : label
                    };
                    ev.category = {'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                }
            },

            /**
             * Select box price and unit update, used to update the label text
             * @method _updatePanelOnDropdownChange
             * @return
             */

            _updatePanelOnDropdownChange: function (evt) {
                evt.preventDefault();

                var self = this,
                    $this = $(evt.currentTarget),
                    $parentWrapper = $this.parents(self.quickPanelWrapper),
                    currentIndex = evt.currentTarget.selectedIndex,
                    listingIndex = parseInt($parentWrapper.data('index')),
                    currentData = self.componentJson.products.productList[listingIndex].productsDetail[currentIndex],
                    template = self.getTemplate('variant-product', self.defaultSettings.variantProductTemplate),
                    binWidgetTemplate = self.getTemplate('bin-widget-listing', self.defaultSettings.binWidgetTemplate),
                    $retail = $parentWrapper.find('.c-dropdown-label__retail'),
                    $unit = $parentWrapper.find('.c-dropdown-label__unit'),
                    $price = $parentWrapper.find('.c-dropdown-label__price');

                if (currentData) {
                    $price.html(self.componentJson.products.productList[listingIndex].currencySymbol + currentData.price);
                    $unit.html(currentData.unit);
                    
                    if (currentData.price) {
                        $retail.html(currentData.retail);
                    } else {
                        $retail.html("");   
                    }

                    //update copy text
                    $parentWrapper.find(this.quickPanelWrapper+'__title').text(currentData.perfectForTitle);
                    $parentWrapper.find(this.quickPanelWrapper+'__productname').text(currentData.name);
                    $parentWrapper.find(this.quickPanelWrapper+'__desc').html(currentData.shortPageDescription);

                    //update product id
                    $parentWrapper.find('#pvi').val(currentData.smartProductId);

                    //update product image template
                    $this.parents(this.quickPanelWrapper).find(this.quickPanelWrapper+'-item-image__img').html(template(currentData.images[0]));

                    //update bin widget
                    $parentWrapper.find(self.defaultSettings.binButtonCTA).attr('data-shopnow-productid', currentData.smartProductId);

                    //reinitiate lazy load method
                    this._initiateLazyLoad();
                }                
            },

            /************************************** ANALYTICS *************************************/
            
            // component attributes tracking
            _trackComponentInfo: function() {
                var $obj = this.$el.find('.c-product-listing-v2');

                digitalData.component = [];

                digitalData.component.push({
                    'componentInfo' :{
                        'name': $obj.data('componentname'),
                    },
                    'attributes': {
                        'position': $obj.data('component-positions'),
                        'variants': $obj.data('component-variants')
                    }
                });
            },

            // product info attributes tracking
            _trackProductInfo: function(obj) {
                var $obj = obj,
                    productID = $obj.data('product-id'),
                    productName = $obj.data('product-name'),
                    productCategory = $obj.data('product-category'),
                    productPrice = $obj.data('product-price'),
                    variant =  $obj.data('product-variant'),
                    brandName = $obj.data('product-brand'),
                    listPosition = parseInt($obj.data('index')) + 1;

                digitalData.product = [];

                digitalData.product.push({
                   'productInfo': {
                       'productID': productID,
                       'productName': productName,
                       'price': productPrice,
                       'brand': brandName,
                       'quantity': 1
                   },
                   'category': {
                        'primaryCategory': productCategory
                   },
                   'attributes': {
                        'productVariants': variant,
                        'listPosition': listPosition
                   }
                });
            },

            //track shop Now
            _trackShopNow: function(obj) {
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    var ev = {},
                        $obj = this.$el.find(this.quickPanelWrapper+'.active'),
                        productName = $obj.data('product-name');

                    // track component info
                    this._trackComponentInfo();
                    // track product info
                    this._trackProductInfo($obj);

                    ev.eventInfo = {
                      'type': ctConstants.trackEvent,
                      'eventAction': ctConstants.purchase,
                      'eventLabel': 'Online - ' + productName
                    };
                    ev.category = {'primaryCategory':ctConstants.conversion};
                    digitalData.event.push(ev);
                }
            },

            // track shopNow tracking using iframeTracker
            _trackIframeMethod: function(obj) {
                var self = this;
                $('.constantco-widget iframe').iframeTracker({
                    blurCallback: function() {
                        self._trackShopNow(obj);
                    },
                    overCallback: function(element) {
                        this._overId = $(element).attr('id'); // Saving the iframe wrapper id
                    },
                    outCallback: function(element) {
                        this._overId = null; // Reset hover iframe wrapper id
                    },
                    _overId: null
                });
            },

            // send parameters to etale
            _sendParametersToEtale: function() {
                // Get the parent page URL as it was passed in, for browsers that don't support
                // window.postMessage (this URL could be hard-coded).
                var parenturl = decodeURIComponent(document.location.hash.replace(/^#/,'')),
                    link;

                // The first param is serialized using $.param (if not a string) and passed to the
                // parent window. If window.postMessage exists, the param is passed using that,
                // otherwise it is passed in the location hash (that's why parent_url is required).
                // The second param is the targetOrigin.              
                function postMessage(model, partnum, prodid, proddesc, markettext, ean, upc, etin, retailer) {
                    var message = '{ "model" : "' + model +
                                    '", "partnum" : "' + partnum +
                                    '", "prodid" : "' + prodid +
                                    '", "proddesc" : "' + proddesc +
                                    '", "markettext" : "' + markettext +
                                    '", "ean" : "' + ean +
                                    '", "upc" : "' + upc +
                                    '", "etin" :"' + etin +
                                    '", "retailer" : " ' + retailer +
                                    '", "date": "' + new Date() + '"}';
                    $.postMessage(message, parenturl, parent);
                }
            },

            // etale tracking
            _etaleTracking: function(obj) {
                // shop now tracking 
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    this._trackProductInfo(obj);

                
                    $.receiveMessage(function (e) {
                        var json = JSON.parse(e.data),
                            retailer = json.proddesc + ' -' + json.retailer,
                            retailername = json.retailer;

                        if (typeof retailername !== 'undefined') {
                            var ev = {};
                            ev.eventInfo = {
                                'type': ctConstants.trackEvent,
                                'eventAction': ctConstants.retailerClick,
                                'eventLabel': 'Online - ' + prodName + ' | ' + retailername
                            };
                            ev.category = {'primaryCategory': ctConstants.custom};

                            digitalData.event.push(ev);
                            if (typeof $BV === 'object') {
                                $BV.SI.trackConversion({
                                    'type': 'BuyOnline',
                                    'label': retailername,
                                    'value': prodId
                                });
                            }
                        }
                    });
                }
            }
        });
    });
    return ProductListingV2;
});