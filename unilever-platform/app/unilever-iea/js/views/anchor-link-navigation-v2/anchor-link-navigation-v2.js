/*global define*/

define(['iscroll', 'TweenMax'], function() {
    'use strict';

    var AnchorLinkNavigationV2 = IEA.module('UI.anchor-link-navigation-v2', function(anchorLinkNavigationV2, app, iea) {

        var self = this,
            windowWidth = $(window).width(),
            mobileBreakpoint = 768,
            scrollFromAnchor = false,
            navScroll = null,
            $activeItem = null;

        _.extend(anchorLinkNavigationV2, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                // Create items based on data attributes
                var $items = $('[data-sticky-nav-id]'),
                    $navWrapper = $('.anchor-link-navigation-v2');

                // Only load the component if there are links to show
                if ($items.length > 0) {

                    $items.each(function(idx, item) {

                        var target = $(item).data('sticky-nav-id'),
                            label = $(this).data('sticky-nav-label');

                        // Add the items to the sticky navigation
                        $('.js-navigation-sticky').append('<li id="js-sticky-item-' + idx + '" class="c-navigation-sticky__item js-navigation-sticky__item"><span class="c-navigation-sticky__button" data-anim-scroll-target="' + target + '">' + label + '</span></li>');
                    });

                    self._initNav();
                }
                else {
                    $navWrapper.remove();
                }

                $(document).on("click",'.js-back-to-top-cta__link',function(){
                    $('.js-navigation-sticky__item').removeClass('js-navigation-sticky--active is--selected');
                });
                this.triggerMethod('enable');
            },

            /**************************PRIVATE METHODS******************************************/

            _initNav: function() {

                var $navBar = $('.o-navbar'),
                    $secNavBar = $('.c-secondary-nav'),
                    $navWrapper = $('.anchor-link-navigation-v2'),
                    navOffset = $navWrapper.offset().top,
                    navHeight = $navWrapper.outerHeight(),
                    $items = $('.js-navigation-sticky__item'),
                    $buttons = $('.c-navigation-sticky__button'),
                    $components = $('[data-sticky-nav-id]'),
                    $stickItem = $('#js-sticky-item-0'),
                    $mobAnchorTxt = $('.js-navigation-sticky__anchor-text'),
                    isSelected = 'is--selected',
                    $stickMobNav = $('.c-navigation-sticky-mobile'),
                    $stickNav = $('.js-navigation-sticky'),
                    $stickNavList = $('.js-navigation-sticky li'),
                    padding = 40;

                // Fade in on homepage
                TweenMax.to($navWrapper, 0.8, {autoAlpha: 1, top: 0, delay: 1.4});

                $items.addClass('cell');

                // Click events
                $buttons.on('click', function(e) {
                    e.preventDefault();
                    self._activateItem($(e.currentTarget), true);
                });

                $items.on('click', function() {
                    $items.removeClass(isSelected);
                    $(this).addClass(isSelected);
                });

                if (windowWidth < mobileBreakpoint) {
                    var textforMob = $stickItem.find('span').text();
                        $stickItem.hide();
                    $mobAnchorTxt.html(textforMob);

                    $stickMobNav.off('click').on('click', function(){
                        $(this).toggleClass('is-close');
                        $stickNav.toggleClass('hidden-xs');
                    });

                    $stickNavList.on('click', function() {
                        $mobAnchorTxt.html($(this).text());
                        $stickNavList.show();
                        $(this).hide();
                        $stickNav.addClass('hidden-xs');
                        $stickMobNav.toggleClass('is-close');
                    });
                }

                // Init iScroll
                navScroll = new IScroll('.js-navigation-sticky-wrapper', { click: true, probeType: 3, scrollX: true, scrollbars: false });

                // Start/stop sticky behaviour
                $(window).on('scroll', function() {
                    var scrollOffset = $(window).scrollTop(),
                        topPosition = 0,
                        curPos = $(this).scrollTop();

                    // Slide up/down the global nav and secondary nav
                    if(scrollOffset >= navOffset) {
                        if (!$('body').hasClass('o-navbar-hidden')) {
                            $('body').addClass('o-navbar-hidden o-sticky-nav-enabled');
                            $navWrapper.addClass('js-container-sticky');
                            TweenMax.to($secNavBar, 0.6, {top: (topPosition - $secNavBar.outerHeight()) + 'px'});
                            TweenMax.to($navBar, 0.6, {top: (topPosition - ($navBar.outerHeight() + $secNavBar.outerHeight())) + 'px'});
                        }
                    } else {
                        if ($('body').hasClass('o-navbar-hidden')) {
                            $('body').removeClass('o-navbar-hidden o-sticky-nav-enabled');
                            $navWrapper.removeClass('js-container-sticky');
                            TweenMax.to($secNavBar, 0.6, {top: topPosition + 'px'});
                            TweenMax.to($navBar, 0.6, {top: (topPosition + $secNavBar.outerHeight()) + 'px'});
                        }
                    }

                    // Check which component is currently into the viewport
                    var $latestItem = null;
                    $components.each(function() {
                        var $actualComponent = $(this).parent().parent().next();
                        var top = $actualComponent.offset().top - navHeight,
                            bottom = top + $actualComponent.outerHeight();
                        if (curPos >= top && curPos <= bottom) {
                            var id = $(this).data('sticky-nav-id');
                            $latestItem = $('[data-anim-scroll-target="' + id + '"]');
                        }
                    });

                    // Only update the nav if the item changed
                    if ($latestItem !== null && !$latestItem.is($activeItem)) {
                        $activeItem = $latestItem;
                        self._activateItem($activeItem, false);
                    }
                });

                $(window).scroll();
            },

            _activateItem: function(target, scroll) {
                if(target.length > 0) {

                    var $items = $('.js-navigation-sticky__item'),
                        $li = target.closest('li'),
                        $scrollTarget = $('[data-sticky-nav-id="' + target.data('anim-scroll-target') + '"]'),
                        $wrapper = $('.js-navigation-sticky-wrapper'),
                        $mobAnchorTxt = $('.js-navigation-sticky__anchor-text'),
                        $isSelected = $('.is--selected');

                    // We need to skip the global nav, filter nav and add a bit of margin
                    var topOffset = $wrapper.height() + 30;

                    $items.removeClass('js-navigation-sticky--active is--selected');
                    $li.addClass('js-navigation-sticky--active is--selected');

                    if (windowWidth < mobileBreakpoint) {
                        var textforMob = $isSelected.find('span').text();
                        $mobAnchorTxt.html(textforMob);
                        $items.show();
                        $items+$isSelected.hide();
                    }

                    // Scroll the page to the target element
                    if (scroll) {
                        scrollFromAnchor = true;
                        TweenMax.to(window, 2, {scrollTo: {y: $scrollTarget.offset().top - topOffset}, ease: Power4.easeOut, autoKill: true, onComplete: self._scrollFromAnchor});
                    }
                }
            },

            _scrollFromAnchor: function(elem) {
                scrollFromAnchor = false;
            }
        });
    });

    return AnchorLinkNavigationV2;
});