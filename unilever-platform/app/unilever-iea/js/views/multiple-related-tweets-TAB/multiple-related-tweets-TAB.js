/*global define*/

define(['slick', 'commonService', 'socialTabService'], function() {
    'use strict';

    var commonService = null,
        socialTabService = null;

    var MultipleRelatedTweets = IEA.module('UI.multiple-related-tweets-TAB', function(multipleRelatedTweets, app, iea) { 
    
        _.extend(multipleRelatedTweets, {
      
            defaultSettings: {
                carouselWrapper: '.js-multiple-related-tweet__carousel',
                template: 'multiple-related-tweets-TAB/partial/multiple-related-tweet.hbss',
                pageStartIndex: 1
            },
            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                commonService = new IEA.commonService(); // creates new instance for common service
                socialTabService = new IEA.SocialTabs(); // creates new instance for common service

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');
                
                var self = this;
        
                self.componentJson = self.getModelJSON().multipleRelatedTweetsTAB;
                self.startIndex = self.defaultSettings.pageStartIndex;

                // render social gallery on page load
                self._getJsonData();

                this.triggerMethod('enable'); 
            },


            /**************************PRIVATE METHODS******************************************/          
            _getJsonData: function() {
                var self = this,
                  options = {};
               
                self.startIndex = self.defaultSettings.pageStartIndex;

                options.queryParams = socialTabService.setQueryParameters(self.componentJson, self.startIndex);

                options.servletUrl = self.componentJson.retrivalQueryParams.requestServletUrl;

                if (options.queryParams !== '') {
                  options.onCompleteCallback = function(data) { 
                    self._generateCarouselList(data);
                  };

                  commonService.getDataAjaxMethod(options);
                } else {
                    this.$el.hide();
                }
            },
            // get json data from service
            _generateCarouselList: function(data) {
                var self = this,
                settings = self.defaultSettings,
                solrData = $.parseJSON(data),
                template = self.getTemplate('social-gallery-list', settings.template),
                combinedJson = null,
                numberOfPosts = self.componentJson.retrivalQueryParams.numberOfPosts,
                delayTime = self.componentJson.delayTime*1000,
                autoPlay = true;
              
              //Autoplay False if delayTime = 0
              if(delayTime === 0){
                autoPlay = false;
              }
        
              combinedJson = {             
                'solrData': solrData,
                'pageNum': (settings.pageNum - 1)*numberOfPosts
              }

              self.$el.find(settings.carouselWrapper).append(template(combinedJson));  
        
              //Initiate Slick Carousel
              self._initCarousel();
            },

            //Initiate Slick Carousel
            _initCarousel: function() {
              var self = this,
                  settings = self.defaultSettings,
                  delayTime = self.componentJson.delayTime*1000,
                  autoPlay = true;
                    
              //Autoplay False if delayTime = 0
              if (delayTime === 0){
                autoPlay = false;
              }
                      
              self.$el.find(self.defaultSettings.carouselWrapper).slick({
                dots: true,
                arrows: false,
                autoplay: autoPlay,
                autoplaySpeed: delayTime
              }).
              on('afterChange', function(event, slick, currentSlide, nextSlide){
                   self._spinCarouselTracking(currentSlide);
              });
            },

            _spinCarouselTracking: function(index) {
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    var compName = $('.c-multiple-related-tweet').data('componentname');
                
                    digitalData.component = [];
                    digitalData.component.push({
                        'componentInfo' :{
                            'componentID': compName,
                            'name': compName
                        },
                        'attributes': {
                            'position': index
                        }
					});
					
                    var ev = {};
                    ev.eventInfo={
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.spincarousel,
                      'eventLabel' : compName
                    };
                    ev.category ={'primaryCategory':ctConstants.custom};
                    ev.attributes={'nonInteractive':{'nonInteraction': 1}};
                    digitalData.event.push(ev);
                }
            }   
        });
    });

    return MultipleRelatedTweets;
});
