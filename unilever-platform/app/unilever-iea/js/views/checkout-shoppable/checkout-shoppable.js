/*global define*/

define(function() {
    'use strict';

    var CheckoutShoppable = IEA.module('UI.checkout-shoppable', function(checkoutShoppable, app, iea) {

        _.extend(checkoutShoppable, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function() {
                this.triggerMethod('beforEnable');
                var jsonData = this.getModelJSON().checkoutShoppable;
                if(jsonData.shoppableConfig.enabled === "true" && jsonData.shoppableConfig.serviceProviderName ==="shoppable") {
                    this._createIframe(jsonData);
                }
                this.triggerMethod('enable');
            },

            /**************************PRIVATE METHODS******************************************/
            
            _createIframe: function(jsonData) {
                var shoppable_cart_id = UnlvCart.GetCartId();
                
                if (shoppable_cart_id != "") {
                    
                    var shoppable_checkout_url = jsonData.shoppableConfig.shoppableCheckoutUrl;
                    shoppable_checkout_url += 'cart=' + shoppable_cart_id + '&site_language='+jsonData.market+'&';
                    shoppable_checkout_url += 'orderComplete=' + encodeURIComponent(jsonData.shoppableConfig.orderCompleteUrl);
                    shoppable_checkout_url += '&campaign=' + jsonData.shoppableConfig.campaign;
                    shoppable_checkout_url += '&publisherCheckout=' + encodeURIComponent(jsonData.shoppableConfig.publisherCheckoutUrl);                    
                    shoppable_checkout_url += '&apiToken='+ jsonData.shoppableConfig.apiToken;
                    shoppable_checkout_url += '&orderCompletereturnToSite=' + encodeURIComponent(jsonData.shoppableConfig.orderCompletereturnToSiteUrl);
                    shoppable_checkout_url += '&noiframe=0';
                    shoppable_checkout_url += '&_ga=';
                    
                    var create_iframe = '<iframe id="ctl00_pageContent_ctl00_checkoutIframe" frameborder="0" scrolling="yes" target="_parent" style="min-height: 1500px;" height="150" width="100%" src="' + shoppable_checkout_url + '"></iframe>';
                    $("#shoppable_checkout_div").html(create_iframe);
                    
                } else {
                    $("#shoppable_checkout_div").html("YOUR SHOPPING BAG IS EMPTY");
                }

                // Create IE + others compatible event handler
                var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
                var eventer = window[eventMethod];
                var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

                // Listen to message sent from child window
                eventer(messageEvent, function (e) {
                    if (e.data != null) {
                        var resultstring = String(e.data);
                        var jsonstring = resultstring.replace("LUX_DATA:", "");
                        var jsondata = JSON.parse(jsonstring);
                        if (jsondata.url != null && jsondata.url != "") {
                            window.location = jsondata.url;
                        }
                    }

                }, false);
            }
        });
    });
    
    return CheckoutShoppable;
});
