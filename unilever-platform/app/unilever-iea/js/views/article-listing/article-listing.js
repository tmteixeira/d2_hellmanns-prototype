/*global define*/

define(['TweenMax','commonService'],function () {
    'use strict';

    var ArticleListing = IEA.module('UI.article-listing', function (articleListing, app, iea) {
        var commonService =null;
        _.extend(articleListing, {
		
			defaultSettings: {
                loadMore: '.js-article-listing__more-link',
                isLoadMore: false
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },
            
            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
				this.data = this.getModelJSON();
                this.$el.html(this.template(this.data));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },
			
			_generateArticleList: function (obj, options) {
                var self = this, objData = obj, itemsToShow;
				
				itemsToShow = objData.articleListing.articles;				
				options.isLoadMore = true;

                //lazy load images
                this.app.trigger('image:lazyload', this);

                setTimeout(function(){
                    self.setupEvents();
                    var items =  self.$el.find('.c-article-listing__item')
                    items.addClass('c-article-listing__show-mobile');
                    TweenMax.to(items,1,{alpha:1});
                    TweenMax.to('.c-article-container_headline-block', 1 ,{alpha:1});
                    TweenMax.to(window,1, {scrollTo:{y:self.$el.offset().top -80}});
                },1000);
				
				if(options.loadmore === true) {
					self.$(options.loadmore).fadeIn(500);
				} else {
					self.$(options.loadmore).fadeOut(500);
				}	
            },

            setupEvents: function () {

                var count= 0,
                    group = this.$el.find('.c-article-listing__item'),
                    str='';

                for (var i = 0; i < group.length; i++) {
                    var obj = group[i],
                        showMobile = this.$el.find('.c-article-listing__more-mobile').attr('data-show-mobile');
                    
                    //limit items to 3 on mobile views
                    if(i < showMobile){
                        $(obj).addClass('c-article-listing__show-mobile');
                    }

                    //add a row in between each group to avoid float issues
                    str='<div class="row"></div>';
                    if(count === 0){
                        $(obj).before(str);
                    }
                    count+= $(obj).data().col;
                    if(count === 12){
                        count = 0;
                    }
                }

            },
            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                commonService = new IEA.commonService();
                var self = this,
					options = {};
					
				options.isLoadMore = self.defaultSettings.isLoadMore;
				options.loadmore = self.defaultSettings.loadMore;
				this.$el.on('click', '.js-article-listing__more-link', function (ev) {
					ev.preventDefault();
					self._generateArticleList(self.getModelJSON(), options);
				});
				self.setupEvents();
				self.transitionIn();

                this.triggerMethod('enable');
			},
            transitionIn: function () {
                // On scroll reaching the articles' list
                var self = this;
                var articleOffset = self.$el.first().offset().top,
                    windowHeight = $(window).height();

                TweenMax.to('.c-article-container_headline-block', 1 ,{alpha:1,delay:1.2});
                if(commonService.isScrollAnimationSupported()){
                    $(document).on('scroll', function (e) {
                        articleOffset = self.$el.first().offset().top;
                        var currentOffset = $(window).scrollTop();
                        if (currentOffset > articleOffset - (windowHeight / 1.5)) {
                            self.staggerIn(self);
                            $(document).off('scroll', function (e) {});
                        }
                    });
                    if(self.$el.index() <= 1){
                        setTimeout(function(){
                            self.staggerIn(self);
                        },2000);
                    }
                }else{
                   self.staggerIn(self);
                }
            },
            staggerIn:function(self){
                TweenMax.staggerTo(self.$el.find('.c-article-listing__item'),1,{alpha:1},0.2);
            },

            /**************************PRIVATE METHODS******************************************/

            /**
             component private functions are written with '_' prefix to diffrentiate between the public methods
             example:

             _showContent: function (argument) {
                    // body...
                }

             */

        });
    });

    return ArticleListing;
});
