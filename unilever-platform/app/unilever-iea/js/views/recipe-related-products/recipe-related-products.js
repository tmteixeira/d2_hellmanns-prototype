/*global define*/

define(['slick','commonService'],function() {
    'use strict';

    var RecipeRelatedProducts = IEA.module('UI.recipe-related-products', function(recipeRelatedProducts, app, iea) {

        var commonService = null,
            self = this,
            isMobile = null,
            relatedProductsJson = null;

        _.extend(recipeRelatedProducts, {

            '$carousel': '.js-recipe-products-carousel',
            '$carouselSlide': '.c-recipe-products-item',
            'firstLoad': true,
            
            default: {
                'carouselTabP': {
                    infinite: false,
                    speed: 1250,
                    arrows: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    focusOnSelect: true,
                    dots: true
                },
                'carouselMob': {
                    infinite: false,
                    speed: 1250,
                    slidesToShow: 1,
                    focusOnSelect: true,
                    centerMode: true,
                    centerPadding: '100%',
                    dots: true
                }
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
             
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * start carousels
             * @method carouselInit
             * @return
             */
            carouselInit: function () {
                var $carousel = $(self.$carousel);
                setTimeout(function () {
                    self._setCarouselSize(self, $carousel);
                    $carousel.slick('slickGoTo', 1);
                    $carousel.slick(self.default.carouselTabP);
                }, 100);
            },
            
            init: function () {
                //cloned the carousel slides for the desktop & tablet version
                var string = $('.js-recipe-products-carousel').html();
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');
                commonService = new IEA.commonService();

                relatedProductsJson = this.getModelJSON().recipeRelatedProducts.Product[0];
                self.isMobile = commonService.isMobile() ? true: false;

                setTimeout(_.bind(function () {
                    if (this.$el.find('.js-recipe-products-container').attr('data-type') === 'carousel') {

                        this.init();
                        if($('.js-recipe-products-carousel').hasClass('enable-recipe-products-carousel')) {
                            this.carouselInit();
                        }
                    }
                }, this), 100);                
                
                //carousel click Analytics
                this._ctCarouselClick();
                
                this.triggerMethod('enable');
            },
            


            /**************************PRIVATE METHODS******************************************/          

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */
             _setCarouselSize: function (self, $carousel) {
                //remove slick if it has been loaded
                if (self.firstLoad) {
                    self.firstLoad = false;
                } else {
                    $carousel.slick('unslick');
                }
                
                if(self.isMobile) {
                    $carousel.slick(self.default.carouselMob);
                } else {
                    $carousel.slick(self.default.carouselTabP);
                }
                
                //scale the middle selected slide and got to the middle
                $carousel.slick('slickGoTo', 1);
            },
            
            _ctComponentInfo: function() {
                var self = this,
                    compName = self.$el.find('[data-componentname]').data('componentname'),
                    compVar = self.$el.find('[data-component-variants]').data('component-variants'),
                    compPos = self.$el.find('[data-component-positions]').data('component-positions');
                
                digitalData.component = [];
                digitalData.component.push({
                    'componentInfo' :{
                        'componentID': compName,
                        'name': compName
                    },
                    'attributes': {
                        'position': compPos,
                        'variants': compVar
                    }
                });
            },
            
            // Carousel click tracking
            _ctCarouselClick: function() {
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {    
                    var ev = {},
                        self = this,
                        $carousel = $(self.$carousel),
                        $carouselItem = self.$el.find($carousel),
                        label;

                    $(document).on('click', '.slick-arrow, .slick-dots li', function(evt) {
                        var _this = $(this),
                        hostedUrl = window.location.href;

                        self._ctComponentInfo();

                        if (_this.is('.slick-arrow')) {
                            label = _this.text();
                        } else {
                            label = 'button#' + parseInt(_this.find('button').text());
                        }

                        ev.eventInfo = {
                          'type': ctConstants.trackEvent,
                          'eventAction': ctConstants.carouselClick,
                          'eventLabel' : label + ' - ' + hostedUrl
                        };

                        ev.category = {'primaryCategory':ctConstants.custom};
                        digitalData.event.push(ev);
                    });
                }
            }
            
        });
    });

    return RecipeRelatedProducts;
});
