/*global define*/

define(['TweenMax', 'iscroll', 'animService', 'search', 'typeahead'], function() {
    'use strict';

    var Navigation = IEA.module('UI.global-navigation', function(globalNavigation, app, iea) {

        var self = this,
            navScroll = null,
            isMobile = false,
            currentLevel = 1,
            scrollingMobile = false,
            commonService = null,
            searchService = null,
            animService = null,
            screenWidth = null;

        _.extend(globalNavigation, {

            defaultSettings: {
                modalCl: '.js-modal-search',
                searchWrapper: '.c-global-search__form .c-global-search__wrapper',
                searchInputField: '.c-global-search__form .typeahead',
                clearSearch: '.o-btn-clear-search',
                searchButton: '.c-global-search__form .js-btn-search',
                searchForm: '.c-global-search__form',
                defaultSuggestions: true,
                searchInputFont: 30,
                searchInputFontMobile: 16,
                suggestionsLimit: 6,
                level2ClassName: '.o-navbar-tier2-item > .o-navbar-label',
                level2BackgroundImg: '.level2-background-image',
                level3ClassName: '.o-navbar-tier3-item > .o-navbar-label',
                level3BackgroundImg: '.level3-background-image',
                level4ClassName: '.o-navbar-tier4-item > .o-navbar-label',
                level4BackgroundImg: '.level4-background-image',
            },

            events: {
                'focus keypress .c-global-search__form .typeahead': 'keyboardHandler', // capture keyboard events
                'click .c-global-search__form .js-btn-search': 'submitForm', // render search listing on search button click
                'click .js-search-btn': 'openModal',
                'click .js-btn-close': 'closeModal',
                'submit .c-global-search__form': 'submitForm'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function(options) {
                this._super(options);
                searchService = new IEA.search(); // creates new search instance
                animService = new IEA.animService();
                commonService = new IEA.commonService();

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function() {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function() {
                this.triggerMethod('beforEnable');

                var self = this,
                    settings = self.defaultSettings;

                self.modelData = self.getModelJSON().globalNavigation;
                self.searchData = self.modelData.search;
                self.searchWrapper = settings.searchWrapper;
                self.searchInputField = settings.searchInputField;
                self.clearSearch = settings.clearSearch;
                self.searchButton = settings.searchButton;
                self.searchForm = settings.searchForm;
                self.shoppableCart = settings.shoppableCart;
                screenWidth = $(window).width();
                self.searchKeyword = '';

                if (commonService.isMobile()) {
                    self.inputFont = settings.searchInputFontMobile;
                } else {
                    self.inputFont = settings.searchInputFont;
                }
               
                // Window events
                app.on('window:scrolled', function() {

                    var $continueBt = $('.o-continue-arrow'),
                        scrollOffset = $(window).scrollTop(),
                        $navBar = $('.o-navbar');

                    if ($continueBt.length > 0) {
                        TweenMax.to('.o-continue-arrow', 1, {
                            autoAlpha: 0,
                            onComplete: function() {
                                $('.o-continue-arrow').remove();
                            }
                        });
                    }

                    if (scrollOffset > 0) {
                        $navBar.addClass('u-transparent');
                    } else {
                        $navBar.removeClass('u-transparent');
                    }
                });

                // Close on press ESC
                $(document).keyup(function(e) {
                    if (e.keyCode == 27 && $(self.defaultSettings.modalCl).is(':visible')) { // escape key maps to keycode `27`
                        $('.js-navbar-overlay-close').trigger('click');
                        self.closeModal();
                    }
                });

                // Can't use app.on('window:resized') because it doesn't get triggered immediately
                var windowResized = commonService.debounce(function() {

                    if (screenWidth !== $(window).width()) {
                        // Logic for while Android keyboard is shown
                        self._resetNav();
                        screenWidth = $(window).width();
                    }

                }, 1000, true);

                window.addEventListener('resize', windowResized);
                 self._resetNav();
                

                // Search method
                self.enableSearchMethods();

                this.triggerMethod('enable');
            },

            onShow: function() {
                commonService.ios7SVGsupport(['.js-navbar-overlay-close']);
            },

            _resetNav: function() {
                self.isMobile = self._checkMobile();
                self._destroyNav();
                self.openNav();
                self._updateScroller();
            },

            _destroyNav: function() {

                // Reset styles
                $('.o-navbar-item, .o-navbar-tier1-scroller-wrapper, .o-navbar-menu-wrapper').removeAttr('style');

                // Close the nav if it's already opened
                var $nav = $('.o-navbar'),
                    $Togglelabel = $('.js-navbar-toggle-label'),
                    $backButton = $('.o-navbar-back'),
                    $tier1Menu = $('.o-navbar-tier1-wrapper');

                $nav.removeClass('open');
                TweenMax.to($backButton, 0.5, { autoAlpha: 0, display: 'none' });
                $('.o-navbar-tier1-wrapper').css('left', 0);

                // Reset custom module if visible
                var $customModule = $('.o-navbar-custom-module-wrapper'),
                    $customMenus = $('.o-navbar-custom-module-list-wrapper');
                TweenMax.to($customModule, 0.5, { 'left': 0, autoAlpha: 0, display: 'none' });

                if ($(window).width() < 991) {
                    $customModule.css('overflow', 'hidden');
                } else {
                    $customModule.css('overflow', 'scroll');
                }

                //self._closeAllTiers();
                self._toggleOverlay(false);
                
                // close the level1 background image
                $('.level1-background-image').hide();

                // Unbind mobile and desktop events
                $('.js-navbar-overlay-close, .o-navbar-back, .js-navbar-toggle, .o-navbar-tier2-wrapper, .o-navbar-tier1-item .o-navbar-label, .o-navbar-tier2-item .o-navbar-label, .o-navbar-menu-wrapper .o-navbar-label').unbind();
                currentLevel = 1;
            },

            _destroyScroller: function($el) {

                if ($el.data('scroller')) {
                    $el.css('overflow', 'visible');
                    var scroller = $el.data('scroller');
                    scroller.scrollTo(0, 0); // If we don't reset position, the child menu will inherit the offset
                    scroller.destroy();
                    scroller.disable();
                    scroller = null;
                    $el.data('scroller', scroller);
                }
            },

            openNav: function() {

                $('.o-navbar-tier1-item > .o-navbar-label').on('click', function() {
                    // Fade out previous menu
                    var $activeMenu = $('.o-navbar-tier1-item.active'),
                        $obj = $(this),
                        dataAttributeName = $(this).closest('li').attr('data-level');
                    
                    self.level2ClassName = self.defaultSettings.level2ClassName;
                    self.level2BackgroundImg = self.defaultSettings.level2BackgroundImg;
                    self.level3ClassName = self.defaultSettings.level3ClassName;
                    self.level3BackgroundImg = self.defaultSettings.level3BackgroundImg;
                    self.level4BackgroundImgClassName = self.defaultSettings.level4ClassName;
                    self.level4BackgroundImg = self.defaultSettings.level4BackgroundImg;
                    
                    if ($activeMenu.length > 0) {
                        TweenMax.to($activeMenu.find('.o-navbar-tier2-wrapper'), 0.5, { autoAlpha: 0 });
                        $('.level1-background-image').hide();
                    }
                    TweenMax.to($obj.siblings('.o-navbar-tier2-wrapper'), 0.2, { autoAlpha: 1 });

                    // Update active items
                    $obj.parent().addClass('active').siblings().removeClass('active');
                    
                    if ($obj.parent().hasClass('custom')) {
                        $('.o-navbar-custom-module-wrapper').addClass('active').removeAttr('style');
                    } else {
                        $('.o-navbar-custom-module-wrapper').removeClass('active');
                    }
                    
                    //level-1 background image on click
                    $('[data-level="'+dataAttributeName+'"]').find('.level1-background-image').show();
                    
                    // level 2 background image on hover of each li item
                    self._showImgOnHover(self.level2ClassName,self.level2BackgroundImg);
                    // level 3 background image on hover of each li item
                    self._showImgOnHover(self.level3ClassName,self.level3BackgroundImg);
                    // level 4 background image on hover of each li item
                    self._showImgOnHover(self.level4ClassName,self.level4BackgroundImg);
                
                    self._toggleOverlay(true);
                });
                
                // Overlay close
                $('.js-navbar-overlay-close').on('click', function() {
                    $('.level1-background-image').hide();
                    self._toggleOverlay(false);
                });

                // Level 2 click events
                $('.o-navbar-tier2-item.submenu .o-navbar-label').on('click', function() {
                    self._toggleSubmenu($(this));
                });

                $(document).off('click touchstart', '.js-navbar-toggle').on('click touchstart', '.js-navbar-toggle', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    self._setupToggleNav();
                });


                if ($(window).width() < 991) {
                    self._setupMobileNav();
                    // Go to submenu
                    $('.o-navbar-item.submenu .o-navbar-label').on('click', function() {
                        self._toggleMobileSubmenu($(this));
                    });

                    $('.o-navbar-custom-module-menu.submenu > .o-navbar-label').on('click', function() {
                        self._toggleMobileCustomSubmenu($(this));
                    });

                    // Go back
                    $('.o-navbar-back').on('touchstart click', function() {
                        if ($('.o-navbar-custom-module-list-wrapper').is(':visible') && $(window).width() < 768) { // Custom submenu opened
                            self._toggleMobileCustomSubmenu(false);
                        } else {
                            self._toggleMobileSubmenu(false);
                        }
                    });
                }
            },
            
            _showImgOnHover: function(levelClass, backgroundImgClass) {
                //background image on hover of each li item
                $(levelClass).hover(function(){
                    var dataAttributeName = $(this).closest('li').attr('data-level');
                    $('[data-level="'+dataAttributeName+'"]').find(backgroundImgClass).show();
                },function(){
                    $(backgroundImgClass).hide(); 
                });
            },

            _setupMobileNav: function() {

                var windowWidth = $(window).width(),
                    windowHeight = $(window.top).height(),
                    navBarHeight = $('.o-navbar').height(),
                    $tier1Wrapper = $('.o-navbar-tier1-scroller-wrapper'),
                    $tier1 = $tier1Wrapper.find('.o-navbar-tier1-wrapper'),
                    h = windowHeight - navBarHeight,
                    offset = 30;

                $('body').addClass('o-navbar-mobile').removeClass('o-navbar-desktop');

                // Move next levels to the left
                $('.o-navbar-tier2-wrapper').css('left', windowWidth);
                $('.o-navbar-tier3-wrapper').css('left', windowWidth);
                $('.o-navbar-tier4-wrapper').css('left', windowWidth);

                // Custom module submenus dimensions and positions
                if (windowWidth < 992) { // Skip iPad portrait
                    var $customMenu = $('.o-navbar-custom-module-list-wrapper');
                    $customMenu.width(windowWidth);
                    $('.o-navbar-custom-module-list-wrapper').height(h);
                    $customMenu.css('left', windowWidth);
                }

                // Add a scroller on mobile landscape or very small screens
                if ($tier1.height() > h - offset) {
                    h -= offset; // Add 30px of offset
                    self._updateMobileScroller();
                    $tier1Wrapper.css({ 'display': 'none' });
                } else {
                    $tier1Wrapper.removeAttr('style');
                    h -= 100;
                }

                // Set the height and width of the menus
                $('.o-navbar-menu-wrapper').not('.o-navbar-tier1-wrapper').height(h);
                $('.o-navbar-menu-wrapper').not('.o-navbar-tier1-wrapper').width(windowWidth);
            },

            _setupToggleNav: function() {
                var $nav = $('.o-navbar'),
                    $Togglelabel = $('.js-navbar-toggle-label'),
                    $backButton = $('.o-navbar-back'),
                    $tier1Menu = $('.o-navbar-tier1-wrapper'),
                    $customModule = $('.o-navbar-custom-module-wrapper');

                $nav.toggleClass('open');
                currentLevel = 1;

                if ($nav.hasClass('open')) {

                    $('.o-navbar-tier1-wrapper').css('left', 0);
                    self._toggleOverlay(true);

                    // Animate tier 1 menu items sequentially
                    var windowWidth = $(window).width();

                    $('.o-navbar-tier1-item').each(function(i) {
                        $(this).css({ 'position': 'relative', 'left': '-' + windowWidth + 'px' });

                        TweenMax.to($(this), 0.5, {
                            left: 0,
                            delay: (0.1 * (i + 2)),
                            onComplete: function(el) {
                                el.css('position', 'initial');
                            },
                            onCompleteParams: [$(this)]
                        });
                    });

                    // Animate tier 1 menu
                    TweenMax.to($('.o-navbar-tier1-scroller-wrapper'), 0.5, { autoAlpha: 1, display: 'block' });
                    TweenMax.to($tier1Menu, 0, {
                        autoAlpha: 1,
                        display: 'block',
                        onComplete: function() {

                            // Refresh tier1 scroller if exists
                            var $tier1Scroller = $('.o-navbar-tier1-scroller-wrapper').css({ 'display': 'block' }).data('scroller');
                            if ($tier1Scroller) {
                                $tier1Scroller.refresh();
                            }
                        }
                    });
                } else {
                    if (commonService.isMobile()) {
                        TweenMax.to($('.o-navbar-tier1-scroller-wrapper'), 0.5, { autoAlpha: 0, display: 'none' });
                    }
                    TweenMax.to($tier1Menu, 0.5, { autoAlpha: 0, display: 'none' });
                    TweenMax.to($backButton, 0.5, { autoAlpha: 0, display: 'none' });

                    // Reset custom module if visible
                    var $customMenus = $('.o-navbar-custom-module-list-wrapper');

                    TweenMax.to($customModule, 0.5, { 'left': 0, autoAlpha: 0, display: 'none' });

                    if ($(window).width() < 768) { // Skip iPad portrait
                        TweenMax.to($customMenus, 0.5, { autoAlpha: 0, display: 'none' });
                    }

                    self._toggleOverlay(false);
                }
            },

            _toggleMobileSubmenu: function($el) {

                if (!self.scrollingMobile) {

                    var windowWidth = $(window).width(),
                        $backButton = $('.o-navbar-back'),
                        goBack = $el ? false : true,
                        $customModule = $('.o-navbar-custom-module-wrapper');

                    if (goBack) {
                        currentLevel--;

                        var $currentMenu = $('.o-navbar-menu-wrapper.opened').parents('.o-navbar-menu-wrapper').first();
                        $('.o-navbar-menu-wrapper').removeClass('opened');
                        $currentMenu.addClass('opened');

                        // Hide custom module if visible
                        if ($customModule.is(':visible')) {
                            TweenMax.to($customModule, 0.5, { autoAlpha: 0, display: 'none' });
                        }

                        TweenMax.to('.o-navbar-tier1-wrapper', 0.5, {
                            left: "+=" + windowWidth,
                            onComplete: function() {

                                switch (currentLevel) {
                                    case 1:
                                        $('.o-navbar-tier2-wrapper').hide();
                                        break;
                                    case 2:
                                        $('.o-navbar-tier3-wrapper').hide();
                                        break;
                                    case 3:
                                        $('.o-navbar-tier4-wrapper').hide();
                                        break;
                                }

                                self._updateMobileScroller();
                            }
                        });
                    } else {
                        if ($el.parent().hasClass('submenu')) {

                            //$el.parent().addClass('opened');

                            var $newLevel = $el.siblings('.o-navbar-menu-wrapper');

                            // Hide all menus and show the next menu
                            TweenMax.to($newLevel, 0.5, { autoAlpha: 1, display: 'block' });

                            switch (currentLevel) {
                                case 1:
                                    $('.o-navbar-tier2-wrapper').hide();
                                    break;
                                case 2:
                                    $('.o-navbar-tier3-wrapper').hide();
                                    break;
                                case 3:
                                    $('.o-navbar-tier4-wrapper').hide();
                                    break;
                            }

                            // Destroy scrollers
                            $('.o-navbar-menu-wrapper').each(function(i) {
                                self._destroyScroller($(this));
                            });
                            self._destroyScroller($('.o-navbar-tier1-scroller-wrapper'));

                            // Update opened menu status
                            $('.o-navbar-menu-wrapper').removeClass('opened');
                            $newLevel.addClass('opened');

                            // Move menus to the left one level
                            TweenMax.to('.o-navbar-tier1-wrapper', 0.5, { left: "-=" + windowWidth, onComplete: self._updateMobileScroller });

                            currentLevel++;
                        }

                        // Display the custom module
                        if ($el.parent().hasClass('custom')) {
                            TweenMax.to($('.o-navbar-custom-module-wrapper'), 0.5, { autoAlpha: 1, display: 'block', delay: 0.5 });
                            $('.o-navbar-tier1-scroller-wrapper').height(0);
                        }
                    }

                    if (currentLevel > 1) {
                        TweenMax.to($backButton, 0.5, { autoAlpha: 1, display: 'block' });
                    } else {
                        TweenMax.to($backButton, 0.5, { autoAlpha: 0, display: 'none' });
                    }

                    self.scrollingMobile = true;
                }
            },

            _toggleMobileCustomSubmenu: function($el) {
                var windowWidth = $(window).width(),
                    goBack = $el ? false : true,
                    $customModule = $('.o-navbar-custom-module-wrapper'),
                    $customMenu = $('.o-navbar-custom-module-list-wrapper');

                $customMenu.removeClass('opened');

                if (goBack) {
                    // Update container overflow to display the menus
                    $('.o-navbar-custom-module-wrapper').css('overflow', 'scroll');

                    // Hide submenus
                    TweenMax.to($customMenu, 0.5, { autoAlpha: 0, display: 'none' });

                    // Move menus to the left one level
                    TweenMax.to($customModule, 0.5, { left: 0, delay: 0.5 });
                } else {
                    var $menuWrapper = $el.siblings('.o-navbar-custom-module-list-wrapper');
                    $menuWrapper.addClass('opened');

                    // Update container overflow to display the menus
                    $('.o-navbar-custom-module-wrapper').css('overflow', 'visible');

                    // Move menus to the left one level
                    TweenMax.to($customModule, 0.5, { left: "-=" + windowWidth });

                    // Show submenus
                    TweenMax.to($menuWrapper, 0.5, { autoAlpha: 1, display: 'block', delay: 0.5, onComplete: self._updateMobileScroller });
                }
            },

            _updateMobileScroller: function() {
                self.scrollingMobile = false;
                var windowHeight = $(window.top).height(),
                    windowWidth = $(window).width(),
                    navBarHeight = $('.o-navbar').height(),
                    $tier1Wrapper = $('.o-navbar-tier1-scroller-wrapper'),
                    $openedMenuWrapper = null,
                    offset = 30,
                    $openedMenu = $tier1Wrapper.find('.o-navbar-tier1-wrapper'),
                    h = windowHeight - navBarHeight - offset;

                // If it's tier 1 and the menu fits, then set the the big offset
                if (($openedMenu.height() < h - offset)) {
                    offset = 100;
                }

                // Tier 1 scroller
                if (currentLevel === 1) {
                    $tier1Wrapper.css({ 'position': 'absolute', 'top': navBarHeight + offset });
                    $tier1Wrapper.width(windowWidth);
                    $tier1Wrapper.height(h);
                    $openedMenu.css({ 'top': '0' });
                } else {
                    $openedMenu = $('.o-navbar-menu-wrapper.opened .o-navbar-dropdown-menu');
                }

                $openedMenuWrapper = $openedMenu.parent();
                var openedMenuHeight = $openedMenu.height(),
                    topOffset = navBarHeight + offset;

                // Check if we need a scroller for the current menu
                //console.log(openedMenuHeight, (windowHeight - topOffset));
                if (openedMenuHeight > (windowHeight - topOffset)) {
                    $openedMenuWrapper.css('overflow', 'hidden');
                    if (!$openedMenuWrapper.data('scroller')) {

                        var newScroller = new IScroll($openedMenuWrapper.get(0), { click: true, probeType: 3, mouseWheel: true, scrollbars: false });
                        $openedMenuWrapper.data('scroller', newScroller);
                    } else {
                        var newScroller = $openedMenuWrapper.data('scroller');
                        newScroller.refresh();
                    }
                }
            },

            _toggleOverlay: function(state) {

                var $overlay = $('.o-navbar-overlay'),
                    $pageWrapper = $('.js-main-wrapper');

                if (state === false) {
                    self.scrollingMobile = false;

                    // Reset custom module if visible
                    var $customModule = $('.o-navbar-custom-module-wrapper');

                    TweenMax.to($customModule, 0.5, { autoAlpha: 0, display: 'none' });

                    $('body').removeClass('o-navbar-opened');
                    TweenMax.to($('.o-continue-arrow'), 0.5, { autoAlpha: 1, display: 'block' });
                    TweenMax.to($('.o-navbar-tier2-wrapper:visible'), 0.2, { autoAlpha: 0 });
                    $('.o-navbar-tier1-item').removeClass('active');
                    TweenMax.to($overlay, 0.2, { opacity: 0, display: 'none', delay: 0.2, ease: Power2.easeInOut });
                    $pageWrapper.removeAttr('style');

                } else if (!$('body').hasClass('o-navbar-opened')) {
                    $('body').addClass('o-navbar-opened');
                    TweenMax.to($('.o-continue-arrow'), 0.5, { autoAlpha: 0, display: 'none' });

                    $overlay.css('display', 'block');
                    TweenMax.to($overlay, 0.5, { opacity: 0.8, ease: Power2.easeInOut });
                    TweenMax.to($pageWrapper, 0.5, { '-webkit-filter': 'blur(10px)', 'filter': 'blur(10px)' });
                }
            },

            _toggleSubmenu: function($el) {
                var $parent = $el.parent(),
                    $submenu = $parent.find('.o-navbar-menu-wrapper').first();

                //$('.o-navbar-tier3-item.submenu').removeClass('opened');
                // Close menu
                if ($parent.hasClass('opened')) {
                    // Show all siblings menus
                    self._closeSubmenu($parent, $submenu);
                } else {
                    self._closeTiers($parent);
                    self._openSubmenu($parent, $submenu);
                }
            },

            _openSubmenu: function($el, $submenu) {
                TweenMax.set($submenu, { opacity: 1, visibility: 'visible', height: 'auto' });
                TweenMax.from($submenu, 0.6, { opacity: 0, height: 0, onComplete: self._updateScroller });
                $el.addClass('opened');

                // Change icons
                TweenMax.to($el.find('.o-navbar-label-down').first(), 1, { autoAlpha: 0 });
                TweenMax.to($el.find('.o-navbar-label-cross').first(), 1, { autoAlpha: 1 });
            },

            _closeSubmenu: function($el, $submenu) {
                $('.o-navbar-dropdown-menu').addClass('animated');
                TweenMax.to($submenu, 0.6, { opacity: 0, height: 0, visibility: 'hidden', onComplete: self._updateScroller });
                $el.removeClass('opened');

                // Change icons
                TweenMax.to($el.find('.o-navbar-label-down').first(), 1, { autoAlpha: 1 });
                TweenMax.to($el.find('.o-navbar-label-cross').first(), 1, { autoAlpha: 0 });
            },

            _closeTiers: function($el) {

                $el = $el || $('.o-navbar-tier3-item.opened').first();

                // Close tier 3 menu
                if ($el.hasClass('o-navbar-tier3-item')) {
                    var $tier3Menu = $('.o-navbar-tier3-item.opened').first();
                    if ($tier3Menu.length > 0 && $tier3Menu !== $el) {
                        self._closeSubmenu($tier3Menu, $tier3Menu.find('.o-navbar-menu-wrapper').first());
                    }
                    $('.o-navbar-tier2-item.opened > .o-navbar-label').addClass('disabled');
                } else { // Close tier 2 menu
                    var $tier2Menu = $('.o-navbar-tier2-item.opened').first();
                    if ($tier2Menu.length > 0 && $tier2Menu !== $el) {
                        self._closeSubmenu($tier2Menu, $tier2Menu.find('.o-navbar-menu-wrapper').first());
                    }

                    var $tier3Menu = $('.o-navbar-tier3-item.opened').first();
                    if ($tier3Menu.length > 0) {
                        $tier3Menu.find('.o-navbar-label').first().trigger('click');
                    }
                }
            },

            _updateScroller: function() {

                setTimeout(function() {
                    $('.o-navbar-dropdown-menu').removeClass('animated');
                }, 1500);

                if (self.navScroll !== undefined) {
                    self.navScroll.refresh();

                    var $openedMenu = $('.o-navbar-tier2-wrapper .opened'),
                        currentElm = $openedMenu.last().get(0),
                        wrapperHeight = $('.o-navbar-tier2-wrapper').height() + 100;

                    if ($openedMenu.length > 0) {
                        if ($(currentElm).height() > wrapperHeight) {
                            self.navScroll.scrollToElement(currentElm, 1000, true, false, IScroll.utils.ease.quadratic);
                        } else {
                            self.navScroll.scrollToElement(currentElm, 1000, true, true, IScroll.utils.ease.quadratic);
                        }
                    }
                }
            },

            _checkMobile: function() {
                return commonService.isMobileAndTablet() && ($(window).width() <= 768);
            },


            /**************************SEARCH METHODS******************************************/

            /**
             * starts search methods
             * @method 
             * @return 
             */
            openModal: function(e) {
                e.preventDefault();
                var options = {};
                animService.openOverlay(this.defaultSettings.modalCl, options = {
                    onCompleteCallback: function() {
                        $('.c-global-search .typeahead').focus();
                    }
                });
            },

            /**
             * to close an overlay
             * @method openModal
             * @return 
             */

            closeModal: function(e) {
                e.preventDefault();
                $(this.defaultSettings.searchInputField).typeahead('val', '');
                animService.closeOverlay(this.defaultSettings.modalCl);
            },

            /**
             * call search  methods
             * @method enableSearchMethods
             * @return 
             */

            enableSearchMethods: function() {
                // enable the typeahead focuse on page load
                var self = this;
                $(document).on('focus.typeahead.data-api', '[data-provide="typeahead"]', function(e) {
                    var $this = $(this);
                    if ($this.data('typeahead')) return true;
                    $this.typeahead($this.data());
                });

                // show suggestions
                if ($(self.searchInputField).parent('.twitter-typeahead').length === 0) {
                    self.renderSuggestions();
                }

                // clear search on cross click
                searchService.clearSearchVal(self.clearSearch, self.searchInputField);
            },

            // handle search on keyboard handler events
            keyboardHandler: function(e) {
                var self = this,
                    kCode = e.keyCode || e.charCode, //for cross browser
                    $this = $(e.target),
                    searchForm = $this.parents(options.searchForm),
                    actionURL = '';

                if (kCode === 13) { //Capture Enter Key
                    if ($this.val().length > 0 && !$this.val().match(/^\s*$/)) {
                        self.updateParams($this.val(), searchForm);
                    } else {
                        $(self.searchInputField).focus();
                        return false;
                    }
                }
            },

            // update parameters of search form submit
            submitForm: function(e) {
                var self = this,
                    _this = $(e.target),
                    searchForm = (_this.is(self.searchForm)) ? _this : _this.parents(self.searchForm),
                    searchInput = searchForm.find('.c-global-search__textbox'),
                    submitUrl = searchForm.data('action');

                if (searchForm.attr('action') === '') {
                    e.preventDefault();

                    searchForm.attr('action', submitUrl);

                    if (searchInput.typeahead('val').length > 0 && !searchInput.typeahead('val').match(/^\s*$/)) {
                        self.updateParams(searchInput.typeahead('val'), searchForm);
                    } else {
                        $(searchInput).focus();
                    }
                } else {
                    return true;
                }
            },

            /**
             * Auto complete
             * @method autoComplete
             * @return 
             */

            renderSuggestions: function() {
                // constructs the suggestion engine
                var self = this,
                    options = {},
                    searchData = self.searchData;

                if (typeof searchData.dropDownLabel !== undefined) {
                    options.dropDownLabel = searchData.dropDownLabel;
                }

                options.searchWrapper = self.searchWrapper;
                options.searchInputField = self.searchInputField;
                options.clearSearch = self.clearSearch;
                options.customKeyword = self.defaultSettings.defaultSuggestions;
                options.locale = $('input[name=Locale]').val();
                options.brandName = $('input[name=BrandName]').val();
                options.clearSearch = self.clearSearch;
                options.searchKeyword = self.searchKeyword;
                options.inputFont = self.inputFont;
                options.suggestionsLimit = self.defaultSettings.suggestionsLimit;

                options.selectCallback = function(options, keywordVal, obj) {
                    self.updateParams(keywordVal, obj.parents(self.searchForm));
                    options.searchKeyword = self.searchKeyword;

                    if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                        self.searchAnalytics();
                    }
                };

                searchService.showSuggestions(options);
            },

            searchAnalytics: function() {
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    var ev = {},
						compName = $(".o-global-search").data('componentname'),
						compVariants = $(".o-global-search").data('component-variants'),
						compPositions = $(".o-global-search").data('component-positions');
					
					digitalData.component.push({
						'componentInfo' :{
							'componentID': compName,
					   		'name': compName
						},
						'attributes': {
							'position': compPositions,
							'variants': compVariants
						}
					});
					
					ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.siteSearch,
                        'eventLabel': this.searchKeyword
                    };
                    ev.category = { 'primaryCategory': ctConstants.other };
                    digitalData.event.push(ev);
					digitalData.component = [];
                }
            },

            updateParams: function(keywordVal, form) {
                var self = this,
                    dropdownLabel = self.searchData.dropDownLabel,
                    queryLabel,
                    searchForm = form,
                    contentType = '';

                if ((keywordVal.indexOf(dropdownLabel.inArticles) !== -1) || (keywordVal.indexOf(dropdownLabel.inProducts) !== -1) || (keywordVal.indexOf(dropdownLabel.inEverything) !== -1)) {

                    if (keywordVal.indexOf(dropdownLabel.inArticles) !== -1) {
                        contentType = 'ContentType:Article';
                        queryLabel = keywordVal.split(' ' + dropdownLabel.inArticles);
                    } else if (keywordVal.indexOf(dropdownLabel.inProducts) !== -1) {
                        contentType = 'ContentType:Product';
                        queryLabel = keywordVal.split(' ' + dropdownLabel.inProducts);
                    } else if (keywordVal.indexOf(dropdownLabel.inEverything) !== -1) {
                        contentType = '';
                        queryLabel = keywordVal.split(' ' + dropdownLabel.inEverything);
                    }
                    self.searchKeyword = queryLabel[0];
                } else {
                    self.searchKeyword = keywordVal;
                    contentType = '';
                }
                $('input[name=q]').val(self.searchKeyword);
                $('input[name=fq]').val(contentType);
                self.searchAnalytics();
                searchForm.submit();
            },

            resizeFont: function(obj, fontsize, field) {
                searchService.shrinkToFill(obj, fontsize, field);
            }
        });
    });

    return Navigation;
});

