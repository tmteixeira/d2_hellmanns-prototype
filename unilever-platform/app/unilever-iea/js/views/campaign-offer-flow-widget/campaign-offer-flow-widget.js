/*global define*/

define(function() {
    'use strict';

    var CampaignOfferFlowWidget = IEA.module('UI.campaign-offer-flow-widget', function(campaignOfferFlowWidget, app, iea) {

        _.extend(campaignOfferFlowWidget, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                this._campaignAnalytics();

                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/          

            _campaignAnalytics: function() {
                /* Campaign offer flow widget analytics */
        var $CW = $CW || {};
        $CW = {
            callback: {
                "onload": function() {
                    // console.log('Campaign Widget Load - Success');
                    //campaignOnLoad();
                    var ev = {};
                    // track component info
                    trackComponentInfo($this);

                    digitalData.promotion=[];
                    digitalData.promotion.push({
                    'promotionInfo': {
                       'promotionId':'PROMO ID',
                       'promotionName':'PROMO NAME',
                       'promotionCreative':'PROMO CREATIVE',
                       'position':4
                    }}

                    );
                    ev.eventInfo={
                      'type':ctConstants.promotionView,
                     };
                    ev.attributes={'nonInteraction':1};
                    ev.category ={'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                },
                "offersubmission": function() {
                 //   console.log('Campaign Widget - Offer Submission - Success');
                 //analytics for coupon selection
                    var ev = {};
                    // track component info
                    trackComponentInfo($this);
                    ev.eventInfo={
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.couponSelected,
                      'eventLabel' : 'EVENT LABEL'
                    };
                    ev.category ={'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                },
                "contactinfosubmission": function() {
                    // console.log('Campaign Widget - Contact Info Submission - Success');
                   
                    // analytics for campaing- user validation
                    var ev = {};
                    // track component info
                    trackComponentInfo($this);

                    ev.eventInfo={
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.campaign,
                      'eventLabel' : 'EVENT LABEL'
                    };
                    ev.category ={'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                },

                "success": function() {
                    // console.log('Campaign Widget - Data Submission to GFH - Success');
                     // analytics for marketing optins 
                   var ev = {};
                   // track component info
                    trackComponentInfo($this);

                    ev.eventInfo={
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.Acquisition,
                      'eventLabel' : 'BRAND OPTIN/CORPORATE OPTIN'
                    };
                    ev.category ={'primaryCategory':ctConstants.conversion};
                    digitalData.event.push(ev);

                    // analytics for campaign - user detail filled in 
                    var ev = {};
                    // track component info
                    trackComponentInfo($this);

                    ev.eventInfo={
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.campaign,
                      'eventLabel' : 'EVENT LABEL'
                    };
                    ev.category ={'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                },
                "error": function() {
                    console.log('Campaign Widget - Data Submission to GFH - Failure');
                    var ev = {};
                    // track component info
                    trackComponentInfo($this);

                    ev.eventInfo={
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.inputerror,
                      'eventLabel' : '[Error Type]'
                    };
                    ev.category ={'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                },
                "thankyou": function() {
                    // console.log('Campaign Widget - Thankyou Panel/Message - Success');
                  //  campaignThankyou();
                  var ev = {};
                  // track component info
                    trackComponentInfo($this);

                    ev.eventInfo={
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.campaign,
                      'eventLabel' : 'EVENT LABEL'
                    };
                    ev.category ={'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                },
                "onloginSuccess": function() {
                    // console.log('Campaign Widget - User Login - Success');
                    //campaignOnloginSuccess();
                    var ev = {};
                    // track component info
                    trackComponentInfo($this);

                    ev.eventInfo={
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.signIns,
                      'eventLabel' : 'EVENT LABEL'
                    };
                    ev.category ={'primaryCategory':ctConstants.other};
                    digitalData.event.push(ev);
                },
                "onloginFailure": function() {
                    console.log('Campaign Widget - User Login - Failure');
                    var ev = {};
                    // track component info
                    trackComponentInfo($this);

                    ev.eventInfo={
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.inputerror,
                      'eventLabel' : '[Error Type]'
                    };
                    ev.category ={'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                },
                "surveysubmission": function() {
                    console.log('Campaign Widget - Survey Submission');
                    var ev = {};
                    // track component info
                    trackComponentInfo($this);

                    ev.eventInfo={
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.campaign,
                      'eventLabel' : 'EVENT LABEL'
                    };
                    ev.category ={'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                },
                "userEmailSubmission": function() {
                    console.log('Campaign Widget - Email Submission');
                    var ev = {};
                    // track component info
                    trackComponentInfo($this);

                    digitalData.promotion=[];
                    digitalData.promotion.push({
                    'promotionInfo': {
                       'promotionId':'PROMO ID',
                       'promotionName':'PROMO NAME',
                       'promotionCreative':'PROMO CREATIVE',
                       'position':4
                    }}

                    );
                    ev.eventInfo={
                      'type':ctConstants.promotionClick,
                     };
                    ev.attributes={'nonInteraction':1};
                    ev.category ={'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                }
               /* "panelAnimationEnd": function() {
                    //analytics for campaignPanelAnimationEnd
                },
                "panelAnimationStart": function() {
                    //analytics for campaignpPnelAnimationStart
                }*/
            }
        }
            }

        });
    });

    return CampaignOfferFlowWidget;
});
