/*global define*/

define(['TweenMax', 'commonService', 'socialTabService', 'socialSharing'], function(TweenMax) {
    'use strict';

    var FeaturedInstagramPost = IEA.module('UI.featured-instagram-post', function(featuredInstagramPost, app, iea) {
		
		var commonService = null,
        socialTabService = null,
		socialService = null;
			
        _.extend(featuredInstagramPost, {
			
			defaultSettings: {
                featuredInstagramList: '.js-featured-instagram-post__wrapper',
                featuredInstagramItem: '.c-featured-instagram-post__item',
                template: 'partials/social-feed-post.hbss',
                pageStartIndex: 1
            },
			
            /*********************PUBLIC METHODS******************************************/
			
            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
				
				commonService = new IEA.commonService(); // creates new instance for common service
                socialTabService = new IEA.SocialTabs(); // creates new instance for Social tab service

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
				
				this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
				this.triggerMethod('beforEnable');

				var self = this,
                settings = self.defaultSettings;
                self.componentJson = self.getModelJSON().featuredInstagramPost;
                self.startIndex = settings.pageStartIndex;
                self.elm = self.$el;

                // render social gallery on page load
                self._getJsonData();

                // reset quick panel on resizing
                socialTabService.resetPanelOnResize(self.elm, settings);
				
				//Social Sharing
				self._socialSharing();
				
                this.triggerMethod('enable'); 
            },


            /**************************PRIVATE METHODS******************************************/          
			
			// get json data from service
            _getJsonData: function() {
                var self = this,
                    options = {};

                options.componentJson = self.componentJson;
                options.startIndex = self.startIndex;

                options.queryParams = socialTabService.setDataParameters(options);
                options.servletUrl = self.componentJson.retrivalQueryParams.requestServletUrl;
                
                if (!$('.local-env').length) {
                     options.isTypePost = true;
                } else {
                    options.queryParams = '?';
                }

                if (options.queryParams !== '') {
                    options.onCompleteCallback = function(data) {
                        self._generateSocialList(data);
                    };

                    commonService.getDataAjaxMethod(options);
                }   
            },

            // get json data from service
            _generateSocialList: function(data) {
                var self = this,
                settings = self.defaultSettings,
                serviceData = $.parseJSON(data),
                template = self.getTemplate('social-feed-post', settings.template),
                combinedJson = null,
                numberOfPosts = self.componentJson.retrivalQueryParams.numberOfPosts;

                combinedJson = {
                    'serviceData': serviceData.responseData,
					'staticJson': self.componentJson
                }

                $(settings.featuredInstagramList).append(template(combinedJson));    
            },
			
			//Social Sharing
			_socialSharing: function(){
				var options = {};
                options.socialSharingMenu = $('.c-social-sharing-stack');
                options.socialSharing = $('.js-social-sharing');
                socialService = new IEA.socialsharing(); // creates new instance
                socialService.enableSocial(options);
			}

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
            */

        });
    });

    return FeaturedInstagramPost;
});