/*global define*/

define(['TweenMax', 'jquery.hammer', 'commonService', 'socialTabService'], function(TweenMax) {
    'use strict';

    var SocialGalleryTabV2 = IEA.module('UI.social-gallery-tab-v2', function(socialGalleryTabV2, app, iea) {

        var commonService = null,
        socialTabService = null;

        _.extend(socialGalleryTabV2, {

            defaultSettings: {
                socialGalleryList: '.js-social-gallery-tab-v2__list',
                socialGalleryItem: '.js-social-gallery-tab-v2__item',
                loadmoreWrapper: '.js-social-gallery-tab-v2__loadmore',
                loadmore: '.js-btn__seeall',
                preloader: '.o-preloader',
                preloaderContent: '.js-preloader__content',
                template: 'social-gallery-tab-v2/partial/social-gallery-list.hbss',
                quickPanelWrapper: '.js-social-gallery-tab-v2__panel',
                thumbnail: '.js-social-gallery-tab-v2__thumbnail',
                animateSpeed: 1000,
                pageScroll: false,
                pageStartIndex: 1,
                pageNum: 1,
                startPageNum: 1,
                itemMargin: 30,
                prevButton: '.js-social-gallery__arrow-prev',
                nextButton: '.js-social-gallery__arrow-next',
                closeButton: '.js-btn-social-gallery__close',
                disableHotKey: true,
                pagination: '.js-social-gallery-tab-v2-pagination',
                paginationTemplate: 'pagination/defaultView.hbss',
                paginationBtn: '.js-btn-pagination',
                isPageLoad: true
            },

            events: {
                'click .js-btn__seeall' : '_showAllHandler', //show results on see all button click
                'click .js-social-gallery-tab-v2__thumbnail' : '_displayQuickPanel', // show quick panel
                'click .js-btn-social-gallery__close' : '_closePanel', //hide quick panel
                'click .js-social-gallery-tab-v2__item.active' : '_closePanelonActiveSlide', //hide quick panel
                'click .js-social-gallery__arrow' : '_navigateToItem', // navigate quick panel through next/prev button
                'click .js-btn-pagination' : '_paginationHandler',
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);

                commonService = new IEA.commonService(); // creates new instance for common service
                socialTabService = new IEA.SocialTabs(); // creates new instance for common service

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },
            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                var self = this;

                self.settings = self.defaultSettings;
                self.componentJson = self.getModelJSON().socialGalleryTabV2;
                self.startIndex = self.settings.pageStartIndex;
                self.pagination = self.settings.pagination;
                self.isPageLoad = self.settings.isPageLoad;
                self.pageNum = self.settings.pageNum;
                self.elm = self.$el;

                // render social gallery on page load
                self._getJsonData();

                if (self.componentJson.pagination.paginationType === 'infiniteScroll') {
                    // show/hide show more button on page scroll
                    self._handleScroller();
                }

                // reset quick panel on resizing
                socialTabService.resetPanelOnResize(self.elm, self.settings);

                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/

            // get json data from service
            _getJsonData: function() {
                var self = this,
                    options = {};

                // fixed aggregation
                if(self.componentJson.generalConfig.aggregationMode === 'manual') {
                    self.totalResults = self.componentJson.socialGalleryPosts.length;
                    options.results = [];
                    for(var i = self.componentJson.pagination.itemsPerPage*(self.pageNum-1); i < self.componentJson.pagination.itemsPerPage*self.pageNum; i++){
                        (self.componentJson.socialGalleryPosts[i]) && options.results.push(self.componentJson.socialGalleryPosts[i]);
                    }
                    self._animateResultsFadeIn();
                    self._generateSocialList(JSON.stringify(options));
                    return;
                }

                if(self.componentJson.generalConfig.aggregationMode === 'automatic') {
                    if (options.queryParams !== '') {
                        // automatic aggregation
                        options.queryParams = socialTabService.setQueryParameters(self.componentJson, self.startIndex);
                        options.servletUrl = self.componentJson.retrivalQueryParams.requestServletUrl;
                        options.onCompleteCallback = function(data) {
                            var actualData = $.parseJSON(data);

                            if (self.componentJson.limitResultsTo !== 0 && actualData.totalResults > self. componentJson.limitResultsTo) {
                                self.totalResults = self.componentJson.limitResultsTo;
                            } else {
                                self.totalResults = actualData.totalResults;
                            }

                            var loopEndValue = (self.totalResults >= self.componentJson.pagination.itemsPerPage*self.pageNum) ? self.componentJson.pagination.itemsPerPage : (self.totalResults % self.componentJson.pagination.itemsPerPage);

                                options.results = [];
                                options.totalResults = '';

                                for (var i = 0; i < loopEndValue; i++){
                                    options.results.push(actualData.results[i]);
                                }
                                options.totalResults = actualData.totalResults;

                            self._animateResultsFadeIn();
                            self._generateSocialList(JSON.stringify(options));
                        };

                        commonService.getDataAjaxMethod(options);
                    }
                }
            },

            // get json data from service
            _generateSocialList: function(data) {
                var self = this,
                solrData = $.parseJSON(data),
                queryParamJson = self.componentJson.retrivalQueryParams,
                template = self.getTemplate('social-gallery-list', self.settings.template),
                paginationTemplate = self.getTemplate('pagination', self.defaultSettings.paginationTemplate),
                combinedJson = null,
                hashTags = '';

                self.totalResults = self.totalResults || solrData.totalResults;
                self.itemsPerPage = self.componentJson.pagination.itemsPerPage;

                var totalPages = Math.ceil(self.totalResults/self.itemsPerPage),
                        paginationList = [];

                    for(var i=0; i < totalPages; i++) {
                        paginationList.push(i);
                    }

                if ($(queryParamJson.hashTags).length > 0) {
                    $(queryParamJson.hashTags).each(function(key, val) {
                        if (key === 0) {
                            hashTags += val.Id;
                        } else {
                            hashTags += '|' +val.Id;
                        }
                    });
                }

                combinedJson = {
                    'componentJson': self.componentJson,
                    'solrData': solrData,
                    'pageNum': self.isPageLoad ? self.settings.pageNum : self.pageNum,
                    'totalPages': Math.ceil(self.totalResults/self.itemsPerPage),
                    'hashTags': hashTags,
                    'paginate': paginationList,
                    'startIndex': self.isPageLoad ? self.settings.pageStartIndex : self.startIndex
                };

                setTimeout( function() {
                    $(self.settings.loadmoreWrapper).find(self.settings.preloader).addClass('hidden');

                    // Check paginiation type
                    if (self.componentJson.pagination.paginationType === 'infiniteScroll') {
                        $(self.settings.socialGalleryList).append(template(combinedJson));
                    } else {
                        $(self.settings.socialGalleryList).html(template(combinedJson));
                    }

                }, self.settings.animateSpeed);

                // render pagination
                if (self.isPageLoad) {
                    $(self.settings.pagination).append(paginationTemplate(combinedJson));
                }
            },

            // event handler for load more click
            _showAllHandler: function(evt) {
                var self = this,
                itemsPerPage = self.componentJson.pagination.itemsPerPage,
                $this = $(evt.target);

                evt.preventDefault();
                self.pageNum++;

                self.startIndex = self.startIndex + itemsPerPage;

                $this.fadeOut(self.settings.animateSpeed);
                self.isPageLoad = false;
                self.settings.pageScroll = true;
                $(self.settings.loadmoreWrapper).find(self.settings.preloader).removeClass('hidden');

                // render search listing
                self._getJsonData();
            },

            // animate resuls with fade In effect
            _animateResultsFadeIn: function() {
                TweenMax.to($(this.settings.socialGalleryList), 0.8, {autoAlpha: 1, top: 0, delay: 0.2});
                $(this.settings.preloader).fadeOut(this.settings.animateSpeed);
            },

            // animate resuls with fade Out effect
            _animateResultsFadeout: function() {
                TweenMax.to($(this.settings.socialGalleryList), 0.8, {autoAlpha: 0, top: '30px', delay: 0.2});
                $(this.settings.preloader).fadeIn(this.settings.animateSpeed);
            },

            _closePanel: function(evt) {
                evt.preventDefault();

                var _this = $(evt.target);

                socialTabService.closePanel(this.elm, this.settings, _this);
            },

            _closePanelonActiveSlide: function(evt) {
                evt.preventDefault();

                var _this = $(evt.target);

                socialTabService.closePanel(this.elm, this.settings, _this);
            },

            _displayQuickPanel: function(evt) {
                evt.preventDefault();

                var _this = $(evt.target),
                    self = this,
                    active = 'active',
                    $currentItem = _this.parents(self.settings.socialGalleryItem),
                    currentIndex = $currentItem.data('index'),
                    $currentQuickPanel = $(self.settings.quickPanelWrapper+'[data-index='+currentIndex+']');

                socialTabService.setActiveParams(self.elm, self.settings, parseInt(currentIndex));

                self.elm.find(self.settings.socialGalleryItem).each(function (index, el) {
                    var panelHeight = 0,
                        cols = 3,
                        row = currentIndex - currentIndex % cols;

                    if (index >= row && index <= row + (cols - 1)) {
                        panelHeight = $currentQuickPanel.height();
                    }
                    if (panelHeight !== 0) {
                        $(el).css("margin-bottom", panelHeight+ self.settings.itemMargin*2);
                    }
                });

                $currentItem.addClass(active);
                $currentQuickPanel.addClass(active).css('top', $currentItem.position().top + $currentItem.height() + self.settings.itemMargin);

                TweenMax.to(window, 0.5, {scrollTo: {y: $currentItem.offset().top + $currentItem.height()/2}});

                self.settings.disableHotKey = false;

                //keyword keys handler
                this._navigateThroughKeys($currentItem, currentIndex, $(self.settings.socialGalleryItem).length);
            },

            _navigateToItem: function(evt) {
                evt.preventDefault();

                var self = this,
                    _this = $(evt.target),
                    active = 'active',
                    $currentItem = _this.parents(self.settings.quickPanelWrapper),
                    currentIndex = $currentItem.data('index');

                if (_this.data('role') === 'next') {
                    currentIndex = currentIndex + 1;
                } else if (_this.data('role') === 'prev') {
                    currentIndex = currentIndex - 1;
                }
                $(self.settings.socialGalleryItem+'[data-index='+currentIndex+'] '+self.settings.thumbnail).click();
            },

            _navigateThroughKeys: function(item, index, last) {
                var self = this,
                    $close = item.find(self.settings.closeButton);

                // Hotkeys for navigation
                $(document).off('keyup').on('keyup', function(e) {
                    if (e.keyCode === 37 && !self.settings.disableHotKey && index > 0) {
                         socialTabService.prevItem(self.settings, index);
                    }
                    if (e.keyCode === 39 && !self.settings.disableHotKey && index < last) {
                        socialTabService.nextItem(self.settings, index);
                    }
                    if (e.keyCode === 27) {
                        self._setParams(self.settings.socialGalleryItem, self.settings.quickPanelWrapper, index);
                        TweenMax.to(window, 0.5, {scrollTo: {y: item.offset().top - item.height()/2}});
                    }
                });

                // Touch gestures with hammerjs for navigation
                self.$el.hammer().off('swipeleft').on('swipeleft', function(e) {
                    if (!self.settings.disableHotKey) {
                        socialTabService.prevItem(self.settings, index);
                    }
                }).off('swiperight').on('swiperight', function(e) {
                    if (!self.settings.disableHotKey) {
                        socialTabService.nextItem(self.settings, index);
                    }
                });
            },

            /**
             * _paginationHandler
             * @method _paginationHandler
             * @return
            */
            _paginationHandler: function(evt) {
                var self = this,
                    $this = $(evt.currentTarget);

                 if ($this.hasClass('o-btn--disabled') || $this.hasClass('o-btn--active')) {
                    evt.preventDefault();
                    return false;
                }

                $(self.preloader).height($(self.listing).height()).removeClass('hidden').show();
                TweenMax.to(window, 0.5, {scrollTo: {y: $(self.$el).offset().top}});

                self.isPageLoad = false;

                //pagination handler common service
                commonService.pagination(self.defaultSettings.paginationBtn, $this, self);

                // render search listing
                self._getJsonData();
            },

            /**
            @description: handle load more click event
            @method: _handleScroller
            @return: {null}
            **/

            _handleScroller: function() {
                var self = this;

                document.addEventListener('scroll', function (event) {
                    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop,
                    $parentElm = (self.$el.parents('.component-wrapper').next('footer').length) ? self.$el.next('footer') : self.$el.parents('.component-wrapper'),
                    $nextElm = (self.$el.next('footer').length) ? self.$el.next('footer') : self.$el.next('div[data-role]'),
                    $scrollElm = (self.$el.parents('.component-wrapper').length) ? $parentElm : $nextElm,
                    pageOffsetBottom = document.body.scrollHeight - $scrollElm.offset().top,
                    scrollOffset = scrollTop + window.innerHeight + pageOffsetBottom - 100;

                    if ((document.body.scrollHeight <= scrollOffset) && (self.pageNum >= 2) && self.settings.pageScroll === true) {
                        self.settings.pageScroll = false;

                        if(self.totalResults >= self.componentJson.pagination.itemsPerPage*self.pageNum) {
                            $(self.settings.loadmore).click();
                        }
                    }
                });
            }

        });
    });

    return SocialGalleryTabV2;
});