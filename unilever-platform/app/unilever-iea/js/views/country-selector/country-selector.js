/*global define*/

define(['accordion'], function() {
    'use strict';

    var CountrySelector = IEA.module('UI.country-selector', function(countrySelector, app, iea) {

        _.extend(countrySelector, {
            
            defaultSettings: {
                headerContainer: '.c-expandcollapse__header',
                speed: 400
            },

            events: {
                'click a.c-expandcollapse__link': '_callExpandCollapse'
            },
            
            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                // functions to be wrirren here

                this.triggerMethod('enable'); 
            },
            
            _callExpandCollapse: function(evt, options) {
                var service = new IEA.accordion(); // creates new instance
                service._handleExpandCollapse(evt, options);
            }


            /**************************PRIVATE METHODS******************************************/          

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */

        });
    });

    return CountrySelector;
});
