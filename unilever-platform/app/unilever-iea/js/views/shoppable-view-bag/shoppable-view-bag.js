/*global define*/

define(function() {
    'use strict';

    var ShoppableViewBag = IEA.module('UI.shoppable-view-bag', function(shoppableViewBag, app, iea) {

        _.extend(shoppableViewBag, {

            /*********************PUBLIC METHODS******************************************/

            oldProductID: '',
            oldProductName: '',
            oldProductPrice: '',
            oldProductQuantity: '',
            oldProductVariant: '',
            oldProductMerchantId: '',

            initialize: function(options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function() {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function() {
                this.triggerMethod('beforEnable');
                var self = this,
                    topics = {};
                self.shoppableBagData = self.getModelJSON().shoppableViewBag;

                jQuery.Topic = function(t) {
                    var e, i = t && topics[t];
                    return i || (e = jQuery.Callbacks(),
                            i = {
                                publish: e.fire,
                                subscribe: e.add,
                                unsubscribe: e.remove
                            },
                            t && (topics[t] = i)),
                        i
                };
                if (typeof self.shoppableBagData.shoppableConfig !== "undefined" && typeof UnlvCart !== "undefined") {
                    if (self.shoppableBagData.shopNow.enabled === "true" && self.shoppableBagData.shopNow.serviceProviderName === "shoppable") {
                        self._shoppingCart(self.shoppableBagData);
                    }
                }

                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/

            /*shopping cart functions */
            _shoppingCart: function(shoppableCart) {
                window.site_language = shoppableCart.market;
                var self = this;


                $('#viewMyBag').hide();
                $('.o-shoppingCart a').off().on('click', function() {
                    var cartCounter = parseInt($(this).text());
                    if (cartCounter > 0) {
                        var mql = window.matchMedia("screen and (max-width: 1024px)");
                        if (mql.matches) { // if media query matches
                            $('.o-shoppingCart a').attr("href", shoppableCart.shoppableConfig.viewBagLink);
                        } else {
                            self.buildViewBag(shoppableCart, true);
                            $('#viewMyBag').show();
                            if (typeof digitalData !== "undefined" && typeof ctConstants !== "undefined") {
                                var ev = {};
                                ev.eventInfo = {
                                    'type': ctConstants.trackEvent,
                                    'eventAction': ctConstants.miniBag,
                                    'eventLabel': 'Open'
                                };
                                ev.category = { 'primaryCategory': ctConstants.custom };
                                digitalData.event.push(ev);
                            }
                        }
                    } else {
                        console.log('Cart is empty. Add items in cart to proceed !');
                    }
                });

                // close view bag click
                $('#viewMyBag').find('#closeViewBag').off().on('click', function() {
                    $('#viewMyBag').siblings('.arrowLink').hide();
                    $('#viewMyBag').hide();
                    if (typeof digitalData !== "undefined" && typeof ctConstants !== "undefined") {
                        var ev = {};
                        ev.eventInfo = {
                            'type': ctConstants.trackEvent,
                            'eventAction': ctConstants.miniBag,
                            'eventLabel': 'Close'
                        };
                        ev.category = { 'primaryCategory': ctConstants.custom };
                        digitalData.event.push(ev);
                    }
                });
                // close button click
                $('#viewMyBag').find('.js-close-btn').off().on('click', function() {
                    if (typeof digitalData !== "undefined" && typeof ctConstants !== "undefined") {
                        var ev = {};
                        ev.eventInfo = {
                            'type': ctConstants.trackEvent,
                            'eventAction': ctConstants.continueShopping,
                            'eventLabel': 'Continue Shopping'
                        };
                        ev.category = { 'primaryCategory': ctConstants.custom };
                        digitalData.event.push(ev);
                    }
                    $('#viewMyBag').hide();
                });

                var mql = window.matchMedia("screen and (min-width: 1025px)")
                if (mql.matches) { // if media query matches
                    $(document.body).on('click', '.o-shoppingCart > a', function(e) {
                        return false;
                    });
                }
                if ($('.o-shoppingCart').length > 0) {
                    UnlvCart.buildItemList();

                    $.Topic('ADD_TO_CART').subscribe(self.UpdateCartCount);
                }

                if ($('.js-shopping-bag').length > 0) {
                    self._viewBag(self.shoppableBagData);
                }
            },
            UpdateCartCount: function(json) {

                var result, count;
                if (typeof json == 'boolean') {
                    result = UnlvCart.get_cart(g_opt_obj.token, UnlvCart.GetCartId());
                    count = result.cart.qty;

                } else {
                    json = $.parseJSON(json);
                    count = json.cart.cart.qty;
                    $('#shoppable_magic_v1_pdp_container_close_btn').trigger('click');
                    var mql = window.matchMedia("screen and (min-width: 1025px)")
                    if (mql.matches) { // if media query matches
                        // UnlvCart.buildViewBag(json);
                    }
                }
                if (count > 0) {
                    $('.o-shoppingCart > a span').html(count);
                } else {
                    $('.o-shoppingCart > a span').html(0);
                }
            },

            viewBagData: function(data, shoppableCart) {
                var viewBagList = "<ul>";

                if (data.hasOwnProperty('merchants')) {
                    $.each(data.merchants, function(index, value) {
                        $.each(value.items, function(i, val) {
                            viewBagList += '<li>';
                            viewBagList += '<div class="product_field">';
                            viewBagList += '<img class="product_image img-responsive" src="' + val.images[0] + '" />';
                            viewBagList += '</div>';
                            viewBagList += '<div class="detail">' + val.name + '</div>';
                            viewBagList += '<div class="product_quantity">' + val.qty + ' </div>';
                            viewBagList += '<div class="product_total">' + shoppableCart.shoppableConfig.currencySymbol + parseFloat(val.subtotal).toFixed(2) + '</div>';
                            viewBagList += '</li>';
                        });
                    });
                }
                viewBagList += "</ul>";
                viewBagList += '<div class="product-calculation-summary">';

                if (data.hasOwnProperty('cart')) {

                    viewBagList += '<div class="product_field">' + data.cart.qty + ' ITEMS</div>';
                    viewBagList += '<div class="product_quantity">' + shoppableCart.staticText.subTotalText + '</div>';
                    viewBagList += '<div class="product_total">' + shoppableCart.shoppableConfig.currencySymbol + '' + parseFloat(data.cart.subtotal).toFixed(2) + '</div>';

                }
                viewBagList += '</div>';
                $('.viewMyBag .listing ul').html('<li class="loader"> </li>');
                $('.viewMyBag .listing').html(viewBagList);
            },
            // pdp page
            buildViewBag: function(datajson) { 
                var data = "",
                    myVar = "",
                    self = this,
                    activeToken = self.shoppableBagData.shoppableConfig.apiToken;

                if (datajson) {
                    data = UnlvCart.get_cart(activeToken, UnlvCart.GetCartId());
                    self.viewBagData(data, self.shoppableBagData);

                } else {
                    var newcount = datajson.cart.cart.qty;
                    console.log('new json= ' + newcount);

                    data = UnlvCart.get_cart(activeToken, UnlvCart.GetCartId());

                    var localcount = data.cart.qty;
                    console.log('loacl json= ' + localcount);


                    if (newcount !== localcount) {

                        myVar = setInterval(function() {
                            data = UnlvCart.get_cart(activeToken, UnlvCart.GetCartId());

                        }, 1000);

                    } else {
                        self.viewBagData(data, self.shoppableBagData);
                        clearInterval(myVar);
                    }
                }

                if ($('.viewMyBag .listing ul li').length == 0) {
                    $('.viewMyBag .listing').html('<ul class="no-product-added"><li class="no-product-added-list"><h5>' + self.shoppableBagData.staticText.noProductText + '</h5></li></ul><div class="product-calculation-summary"><div class="product_field">' + data.cart.qty + ' ITEMS</div><div class="product_quantity">' + self.shoppableBagData.staticText.subTotalText + '</div><div class="product_total">' + self.shoppableBagData.shoppableConfig.currencySymbol + '' + parseFloat(data.cart.subtotal).toFixed(2) + '</div></div>');
                    $('.viewMyBag .js-viewbag-btn').addClass('disabled');
                }
            },


            /* view bag functions */
            _viewBag: function(shoppableBagData) {
                var self = this,
                    result = UnlvCart.get_cart(UnlvCart.activeToken(), UnlvCart.GetCartId());
                $('.shoppable-grid-items').html('').append(this.CreateTableRows(result, UnlvCart.activeToken(), UnlvCart.GetCartId(), shoppableBagData));

                $('.shoppable-grid-items').off().on('click', '.js-editItem', function(event) {
                    event.stopPropagation();
                    var sku = $(this).data('sku');
                    self.get_merchant_products('UPC', sku, self.buildEdit);
                    $(this).parents('li').addClass('overlEdit');
                    self.oldProductID = $(this).parents('.list-items').data('upc');
                    self.oldProductName = $(this).parents('.list-items').find('.detail a').attr('title');
                    self.oldProductPrice = $(this).parents('.list-items').find('.product_price').text();
                    self.oldProductQuantity = $(this).parents('.list-items').find('.product_quantity .qty-val').text();
                    self.oldProductVariant = $(this).parents('.list-items').find('.size').text();
                    self.oldProductMerchantId = $(this).parents('.list-items').find('.merchant_field .retailerName').data('merchantid');
                });
                $('.shoppable-grid-items').on('click', '.js-removeItem', function(event) {
                    event.stopPropagation();
                    var token = $(this).data('token'),
                        cart = $(this).data('cartid'),
                        sku = $(this).data('sku');
                    if (typeof digitalData !== "undefined" && typeof ctConstants !== "undefined") {
                    var ev = {};
                    digitalData.product = [];
                    digitalData.product.push({
                        'productInfo': {
                            'productID': $(this).parents('.list-items').data('upc'),
                            'productName': $(this).parents('.list-items').find('.detail').text(),
                            'price': $(this).parents('.list-items').find('.product_price').text(),
                            'brand': shoppableBagData.shoppableConfig.campaign,
                            'quantity': $(this).parents('.list-items').find('.product_quantity .qty-val').text()
                        },
                        'category': {
                            'primaryCategory': ''
                        },
                        'attributes': {
                            'productVariants': $(this).parents('.list-items').find('.size').text(),
                            'listPosition': ''
                        }
                    });
                    ev.eventInfo = {
                        'type': ctConstants.removecart
                    };
                    ev.category = { 'primaryCategory': ctConstants.custom };
                    digitalData.event.push(ev);
                }
                    self.deleteItem(token, cart, sku, self.removeCallback);
                });
                // cancel click
                $(document.body).off().on('click', '.VirtualProductEdit .cancel', function() {
                    self.CloseOverlay();
                });

                // Save button click
                $(document).off().on('change', ".cmbmerchant, .cmbsize", function() {
                    self.updateProductPrice();
                });
                // Analytics on click of checkout button
                $(document).on('click', ".js-checkout-btn", function() {
                    var data = UnlvCart.get_cart(UnlvCart.activeToken(), UnlvCart.GetCartId());

                    if (typeof digitalData !== "undefined" && typeof ctConstants !== "undefined") {
                        var ev = {};
                        digitalData.product = [];
                        $.each(data.merchants, function(index, value) {
                            $.each(value.items, function(i, val) {
                                var productDetail = {
                                    productInfo: {},
                                    category: {},
                                    attributes: {}
                                }
                                productDetail.productInfo['productID'] = val.upc;
                                productDetail.productInfo['productName'] = val.name;
                                productDetail.productInfo['price'] = shoppableBagData.shoppableConfig.currencySymbol + val.price;
                                productDetail.productInfo['brand'] = shoppableBagData.shoppableConfig.campaign;
                                productDetail.productInfo['quantity'] = val.qty;
                                productDetail.category['primaryCategory'] = '';
                                productDetail.attributes['productVariants'] = val.size;
                                productDetail.attributes['listPosition'] = index;

                                digitalData.product.push(productDetail);
                            });

                        });
                        ev.eventInfo = {
                            'type': ctConstants.checkoutenhanced
                        };
                        ev.category = { 'primaryCategory': ctConstants.custom };
                        digitalData.event.push(ev);
                    }
                });
            },

            removeCallback: function(merchantProducts, self) {
                self.buildItemList();

            },
            updateCallback: function(merchantProducts, selected_sku, prodSkuId, token, cartid, self) {
                if (selected_sku != prodSkuId) {
                    self.deleteItem(token, cartid, prodSkuId, self.removeCallback);
                } else {
                    self.buildItemList();
                }
                self.CloseOverlay();
            },
            deleteItem: function(token, cart_id, sku, delcallback) {

                var apiUrl = framesApiCall + '/v2/token/' + UnlvCart.activeToken() + "/cart/" + cart_id + "/delete/" + sku,
                    xhr = createCORSRequest('GET', apiUrl),
                self = this;
                if (!xhr) {
                    console.log('CORS not supported');
                    return;
                }
                xhr.onload = function() {

                    var response_text = xhr.responseText;
                    response_text = JSON.parse(response_text);
                    console.log(response_text);
                    delcallback(response_text, self);
                };
                xhr.send();
            },

            CreateTableRows: function(data, token, cartId, shoppableBagData) {
                var self = this,
                    tableHtml = "<ul>";
                if (data.hasOwnProperty('merchants')) {
                    $.each(data.merchants, function(index, value) {
                        $.each(value.items, function(i, val) {
                            var retailerClass = (value.name).split('.')[0].toLowerCase();
                            tableHtml += '<li class="list-items" data-upc=' + val.upc + '>';
                            tableHtml += '<div class="product_field col-sm-2 col-xs-5"><img class="img-responsive" src="' + val.images[0] + '" /><div class="action"> <a class="js-editItem" data-sku=' + val.sku + '  \>' + shoppableBagData.staticText.editText + '</a> | <a class="js-removeItem" data-token=' + token + ' data-cartId=' + cartId + ' data-sku=' + val.sku + ' \>' + shoppableBagData.staticText.removeText + '</a></div></div>';

                           tableHtml += '<section class="col-sm-10 col-xs-7">'
                            tableHtml += '<div class="detail col-sm-3" id ="detail_' + val.sku + '"><a title="' + val.name + '" href="' + val.url2 + '"><p>' + val.name + '</p></a></div>'
                            tableHtml += '<div class="merchant_field col-sm-4" id="' + value.id + '" ><span class="txt">' + shoppableBagData.staticText.retailerText + ' </span><span class="retailerName ' + retailerClass + '" data-merchantId="' + value.id + '">' + value.name + '</span></div>';
                            tableHtml += '<div class="size col-sm-1"><span class="txt">' + shoppableBagData.staticText.sizeText + ' </span><span class="actual_size">' + val.size + '</span></div>';
                            tableHtml += '<div class="product_quantity col-sm-1"><span class="txt">' + shoppableBagData.staticText.quantityShortText + ' </span><span class="qty-val">' + val.qty + '</span></div>';
                            tableHtml += '<div class="product_price col-sm-1" id ="price_' + val.sku + '"><span class="txt">' + shoppableBagData.staticText.priceText + ': </span><span class="price">' + shoppableBagData.shoppableConfig.currencySymbol + val.price + '</span></div>';
                            tableHtml += '<div class="product_total col-sm-1"><span class="txt">' + shoppableBagData.staticText.totalText + ': </span><span class="total"' + shoppableBagData.shoppableConfig.currencySymbol + parseFloat(val.subtotal).toFixed(2) + '</span></div>';
                            tableHtml += '</section>'
                        });
                    });
                } else {
                    tableHtml += '<li class="no-product-added-list"><h5>' + shoppableBagData.staticText.noProductText + '</h5></li>';
                    $(".js-shopping-bag .js-checkout-btn").addClass('disabled');
                    $(".checkout-wrapper .js-checkout-btn").addClass('disabled');
                }
                if (data.hasOwnProperty('cart')) {
                    $('.checkout-wrapper .subtotal .value').text(shoppableBagData.shoppableConfig.currencySymbol + parseFloat(data.cart.subtotal).toFixed(2));


                }
                return tableHtml + "</ul>";
            },

            buildItemList: function() {

                var self = this,
                    result = UnlvCart.get_cart(UnlvCart.activeToken(), UnlvCart.GetCartId());
                $('.shoppable-grid-items').html('').append(self.CreateTableRows(result, UnlvCart.activeToken(), UnlvCart.GetCartId(), self.shoppableBagData));
                UnlvCart.UpdateCartCount(true);
            },
            buildEdit: function(merchantProducts, self) {

                var editPopupHeader = $('.col-heading').html(),
                    source = '<div class="VirtualProductEdit"><div id="virtualProduct-header"><span class="clr">header</span></div>' + editPopupHeader + '<ul><li class="virtualProduct-items"><div class="product_field col-sm-2"><img class="product_image" src="{{current_object.product_objects.[0].[0].variance.[0].image.[0]}}"><div class="action"><div class="updateCancel"><a id="shoppable_magic_add_to_cart_btn" class="update">Save</a> | <a class="cancel">Cancel</a></div></div></div><section><div class="detail col-sm-2"><p>{{current_object.product_objects.[0].[0].brand}} - {{current_object.product_objects.[0].[0].name}}</p></div><div class="merchant_field col-sm-3"><span class="txt">Retailer:</span><select id="shoppable_magic_select_merchant">{{#each handles }}<option value="{{product_objects.[0].[0].merchant_id}}" variance-number="{{@index}}">{{product_objects.[0].[0].merchant}}</options>{{/each}}</select></div><div class="size col-sm-1"><span class="txt">Size:</span>{{#if current_object.product_objects.[0].[0].variance.[1]}}<select id="shoppable_magic_select_color_size" class="cmbsize" >{{#each current_object.product_objects.[0].[0].variance }}<option value="{{id}}" price="{{price}}" variance-number="{{@index}}">{{size}}</option>{{/each}}</select>{{else}}<select id="shoppable_magic_select_color_size">{{#each current_object.product_objects.[0].[0].variance }}<option value="{{id}}" price="{{price}}" variance-number="{{@index}}"> {{size}}</option>{{/each}}</select>{{/if}} </div><div class="product_quantity col-sm-1"><span class="txt">Qty:</span><select class="cmbmerchant">{{#create_options}}<option>{{this}}</option>{{/create_options}}</select></div><div class="product_price col-sm-1">$ 00</div><div class="product_total col-sm-1">$ 00</div></section></li></ul></div>';


                var template = Handlebars.compile(source),
                    data = {
                        handles: merchantProducts,
                        current_object: merchantProducts[0]
                    },
                    result = template(data);

                // open overlay
                self.OpenOverlay();

                // append result
                $('.overlEdit').append(result);

                //  var shoppingbagheader = $('#shopping-bag > #shoppingBag-header').html();

                //$('#virtualProduct-header').append(shoppingbagheader);

                var select_merchant = document.getElementById('shoppable_magic_select_merchant');

                // change function on dropdown
                $('#shoppable_magic_select_merchant').on('change', function() {
                    var selected_m = select_merchant.options[select_merchant.selectedIndex];
                    var variance_number = select_merchant.options[select_merchant.selectedIndex].getAttribute('variance-number');
                    document.getElementsByClassName("product_image")[0].src = merchantProducts[variance_number].product_objects[0][0].variance[0].image[0];
                    var current_object = merchantProducts[variance_number];
                    var price_size_source = '{{#each current_object.product_objects.[0].[0].variance }}' +
                        '<option value="{{id}}" price="{{price}}" variance-number="{{@index}}">{{size}}</option>' +
                        '{{/each}}';
                    var price_size_data = {
                        current_object: current_object
                    };
                    var price_size_template = Handlebars.compile(price_size_source);
                    var price_size_result = price_size_template(price_size_data);
                    document.getElementById('shoppable_magic_select_color_size').innerHTML = price_size_result;
                    self.updateProductPrice();
                });

                // call add to cart
                self.create_add_to_cart_btn(g_opt_obj.token, g_cart_id, data);
            },
            updateProductPrice: function() {
                var _ItemPrice = 0,
                    _ItemCount = 0,
                    self = this;
                _ItemPrice = $('option:selected', $('.overlEdit #shoppable_magic_select_color_size')).attr('price');
                _ItemCount = $('.overlEdit select.cmbmerchant').val();
                $('.overlEdit .VirtualProductEdit .product_price').html('<span class="txt">' + self.shoppableBagData.staticText.priceText + '</span><span class="price">' + self.shoppableBagData.shoppableConfig.currencySymbol + _ItemPrice +'</span>');
                $('.overlEdit .VirtualProductEdit .product_total').html('<span class="txt">' + self.shoppableBagData.staticText.totalText + '</span><span class="total">' + self.shoppableBagData.shoppableConfig.currencySymbol + parseFloat(_ItemPrice * _ItemCount).toFixed(2) +'</span>');
            },

            get_merchant_products: function(idType, skuid, callback) {

                var upcapiUrl = framesApiCall + '/v2/unlv/token/' + g_opt_obj.token + "/product_by_id/" + skuid,
                    xhr = createCORSRequest('GET', upcapiUrl),
                    self = this;
                if (!xhr) {
                    console.warn('CORS not supported');
                    return;
                }
                xhr.onload = function() {
                    var upcCode = '',
                        productapiUrl = '',
                        productid;
                    var response_text = xhr.responseText;
                    response_text = JSON.parse(response_text);
                    if (response_text[0].hasOwnProperty('variance')) {
                        $.each(response_text[0].variance, function(index, value) {
                            if (value.sku == skuid) {
                                upcCode = value.upc;
                            }
                        });
                    }


                    if (self.shoppableBagData.shoppableConfig.upcIdType == "upc_code") {
                        if (g_demo_url) {
                            productapiUrl = framesApiCall + "/v2/unlv/token/" + g_opt_obj.token + "/product/" + upcCode;
                        } else {
                            productapiUrl = framesApiCall + "/v2/unlv/token/" + g_opt_obj.token + "/product/" + upcCode;
                        }
                    } else {
                        productid = response_text[0].part_number.split("-")[1];
                        if (g_demo_url) {
                            productapiUrl = framesApiCall + "/v2/unlv/token/" + g_opt_obj.token + "/product_by_external_id/" + productid + '/brand/' + self.shoppableBagData.shoppableConfig.campaign;
                        } else {
                            productapiUrl = framesApiCall + "/v2/unlv/token/" + g_opt_obj.token + "/product_by_external_id/" + productid + '/brand/' + self.shoppableBagData.shoppableConfig.campaign;
                        }
                    }


                    var product_xhr = createCORSRequest('GET', productapiUrl);
                    if (!product_xhr) {
                        console.log('CORS not supported');
                        return;
                    }
                    product_xhr.onload = function() {
                        var response_text = product_xhr.responseText;
                        response_text = JSON.parse(response_text);
                        var found_products = response_text;
                        callback(found_products, self);
                    };
                    product_xhr.onerror = function() {
                        console.log('Woops, there was an error making the request.');
                    };
                    product_xhr.send();
                };
                xhr.send();
            },
            create_add_to_cart_btn: function(token, cart_id, product) {

                $(".overlEdit .VirtualProductEdit .cmbmerchant").val($(".overlEdit .product_quantity").text());
                var merchant_value = $(".overlEdit .merchant_field").attr('id'),
                    brand_value = $(".overlEdit .detail").attr('id'),
                    brand_price = $(".overlEdit .product_price").attr('id'),
                    self = this,
                    brand_name = $("#" + brand_value + ' p').text(),
                    price_name = 'price_' + brand_price;

                $('.virtualProduct-items .detail p').text(brand_name);
                $(".overlEdit .product_price").html($("#" + price_name).html());

                $.each($("#shoppable_magic_select_merchant option"), function(index, item) {
                    if ($(item).attr('value') == merchant_value) {
                        var valueNew = $('#shoppable_magic_select_merchant option:eq(' + index + ')').val();
                        $('#shoppable_magic_select_merchant').val(valueNew).trigger('change');

                    }
                });

                var quantity_value = $(".list-items.overlEdit .product_quantity .qty-val").text();

                $.each($(".cmbmerchant option"), function(index, item) {

                    $('.cmbmerchant').val(quantity_value).trigger('change');


                });


                var sizeSelection = $('.list-items.overlEdit .product_price').html().split(self.shoppableBagData.shoppableConfig.currencySymbol)[1];
                // var sizeSelection = $('.list-items.overlEdit .product_price').html();

                $.each($('#shoppable_magic_select_color_size option'), function(index, item) {
                    if ($(item).attr('price') == sizeSelection) {

                        var valueNew = $('#shoppable_magic_select_color_size option:eq(' + index + ')').val();
                        $('#shoppable_magic_select_color_size').val(valueNew).change();
                    }
                });

                var prodSkuId = $('#shoppable_magic_select_color_size').val(),
                    prodSelectedQuantity = $('.cmbmerchant').val();

                var btn = $('#shoppable_magic_add_to_cart_btn');
                        $(btn).on('click', function() {
                    var select_color_size = $("#shoppable_magic_select_color_size"),
                        selected_sku = $("#shoppable_magic_select_color_size option:selected").val(),
                        selected_merchant = $("#shoppable_magic_select_merchant");

                    self.put_item(selected_sku, g_opt_obj.token, cart_id, $('.cmbmerchant').val(), self.updateCallback, prodSkuId);

                        var newProductVariant = $('#shoppable_magic_select_color_size').find(':selected').text(),
                        newProductMerchantId = $('#shoppable_magic_select_merchant').val(),
                        newProductQty = $('.VirtualProductEdit .cmbmerchant').val(),
                        newProductPrice = $('.VirtualProductEdit .product_price .price').text();

                        if(newProductMerchantId !== self.oldProductMerchantId || newProductQty !== self.oldProductQuantity || newProductVariant!== self.oldProductVariant) {
                            // remove analytics call before edit product
                            if (typeof digitalData !== "undefined" && typeof ctConstants !== "undefined") {
                            var ev = {};
                            digitalData.product=[];
                            digitalData.product.push(
                            {
                               'productInfo' :{
                               'productID':self.oldProductID,
                               'productName': self.oldProductName,
                               'price':self.oldProductPrice ,
                               'brand': self.shoppableBagData.shoppableConfig.campaign,
                               'quantity': self.oldProductQuantity
                               },
                               'category':{
                               'primaryCategory': ''
                               },                               
                               'attributes':
                               {
                               'productVariants': self.oldProductVariant,
                               'listPosition': ''
                               }
                              });
                            ev.eventInfo={
                              'type':ctConstants.removecart
                            };
                            ev.category ={'primaryCategory':ctConstants.custom};
                            digitalData.event.push(ev);

                            console.log('remove product from cart analytics fired.');

                            // Add anlaytics call for edited product
                            var ev = {};
                            digitalData.product=[];
                            digitalData.product.push(
                            {
                               'productInfo' :{
                               'productID': self.oldProductID,
                               'productName': self.oldProductName,
                               'price': newProductPrice,
                               'brand': self.shoppableBagData.shoppableConfig.campaign,
                               'quantity': newProductQty
                               },
                               'category':{
                               'primaryCategory': ''
                               },                               
                               'attributes':
                               {
                               'productVariants': newProductVariant,
                               'listPosition': ''
                               }

                               
                               });
                            ev.eventInfo={
                              'type':ctConstants.addtoCart
                          };
                            ev.category ={'primaryCategory':ctConstants.custom
                        };

                            digitalData.event.push(ev);

                            console.log('Add product to cart analytics fired.');
                        }
                        }
                });
            },
            put_item: function(selected_sku, token, cart_id, qty, putcallback, prodSkuId) { // shoppingbag page

                var apiUrl = framesApiCall + '/v2/token/' + g_opt_obj.token + "/cart/" + cart_id + "/put/" + selected_sku + "/qty/" + qty + "/idLookupType/" + "sku" + "/productId/" + selected_sku,
                    xhr = createCORSRequest('GET', apiUrl),
                    self = this;
                xhr.onload = function() {
                    var response_text = xhr.responseText;
                    response_text = JSON.parse(response_text);
                    putcallback(response_text, selected_sku, prodSkuId, g_opt_obj.token, cart_id, self);

                };
                xhr.send();
            },
            OpenOverlay: function() {
                $('body').append('<div class="overl"></div>');
                $('.overlEdit div').hide();
            },
            CloseOverlay: function() {
                $('.overl,.updateCancel').remove();
                $('.overlEdit div').show();
                $('.VirtualProductEdit').remove();
                $('.shoppable-grid-items ul li').removeClass('overlEdit');
            }


        });
    });

    return ShoppableViewBag;
});
