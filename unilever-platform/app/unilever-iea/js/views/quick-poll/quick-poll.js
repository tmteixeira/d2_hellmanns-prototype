/*global define*/

define(['formService','eGiftingService'], function() {
    'use strict';
    
    var formService = null,
        eGiftingService = null;

    var QuickPoll = IEA.module('UI.quick-poll', function(quickPoll, app, iea) {

      _.extend(quickPoll, {
      
            defaultSettings: {                
                quickPoll: '.c-quick-poll',
                barChartWrapper: '.c-bar-chart-container', 
                template: 'quick-poll/partial/bar-chart.hbss',
                questionAnswer: '.c-qusAns-answer',
                quickPollWrap: '.c-quick-poll-wrap'
            },

            events: {
                'click .js-btn-export' : '_exportCSV'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                var moduleName = this.moduleName.hyphenToCamelCase();

                formService = new IEA.formService();
                eGiftingService = new IEA.EGifting();
                
                this.opinionEntered = '',
                this.jsonObj = this.getModelJSON()[moduleName];
                this.quickPollOpinionType = this.jsonObj.question.opinionType;
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                var self = this;

                self._renderQuickPoll(); 
                self._renderTemplateOnDate();                   
            },

            _titleCase: function(string) {
                return string.replace( /(^|\s)([a-z])/g , function(m,p1,p2){ return p1+p2.toUpperCase(); });
            },

            _renderTemplateOnDate: function() {
                var self = this,
                    currentDate = new Date(),
                    startDate = new Date(self.jsonObj.question.startDate),
                    endDate = new Date(self.jsonObj.question.endDate),
                    oneDay = 24*60*60*1000, // hours*minutes*seconds*milliseconds
                    diffDays = Math.floor(Math.abs((endDate.getTime() - startDate.getTime())/(oneDay)));

                if (currentDate >= startDate && currentDate < endDate) {
                    self.$el.find(self.defaultSettings.quickPollWrap).html();
                } else {
                    self.$el.find(self.defaultSettings.quickPollWrap).empty();
                }
            },
            
            _exportCSV: function(evt) {                
                var self = this, 
                    getUrl = self.jsonObj.exportButton.downloadCsvURL, 
                    brandNameTitleCase = self._titleCase(self.jsonObj.brandName),
                    exportUrl = getUrl + '?' + 
                    'pollId=' + self.jsonObj.pollId + 
                    '&brand=' + brandNameTitleCase + 
                    '&entity=' + self.jsonObj.entity + 
                    '&locale=' + self.jsonObj.locale;

                window.open(exportUrl);
            },
            
            _renderQuickPoll: function() {
                var storedItem = eGiftingService.hasCookieData('poll-'+ this.jsonObj.pollId);
                
                if (storedItem) {
                    this._loadQuickPollReport();
                } else {
                    this._loadQuickPoll();
                }
            },
            
            _loadQuickPoll: function() {
                var self = this,
                    context = self.$el.find('#c-quick-poll-form-' + self.jsonObj.pollId),
                    submitBtn = context.find('.js-btn-quick-poll');
               
               context.parent(self.defaultSettings.quickPoll).removeClass('hide');
                
                if (self.quickPollOpinionType.toLowerCase() === 'freetext') {
                    eGiftingService.remainingChars('.c-form-textarea');
                    
                    $('.c-form-textarea').on('keyup', function(evt) {
                        self.opinionEntered = event.currentTarget.value;
                        (self.opinionEntered.length > 1 ) ? (submitBtn.removeClass('disabled')) : submitBtn.addClass('disabled');
                    });
                } else {
                    $(self.defaultSettings.questionAnswer).on('click', function(evt) {                            
                        self.$el.find(self.defaultSettings.questionAnswer).removeClass('selected');
                        $(this).toggleClass('selected');
                        self._selectedChoiceId = evt.currentTarget.id;
                        submitBtn.removeClass('disabled');
                    });
                }
                submitBtn.on('click', function(evt) {
                    var context = $(this),
                        formDataToSend = formService.formToJson(context.parents('form'));
                    evt.preventDefault();
                    self._createPollJSONToSend(formDataToSend,context);
                });
            },
            /**
             * createPollJSONToSend function creates the JSON data structure to be send to server, 
             * this function gets called when the user click on the available options component
             * @method _createPollJSONToSend
             * @param json to be decorated 
             * @return 
             */
            _createPollJSONToSend:function(formData, context){
                var freeText = (this.quickPollOpinionType.toLowerCase() === "freetext") ? true : false,
                    obj = this._createOpinionJSON(),
                    formObj = JSON.parse(formData),
                    ajaxUrl = this.jsonObj.ajaxURL;

                delete formObj.question;
                formObj.answers = (freeText)?[]:obj;
                formObj.freeText = (this.quickPollOpinionType.toLowerCase() === "freetext") ? true : false;
                formObj.freeTextAns = (freeText)? obj : "";
                formObj.brand = this._titleCase(this.jsonObj.brandName);
                formObj.entity = this.jsonObj.entity;
                formObj.locale = this.jsonObj.locale;
                
                JSON.stringify(formObj);
                this._submitDataToServer(formObj,context);
            },

            _submitDataToServer:function(formObj, context) {
                var self = this, 
                    postUrl = self.jsonObj.configuration.postPollUrl, 
                    dataToSend = JSON.stringify(formObj), 
                    currentDate = new Date(), 
                    endDate = new Date(self.jsonObj.question.endDate), 
                    oneDay = 24*60*60*1000,
                    days = Math.floor(Math.abs((endDate.getTime() - currentDate.getTime()) / (oneDay))), 
                    timer = days*24*60;              
                    
                $.ajax({
                    url: postUrl,
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: dataToSend,
                    dataType: 'json',
                    error: function() {
                      console.log('could not post data');
                    },
                    success: function(data) {
                        context.parents(self.defaultSettings.quickPoll).addClass('hide');
                        context.parents(self.defaultSettings.quickPoll).next('.c-quick-poll-res').removeClass('hide');
                        
                        var cookieData = {
                            'pollId': formObj.pollId, 
                            'brand': formObj.brand, 
                            'entity': formObj.entity, 
                            'locale': formObj.locale
                        };

                        eGiftingService.createCookie('poll-'+ formObj.pollId, JSON.stringify(cookieData), timer);
                        self._showChart(data);                      
                    }
                });
            },
            
            _loadQuickPollReport:function() {
                var self = this,
                    storedItem = eGiftingService.hasCookieData('poll-'+self.jsonObj.pollId),
                    cookieVal = eGiftingService.readCookie('poll-'+self.jsonObj.pollId), 
                    cookieObj = JSON.parse(cookieVal), dataToSend,
                    context = self.$el.find('#c-quick-poll-form-'+ self.jsonObj.pollId), 
                    getUrl = self.jsonObj.configuration.getPollUrl;

                if (storedItem) {
                    dataToSend = {
                        pollId: cookieObj.pollId, 
                        brand: cookieObj.brand, 
                        entity: cookieObj.entity, 
                        locale: cookieObj.locale
                    }
                }
                
                $.ajax({
                   url: getUrl,
                   type: 'GET',
                   data: dataToSend,
                   dataType: 'json', 
                   error: function() {
                      console.log('could not post data');
                   },
                   success: function(data) {
                      console.log("data saved "+data);
                      self._showChart(data);
                      context.parent(self.defaultSettings.quickPoll).addClass('hide');
                      context.parent(self.defaultSettings.quickPoll).next('.c-quick-poll-res').removeClass('hide');
                   }
                });
            },

            _showChart: function(chartData) {
                var self = this,
                    $barChartWrapper = self.$el.find(self.defaultSettings.barChartWrapper),
                    template = self.getTemplate('bar-chart', self.defaultSettings.template);
                
                $barChartWrapper.append(template(chartData));
            },
            
            /*generate JSON to be sent to server based on opinion type (either freeText or NonFreeText)*/
            _createOpinionJSON: function() {
                var obj;
                if (this.quickPollOpinionType.toLowerCase() === "freetext") {
                    obj = this.opinionEntered;
                } else {
                    var answerOptions = this.jsonObj.question.text;
                    obj = JSON.parse(JSON.stringify(answerOptions));
                    obj = obj.map(function (paramObj) { 
                        paramObj['selected'] = false;
                        delete paramObj['image'];
                        return paramObj;
                    });
                    obj[this._selectedChoiceId].selected = true;
                }
                return obj;
            }
      });    
  });
    return QuickPoll;
});