/*global define*/

define(['commonService', 'socialTabService'], function() {
    'use strict';

    var SocialGalleryCarouselThumbnailsTab = IEA.module('UI.social-gallery-carousel-thumbnails-TAB', function(socialGalleryCarouselThumbnailsTAB, app, iea) {

        var commonService = null,
        socialTabService = null;

        _.extend(socialGalleryCarouselThumbnailsTAB, {

            defaultSettings: {
                socialGalleryCarousel: '.c-social-gallery-carousel-thumbnail__list',
                template: 'partials/social-gallery-thumbnail.hbss',
                pageStartIndex: 1
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);

                commonService = new IEA.commonService(); // creates new instance for common service
                socialTabService = new IEA.SocialTabs(); // creates new instance for common service

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                var self = this;

                self.componentJson = self.getModelJSON().socialGalleryCarouselThumbnailsTAB;
                self.startIndex = self.defaultSettings.pageStartIndex;

                // render social gallery on page load
                self._getJsonData();

                this.triggerMethod('enable');
            },

            /**************************PRIVATE METHODS******************************************/          

            // get json data from service
            _getJsonData: function() {
                var self = this,
                    options = {};

                options.queryParams = socialTabService.setQueryParameters(self.componentJson, self.startIndex);

                options.servletUrl = self.componentJson.retrivalQueryParams.requestServletUrl;

                if (options.queryParams !== '') {
                    options.onCompleteCallback = function(data) {
                        self._generateSocialList(data);
                    };

                    commonService.getDataAjaxMethod(options);
                }
            },

            // get json data from service
            _generateSocialList: function(data) {
                var self = this,
                settings = self.defaultSettings,
                solrData = $.parseJSON(data),
                template = self.getTemplate('social-gallery-thumbnail', settings.template),
                combinedJson = null,
                numberOfPosts = self.componentJson.retrivalQueryParams.numberOfPosts;

                combinedJson = {
                    'solrData': solrData,
                    'cta': self.componentJson.cta,
                    'pageNum':0
                }

                $(settings.socialGalleryCarousel).append(template(combinedJson));                   
            }
        });
    });

    return SocialGalleryCarouselThumbnailsTab;
});
