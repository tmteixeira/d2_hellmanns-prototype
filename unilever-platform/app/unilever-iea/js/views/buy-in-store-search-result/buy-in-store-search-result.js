/*global define*/

define(['commonService', 'animService', 'ratings', 'postmessage'], function() {
    'use strict';

    var BuyInStoreSearchResult = IEA.module('UI.buy-in-store-search-result', function(buyInStoreSearchResult, app, iea) {
        var animService = null;
        var commonService = null,
            ratingService = null;
        var markerLength = [];
        var InfoWindowlength = [];
        _.extend(buyInStoreSearchResult, {

            defaultSettings: {
                loadmore: '.js-btn-loadmore',
                postalInputField: '.c-search-store-form .c-search-store__textbox',
                storeSelectField: '.c-search-store__select option',
                modalCl: '.js-modal-etale'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                commonService = new IEA.commonService(); // creates new instance for common service
                this._super(options);
                ratingService = new IEA.ratings(); // creates new instance
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this.triggerMethod('beforEnable');
                var storeSearchResultData = this.getModelJSON().buyInStoreSearchResult;
                var self = this, options = {}, trackResults = true;
                    options.noStore = storeSearchResultData.buyInStoreData.noOfStores;
                    options.serviceurl = storeSearchResultData.buyInStoreData.serviceUrl;
                    options.loadMoreDisplayCount = storeSearchResultData.buyInStoreData.loadMoreDisplayCount;
                    options.mapApi = storeSearchResultData.buyInStoreData.mapApi;
                    options.loadmore = self.defaultSettings.loadmore;
                    options.mapItLabel = storeSearchResultData.mapItLabel;
                    options.distanceLabel = storeSearchResultData.buyInStoreData.fields.distanceWithin.optionsLabel;
                    options.postalInputField = self.defaultSettings.postalInputField;
                    options.storeSelectField = self.defaultSettings.storeSelectField;
                    options.searchDesc = storeSearchResultData.buyInStoreData.searchDescription;
                    options.regExp = storeSearchResultData.buyInStoreData.fields.postCode.regex;
                    options.ratingReview = storeSearchResultData.review;

                animService = new IEA.animService();

                $.getScript(options.mapApi).done(function(){
                    self.storeLocatorData(options, trackResults);
                });

                self.getKeywordValOnLoad(options);

                this.$el.on('click', '.js-btn-show-store', function(){
                    options.postalCode = $(options.postalInputField).val();
                    options.storeDist = $(options.storeSelectField+':selected').val();
                    markerLength = [];
                    InfoWindowlength = [];

                    var validZip = buyInStoreSearchResult.zipCodeValidation(options.postalCode, options.regExp),
                        productId = storeSearchResultData.product.productID;

                    if(validZip === true) {
                        $('.help-block').addClass('hidden');
                        $('.postalCode').parents('.form-group').removeClass('has-error');
                        self.storeLocatorData(options);

                        //ROI Beacon
                        if(typeof $BV === 'object') {
                            $BV.SI.trackConversion({
                                'type': 'StoreLocator',
                                'label': options.postalCode,
                                'value': productId
                            });
                        }

                    } else {
                        $('.postalCode').parents('.form-group').addClass('has-error');
                        $('.help-block').removeClass('hidden');
                        return false;
                    }
                });
                this.handleBinButton(storeSearchResultData.product, storeSearchResultData.shopNow);
                ratingService.initRatings(options);
                this.triggerMethod('enable');
            },

            storeLocatorData: function(options, trackResults) {
                var self = this, template;
                template = self.getTemplate('buy-in-store-search-list', 'buy-in-store-search-result/partial/buy-in-store-search-list.hbss');

                $.ajax({
                    url: options.serviceurl,
                    type: 'GET',
                    data: {'prodId': options.prodId, 'zip': options.postalCode, 'radius': options.storeDist, 'numStores': options.noStore, 'aggType': 'upc'},
                    dataType: ($('.fe-page').length > 0) ? 'json' : 'jsonp',
                    success: function(data) {
                        var storeSize = data.data.store.length, storeRadius = options.storeDist, mapIt = options.mapItLabel, distanceUnit = options.distanceLabel, productId = options.prodId, zipCode = options.postalCode;
                        data.data.mapIt = mapIt, data.data.distanceUnit = distanceUnit;
                        self.descTxt(storeSize, storeRadius);
                        if(storeSize > 0){
                            $('.c-buy-store-result').addClass('success-state');
                            $('.c-store-no-result').hide();
                            self.drawList(data);
                            self.mapMarker(data);
                        } else {
                            self.noResult(status);
                        }

                        if(trackResults == true){
                            self.resultTrackingLoad(data);
                            trackResults = false;
                        } else {
                            self.resultTrackingClick(data);
                        }
                    },
                    error: function(xhr, status, error) {
                        self.descTxt("0", options.storeDist);
                        self.noResult(status);
                    }
                });

                self.drawList = function(data) {
                    $('.c-result-list-body ul').empty();
                    self.drawListItem(data.data);
                    self.showResults(data.data.store);
                }

                self.drawListItem = function(listData){
                    $('.c-result-list-body ul').append(template(listData));
                }

                self.showResults = function(rows){
                    var numStore = options.loadMoreDisplayCount, $listItem = $('.c-result-list-body li'),
                    itemtoShow = options.loadMoreDisplayCount;
                    $listItem.hide();
                    var rowSize = $listItem.size();
                    //$('.c-result-list-body li:lt('+itemtoShow+')').show();
                    $('.c-result-list-body li').show();
                    //if(rowSize > itemtoShow) {
                        //$(options.loadmore).removeClass('hidden');
                    //} else {
                        $(options.loadmore).addClass('hidden');
                    //}

                    //more result show on load more click
                    $(options.loadmore).off().on('click', function(){
                        itemtoShow = (itemtoShow + numStore <= rowSize) ? itemtoShow + numStore : rowSize;
                        $('.c-result-list-body li:lt('+itemtoShow+')').show();
                        if(itemtoShow === rowSize) {
                            $(options.loadmore).addClass('hidden');
                        }
                    });
                    self.bindMapEvents();
                }
                self.bindMapEvents = function() {
                    // click event for store list item
                    $('[data-longt],[data-lat]').on('click',function(){
                        $('.c-result-list-body li').removeClass('active');
                        $(this).addClass('active');
                        var index = $(this).index();
                        var latNew = parseInt($(this).data('lat')), lngNew = parseInt($(this).data('longt'));
                        map.setCenter({lat: latNew, lng: lngNew});
                        //google.maps.event.trigger(markerLength[index], 'click')
                        for (var i = 0; i < InfoWindowlength.length; i++) {
                            InfoWindowlength[i].close(map,markerLength[i]);
                            markerLength[i].setIcon('');
                        }
                        InfoWindowlength[index].open(map,markerLength[index])
                        markerLength[index].setIcon('//maps.google.com/mapfiles/ms/icons/green-dot.png')
                    });

                    // scroll event for map stucking right
                    if ($(window).width() >= 768) {
                        $(window).scroll(function() {
                          if ($(window).scrollTop() > $('.c-result-list-body').offset().top-100 && $(window).scrollTop() < $('.c-result-list-body').height()) {
                            $('#map').css({'top':$(window).scrollTop()-$('.c-result-list-body').offset().top+130 });
                          }
                          else if ($(window).scrollTop() < $('.c-result-list-body').offset().top-100) {
                            $('#map').css({'top':0});
                          }
                        });
                    }


                }

                self.mapMarker = function(data) {
                    var lat, lng, title, add, ph, marker, infowindow, markers = [], latLng = [], currentInfoWindow = null;

                    //create array of markers information
                    $.each(data.data.store, function(i) {
                        markers.push({
                            "lat": data.data.store[i].Latitude,
                            "lng": data.data.store[i].Longitude,
                            "title": data.data.store[i].Name,
                            "add": data.data.store[i].Address,
                            "ph": data.data.store[i].Phone,
                            "chld": data.data.store[i]['Number']
                        });
                    });

                    // Create a map object and specify the DOM element for display.
                    window.map = new google.maps.Map(document.getElementById('map'), {
                        center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                        scrollwheel: true,
                        zoom: 8
                    });

                    // Create a marker and set its position.
                    $.each(markers, function(i){
                        var data = markers[i], infowindow, contentString, myLatlng = new google.maps.LatLng(data.lat, data.lng);

                        latLng.push(myLatlng);
                        contentString = "<h4>"+data.title+"</h4><p>"+data.add+"</p><p>"+data.ph+"</p>";

                        infowindow = new google.maps.InfoWindow({
                            content: contentString
                        });
                        InfoWindowlength.push(infowindow);
                        marker = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
                            title: data.title,
                            icon: '//chart.apis.google.com/chart?chst=d_map_spin&chld=1|0|FF0000|10|_|'+data.chld
                        });
                        markerLength.push(marker);
                        google.maps.event.addListener(marker, 'click', function() {
                            for (var infoCount = 0; infoCount < InfoWindowlength.length; infoCount++) {
                                InfoWindowlength[infoCount].close(map,markerLength[infoCount])
                                markerLength[infoCount].setIcon('');
                            }
                            if (currentInfoWindow != null) {
                                currentInfoWindow.close();
                            }
                            infowindow.open(map, this);
                            currentInfoWindow = infowindow;
                            $('.c-result-list-body li').removeClass('active')
                            $('.c-result-list-body li:eq('+i+')').addClass('active')
                            $(window).scrollTop($('.c-result-list-body li:eq('+i+')').offset().top-$('.c-result-list-body li:eq('+i+')').height()-200);
                            //marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png')
                            this.setIcon('//maps.google.com/mapfiles/ms/icons/green-dot.png')
                        });
                    });
                }

                self.noResult = function(status) {
                    if(status === 'error' || status === 'parseerror' || status === 'parsererror' || status === 'success' || status === "") {
                        $('.c-buy-store-result').removeClass('success-state');
                        $('.c-store-no-result').show();
                    } else {
                        $('.c-store-no-result').hide();
                        $('.c-buy-store-result').addClass('success-state');
                    }
                }

                self.descTxt = function(size, distance) {
                    var storeLength = size, radius = distance, searchDesc = options.searchDesc,
                        mapObj = {
                            noOfStores: storeLength,
                            noOfMiles: radius
                        };
                    searchDesc = searchDesc.replace(/noOfStores|noOfMiles/gi, function(matched){
                        return mapObj[matched];
                    });

                    $('.search-desc').text(searchDesc);
                }
            },

            handleBinButton: function(productData, shopNowData) {
                var productId,
                    self = this;
                $('.js-bin-button').click(function() {
                    productId = productData.productsDetail[0].smartProductId;
                    loadsWidget(productId, this,'retailPopup','en');
                });

                $('.js-etale-bin-button').click(function(e) {
                    e.preventDefault();
                    self.openModal();
                    $('.js-main-wrapper').removeClass('u-blur-background');

                    self.etaleTracking(productData.productID);

                    if(productData.productsDetail.length) {
                        $('#iframeId').attr('src', shopNowData.serviceURL+productData.productsDetail[0].smartProductId+'#'+shopNowData.pageurl);
                    }
                });

                $('.js-btn-close').click(function(e) {
                    e.preventDefault();
                    self.closeModal();
                });
            },

            openModal: function () {
                animService.openOverlay(this.defaultSettings.modalCl);
            },

            closeModal: function () {
                animService.closeOverlay(this.defaultSettings.modalCl);
            },

            zipCodeValidation: function(postalCode, regExp) {
                var regEx = regExp, pattern = new RegExp(regEx), isValidZip = pattern.test(postalCode);
                return isValidZip;
            },

            getQueryStringVal: function(name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
                return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            },

            getKeywordValOnLoad: function(options) {
                var self = this;
                // page load search result
                options.prodId = self.getQueryStringVal('pvi');
                options.postalCode = self.getQueryStringVal('pc');
                options.storeDist = self.getQueryStringVal('dw');
                options.prevUrl = self.getQueryStringVal('preUrl');

                $(options.postalInputField).val(options.postalCode);
                $(options.storeSelectField+'[value="'+options.storeDist+'"]').prop('selected', true);
            },

            resultTrackingLoad: function(data) {
                var ev = {}, compName = $('.c-search-store').data('componentname'), totalResult = data.data.store.length, radiusSel = $('.c-search-store-form .c-search-store__select option:selected').text();;
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    digitalData.component.push({
                        'componentInfo' :{
                            'name': compName
                        }
                    });

                    ev.eventInfo = {
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.storeLocator,
                      'eventLabel' : 'Store Locator Results - ' + totalResult
                    };
                    ev.category ={'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);

                    digitalData.component = [];
                }
            },

            resultTrackingClick: function(data) {
                var ev = {}, compName = $('.c-search-store').data('componentname'), totalResult = data.data.store.length, radiusSel = $('.c-search-store-form .c-search-store__select option:selected').text();;
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    digitalData.component.push({
                        'componentInfo' :{
                            'name': compName
                        }
                    });

                    ev.eventInfo={
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.storeLocator,
                      'eventLabel' : 'Store Locator Option ' + radiusSel + ' | Results Refreshed ' + totalResult
                    };
                    ev.category ={'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);

                    digitalData.component = [];
                }
            },

            getProductVariant: function(storeSearchResultData, options) {
                var productVariant, productUnit;
                _.filter(storeSearchResultData.product.productsDetail, function(item) {
                    productVariant = item.shortIdentifierValue;
                    if(options.prodId === productVariant) {
                        productUnit = item.unit;
                    }
                });

                return productUnit;
            },

            etaleTracking: function(prodId){
                // Get the parent page URL as it was passed in, for browsers that don't support
                // window.postMessage (this URL could be hard-coded).
                var parent_url = decodeURIComponent( document.location.hash.replace( /^#/, '' ) ),
                    link;

                // The first param is serialized using $.param (if not a string) and passed to the
                // parent window. If window.postMessage exists, the param is passed using that,
                // otherwise it is passed in the location hash (that's why parent_url is required).
                // The second param is the targetOrigin.
                function postMessage(model, partnum, prodid, proddesc, markettext, ean, upc, etin, retailer) {
                    var message = '{ "model" : "' + model +
                                    '", "partnum" : "' + partnum +
                                    '", "prodid" : "' + prodid +
                                    '", "proddesc" : "' + proddesc +
                                    '", "markettext" : "' + markettext +
                                    '", "ean" : "' + ean +
                                    '", "upc" : "' + upc +
                                    '", "etin" :"' + etin +
                                    '", "retailer" : " ' + retailer +
                                    '", "date": "' + new Date() + '"}' ;
                    $.postMessage(message, parent_url, parent );
                }

                $.receiveMessage(
                    function (e) {
                        var json = JSON.parse(e.data),
                            retailername = json.retailer;

                        if (typeof retailername !== 'undefined') {
                            if(typeof $BV === 'object') {
                                $BV.SI.trackConversion({
                                    'type': 'BuyOnline',
                                    'label': retailername,
                                    'value': prodId
                                });
                            }
                        }
                    }
                );
            }


            /**************************PRIVATE METHODS******************************************/

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }

            */

        });
    });

    return BuyInStoreSearchResult;
});