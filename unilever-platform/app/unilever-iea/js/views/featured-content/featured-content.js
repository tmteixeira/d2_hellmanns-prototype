/*global define*/

define(function() {
    'use strict';

    var featuredContent = IEA.module('UI.featured-content', function(featuredContent, app, iea) {

        _.extend(featuredContent, {
            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');
                // calling methods here
                
                this._spotlightDataHandler();
                 
                this.triggerMethod('enable');
            },

            /**************************PRIVATE METHODS******************************************/  
            
            _spotlightDataHandler: function(){
                var self = this,
                    spotlightJson = self.getModelJSON().featuredContent,
                    currentDate = new Date(),
                    startDate,endDate;
                
                if(spotlightJson.startDate !== ''){ 
                    startDate = new Date(spotlightJson.startDate);
                }
                
                if(spotlightJson.endDate !== ''){ 
                    endDate = new Date(spotlightJson.endDate);
                }
                
                if((typeof startDate !== 'undefined' &&  typeof endDate !== 'undefined' && currentDate >= startDate && currentDate <= endDate)
                    ||(typeof endDate !== 'undefined' && typeof startDate === 'undefined' && currentDate <= endDate) 
                    || (typeof startDate !== 'undefined' && typeof endDate === 'undefined' && currentDate >= startDate) 
                    || (typeof startDate === 'undefined' && typeof endDate === 'undefined')){
                    
                    this.$el.find('.c-featured-content').html();
                }else{
                    this.$el.find('.c-featured-content').empty();
                }
            }
        });
    });
    return featuredContent;
});
