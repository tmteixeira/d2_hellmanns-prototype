/*global define*/

define(['commonService'], function() {
    'use strict';

    var SimpleFooter = IEA.module('UI.simple-footer', function(simpleFooter, app, iea) {
        
        var commonService = null;

        _.extend(simpleFooter, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                var throttledLazyLoad;
                
                commonService = new IEA.commonService();

                // set tooltip
                commonService.tooltipEvent('.c-tooltip-info__icon', '.c-tooltip-info__copy', '.c-tooltip-info');
                
                commonService.imageLazyLoadOnPageScroll(); // Init lazy load
                throttledLazyLoad = _.throttle(commonService.imageLazyLoadOnPageScroll, 150);
                $(window).on('scroll.lazyLoad', throttledLazyLoad); // Window scroll lazy load

                this.triggerMethod('enable');
            }


            /**************************PRIVATE METHODS******************************************/          

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */

        });
    });

    return SimpleFooter;
});
