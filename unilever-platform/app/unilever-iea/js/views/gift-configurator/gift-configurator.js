/*global define*/

define(['eGiftingService'], function() {
    'use strict';

    var eGiftingService = null;

    var GiftConfigurator = IEA.module('UI.gift-configurator', function(giftConfigurator, app, iea) {

        _.extend(giftConfigurator, {

            defaultSettings: {
                formInput: '.c-gift-configurator__form--text .c-form-input',
                quantity: 1,
                formElement: '.c-gift-configurator__form--group',
                timerWrapper: '.c-simple-header__copy-wrap',
                timerSelector: '#timerText',
                radioBtn: '.c-form-radio input[name=giftwrap]',
                helpBlockCl: '.help-block',
                hasError: 'has-error',
                hiddenCl: 'hidden',
                priceWrap: '.c-gift-configurator__price--wrap',
                totalPrice: '#totalPrice',
                charLeft: '.character-left',
                cartButton: '.js-btn__addtocart',
                cookieName: 'cartSession',
                storageName: 'productCartData',
                disable: 'disabled'
            },

            events: {
                'click .js-btn__addtocart': 'setLocalStorage' // local storage call
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this.isValid = false;
                this._super(options);
                eGiftingService = new IEA.EGifting();

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
				this.triggerMethod('beforEnable');
                
                var self = this,
                    settings = self.defaultSettings,
                    charCount = $(settings.formInput).data('rule-maxlength');

                self.componentJson = self.getModelJSON().giftConfigurator;
                self.cookieName = settings.cookieName+'-'+self.componentJson.market;
                self.storageName = settings.storageName;

                self.sessionData = eGiftingService.getItem(self.storageName);
                // method for character count
                eGiftingService.remainingChars(settings.formInput);

                // re render data
                self.repopulateData(self.componentJson);
				self.isRadioChecked();
                self.isCartButtonEnabled();

                this.triggerMethod('enable');
            },

            setCookieInCart: function() {
                var self = this,
                settings = self.defaultSettings,
                timerVal = $(settings.timerSelector).data('timer');

                if (!eGiftingService.hasCookieData(self.cookieName)) {
                    var setTimer = new Date(+new Date + (timerVal * 60 * 1000));
                    eGiftingService.createCookie(self.cookieName, setTimer, timerVal);
                    $(settings.timerWrapper).removeClass('o-hidden');
                }
            },

            isRadioChecked: function () {
				var self = this,
                settings = self.defaultSettings,
                infoPrice = '.c-gift-configurator__info--price ';

                $(settings.radioBtn).change(function() {			
                    if ($(this).prop('checked')) {
						var jsonData = self.getModelJSON().giftConfigurator,
							totalPrice;
							
						totalPrice = parseFloat(jsonData.Product.giftConfigurator.customisedWrappingPrice) + parseFloat(jsonData.Product.giftConfigurator.customisedPrice);

                        $(infoPrice+settings.priceWrap).fadeIn(600);
						$(infoPrice+settings.totalPrice).text(totalPrice.toFixed(2));
                    }
                });
            },

            setLocalStorage: function(e) {
                var self = this;

                e.preventDefault();
                self.validateField();

                if (self.isValid) {
                    self.setCookieInCart();
                    if (eGiftingService.hasCookieData(self.cookieName)) {
                        self.storeDataInStorage(self.componentJson);
                    } else {
                        eGiftingService.setItem(self.storageName, '');
                    }
                    window.location.href = $(this.defaultSettings.cartButton).attr('href');
                }
            },

            storeDataInStorage: function(data) {                
                // Check browser support
                var self = this,
                    settings = self.defaultSettings,
                    charLeftCl = settings.charLeft,
                    recipientVal = $('#'+data.giftFeilds[0].formElement.name).val(),
                    recipientValCount = $('#'+data.giftFeilds[0].formElement.name).siblings(charLeftCl).text(),
                    greetingVal = $('#'+data.giftFeilds[1].formElement.name).val(),
                    greetingValCount = $('#'+data.giftFeilds[1].formElement.name).siblings(charLeftCl).text(),
                    giftWrapDetails = parseInt($(settings.radioBtn +':checked').data('index')),
                    jsonData = [],
                    cartData = {
                        "products": '',
                        "encryptedDeliveryPrice":data.deliveryDetail.encryptedDeliveryPrice,
                        "deliveryPrice":data.deliveryDetail.deliveryPrice,
                        "currencySymbol": data.Product.currencySymbol
                    },
                    productData;

                    productData = {
                        "title": data.Product.title,
                        "productId": data.Product.productID,
						"primaryCategory": data.Product.primaryCategory,
                        "image": {
                            "extension": data.Product.productsDetail[0].images[0].extension,
                            "title": data.Product.productsDetail[0].images[0].title,
                            "altImage": data.Product.productsDetail[0].images[0].altImage,
                            "isNotAdaptiveImage": data.Product.productsDetail[0].images[0].isNotAdaptiveImage,
                            "fileName": data.Product.productsDetail[0].images[0].fileName,
                            "path": data.Product.productsDetail[0].images[0].path
                        },
                        "customization": {
                            "recipientName": recipientVal,
                            "recipientLebelCount": recipientValCount,
                            "greetingMessage": greetingVal,
                            "greetingLabelCount": greetingValCount
                        },
                        
                        "price" : data.Product.giftConfigurator.customisedPrice,
                        "encryptedPrice": data.Product.giftConfigurator.encryptedPrice,
                        "encryptedWrappingPrice": data.Product.giftConfigurator.encryptedWrappingPrice,
                        "customisedWrappingPrice": data.Product.giftConfigurator.customisedWrappingPrice,
                        "priceWithQuantity": data.Product.giftConfigurator.customisedPrice,
                        "wrappingPriceWithQuantity": data.Product.giftConfigurator.customisedWrappingPrice,

                        "giftWrap": data.Product.giftConfigurator.giftWraps[giftWrapDetails],
                        "quantity" : settings.quantity,
                        "isEdit" : false,
                        "giftWrapsIndex": giftWrapDetails
                    };

                if (self.sessionData !== undefined && self.sessionData !== '' && self.sessionData !== null) {
                    var storageCartData = $.parseJSON(self.sessionData);
                    jsonData = storageCartData.products;
                }

                if ($(jsonData).length !== 0) {
                    var editable = false;
                    $(jsonData).each(function(key, obj) {
                        if (obj.isEdit) {
                            productData.quantity = obj.quantity;
                            productData.priceWithQuantity = parseFloat((productData.price)*(obj.quantity)).toFixed(2);
                            productData.wrappingPriceWithQuantity = parseFloat((productData.customisedWrappingPrice)*(obj.quantity)).toFixed(2);
                            jsonData[key] = productData;
                            editable = true;
                        }
                    });

                    if (!editable) {
                        jsonData.push(productData);
                    }
                } else {
                    jsonData.push(productData);
                }
                                               
                if (typeof(localStorage) !== "undefined") {
                    cartData.products = jsonData;
                    eGiftingService.setItem(self.storageName, JSON.stringify(cartData));
                } else {
                    console.log('Sorry, your browser does not support Web Storage...');
                }
            },

            repopulateData: function(data, formInput, charCount) {
                var self = this,
                    settings = self.defaultSettings,
                    charLeftCl = settings.charLeft,
                    formElementRecipient = data.giftFeilds[0].formElement.name,
                    formElementGreetings = data.giftFeilds[1].formElement.name,
                    totalPrice;

                if (self.sessionData !== undefined && self.sessionData !== '' && self.sessionData !== null) {
                    var storageData = jQuery.parseJSON(self.sessionData);

                    $(storageData.products).each(function(key, obj) {
                        if (obj.isEdit === true) {
                            $('#'+formElementRecipient).val(obj.customization.recipientName);
                            $('#'+formElementRecipient).siblings(charLeftCl).text(obj.customization.recipientLebelCount);
                            
                            $('#'+formElementGreetings).val(obj.customization.greetingMessage);
                            $('#'+formElementGreetings).siblings(charLeftCl).text(obj.customization.greetingLabelCount);
                            
                            $('.js-'+formElementRecipient).text(obj.customization.recipientName);
                            $('.js-'+formElementGreetings).text(obj.customization.greetingMessage);

                            $(settings.radioBtn +':eq('+obj.giftWrapsIndex+')').attr('checked', 'checked');

                            totalPrice = parseFloat(obj.customisedWrappingPrice) + parseFloat(obj.price);

                            $(settings.priceWrap).fadeIn(600);
                            $(settings.totalPrice).text(totalPrice.toFixed(2));
                            $(settings.cartButton).removeClass(settings.disable); 
                        }
                    });
                }
            },

            validateField: function() {
                var self = this,
                settings = self.defaultSettings,
                $formElement = $(settings.formElement),
                checkedLength = 0;

                $formElement.each(function(key) {
                    var $this = $(this),
                    $inputField = $this.find('input'),
                    helpBlockCl = settings.helpBlockCl,
                    hasError = settings.hasError,
                    hidden = settings.hiddenCl;

                    if ($inputField.attr('type') === 'text') {
                        if ($inputField.val().length > 1) {
                            $this.removeClass(hasError);
                            $this.find(helpBlockCl).addClass(hidden);
                        } else {
                            $this.addClass(hasError);
                            $this.find(helpBlockCl).removeClass(hidden);
                        }
                    } else if ($inputField.attr('type') === 'radio') {
                        if ($inputField.prop('checked')) {
                            checkedLength++;
                        }
                        if (checkedLength === 1) {
                            $this.parent().removeClass(hasError);
                            $this.parent().find(helpBlockCl).addClass(hidden);
                        } else {
                            $this.parent().addClass(hasError);
                            $this.parent().find(helpBlockCl).removeClass(hidden);
                        }
                    }
                });

                if (self.$el.find('.'+settings.hasError).length === 0) {
                    self.isValid = true;
                } else {
                    self.isValid = false;
                }

                if (self.isValid === false) {
                    $('html, body').animate({
                        scrollTop : self.$el.find('.'+settings.hasError).offset().top - 200
                    }, 1000);
                }
            },

            isCartButtonEnabled: function() {
                var self = this,
                    settings = self.defaultSettings,
                    $formInput = $(settings.formElement).find('input'),
                    isEnabled = false;

                $formInput.on('change keyup', function() {
                    var $this = $(this),
                    inputFieldLength = $(settings.formElement).find('input[type="text"]').length + 1,
                    inputCount = 0;

                    $formInput.each(function() {
                        var $inputField = $(this);

                        if ($inputField.attr('type') === 'text') {
                            if ($inputField.val().length > 1) {
                                inputCount++;
                            } else {
                                inputCount--;
                            }
                        } else if ($inputField.attr('type') === 'radio') {
                            if ($inputField.prop('checked')) {
                                inputCount++;
                            }
                        }
                    });

                    if (inputCount === inputFieldLength) {
                        $(settings.cartButton).removeClass(settings.disable); 
                    } else {
                        $(settings.cartButton).addClass(settings.disable); 
                    }
                });
            }
        });
    });

    return GiftConfigurator;
});
