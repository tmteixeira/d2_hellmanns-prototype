/*global define*/

define(['TweenMax', 'videoplayerService', 'commonService'], function(TweenMax) {
    'use strict';

    var Hero = IEA.module('UI.hero', function(hero, app, iea) {

        var videoplayerService = null,
        commonService = null;

        _.extend(hero, {

            $imageContainer: null,
            $videoContainer: null,
            _isVideoOpened: false,
            windowHeight: 0,
            windowWidth: 0,
            videoPlayer: null,

            events: {

                'click .js-button-more': '_clickReadMoreHandler'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                var self = this;
                self._setupHero();
                self._setupPlayer();

                $('.c-hero-image .is-hero-video').click(function(e){
                    self._clickVideoHandler(e);
                });
                // Home page hero animations
                TweenMax.to('.c-hero-image', 0.5, {autoAlpha: 1, delay: 0.4});
                $('.c-hero-image').addClass('js-enabled');
                TweenMax.to('.c-hero-body', 0.8, {autoAlpha: 1, top: 0, delay: 0.8});

                // Arrow button animation
                //TweenMax.to('.o-continue-arrow', 1, {alpha: 1, repeat: -1, yoyo:true});
                TweenMax.to('.o-continue-arrow', 1, {autoAlpha: 1, delay: 2});

                this.triggerMethod('enable');
            },

            onWindowResized: function() {

                var $window = $(window);
                if($window.height() !== this.windowHeight || $window.width() !== this.windowWidth){

                    this.windowWidth = $window.width();
                    this.windowHeight = $window.height();
                    this._resizeImageContainer();
                }
            },

            /**************************PRIVATE METHODS******************************************/

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }

            */

            _setupHero: function() {

                this.windowHeight = $(window).height();
                this.windowWidth = $(window).width();
                this.$imageContainer = this.$el.find('.c-hero-image');
            },

            _setupPlayer: function() {
                var options = {},
                videoJsonData = this.getModelJSON().hero;

                options.videoContainerId = 'hero-video';
                options.view = this;
                options.videoType = videoJsonData.videoType;
                options.videoFlag = true;

                if (this.$el.find('#' + options.videoContainerId).length) {
                    commonService = new IEA.commonService();
                    videoplayerService = new IEA.videoPlayerService(options);
                }
            },

            onVideoEnded: function() {
                if (commonService.isMobileAndTablet()) {
                    this.$el.find('.js-video-btn').removeClass('hidden');
                    this.events['click .js-video-btn'] = '_replayVideoHandler';
                    this.delegateEvents();
                }
            },

            onPlayerReady: function() {

                this.$videoContainer = this.$el.find('#hero-video');

                this.events['click .c-hero-image .is-hero-video'] = '_clickVideoHandler';
                this.delegateEvents();
            },

            _toggleFullScreen: function(elem) {
                var doc = window.document;

                var requestFullScreen = elem.requestFullscreen ||
                                        elem.webkitRequestFullScreen ||
                                        elem.mozRequestFullScreen ||
                                        elem.msRequestFullscreen;

                var cancelFullScreen = doc.exitFullscreen ||
                                        doc.webkitExitFullscreen ||
                                        doc.mozCancelFullScreen||
                                        doc.msExitFullscreen;

                if (!(doc.fullscreenElement || doc.mozFullScreenElement ||
                    doc.webkitFullscreenElement || doc.msFullscreenElement)) {

                    requestFullScreen.call(doc.body);
                } else {

                    cancelFullScreen.call(doc);
                }
            },

            _clickReadMoreHandler: function(evt) {

                if (evt) {

                    evt.preventDefault();
                }

                var $heroDetailedArticle = this.$el.find('.c-hero-article-container');
                var $readMoreBtn = $(evt.currentTarget);

                if ($readMoreBtn.hasClass('js-button-less')) {

                    TweenMax.to($heroDetailedArticle, 1, {height: '0'});
                    $readMoreBtn.removeClass('js-button-less');
                } else {

                    TweenMax.set($heroDetailedArticle, {height: 'auto'});
                    TweenMax.from($heroDetailedArticle, 1, {height: 0});
                    $readMoreBtn.addClass('js-button-less');
                }
            },

            _clickVideoHandler: function(evt) {
                var options = {},
                videoJsonData = this.getModelJSON().hero;

                options.videoContainerId = 'hero-video';
                options.view = this;
                options.videoType = videoJsonData.videoType;
                options.videoFlag = true;
                
                if (evt) {
                    evt.preventDefault();
                }
                this.$imageContainer.toggleClass('is-hero-video-open');
               
                videoplayerService.setupVidepAPI(options);

                this._isVideoOpened = !this._isVideoOpened;
                this._resizeImageContainer(true);
            },

            _replayVideoHandler: function() {

                videoplayerService.play();

                this.$el.find('.js-video-btn').addClass('hidden');
                delete this.events['click .js-video-btn'];
                this.delegateEvents();
            },

            _resizeImageContainer: function(transition) {

                if(this._isVideoOpened) {
                    var containerHeight = this._isVideoOpened ? $('#hero-video').height(): this.$el.find('img').height();

                    if (transition) {
                        TweenMax.to(this.$imageContainer, 1, {height: containerHeight});
                    } else {
                        this.$imageContainer.height(containerHeight);
                    }
                }

            },
            
            _stopPlayVideo: function(target) {
                var self = this,
                    videoContainerId = 'hero-video',
                    videoName = $('#'+videoContainerId).data('video-name'),
                    parentDiv = $('#'+videoContainerId).parent();
                
                $('.c-hero-image .is-hero-video').click(function() {
                    if (!self._isVideoOpened) {
                        var playerDiv = '<div id="'+videoContainerId+'" class="c-hero-video" data-video-id="'+$('#'+videoContainerId).data("video-id")+'" data-video-name="'+videoName+'"></div>';
                        
                        $('iframe#'+videoContainerId).remove();
                        if (parentDiv.children().length === 0) {
                            parentDiv.append(playerDiv);
                        }
                        $('.c-hero-image').height('auto');  
                                   
                    }
                });
            },
			
			_progressTrack:function(progress,videoId){
                 var ev = {},
					 compName = $('.c-hero').data('componentname'), compVar = $('.c-hero').data('component-variants'), compPos = $('.c-hero').data('component-positions');
				
				digitalData.component = [];
                digitalData.component.push({
					'componentInfo' :{
						'componentID': compName,
						'name': compName
					},
					'attributes': {
						'position': compPos,
						'variants': compVar
					}
				});

				ev.eventInfo={
				  'type':ctConstants.trackEvent,
				  'eventAction': ctConstants.videoProgress,
				  'eventLabel' : 'VIDEO PROGRESS '+progress
				};
				ev.attributes={'nonInteraction':1};
				ev.category ={'primaryCategory':ctConstants.custom};
				digitalData.event.push(ev);
            },

            _ctTag: function(videoName, videoId, play) {
                var ev = {},
                compName = $('.c-hero').data('componentname'), compVar = $('.c-hero').data('component-variants'), compPos = $('.c-hero').data('component-positions');
                
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    digitalData.component = [];
                    digitalData.video = [];

                    digitalData.component.push({
						'componentInfo' :{
							'componentID': compName,
							'name': compName
						},
						'attributes': {
							'position': compPos,
							'variants': compVar
						}
					});

                    digitalData.video.push({
                        'videoId':videoId
                    });

                    if (play === true) {
                        ev.eventInfo={
                            'type':ctConstants.trackEvent,
                            'eventAction':ctConstants.videoPlays,
                            'eventLabel':videoName
                        };
                    } else {
                        ev.eventInfo={
                            'type':ctConstants.trackEvent,
                            'eventAction':ctConstants.videoCompletes,
                            'eventLabel':videoName
                        };
                    }
                    ev.category = {'primaryCategory':ctConstants.other};
                    digitalData.event.push(ev);
                }
            }
        });
    });

    return Hero;
});
