/*global define*/

define(['ratings'], function() {
    'use strict';

    var ProductRatingOverview = IEA.module('UI.product-rating-overview', function(productRatingOverview, app, iea) {

        var ratingService = null;

        _.extend(productRatingOverview, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                ratingService = new IEA.ratings(); // creates new instance

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                //_inject Rating Service
                this._injectRatingService();

                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/          

            _injectRatingService: function() {
                var componentJson = this.getModelJSON().productRatingOverview,
                    options = {};
                
                if (componentJson.product.ratings) {
                    options = {
                        ratingReview: componentJson.product.review,
                        ratingObj: componentJson.product.ratings,
                        productId: componentJson.product.ratings.uniqueId,
                        productName: componentJson.product.title,
                        brandName: componentJson.brandName,              
                        componentName: componentJson.componentName,
                        componentVariants: componentJson.componentVariation,
                        componentPositions: componentJson.componentPosition,
                        productCategory: '' //to do will assign the primary category from product
                    };
                }                

                ratingService.initRatings(options);
            }

        });
    });

    return ProductRatingOverview;
});
