/*global define*/

define(['turnToService', 'commonService'],function() {
    'use strict';
    
    var turnToService=null,
        commonService = null;
    
    var CommunityProductQnaOverview = IEA.module('UI.community-product-qna-overview', function(communityProductQnaOverview, app, iea) {

        _.extend(communityProductQnaOverview, {

            /*********************PUBLIC METHODS******************************************/
            events: {
                'click .TurnToItemTeaser': '_scrollToEvent',
                'click .TurntoItemTeaserClick': '_ctAnchorLinks',
                'click .TurnToIteaSee': '_ctAnchorLinks'
            },

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');
                
                var self = this, 
                    componentJson = self.getModelJSON().communityProductQnaOverview;

                self.options = {
                    siteKey:componentJson.communityProductQA.siteKey,
                    setupType:componentJson.communityProductQA.setupType,
                    productID:componentJson.product.productID,
                    src:componentJson.communityProductQA.source,
                    itemJS:componentJson.communityProductQA.itemJS,
                    skuId:componentJson.product.productID,
                    skipCssLoad: componentJson.communityProductQA.skipCssLoad,
                    teaserCss: componentJson.communityProductQA.inputteasersCss,
                    traCss: componentJson.communityProductQA.traCss
                };
                
                commonService = new IEA.commonService();
                turnToService = new IEA.turnToService();
                
                self._renderQnaWidget();
                
                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************
            
            /**
             * scroll to CTA to questions
             * name: _scrollToEvent
             */
            _scrollToEvent: function(event) {
                var $this = $(event.currentTarget),
                    wrapperId = 'TurnToContent';
                
                // Remove "link" from the ID
                wrapperId = wrapperId.replace('link', '');
                
                // Scroll
                $('html,body').animate({
                  scrollTop: $("#" + wrapperId).offset().top
                },'slow');
            },

            /**
             * call turn to method which will load dependent global objects and stylesheets
             * call back is used to load the necessary script files required to render the widget.
             */
            _renderQnaWidget: function() {
                var self = this;
                turnToService.initCommunityProductQna(self.options, function(options) {
                    var src = options.itemJS;
                    
                    if ($('script[src*=\'itemjs\']').length === 0) {
                        commonService.renderScript(src);
                    }
                    
                    if ($('script[src*=\'tra.js\']').length === 0) {
                        commonService.renderScript(options.src);
                    }
                });
            },

            /**
             * track component info
             * name: _ctComponentInfo
             */
            _ctComponentInfo: function() {
                var self = this,
                    compName = self.$el.find('[data-componentname]').data('componentname'),
                    compVar = self.$el.find('[data-component-variants]').data('component-variants'),
                    compPos = self.$el.find('[data-component-positions]').data('component-positions');
                
                digitalData.component = [];
                digitalData.component.push({
                    'componentInfo' :{
                        'componentID': compName,
                        'name': compName
                    },
                    'attributes': {
                        'position': compPos,
                        'variants': compVar
                    }
                });
            },

            /**
             *  _ctAnchorLinks
             */
            _ctAnchorLinks: function(evt) {
                var $this = $(evt.currentTarget),
                    ev = {},
                    label = $this.text();
                
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    this._ctComponentInfo();

                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.anchorLinkClicked,
                        'eventLabel' : label
                    };

                    ev.category = {'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                }
            }       
        });
    });

    return CommunityProductQnaOverview;
});
