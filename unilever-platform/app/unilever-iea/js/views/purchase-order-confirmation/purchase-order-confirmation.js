/*global define*/

define(['commonService'], function(TweenMax) {
    'use strict';

    var commonService = null;

    var PurchaseOrderConfirmation = IEA.module('UI.purchase-order-confirmation', function(purchaseOrderConfirmation, app, iea) {

        _.extend(purchaseOrderConfirmation, {

            defaultSettings: {
                placeholderOrderNumber: '.js-placeholder-order-number',
                placeholderCustomerEmail: '.js-placeholder-customer-email'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                commonService = new IEA.commonService(); // creates new instance for common service

                // add click event for button type CTA
                this.$el.find('button').off('click').on('click', function(){
                    var $this = $(this),
                        href = $this.data('href'),
                        target = $this.data('target');

                    if (target === 'blank') {
                        window.open(href);
                    } else {
                        window.location.href = href;
                    }
                });

                // updatePlaceholder
                this._updatePlaceholder();

                this.triggerMethod('enable');
            },

            /**************************PRIVATE METHODS******************************************/          
            _updatePlaceholder: function() {
                var self = this,                
                    orderId = commonService.getQueryStringVal('orderId'),
                    emailId = commonService.getQueryStringVal('emailId');

                self.$el.find(self.defaultSettings.placeholderOrderNumber).html(orderId);
                self.$el.find(self.defaultSettings.placeholderCustomerEmail).html(emailId);
            }
        });
    });

    return PurchaseOrderConfirmation;
});
