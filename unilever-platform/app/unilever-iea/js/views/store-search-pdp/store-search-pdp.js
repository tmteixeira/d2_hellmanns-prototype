/*global define*/

define(['accordion'], function() {
    'use strict';

    var StoreSearchPdp = IEA.module('UI.store-search-pdp', function(storeSearchPdp, app, iea) {

        _.extend(storeSearchPdp, {

            defaultSettings: {
                zipCodeField: '.input-zipcode',
                accordion: '.js-store-search-pdp-copy',
                location: ''
            },

            events: {
                'click .js-btn-store-search' : '_findStores', // submit form
                'click .js-find-location' : '_findLocation', // find locations
                'click .js-store-search-pdp-header': '_toggleStoreSearch' //toggle store search
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                this.location = this.defaultSettings.location;

                this.triggerMethod('enable');
            },

            /**************************PRIVATE METHODS******************************************/ 

            /**
             * toggle store search wrapper on label click
             * @method _toggleStoreSearch
             * @return 
             */

            _toggleStoreSearch: function(evt) {
                var service = new IEA.accordion(), // creates new instance
                    options = {};

                options.wrapper = this.defaultSettings.accordion;
                service._handleExpandCollapse(evt, options);
            },

            /**
             * search zip code based on user location
             * @method _shareLoc
             * @return 
             */

            _findLocation: function() {             
                var self = this,
                    pos, reverseGeoCode;

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        }
                        reverseGeoCode(pos.lat, pos.lng, function(zip) {
                           self.$el.find(self.defaultSettings.zipCodeField).val(zip);
                        }); 
                    });
                    
                    reverseGeoCode = function(lat, lng, callback) {
                        var mapServiceUrl = '//maps.googleapis.com/maps/api/geocode/json?latlng=',
                            googleService = mapServiceUrl + lat + ',' + lng + '&sensor=false';
                        
                        $.getJSON(googleService, getMapData);

                        function getMapData(data) {
                            var addressComponents = data.results[0].address_components,
                                postalCode = '', 
                                countryCode = '', 
                                type = '';

                            if (addressComponents) {                                
                                _.each(addressComponents, function(item, index) { 
                                    if (typeof item.types == "string") {
                                        type = item.types;
                                    } else if (typeof item.types == "object") {
                                        type = item.types[0];
                                    }
                                    
                                    if (type === 'postal_code') {
                                        postalCode = item.long_name;
                                    }
                                    if (type === 'postal_town') {
                                        self.location = item.long_name;
                                    }
                                });
                            
                                if (typeof callback === 'function') {
                                    callback(postalCode);
                                }
                            }
                        }   
                    }
                }               
            },

            /**
             * submit buy in store form
             * @method _submitBuyStoreForm
             * @return 
             */

            _findStores: function(evt) {
                evt.preventDefault();

                var $this = $(evt.currentTarget),
                    $postalCode = this.$el.find(this.defaultSettings.zipCodeField),
                    isZipcodeValid = this._validateZipCode($postalCode.val(), $postalCode.data('regex')),                   
                    $formGroup = $this.parents('.form-group'),
                    $helpBlock = this.$el.find('.help-block');
                
                if (isZipcodeValid && $postalCode.val() !== '') {
                    $helpBlock.addClass('hidden');
                    $formGroup.removeClass('has-error');
                    $this.parents('form').submit();
                    // track store search analytics
                   this._ctStoreSearch();
                } else {
                    $formGroup.addClass('has-error');
                    $helpBlock.removeClass('hidden');
                    $postalCode.focus();
                    return false;
                }                  
            },

            /**
             * Zip code validation based on regex provided
             * @method _validateZipCode
             * @return 
             */
            
            _validateZipCode: function(fieldVal, regExp) {
                var regEx = regExp, 
                    pattern = new RegExp(regEx), 
                    isValidZip = pattern.test(fieldVal);

                return isValidZip;
            },

            _ctStoreSearch: function() {
                var ev = {},
                    $this = this.$el.find('.c-store-search-pdp'),
                    compName = $this.data('componentname'),
                    compVar = $this.data('component-variants'),
                    compPos = $this.data('component-positions'),
                    label = 'StoreSearchSubmitted < ' + $('#pc').val() + ' | ' + this.location;
            
                digitalData.component = [];
                digitalData.component.push({
                    'componentInfo' :{
                        'name': compName
                    },
                    'attributes': {
                        'position': compPos,
                        'variants': compVar
                    }
                });

                digitalData.product = [];
                digitalData.product.push({
                   'productInfo' :{
                       'productID': $('#pvi').val(),
                       'productName': $('.js-product-images-thumbnail').data('product-name'),
                    },
                    'category':{
                        'primaryCategory': $('.c-product-images').data('product-category')
                    }
                });
                ev.eventInfo = {
                  'type': ctConstants.trackEvent,
                  'eventAction': ctConstants.storesearch,
                  'eventLabel' : label
                };
                ev.category = {'primaryCategory':ctConstants.custom};
                ev.attributes={'nonInteractive':{'nonInteraction': 0}};
                digitalData.event.push(ev);
            }
        });
    });

    return StoreSearchPdp;
});
