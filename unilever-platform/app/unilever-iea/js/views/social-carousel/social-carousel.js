/*global define*/

define(['TweenMax', 'jquery.hammer', 'commonService', 'socialTabService'], function(TweenMax) {
    'use strict';

    var SocialCarousel = IEA.module('UI.social-carousel', function(socialCarousel, app, iea) {
		
		var commonService = null,
        socialTabService = null;
		
        _.extend(socialCarousel, {
			
			defaultSettings: {
                socialCarouselList: '.js-social-carousel__list',
                socialGalleryItem: '.js-social-carousel__item',
                template: 'social-carousel/partial/social-carousel-list.hbss',
                quickPanelWrapper: '.js-social-carousel__panel',
                thumbnail: '.js-social-carousel__thumbnail',
                animateSpeed: 1000,
                pageScroll: false,
                pageStartIndex: 1,
                pageNum: 1,
                itemMargin: 30,
                prevButton: '.js-social-carousel__arrow-prev',
                nextButton: '.js-social-carousel__arrow-next',
                closeButton: '.js-btn-social-carousel__close',
                disableHotKey: true
            },

            events: {
                'click .js-social-carousel__thumbnail' : '_displayQuickPanel', // show quick panel
                'click .js-btn-social-carousel__close' : '_closePanel', //hide quick panel
                'click .js-social-carousel__item.active' : '_closePanelonActiveSlide', //hide quick panel
                'click .js-social-carousel__arrow' : '_navigateToItem' // navigate quick panel through next/prev button
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);

                commonService = new IEA.commonService(); // creates new instance for common service
                socialTabService = new IEA.SocialTabs(); // creates new instance for common service

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                var self = this;

                self.settings = self.defaultSettings;
                self.componentJson = self.getModelJSON().socialCarousel;
                self.startIndex = self.settings.pageStartIndex;
                self.elm = self.$el;
                self.navClickPrev = 1;
                self.navClickNext = 1;

                // render social gallery on page load
                self._getJsonData();

                // reset quick panel on resizing
                socialTabService.resetPanelOnResize(self.elm, self.settings);
                            
                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/          

            // get json data from service
            _getJsonData: function() {
                var self = this,
                    options = {};

                options.componentJson = self.componentJson;
                options.startIndex = self.startIndex;

                if (self.componentJson.retrivalQueryParams.isExpandedViewRequired) {
                    options.noLazyLoad = true;
                }               
                
                options.queryParams = socialTabService.setDataParameters(options);
                options.servletUrl = self.componentJson.retrivalQueryParams.requestServletUrl;
                
                if (!$('.local-env').length) {
                     options.isTypePost = true;
                } else {
                    options.queryParams = '?';
                }

                if (options.queryParams !== '') {
                    options.onCompleteCallback = function(data) {
                        self._generateSocialList(data);
                    };

                    commonService.getDataAjaxMethod(options);
                }                  
            },

            // append social list based on the results
            _generateSocialList: function(data) {
                var self = this,
                serviceData = $.parseJSON(data),
                queryParamJson = self.componentJson.retrivalQueryParams,
                template = self.getTemplate('social-carousel-list', self.settings.template),
                combinedJson = null,
                numberOfPosts = queryParamJson.numberOfPosts,
                hashTags = '';

                if ($(queryParamJson.hashTags).length > 0) {
                    $(queryParamJson.hashTags).each(function(key, val) {
                        if (key === 0) {
                            hashTags += val.Id;
                        } else {
                            hashTags += '|' +val.Id;
                        }                       
                    });
                }

                combinedJson = {
                    'staticJson': self.componentJson,
                    'serviceData': serviceData.responseData,
                    'pageNum': (self.settings.pageNum - 1)*numberOfPosts,
                    'hashTags': hashTags
                }

                self.elm.find(self.settings.socialCarouselList).append(template(combinedJson));

                // carousel init
                if (self.settings.pageNum === 1 && self.elm.find(self.settings.thumbnail).length) {
                    self._initCarousel();

                    if (serviceData.responseData.totalResult > 1) {
                        self.elm.find(self.settings.nextButton).fadeIn(self.settings.animateSpeed);
                        self.elm.find(self.settings.nextButton).fadeOut(3000);
                    }                   
                }                           
            },

            // init carousel
            _initCarousel: function() {
                var self = this,
                carouselList = self.elm.find(self.settings.socialCarouselList),
                numberOfPosts,
                compClass = self.elm.attr('class'),
                socialGalleryCarouselItem = carouselList.find(self.settings.socialGalleryItem).attr('class').split(' ')[0];

                if (commonService.isMobile()) {
                    numberOfPosts = 3;
                } else {
                    numberOfPosts = this.componentJson.retrivalQueryParams.numberOfPosts;
                }

                carouselList.each(function() {
                    $(this).slick({
                        infinite: false,
                        arrows: true,
                        slidesToShow: numberOfPosts,
                        slidesToScroll: numberOfPosts,
                        slide: '.'+socialGalleryCarouselItem
                    })
                    .on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                        var $activeSlide = self.elm.find(self.settings.socialGalleryItem+'.active'),
                            direction, currentIndex;
                        
                        if (Math.abs(nextSlide - currentSlide) === 1) {
                            direction = (nextSlide - currentSlide > 0) ? 'right' : 'left';
                        } else {
                            direction = (nextSlide - currentSlide > 0) ? 'left' : 'right';
                        }

                        if (currentSlide === 0) {
                            self.navClickNext = 1;
                            self.navClickPrev = 1;
                        }

                        if ($activeSlide.length) {
                            if (direction === 'left') {
                                currentIndex = nextSlide;
                                self.navClickNext++;

                                if (self.navClickPrev > 1) {
                                    self.navClickPrev--;
                                }
                            } else if (direction === 'right') {
                                currentIndex = currentSlide - 1;
                                self.navClickPrev++;

                                if (self.navClickNext > 1) {
                                    self.navClickNext--;
                                }
                            }
                            self.elm.find(self.settings.socialGalleryItem+':eq('+currentIndex+') '+self.settings.thumbnail).click();                           
                        }
                    });
                });              
            },

            // event handler for load more click
            _loadMoreHandler: function(evt) {
                var self = this,
                numberOfPosts = self.componentJson.retrivalQueryParams.numberOfPosts,
                $this = $(evt.target)

                evt.preventDefault();

                self.settings.pageNum++;

                self.startIndex = self.startIndex + numberOfPosts;

                // render search listing
                self._getJsonData();           
            },

            // event handler for quick view panel close button
            _closePanel: function(evt) {
                evt.preventDefault();
                
                var _this = $(evt.target),
                    self = this,
                    item = self.elm.find(self.settings.socialCarouselList);

                socialTabService.setActiveParams(this.elm, self.settings, _this);
                TweenMax.to(window, 0.5, {scrollTo: {y: item.offset().top - 150}});
            },

            _closePanelonActiveSlide: function(evt) {
                evt.preventDefault();
                
                var _this = $(evt.target),
                    self = this,
                    item = self.elm.find(self.settings.socialCarouselList);

                socialTabService.setActiveParams(this.elm, self.settings, _this);
                TweenMax.to(window, 0.5, {scrollTo: {y: item.offset().top - 150}});
            },

            // event handler for showing quick view panel 
            _displayQuickPanel: function(evt) {
                evt.preventDefault();

                var _this = $(evt.target),
                    self = this,
                    active = 'active',
                    $currentItem = _this.parents(self.settings.socialGalleryItem),
                    currentIndex = $currentItem.data('index'),
                    $currentQuickPanel = self.elm.find(self.settings.quickPanelWrapper+'[data-index='+currentIndex+']');

                socialTabService.setActiveParams(self.elm, self.settings, parseInt(currentIndex));

                $currentItem.addClass(active);
                $currentQuickPanel.addClass(active);

                TweenMax.to(window, 0.5, {scrollTo: {y: $currentItem.offset().top + $currentItem.height()/2}});

                self.settings.disableHotKey = false;

                //keyword keys handler
                this._navigateThroughKeys($currentItem, currentIndex, $(self.settings.socialGalleryItem).length);
            },

            _navigateToItem: function(evt) {
                evt.preventDefault();

                var _this = $(evt.target),
                    self = this,
                    isCarouselClicked = false,
                    active = 'active',
                    $currentItem = _this.parents(self.settings.quickPanelWrapper),
                    currentIndex = $currentItem.data('index'),
                    itemLength = self.elm.find(self.settings.socialGalleryItem).length,
                    slickActiveLength = self.elm.find(self.settings.socialGalleryItem +'.slick-active').length;

                isCarouselClicked = false;

                if (_this.data('role') === 'next') {
                    currentIndex = currentIndex + 1;
                    var nextItemToScroll = (itemLength >= slickActiveLength*self.navClickNext) ? (slickActiveLength*self.navClickNext) : itemLength;

                    if (currentIndex === nextItemToScroll) {
                        self.elm.find('.slick-next').click();
                        isCarouselClicked = true;
                    }
                } else if (_this.data('role') === 'prev') {

                    if (currentIndex === slickActiveLength || currentIndex === itemLength - slickActiveLength*self.navClickPrev) {
                        self.elm.find('.slick-prev').click();
                        isCarouselClicked = true;
                    }
                    currentIndex = currentIndex - 1;
                }
                if (!isCarouselClicked) {
                    self.elm.find(self.settings.socialGalleryItem+'[data-index='+currentIndex+'] '+self.settings.thumbnail).click();
                }                
            },

            _navigateThroughKeys: function(item, index, last) {
                var self = this,
                    $close = item.find(self.settings.closeButton);
                
                // Hotkeys for navigation
                $(document).off('keyup').on('keyup', function(e) {
                    if (e.keyCode === 37 && !self.settings.disableHotKey && index > 0) {
                         socialTabService.prevItem(self.settings, index);
                    }
                    if (e.keyCode === 39 && !self.settings.disableHotKey && index < last) {                    
                        socialTabService.nextItem(self.settings, index);
                    }
                    if (e.keyCode === 27) {
                        self._setParams(self.settings.socialGalleryItem, self.settings.quickPanelWrapper, index);
                        TweenMax.to(window, 0.5, {scrollTo: {y: item.offset().top - item.height()/2}});
                    }
                });

                // Touch gestures with hammerjs for navigation
                self.$el.hammer().off('swipeleft').on('swipeleft', function(e) {                   
                    if (!self.settings.disableHotKey) {
                        socialTabService.prevItem(self.settings, index);
                    }
                }).off('swiperight').on('swiperight', function(e) {                 
                    if (!self.settings.disableHotKey) {
                        socialTabService.nextItem(self.settings, index);
                    }
                });
            }
        });
    });

    return SocialCarousel;
});
