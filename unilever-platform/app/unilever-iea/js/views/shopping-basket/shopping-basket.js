/*global define*/

define(['eGiftingService'], function() {
    'use strict';

    var eGiftingService = null;
    var ShoppingBasket = IEA.module('UI.shopping-basket', function(shoppingBasket, app, iea) {

        _.extend(shoppingBasket, {

            defaultSettings: {
                basketWrapper: '.c-shopping-basket__wrapper',
                basketList: '.c-shopping-basket__wrapper-cart',
                loader: '.o-preloader',
                emptyBasketWrap: '.c-shopping-basket__no-product',
                quantityCl: '.quantitywrap__cta-quantity',
                qualtitiWrapPrice: '#quantityWrapPrice',
                template: 'shopping-basket/partial/shopping-cart-list.hbss',
                orderSummary: '.c-order-summary',
                orderSummaryContainer: '.c-order-summary__container',
                cartButton: '.js-btn-checkout',
                disabledCl: 'disabled',
                storageName: 'productCartData'
            },

            events: {
                'click .quantitywrap__cta .o-btn': 'addQuantity', // calculate prive on Edit quantity
                'click .js-btn--edit': 'editGifts',  // update session data on Edit
                'click .js-btn--cancel': 'removeItem' //remove items from cart
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                eGiftingService = new IEA.EGifting();
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                var self = this;
                self.componentJson = self.getModelJSON().shoppingBasket;
                self.storageName = self.defaultSettings.storageName;
                self.parsedData = null;
                self.sessionData = eGiftingService.getItem(self.storageName);
                
                if (self.sessionData !== undefined && self.sessionData !== '' && self.sessionData !== null) {
                    self.parsedData = $.parseJSON(self.sessionData); 
                }

                // render shopping baket data
                self.renderShoppingBasket();
                this.triggerMethod('enable');        
            },

            renderShoppingBasket: function() {
                var self = this,
                    settings = self.defaultSettings,
                    combinedJson = null,
                    template = self.getTemplate('shopping-cart-list', settings.template),
                    $basketWrapper = $(settings.basketWrapper),
                    $basketList = $(settings.basketList);

                if (self.sessionData !== undefined && self.sessionData !== '' && self.sessionData !== null) {
                    if (self.parsedData.products.length) {
                         combinedJson =  {'shoppingBasket': {
                            'shoppingBasket': self.componentJson,
                            'productInfo': self.parsedData
                        }};

                        $basketList.append(template(combinedJson));
                    } else {
                        self.renderEmptyBasket();
                    }            
                    $basketWrapper.find(settings.loader).hide();                   
                } else {
                    $basketWrapper.find(settings.loader).hide();
                    self.renderEmptyBasket();
                }            
            },

            scrollPageOnTop: function() {
                // scroll page on top
                $('html, body').animate({
                    scrollTop : this.$el.offset().top - 200
                }, 1000);   
            },

            renderEmptyBasket: function() {
                var self = this,
                $emptyBasket = $(self.defaultSettings.emptyBasketWrap);
                
                $emptyBasket.append('<h3 class="o-text__heading-3">'+ self.componentJson.static.emptyBasketCopy + '</h3>');
                // scroll page on top
                self.scrollPageOnTop();
            },

            editGifts: function(evt) {                
                var $this = $(evt.target),
                storageData = this.parsedData,
                dataIndex = $this.data('index'),
                listItem =  $this.parents('li'),
                dataToBeStored = $(storageData.products)[dataIndex];

                dataToBeStored.isEdit = true;
                dataToBeStored.quantity = parseInt(listItem.find(this.defaultSettings.quantityCl).text());
                eGiftingService.setItem(this.storageName,  JSON.stringify(storageData));
            },

            addQuantity: function(evt) {
                var quantity, giftId,
                    self = this,
                    settings = self.defaultSettings,
                    componentData=self.getModelJSON().shoppingBasket,
                    productInfo = self.parsedData.products,
                    maximumItem = parseInt(componentData.configs.maximumItemPerProduct, 10),
                    $this = $(evt.target),
                    orderSummaryTotal = 0,
                    parentElm = $this.parent(),
                    parentsElem = $this.parents('.c-shopping-basket__quantitywrap'),
                    giftId = $this.parents('li').find(".c-shopping-basket__cta .o-btn").data('index'),
                    dataToBeStored = $(productInfo)[giftId];

                    quantity = parseInt(parentElm.find(settings.quantityCl).text(), 10);                   

                    if (quantity > 1) {
                        $('.o-btn--remove').removeClass('o-btn--remove__disabled');
                    }

                    if ($this.hasClass('o-btn--add') && (quantity < maximumItem)) {
                        quantity++;
                    } else if ($this.is('.o-btn--remove')) {
                        if (quantity > 1) {
                            quantity--;
                        } else {
                            $this.addClass('o-btn--remove__disabled');
                        }                     
                    }
                               
                    parentElm.find(settings.quantityCl).text(quantity);

                    $('#giftid_' + giftId+' #updateQuantity').text(quantity); 
                                      
                    $(parentsElem.find(settings.qualtitiWrapPrice)).text(((parseFloat(dataToBeStored.price) + parseFloat(dataToBeStored.customisedWrappingPrice))*quantity).toFixed(2));

                    $(self.parsedData.products)[giftId].quantity = quantity;
                    $(self.parsedData.products)[giftId].priceWithQuantity = parseFloat(dataToBeStored.price*quantity).toFixed(2);
                    $(self.parsedData.products)[giftId].wrappingPriceWithQuantity = parseFloat(dataToBeStored.customisedWrappingPrice*quantity).toFixed(2);

                    eGiftingService.setItem(self.storageName,  JSON.stringify(self.parsedData));
                    //update total amount in order summary
                    eGiftingService.calculatePrice(JSON.stringify(self.parsedData));                    
            },
            
            // Remove item from the basket
            removeItem: function(evt) {
                var self = this,
                    settings = self.defaultSettings,
                    productInfo = self.parsedData.products,
                    sessionData = self.parsedData,
                    itemVal = 0,
                    $this = $(evt.target),
                    itemIndex = $this.data('index'),
                    
                    //Getting Values from Local Storage
                    productId = productInfo[itemIndex].productId,
                    productName = productInfo[itemIndex].title,
                    productPrice = productInfo[itemIndex].price,
                    productQuantity = productInfo[itemIndex].quantity;
                
                $(settings.orderSummaryContainer+':eq('+itemIndex+')').remove();

                if (itemVal) {
                    productInfo = $.parseJSON(productInfo);
                }
                
                productInfo.splice(itemIndex, 1);
                
                if (productInfo.length === 0) {
                    productInfo = [];
                    sessionData.products = productInfo;
					$(settings.cartButton).addClass(settings.disabledCl);
                    $(settings.orderSummary).remove();
                } else {
                    productInfo = JSON.stringify(_.compact(productInfo)); 
                    sessionData.products = $.parseJSON(productInfo);     
                }                  

                eGiftingService.setItem(self.storageName, JSON.stringify(sessionData));

                // re render shopping basket
                $(settings.basketList).html('');
                self.renderShoppingBasket();

                //update total amount in order summary
                eGiftingService.calculatePrice(JSON.stringify(sessionData));

                // scroll page on top
                self.scrollPageOnTop();
                itemVal++; 
                
            }
        });
    });

    return ShoppingBasket;
});
