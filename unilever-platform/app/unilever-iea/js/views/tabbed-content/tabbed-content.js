/*global define*/

define(function() {

    'use strict';

    var TabbedContent = IEA.module('UI.tabbed-content', function(module, app, iea) {

        _.extend(module, {
            // component override logic goes here
        });

    });

    return TabbedContent;
});
