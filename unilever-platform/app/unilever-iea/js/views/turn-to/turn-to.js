/*global define*/

define(['turnToService'],function() {
    'use strict';

     var turnToService=null;   

    var TurnTo = IEA.module('UI.turn-to', function(turnTo, app, iea) {

        _.extend(turnTo, {

            events: {
                'click .TurnToItemTeaser': '_scrollToEvent'
            },
            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();                    
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                var self = this, 
                options = {},
                turnToData = self.getModelJSON().turnTo;

                this.options={
                    siteKey:turnToData.siteKey,
                    setupType:turnToData.setupType,
                    productID:turnToData.productID,
                    src:turnToData.source,
                    itemJS:turnToData.itemJS
                };
                turnToService = new IEA.turnToService();
                turnToService.initTurnTo(this.options);

                //Analytics
                this._trackTurnToEvents();
            },

            /**************************PRIVATE METHODS******************************************/          
            
            /**
             * scroll to CTA to questions
             * name: _scrollToEvent
             */
            _scrollToEvent: function(event) {
                var $this = $(event.currentTarget),
                    wrapperId = 'TurnToContent';
                
                // Remove "link" from the ID
                wrapperId = wrapperId.replace('link', '');
                
                // Scroll
                $('html,body').animate({
                  scrollTop: $("#" + wrapperId).offset().top
                },'slow');
            },

            /**
             * track component info
             * name: _ctComponentInfo
             */
            _ctComponentInfo: function() {
                var self = this,
                    compName = self.$el.find('[data-componentname]').data('componentname'),
                    compVar = self.$el.find('[data-component-variants]').data('component-variants'),
                    compPos = self.$el.find('[data-component-positions]').data('component-positions');
                
                digitalData.component = [];
                digitalData.component.push({
                    'componentInfo' :{
                        'componentID': compName,
                        'name': compName
                    },
                    'attributes': {
                        'position': compPos,
                        'variants': compVar
                    }
                });
            },

            /**
             * track analytics on question/answer submissions
             * name: _ctTurnToTracking
            */
            _ctTurnToTracking: function(label) {
                var ev = {};

                this._ctComponentInfo();

                ev.eventInfo = {
                  'type':ctConstants.trackEvent,
                  'eventAction': ctConstants.turnto,
                  'eventLabel' : label
                };
                ev.category = {'primaryCategory':ctConstants.custom};
                ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
                digitalData.event.push(ev);
            },

            /**
             * bind events on question/answer submissions
             * name: _trackTurnToEvents
            */

            _trackTurnToEvents: function() {
                var self = this;
                // click on 'submit a question'
                $(document).on('click', '#TT2askOwnersBtn', function() {
                    self._ctTurnToTracking('Question Submit');
                }).on('click', '#TurnToContent .TT3answerBtn', function() {
                    self._ctTurnToTracking('Answer Submit');
                }).on('click', '#TurnToContent .TTflagAnswer a', function() {
                    self._ctTurnToTracking('Rated Inaccurate');
                });
            }
        });
    });

    return TurnTo;
});
