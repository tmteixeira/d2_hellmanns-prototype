/*global define*/

define(['slick', 'handlebars', 'TweenMax', 'commonService'], function (Handlebars) {
    'use strict';

    var MultipleRelatedArticles = IEA.module('UI.multiple-related-articles', function (multipleRelatedArticles, app, iea) {

		var commonService = null,
            self = this,
            DEFAULT_MOBILE_P = 480,
            DEFAULT_MOBILE_L = 667,
            isMobile = null,
			multipleRelatedArticlesData = null;


        _.extend(multipleRelatedArticles, {

            '$carousel': '.js-multiple-related-articles-carousel',
            '$carouselSlide': 'div.c-related-article-wrapper',
            'firstLoad': true,
            default: {
                'carouselTabP': {
                    infinite: false,
                    speed: 1250,
					arrows: true,
                    slidesToShow: 3,
					slidesToScroll: 3,
                    focusOnSelect: true,
                    centerPadding: '210px'

                },
                'carouselMobL': {
                    infinite: false,
                    speed: 1250,
                    slidesToShow: 1,
                    focusOnSelect: true,
                    centerMode: true,
                    centerPadding: '25%'

                },
                'carouselMobP': {
                    infinite: false,
                    speed: 1250,
                    slidesToShow: 1,
                    focusOnSelect: true,
                    centerMode: true,
                    centerPadding: '20%'

                }
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },


            /**
             * start carousels
             * @method carouselInit
             * @return
             */
            carouselInit: function () {

                $(self.$carousel).on('click', self.$carouselSlide, function (e) {
                    var item = $(this),
                        container = $(self.$carousel).find(self.$carouselSlide);
                    
                });

                setTimeout(function () {
                   if(multipleRelatedArticlesData.multipleRelatedArticles.articles.length > 3) {
					   	self.setCarouselSize(self);
						$(self.$carousel).slick('slickGoTo', 1);
						$(self.$carousel).slick(self.default.carouselTabP);
						$(self.$carousel).on('beforeChange', function(event, slick, currentSlide, nextSlide) {
							var $nextSlide = $(self.$carousel).find(self.$carouselSlide).eq(nextSlide);							
						});
				   }
                }, 100);

                //change carousel type according to view size
                app.on('window:resized', function () {
                    self.setCarouselSize(self);
                })


            },
            setCarouselSize: function (self) {

                var $slideItems = $('.js-multiple-related-carousel-placeholder').find(self.$carouselSlide);
                //remove slick if it has been loaded
                if (self.firstLoad) {
                    self.firstLoad = false;
                } else {
                    $(self.$carousel).slick('unslick');
                }

                var $width = $(window).width();

                // create the device appropriate carousel


                if ($width <= DEFAULT_MOBILE_P) {
                    // Less than or equal to 480
                    $(self.$carousel).slick(self.default.carouselMobP);
                } else if ($width > DEFAULT_MOBILE_P && $width <= DEFAULT_MOBILE_L) {
                    // Greater than 480 AND less than or equal to 667
                    $(self.$carousel).slick(self.default.carouselMobL);
                } else if ($width > DEFAULT_MOBILE_L) {
                    // Greater than 667
                    $(self.$carousel).slick(self.default.carouselTabP);
                }

                //$(self.$carousel).slick('refresh',true);

                //scale the middle selected slide and got to the middle
                $(self.$carousel).slick('slickGoTo', 1);
                
            },

            init: function () {
                //cloned the carousel slides for the desktop & tablet version
                var string = $('.js-multiple-related-articles-carousel').html();
                // $('.js-multiple-related-carousel-placeholder').html(string);
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;

                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */

            enable: function () {
                this.triggerMethod('beforEnable');

                multipleRelatedArticlesData = this.getModelJSON();                
                commonService = new IEA.commonService();
                self.isMobile = (commonService.isMobile() || $( window ).width() <= DEFAULT_MOBILE_L)? true: false;

                setTimeout(_.bind(function () {
                    if (this.$el.find('.js-articles-container').attr('data-type') === 'carousel') {

                        this.init();
                        this.carouselInit();
                    }
                }, this), 100);

                this.triggerMethod('enable');
            }

            /**************************PRIVATE METHODS******************************************/

            /**
             component private functions are written with '_' prefix to diffrentiate between the public methods
             example:

             _showContent: function (argument) {
                    // body...
                }

             */

        });
    });

    return MultipleRelatedArticles;
});
