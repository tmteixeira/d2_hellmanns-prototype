/*global define*/

define(['slick', 'commonService', 'binWidgetService', 'quickPanelService', 'ratings'], function () {
    'use strict';

    var RelatedProducts = IEA.module('UI.related-products', function(relatedProducts, app, iea) {
        
        var commonService = null,
            ratingService = null,
            quickPanelService = null,
            binWidgetService = null,
            self = this,
            isMobile = null,
            componentJson = null;

        _.extend(relatedProducts, {
            
            '$carousel': '.js-related-products-carousel',
            '$carouselSlide': '.c-related-products-item',
            'firstLoad': true,
            
            defaultSettings: {
                'carouselTabP': {
                    infinite: false,
                    speed: 1250,
                    arrows: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    focusOnSelect: true,
                    dots: true
                },
                'carouselMob': {
                    infinite: false,
                    speed: 1250,
                    slidesToShow: 1,
                    focusOnSelect: true,
                    centerMode: true,
                    centerPadding: '25%',
                    dots: true
                },
                binWidgetTemplate: 'partials/bin-widget-listing.hbss',
                relatedProductItem: '.js-related-products-item',
                quickPanelWrapper: '.c-related-products-quickview',
                quickPanelCta: '.c-related-products__quick-cta',
                quickPanelNav: '.js-related-products__arrow',
                itemMargin: 30,
                binButtonCTA: '.js-bin-btn',
                binOvarlay: '.bin-widget-overlay',
                binWidgetWrapper: '.js-related-products-quickview__bin'

            },

            events: {
                'click .c-related-products__quick-cta': '_showQuickPanel',
                'click .js-btn-related-products-panel-close' : '_closePanel', //hide quick panel
                'click .js-related-products__arrow' : '_navigateToItem', // navigate quick panel through next/prev button
                'change .js-variant-dropdown-select': '_updatePanelOnDropdownChange'
            },
            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                ratingService = new IEA.ratings();
                quickPanelService = new IEA.QuickPanel(); // creates new instance for quick panel service
                binWidgetService = new IEA.BinWidget(); // creates new instance for bin widget service
                this.triggerMethod('init');
            },
            
            /**
             * start carousels
             * @method carouselInit
             * @return
             */
            carouselInit: function () {
                var $carousel = $(self.$carousel);
                setTimeout(function () {
                    self._setCarouselSize(self, $carousel);
                    $carousel.slick('slickGoTo', 1);
                    $carousel.slick(self.defaultSettings.carouselTabP);
                }, 100);
            },
            
            init: function () {
                //cloned the carousel slides for the desktop & tablet version
                var string = $('.js-related-products-carousel').html();
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                this.componentJson = this.getModelJSON().relatedProducts;
                
                var options = {};

                this.relatedProductItem = this.defaultSettings.relatedProductItem;
                this.quickPanelWrapper = this.defaultSettings.quickPanelWrapper;
                
                options.ratingReview = this.componentJson.products.review;

                if (this.componentJson.products.productList.length) {
                    options.productId = this.componentJson.products.productList;
                    options.brandName = this.componentJson.brandName;
                    options.ratingObj = this.componentJson.products.productList[0].ratings;
                    ratingService.initRatings(options);
                }
                
                commonService = new IEA.commonService();
                self.isMobile = commonService.isMobile() ? true: false;

                setTimeout(_.bind(function () {
                    if (this.$el.find('.js-products-container').attr('data-type') === 'carousel') {

                        this.init();
                        if($('.js-related-products-carousel').hasClass('enable-related-products-carousel')) {
                            this.carouselInit();
                        }
                    }
                }, this), 100);
                
                //carousel click Analytics
                this._ctCarouselClick();

                //bin widget
                this._binWidgetInit();
                
                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/          

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */
            
            _setCarouselSize: function (self, $carousel) {
                var $slideItems = $('.js-related-products-carousel-placeholder').find(self.$carouselSlide);
                //remove slick if it has been loaded
                if (self.firstLoad) {
                    self.firstLoad = false;
                } else {
                    $carousel.slick('unslick');
                }
                
                if(self.isMobile) {
                    $carousel.slick(self.defaultSettings.carouselMob);
                } else {
                    $carousel.slick(self.defaultSettings.carouselTabP);
                }
                
                //scale the middle selected slide and got to the middle
                $carousel.slick('slickGoTo', 1);
            },
            
            // Carousel click tracking
            _ctCarouselClick: function() {
                var ev = {},
                    self = this,
                    $carousel = $(self.$carousel),
                    $carouselItem = self.$el.find($carousel),
                    label;

                $(document).on('click', '.slick-arrow, .slick-dots li', function(evt) {
                    var _this = $(this),
                    hostedUrl = window.location.href;

                    self._ctComponentInfo();

                    if (_this.is('.slick-arrow')) {
                        label = _this.text();
                    } else {
                        label = 'button#' + parseInt(_this.find('button').text());
                    }

                    ev.eventInfo = {
                      'type': ctConstants.trackEvent,
                      'eventAction': ctConstants.carouselClick,
                      'eventLabel' : label + ' - ' + hostedUrl
                    };

                    ev.category = {'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                });
            },
            
            // component attributes tracking
            _ctComponentInfo: function() {
                var self = this,
                    compName = self.$el.find('[data-componentname]').data('componentname'),
                    compVar = self.$el.find('[data-component-variants]').data('component-variants'),
                    compPos = self.$el.find('[data-component-positions]').data('component-positions');
                
                digitalData.component = [];
                digitalData.component.push({
                    'componentInfo' :{
                        'componentID': compName,
                        'name': compName
                    },
                    'attributes': {
                        'position': compPos,
                        'variants': compVar
                    }
                });
            },

            /**
             * show/hide quick panel
             * @method _showQuickPanel
             * @return 
            */
            _showQuickPanel: function(evt) {
                evt.preventDefault();

                var $this = $(evt.currentTarget),
                    self = this,
                    active = 'active',
                    $currentItem = $this.parents(self.relatedProductItem),
                    currentIndex = $currentItem.data('index'),
                    $currentQuickPanel = self.$el.find(self.quickPanelWrapper+'[data-index='+currentIndex+']'),
                    options = {};

                options = {
                    nextButton: $(self.defaultSettings.quickPanelNav+'-next'),
                    prevButton: $(self.defaultSettings.quickPanelNav+'-prev'),
                    animateSpeed: 1000,
                    quickPanelWrapper: self.quickPanelWrapper,
                    listingItem: self.relatedProductItem,
                    itemMargin: self.defaultSettings.itemMargin,
                    quickPanelCta: self.defaultSettings.quickPanelCta
                };
                
                quickPanelService.setActiveParams(options, parseInt(currentIndex));

                $currentItem.addClass(active);
                               
                self.$el.find(self.relatedProductItem).each(function (index, el) {
                    var panelHeight = 0,
                        cols = 3,
                        row = currentIndex - currentIndex % cols;

                    if (index >= row && index <= row + (cols - 1)) {
                        panelHeight = $currentQuickPanel.height();
                    }
                    if (panelHeight !== 0) {
                        $(el).css("margin-bottom", panelHeight+ options.itemMargin*2);
                    }                   
                });
                
                $currentQuickPanel.addClass(active).css('top', $currentItem.position().top + $currentItem.height() + options.itemMargin);
                   

                TweenMax.to(window, 0.5, {scrollTo: {y: $currentItem.offset().top + $currentItem.height()/2}});

                self.defaultSettings.disableHotKey = false;

                //keyword keys handler
                self._navigateThroughKeys($currentItem, currentIndex, self.$el.find(self.relatedProductItem).length);
            },

            /**
             * open quick panel on clicking on current item 
             * @method _navigateToItem
             * @return 
            */           
            _navigateToItem: function(evt) {
                evt.preventDefault();

                var $this = $(evt.currentTarget),
                    self = this,                  
                    active = 'active',
                    $currentItem = $this.parents(self.quickPanelWrapper),
                    currentIndex = $currentItem.data('index');

                if ($this.data('role') === 'next') {
                    currentIndex = currentIndex + 1;
                } else if ($this.data('role') === 'prev') {
                    currentIndex = currentIndex - 1;
                }
                self.$el.find(self.relatedProductItem+'[data-index='+currentIndex+'] '+self.defaultSettings.quickPanelCta).click();
            },

            /**
             * close quick panel
             * @method _closePanel
             * @return 
            */
            _closePanel: function(evt) {
                evt.preventDefault();                
                
                var self = this,
                    $this = $(evt.currentTarget),
                    options = {};

                options = {
                    nextButton: self.$el.find(self.defaultSettings.quickPanelNav+'-next'),
                    prevButton: self.$el.find(self.defaultSettings.quickPanelNav+'-prev'),
                    animateSpeed: 1000,
                    quickPanelWrapper: $this.parents(self.quickPanelWrapper),
                    listingItem: self.relatedProductItem,
                    itemMargin: self.defaultSettings.itemMargin,
                    quickPanelCta: self.defaultSettings.quickPanelCta
                };

                quickPanelService.closePanel(options);
            },

            /**
             * navigate quick panel through next/previous button
             * @method _closePanel
             * @return 
            */
            _navigateThroughKeys: function(item, index, last) {
                var self = this,
                    settings = self.defaultSettings,
                    options = {};

                options = {
                    nextButton: self.$el.find(self.defaultSettings.quickPanelNav+'-next'),
                    prevButton: self.$el.find(self.defaultSettings.quickPanelNav+'-prev'),
                    animateSpeed: 1000,
                    quickPanelWrapper: self.quickPanelWrapper,
                    relatedProductItem: self.relatedProductItem,
                    itemMargin: self.defaultSettings.itemMargin,
                    quickPanelCta: self.defaultSettings.quickPanelCta
                };
                
                // Hotkeys for navigation
                $(document).off('keyup').on('keyup', function(e) {
                    if (e.keyCode === 37 && !settings.disableHotKey && index > 0) {
                         quickPanelService.prevItem(options, index);
                    }
                    if (e.keyCode === 39 && !settings.disableHotKey && index < last) {                    
                        quickPanelService.nextItem(options, index);
                    }
                    if (e.keyCode === 27) {
                        TweenMax.to(window, 0.5, {scrollTo: {y: item.offset().top - item.height()/2}});
                    }
                });

                // Touch gestures with hammerjs for navigation
                self.$el.hammer().off('swipeleft').on('swipeleft', function(e) {                   
                    if (!settings.disableHotKey) {
                        quickPanelService.prevItem(options, index);
                    }
                }).off('swiperight').on('swiperight', function(e) {                 
                    if (!settings.disableHotKey) {
                        quickPanelService.nextItem(options, index);
                    }
                });
            },

            /**
            @bin widget init
            @method _binWidgetInit
            @return
            **/

            _binWidgetInit: function() {
                //intiate Bin Widget
                var options = {
                    shopNow: this.componentJson.products.shopNow,
                    modalCl: '.js-modal-etale'
                };

                if (options.shopNow) {
                    binWidgetService.injectBinJS(options);
                    this._binWidgetHandler(options);
                    $('.icon-cross').attr('brand', this.componentJson.brandName);
                }               
            },

            /**
            @bin widget handler
            @method _binWidgetHandler
            @return 
            **/

            _binWidgetHandler: function(options) {
                var self = this,
                    productId,
                    $variantDropdown = self.$el.find(self.defaultSettings.variantDropdown),
                    productData = self.componentJson.products,
                    selectedIndex = self.$el.find(self.defaultSettings.variantDropdown+' option:selected').index();

                $(self.defaultSettings.binButtonCTA).click(function(evt) {
                    evt.preventDefault();
                    var $this = $(this),
                        productListIndex = parseInt($this.parents(self.quickPanelWrapper).data('index'));

                   productId = $this.data('shopnow-productid');
                    
                    if (options.shopNow.serviceProviderName === 'cartwire') {
                        loadsWidget(options.productId, this,'retailPopup','en');
                    
                    } else if (options.shopNow.serviceProviderName === 'etale') {
                        $this.next(self.defaultSettings.binOvarlay).show();
                    }
                });
                
                $('.js-btn-close').click(function(evt) {
                    evt.preventDefault();
                    $(self.defaultSettings.binOvarlay).hide();
                });

            },
            /**
             * toggle store search wrapper on label click
             * @method _toggleStoreSearch
             * @return 
             */

            _toggleStoreSearch: function(evt) {
                var self = this,
                    $this = $(evt.currentTarget),
                    service = new IEA.accordion(), // creates new instance
                    options = {},
                    $currentQuickPanel = $this.parents(self.quickPanelWrapper),
                    currentIndex = $currentQuickPanel.data('index'),
                    $storeWrapper = $currentQuickPanel.find('.c-store-search-pdp-header__body');

                options.wrapper = this.defaultSettings.accordion;
                service._handleExpandCollapse(evt, options);
                
                // update listing margin
                self.$el.find(self.relatedProductItem).each(function (index, el) {
                    var panelHeight = 0,
                        cols = 3,
                        row = currentIndex - currentIndex % cols;

                    if (index >= row && index <= row + (cols - 1)) {
                        panelHeight = $currentQuickPanel.height();
                    }
                    if (panelHeight !== 0) {
                        $(el).css("margin-bottom", panelHeight+ parseInt($storeWrapper.height()));
                    }                   
                });
            },

            /**
             * search zip code based on user location
             * @method _shareLoc
             * @return 
             */

            _findLocation: function() {             
                var self = this,
                    pos, reverseGeoCode;

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        }
                        reverseGeoCode(pos.lat, pos.lng, function(zip) {
                           self.$el.find('.active .input-zipcode').val(zip);
                        }); 
                    });
                    
                    reverseGeoCode = function(lat, lng, callback) {
                        var mapServiceUrl = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=',
                            googleService = mapServiceUrl + lat + ',' + lng + '&sensor=false';
                        
                        $.getJSON(googleService, getMapData);

                        function getMapData(data) {
                            var addressComponents = data.results[0].address_components,
                                postalCode = '', 
                                countryCode = '', 
                                type = '';

                            if (addressComponents) {                                
                                _.each(addressComponents, function(item, index) { 
                                    if (typeof item.types == "string") {
                                        type = item.types;
                                    } else if (typeof item.types == "object") {
                                        type = item.types[0];
                                    }
                                    
                                    if (type === 'postal_code') {
                                        postalCode = item.long_name;
                                    }
                                    if (type === 'postal_town') {
                                        self.location = item.long_name;
                                    }
                                });
                            
                                if (typeof callback === 'function') {
                                    callback(postalCode);
                                }
                            }
                        }   
                    }
                }               
            },

            /**
             * submit buy in store form
             * @method _submitBuyStoreForm
             * @return 
             */

            _findStores: function(evt) {
                evt.preventDefault();

                var $this = $(evt.currentTarget),
                    $postalCode = $this.parents('form').find('.input-zipcode'),
                    isZipcodeValid = this._validateZipCode($postalCode.val(), $postalCode.data('regex')),                   
                    $formGroup = $this.parents('.form-group'),
                    $helpBlock = this.$el.find('.help-block');
                
                if (isZipcodeValid && $postalCode.val() !== '') {
                    $helpBlock.addClass('hidden');
                    $formGroup.removeClass('has-error');
                    $this.parents('form').submit();
                    // track store search analytics
                   this._ctStoreSearch();
                } else {
                    $formGroup.addClass('has-error');
                    $helpBlock.removeClass('hidden');
                    $postalCode.focus();
                    return false;
                }              
            },

            /**
             * Zip code validation based on regex provided
             * @method _validateZipCode
             * @return 
             */
            
            _validateZipCode: function(fieldVal, regExp) {
                var regEx = regExp, 
                    pattern = new RegExp(regEx), 
                    isValidZip = pattern.test(fieldVal);

                return isValidZip;
            },

            _ctStoreSearch: function() {
                var ev = {},
                    $this = this.$el.find('.c-product-listing-v2'),
                    compName = $this.data('componentname'),
                    compVar = $this.data('component-variants'),
                    compPos = $this.data('component-positions'),
                    label = 'StoreSearchSubmitted < ' + $('#pc').val() + ' | ' + this.location;
            
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    digitalData.component = [];
                    digitalData.component.push({
                        'componentInfo' :{
                            'name': compName
                        },
                        'attributes': {
                            'position': compPos,
                            'variants': compVar
                        }
                    });

                    digitalData.product = [];
                    digitalData.product.push({
                       'productInfo' :{
                           'productID': $('#pvi').val(),
                           'productName': $('.js-product-images-thumbnail').data('product-name'),
                        },
                        'category':{
                            'primaryCategory': $('.c-product-images').data('product-category')
                        }
                    });
                    ev.eventInfo = {
                      'type': ctConstants.trackEvent,
                      'eventAction': ctConstants.storesearch,
                      'eventLabel' : label
                    };
                    ev.category = {'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                }
            },

            /**
             * Select box price and unit update, used to update the label text
             * @method _updatePanelOnDropdownChange
             * @return
             */

              _updatePanelOnDropdownChange: function (evt) {
                evt.preventDefault();

                var self = this,
                    $this = $(evt.currentTarget),
                    $parentWrapper = $this.parents(self.quickPanelWrapper),
                    currentIndex = evt.currentTarget.selectedIndex,
                    listingIndex = parseInt($parentWrapper.data('index')),
                    currentData = self.componentJson.products.productList[listingIndex].productsDetail[currentIndex],
                    template = self.getTemplate('variant-product', self.defaultSettings.variantProductTemplate),
                    binWidgetTemplate = self.getTemplate('bin-widget-listing', self.defaultSettings.binWidgetTemplate),
                    $retail = $parentWrapper.find('.c-dropdown-label__retail'),
                    $unit = $parentWrapper.find('.c-dropdown-label__unit'),
                    $price = $parentWrapper.find('.c-dropdown-label__price');

                if (currentData) {
                    $price.html(self.componentJson.products.productList[listingIndex].currencySymbol + currentData.price);
                    $unit.html(currentData.unit);
                    
                    if (currentData.price) {
                        $retail.html(currentData.retail);
                    } else {
                        $retail.html("");   
                    }

                    //update copy text
                    $parentWrapper.find(this.quickPanelWrapper+'__title').text(currentData.perfectForTitle);
                    $parentWrapper.find(this.quickPanelWrapper+'__productname').text(currentData.name);
                    $parentWrapper.find(this.quickPanelWrapper+'__desc').html(currentData.shortPageDescription);

                    //update product id
                    $parentWrapper.find('#pvi').val(currentData.smartProductId);

                    //update product image template
                    $this.parents(this.quickPanelWrapper).find(this.quickPanelWrapper+'-item-image__img').html(template(currentData.images[0]));

                    //update bin widget
                    $parentWrapper.find(self.defaultSettings.binButtonCTA).attr('data-shopnow-productid', currentData.smartProductId);

                    //reinitiate lazy load method
                    this._initiateLazyLoad();
                }                
            }

        });
    });

    return RelatedProducts;
});