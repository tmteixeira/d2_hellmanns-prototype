/*global define*/

define(['slick', 'commonService', 'TweenMax', 'videoPlayerServiceV2'], function() {
    'use strict';

    var CarouselV2 = IEA.module('UI.carousel-v2', function(carouselV2, app, iea) {
        
        var videoPlayerServiceV2 = null,
        commonService = null;

        _.extend(carouselV2, {

            _isVideoOpened: false,
            
            defaultSettings: {
                carouselItem: '.js-carousel-v2-item',
                carouselNav: '.js-carousel-v2-nav',
                progressBarTrack: '.js-progress-bar__track',
                readMoreSelector: '.js-btn-readmore',
                heroWrapper: '.c-hero-v2',
                toggleClass: 'is-open',
                readMoreCopy: '.c-hero-v2__longcopy',
                videoWrapper: '.js-video-player',
                videoHandler: '.js-player-handler',
                contentWrapper: '.c-hero-v2__wrapper',
                isVideoOpen: 'is-video-player__open',
                videoButton: '.js-btn-video',
                multipleVideoInstances: false
            },

            events: {
                'click .js-btn-readmore': '_toggleReadMore',
                'click .js-player-handler' : '_videoPlayHandler',
                'click .o-btn--pause' : '_togglePlayPause'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                
                // video service options
                var options = {},
                    self = this;                
                
                options = {
                    selector: self.defaultSettings.videoHandler,
                    videoWrapper : self.defaultSettings.videoWrapper,
                    view: self,
                    previewImage: '.js-preview-image',
                    multipleVideoInstances: self.defaultSettings.multipleVideoInstances
                };

                videoPlayerServiceV2 = new IEA.videoPlayerServiceV2();
                videoPlayerServiceV2.initVideoPlayer(options);

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */

            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */

            enable: function () {
                this.triggerMethod('beforEnable');

                commonService = new IEA.commonService();

                this.componentJson = this.getModelJSON().carouselV2;
                this.timer = (this.componentJson.intervalsOfRotation)*1000; //mutiply the timer by 1000 as we are getting the timer value in 1-10

                //init carousel method
                this._initCarousel();

                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/          

            // init carousel
            _initCarousel: function() {
                var self = this,
                settings = self.defaultSettings,
                carouselItem = self.$el.find(settings.carouselItem),
                carouselNav = self.$el.find(settings.carouselNav),
                carouselItemsLength = self.componentJson.slides.length;

                $(carouselItem).slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    pauseOnHover: false,
                    autoplay: (self.timer) ? true : false,
                    autoplaySpeed: self.timer,
                    fade: (commonService.isMobile() === true) ? false : true,
                    asNavFor: (commonService.isMobile()) ? '' : carouselNav,
                    dots: (commonService.isMobile() === true) ? true : false

                }).on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                    $(self.$el.find(settings.progressBarTrack)).animate({
                        width: (100/carouselItemsLength)*(nextSlide +1) +'%'
                    }, 1000);
                    
                    //fix the carousel nav active slide issue
                    if (carouselItemsLength <= 4) {
                        $(carouselNav).find('.slick-slide').removeClass('slick-current').eq(nextSlide).addClass('slick-current');
                    }
                });

                $(carouselNav).slick({
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    asNavFor: carouselItem,
                    arrows: false,
                    focusOnSelect: true
                });  
            },

             // toggle play pause button
            _togglePlayPause: function(evt) {
                evt.preventDefault();

                var self = this,
                    $this = $(evt.currentTarget),
                    $carouselItem = $(this.defaultSettings.carouselItem),
                    playBtn = 'o-btn--play';

                if ($this.is('.'+playBtn)) {
                    $this.removeClass(playBtn);
                    $carouselItem.slick('slickPlay');
                } else {
                    $this.addClass(playBtn);
                    $carouselItem.slick('slickPause');
                }
            },

            _toggleReadMore: function(evt) {
                var self = this,
                $this = $(evt.currentTarget),
                settings = self.defaultSettings;

                evt.preventDefault();

                if ($this.is('.'+settings.toggleClass)) {
                    TweenMax.to(settings.readMoreCopy, 1, {height: 0});
                    $this.removeClass(settings.toggleClass);
                    
                    //Analytics read more button if collapsed
                    self._ctReadMore('close');
                } else {
                    TweenMax.set(settings.readMoreCopy, {height: 'auto'});
                    TweenMax.from(settings.readMoreCopy, 1, {height: 0});
                    $this.addClass(settings.toggleClass);

                    //Analytics read more button if expanded
                    self._ctReadMore('open');
                }
            },


            /**************************VIDEO PLAYER METHODS******************************************/ 

            _videoPlayHandler: function(evt) {
                if (this.timer) {
                    this.$el.find('.o-btn--pause:not(.o-btn--play)').click();   
                }                        
            },

            // component attributes tracking
            _ctComponentInfo: function() {
                var self = this,
                    compName = self.$el.find('[data-componentname]').data('componentname'),
                    compVar = self.$el.find('[data-component-variants]').data('component-variants'),
                    compPos = self.$el.find('[data-component-positions]').data('component-positions');
                
                digitalData.component = [];
                digitalData.component.push({
                    'componentInfo' :{
                        'componentID': compName,
                        'name': compName
                    },
                    'attributes': {
                        'position': compPos,
                        'variants': compVar
                    }
                });
            },

            // Hero read More link Analytics
            _ctReadMore: function(openCloseStatus) {
                var ev = {};
            
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {                   
                    this._ctComponentInfo();                  
                    
                    ev.eventInfo={
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.readMore,
                        'eventLabel' : openCloseStatus
                    };
                       
                    ev.attributes = {'nonInteraction': 1};
                    ev.category = {'primaryCategory': ctConstants.custom};
                    digitalData.event.push(ev);
                }
            },

            /**
             * Player state change callback with player states
             * @method _playerStateChange
             * @return 
             */

            _playerStateChange: function(videoPlayer, state) {                
                var $player = $(videoPlayer.h);
                if (state) {
                    /*switch (state.data) {
                        case -1:
                            console.log('buffering');
                            break;
                        case 0:
                            console.log('video completes');
                            break;
                        case 1:
                            console.log('playing');
                            break;
                        case 2:
                            console.log('pause');
                            break;
                        case 3:
                            console.log('replay');
                            break;
                    }
                    */
                    // video analytics
                    this._ctTag($player.data('video-source'), $player.data('video-id'), state.data);
                }

                this.triggerMethod('playerStateChange');
            },

            /**
             * _onPlayerReady callback to check id youtube player is ready to use
             * @method _onPlayerReady
             * @return 
             */
            _onPlayerReady: function(videoPlayer) {
                //console.log('player ready');
            },

            /**
             * You tube tracking to check the video progress
             * @method _progressTrack
             * @return 
             */
            _progressTrack: function(videoSource, videoId, progress) {
                var ev = {},
                    self = this;
                
                self._ctComponentInfo();

                ev.eventInfo = {
                  'type':ctConstants.trackEvent,
                  'eventAction': ctConstants.videoProgress,
                  'eventLabel' : videoSource + ' - '+ videoId + ' - ' + 'progressed '+ progress
                };

                ev.attributes = {'nonInteraction':1};
                ev.category = {'primaryCategory':ctConstants.custom};
                digitalData.event.push(ev);
            },

            /**
             * Youtube play/completes tracking 
             * @method _ctTag
             * @return 
             */
            _ctTag: function(videoName, videoId, state) {
                var ev = {};
                
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    
                    digitalData.video = [];

                    this._ctComponentInfo();

                    digitalData.video.push({
                        'videoId': videoId
                    });

                    if (state === 1) {
                        ev.eventInfo={
                            'type': ctConstants.trackEvent,
                            'eventAction': ctConstants.videoPlays,
                            'eventLabel': videoName
                        };
                    } else if (state === 0) {
                        ev.eventInfo={
                            'type': ctConstants.trackEvent,
                            'eventAction': ctConstants.videoCompletes,
                            'eventLabel': videoName
                        };
                    }
                    ev.category = {'primaryCategory':ctConstants.other};
                    digitalData.event.push(ev);
                }
            }
        });
    });

    return CarouselV2;
});
