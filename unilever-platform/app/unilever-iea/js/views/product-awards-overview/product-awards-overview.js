/*global define*/

define(['TweenMax'], function() {
    'use strict';

    var ProductAwardsOverview = IEA.module('UI.product-awards-overview', function(productAwardsOverview, app, iea) {

        _.extend(productAwardsOverview, {

            defaultSettings: {   
                productWrapper: '.product-awards',
                awardList: '.c-product-awards-overview-list'
            },

            events: {
                'click .js-product-awards-overview__cta' : '_scrollToAwards',
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                this.componentJson = this.getModelJSON().productAwardsOverview;
                
                // show/hide awards based on award type
                this._filterAwardsByType();

                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/          

           /**
             * scroll to product awards component to View more awards
             * @method _scrollToAwards
             * @return 
             */

           _scrollToAwards: function(evt) {
                evt.preventDefault();

                var productWrapper = this.defaultSettings.productWrapper,
                    topOffset = $('header').height();

                TweenMax.to(window, 2, {
                    scrollTo: {
                        y: $(productWrapper).offset().top - topOffset
                    }, 
                    ease: Power4.easeOut
                });
            },

            /**
             * filter awards based on type and show/hide 
             * @method _filterAwardsByType
             * @return 
             */

            _filterAwardsByType: function() {
                var self = this,
                    awardList = self.$el.find(self.defaultSettings.awardList),
                    primaryAwards = 1,
                    secondaryAwards = 1;

                _.each($(awardList), function(item, index) {
                    var $item = $(item);
                    if ($item.data('type') === 'Primary') {
                        if (self.componentJson.productAwardsDetail.numberOfPrimaryAwardsToDisplay >= primaryAwards) {
                            $item.removeClass('hidden');
                        }
                        primaryAwards++;
                    } else if ($item.data('type') === 'Secondary') {
                        if (self.componentJson.productAwardsDetail.numberOfSecondaryAwardsToDisplay >= secondaryAwards) {
                            $item.removeClass('hidden');
                        }
                        secondaryAwards++;
                    }
                });
            }
        });
    });

    return ProductAwardsOverview;
});
