/*global define*/

define(['ratings','animService','iframetracker', 'postmessage'],function() {
    'use strict';

    var FeaturedProductV2 = IEA.module('UI.featured-product-v2', function(featuredProductV2, app, iea) {
        var ratingService = null,
            animService = null;

        _.extend(featuredProductV2, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                ratingService = new IEA.ratings();
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');
                var options = {},
                    featuredProductData = this.getModelJSON().featuredProductV2;
                
                options.ratingReview = featuredProductData.review;

                if (featuredProductData.productList.length) {
                    options.productId = featuredProductData.productList;
                    options.brandName = featuredProductData.brandName;
                    options.ratingObj = featuredProductData.productList[0].ratings;
                    ratingService.initRatings(options); 
                }

                // Retrieve data from JSON Data contract
                this._handleBinButton(featuredProductData);

                // track constant commerce BIN button click
                if (featuredProductData.shopNow.serviceProviderName === 'constantcommerce') {
                    this._trackIframeMethod();
                }

                if (featuredProductData.shopNow.serviceProviderName === 'etale') {
                    this._sendParametersToEtale();
                } 

                animService = new IEA.animService();             

                this.triggerMethod('enable');
            },

            /**************************PRIVATE METHODS******************************************/   

            _handleBinButton: function(data) {
                var self = this,
                    productId = data.productList[0].productID;

                // handle cartwire click event
                $('.js-cartwire-bin-button').click(function() {

                    // extract language from Data contract and load Widget
                    var language = data.market.substring(0,2);

                    // this function will only work in the UAT environment
                    // therefore a "Uncaught ReferenceError: loadsWidget is not defined"
                    // is expected during development :-(
                    loadsWidget(productId, this, 'retailPopup', language);
                    self._trackShopNow();
                });
                
                // handle etale click event
                $('.js-etale-bin-button').click(function(e) {

                    e.preventDefault();
                    self._openModal();

                    $('.js-main-wrapper').removeClass('u-blur-background');
                    $('.o-modal').css('background', 'rgba(255, 255, 255, 1)');
                    self._createIframeSrcAttribute(data);

                    // etale tracking
                    self._etaleTracking();
                });
                
                $('.js-btn-close').click(function(e) {
                    e.preventDefault();
                    self._closeModal();
                });

                $('.clicbuy').click(function() {
                    self._trackShopNow();
                });
            },

            _openModal: function () {
                animService.openOverlay(this.modalCl);
            },
            
            _closeModal: function () {
                animService.closeOverlay(this.modalCl);
            },

            _createIframeSrcAttribute: function(data){

                // eTale iframe setup

                // this src attribute will only work in the UAT environment
                // therefore a "GET http://int.magic.shoppable.co8711600450523/ net::ERR_NAME_NOT_RESOLVED"
                // is expected during development :-(
                document.getElementById("iframeId").setAttribute("src", data.shopNow.serviceURL + data.productList[0].productsDetail[0].smartProductId + "#" + data.shopNow.pageurl);
            },

            _trackIframeMethod: function() {
                var self = this;
                $('.constantco-widget iframe').iframeTracker({
                    blurCallback: function() {
                        self._trackShopNow();
                    },
                    overCallback: function(element) {
                        this._overId = $(element).attr('id'); // Saving the iframe wrapper id
                    },
                    outCallback: function(element) {
                        this._overId = null; // Reset hover iframe wrapper id
                    },
                    _overId: null
                });
            },

            _trackProductInfo: function() {
                var obj = $('.c-featured-product-v2'),
                    productID = obj.data('product-id'),
                    productName = obj.data('product-name'),
                    productCategory = obj.data('product-category'),
                    productPrice = obj.data('product-price'),
                    variant =  obj.data('product-variant'),
                    brandName = obj.data('product-brand');

                digitalData.product = [];
                digitalData.component = [];

                digitalData.component.push({
                    'componentInfo' :{
                        'name': obj.data('componentname'),
                    },
                    'attributes': {
                        'position': obj.data('component-positions'),
                        'variants': obj.data('component-variants')
                    }
                });

                digitalData.product.push({
                   'productInfo': {
                       'productID': productID,
                       'productName': productName,
                       'price': productPrice,
                       'brand': brandName,
                       'quantity': 1
                   },
                   'category': {
                        'primaryCategory': productCategory
                   },
                   'attributes': {
                        'productVariants': variant,
                        'listPosition': 1
                   }
                });
            },

            _trackShopNow: function() {
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    var ev = {},
                        self = this,
                        obj = $('.c-featured-product-v2'),
                        productName = obj.data('product-name');

                    // track product info
                    self._trackProductInfo();

                    ev.eventInfo = {
                      'type': ctConstants.trackEvent,
                      'eventAction': ctConstants.purchase,
                      'eventLabel': 'Online - ' + productName
                    };
                    ev.category = {'primaryCategory':ctConstants.conversion};
                    digitalData.event.push(ev);
                }
            },

            // send parameters to etale
            _sendParametersToEtale: function() {
                // Get the parent page URL as it was passed in, for browsers that don't support
                // window.postMessage (this URL could be hard-coded).
                var parent_url = decodeURIComponent(document.location.hash.replace(/^#/,'')),
                    link
                    self = this;

                // The first param is serialized using $.param (if not a string) and passed to the
                // parent window. If window.postMessage exists, the param is passed using that,
                // otherwise it is passed in the location hash (that's why parent_url is required).
                // The second param is the targetOrigin.              
                function postMessage(model, partnum, prodid, proddesc, markettext, ean, upc, etin, retailer) {
                    var message = '{ "model" : "' + model + 
                                    '", "partnum" : "' + partnum + 
                                    '", "prodid" : "' + prodid + 
                                    '", "proddesc" : "' + proddesc + 
                                    '", "markettext" : "' + markettext +
                                    '", "ean" : "' + ean + 
                                    '", "upc" : "' + upc + 
                                    '", "etin" :"' + etin + 
                                    '", "retailer" : " ' + retailer +
                                    '", "date": "' + new Date() + '"}'; 
                     $.postMessage(message, parent_url, parent);
                }
            },

            _etaleTracking: function() {
                // shop now tracking 
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    this._trackProductInfo();

                
                    $.receiveMessage(function (e) {
                        var json = JSON.parse(e.data),
                            retailer = json.proddesc + ' -' + json.retailer,
                            retailername = json.retailer;

                        if (typeof retailername !== 'undefined') {
                            var ev = {};
                            ev.eventInfo = {
                                'type': ctConstants.trackEvent,
                                'eventAction': ctConstants.retailerClick,
                                'eventLabel': 'Online - ' + prodName + ' | ' + retailername
                            };
                            ev.category = {'primaryCategory': ctConstants.custom};

                            digitalData.event.push(ev);
                            if (typeof $BV === 'object') {
                                $BV.SI.trackConversion({
                                    'type': 'BuyOnline',
                                    'label': retailername,
                                    'value': prodId
                                });
                            }
                        }
                    });
                }
            }
        });
    });

    return FeaturedProductV2;
});
