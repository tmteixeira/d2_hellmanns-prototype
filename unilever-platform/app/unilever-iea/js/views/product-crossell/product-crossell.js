/*global define*/

define(['slick', 'handlebars', 'TweenMax', 'commonService'], function (Handlebars) {
    'use strict';

    var ProductCrossell = IEA.module('UI.product-crossell', function(productCrossell, app, iea) {

		var commonService = null,
            self = this,
            DEFAULT_MOBILE_P = 480,
            DEFAULT_MOBILE_L = 667,
            isMobile = null,
			productCrossellData = null;


        _.extend(productCrossell, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
				productCrossellData = this.getModelJSON();
                this.$el.html(this.template(productCrossellData));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */

            enable: function () {
                this.triggerMethod('beforEnable');

                // methods to be used here

                this.triggerMethod('enable');
            }
        });
    });

    return ProductCrossell;
});
