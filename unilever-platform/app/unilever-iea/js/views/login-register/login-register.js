/*global define*/

define(['eGiftingService', 'commonService', 'formService', 'postmessage'], function() {
    'use strict';
    
    var eGiftingService = null, commonService = null, formService = null;
    
    var LoginRegister = IEA.module('UI.login-register', function(loginRegister, app, iea) {

        _.extend(loginRegister, {
            
            defaultSettings: {
                loginWrap: ".c-login-wrap",
                logInBtn: ".js-btn-login",
                registerBtn: ".js-btn-register",
                loginRegister: ".c-login-register",
                loginRegisterInfo: ".c-login-register-info",
                quickCloseBtn: ".c-login__quick-close",
                profileLogout: ".c-profile-logout-info",
                loginSubmit: ".c-login-wrap form button[type=submit]",
                profileDisName: ".c-profile-logout-info .display-name"
            },
            
            events: {
                'click .js-btn-login' : 'showLogin',
                'click .c-login__quick-close' : 'quickClose',
                'click .js-btn-register' : 'showRegister',
                'click .log-out' : 'logoutForm'
            },
            

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                var self = this;
                self._super(options);
                eGiftingService = new IEA.EGifting();
                commonService = new IEA.commonService();
                formService = new IEA.formService();
                self.changedSet = {};
                self.isvalidationRequired = false;
                self.isFormValid = false;
                self.isSubmitted = false;
                self.jsonFromHtml = {};
                self.validator = app.form.validator;
                self.validator.addValidation(self, {
                    model: self.model
                });

                $('.c-login-wrap .c-already-registered-login').css("visibility","hidden");
                this.triggerMethod('init');

            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');              
                
                var self = this, settings = self.defaultSettings, modelData = self.getModelJSON().loginRegister;                
                self.$loginForm = $(settings.loginWrap + ' form');
                
                self.form = {
                    isAjax: self.$loginForm.data('ajax'),
                    postURL: self.$loginForm.attr('action'),
                    failureMessage: self.$loginForm.data('fail'),
                    successMessage: self.$loginForm.data('success'),
                    validation: self.$loginForm.data('validation')
                };  
                self.submitLogin(self.form.postURL, self.$loginForm);
                self.readCookie();
                if($('[name="secure-frame-login"]').length > 0) {
                    self.loadPostMessage();
                }
                $("#rememberme").change(function() {
                    if(this.checked) { 
                        if($("#rememberMeExpiration").length > 0) {
                           $("#ExpirationDuration").val($("#rememberMeExpiration").val());
                        }
                    }
                    if(!this.checked) {
                        if($("#defaultExpiration").length > 0) {
                           $("#ExpirationDuration").val($("#defaultExpiration").val()); 
                       }                        
                    }
                });


                self.validator.addRules(self, {
                    rules: self._createValidationRules() 
                });
				
				self.inputErrorTrack();
                
                this.triggerMethod('enable');
            },
            
            loadPostMessage: function() {
                var self = this;
                self.receiveIframeMessage();
            },
            
            receiveIframeMessage: function () {
                var resHeader = '', 
                    originURL = window.location.hostname,
                    self = this;

                $.receiveMessage(function(e) {
                    resHeader = Number(e.data.replace( /.*resHeader=(\d+)(?:&|$)/, '$1' ));
                    self.loginRedirects(resHeader);
                },"https://"+originURL); 
            },
            
            showLogin: function() {
                var self = this, settings = self.defaultSettings;
                $(settings.loginWrap).css("visibility","visible");
            },
            
            quickClose: function() {
                var self = this, settings = self.defaultSettings;
                $(settings.loginWrap).css("visibility","hidden");
                $('.c-login-wrap .c-already-registered-login').css("visibility","hidden");
            },
            
            showRegister: function() {
                var self = this, settings = self.defaultSettings;
                $(settings.loginWrap).css("visibility","hidden");
                $('.c-login-wrap .c-already-registered-login').css("visibility","hidden");
            },
            
            scrollPageToError: function(elem) {
                $('html, body').animate({
                    scrollTop : elem.offset().top - 150
                }, 1000);
            },
            
            loginRedirects: function(resHeader) {
                var self = this,
                    redirectLocation = "",
                    urlMappings = self.getModelJSON().loginRegister.redirectsConfig;
                
                if(resHeader == 2){
                    $.each(urlMappings, function(k, v){
                        if(window.location.href.indexOf(k) !== -1){
                            redirectLocation = v;
                            return false;
                        }
                    });

                    if(redirectLocation !== ""){
                        redirectLocation = redirectLocation;
                    } else {
                        //self.readCookie();
                        // refresh page
                        redirectLocation = window.location.href;
                    }
                } else if(resHeader == 1) {
                    // take user to registration page, user details tab.
                    redirectLocation = self.getModelJSON().loginRegister.register.registerUrl;
                }

                window.location.href = redirectLocation;  
            },
            
            submitLogin: function(postUrl, $loginForm) {
                var self = this, settings = self.defaultSettings;

                $(settings.loginSubmit).on('click', function(evt) {

                    var formData = IEA.serializeFormObject($loginForm);
                    var formDatatoSend = formService.formToJson($loginForm);
					var formDataLogin = JSON.parse(formDatatoSend);
                    
                    evt.preventDefault();
                    
                    self.model.unset(self.changedSet);
                    self.model.set(formData);
                    self.changedSet = self.model.changed;
                    
                    if (!self.isvalidationRequired || (self.isvalidationRequired && self.model.isValid(true))) {
                        self.isFormValid = true;
                    } else {
                        self.isFormValid = false;
                        self.scrollPageToError($('.form').find('.has-error, .has-error-block'));
                    }   

                    if (self.isFormValid && postUrl !== '' && self.isSubmitted === false) {
                        if (self.form.isAjax) {
                            if($('input.c-input-checkbox').is(':checked')) {
                                var uname = btoa($('#Credential-Username').val()),
                                    pwd = btoa($('#Credential-Password').val());
                            } 
                        
                            $.ajax({
                                url: postUrl,
                                type: 'POST',
                                data: formDatatoSend,   
                                dataType: 'json',
                                xhrProperties: {
                                    withCredential: true
                                },
                                crossDomain: true,
                                contentType: 'application/json', 
                                success: function(data,status,xhr) {
                                    var resHeader = data.UserRegistrationStatus;
                                    self.loginRedirects(resHeader);
									self.loginTrack();

                                    
                                },
                                error: function(xhr, status, error) {
                                    
                                    if(xhr.status === 403 && xhr.responseJSON){
                                        var data = xhr.responseJSON,
                                            errorCode = data.ErrorCode, //'error invalid user', 
                                            $submit=self.$loginForm.find('button[type="submit"]'),
											msg;
                                        
										if(errorCode === "InvalidInput"){
											msg = self.$loginForm.find('[name="Credentials-Password"]').data('form-invalidinput-msg');
										} else if(errorCode === "ItemNotFound"){
											msg = self.$loginForm.find('[name="Credentials-Password"]').data('form-itemnotfound-msg');
										} else {
											msg = data.ErrorMessage;
										}
										
                                        self.$loginForm.find('.error-msg-text').remove();
                                        $submit.parents('.form-group').addClass('has-error-block');
                                        var $span = $("<span />").addClass('error-msg-text help-block-error').text(msg);
                                        $submit.parent().prepend($span);
                                        self.isSubmitted = false;
                                    }
                                    
                                }                   
                            });
                        } else {
                            self.$loginForm.submit();
                        }
                        self.triggerMethod('formSubmit', self);
                        self.isSubmitted = true;
                    }
                });
            },

            logoutForm: function() {
                var self = this,
                    settings = self.defaultSettings,
                    logoutUrl = self.getModelJSON().loginRegister.endPoints.logoutUrl;

                $.ajax({
                    url: logoutUrl,
                    type: 'POST',
                    dataType: 'json',
                    xhrFields: {
                       withCredentials: true
                    },
                    contentType: 'application/json',
                    success: function (data, status, xhr) {
                        $(settings.profileLogout).hide();
                        $(settings.loginRegisterInfo).show();
                        $(settings.loginRegister).css('visibility', 'visible');
                         var urlMappings = self.getModelJSON().loginRegister.logoutRedirectsConfig,
                            redirectLocation = "";
                        
                        if($('body').hasClass('reset-form')){
                            $('form').each(function() {
                                this.reset();
                                $('input[type="radio"], input[type="checkbox"]').attr('checked', false);
                            });    
                        }
                        
                        $.each(urlMappings, function(k, v){
                            if(window.location.href.indexOf(k) !== -1){
                                redirectLocation = v;
                                return false;
                            }
                        });

                        if(redirectLocation !== ""){
                            window.location.href = redirectLocation;
                            return;
                        }
                    },
                    error: function (xhr, status, error) {

                    }
                });
                
               
            },

            readCookie: function() {
                var self = this, settings = self.defaultSettings;
                if (typeof formService.getCookie('token') === 'undefined') {
                    $(settings.loginRegisterInfo).show();
                    $(settings.profileLogout).hide();
                    $(settings.loginRegister).css('visibility', 'visible');
					
					var urlMappings = self.getModelJSON().loginRegister.logoutRedirectsConfig,
						redirectLocation = "";

					$.each(urlMappings, function(k, v){
						if(window.location.href.indexOf(k) !== -1){
							redirectLocation = v;
							return false;
						}
					});

					if(redirectLocation !== ""){
						window.location.href = redirectLocation;
						return;
					}
					
                } else {
                    $.ajax({
                        url: self.getModelJSON().loginRegister.endPoints.getUserUrl,
                        type: 'GET',
                        cache: false,
                        dataType: 'json',
                        xhrFields: {
                           withCredentials: true
                        },
                        contentType: 'application/json', 
                        success: function(data, status, xhr) {
                            var resHeader = data.UserRegistrationStatus;

                            if(resHeader == 2){
                                var urlMappings = self.getModelJSON().loginRegister.redirectsConfig,
                                    redirectLocation = "";

                                $.each(urlMappings, function(k, v){
                                    if(window.location.href.indexOf(k) !== -1){
                                        redirectLocation = v;
                                        return false;
                                    }
                                });

                                if(redirectLocation !== ""){
                                    window.location.href = redirectLocation;
                                    return;
                                }
                            }

                            var displayName;
                            if(data.Profile.Name.GivenName){
                                displayName= data.Profile.Name.GivenName;
                            }
                            else{
                                displayName= data.Profile.Name.DisplayName;
                            }
                            $(settings.profileDisName).text(displayName);
                            $(settings.loginRegisterInfo).hide();
                            $(settings.profileLogout).show();
                            $(settings.loginWrap).css("visibility","hidden");
                            $(settings.loginRegister).css('visibility', 'visible');
                        },
                        error: function(xhr, status, error) {
                            $(settings.loginRegisterInfo).show();
                            $(settings.profileLogout).hide();
                            $(settings.loginRegister).css('visibility', 'visible');
                        }                   
                    });
                }
                
                if(eGiftingService.hasCookieData('_remember') === true){
                    var uname = atob(eGiftingService.readCookie('_u_name')), pwd = atob(eGiftingService.readCookie('_p_wd'));
                    $('#Credential-Username').val(uname), $('#Credential-Password').val(pwd);   
                }
            },
            
            _createValidationRules: function() {
                var self = this,
                    moduleName = self.moduleName.hyphenToCamelCase(),
                    htmlStr = self.getModelJSON()[moduleName].login.htmlSnippet,
                    parseHtmlStr = $.parseHTML(htmlStr),
                    element, elementRule = {},
                    validationRules = {},
                    idx = 0,
                    jdx = 0,
                    pattern;
                
                $.each(parseHtmlStr, function() {
                    if(this.nodeName === 'SCRIPT') {
                        if(this.id.indexOf('form') !== -1) {
                            self.jsonFromHtml = JSON.parse(this.innerHTML);
                        }
                    }
                });
                
                var formJson = self.jsonFromHtml, formElements = formJson.data.form.formElementsConfig;
                
                function createValidationRules(el) {
                  validationRules[el.name] = [];
                  if (typeof el.rules !== 'undefined' && _.isArray(el.rules)) {

                    for (jdx = 0; jdx < el.rules.length; jdx++) {
                      self.isvalidationRequired = true;
                      elementRule = el.rules[jdx];
                      validationRules[el.name].push({
                        xss: true
                      });
                      validationRules[el.name].push(elementRule);
                    }

                  }
                }
                
                for(idx = 0; idx<formElements.length; idx++) {
                    element = formElements[idx];
                    if (element !== null) {
                        if (!_.isFunction(element.formElement) && element.formElement) {
                            createValidationRules(element.formElement);
                        } else if (!_.isFunction(element.formcheckbox) && element.formcheckbox) {
                            createValidationRules(element.formcheckbox);
                        }                       
                    } 
                }
                return validationRules;
            },
			
			loginTrack: function() {
                var settings = this.defaultSettings;
				if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
					var ev = {},
						 compName = $(settings.loginSubmit).parents('[data-componentname]').data('componentname');

					digitalData.component = [];
					digitalData.component.push({
						'componentInfo' :{
							'componentID': compName,
							'name': compName
						}
					});

					ev.eventInfo={
						'type':ctConstants.trackEvent,
						'eventAction': ctConstants.signIns,
						'eventLabel' : "Signup/Registration"
					};
					ev.category ={'primaryCategory':ctConstants.other};
					digitalData.event.push(ev);
				}
			},
			
			//Input error Tracking
			inputErrorTrack: function() {
                var settings = self.defaultSettings;
				$(document).on('click', '.c-login-wrap form button[type=submit]', function(){
					if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
						if($('.has-error').length) {
							var ev = {},
								errorlog = [],
								compName = $(settings.loginSubmit).parents('[data-componentName]').attr('data-componentName'),
								compVariants = $(settings.loginSubmit).parents('[data-component-variants]').data('component-variants'),
								compPositions = $(settings.loginSubmit).parents('[data-component-positions]').data('component-positions');

							$('.has-error').each(function(i, value) {
							   errorlog.push($(this).find('.help-block').text());
							});
							var errorvalue= errorlog.join(" | ");

							digitalData.component = [];
							digitalData.component.push({
								'componentInfo' :{
									'componentID': compName,
									'name': compName
								},
								'attributes': {
									'position': compPositions,
									'variants': compVariants
								}
							});

							ev.eventInfo={
							  'type':ctConstants.trackEvent,
							  'eventAction': ctConstants.inputerror,
							  'eventLabel' : errorvalue
							};
							ev.category ={'primaryCategory':ctConstants.custom};
							digitalData.event.push(ev);
						}
					}
				});
			}

            /**************************PRIVATE METHODS******************************************/          

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */

        });
    });

    return LoginRegister;
});
