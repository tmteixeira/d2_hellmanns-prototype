/*global define*/

define(function() {
	'use strict';

	var CallToAction = IEA.module('UI.call-to-action', function(callToAction, app, iea) {

		_.extend(callToAction, {

			/*********************PUBLIC METHODS******************************************/

			/**
			 * intialize function. the super function inside it will call the abstract initializse of the iea view.
			 * @method initialize
			 * @param {} options
			 * @return 
			 */
			initialize: function (options) {
				this._super(options);
			},

			/**
			 * render logic . this gets automatically called if the component is a client side component
			 * @method render
			 * @return 
			 */
			render: function () {
				this.$el.html(this.template(this.getModelJSON()));

				if(this._isEnabled === false) {
					this.enable();
					this._isEnabled = true;
				}
			},

			/**
			 * enable function to write component enable logics. this function gets automatically called if the 
			 * component is a server side component, skipping the call to render
			 * @method enable
			 * @return 
			 */
			enable: function () {
				// add click event for button type CTA
				this.$el.find('button').off('click').on('click', function(){
					var href = $(this).data('href'),
						target = $(this).data('target');

					if(target === 'blank'){
						window.open(href);
					} else {
						window.location.href = href;
					}
				});

			}

			/**************************PRIVATE METHODS******************************************/          

		});
	});

	return CallToAction;
});
