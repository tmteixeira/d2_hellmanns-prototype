/*global define*/

define(['iscroll', 'TweenMax'], function() {
    'use strict';

    var AnchorLinkNavigation = IEA.module('UI.anchor-link-navigation', function(anchorLinkNavigation, app, iea) {

        var self = this,
            windowWidth = $(window).width(),
            windowHeight = $(window).height(),
            mobileBreakpoint = 768,
            scrollFromAnchor = false,
            navScroll = null,
            $activeItem = null,
            $scrollList = null,
            arrowsEnabled = false,
            type = 'curve';

        _.extend(anchorLinkNavigation, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                // Create items based on data attributes
                var $items = $('[data-sticky-nav-id]'),
                    $navWrapper = $('.anchor-link-navigation');

                type = $navWrapper.find('.js-highlighted-line-container').data('sticky-nav-shape');

                $navWrapper.addClass(type);

                // Only load the component if there are links to show
                if ($items.length > 0) {

                    $items.each(function(idx, item) {

                        var target = $(item).data('sticky-nav-id'),
                            label = $(this).data('sticky-nav-label');

                        // Add the items to the sticky navigation
                        $('.js-navigation-sticky').append('<li id="js-sticky-item-' + idx + '" class="c-navigation-sticky__item js-navigation-sticky__item"><span class="c-navigation-sticky__button" data-anim-scroll-target="' + target + '">' + label + '</span></li>');
                    });

                    self._initNav();
                }
                else {
                    $navWrapper.remove();
                }

                this.triggerMethod('enable');
            },

            /**************************PRIVATE METHODS******************************************/

            _initNav: function() {

                var $navBar = $('.o-navbar'),
                    $secNavBar = $('.c-secondary-nav'),
                    $navWrapper = $('.anchor-link-navigation'),
                    $hero = $('.hero'),
                    bodyMargin = parseInt($('body').css('margin-top'), 10),
                    navOffset = 0,
                    total = $('.js-navigation-sticky__item').length,
                    $items = $('.js-navigation-sticky__item'),
                    $buttons = $('.c-navigation-sticky__button'),
                    $wrapper = $('.js-navigation-sticky-wrapper'),
                    $line = $('.js-highlighted-line-container'),
                    $components = $('[data-sticky-nav-id]'),
                    $shoppableCart = $('.o-shoppingCart'),
                    padding = 40;

                $scrollList = $('.js-navigation-sticky');

                // Fade in on homepage
                TweenMax.to($navWrapper, 0.8, {autoAlpha: 1, top: 0, delay: 1.4});

                if ($scrollList.width() > windowWidth) {
                    padding = 20;
                }

                var wItem = parseInt($items.css('width').replace('px','')) + padding;

                $items.addClass('cell');
                $scrollList.width(total*wItem);

                // Click events
                $buttons.on('click', function(e) {
                    e.preventDefault();
                    self._activateItem($(e.currentTarget), true);
                });

                // Init iScroll
                navScroll = new IScroll('.js-navigation-sticky-wrapper', { click: true, probeType: 3, scrollX: true, scrollbars: false });
                navScroll.on('scroll', self._onScrollHorizontally);

                // Drag and drop icons
                if ($scrollList.width() > windowWidth) {

                    $scrollList.css('cursor', '-webkit-grab');

                    $scrollList.on('mousedown', function() {
                        $(this).css('cursor', '-webkit-grabbing');
                    });

                    $scrollList.on('mouseup', function() {
                        $(this).css('cursor', '-webkit-grab');
                    });
                }

                // Start/stop sticky behaviour
                $(window).on('scroll', function() {

                    var scrollOffset = $(window).scrollTop(),
                        topPosition = 0;

                    // Check if the navbar is already loaded
                    if ($navBar.length === 0) {
                        $navBar = $('.o-navbar');
                    }
                    // Fix the anchor nav position when the hero copy is opened
                    if ($hero.length > 0) {
                        navOffset = bodyMargin + $hero.height();
                    } else {
                        navOffset = $navWrapper.offset().top;
                    }
                    
                    // Slide up/down the global nav and secondary nav
                    if (scrollOffset >= navOffset) {
                        if (!$('body').hasClass('o-navbar-hidden')) {
                            $('body').addClass('o-navbar-hidden');
                            TweenMax.to($secNavBar, 0.6, {top: (topPosition - 52) + 'px'});
                            TweenMax.to($navBar, 0.6, {top: (topPosition - 132) + 'px'});
                            TweenMax.to($shoppableCart, 0.6, {top: (topPosition - 132) + 'px'});
                            $('.js-navbar-overlay-close').click();
                        }
                    } else {
                        if ($('body').hasClass('o-navbar-hidden')) {
                            $('body').removeClass('o-navbar-hidden');
                            TweenMax.to($secNavBar, 0.6, {top: topPosition + 'px'});
                            TweenMax.to($navBar, 0.6, {top: 52 + 'px'});
                            TweenMax.to($shoppableCart, 0.6, {top: 52 + 'px'});
                        }
                    }

                    // Fix the anchor nav
                    if (scrollOffset >= navOffset - 45) {
                        TweenMax.to($line, 0.5, {autoAlpha: 1});
                        $navWrapper.addClass('js-container-sticky');
                        $('body').addClass('o-sticky-nav-enabled');

                        $navWrapper.css({top: topPosition + 'px'});
                    }
                    else {
                        TweenMax.to($line, 0.5, {autoAlpha: 0});
                        $navWrapper.removeClass('js-container-sticky');
                        $('body').removeClass('o-sticky-nav-enabled');

                        $navWrapper.css({top: 0});
                    }
                    // Check which component is currently into the viewport
                    
                        var $latestItem = null;

                        $components.each(function() {

                            if ($(this).offset().top < (scrollOffset + (windowHeight/3))) {

                                var id = $(this).data('sticky-nav-id');
                                $latestItem = $('[data-anim-scroll-target="' + id + '"]');
                            }
                        });

                        // Only update the nav if the item changed
                        if ($latestItem !== null && !$latestItem.is($activeItem)) {
                            $activeItem = $latestItem;
                            self._activateItem($activeItem, false);
                        }
                });

                // Update the highlighted curve on window resize
                app.on('window:resized', function (options) {

                    self._resized();
                });

                self._resized();
                $(window).scroll();
                var service = new IEA.animService();
            },

            _resized: function() {

                var $line = $('.js-highlighted-line'),
                    offset = 0.20; // Add 20% of offset for the highlighted curve, which is 120% width

                // Reset the window width
                windowWidth = $(window).width();

                if (windowWidth < mobileBreakpoint) {
                    offset = 0.60; // The curve on mobile is 160% width
                }

                // Display the right arrow if the scroll is enabled
                if ($scrollList.width() > windowWidth) {
                    arrowsEnabled = true;
                    $('.js-navigation-sticky__arrow--right').show();
                }
                else {
                    arrowsEnabled = false;
                    $('.js-navigation-sticky__arrow--right').hide();
                    $('.js-navigation-sticky__arrow--left').hide();
                }

                // Update line and marker
                $line.width(windowWidth + (windowWidth*offset));
                //self._updatePosition();
                self._updateActivePosition();
            },

            _activateItem: function(target, scroll) {
                if(target.length > 0) {

                    var $items = $('.js-navigation-sticky__item'),
                        $li = target.closest('li'),
                        $scrollTarget = $('[data-sticky-nav-id="' + target.data('anim-scroll-target') + '"]'),
                        $wrapper = $('.js-navigation-sticky-wrapper');

                    // We need to skip the global nav, filter nav and add a bit of margin
                    var topOffset = $wrapper.height() + 30;

                    $items.removeClass('js-navigation-sticky--active is--selected');
                    $li.addClass('js-navigation-sticky--active is--selected');
                    
                    if (windowWidth < mobileBreakpoint) {
                        var textforMob = $('.is--selected span').text();
                        $('.js-navigation-sticky__anchor-text').html(textforMob);
                        $('.js-navigation-sticky__item').show();
                        $('.js-navigation-sticky__item.is--selected').hide();
                    }

                    // Scroll the page to the target element
                    if (scroll) {
                        scrollFromAnchor = true;
                        TweenMax.to(window, 2, {scrollTo: {y: $scrollTarget.offset().top - topOffset}, ease: Power4.easeOut, autoKill: true, onComplete: self._scrollFromAnchor});
                    }
                    else {
                        if (arrowsEnabled) {
                            // Move item to the center
                            navScroll.scrollToElement('#' + $li.attr('id'), 1000, true, true, IScroll.utils.ease.quadratic);
                        }
                    }

                    self._updateActivePosition();
                }
            },

            _updatePosition: function() {

                // Update the highlighted line based on scroll position
                var scrollOffset = $(window).scrollTop(),
                    $marker = $('.js-navigation-sticky__marker'),
                    $line = $('.js-highlighted-line-container'),
                    normalizedOffset = scrollOffset / ($(document).height() - windowHeight),
                    newPos = (windowWidth * normalizedOffset),
                    offset = 0.10; // Add 10% of offset for the left offset of the highlighted curve

                if (windowWidth < mobileBreakpoint) {
                    offset = 0.30; // The curve on mobile is 160% width
                }

                // Animate highlight line
                TweenMax.to($line, 1, {width: (windowWidth*offset) + newPos});

                // Animate the marker
                if (type !== 'line') {
                    TweenMax.to($marker, 1, {left: newPos, onUpdate: self._moveMarker});
                }
            },

            _updateActivePosition: function() {

                // Update the marker position based on active item
                var $li = $('.js-navigation-sticky--active');

                if ($li.length > 0) {

                    var $wrapper = $('.c-navigation-sticky-wrapper'),
                        $marker = $('.c-navigation-sticky__marker'),
                        $line = $('.js-highlighted-line-container'),
                        markerPos = $li.offset().left + ($li.width()/2),
                        offset = 0.10; // Add 10% of offset for the left offset of the highlighted curve

                    if (windowWidth < mobileBreakpoint) {
                        offset = 0.30; // The curve on mobile is 160% width
                    }
                    else {
                        markerPos += 15;
                    }

                    // Animate highlight line
                    TweenMax.to($line, 1, {width: (windowWidth*offset) + markerPos});

                    // Animate the marker
                    if (type !== 'line') {
                        TweenMax.to($marker, 1, {left: markerPos, onUpdate: self._moveMarker});
                    }
                }
            },

            _moveMarker: function(e) {

                var $marker = $('.js-navigation-sticky__marker'),
                    pos = parseInt($marker.css('left').replace('px', ''));

                $marker.css('top', self._calculateTop(pos));
            },

            _calculateTop: function(newPos) {

                var minTop = 55, maxTop = 69, radius = -852;

                if (windowWidth < mobileBreakpoint) {
                    minTop = 66;
                    maxTop = 75;
                    radius = -537;
                }

                var percent = newPos / (windowWidth/2),
                    aperc = Math.abs(percent - 1),
                    maxAngle = (10/360) * (2*Math.PI),
                    h = (Math.cos(maxAngle * aperc) / (1 - Math.cos(maxAngle))) * (maxTop - minTop);

                return radius + h; // Offset is equal to radius of the virtual circle defined by the curve
            },

            _onScrollHorizontally: function(e) {

                var $leftArrow = $('.js-navigation-sticky__arrow--left'),
                    $rightArrow = $('.js-navigation-sticky__arrow--right'),
                    navWidth = -($('.js-navigation-sticky').width() - $('.js-navigation-sticky-wrapper').width() - 20);

                if (this.x < 0) $leftArrow.show();
                else $leftArrow.hide();

                if (this.x <= navWidth) $rightArrow.hide();
                else $rightArrow.show();

                self._updateActivePosition();
            },

            _scrollFromAnchor: function(elem) {
                scrollFromAnchor = false;
            }
        });
    });

    return AnchorLinkNavigation;
});