/*global define*/

define(function() {

    'use strict';

    var RichTextImage = IEA.module('UI.rich-text-image', function(module, app, iea) {
        _.extend(module, {
            // your extendable hooks and API methods goes here
        });
    });

    return RichTextImage;
});
