/*global define*/

define(function() {
    'use strict';

    var RecipeNutrients = IEA.module('UI.recipe-nutrients', function(recipeNutrients, app, iea) {

        _.extend(recipeNutrients, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            events: {
                'click .c-recipe-nutrients__heading': '_toggleInfo'
            },
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforeEnable');
                var self = this,
                titleToggle = $('.c-recipe-nutrients__heading');
                self.recipeNutrientsData = self.getModelJSON().recipeNutrients;
                if(titleToggle.find('.o-collapse')) {
                    $('.c-recipe-nutrients-info').hide();
                }
                else if(titleToggle.find('.o-expand')) {
                    $('.c-recipe-nutrients-info').show();
                }
               
                this.triggerMethod('enable');
            },

            


            /**************************PRIVATE METHODS******************************************/          

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */
            _toggleInfo: function(e) {
                var $this = $(e.target);
                if($this.parents('.c-recipe-nutrients').find('span').hasClass('o-expand')) {
                    $('.c-recipe-nutrients-info').slideUp();
                    $this.parents('.c-recipe-nutrients').find('span').removeClass('o-expand');
                    $this.parents('.c-recipe-nutrients').find('span').addClass('o-collapse');                    
                }
                else if($this.parents('.c-recipe-nutrients').find('span').hasClass('o-collapse')) {
                    $('.c-recipe-nutrients-info').slideDown(); 
                    $this.parents('.c-recipe-nutrients').find('span').addClass('o-expand');
                    $this.parents('.c-recipe-nutrients').find('span').removeClass('o-collapse');                    
                }
            }

        });
    });

    return RecipeNutrients;
});
