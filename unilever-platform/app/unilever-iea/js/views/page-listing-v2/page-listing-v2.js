/*global define*/

define(['commonService'], function() {
    'use strict';

    var PageListingV2 = IEA.module('UI.page-listing-v2', function (pageListingV2, app, iea) {

        var commonService = null;

        _.extend(pageListingV2, {

            defaultSettings: {
                searchWrapper: '.c-page-listing-v2__wrapper',
                filterWrapper: '.c-page-listing-filter__filter',
                template: 'partials/page-listing-v2-page.hbss',
                paginationTemplate: 'pagination/defaultView.hbss',
                pageListingList: '.c-page-listing-v2-listing__list',
                paginationBtn: '.js-btn-pagination',
                pagination: '.c-page-listing-v2-pagination',
                paginationContainer: '.js-page-listing-v2-pagination',
                filterCount: '.js-filter-count',
                loadmoreWrapper: '.c-page-listing-v2__loadmore',
                loadmoreBtn: '.js-btn-loadmore',
                ctaAnchor: '.c-page-listing-v2-listing-item-cta__anchor',
                isLoadMore: false,
                isPageLoad: true,
                preloader: '.c-page-listing-v2__loadmore .o-preloader',
                preloaderContent: '.o-preloader--content',
                startPageNum: 1,
                startIndex: 1,
                pageNum: 1
            },

            events: {
                'change .c-page-listing-filter__filter' : '_filterChangeHandler', //update params and render search results on dropdown change
                'click .js-btn-loadmore' : '_loadMorehandler', // render results on show all button click
                'click .js-btn-pagination' : '_paginationHandler'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
                commonService = new IEA.commonService(); // creates new instance for common service

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                this.componentJson = this.getModelJSON().pageListingV2;
                this.pageNum = this.defaultSettings.pageNum;
                this.pagination = this.$el.find(this.defaultSettings.pagination);
                this.preloaderContent = this.$el.find(this.defaultSettings.preloaderContent);
                this.isPageLoad = this.defaultSettings.isPageLoad;
                this.startIndex = this.defaultSettings.startIndex;
                this.pageScroll = false;

                // show/hide load more on page scroll
                this._handleLoadMore();
                this._renderPagination(this.componentJson.totalCount);


                this.triggerMethod('enable');
            },

            /**************************PRIVATE METHODS******************************************/

            _getServiceQueryParams: function () {
                var self = this,
                    serviceQueryParams = '',
                    filterParams = '',
                    pageNumParam;

                $(self.defaultSettings.filterWrapper).each(function(key, val) {
                    var _this = $(this),
                        filterType = _this.data('filter-type'),
                        selectedVal = '';

                    switch (filterType) {
                        case 'dropdown':
                            selectedVal = _this.find('option:selected').val();
                            break;

                        case 'radioButton':
                            selectedVal = (_this.is(':checked')) ? _this.val() : '';
                            break;

                        case 'checkBox':
                            selectedVal = (_this.is(':checked')) ? _this.val() : '';
                            break;
                    }

                    filterParams += self.componentJson.filterConfig.filterParamName + '=' + selectedVal + '&';
                });

                pageNumParam = self.componentJson.filterConfig.pageNoParamName + '=' + self.pageNum;

                serviceQueryParams += '?' + filterParams + pageNumParam;

                return serviceQueryParams;
            },

           /**
            @description: Backbone.js Bind Callback to Successful Model Fetch
            @method _searchView
            @return {null}
            **/

            _populateData: function() {
                /* Backbone.js Bind Callback to Successful Model Fetch */
                var self = this,
                    options = {};

                options.queryParams = self._getServiceQueryParams();

                options.servletUrl = self.componentJson.filterConfig.ajaxURL;

                options.onCompleteCallback = function(response) {
                    if (response) {
                        var parsedJson = $.parseJSON(response);
                        self._updatePageListing(parsedJson.data.pageListingV2.pages, parsedJson.data.pageListingV2.totalCount);
                    }
                };

                commonService.getDataAjaxMethod(options);
            },

            _filterChangeHandler: function() {
                var self = this;

                // Start from the beginning again
                self.pageNum = self.defaultSettings.startPageNum;
                self.isPageLoad = true;

                self._populateData();
            },

            /**
            @description: handle load more click event
            @method: _updatePageListing
            @return: {null}
            **/

            _updatePageListing: function (pages, totalCount) {
                var self = this,
                    pageData = $(pages),
                    settings = self.defaultSettings,
                    listing = '',
                    template = self.getTemplate('page-listing-v2-page', settings.template);

                pageData.each(function(key, item) {
                    listing += template(item);
                });

                // Update total count
                self.componentJson.totalCount = totalCount;

                // update result count
                self.$el.find(settings.filterCount).text(totalCount);

                //hide preloader
                $(settings.preloader).addClass('hidden');
                if (self.pageNum > 1 && self.componentJson.pagination.paginationType === 'infiniteScroll') {
                    $(settings.pageListingList).append(listing);
                } else {
                   $(settings.pageListingList).html(listing);
                }

                // Update CTA Label
                self.$el.find(settings.ctaAnchor).text(self.componentJson.generalConfig.ctaLabelText);
                self._renderPagination(totalCount);
                // load more params
                self._updateParamsOnPageScroll(totalCount);

                if (totalCount > self.componentJson.pagination.itemsPerPage) {
                       $(self.pagination).removeClass('hidden');
                       $(self.loadmoreWrapper).removeClass('hidden');

                       if (self.pageNum === 1) {
                            self.$el.find(settings.loadmoreBtn).fadeIn();
                       }
                } else {
                    $(self.pagination).addClass('hidden');
                    $(self.loadmoreWrapper).addClass('hidden');
                }
            },

            _renderPagination: function(totalCount) {
                var self = this,
                    paginationTemplate = self.getTemplate('pagination', self.defaultSettings.paginationTemplate),
                    combinedJson = null,
                    totalPages = Math.ceil(totalCount/self.componentJson.pagination.itemsPerPage),
                    paginationList = [];

                for (var i=0; i < totalPages; i++) {
                    paginationList.push(i);
                }

                combinedJson = {
                    'componentJson': self.componentJson,
                    'pageNum': self.defaultSettings.pageNum,
                    'totalPages': totalPages,
                    'paginate': paginationList
                };

                // render pagination
                if (self.isPageLoad) {
                    $(self.pagination).html(paginationTemplate(combinedJson));
                }

            },

            /**
            @description: handle load more click event
            @method: _updateParamsOnPageScroll
            @return: {null}
            **/

            _updateParamsOnPageScroll: function (totalCount) {
                var self = this,
                    itemsPerPage = self.componentJson.filterConfig.itemsPerPage;

                self.pageScroll = (totalCount > itemsPerPage * self.pageNum);
            },

            /**
            @description: handle load more click event
            @method: _loadMorehandler
            @return: {null}
            **/

            _loadMorehandler: function(e) {
                var self = this,
                    $this = $(e.currentTarget),
                    settings = self.defaultSettings;

                e.preventDefault();

                self.pageNum++;

                $this.fadeOut(500);
                $(settings.preloader).removeClass('hidden');
                settings.isLoadMore = true;

                // render search listing
                self._populateData();
            },

            _paginationHandler: function(evt) {
                var self = this,
                    $this = $(evt.currentTarget);

                 if ($this.hasClass('o-btn--disabled') || $this.hasClass('o-btn--active')) {
                    evt.preventDefault();
                    return false;
                }

                $(self.preloaderContent).height($(self.productList).height()).removeClass('hidden').show();
                TweenMax.to(window, 0.5, {scrollTo: {y: $(self.$el).offset().top}});

                self.startIndex = self.startIndex + self.componentJson.pagination.itemsPerPage;
                self.isPageLoad = false;

                //pagination handler common service
                commonService.pagination(self.defaultSettings.paginationBtn, $this, self);

                // render search listing
                self._populateData();
            },
            /**
            @description: handle load more click event
            @method: _handleLoadMore
            @return: {null}
            **/
            _handleLoadMore: function() {
                var self = this,
                    settings = self.defaultSettings;

                document.addEventListener('scroll', function (event) {
                    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop,
                        scrollOffset = scrollTop + window.innerHeight + $('.c-footer-container').height() - 50;

                    if ((document.body.scrollHeight <= scrollOffset) && (self.pageNum >= 2) && self.pageScroll === true) {
                        self.pageScroll = false;
                        $(settings.loadmoreBtn).click();
                    }
                });
            }
        });
    });
    return PageListingV2;
});