/*global define*/

define(['slick', 'commonService', 'ratings'], function() {
    'use strict';

    var RecipeListing = IEA.module('UI.recipe-listing', function(recipeListing, app, iea) {

        var commonService = null,
            ratingService = null,
            self = this,
            isMobile = null;


        _.extend(recipeListing, {

            defaultSettings: {
                searchWrapper: '.c-recipe-listing__wrapper',
                filterWrapper: '.c-recipe-listing-filter__filter',
                template: 'partials/recipe-listing.hbss',                
                paginationTemplate: 'pagination/defaultView.hbss',
                recipeListingList: '.c-recipe-listing__list',
                paginationContainer: '.c-recipe-listing-pagination',
                loadmoreWrapper: '.c-recipe-listing__loadmore',
                noResult: '.c-recipe-listing-noResult',
                loadmoreBtn: '.js-btn-loadmore',
                filtertags: '.js-filter-tags',
                isLoadMore: false,
                pageLoad: true,
                preloader: '.c-recipe-listing__loadmore .o-preloader',
                preloaderContent: '.o-preloader--content',
                startPageNum: 1,
                pageNum: 1,
                queryParamsStore: [],                
                paginationBtn: '.js-btn-pagination',
                carouselSettings: {
                    'carouselTabP': {
                        infinite: false,
                        speed: 1250,
                        arrows: true,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        focusOnSelect: true,
                        dots: true
                    },
                    'carouselMob': {
                        infinite: false,
                        speed: 1250,
                        slidesToShow: 1,
                        focusOnSelect: true,
                        centerMode: true,
                        centerPadding: '25%',
                        arrows: true
                    }
                }
            },

            events: {
                'change .c-recipe-listing-filter__filter': '_filterChangeHandler', //update params and render search results on dropdown change
                'click .js-apply-filters': '_filterApplyHandler',
                'click .js-cancel-filters': '_filterCancelHandler',
                'click .js-clear-filters': '_filterClearHandler',
                'click .js-more-filters': '_filterExpandHandler',
                'click .js-see-filters': '_filterSeeHandler',
                'click .js-remove-filterTag': '_removeFilterTagHandler',
                'change .c-recipe-listing-sort__dropdown': '_sortHandler',
                'click .js-btn-loadmore': '_loadMorehandler', // render results on show all button click               
                'click .js-btn-pagination' : '_paginationHandler',
            },

            'carouselWrapper': '.js-related-recipes-carousel',
            'carouselSlide': '.c-recipe-listing__list-item',
            'firstLoad': true,


            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function(options) {
                this._super(options);
                commonService = new IEA.commonService();
                ratingService = new IEA.ratings();
                this.triggerMethod('init');
            },
            /**
             * start carousels
             * @method carouselInit
             * @return
             */
            carouselInit: function() {
                var carouselWrapper = $(self.carouselWrapper);

                setTimeout(function() {
                    self._setCarouselSize(self, carouselWrapper);
                    carouselWrapper.slick('slickGoTo', 1);
                    carouselWrapper.slick(self.defaultSettings.carouselSettings.carouselTabP);
                }, 100);
            },


            init: function() {
                //cloned the carousel slides for the desktop & tablet version
                var string = $('.js-related-recipes-carousel').html();
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function() {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function() {
                this.triggerMethod('beforEnable');
                var self= this;

                self.recipeListingJson = self.getModelJSON().recipeListing;
                self.pageNum = self.defaultSettings.pageNum;
                self.pageScroll = false;
                self.pagination = self.$el.find(self.defaultSettings.pagination);
                self.loadmoreWrapper = self.$el.find(self.defaultSettings.loadmoreWrapper);
                self.isPageLoad = self.defaultSettings.isPageLoad;
                self.preloaderContent = self.$el.find(self.defaultSettings.preloaderContent);
                self.recipeList = self.$el.find(self.defaultSettings.recipeListingList);
                self.startIndex = self.defaultSettings.startIndex;
                self.listingItem = self.defaultSettings.listingItem;
                self.selectedFiltersIndex = 0;
                self.activeState = false;

                // Carousel view mode
                if (self.recipeListingJson.componentVariation === 'carousel-view') {
                    self.isMobile = commonService.isMobile() ? true : false;

                    setTimeout(_.bind(function() {
                        if (self.$el.find('.js-related-recipes-container').attr('data-type') === 'carousel') {

                            self.init();
                            if ($('.js-related-recipes-carousel').hasClass('enable-related-recipes-carousel')) {
                                if (self.recipeListingJson.recipes[0].relatedRecipes.length > 3) {
                                    self.carouselInit();
                                }
                            }
                        }
                    }, self), 100);

                    //carousel click Analytics
                    self._ctCarouselClick();
                }

                // list-view mode
                if (self.recipeListingJson.componentVariation === 'list-view') {
                    self.pageNum = self.defaultSettings.pageNum;
                    self.pageScroll = false;
                    
                    //reinitiate lazy load method
                    self._initiateLazyLoad();

                    // show/hide load more on page scroll
                    self._handleLoadMore();

                    // render pagination
                    self._renderPagination(self.recipeListingJson.recipeConfig.totalCount);

                    /*Display Filter logic  - Sticky on page scroll*/
                   // self._displyFilters();
                    self.$el.find('.c-filter-see').hide();
                    self.$el.find('.c-filter__resultCount span').text(self.recipeListingJson.recipeConfig.totalCount);

                   /* $(window).on('scroll', function() {
                        var window_top = $(window).scrollTop(),
                            div_top = $('.c-filter-tags').offset().top;
                        if (window_top > div_top) {
                            $('.c-filter-tags').addClass('stick');
                            $('.c-filter-clear').hide();
                            $('.c-recipe-listing-sorting').hide();
                            $('.c-filter-see').show();
                        } else {
                            $('.c-filter-tags').removeClass('stick');
                            $('.c-recipe-listing-sorting').show();
                            $('.c-filter-clear').show();
                            $('.c-filter-see').hide();
                        }
                    });*/
                }

                //Recipe attributes display in recipe listing
               self._showRecipeAttributes();
               self._showRecipeDietaryAttributes();
               self._showRecipeFeaturedTags();

                // Rating service initialization for listing
                if (self.recipeListingJson.recipeConfig.recipeRating) {
                    var options = {};
                    $.each(self.recipeListingJson.recipes, function() {
                        options.ratingReview = this.recipeRating;
                        options.brandName = this.recipeRating.brandName;
                        ratingService.initRatings(options);
                    });
                }
                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/

            _showRecipeAttributes: function() {
                var self = this,
                    attributeName;
                $.each(self.$el.find('.c-recipe-listing-item .c-recipe-attributes__list-item'), function() {
                    var context = $(this).find('.c-recipe-attributes__value'),
                        recipelistItem = $(this),
                        attributeName = context.data('attributename'),
                        recipeItemId = $(this).parent().data('recipeid');                        
                    $.each(self.recipeListingJson.recipes, function(key, value) {
                        if (this.recipeId == recipeItemId) {
                            $.each(value, function(k, v) {
                            if($.inArray(k.toLowerCase(), ['','preparationtime', 'cookingtime', 'difficulties', 'serves']) > 0) {
                                if (k === attributeName) {
                                    context.html(v);
                                    if(attributeName === 'difficulties' && v.length) {
                                         context.html(v[0].categoryName);
                                    }
                                    recipelistItem.show();
                                }
                                else {
                                    recipelistItem.hide();
                                }
                                return;
                            } 
                        });
                        }
                    });
                });
            },
            _showRecipeDietaryAttributes: function() {
                var self = this,
                    attributeName;
                $.each(self.$el.find('.c-recipe-listing-item .c-recipe-dietary-attributes__list-item'), function() {
                    var context = $(this).find('.c-recipe-dietary-attributes__label'),
                        recipelistItem = $(this),
                        attributeName = context.data('attributename'),
                        recipeItemId = $(this).parent().data('recipeid');                        
                    $.each(self.recipeListingJson.recipes, function(key, value) {
                        if (this.recipeId == recipeItemId) {                            
                            $.each(this.dietary, function(k,v) {                                
                                return attributeName == k ? (v.stateValue == "true" ? recipelistItem.show() : recipelistItem.hide()) : void 0;
                            })
                        }
                    })
                })
            },
            _showRecipeFeaturedTags: function() {
                var self = this,
                    attributeName;
                $.each(self.$el.find('.c-recipe-listing-item .c-recipe-listing__tags-item'), function() {
                    var recipelistItem = $(this),
                        recipeItemId = $(this).parent().data('recipeid');                        
                    $.each(self.recipeListingJson.recipeConfig.featureTags, function(key, value) {                       
                        return recipelistItem.text() === value.tagName ? recipelistItem.show() : recipelistItem.hide();
                    })
                })
            },

            _setCarouselSize: function(self, carouselWrapper) {
                var self = this,
                    $slideItems = $(self.carouselSlide);
                //remove slick if it has been loaded
                if (self.firstLoad) {
                    self.firstLoad = false;
                } else {
                    carouselWrapper.slick('unslick');
                }

                if (self.isMobile) {
                    carouselWrapper.slick(self.defaultSettings.carouselSettings.carouselMob);
                } else {
                    carouselWrapper.slick(self.defaultSettings.carouselSettings.carouselTabP);
                }

                //scale the middle selected slide and got to the middle
                carouselWrapper.slick('slickGoTo', 1);
            },

            // Carousel click tracking
            _ctCarouselClick: function() {
                var ev = {},
                    self = this,
                    carouselWrapper = $(self.carouselWrapper),
                    carouselWrapperItem = self.$el.find(carouselWrapper),
                    label;

                $(document).on('click', '.slick-arrow, .slick-dots li', function(evt) {
                    var _this = $(this),
                        hostedUrl = window.location.href;

                    self._ctComponentInfo();

                    if (_this.is('.slick-arrow')) {
                        label = _this.text();
                    } else {
                        label = 'button#' + parseInt(_this.find('button').text());
                    }

                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.carouselClick,
                        'eventLabel': label + ' - ' + hostedUrl
                    };

                    ev.category = { 'primaryCategory': ctConstants.custom };
                    digitalData.event.push(ev);
                });
            },

            // component attributes tracking
            _ctComponentInfo: function() {
                var self = this,
                    compName = self.$el.find('[data-componentname]').data('componentname'),
                    compVar = self.$el.find('[data-component-variants]').data('component-variants'),
                    compPos = self.$el.find('[data-component-positions]').data('component-positions');

                digitalData.component = [];
                digitalData.component.push({
                    'componentInfo': {
                        'componentID': compName,
                        'name': compName
                    },
                    'attributes': {
                        'position': compPos,
                        'variants': compVar
                    }
                });
            },
            _displyFilters: function() {
                var self = this;
                self.$el.find($('.c-recipe-listing-filter__block .c-recipe-listing-filter__container')).hide();
            },

            
            _getServiceQueryParams: function() {
                var self = this,
                    serviceQueryParams = '',
                    filterParams = '',
                    pageNumParam;
                $(self.defaultSettings.filtertags).html('');

                $(self.defaultSettings.filterWrapper).each(function(key, val) {
                    var $this = $(this),
                        filterType = $this.data('filter-type'),
                        selectedVal = '',
                        selectedLabel;

                    switch (filterType) {
                        case 'dropdown':
                            selectedVal = $this.find('option:selected').val();
                            selectedLabel = $this.find('option:selected').text();
                            break;

                        case 'radioButton':
                        case 'checkBox':
                            selectedVal = ($this.is(':checked')) ? $this.val() : '';
                            selectedLabel = $this.data('label');
                            break;
                    }

                    filterParams += self.recipeListingJson.recipeConfig.filterConfig.filterParamName + '=' + selectedVal + '&';
                    /*Logic to add tags as per selected filter */
                    if (selectedVal !== '' && $('.js-filter-tags').find('li[data-filter-value="' + selectedVal + '"]').length == 0) {

                        (self.defaultSettings.queryParamsStore).push(filterType + "__" + selectedVal);
                        var tagList = '<li class="" data-filter-type="' + filterType + '" data-filter-value="' + selectedVal + '">' + selectedLabel + ' <span class="js-remove-filterTag">x</span></li>';
                        $(self.defaultSettings.filtertags).append(tagList);
                    }
                });

                pageNumParam = self.recipeListingJson.recipeConfig.filterConfig.pageNoParamName + '=' + self.pageNum;

                serviceQueryParams += '?' + filterParams + pageNumParam;

                return serviceQueryParams;
            },

            /**
             @description: Backbone.js Bind Callback to Successful Model Fetch
             @method _searchView
             @return {null}
             **/

            _populateData: function() {
                /* Backbone.js Bind Callback to Successful Model Fetch */
                var self = this,
                    options = {};

                options.queryParams = self._getServiceQueryParams();

                options.servletUrl = self.recipeListingJson.recipeConfig.filterConfig.ajaxURL;

                options.onCompleteCallback = function(response) {
                    if (response) {
                        var parsedJson = $.parseJSON(response);
                        self._updateRecipeListing(parsedJson.data.recipeListing.recipes, parsedJson.data.recipeListing.recipeConfig.totalCount);
                    }
                };
                commonService.getDataAjaxMethod(options);
            },

            /**
            @description: update data on filter change
            @method: _filterChangeHandler
            @return: {null}
            **/
            _filterChangeHandler: function(evt) {
                if (!($('.c-recipe-listing-filter').find('.c-recipe-listing-filter__expand').is(':visible'))) {
                    var self = this, selectedVal = '',selectedLabel;
                    self._filterApplyHandler();
                }
                var filterType = $(event.target).data('filter-type'),
                    $this = $(event.target);
                switch (filterType) {
                    case 'dropdown':
                        selectedVal = $this.find('option:selected').val();
                        selectedLabel = $this.find('option:selected').text();
                        if(!($this.find('option:selected'))) {
                            $(self.defaultSettings.filtertags).find('li["data-filter-value="'+selectedVal+']').remove();
                        }
                    break;

                    case 'radioButton':
                    case 'checkBox':
                        if(!($this.is(':checked'))) {
                            $(self.defaultSettings.filtertags).find('li[data-filter-value="'+$this.val()+'"]').remove();
                        } 
                    break;
                }
            },

            _filterApplyHandler: function() {
                var self = this;

                self.pageNum = self.defaultSettings.startPageNum;
                self.isPageLoad = true;

                $(self.preloaderContent).removeClass('hidden').show();
                
                if (self.defaultSettings.isLoadMoreClicked) {
                    self.defaultSettings.isLoadMore = false;
                }
                
                self._populateData();
                self._filterCollapseHandler();
            },

            _filterClearHandler: function(event) {
                var self = this;
                $('.c-recipe-listing-filter__block select').prop('selectedIndex', 0);
                $('.c-recipe-listing-filter__block input').prop('checked', false);
                if (event !== 'cancel') {
                    $(self.defaultSettings.filtertags).html('');
                    self._populateData();                    
                }
                self._filterCollapseHandler();
            },
            _filterCancelHandler: function() {
                var self = this,
                    queryParamsStore = self.defaultSettings.queryParamsStore;
                self._filterClearHandler('cancel');

                for (var i = 0; i < (queryParamsStore).length; i++) {

                    var filterType = (queryParamsStore[i]).split('__')[0],
                        filterCategoryVal = (queryParamsStore[i]).split('__')[1];

                    switch (filterType) {
                        case 'dropdown':
                            //$('select.c-recipe-listing-filter__filter option[value='+filterCategoryVal+']').attr('selected', true);
                            $('select.c-recipe-listing-filter__filter').val(filterCategoryVal).prop('selected', true);
                            break;

                        case 'radioButton':
                        case 'checkBox':
                            $(self.defaultSettings.filterWrapper).filter(function() {
                                return this.value == filterCategoryVal
                            }).prop('checked', true);

                            break;
                    }
                }
            },
            _filterExpandHandler: function() {
                var self = this;
                $('.c-recipe-listing-filter__expand').show();
                $('.c-filter-more').hide();
            },
            _filterCollapseHandler: function() {
                var self = this;
                $('.c-recipe-listing-filter__expand').hide();
                $('.c-filter-more').show();
            },

            /**
            @description: remove filter tags on click of cross on tags
            @method: _filterChangeHandler
            @return: {null}
            **/
            _removeFilterTagHandler: function(e) {
                var self = this,
                    clearFlag = false,
                    filterValue = $(e.currentTarget).parent().data('filter-value');
                $(self.defaultSettings.filterWrapper).each(function(key, val) {
                    var $this = $(this),
                        filterType = $this.data('filter-type'),
                        selectedVal = '';
                    switch (filterType) {
                        case 'dropdown':
                            if (filterValue == $this.find('option:selected').val()) {
                                $this.prop('selectedIndex', 0);
                                clearFlag = true;
                                break;
                            }
                        case 'radioButton':
                        case 'checkBox':
                            if ($this.is(':checked') && filterValue == $this.val()) {
                                $this.prop('checked', false);
                                clearFlag = true;
                                break;
                            }
                    }
                    if (clearFlag) {
                        return false;
                    }
                });
                $(e.currentTarget).parent().remove();
                self._populateData();
            },

            _filterSeeHandler: function() {
                $('.c-filter-tags').removeClass('stick');
                $('.c-recipe-listing-sorting').show();
                $('.c-filter-clear').show();
                $('.c-filter-see').hide();
            },

            _sortHandler: function(e) {
                var self = this,
                    options = {},
                    sortParams = '?';
                sortParams += self.recipeListingJson.recipeConfig.sortConfig.sortParams.sortingCategory + '=' + $(e.currentTarget).val() + '&';
                sortParams += self.recipeListingJson.recipeConfig.sortConfig.sortParams.pageSize + '=' + self.recipeListingJson.recipeConfig.pagination.itemsPerPage + '&';

                options.queryParams = sortParams;

                options.servletUrl = self.recipeListingJson.recipeConfig.filterConfig.ajaxURL;

                options.onCompleteCallback = function(response) {
                    if (response) {
                        var parsedJson = $.parseJSON(response);
                        self._updateRecipeListing(parsedJson.data.recipeListing.recipes, parsedJson.data.recipeListing.recipeConfig.totalCount);
                    }
                };

                commonService.getDataAjaxMethod(options);
            },
            /**
            @description: handle load more click event
            @method: _updateRecipeListing
            @return: {null}
            **/

            _updateRecipeListing: function(pages, totalCount) {
                var self = this,
                    pageData = $(pages),
                    settings = self.defaultSettings,
                    listing = '',
                    template = self.getTemplate('recipe-listing', settings.template);

                
                // Update total count
                self.recipeListingJson.recipeConfig.totalCount = totalCount;
                self.$el.find('.c-filter__resultCount span').text(self.recipeListingJson.recipeConfig.totalCount);

                if(totalCount > 0) {
                    pageData.each(function(key, item) {
                        listing += template(item);
                    });
                    $(self.defaultSettings.noResult).hide();

                    
                    //hide preloader
                    //$(self.preloaderContent).addClass('hidden');
                    $(settings.preloader).addClass('hidden');
                    if (self.pageNum > 1 && self.recipeListingJson.recipeConfig.pagination.paginationType === 'infiniteScroll') {
                        $(settings.recipeListingList).append(listing);
                    } else {
                       $(settings.recipeListingList).html(listing);
                    }

                    // Update CTA Label
                        self.$el.find(settings.ctaAnchor).text(self.recipeListingJson.recipeConfig.ctaLabelText);
                        self._renderPagination(totalCount);
                        // load more params
                        self._updateParamsOnPageScroll(totalCount);
                        if (totalCount > self.recipeListingJson.recipeConfig.pagination.itemsPerPage) {
                               $(self.pagination).removeClass('hidden');
                               $(self.loadmoreWrapper).removeClass('hidden');
                        } else {
                            $(self.pagination).addClass('hidden');
                            $(self.loadmoreWrapper).addClass('hidden');
                        }

                        // Init rating 
                        if (typeof ratingReview !== 'undefined' && typeof ratingReview.widget !== 'undefined') {
                            ratingService._kritiqueRatings(ratingReview);
                        } 

                        //reinitiate lazy load method
                        self._initiateLazyLoad();   

                        self._showRecipeAttributes();
                        self._showRecipeDietaryAttributes();
                        self._showRecipeFeaturedTags();

                }
                else {
                    $(self.defaultSettings.noResult).show();
                    $(settings.recipeListingList).html(listing);
                }
               
            },

            // trigger lazy load method
            _initiateLazyLoad: function() {
                //reinitiate lazy load method
                commonService.imageLazyLoadOnPageScroll(); // Init lazy load
                var throttledLazyLoad = _.throttle(commonService.imageLazyLoadOnPageScroll, 150);
                $(window).on('scroll.lazyLoad', throttledLazyLoad); // Window scroll lazy load 
            },

            _renderPagination: function(totalCount) {
                var self = this,
                    paginationTemplate = self.getTemplate('pagination', self.defaultSettings.paginationTemplate),
                    combinedJson = null,
                    totalPages = Math.ceil(totalCount/self.recipeListingJson.recipeConfig.pagination.itemsPerPage),
                    paginationList = [];

                for (var i=0; i < totalPages; i++) {
                    paginationList.push(i);
                }

                combinedJson = {
                    'componentJson': self.recipeListingJson,
                    'pageNum': self.defaultSettings.pageNum,
                    'totalPages': totalPages,
                    'paginate': paginationList
                };

                // render pagination
                if (self.pageLoad) {
                    $(self.pagination).html(paginationTemplate(combinedJson));
                }

            },
            

            /**
            @description: handle load more click event
            @method: _updateParamsOnPageScroll
            @return: {null}
            **/

            _updateParamsOnPageScroll: function(totalCount) {
                var self = this,
                    itemsPerPage = self.recipeListingJson.recipeConfig.pagination.itemsPerPage;

                self.pageScroll = (totalCount > itemsPerPage * self.pageNum);
            },

            /**
            @description: handle load more click event
            @method: _loadMorehandler
            @return: {null}
            **/

            _loadMorehandler: function(e) {
                var self = this,
                    $this = $(e.currentTarget),
                    settings = self.defaultSettings;

                e.preventDefault();

                self.pageNum++;

                $this.fadeOut(500);
                $(settings.preloader).removeClass('hidden');
                settings.isLoadMore = true;

                // render search listing
                self._populateData();
            },

            _paginationHandler: function(evt) { //new
                var self = this,
                    $this = $(evt.currentTarget);

                 if ($this.hasClass('o-btn--disabled') || $this.hasClass('o-btn--active')) {
                    evt.preventDefault();
                    return false;
                }

                $(self.preloaderContent).height($(self.recipeList).height()).removeClass('hidden').show();
                TweenMax.to(window, 0.5, {scrollTo: {y: $(self.$el).offset().top}});

                self.startIndex = self.startIndex + self.recipeListingJson.pagination.itemsPerPage;
                self.isPageLoad = false;

                //pagination handler common service
                commonService.pagination(self.defaultSettings.paginationBtn, $this, self);

                // render search listing
                self._populateData();
            },

            
            /**
            @description: handle load more click event
            @method: _handleLoadMore
            @return: {null}
            **/
            _handleLoadMore: function() {
                var self = this,
                    settings = self.defaultSettings;

                document.addEventListener('scroll', function(event) {
                    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop,
                        scrollOffset = scrollTop + window.innerHeight + $('.c-footer-container').height() - 50;

                    if ((document.body.scrollHeight <= scrollOffset) && (self.pageNum >= 2) && self.pageScroll === true) {
                        self.pageScroll = false;
                        $(settings.loadmoreBtn).click();
                    }
                });
            }


        });
    });

    return RecipeListing;
});
