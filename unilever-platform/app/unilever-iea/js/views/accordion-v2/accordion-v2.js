/*global define*/

define(function() {
    'use strict';

    var AccordionV2 = IEA.module('UI.accordion-v2', function(accordionV2, app, iea) {

        _.extend(accordionV2, {

            rootClassName: 'accordion-v2',

            events: {
                'click .accordion-v2-panel-heading__link': '_handleExpandCollapse'
            },

            initialize: function (options) {

                this._super(options);
                this.triggerMethod('init');

                var additionalOptions = {
                    defaultSettings: {
                        accordionContainer: '.' + this.rootClassName,
                        headingContainer: '.' + this.rootClassName +  '-panel-heading',
                        speed: 400
                    },
                    cssClasses: {
                        panelHeadingContainer: this.rootClassName + '-panel-heading',
                        isMultiExpand: 'js-accordion-v2--is-multi-expand', // NOTE: js-
                        panelHeading: {
                            isCollapsed: this.rootClassName + '-panel-heading--is-collapsed'
                        },
                        panelBody: {
                            isCollapsed: this.rootClassName + '-panel__body--is-collapsed'
                        }
                    }
                };

                this.options = _.extend(additionalOptions, options);

                return this;
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this.triggerMethod('beforeEnable');

                this.options.isMultiExpand = this.$el.find(this.options.defaultSettings.accordionContainer)
                    .hasClass(this.options.cssClasses.isMultiExpand);

                this.triggerMethod('enable');
            },

            expandPanel: function(elem) {
                var self = this;

                // First remove the collapsed class
                elem.removeClass(this.options.cssClasses.panelHeading.isCollapsed)
                    .attr('aria-expanded', true)
                    // Then find its panel body sibling and remove the equivalent collapsed class
                    .next()
                    .removeClass(self.options.cssClasses.panelBody.isCollapsed);


                // Analytics script: Please do not change or remove
                // Send flag when user clicks on Accordian panel heading or a (+) sign to expand a panel
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined' && jTitle) {  

                    var ev = {},
                        $this = $('.accordion-v2'),
                        panelHeading = elem.data('component-panel-heading'),
                        parentWrapper = $this.find('[data-componentname]'),
                        parentCompInfo = (parentWrapper !== undefined && parentWrapper.data('componentname') !== '') ? parentWrapper : parentWrapper.parents('[data-componentname]'),

                        compName = parentCompInfo.data('componentname'),
                        compVariants = parentCompInfo.data('component-variants'),
                        compPositions = parentCompInfo.data('component-positions');

                    // track component details
                    digitalData.component = [];
                    digitalData.component.push({
                        'componentInfo' :{
                            'name': compName
                        },
                        'attributes': {
                            'position': compPositions,
                            'variants': compVariants
                        }
                    });
                    
                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.accordianClicked,
                        'eventLabel' : jTitle + ' - ' + panelHeading
                    };

                    ev.category = {
                        'primaryCategory' : ctConstants.custom
                    };

                    ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
                    
                    digitalData.event.push(ev);
                }
            },

            collapsePanel: function(elem) {

                this.triggerMethod('BeforeCollapse', elem);

                var self = this;

                // Add collapse to heading
                elem.addClass(self.options.cssClasses.panelHeading.isCollapsed)
                    .attr('aria-expanded', false)
                    .next()
                    .addClass(self.options.cssClasses.panelBody.isCollapsed);

                this.triggerMethod('AfterCollapse', elem);
            },

            collapseExpandedPanels: function(elem) {
                var self = this;

                elem.closest(this.options.defaultSettings.accordionContainer)
                    .find(this.options.defaultSettings.headingContainer)
                    .not(this.options.cssClasses.panelHeading.isCollapsed)
                    .each(function(i, element) {
                        self.collapsePanel($(element));
                    });
            },

            expand: function(elem, options) {

                this.triggerMethod('BeforeExpand', elem);

                // If this is not a multiple expanded accordion, collapse the currently expanded panel(s)
                if (!this.options.isMultiExpand) {
                    this.collapseExpandedPanels(elem);
                }

                // Then expand this panel
                this.expandPanel(elem);

                this.triggerMethod('AfterExpand', elem);
            },

            /* ----------------------------------------------------------------------------- *\
             Private Methods
             \* ----------------------------------------------------------------------------- */

            /**
             handle expand and collapse

             @method _handleExpandCollapse

             @return {null}
             **/

            _handleExpandCollapse: function(evt, options) {
                evt.preventDefault();

                var _self = this;
                var target = $(evt.currentTarget).closest(_self.options.defaultSettings.headingContainer);

                if (!target.hasClass(_self.options.cssClasses.panelHeading.isCollapsed)) {
                    _self.collapsePanel(target);
                } else {
                    _self.expand(target, options);
                }
            }
        });
    });

    return AccordionV2;
});