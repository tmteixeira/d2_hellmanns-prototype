/*global define*/

define(['commonService'], function() {
    'use strict';

    var ThankYou = IEA.module('UI.thank-you', function(thankYou, app, iea) {

        var commonService = null,
            orderFeed = null,
            cartFeed = null;

        _.extend(thankYou, {

            /*********************PUBLIC METHODS******************************************/


            TurnToResponse: "",
            customerDetail: {},
            orderData: [],
            addFeedPurchaseOrder: [],
            item: [],
            addLineItem: [],
            counter: 0,
            shipping_cost: 0,
            item_cost: 0,

            initialize: function(options) {
                commonService = new IEA.commonService(); // creates new instance for common service
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function() {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function() {
                this.triggerMethod('beforeEnable');
                var self = this;
                self.thankyouData = self.getModelJSON().thankYou;

                this._getOrderId();
                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/
            getUrlParameter: function(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            },
            _getOrderId: function() {
                var orderid = this.getUrlParameter('oid'),
                    self = this,
                    jsonData = this.getModelJSON().thankYou;
                if (orderid !== "") {
                    $('.js-order-number span').text(orderid);
                } else {
                    $('.js-order-number span').text(jsonData.failureMessage);
                }
                //analytics call
                  if (typeof digitalData !== "undefined") {
                var ev = {}; 
                    ev.eventInfo={
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.ordersummary,
                      'eventLabel' : orderid
                    };
                    ev.category ={'primaryCategory':ctConstants.custom};

                    digitalData.event.push(ev);
                }

                self._orderDetail(orderid);

            },
            _orderDetail: function(orderid) {

                //url to capture order Details
                var apiUrl = framesApiCall + '/v2/unlv/token/' + UnlvCart.activeToken() + "/order/" + orderid,
                    self = this,
                    options = {};
                options.servletUrl = apiUrl;
                options.queryParams = "";
                options.onCompleteCallback = function(data) {
                    self._generateTurntoFeed(data);
                };

                commonService.getDataAjaxMethod(options);
            },

            // TurnTo feed 
            _generateTurntoFeed: function(obj) {
                var self = this,order_number,  merchant="", productData = [],
                    turnToFlag = false,
                    orderDetail = {},            
                    purchaseFlag = true, // Flag
                    jsonObj = jQuery.parseJSON(obj),
                    jsonLen = Object.keys(jsonObj).length;

                $.each(jsonObj, function(index, data) {
                    if (purchaseFlag) {
                        order_number =  data.order_number;
                        orderDetail['orderId'] = data.order_number;
                        orderDetail['email'] = data.billing_email;
                        orderDetail['firstName'] = data.billing_first_name;
                        orderDetail['lastName'] = data.billing_last_name;
                        orderDetail['postalCode'] = data.billing_postal_code;
                        self.addFeedPurchaseOrder.push(orderDetail); //TurnTo.addFeedPurchaseOrder json ready    
                        console.log('addFeedPurchaseOrder ' + self.addFeedPurchaseOrder);

                        purchaseFlag = false;

                        orderDetail['nickName'] = "";
                        orderDetail['deliveryDate'] = "";
                        // orderDetail['emailOptOut'] = "";

                        // Customer details for schema  
                        self.customerDetail = orderDetail;
                        delete self.customerDetail['orderId'];
                        delete self.customerDetail['nickName'];
                        delete self.customerDetail['deliveryDate'];

                        self.item_cost = data.item_total;
                        self.shipping_cost = data.total - data.item_total;

                       
                    }
                    var productDetail = {};

                    productDetail['title'] = data.title;
                    productDetail['sku'] = data.sku;
                    productDetail['upcCode'] = data.unlv_internal_id;
                    productDetail['price'] = data.price;
                    merchant += data.merchant+" | ";

                    // Ajax call for product information
                    var productId = data.unlv_internal_id,productVariant,
                        productApiUrl = framesApiCall + "/v2/unlv/token/" + UnlvCart.activeToken() + "/product_by_external_id/" + productId + '/brand/' + self.thankyouData.campaign,

                        options = {};
                    options.servletUrl = productApiUrl;
                    options.queryParams = "";
                    options.onCompleteCallback = function(productJson) {
                        var pJson = JSON.parse(productJson);
                        self.counter = self.counter + 1
                        $.each(pJson, function(j, prod) {
                            if (data.merchant_id.toString() === pJson[j].product_objects[0][0].merchant_id) {
                                productDetail['url'] = pJson[j].product_objects[0][0].url2;
                                productDetail['itemImageUrl'] = pJson[j].product_objects[0][0].variance[0].image[0];
                                productVariant = pJson[j].product_objects[0][0].variance[0].size;
                            }
                        });

                        self.item.push(productDetail)
                        //self.addLineItem.push(productDetail);
                        delete self.addLineItem['upcCode'];

                        if ((jsonLen) === self.counter) {
                            turnToFlag = true;
                        }
                    };
                    commonService.getDataAjaxMethod(options);


                    // purchase order analytics
                   var productDetailAnalytics = { 
                            productInfo : {}, 
                            category : {},
                            attributes : {} 
                        }
                        productDetailAnalytics.productInfo['productID'] = data.unlv_internal_id;
                        productDetailAnalytics.productInfo['productName']=  data.product_name;
                        productDetailAnalytics.productInfo['price'] = data.price;
                        productDetailAnalytics.productInfo['brand'] = self.thankyouData.campaign;
                        productDetailAnalytics.productInfo['quantity'] = data.quantity;
                        productDetailAnalytics.category['primaryCategory'] = '';
                        productDetailAnalytics.attributes['productVariants'] = productVariant;
                        productDetailAnalytics.attributes['listPosition'] =  index;
                        
                        productData.push(productDetailAnalytics)

                });

                 // purchase order analytics
                   if (typeof digitalData !== "undefined") {
                    var ev = {};
                    digitalData.product=[];
                    digitalData.product = productData;

                    digitalData.transaction=[];
                    digitalData.transaction.push({
                    'transactionID': order_number,
                    'total':{
                    'basePrice': self.item_cost,
                    'taxRate': '',
                    'shipping': self.shipping_cost
                    },
                    'attributes': {
                    'affiliation':  merchant,          
                    'coupon': ''
                    }               
                    });

                    ev.eventInfo={
                      'type':ctConstants.purchaseenhanced,
                    };
                    ev.category ={'primaryCategory':ctConstants.conversion};
                    digitalData.event.push(ev);
                }

                // TurnTo feed 3 calls 
             /*   var checkTurnTo = setInterval(function() {
                    if (typeof TurnToFeed !== "undefined" && turnToFlag) {
                        // TurnTo feed addFeedPurchaseOrder call
                        var response = TurnToFeed.addFeedPurchaseOrder(orderDetail);

                        $.each(self.addLineItem, function(i, product) {
                            TurnToFeed.addFeedLineItem(product);
                        });
                        TurnToFeed.debugFeed();
                        TurnToFeed.sendFeed();
                        self._generateSchemaJson();
                        clearInterval(checkTurnTo);
                    }
                }, 1000); */
                var checkTurnTo = setInterval(function() {
                    if (turnToFlag) {
                        self._generateSchemaJson();
                        clearInterval(checkTurnTo);
                    }
                }, 1000); 
            },

            // Generate Json as per Schema 
            _generateSchemaJson: function() {
                var jsonSchema = {},
                    self = this,
                    d = new Date(),
                    orderDate = d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + (d.getDate() + 1)).slice(-2) + "T" + ("0" + (d.getHours() + 1)).slice(-2) + ":" + ("0" + (d.getMinutes() + 1)).slice(-2) + ":" + ("0" + (d.getSeconds() )).slice(-2);

                jsonSchema = {
                    'entity': self.thankyouData.entity,
                    'locale': self.thankyouData.locale,
                    'brand': self.thankyouData.serviceBrandName,
                    'orderId': self.getUrlParameter('oid'),
                    'orderDate': orderDate,
                    'feedOrderId': "", // condition check
                    'processedStatus': "",
                    products: [],
                    customerDetail: {}
                };
                jsonSchema.products = self.item;
                jsonSchema.customerDetail = self.customerDetail;
                var jsonCall = JSON.stringify(eval(jsonSchema));
                
                $.ajax({
                    url: self.thankyouData.transactionServiceUrl,
                    type: 'POST',
                    data: jsonCall,
                    contentType: 'application/json',
                    success: function(data) {
                       console.log('success '+data);
                    },
                    error: function(xhr, status, error) {
                       console.log(status +" - "+error);
                    }
                });

            }

        });
    });

    return ThankYou;
});
