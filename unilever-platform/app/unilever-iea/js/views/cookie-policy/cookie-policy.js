/*global define*/

define(['TweenMax', 'cookies'], function(TweenMax, Cookies) {
    'use strict';

    var CookiePolicy = IEA.module('UI.cookie-policy', function(cookiePolicy, app, iea) {

        var SLIDE_ANIMATION_TIME = 0.5;
        var COOKIE_NAME = 'unilever';

        _.extend(cookiePolicy, {

            isContentOpened: false,
            hideTimeout: null,

            events: {

                'click .js-close-btn': '_closeContent'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;

                    this._checkCookie();
                }
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {

            },

            onWindowResized: function() {

                if (this.isContentOpened) {

                    this._showContent();
                }
            },


            /**************************PRIVATE METHODS******************************************/

            _checkCookie: function() {

                if (Cookies.get(COOKIE_NAME)) {

                    return;
                }

                var date = new Date();
                date.setMonth(date.getMonth() + 1);
                Cookies.set(COOKIE_NAME, 'check cookie policy', { expires: date });

                setTimeout(function() {

                    this._setElements();
                    this._showContent(true);
                    this._hideAuto();
                }.bind(this), 2000);
            },

            _hideAuto: function() {

                this.hideTimeout = setTimeout(this._closeContent.bind(this), 20 * 1000);
            },

            _setElements: function() {

                this.$container = this.$el.find('.js-cookie-policy');
                this.$content = this.$el.find('.js-content');
                this.$mainWrapper = $('.js-main-wrapper');
                this.$navBar = $('.o-navbar');
                this.$stickyMenu = $('.js-container-sticky');
            },

            _showContent: function(isAnimated) {

                var contentHeight = this.$content.outerHeight();
                TweenMax.to(this.$container, SLIDE_ANIMATION_TIME, {height: contentHeight});

                if (isAnimated) {

                    TweenMax.to(this.$mainWrapper, SLIDE_ANIMATION_TIME, {top: contentHeight + 'px'});
                } else {

                    this.$mainWrapper.css({top: contentHeight + 'px'});
                }

                var isStickyNavOpened = !!$('.js-container-sticky').length;

                if (!isStickyNavOpened) {

                    if (isAnimated) {

                        TweenMax.to(this.$navBar, SLIDE_ANIMATION_TIME, {top: contentHeight + 'px'});
                    } else {

                        this.$navBar.css({top: contentHeight + 'px'});
                    }

                } else {

                    this.$navBar.css({top: (contentHeight - 80) + 'px'});

                    if (isAnimated) {

                        TweenMax.to($('.js-container-sticky'), SLIDE_ANIMATION_TIME, {top: contentHeight + 'px'});
                    } else {

                        $('.js-container-sticky').css({top: contentHeight + 'px'});
                    }
                }

                this.$container.attr('data-open', 1);
                this.isContentOpened = true;
            },

            _closeContent: function(evt) {

                if (evt) {

                    evt.preventDefault();
                }

                if (this.hideTimeout) {

                    clearTimeout(this.hideTimeout);
                    this.hideTimeout = null;
                }

                TweenMax.to(this.$container, SLIDE_ANIMATION_TIME, {height: 0});
                TweenMax.to(this.$mainWrapper, SLIDE_ANIMATION_TIME, {top: 0});

                var isStickyNavOpened = !!$('.js-container-sticky').length;

                if (!isStickyNavOpened) {

                    TweenMax.to(this.$navBar, SLIDE_ANIMATION_TIME, {top: 0});
                } else {

                    this.$navBar.css({top: '-80px'});
                    TweenMax.to($('.js-container-sticky'), SLIDE_ANIMATION_TIME, {top: 0});
                }

                this.$container.attr('data-open', 0);
                this.isContentOpened = false;
            }

        });
    });

    return CookiePolicy;
});
