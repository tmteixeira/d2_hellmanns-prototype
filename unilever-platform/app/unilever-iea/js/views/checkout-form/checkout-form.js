/*global define*/

define(['braintree', 'eGiftingService'], function(braintree) {
    'use strict';

    var eGiftingService = null;
    var CheckoutForm = IEA.module('UI.checkout-form', function(checkoutForm, app, iea) {

        _.extend(checkoutForm, {
            defaultSettings: {
                formErrorMsg: '.form-error-msg',
                errorMsg: '#errorMsg',
                redirectForm: '#redirectForm',
                optinSmsBrand: '#optIn-smsBrand',
                optinSmsAll: '#optIn-smsAll',
                totalAmount: '.o-product-price-total #totalAmount',
                contactNumber: '#contact-phoneNumbers-value',
                egiftingForm: '#egiftingForm',
                storageName: 'productCartData'
            },

            events: {                
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function(options) {
                var self = this;

                self._super(options);
                self.validator = app.form.validator; // need to include validation DI to define.
                self.formCalendarLoaded = false;
                self.changedSet = {};
                self.isvalidationRequired = false;
                self.isFormValid = false;
                self.isSubmitted = false;
                self.multiValueFields = {};

                self.successRedirectURL = '';
                self.errorRedirectURL = '';
                self.redirectForm = self.defaultSettings.redirectForm;
                self.formComponentJson = self.getModelJSON().checkoutForm.formElementsConfig;

                self.validator.addValidation(self, {
                    model: self.model
                });

                self.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * show copmnent
             * @method show
             * @return
             */
            show: function() {
                this._super();
            },

            /**
             * hide component
             * @method hide
             * @param {} cb
             * @param {} scope
             * @param {} params
             * @return
             */
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
             * clean compnent on exist from the component
             * @method clean
             * @return
             */
            clean: function() {
                this._super();
                this.validator.removeValidation(this, {
                    model: this.model
                });
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');
                
                var moduleName = this.moduleName.hyphenToCamelCase(),
                    modelData = this.getModelJSON()[moduleName],
                    allElementsConfig = modelData.formElementsConfig,
                    idx = 0,
                    item, $elem;

                eGiftingService = new IEA.EGifting();

                this.checkOptins();

                this.getComponentJson();
               
                this.$form = $('form', this.$el);
                this.$formElements = this.$form.find(':input');
                this.popup = IEA.popup();
				this.findDeliveryAddress();
				
                this.form = {
                    isAjax: this.$form.data('ajax') || (modelData.isAjax) ? modelData.isAjax : false,
                    postURL: this.$form.attr('action') || (modelData.servletPath) ? modelData.servletPath : '',
                    failureMessage: this.$form.data('fail') || (modelData.failureMessage) ? modelData.failureMessage : 'Submission Failed',
                    successMessage: this.$form.data('success') || (modelData.successMessage) ? modelData.successMessage : 'Thank you for your submission',
                    validation: this.$form.data('validation') || (modelData.validation) ? modelData.validation : false
                };

                // add validation rules from the model into the validator engine.
                this.validator.addRules(this, {
                    rules: this._createValidationRules() 
                });

                // Getting all the multivalue field together
                for (idx=0; idx < allElementsConfig.length; idx++) {
                    if (allElementsConfig[idx] !== null) {
                        item = allElementsConfig[idx].formElement;
                    }                    
                    if (item !== null && item && item.multivalue) {
                        // adding rule to the first elemnet of multivalue
                        this.multiValueFields[item.name] = {
                            'name': item.name,
                            rules: this.model.validation[item.name]
                        };

                        $elem = $('input[name^='+item.name+']');
                        this._setUpMultiValueField($elem, item.name, item.name+'_1' );

                        delete this.model.validation[item.name];
                    }
                }
                
                this.triggerMethod('enable');
                this.brainTreeErrorAnalytics();
            },

            getComponentJson: function() {
                var moduleName = this.moduleName.hyphenToCamelCase(),
                    modelData = this.getModelJSON()[moduleName],
                    allElementsConfig = modelData.formElementsConfig,
                    idx, item, paymentFormJson, orderSummaryJson;

                // Getting all the multivalue field together
                for (idx=0; idx < allElementsConfig.length; idx++) {
                    item = allElementsConfig[idx];
                    if (item !== null && !item.formElement) {
                        if (item.paymentForm) {
                            paymentFormJson = item.paymentForm;
                        } else if (item.orderSummary) {
                            orderSummaryJson = item.orderSummary;
                        } 
                    }                   
                }
                this.renderPaymentForm(paymentFormJson);
                
                // for the frontend use only
                if ($('.fe-page').length) {
                    this.renderOrderSummary(orderSummaryJson);
                }           

                this.successRedirectURL = paymentFormJson.urlMap.purchaseConfirmationPageURL;
                this.errorRedirectURL = paymentFormJson.urlMap.dyanamicErrorPageURL;
            },

            renderPaymentForm: function(data) {
                var options = {},
                self = this,
                componentJson = data;

                options.cardNumber = '#cardNumber';
                options.expirationMonth = '#expirationMonth';
                options.expirationYear = '#expirationYear';
                options.cvv = '#cvv';
                options.selector = '.js-btn--paycard';
                options.wrapper = '.c-payment-form__paymenttype';
                options.formId = 'egiftingForm';
                options.clientToken = componentJson.clientToken;
                options.button = '.js-egifting-submit-btn';
                options.paymentWrap = '.c-payment-form__wrap';

                options.onCompleteCallback = function(event) {
                    self._handleFormSubmit(event);
                };

                eGiftingService.switchTabs(options);
            },

            renderOrderSummary: function(data) {
                var self = this,
                    combinedJson = null,
                    componentJson = data,
                    template = self.getTemplate('order-summary-list', 'partials/order-summary-list.hbss'),
                    $basketWrapper = $('.c-order-summary__wrapper'),
                    parsedData = null,
                    sessionData = eGiftingService.getItem(this.defaultSettings.storageName);                 

                if (sessionData !== undefined && sessionData !== '' && sessionData !== null) {
                    parsedData = jQuery.parseJSON(sessionData);
                    combinedJson = {
                        'orderSummary': {
                            'orderSummary': componentJson,
                            'productInfo': parsedData
                        }
                    };

                    $basketWrapper.html(template(combinedJson));

                    //update total amount in order summary
                    eGiftingService.calculatePrice(sessionData);
                }
            },

            /* ----------------------------------------------------------------------------- *\
               Private Methods
            \* ----------------------------------------------------------------------------- */

            /**
             * create validation rules from the model data and then create a the validation rules which
             * then is set to the form validation engine
             * @method _createValidationRules
             * @return validationRules
             */
			 findDeliveryAddress:function() {
				 var self = this,
				 formElements = this.formComponentJson,
				 form1 = $("[id*=billing]"),
				 form2 = $("[id*=delivery]"),
				 $deliveryAddr = form1.parent().find("input[type=checkbox]");

				 $deliveryAddr.on("change", function(event) {
					var $this = $(this);
					if($this.is(":checked")){
						form1.each(function(index, val){
							var deliveryId, $val;
							$val = $(val);
							
							deliveryId = $val.attr('id').replace('billing', 'delivery');
							
							if ($val.attr('type') === 'text') {
								$('#' + deliveryId).val($val.val());
							}							
							if ($val[0].tagName.toLowerCase() === 'select' && $val.val() !== '') {					
								$('#' + deliveryId + ' option[value=\'' + $val.val() +'\']').attr('selected', true);
							}
						});
						
					} else {
						form2.each(function(index, val) {
							var $val = $(val);
							
							if ($val.attr('type') === 'text') {
								$(val).val("");
							}					
							if ($val[0].tagName.toLowerCase() === 'select') {							
								$val.find('option:eq(0)').attr('selected', true);
							}
						});
					}
				});	
			 },
            _createValidationRules: function() {
                var self = this,
                    moduleName = this.moduleName.hyphenToCamelCase(),
                    formElements = this.getModelJSON()[moduleName].formElementsConfig,
                    element, elementRule = {},
                    validationRules = {},
                    idx = 0,
                    jdx = 0,
                    pattern;

                for (idx = 0; idx<formElements.length; idx++) {
                    element = formElements[idx];
                    if (element !== null) {
                        if (!_.isFunction(element.formElement) && element.formElement) {
                            validationRules[element.formElement.name] = [];
                            if (typeof element.formElement.rules !== 'undefined' && _.isArray(element.formElement.rules)) {

                                for (jdx = 0; jdx<element.formElement.rules.length; jdx++) {
                                    self.isvalidationRequired = true;
                                    elementRule = element.formElement.rules[jdx];
                                    validationRules[element.formElement.name].push({
                                        xss: true
                                    });
                                    if (_.has(elementRule, 'pattern')) {
                                        if(elementRule.pattern === 'creditcard') {
                                            validationRules[element.formElement.name].push({
                                                creditcard: true,
                                                msg: elementRule.msg
                                            });
                                        } else {
                                            pattern = decodeURIComponent(elementRule.pattern).replace(/%24/g, '$');
                                            validationRules[element.formElement.name].push({
                                                pattern: this.validator.patterns[pattern] || new RegExp(pattern),
                                                msg: elementRule.msg
                                            });
                                        }
                                        
                                    } else {
                                        validationRules[element.formElement.name].push(elementRule);
                                    }
                                }

                            }
                        } else if (!_.isFunction(element.formcheckbox) && element.formcheckbox) {
                            validationRules[element.formcheckbox.name] = [];
                            if (element.formcheckbox.rules[0].required === true && typeof element.formcheckbox.rules !== 'undefined' && _.isArray(element.formcheckbox.rules)) {

                                for (jdx = 0; jdx<element.formcheckbox.rules.length; jdx++) {
                                    self.isvalidationRequired = true;
                                    elementRule = element.formcheckbox.rules[jdx];
                                    validationRules[element.formcheckbox.name].push({
                                        xss: true
                                    });                                   
                                    validationRules[element.formcheckbox.name].push(elementRule);
                                }

                            }
                        }                       
                    } 
                }

                return validationRules;
            },


            /**
             * handle submit
             * @method _handleFormSubmit
             * @param {} evt
             * @return
             */
            _handleFormSubmit: function(evt) {
                var self = this,
                    formData = IEA.serializeFormObject(this.$form),
                    formDatatoSend = this.formToJson(this.$form),
                    submitxhr, idx =0, key, item,
                    formElements = this.formComponentJson,
                    emailValidate = $("[id*=email]"),
                    confirmEmailElm = emailValidate.eq(1),
                    errorMsg, msg = '';

                evt.preventDefault();

                this.model.unset(this.changedSet);
                this.model.set(formData);
                this.changedSet = this.model.changed;

                this.confirmEmail = (function() {
                    formElements.forEach(function(value, index) {
                        if(value && value.formElement && value.formElement.name === $("[id*=email]").eq(1).attr("name")) {
                            msg = 'Confirm email address must match with email address.'; //TODO - Dev team will provide the error message later
                        }
                    });

                    if (emailValidate.eq(0).val() !== confirmEmailElm.val()) {
                        confirmEmailElm.parents('.form-group').addClass('has-error-block');
                        var $span = $("<span />").addClass('error-msg-text help-block-error').text(msg);
                        confirmEmailElm.parent().append($span); 
                        return false;
                    } else {
                        $('.error-msg-text').hide();
                        confirmEmailElm.parents('.form-group').removeClass('has-error-block');
                        return true;
                    }    
                })();

                // Check if the model is valid before saving
                if (!this.isvalidationRequired || (this.isvalidationRequired && this.model.isValid(true) && this.confirmEmail)) {
                    this.isFormValid = true;
                } else {
                    this.isFormValid = false;
                    this.scrollPageToError($('.form').find('.has-error, .has-error-block'));
                    self.checkoutValidationErrorAnalytics();
                }

                for (idx in this.multiValueFields) {
                    var multiValueArray = [],
                        match, elem;
                    item = this.multiValueFields[idx];

                    // Get the value from input field and put it back to its hidden field
                    for(key in formData) {
                        match = key.match(item.name);
                        if(match && match.length > 0) {
                            elem = $('input[name='+key+']');
                            elem.siblings('input[type=hidden]').val(elem.val());
                        }
                    }

                    formData[item] = multiValueArray;
                }

                if(this.$errorBlock) {
                    this.$errorBlock.hide();
                }

                if (this.isFormValid && this.form.postURL !== '' && this.isSubmitted === false) {
                    if (this.form.isAjax) {
                        
                        var submitxhr = $.ajax({
                            type: "POST",
                            url: this.form.postURL,
                            contentType: "application/json",
                            dataType: "json",
                            data: formDatatoSend
                        });

                        submitxhr.done($.proxy(this._successHandler, this));
                        submitxhr.fail($.proxy(this._errorHandler, this));
                    } else {
                        this.$form[0].submit();
                    }
                    this.triggerMethod('formSubmit', this);
                    this.isSubmitted = true;
                }
            },

            /**
             * on success submission of the form
             * @method _successHandler
             * @param {} data
             * @return
             */
            _successHandler: function(data) {
                var self = this;
                if(data.code === 200 || data.code === 201) {
                    // optin tracking
                    this.optinsTracking();

                    if (typeof this.successRedirectURL !== 'undefined' && this.successRedirectURL !== '' && this.successRedirectURL !== false) {
                        // if the router is running, then make sure its using hash url change
                        // otherwise change the complete URL
                        if (IEA.History.started) {
                            Router.navigate(this.successRedirectURL, {
                                trigger: true
                            });
                        } else {
                            self.checkoutSuccessAnalytics();
                            var cookieData = 'cartSession-'+$('.c-simple-header').data('market'),
                                orderDetailsData = {
                                    "pdfToken": data.responseData.results[0].pdfToken,
                                    "orderId": data.responseData.results[0].OrderId,
                                    "userEmailId": $('#contact-email').val()
                                };

                            eGiftingService.setItem('orderDetails', JSON.stringify(orderDetailsData));                                      
                            self.checkoutOrderAnalytics(data.responseData.results[0].OrderId);
                            
                            window.location.href = this.successRedirectURL;
                            
                            // remove cookie and storage data on form submit
                            eGiftingService.clearSession(cookieData, this.defaultSettings.storageName);
                        }
                    } else {
                        this.$sucessMsgBlock = $('<div class="form-success-msg"><span class="glyphicon glyphicon-ok"></span><span class="success-msg-text">' + this.form.successMessage + '</span></div>');
                        this.$form.before(this.$sucessMsgBlock);
                        this.$form.hide();
                    }
                } else {
                    this._errorHandler(data);
                }
                this.triggerMethod('success', this);
                this.isSubmitted = false;
            },

            /**
             * on form submission error
             * @method _errorHandler
             * @param {} err
             * @return
             */
            _errorHandler: function(data) {
                var settings = this.defaultSettings;

                if (typeof this.errorRedirectURL !== 'undefined' && this.errorRedirectURL !== '' && this.errorRedirectURL !== false) {
                    // if the router is running, then make sure its using hash url change
                    // otherwise change the complete URL
                    if (IEA.History.started) {
                        Router.navigate(this.errorRedirectURL, {
                            trigger: true
                        });
                    } else {
                        if (data.errors !== undefined) {
                            if (data.code === 422) {                               
                                $(settings.formErrorMsg).remove();
                                var $errorBlock = $('<div class="form-error-msg text-danger"><span class="glyphicon glyphicon-warning-sign"></span><span class="error-msg-text">' + data.errors[0].error + '</span></div>');
                                $(settings.egiftingForm).prepend($errorBlock); 
                                // Analytics Error
                                this.checkoutErrorAnalytics(data.errors[0].error);                           
                                this.scrollPageToError($(settings.formErrorMsg));                               
                            } else {
                                $(this.redirectForm).attr('action', this.errorRedirectURL);
                                $(settings.errorMsg).val(data.errors[0].error);
                                $(this.redirectForm).submit();
                            }
                        }
                    }
                } else {
                    this.$errorBlock = $('<div class="form-error-msg text-danger"><span class="glyphicon glyphicon-warning-sign"></span><span class="error-msg-text">' + this.form.failureMessage + '</span></div>');
                    this.$form.before(this.$errorBlock);
                }

                this.triggerMethod('error', this.form.failureMessage);
                this.isSubmitted = false;
            },

            formToJson: function(form) {
                var objectGraph = {},
                    $form = $('form', this.$el),
                    sessionData = eGiftingService.getItem(this.defaultSettings.storageName),
                    parsedData;

                if (sessionData !== undefined && sessionData !== '' && sessionData !== null) {
                    parsedData = $.parseJSON(sessionData);
                }

                function add(objectGraph, name, value) {
                    if(name.length === 1) {
                        //if the array is now one element long, we're done
                        objectGraph[name[0]] = value;
                    } else {
                        //else we've still got more than a single element of depth
                        if(objectGraph[name[0]] == null) {
                            //create the node if it doesn't yet exist
                            objectGraph[name[0]] = {};
                        }
                    //recurse, chopping off the first array element
                        add(objectGraph[name[0]], name.slice(1), value);
                    }
                };
                //loop through all of the input/textarea elements of the form
                $form.find('input, textarea, select').each(function() {
                     if($(this).attr('name') !== 'submit') {
                        //split the - notated names into arrays and pass along with the value
                        if ($(this).attr('type') === "checkbox") {
                            add(objectGraph, $(this).attr('name').split('-'), this.checked);
                        } else {
                            if ($(this).val() !== null && $(this).val() !== "") {
                               add(objectGraph, $(this).attr('name').split('-'), $(this).val());
                            }
                            else {
                                add(objectGraph, $(this).attr('name').split('-'), null); 
                            }                        
                        }
                    }
                });

                var formData = objectGraph,
                    dataArray = [],
                    productInfo = parsedData;

                dataArray.push(formData.contact.phoneNumbers);
                formData.productInfo = productInfo;
                formData.contact.phoneNumbers = dataArray;
                formData.transactionDetails.amount = parseInt($(this.defaultSettings.totalAmount).text());
                
                return JSON.stringify(formData);
            },

            scrollPageToError: function(elem) {
                $('html, body').animate({
                    scrollTop : elem.offset().top - 150
                }, 1000);
            },

            checkOptins: function() {
                var self = this,
                    settings = self.defaultSettings, 
                    $optinSmsBrand = $(settings.optinSmsBrand),
                    $optinSmsAll = $(settings.optinSmsAll);

                //onload disable the sms checkbox
                $optinSmsBrand.attr('disabled', true);
                $optinSmsAll.attr('disabled', true);
                
                $(settings.contactNumber+', '+settings.optinSmsAll).on("keyup change blur", function() {
                    if ($(this).val() !=='') {
                        $optinSmsBrand.attr('disabled', false);
                        $optinSmsAll.attr('disabled', false);
                    } else{
                        $optinSmsBrand.attr('disabled', true);
                        $optinSmsAll.attr('disabled', true);

                        $optinSmsAll.attr('checked', false);
                        $optinSmsBrand.attr('checked', false);
                    }
                });
            },

            optinsTracking: function() {
                var ev = {},
                $optInBrancdOnline = $('#optIn-onlineBrand'),
                $optInAllOnline = $('#optIn-onlineAll'), compName = $('.c-form-submit-btn').parents('form').data('componentname');
                  
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {

                    if (($optInBrancdOnline.is(':checked')) && ($optInAllOnline.is(':checked'))) {
                        ev.eventInfo={
                            'type':ctConstants.trackEvent,
                            'eventAction': ctConstants.Acquisition,
                            'eventLabel' : 'BRAND OPTIN & CORPORATE OPTIN'
                        };
                        ev.category ={'primaryCategory':ctConstants.conversion};
                        digitalData.event.push(ev);

                    } else if ($optInBrancdOnline.is(':checked')) {
                        ev.eventInfo={
                          'type':ctConstants.trackEvent,
                          'eventAction': ctConstants.Acquisition,
                          'eventLabel' : 'BRAND OPTIN'
                        };
                        ev.category ={'primaryCategory':ctConstants.conversion};
                        digitalData.event.push(ev);   

                    } else if ($optInAllOnline.is(':checked')) {
                        ev.eventInfo={
                          'type':ctConstants.trackEvent,
                          'eventAction': ctConstants.Acquisition,
                          'eventLabel' : 'CORPORATE OPTIN'
                        };
                        ev.category ={'primaryCategory':ctConstants.conversion};
                        digitalData.event.push(ev);   

                    } else {
                        ev.eventInfo={
                          'type':ctConstants.trackEvent,
                          'eventAction': ctConstants.suscribeNewsletter,
                          'eventLabel' : $('.form-page h1.c-form-copy-content__headline').text()
                        };
                        ev.category ={'primaryCategory':ctConstants.other};
                        digitalData.event.push(ev);
                    }
                    
                    digitalData.component.push({'componentInfo' :{
					   'name': compName
					}});
                    
                    digitalData.component = [];
                }          
            },

            checkoutSuccessAnalytics: function() {
                var sessionData = eGiftingService.getItem(this.defaultSettings.storageName),
                    parsedData,
                    ev = {};

                if (sessionData !== undefined && sessionData !== '' && sessionData !== null) {
                    parsedData = $.parseJSON(sessionData);
                }

                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    digitalData.product = [];

                    $(parsedData.products).each(function(key, item) {
                        digitalData.product.push({
                           'productInfo' :{
                               'productID': item.productId,
                               'productName': item.title,
                               'price': item.price,
                               'brand': $('.c-order-summary').data('brand'),
                               'quantity': item.quantity
                            },
                            'category':{
                                'primaryCategory':''
                            }
                        });
                    });
                    
                    ev.eventInfo = {
                      'type':ctConstants.purchaseenhanced,
                    };

                    ev.category ={'primaryCategory':ctConstants.conversion};
                    digitalData.event.push(ev);
                }
            },
            
            checkoutOrderAnalytics: function(orderId) {
                var ev = {};

                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    ev.eventInfo = {
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.ordersummary,
                      'eventLabel' : orderId
                    };

                    ev.category ={'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                }
            },

            checkoutErrorAnalytics: function(error) {
                var ev = {};

                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    ev.eventInfo = {
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.inputerror,
                      'eventLabel' : error
                    };

                    ev.category = {'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                }
            },
            
            brainTreeErrorAnalytics: function() {
                var self = this;
                $('.js-egifting-submit-btn').click(function() {
                    self.checkoutValidationErrorAnalytics();
                });
            },
            
            checkoutValidationErrorAnalytics: function() {
                var productData = localStorage.getItem('productCartData');

                if(productData !== undefined && productData !== '' && productData !== null) {

                    var ev = {},
                        options = {},
                        compWrapper = $('.c-order-summary'),
                        parsedProductData = JSON.parse(productData).products;

                    digitalData.component = [];
                    digitalData.product = [];

                    $.each(parsedProductData, function(key, val) {
                        options.compName = compWrapper.data('componentname');
                        options.productID = val.productId;
                        options.productName = val.title;
                        options.price = val.price;
                        options.brand = compWrapper.data('brand');
                        options.quantity = val.quantity;

                        digitalData.component.push({'componentInfo' :{
                           'componentID':options.compName,
                           'name':options.compName
                        }});

                        digitalData.product.push({
                           'productInfo' :{
                               'productID': options.productID,
                               'productName': options.productName,
                               'price': options.price,
                               'brand': options.brand,
                               'quantity': options.quantity
                            }
                        });
                    });

                    var errorlog=[], errorvalue = '';
                    if($('.has-error .help-block').length > 0) {
                        $('.has-error .help-block').each(function(i, value) {
                           errorlog.push($(this).text());
                        });
                    } 
                    
                    setTimeout(function() {
                        if($('.braintree-msg-list').hasClass('has-error') && $('.braintree-msg-list li').length > 0) {
                            $('.braintree-msg-list li').each(function(key, val) {
                                var errMsg = $(this).text();
                                errorlog.push(errMsg.slice(3));
                            });
                        }

                        errorvalue += errorlog.join(" | ");

                        var ev = {};
                        ev.eventInfo={
                          'type':ctConstants.trackEvent,
                          'eventAction': ctConstants.inputerror,
                          'eventLabel' : errorvalue
                        };
                        ev.category ={'primaryCategory':ctConstants.custom};
                        digitalData.event.push(ev);
                    }, 500);
                }
            }
        });
    });

    return CheckoutForm;
});
