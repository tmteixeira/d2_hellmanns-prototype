/*global define*/
define(['eGiftingService'], function() {
    'use strict';

    var eGiftingService = null;

    var OrderSummary = IEA.module('UI.order-summary', function(orderSummary, app, iea) {

        _.extend(orderSummary, {

            defaultSettings: {
                basketWrapper: '.c-order-summary__wrapper',
                cookieName: 'cartSession',
                storageName: 'productCartData',
                template: 'partials/order-summary-list.hbss'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function(options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function() {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function() {
                this.triggerMethod('beforEnable');
               
                var self = this,
                    settings = self.defaultSettings;

                self.componentJson = self.getModelJSON().orderSummary;
                self.storageName = settings.storageName;
                self.parsedData = null;
                self.sessionData = localStorage.getItem(self.storageName);
                
                if (self.sessionData !== undefined && self.sessionData !== '' && self.sessionData !== null) {
                    self.parsedData = $.parseJSON(self.sessionData);
                }
                self.renderOrderSummary();

                this.triggerMethod('enable');            
            },

            renderOrderSummary: function() {
                var self = this,
                    combinedJson = null,
                    template = self.getTemplate('order-summary-list', self.defaultSettings.template),
                    $basketWrapper = $(self.defaultSettings.basketWrapper),
                    cartData = self.parsedData;

                eGiftingService = new IEA.EGifting();
                
                if (self.sessionData !== undefined && self.sessionData !== '' && self.sessionData !== null) {
                    combinedJson = {
                        'orderSummary': {
                            'orderSummary': self.componentJson,
                            'productInfo': cartData
                        }
                    };

                    $basketWrapper.append(template(combinedJson));

                    //update total amount in order summary
                    eGiftingService.calculatePrice(JSON.stringify(cartData));
                }
            }
           


            /**************************PRIVATE METHODS******************************************/

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */

        });
    });

    return OrderSummary;
});