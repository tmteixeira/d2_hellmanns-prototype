/*global define*/

define(['search', 'typeahead'], function() {
    'use strict';

    var SearchInputV2 = IEA.module('UI.search-input-v2', function(searchInputV2, app, iea) {

        var searchService = null;
        
        _.extend(searchInputV2, {
            
            defaultSettings: {
                searchWrapper: '.c-search-input-v2-form__elements',
                searchInput: '.c-search-input-v2-form__textbox.typeahead',
                clearSearch: '.o-btn--reset',
                searchButton: '.js-search-handler',
                searchForm: '.c-search-input-v2-form'
            },

            events: {
                'click .js-search-handler': '_submitForm', // render search listing on search button click
                'submit form': '_submitForm'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                searchService = new IEA.search();
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                var self = this,
                    settings = self.defaultSettings;
                
                self.componentJson = self.getModelJSON();
                self.searchData = self.componentJson.searchInputV2;
                self.searchWrapper = self.$el.find(settings.searchWrapper);
                self.searchInputField = self.$el.find(settings.searchInput);
                self.clearSearch = settings.clearSearch;
                self.searchButton = settings.searchButton;
                self.searchForm = settings.searchForm;
                
                self.searchKeyword = '';

                self._enableSearchMethods();
            },

             /**************************PRIVATE METHODS******************************************/
            
            /**
             * call search  methods
             * @method enableSearchMethods
             * @return 
             */

            _enableSearchMethods: function() {
                // enable the typeahead focuse on page load
                var self = this;
                $(document).on('focus.typeahead.data-api', '[data-provide="typeahead"]', function(e) {
                    var $this = $(this)
                    if ($this.data('typeahead')) return true
                    $this.typeahead($this.data());
                });

                // show suggestions
                if ($(self.searchInputField).parent('.twitter-typeahead').length === 0 && !self.searchData.disableAutoComplete) {
                    self._renderSuggestions();
                }

                // clear search on cross click
                searchService.clearSearchVal(self.clearSearch, self.searchInputField);
            },
            
            // handle search on keyboard handler events
            _keyboardHandler: function(e, obj) {
                e.preventDefault();
                var self = this,
                    $this = $(e.currentTarget),
                    actionURL = '';

                if (obj.length > 0 && !obj.match(/^\s*$/)) {
                    self._updateParams(obj, $(self.searchForm));
                } else {
                    $(self.searchInputField).focus();
                    return false;
                }
            },
            
            // update parameters of search form submit
            _submitForm: function(e) {
                var self = this,
                    _this = $(e.target),
                    searchForm = $(self.searchForm),
                    searchInput = self.searchInputField,
                    submitUrl = searchForm.data('action');

                if (searchForm.attr('action') === '') {
                    e.preventDefault();

                    searchForm.attr('action', submitUrl);

                    if (searchInput.typeahead('val').length > 0 && !searchInput.typeahead('val').match(/^\s*$/)) {
                        self._updateParams(searchInput.typeahead('val'), searchForm);
                    } else {
                        $(searchInput).focus();
                    }
                } else {
                    return true;
                }
            },
            
            /**
             * Auto complete
             * @method autoComplete
             * @return 
             */

            _renderSuggestions: function() {
                // constructs the suggestion engine
                var self = this,
                    options,
                    searchData = self.searchData;

                options = {
                    inputFont: parseInt($(self.searchInputField).css('font-size')),
                    dropDownLabel: (typeof searchData.includeContentType !== undefined) ? searchData.includeContentType : '',
                    searchWrapper: self.searchWrapper,
                    inLabel: searchData.inLabel,
                    searchInputField: self.searchInputField,
                    clearSearch: self.clearSearch,
                    customKeyword: !self.searchData.disableContentTypeFilter,
                    hint: !self.searchData.disableAutoComplete,
                    locale: $('input[name=Locale]').val(),
                    brandName: $('input[name=BrandName]').val(),
                    searchKeyword: self.searchKeyword.toLowerCase(),
                    suggestionsLimit: self.searchData.autoSuggestResultCount,
                    minLengthToTriggerAutoSuggest: self.searchData.minCharToTriggerAutoSuggest
                };

                options.selectCallback = function(options, keywordVal, obj) {
                    self._updateParams(keywordVal, obj.parents(self.searchForm));
                    options.searchKeyword = self.searchKeyword;

                    if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                        self._searchAnalytics();
                    }
                }

                options.keyupCallback = function(options, event, obj) {
                    self._keyboardHandler(event, obj);
                }

                searchService.showSuggestions(options);
            },
            
            _updateParams: function(keywordVal, form) {
                var self = this,
                    dropdownLabel = self.searchData.includeContentType,
                    queryLabel,
                    searchForm = form,
                    contentType = '',
                    contentTypeFlag = false;

                if (dropdownLabel.length) {
                    $(dropdownLabel).each(function(key, value) {
                        if (keywordVal.indexOf(value.contentType) !== -1) {
                            contentType = 'ContentType:'+value.contentType;
                            self.searchKeyword = keywordVal.split(' ' + self.searchData.inLabel + ' ' + value.contentType)[0];
                            return false;
                        } else {
                            self.searchKeyword = keywordVal;
                            contentType = '';
                        }
                    });
                } else {
                    self.searchKeyword = keywordVal;
                    contentType = '';
                }
                $('input[name=q]').val(self.searchKeyword);
                $('input[name=fq]').val(contentType);
                self._searchAnalytics();
                searchForm.submit();
            },

            _searchAnalytics: function() {
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    var ev = {},
                        $searchInput = $(".c-search-input-v2"),
                        compName = $searchInput.data('componentname'),
                        compVariants = $searchInput.data('componentvariant'),
                        compPositions = $searchInput.data('componentposition');
                    
                    digitalData.component.push({
                        'componentInfo' :{
                            'componentID': compName,
                            'name': compName
                        },
                        'attributes': {
                            'position': compPositions,
                            'variants': compVariants
                        }
                    });
                    
                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.siteSearch,
                        'eventLabel': this.searchKeyword
                    };
                    ev.category = { 'primaryCategory': ctConstants.other };
                    digitalData.event.push(ev);
                    digitalData.component = [];
                }
            }
        });
    });

    return SearchInputV2;
});
