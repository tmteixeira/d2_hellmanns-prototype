/*global define*/

define(['accordion'], function(accordion) {
    'use strict';

    var accordion = null;
    
    var Ifaq = IEA.module('UI.ifaq', function(ifaq, app, iea) {

        _.extend(ifaq, {
            
            defaultSettings: {
                template: 'ifaq/partial/ifaq-item.hbss'
            },


            events: {
                'click a.c-expandcollapse__link': '_callExpandCollapse'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));
                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            
            enable: function () {
                this.triggerMethod('beforEnable');            
                
                var self = this;
                    self.ifaqData = this.getModelJSON().ifaq;   
                    self.getDataFromJson();

                this.triggerMethod('enable');
            },
            
            _callExpandCollapse: function(evt, options) {
                var service = new IEA.accordion(); // creates new instance
                service._handleExpandCollapse(evt, options);
            },
            
            getDataFromJson: function() {
                var self = this,
                    options = {};
                    options.queryParams = '';
                    options.servletUrl =  self.ifaqData.serviceUrl;

                $.ajax({
                     url: options.servletUrl,
                     type: 'GET',
                     crossDomain: true,	
                     dataType: 'jsonp',
                     success: function(data){
                        self.renderIfaqList(data);
                    }
                });
            },
            
            renderIfaqList: function(data) {
                var faqData = data.data.faqCollection,                
                    self = this,
                    listing = '';
                $.each(faqData, function(i) {                   
                    var faqDatVal = faqData[i],
                        faqTem = self.getTemplate('ifaq-item', self.defaultSettings.template); 
                        listing += faqTem(faqDatVal);
                        $('.ifaq-list').html(listing);
                });                    
            }
        });
    });

    return Ifaq;
});