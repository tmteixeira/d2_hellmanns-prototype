/*global define*/

define(['TweenMax', 'jquery.hammer', 'commonService', 'socialTabService'], function(TweenMax) {
    'use strict';

    var SocialGalleryTAB = IEA.module('UI.social-gallery-TAB', function(socialGalleryTAB, app, iea) {

        var commonService = null,
        socialTabService = null;

        _.extend(socialGalleryTAB, {

            defaultSettings: {
                socialGalleryList: '.js-social-gallery-TAB__list',
                socialGalleryItem: '.js-social-gallery-TAB__item',
                loadmoreWrapper: '.js-social-gallery-TAB__loadmore',
                loadmore: '.js-btn__seeall',
                preloader: '.o-preloader',
                preloaderContent: '.js-preloader__content',
                template: 'social-gallery-TAB/partial/social-gallery-list.hbss',
                quickPanelWrapper: '.js-social-gallery-TAB__panel',
                thumbnail: '.js-social-gallery-TAB__thumbnail',
                animateSpeed: 1000,
                pageScroll: false,
                pageStartIndex: 1,
                pageNum: 1,
                itemMargin: 30,
                prevButton: '.js-social-gallery__arrow-prev',
                nextButton: '.js-social-gallery__arrow-next',
                closeButton: '.js-btn-social-gallery__close',
                disableHotKey: true
            },

            events: {
                'click .js-btn__seeall' : '_showAllHandler', //show results on see all button click
                'click .js-social-gallery-TAB__thumbnail' : '_displayQuickPanel', // show quick panel
                'click .js-btn-social-gallery__close' : '_closePanel', //hide quick panel
                'click .js-social-gallery-TAB__item.active' : '_closePanelonActiveSlide', //hide quick panel
                'click .js-social-gallery__arrow' : '_navigateToItem' // navigate quick panel through next/prev button
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);

                commonService = new IEA.commonService(); // creates new instance for common service
                socialTabService = new IEA.SocialTabs(); // creates new instance for common service

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                var self = this;
                
                self.settings = self.defaultSettings;
                self.componentJson = self.getModelJSON().socialGalleryTAB;
                self.startIndex = self.settings.pageStartIndex;
                self.elm = self.$el;

                // render social gallery on page load
                self._getJsonData();

                // show/hide show more button on page scroll
                self._handleScroller();

                // reset quick panel on resizing
                socialTabService.resetPanelOnResize(self.elm, self.settings);

                this.triggerMethod('enable'); 
            },


            /**************************PRIVATE METHODS******************************************/          

            // get json data from service
            _getJsonData: function() {
                var self = this,
                    options = {};

                options.queryParams = socialTabService.setQueryParameters(self.componentJson, self.startIndex);

                options.servletUrl = self.componentJson.retrivalQueryParams.requestServletUrl;

                if (options.queryParams !== '') {
                    options.onCompleteCallback = function(data) {
                        self._animateResultsFadeIn(); 
                        self._generateSocialList(data);
                    };

                    commonService.getDataAjaxMethod(options);
                }   
            },

            // get json data from service
            _generateSocialList: function(data) {
                var self = this,
                solrData = $.parseJSON(data),
                queryParamJson = self.componentJson.retrivalQueryParams,
                template = self.getTemplate('social-gallery-list', self.settings.template),
                combinedJson = null,
                numberOfPosts = queryParamJson.numberOfPosts,
                hashTags = '';

                if ($(queryParamJson.hashTags).length > 0) {
                    $(queryParamJson.hashTags).each(function(key, val) {
                        if (key === 0) {
                            hashTags += val.Id;
                        } else {
                            hashTags += '|' +val.Id;
                        }                       
                    });
                }

                combinedJson = {
                    'socialGalleryTAB': self.componentJson,
                    'solrData': solrData,
                    'pageNum': (self.settings.pageNum - 1)*numberOfPosts,
                    'hashTags': hashTags
                }

                setTimeout( function() {
                    $(self.settings.loadmoreWrapper).find(self.settings.preloader).addClass('hidden');
                    $(self.settings.socialGalleryList).append(template(combinedJson));                 
                }, self.settings.animateSpeed); 


                if (solrData.totalResults > numberOfPosts*self.settings.pageNum) {
                    if (self.settings.pageNum === 1) {
                        $(self.settings.loadmore).fadeIn(self.settings.animateSpeed);
                    }
                    setTimeout( function() {
                        self.settings.pageScroll = true; 
                    }, self.settings.animateSpeed);

                } else {
                    $(self.settings.loadmore).fadeOut(self.settings.animateSpeed);
                    self.$el.find(self.settings.loadmoreWrapper).fadeOut(self.settings.animateSpeed);
                    self.settings.pageScroll = false;
                }       
            },

            // event handler for load more click
            _showAllHandler: function(evt) {
                var self = this,
                numberOfPosts = self.componentJson.retrivalQueryParams.numberOfPosts,
                $this = $(evt.target)

                evt.preventDefault();
                self.settings.pageNum++;

                self.startIndex = self.startIndex + numberOfPosts;

                $this.fadeOut(self.settings.animateSpeed);
                self.settings.pageScroll = false;
                $(self.settings.loadmoreWrapper).find(self.settings.preloader).removeClass('hidden');

                // render search listing
                self._getJsonData();                   
            },

            // animate resuls with fade In effect
            _animateResultsFadeIn: function() {
                TweenMax.to($(this.settings.socialGalleryList), 0.8, {autoAlpha: 1, top: 0, delay: 0.2});
                $(this.settings.preloaderContent).fadeOut(this.settings.animateSpeed);
            },

            // animate resuls with fade Out effect
            _animateResultsFadeout: function() {
                TweenMax.to($(this.settings.socialGalleryList), 0.8, {autoAlpha: 0, top: '30px', delay: 0.2});
                $(this.settings.preloaderContent).fadeIn(this.settings.animateSpeed);
            },

            _closePanel: function(evt) {
                evt.preventDefault();
                
                var _this = $(evt.target);

                socialTabService.closePanel(this.elm, this.settings, _this);
            },

            _closePanelonActiveSlide: function(evt) {
                evt.preventDefault();
                
                var _this = $(evt.target);

                socialTabService.closePanel(this.elm, this.settings, _this);
            },

            _displayQuickPanel: function(evt) {
                evt.preventDefault();

                var _this = $(evt.target),
                    self = this,
                    active = 'active',
                    $currentItem = _this.parents(self.settings.socialGalleryItem),
                    currentIndex = $currentItem.data('index'),
                    $currentQuickPanel = $(self.settings.quickPanelWrapper+'[data-index='+currentIndex+']');

                socialTabService.setActiveParams(self.elm, self.settings, parseInt(currentIndex));

                self.elm.find(self.settings.socialGalleryItem).each(function (index, el) {
                    var panelHeight = 0,
                        cols = 3,
                        row = currentIndex - currentIndex % cols;

                    if (index >= row && index <= row + (cols - 1)) {
                        panelHeight = $currentQuickPanel.height();
                    }
                    if (panelHeight !== 0) {
                        $(el).css("margin-bottom", panelHeight+ self.settings.itemMargin*2);
                    }                   
                });

                $currentItem.addClass(active);
                $currentQuickPanel.addClass(active).css('top', $currentItem.position().top + $currentItem.height() + self.settings.itemMargin);

                TweenMax.to(window, 0.5, {scrollTo: {y: $currentItem.offset().top + $currentItem.height()/2}});

                self.settings.disableHotKey = false;

                //keyword keys handler
                this._navigateThroughKeys($currentItem, currentIndex, $(self.settings.socialGalleryItem).length);
            },

            _navigateToItem: function(evt) {
                evt.preventDefault();

                var self = this,
                    _this = $(evt.target),
                    active = 'active',
                    $currentItem = _this.parents(self.settings.quickPanelWrapper),
                    currentIndex = $currentItem.data('index');

                if (_this.data('role') === 'next') {
                    currentIndex = currentIndex + 1;
                } else if (_this.data('role') === 'prev') {
                    currentIndex = currentIndex - 1;
                }
                $(self.settings.socialGalleryItem+'[data-index='+currentIndex+'] '+self.settings.thumbnail).click();
            },

            _navigateThroughKeys: function(item, index, last) {
                var self = this,
                    $close = item.find(self.settings.closeButton);
                
                // Hotkeys for navigation
                $(document).off('keyup').on('keyup', function(e) {
                    if (e.keyCode === 37 && !self.settings.disableHotKey && index > 0) {
                         socialTabService.prevItem(self.settings, index);
                    }
                    if (e.keyCode === 39 && !self.settings.disableHotKey && index < last) {                    
                        socialTabService.nextItem(self.settings, index);
                    }
                    if (e.keyCode === 27) {
                        self._setParams(self.settings.socialGalleryItem, self.settings.quickPanelWrapper, index);
                        TweenMax.to(window, 0.5, {scrollTo: {y: item.offset().top - item.height()/2}});
                    }
                });

                // Touch gestures with hammerjs for navigation
                self.$el.hammer().off('swipeleft').on('swipeleft', function(e) {                   
                    if (!self.settings.disableHotKey) {
                        socialTabService.prevItem(self.settings, index);
                    }
                }).off('swiperight').on('swiperight', function(e) {                 
                    if (!self.settings.disableHotKey) {
                        socialTabService.nextItem(self.settings, index);
                    }
                });
            },

            /**
            @description: handle load more click event
            @method: _handleScroller
            @return: {null}
            **/
            
            _handleScroller: function() {
                var self = this;

                document.addEventListener('scroll', function (event) {
                    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop,
                    $parentElm = (self.$el.parents('.component-wrapper').next('footer').length) ? self.$el.next('footer') : self.$el.parents('.component-wrapper'),
                    $nextElm = (self.$el.next('footer').length) ? self.$el.next('footer') : self.$el.next('div[data-role]'),
                    $scrollElm = (self.$el.parents('.component-wrapper').length) ? $parentElm : $nextElm,
                    pageOffsetBottom = document.body.scrollHeight - $scrollElm.offset().top,
                    scrollOffset = scrollTop + window.innerHeight + pageOffsetBottom - 100;

                    if ((document.body.scrollHeight <= scrollOffset) && (self.settings.pageNum >= 2) && self.settings.pageScroll === true) {
                        self.settings.pageScroll = false;
                        $(self.settings.loadmore).click();
                    }
                });
            }

        });
    });

    return SocialGalleryTAB;
});