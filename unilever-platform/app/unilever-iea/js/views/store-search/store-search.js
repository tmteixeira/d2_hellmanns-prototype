/*global define*/

define(['TweenMax', 'commonService','animService'], function(TweenMax) {
    'use strict';

    var productIndex, varientIndex;
    var StoreSearch = IEA.module('UI.store-search', function(storeSearch, app, iea) {
        var commonService = null,
        animService = null;
        _.extend(storeSearch, {
            modalCl: '.js-modal-etale',
            events: {
                'click .js-btn-shareLoc' : 'shareLoc'
            },
            defaultSettings: {
                template: 'store-search/partial/store-serach-info.hbss',
                isLoadMore: true,
                pageNum: 1
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                var self = this, options = {};
                self.completeData = self.getModelJSON().storeSearch;

                options.catData = self.completeData.categories;
                options.regExp = self.completeData.storeLocator.postCode.regex;
                self.generateSubCat(options);
                self.submitWtbForm(options);
                self.getProductValue();
                animService = new IEA.animService();
                commonService = new IEA.commonService();
                commonService.imageLazyLoadOnPageScroll();
                var throttledLazyLoad = _.throttle(commonService.imageLazyLoadOnPageScroll, 150);
                this.$el.on('click', '.js-store-search__more-link', function (e) {
                    e.preventDefault();
                    self.defaultSettings.isLoadMore = true;
                    self.defaultSettings.pageNum++;
                    self.lazyloadProducts(self.productData, false);

                });

                this.triggerMethod('enable');
            },

            generateSubCat: function(options) {
                var self = this;

                var dataArray=[];
                for(var i = 0; i < options.catData.length; i++){
                    dataArray.push([options.catData[i].name, options.catData[i].subCategory, options.catData[i].pagePath])
                };
                if(self.completeData.hideProductCategoryFlag===undefined){
                    $(document).on('change', '.js-bss-selectItem', function(e){
                        e.preventDefault;
                        var foundFlag = false;
                        $('.c-store-search-step2 ul').empty();
                        self.defaultSettings.pageNum = 1;
                        self.defaultSettings.isLoadMore = true;
                        $('.c-store-search__form-wrapper').hide();
                            $(this).nextAll().remove();

                            var clickedVal = this.value,
                                subCat = '';
                            dataArray.filter(function(v){
                                if(v[0] === clickedVal) {
                                    subCat = v[1];
                                    if(subCat !== undefined) {
                                        for(var i = 0; i < subCat.length; i++){
                                            for(var j = 0; j < dataArray.length; j++){
                                                if(dataArray[j][0] == [subCat[i].name]){
                                                    foundFlag = true;
                                                }
                                            }
                                            if(foundFlag === false){
                                                dataArray.push([subCat[i].name, subCat[i].subCategory, subCat[i].pagePath])
                                            }
                                            foundFlag = false;
                                        }
                                        selectSubCat(clickedVal, subCat);
                                    } else {
                                        var pagePathVal = v[2];
                                        self.getProductData(pagePathVal);
                                    }
                                }
                            });
                    });
                }
                else{
                   self.getProductData(' ');
                }

                function selectSubCat(currentValue, data) {
                    var returnArray=[], options = '', selectStr = $('<select class="c-store-search__select o-dropdown form-control js-bss-selectItem"><option value="select">select</option></select>');
                    for(var i = 0; i < data.length; i++){
                        returnArray.push([data[i].name, data[i].subCategory]);
                        options += '<option value="' +data[i].name+ '">'+data[i].name+'</option>';
                    }
                    var dropList = selectStr.append(options);
                    $('.c-store-search-select').append(dropList);
                }
            },

            getProductData: function(pagePath) {
                var self = this, replaceProductCount=self.completeData.staticText.productListHeading , currentPageUrl = self.completeData.currentPage, dataUrl = self.completeData.productDataPath;

                if(self.completeData.hideProductCategoryFlag === undefined){
                    $.ajax({
                    url: dataUrl,
                    type: 'GET',
                    data: {'cpt' : pagePath, 'cppt' : currentPageUrl},
                    dataType: 'json',
                    contentType: "application/json; charset=ISO-8859-1",
                    success: function(data) {
                        var template, productList;
                        template = self.getTemplate('store-search-info', 'store-search/partial/store-search-info.hbss');
                        self.productData = data.data;
                        var mapObj = {
                            noOfProducts: self.productData.products.length
                        };
                        self.noOfProducts = replaceProductCount.replace(/placeholderForNumberOfProducts/gi,mapObj.noOfProducts);
                        $('.o-store-search-product--header h3').text(self.noOfProducts);
                        $('.o-store-search-product--header').removeClass('hidden');

                        self.lazyloadProducts(self.productData, true);
                    },
                    error: function() {
                    }
                    })
                }
                else{
                     var template, productList;
                        template = self.getTemplate('store-search-info', 'store-search/partial/store-search-info.hbss');
                        self.productData = self.completeData;
                        self.lazyloadProducts(self.productData, true);
                }
            },

            lazyloadProducts: function(productData, onFirstLoad) {

                var self = this,
                    products = productData.products,
                    prod = '',
                    dataLength = productData.products.length,
                    itemToShow = self.completeData.loadMoreCount,
                    $moreClick=this.$el.find('.js-store-search__more-link'),
                    template = self.getTemplate('store-search-info', 'store-search/partial/store-search-info.hbss'),
                    templateStoreSearchFind = self.getTemplate('store-search-find', 'store-search/partial/store-search-find.hbss');


                $(products).each(function(key, val){
                    if(self.defaultSettings.isLoadMore) {
                        var minLevel = (self.defaultSettings.page - 1) * itemToShow,
                            maxLevel = (function(){
                                var level;
                                if( (self.defaultSettings.pageNum * itemToShow) > dataLength) {
                                    level = dataLength;
                                }else {
                                    level = self.defaultSettings.pageNum * itemToShow;
                                }

                                return level;

                            })();
                        if(key < maxLevel) {
                            var tempTpl = '<li class="col-sm-4">' + template(val) + templateStoreSearchFind(self.getModelJSON()) + '</li>';
                            prod += tempTpl;

                        }
                    }
                });

                $('.c-store-search-step2 ul').html(prod);

                if(this.$el.find('.c-store-search-list > li').length === dataLength) {
                    $moreClick.addClass("hidden");
                }else{
                    if($moreClick.hasClass("hidden")) {
                        $moreClick.removeClass("hidden");
                    }
                }
            },


            getProductValue: function() {
                var self = this;
                $(document).on('click', '.js-store-search-list .choose-variant[name="variant"]', function() {
                    var variantUrl = $(this).data('variant-url'),
                        productId = $(this).attr('data-product-id'),
                        productName = $(this).attr('data-product-name');

                    $(this).parents().find('#pvi').val(productId);
                    $(this).parents().find('#c-store-search-form').attr('action', variantUrl);
                    $('.c-store-search__form-wrapper').hide();
                    productIndex = $(this).parents('li').index();

                    var $storeWrapper = $(this).parents('li').find('.c-store-search__form-wrapper');
                    var $prodDetails = $('.c-product-details');
                    var left = -$(this).parents("li").offset().left + parseFloat($(this).parents(".container").css("margin-left")) + parseFloat($(this).parents(".container").css("padding-left"));
                    var top = $storeWrapper.find(".c-product-details").eq(0).height();

                    $storeWrapper.css({left: left + 'px', width: $(this).parents("ul").width(), top: top + "px"});

                    var isMobile = $(window).innerWidth() < 768;
                    var cols = isMobile ? 2 : 3;
                    var pos = $(this).parents("li").index();
                    var row = Math.floor(pos / cols) + 1;
                    var posOfItemInRow = pos % cols;
                    var indexArr = (function(){
                        var arr = [];

                        for (var i = 0; i < cols; i++) {
                            arr.push(((row - 1) * cols) + i);
                        }

                        return arr;
                    })();
                    $prodDetails.each(function(key, val) {
                        if(indexArr.indexOf(key) > -1) {
                            $(this).css({'margin-bottom': $('.c-store-search__form-wrapper').height()});
                        }else {
                            $(this).css({'margin-bottom': '0px'});
                        }
                    });

                    $storeWrapper.show();
                    varientIndex = $(this).parents('li').index();
                    self.buyOnline(productId, productName);
                });
            },

            buyOnline: function(productId,productName) {
                var self = this ,buyOnlineObj = {}, template, buyOnlineBtn, $buyOnline, $buyOnlineBtn;
                buyOnlineObj.shopNow = self.productData.shopNow;
                buyOnlineObj.product = self.productData.products[productIndex];
                template = this.getTemplate('bin-widget', 'partials/bin-widget.hbss');
                buyOnlineBtn = template(buyOnlineObj);
                $buyOnlineBtn = $(buyOnlineBtn);
                $buyOnline = $('.c-buyonline');
                $buyOnline.empty();
                $buyOnline.append(buyOnlineBtn);


                if(buyOnlineObj.shopNow.enabled === 'true') {
                    this.$el.find('.c-storeprefix-text').removeClass('hidden');
                }
                if(self.productData.shopNow.serviceProviderName === 'constantcommerce' || self.productData.shopNow.serviceProviderName === 'click2buy') {
                    $.getScript(self.productData.shopNow.serviceURL);
                }
                this.appendPostMessage(buyOnlineObj);
                this.handleBinButton(buyOnlineObj.product, buyOnlineObj.shopNow);
            },

            appendPostMessage: function(data) {
                var serviceProviderName = data.shopNow.serviceProviderName;

                if(serviceProviderName === 'etale') {
                    if($('script[src*=\'postmessage.js\']').length === 0){
                        var scriptUrl = '/etc/ui/'+ data.brandName +'/clientlibs/core/core/config/postmessage.js',
                         service = document.createElement('script');
                        service.type = 'text/javascript';
                        service.async = true;
                        service.src = scriptUrl;
                        document.querySelector('head').appendChild(service);
                    }
                }
            },

            handleBinButton: function(productData, shopNowData) {
                var smartProductId, self = this;
                $('.js-bin-button').click(function() {
                    self._trackShopNow(this);
                    smartProductId = productData.productsDetail[0].smartProductId;
                    loadsWidget(smartProductId, this,'retailPopup','en');
                });

                $('.js-etale-bin-button').click(function(e) {
                    e.preventDefault();
                    self._trackShopNow(this);
                    self.openModal();
                    $('.js-main-wrapper').removeClass('u-blur-background');
                    $("#iframeId").attr("src", shopNowData.serviceURL+productData.productsDetail[0].smartProductId+"#"+shopNowData.pageurl);

                });

                $('.js-btn-close').click(function(e) {
                    e.preventDefault();
                    self.closeModal();
                });
            },

            openModal: function () {
                animService.openOverlay(this.modalCl);
            },

            closeModal: function () {
                animService.closeOverlay(this.modalCl);
            },

            submitWtbForm: function(options) {
                var self = this;
                $('.js-btn-bss').click(function() {
                    var postalCode = this.$el.parents("form").find('.postalCode').val(),
                        actualURL = '',
                        validZip = self.zipCodeValidation(postalCode, options.regExp);

                    if(validZip === true) {
                        self._storeSearch(postalCode);
                        $('.help-block').addClass('hidden');
                        $('.postalCode').parents('.form-group').removeClass('has-error');
                    } else {
                        $('.postalCode').parents('.form-group').addClass('has-error');
                        $('.help-block').removeClass('hidden');
                        return false;
                    }
                });
            },

            zipCodeValidation: function(postalCode, regExp) {
                var regEx = regExp, pattern = new RegExp(regEx), isValidZip = pattern.test(postalCode);
                return isValidZip;
            },

            shareLoc: function() {
                var self=this;
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position){
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        }
                        reverseGeoCode(pos.lat, pos.lng, function(zip) {
                            $('.postalCode').val(zip);
                        });
                    });

                    var reverseGeoCode = function(lat, lng, callback) {
                        var mapServiceUrl = self.completeData.geoCodeApi,
                            googleService = mapServiceUrl + lat + ',' + lng + '&sensor=false';
                        $.getJSON(googleService, cbFunc);

                        function cbFunc(data) {
                            var addressComponents = data.results[0].address_components,
                            postalCode = '', countryCode = '', type = '';
                            if (addressComponents) {
                                for (var i = 0, ii = addressComponents.length; i < ii; i++) {
                                    if (typeof addressComponents[i].types == "string") {
                                        type = addressComponents[i].types;
                                    }
                                    else if (typeof addressComponents[i].types == "object") {
                                        type = addressComponents[i].types[0];
                                    }

                                    if (type == 'postal_code') {
                                        postalCode = addressComponents[i].long_name;
                                    }
                                }

                                if (typeof callback === 'function') {
                                    callback(postalCode);
                                }
                            }
                        }
                    }
                }
            },
            /**************************PRIVATE METHODS******************************************/

            _storeSearch: function(postalCode){
                var ev = {}, label,
                    label = "StoreSearchSubmitted" + postalCode;

                if(typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    digitalData.component.push({
                        'componentInfo' :{
                            'name': compName
                        }
                    });
                    ev.eventInfo={
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.storesearch,
                      'eventLabel' : label
                    };
                    ev.category ={'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                    digitalData.component = [];
                }
            },
            _trackShopNow: function(self) {
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    var ev = {},
                        obj = $(self).parents("form").find('.c-buyonline'),
                        prodDetails = $(self).parents("li").find(".choose-variant"),
                        productID = prodDetails.data('product-id'),
                        productName = prodDetails.data('product-name'),
                        productCategory = obj.data('product-category'),
                        compName = $('.c-store-search').data('componentname'),
                        productPrice,variant,
                        brandName = obj.data('brandname');

                    digitalData.product = [];
                    digitalData.component = [];

                    digitalData.component.push({'componentInfo' :{
                        'componentID': compName,
                        'name': compName
                    }});

                    digitalData.product.push({
                       'productInfo': {
                           'productID': productID,
                           'productName': productName,
                           'price': productPrice,
                           'brand': brandName,
                           'quantity': 1
                       },
                       'category': {
                            'primaryCategory': productCategory
                       },
                       'attributes': {
                            'productVariants': variant,
                            'listPosition': 1
                       }
                    });

                    ev.eventInfo = {
                      'type': ctConstants.trackEvent,
                      'eventAction': ctConstants.purchase,
                      'eventLabel': 'Online - ' + productName
                    };
                    ev.category = {'primaryCategory':ctConstants.conversion};
                    digitalData.event.push(ev);
                }
            }

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:
            */

        });
    });

    return StoreSearch;
});
