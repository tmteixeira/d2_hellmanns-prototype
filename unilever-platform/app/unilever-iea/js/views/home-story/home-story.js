/*global define*/

define(['TweenMax', 'animService', 'commonService'], function(TweenMax) {
    'use strict';

    var ProductStory = IEA.module('UI.home-story', function(homeStory, app, iea) {

        _.extend(homeStory, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                // functions to be wrirren here

                this.triggerMethod('enable');
            }



            /**************************PRIVATE METHODS******************************************/
        });
    });

    return ProductStory;
});
