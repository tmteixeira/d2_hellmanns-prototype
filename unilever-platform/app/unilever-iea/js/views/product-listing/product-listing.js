/*global define*/

define(['TweenMax', 'handlebars', 'commonService','ratings'], function (TweenMax, handlebars) {
    'use strict';

    var ProductListing = IEA.module('UI.product-listing', function (productListing, app, iea) {

        var commonService = null,
			ratingService = null;

        _.extend(productListing, {

            defaultSettings: {
                loadMore: '.js-product-listing__more-link',
                isLoadMore: false,
                filterVal: '0',
                pageNum: 1
            },
            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param options
             * @return
             */
            initialize: function (options) {

                var self = this;
				ratingService = new IEA.ratings(); 
				
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.data = this.getModelJSON();
                this.$el.html(this.template(this.data));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
             /**
            @description: Show the component
            @method show
            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            @description: Hide the component
            @method hide
            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean
            @return {null}
            **/
            clean: function() {
                this._super();
            },
            data : {},
            dataClone: {},

            enable: function () {
                this.triggerMethod('beforEnable');

                var self = this,
                    options = {},
                    componentJson = self.getModelJSON().productListing;

				commonService = new IEA.commonService();
                             
                if (componentJson.productList.length) {
                    options.ratingObj = componentJson.productList[0].ratings;
                    options.productId = componentJson.productList;
                    options.ratingReview = componentJson.review;
                    options.brandName = componentJson.brandName;
                }

				options.isLoadMore = self.defaultSettings.isLoadMore;
                options.pageNum = self.defaultSettings.pageNum;
                options.loadmore = self.defaultSettings.loadMore;
                options.filterVal = self.defaultSettings.filterVal;
                options.currentProductID = self.defaultSettings.currentProductID;     
               

                // Reload products if the filter has been already set previously
                if ($('[name="filterOne"]')[0] !== undefined && $('[name="filterTwo"]')[0] !== undefined) {

                    if ($('[name="filterOne"]')[0].selectedIndex !== 0) {
                        options.filterOneVal = $('[name="filterOne"]').val();
                    }

                    if( $('[name="filterTwo"]')[0].selectedIndex !== 0) {
                        options.filterTwoVal = $('[name="filterTwo"]').val();
                    }

                    self._generateProductList(self.getModelJSON(), options.filterVal, options.filterOneVal, options.filterTwoVal, options);
                }
                
                else{
					self._generateProductList(self.getModelJSON(), options.filterVal, options.filterOneVal, options.filterTwoVal, options);
				}
                self._resizeDropdowns();
				self._setGrid();
                self.transitionIn();
                self._updateColClass();
				
				ratingService.initRatings(options);

                this.$el.on("click", ".c-product-listing__filter-cta", function () {
                    self.$el.toggleClass("is-open");
                });

                this.$el.on("change", ".c-product-listing__form select", function (e) {
                    e.preventDefault();
                    var panelOffset = $('.product-listing').offset().top - ($('.js-navigation-sticky-wrapper').height() + $('.js-product-listing__overlay').height());

                    options.isLoadMore = false;
                    options.filter = $(this).attr('name');
                    options.filterVal = $(this).val();
                    options.filterOneVal = $('[name="filterOne"]').val();
                    options.filterTwoVal = $('[name="filterTwo"]').val();

                    if(options.filter === 'filterOne') {
                        self._generateFilterOptions(self.getModelJSON(), options);
                    }

                    self._generateProductList(self.getModelJSON(), options.filterVal, options.filterOneVal, options.filterTwoVal, options);
                    TweenMax.to(window, 2, {scrollTo:{y:panelOffset}, ease:Power2.easeOut});

                    self._updateColClass();
					
					self._setGrid();

                    // reload images
                    commonService.imageLazyLoadOnPageScroll(); 
                    var throttledLazyLoad = _.throttle(commonService.imageLazyLoadOnPageScroll, 150);

                    // Init rating 
                    if (typeof ratingReview !== 'undefined' && typeof ratingReview.widget !== 'undefined') {
                        ratingService._kritiqueRatings(ratingReview);
                    }
                    
                    $(window).on('scroll.lazyLoad', throttledLazyLoad); // Window scroll lazy load
                });

                this.$el.on('click', '.js-product-listing__more-link', function (e) {
                    e.preventDefault();
                    var title = $(this).attr('title'),
                        panelOffset =$('.product-listing').offset().top - ($('.js-navigation-sticky-wrapper').height() + $('.js-product-listing__overlay').height()) + $('.product-listing').innerHeight();

                    options.isLoadMore = true;
                    options.filterOneVal = $('[name="filterOne"]').val();
                    options.filterTwoVal = $('[name="filterTwo"]').val();


                    TweenMax.to(window, 2, {scrollTo:{y:panelOffset}, ease:Power2.easeOut});

                    self._generateProductList(self.getModelJSON(), options.filterVal, options.filterOneVal, options.filterTwoVal, options);
                    self._updateColClass();

                    self._setGrid();

                    // reload images
                    commonService.imageLazyLoadOnPageScroll(); 
                    var throttledLazyLoad = _.throttle(commonService.imageLazyLoadOnPageScroll, 150);

                    // Init rating 
                    if (typeof ratingReview !== 'undefined' && typeof ratingReview.widget !== 'undefined') {
                        ratingService._kritiqueRatings(ratingReview);
                    }
					
					$(window).on('scroll.lazyLoad', throttledLazyLoad); // Window scroll lazy load
                });

                this.$el.on("click", ".quick-view-btn", function (e) {
                    e.preventDefault();
                    self._setActiveItem($(this).parents(".c-product-listing__product-wrap"), $(this).attr('class').split(' ')[0]);
                });

                this.$el.on("click", ".c-product-listing__quick-close", function () {
                    self._closeQuickView();
                });
                
                $(window).on("orientationchange", function () {
                    this._setActiveItem($(".js-product-listing__product-active"), $('.quick-view-panel-active').length > 0 ? $('.quick-view-panel-active').attr('class').split(' ')[0].slice(0, -6) : '', true);
                });

                this.$el.on('change', '.js-product-listing__select', function () {
                    self._updateDropdown($(this), self.getModelJSON());
                });

                // Add keyboard support
                $(document).keydown(function(e) {
                    var $item = null;

                    switch(e.which) {
                        case 37: // left
                            $item = $('.js-product-listing__product-active').prevAll('.c-product-listing__product-wrap').first();
                        break;

                        case 39: // right
                            $item = $('.js-product-listing__product-active').nextAll('.c-product-listing__product-wrap').first();
                        break;

                        default: return; // exit this handler for other keys
                    }
                    e.preventDefault(); // prevent the default action (scroll / move caret)

                    if ($item !== null) {
                        self._setActiveItem($item, $('.quick-view-panel-active').length > 0 ? $('.quick-view-panel-active').attr('class').split(' ')[0].slice(0, -6) : '');
                    }
                });

                setTimeout(function(){
                    $('.c-product-listing__product-item .aggregateRating').click(function(e) {
                      e.stopPropagation();
                      var pdpUrl=$(this).parents('.o-rating').siblings('.c-product-listing__title').attr('href');
                        if(pdpUrl!=''){
                         window.location.href=pdpUrl+'#reviews';
                        }
                    });
                },3000);

                // Set Nutrition Facts tooltip
                //commonService.tooltipEvent('.js-btn__info', '.c-product-listing__info', 'div[itemprop="itemListElement"]');
                //commonService.switchContentWithTabs('.c-product-listing__info--nav__link', '.c-product-listing__info-item');
                self._switchProductInfoTabs('.c-product-listing__info--nav__link', '.c-product-listing__info-item');
                
                if($('.c-product-listing__container').find('.c-product-overview__awards').length > 0) {
                    self._dateformat();
                }   
				
				// reload images
				commonService.imageLazyLoadOnPageScroll();
				var throttledLazyLoad = _.throttle(commonService.imageLazyLoadOnPageScroll, 150);
				$(window).on('scroll.lazyLoad', throttledLazyLoad); // Window scroll lazy load

                this.triggerMethod('enable');
            },

             /**
            @description: creates product list item
            @method _searchView
            @return {null}
            **/
            _dateformat: function() {
                $('.c-award-date-range').each(function(index){
                    var daterange = $(this).html().split(' - '),
                    newdaterange = commonService.getSubstring(daterange[0],0,10) +" - "+ commonService.getSubstring(daterange[1],0,10);
                    $(this).html(newdaterange);
                });
            },
            _generateProductList: function (obj, drpdnVal, drpdnValOne, drpdnValTwo, options) {

                var objData = obj,
                    onFirstLoad = false,
                    filterData = objData.productListing.productList,
                    reviewObj = objData.productListing.review,
                    self = this,
                    listing = '',
                    dataLength,
                    itemsToShow,
                    template = self.getTemplate('product-listing-products', 'partials/product-listing-products.hbss'),
                    listingFilter = '.c-product-listing__filter-input',
                    lengthIndex = 0;


                dataLength = $(filterData).length;
                itemsToShow = objData.productListing.displayCount;

                $(filterData).each(function(key) {
                    var args = []
                    args[0] = reviewObj;
                    var mergeReviewData = $(filterData)[key];
                    mergeReviewData.args = args;
                    var listData = mergeReviewData;
                    if ((!drpdnValOne || drpdnValOne === "0") && !drpdnValTwo) {
                        onFirstLoad = true;
                        if (options.isLoadMore === false) {
                            if (key < itemsToShow) {
                                listing += template(listData);
                            }
                        } else if (options.isLoadMore === true) {
                            listing += template(listData);
                        }
                        dataLength = $(filterData).length;
                    } else if(drpdnValOne !== "0" && !drpdnValTwo) {
                        $(listData.productTag).each(function(index) {

                            var filterID = $(listData.productTag)[index];
                            if (filterID.id.indexOf(drpdnValOne) !== -1) {
                                if (options.isLoadMore === false) {
                                    if (lengthIndex < itemsToShow) {
                                        listing += template(listData);
                                    }
                                } else if (options.isLoadMore === true) {
                                    listing += template(listData);
                                }

                                lengthIndex++;
                                return false;
                            }
                        });


                        dataLength = lengthIndex;
                    } else if((!drpdnValOne || drpdnValOne === "0") && drpdnValTwo) {
                         $(listData.productTag).each(function(index) {
                            var filterID = $(listData.productTag)[index];
                            if (filterID.id.indexOf(drpdnValTwo) !== -1) {
                                if (options.isLoadMore === false) {
                                    if (lengthIndex < itemsToShow) {
                                        listing += template(listData);
                                    }
                                } else if (options.isLoadMore === true) {
                                    listing += template(listData);
                                }
                                lengthIndex++;
                                return false;
                            }
                        });
                        dataLength = lengthIndex;
                    } else if(drpdnValOne !== "0" && drpdnValTwo) {
                        var filterArrayString = '';
                        $(listData.productTag).each(function(index) {
                            var filterID = $(listData.productTag)[index];

                            filterArrayString += filterID.id + ",";
                        });

                        if ((filterArrayString.indexOf(drpdnValOne) !== -1) && (filterArrayString.indexOf(drpdnValTwo) !== -1)) {
                            if (options.isLoadMore === false) {
                                if (lengthIndex < itemsToShow) {
                                    listing += template(listData);
                                }
                            } else if (options.isLoadMore === true) {
                                listing += template(listData);
                            }
                            lengthIndex++;
                        }
                        dataLength = lengthIndex;
                    }
                });

                if ((dataLength <= itemsToShow) || options.isLoadMore === true) {
                    $(options.loadmore).fadeOut(500);
                } else {
                    $(options.loadmore).fadeIn(500);
                }

                // Get the number of products that were already loaded
                var numAlreadyLoaded = $('.c-product-listing__product-wrap').length;

                $('.c-product-listing__product-list').html(listing);
                $('.c-product-listing__btn').text(objData.productListing.shopNow.ctaCopy);
                $('.js-filter-count').text(dataLength);

                // Display the products that were already loaded
                 if (options.isLoadMore === true) {
                    $('.c-product-listing__product-wrap:lt('+numAlreadyLoaded+')').css({'opacity': '1'});
                    $('.c-product-listing__product-wrap:gt('+(numAlreadyLoaded-1)+')').addClass('js-product-listing__load-more');
                }
                
                self._switchProductInfoTabs('.c-product-listing__info--nav__link', '.c-product-listing__info-item');

                //lazy load images
                self.app.trigger('image:lazyload', self);

                //complete page load animation for products
                self.$el.addClass("js-product-listing__loaded");

                self.transitionIn(true);

                //show product filters
                $('.js-product-listing__overlay').removeClass('u-hide').addClass('open');

                //resize product filters dropdown
                self._resizeDropdowns(true);
                ratingService.initRatings(options);
            },


            /**
             @description: creates drop down options
             @method _generateFilterOptions
             @return {null}
             **/
            _generateFilterOptions: function(obj, options) {
                var self = this,
                    objData = obj,
                    dropDownJSON = new Object(),
                    filterData = objData.productListing.productList,
                    filterDropDowns = objData.productListing.masterfilters,
                    filter = options.filter,
                    filterOne = filterDropDowns.filterOne,
                    filterTwo = filterDropDowns.filterTwo,
                    drpdnValOne = options.filterOneVal,
                    drpdnValTwo = options.filterTwoVal,
                    filterTwoOptions = [],
                    filterID = "",
                    template = self.getTemplate('partials/product-listing-filter-option', 'partials/product-listing-filter-option.hbss');

                $(filterData).each(function(key) {
                    var listData = $(filterData)[key];

                    $(listData.productTag).each(function(index) {
                        filterID = $(listData.productTag)[index];

                        if (filterID.id === drpdnValOne || drpdnValOne === '0') {
                            _.each($(filterData)[key].productTag, function(pt) {
                                _.each(filterTwo.list, function(f2List) {
                                    if (f2List.id === pt.id) {
                                        filterTwoOptions.push(pt.id);
                                    }
                                });
                            });
                        }
                    });
                });

                dropDownJSON.defaultOption = filterTwo.defaultOption;
                dropDownJSON.list = [];
                filterTwoOptions = _.uniq(filterTwoOptions);

                _.each(filterTwo.list, function(value, key) {
                    _.each (filterTwoOptions, function(value2, key2) {

                        if ( value.id === value2 ) {
                            dropDownJSON.list.push(value);

                            options.filterOneVal = drpdnValOne !== '' ? drpdnValOne : '0';
                        }
                    });
                });

                $('[name="filterTwo"]').html(template(dropDownJSON));

                if ( drpdnValTwo !== '') {
                    if (!_.contains(filterTwoOptions, drpdnValTwo)) {
                        options.filterTwoVal = '';
                        $('[name="filterTwo"]').val('')
                    } else {
                        options.filterTwoVal = drpdnValTwo;
                        $('[name="filterTwo"]').val(drpdnValTwo)
                    }
                }
            },


            _updateColClass: function() {
                var _this = this,
                gridCl = '.c-product-listing__product-wrap',
                gridObj = $(gridCl),
                totalRecord = gridObj.length,
                lastItem = totalRecord - 1,
                gridCount = commonService.isMobile() ? 2 : 3,
                offsetRow = parseInt((totalRecord)/gridCount),
                offsetCol = totalRecord%gridCount,
                offsetClassCol = offsetRow*gridCount;

                if (totalRecord < gridCount) {
                    offsetRow = totalRecord;
                }
                if (offsetCol === 1) {
                    gridObj.eq(offsetClassCol).addClass(commonService.isMobile() ? 'col-xs-offset-3' : 'col-sm-offset-4');
                } else if (offsetCol === 2) {
                    gridObj.eq(offsetClassCol).addClass('col-sm-offset-2');
                }

            },

            onWindowResized: function () {
                this._setActiveItem($(".js-product-listing__product-active"), $('.quick-view-panel-active').length > 0 ? $('.quick-view-panel-active').attr('class').split(' ')[0].slice(0, -6) : '', true);
            },


            /**************************PRIVATE METHODS******************************************/

            _updateDropdown: function (el, obj) {
                var data = el.find(":selected"),
                    currentProductIndex, 
                    currentProductImage, 
                    adaptiveImage,
                    $activePanel = '.quick-view-panel-active',
                    $panelImage = '.c-product-listing__bottom';
                    
                currentProductIndex = data.index();

                currentProductImage = obj.productListing.productList[currentProductIndex].productsDetail[currentProductIndex].images[0];
                adaptiveImage = handlebars.helpers.adaptiveRetina(currentProductImage.path, currentProductImage.extension, currentProductImage.altImage, currentProductImage.title, currentProductImage.fileName, currentProductImage.url, "false", "320,320,320,320,310,310,220,220", "false");
                
               el.parents($activePanel).find($panelImage).html('<a class="c-product-listing__image-wrap" href="">' + adaptiveImage + '</a>');  

                el.prev().find('.js-dropdown__price').html(data.attr('value'));
                el.prev().find('.js-dropdown__unit').html(data.attr('data-unit'));

                if(data.attr('value')) {
                    el.prev().find('.js-dropdown__retail').html(data.attr('data-retail'));
                } else {
                    el.prev().find('.js-dropdown__retail').html('');
                }
            },
			
            _closeQuickView: function () {
                var isMobile = $(window).width() < 768;

                //var cols = isMobile ? 2 : 3;
                var panelHeight = isMobile ? 330 : 450;
                var activeItem = $(".js-product-listing__product-active");
                var panel = activeItem.find("+ .c-product-listing__quick-panel-wrap");
                setTimeout(function () {
                    activeItem.removeClass("js-product-listing__product-active");
                }, 500);

                TweenMax.fromTo(activeItem.find(".c-product-listing__product-wrap-arrow"), 1,
                    {opacity: 1}, {opacity: 0});
                TweenMax.fromTo(panel, 1, {height: panelHeight, opacity: 1}, {height: 0, opacity: 0});
                TweenMax.to(this.$el.find(".c-product-listing__product-wrap"), 1, {'margin-bottom': 0});


            },
            
            _rowHeights: [],

            _setGrid: function () {
                var self = this;
                var isMobile = $(window).width() < 768;
                var cols = isMobile ? 2 : 3;
                var list = this.$el.find(".c-product-listing__product-wrap");
                this._rowHeights = [];

                list.css("height", "auto");

                list.each(function (index, el) {

                    var row = (index - index % cols) / cols;

                    if (index % cols === 0) {
                        self._rowHeights[row] = 0;
                    }


                    if ($(el).height() > self._rowHeights[row]) {
                        self._rowHeights[row] = $(el).height();
                    }
					$(el).css("height", self._rowHeights[row]);
                        });
            },

            _setActiveItem: function (item, btnClass, stopScroll) {
                this._setGrid();
                if (item.length === 0 ) {
                    return null;
                }
                var self = this;
                var isMobile = $(window).innerWidth() < 768;
                var cols = isMobile ? 2 : 3;
                var panelHeight = isMobile ? 330 : 450;
                var list = this.$el.find(".c-product-listing__product-wrap");
                var pos = list.index(item);
                var row = pos - pos % cols;
                var prevPanel = this.$el.find(".js-product-listing__product-active");

                if(stopScroll || item.hasClass("js-product-listing__product-active")) {
                    prevPanel.removeClass("js-product-listing__product-active");
                    prevPanel.find(".c-product-listing__product-wrap-arrow").css({"opacity": 0});
                }else {
                    setTimeout(function () {
                        prevPanel.removeClass("js-product-listing__product-active");
                        prevPanel.find(".c-product-listing__product-wrap-arrow").css({"opacity": 0});
                    }, 100);
                }



                setTimeout(function () {


                    list.each(function (index, el) {
                        var d = 0;
                        if (index >= row && index <= row + (cols - 1)) {
                            d = panelHeight;
                        }
                        if (prevPanel.length === 0) {
                            TweenMax.fromTo($(el), 1, {'margin-bottom': 0}, {'margin-bottom': d});
                        } else {
                            $(el).css("margin-bottom", d);
                        }
                    });

                    item.addClass("js-product-listing__product-active");
                    item.next().find('.quick-view-panel-active').removeClass('quick-view-panel-active');
                    
                    item.next().find('.'+ btnClass + '-panel').addClass('quick-view-panel-active');
                    if((item.next().find('.'+ btnClass + '-panel a.is-active').length == 0)) {
                        item.next().find('.'+ btnClass + '-panel .c-product-listing__info--nav__list li:first a').addClass('is-active');
                    }
                    item.next().find('.'+ btnClass + '-panel .c-product-listing__info-item:not(:first)').addClass('o-hidden');
                    var panel = item.find("+ .c-product-listing__quick-panel-wrap");

                    panel.css({
                        "top": item.position().top + item.height(),
                        "width": $(window).innerWidth(),
                        "left": -0.5 * ($(window).innerWidth() - self.$el.find(".c-product-listing__container").width()) + 10
                    });

                    if (isMobile) {
                        panelHeight = panel.find(".container").height() + 70;
                    }
                    if (prevPanel.length === 0) {

                        TweenMax.fromTo(panel, 1, {height: 0, opacity: 0}, {height: panelHeight, opacity: 1});
                        TweenMax.fromTo(item.find(".c-product-listing__product-wrap-arrow"), 1,
                            {opacity: 0}, {opacity: 1});

                    } else {
                        panel.css({height: panelHeight, opacity: 1});
                        item.find(".c-product-listing__product-wrap-arrow").css({"opacity": 1});

                    }

                    TweenMax.fromTo(panel.find(".c-product-listing__image-wrap"), 1, {top: 50, opacity: 0}, {top: 0, opacity: 1});
                    //TweenMax.fromTo(panel.find(".c-product-listing__image-wrap"), 1, {opacity:0, top:50}, {opacity:1, top:0});
                }, stopScroll !== true && prevPanel.length === 0 ? 500 : 0);

                if (stopScroll !== true) {
                    TweenMax.to(window, 0.5, {scrollTo: {y: item.offset().top + item.height() - (isMobile ? 360 : 300)}});

                }
            },
            
            _switchProductInfoTabs: function(selector, wrapper) {
                var speed = 700,
                    self = this,
                    activeCl = 'is-active',
                    hiddenCl = 'o-hidden';

                $(selector).click(function(e) {
                    e.preventDefault();
                    var $this = $(this),
                        selectorId = $this.attr('href');
                    
                    if ($this.hasClass(activeCl)) {
                        e.preventDefault();
                    } else {
                        $this.parent().siblings().find('a').removeClass(activeCl);
                        $this.toggleClass(activeCl);
                        $(this).parents('.c-product-listing__info').find(wrapper).slideUp(speed).addClass(hiddenCl);
                        $(selectorId).slideDown(speed).removeClass(hiddenCl);
                    }
                });
            },

            _resizeDropdowns: function (dropdownSelected) {
                // Resize dropdowns based on selected option width. needs inline style as style not applied so widths not calculated correctly.
                $('body').append('<div id="width-tmp"></div>');
				var el = $('#width-tmp');

                $('select.o-filter-dropdown').each(function () {
                    var calculatedWidth,
                        selectedTextWidth,
                        selectedTextLength = $(this).find(':selected').text().length;

                    el.html($(this).find(':selected').text());

                    selectedTextWidth = el.width();

                    calculatedWidth = (dropdownSelected) ? Math.ceil(selectedTextWidth / selectedTextLength) + 5 : Math.ceil(selectedTextWidth / selectedTextLength) *2.5


                    $(this).width(el.width() + calculatedWidth); // 30 : the size of the down arrow of the select box
                });

                el.remove();

            },

            transitionIn: function (isLoadMore) {
                // On scroll reaching the articles' list
                var self = this;
                var articleOffset = self.$el.first().offset().top,
                    windowHeight = $(window).height();

                TweenMax.to('.product-listing', 1 ,{alpha:1,delay:1.2});

                if (isLoadMore) {
                    self.staggerIn(self);
                }
                else {
                    if(commonService.isScrollAnimationSupported()){
                        $(document).on('scroll', function (e) {
                            articleOffset = self.$el.first().offset().top;
                            var currentOffset = $(window).scrollTop();
                            if (currentOffset > articleOffset - (windowHeight / 1.5)) {
                                self.staggerIn(self);
                                $(document).off('scroll', function (e) {});
                            }
                        });
                        if(self.$el.index() <= 1){
                            setTimeout(function(){
                                self.staggerIn(self);
                            },2000);
                        }
                    }else{
                        self.staggerIn(self);
                    }
                }
            },
            staggerIn:function(self){
                TweenMax.staggerTo(self.$el.find('.js-product-listing__load-more'),1,{alpha:1},0.2);
                TweenMax.staggerTo(self.$el.find('.c-product-listing__product-wrap'),1,{alpha:1},0.2);

            }            

        });
    });
    return ProductListing;
});