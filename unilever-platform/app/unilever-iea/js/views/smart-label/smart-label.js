/*global define*/

define(['accordion'], function(accordion) {
    'use strict';
    accordion = null;
    var SmartLabel = IEA.module('UI.smart-label', function(smartLabel, app, iea) {

        _.extend(smartLabel, {

             defaultSettings: {
                headerContainer: '.c-expandcollapse__header',
                speed: 400
            },

            events: {
                'click a.c-expandcollapse__link': '_callExpandCollapse'
                
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));
                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this._init();
            },
            /**************************PRIVATE METHODS******************************************/    
            //method to initialized data on load      
            _init:function() {
                var self = this,
                    completeData = this.getModelJSON().smartLabel,
                    locale = completeData.market,
                    labelInsightId="",
                    baseUrl;
              
                    if(completeData.product.productsDetail.length === 1) {
                        labelInsightId = completeData.product.productsDetail[0].labelInsightId;
                        baseUrl = completeData.baseUrl + labelInsightId + '?locale='+locale;
                        $('#js-smart-label__product').on('click',function() {
                            if(labelInsightId !== "") {
                                self.redirectToUrl(baseUrl);
                            }                            
                        });
                    } else {
                        labelInsightId = completeData.product.productsDetail[0].labelInsightId;
                        baseUrl = completeData.baseUrl + labelInsightId + '?locale='+locale;
                        $('#js-smart-label__select').change(function() {
                            labelInsightId = $(this).find(':selected').attr('data-sku-id');
                            baseUrl = completeData.baseUrl + labelInsightId + '?locale='+locale;
                        });                        
                    }
           
                $('.o-btn-smart-label').click(function() {
                   if(labelInsightId !== "") {
                            self.redirectToUrl(baseUrl);
                        }
                });
            },
            // method handles expand collapse of the accordion
            _callExpandCollapse: function(evt, options) {
                var service = new IEA.accordion(); // creates new instance
                service._handleExpandCollapse(evt, options);
            },
            redirectToUrl: function(url) {
                var openNewWindow = this.getModelJSON().smartLabel.openInNewWindow;
                (openNewWindow)?(window.open(url,'_blank')):(window.location.href = url);
            },
        });
    });
    return SmartLabel;
});
