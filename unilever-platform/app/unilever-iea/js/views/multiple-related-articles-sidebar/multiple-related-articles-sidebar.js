/*global define*/

/*global define*/

define(function () {
    'use strict';

    var MultipleRelatedArticlesSidebar = IEA.module('UI.multiple-related-articles-sidebar', function (multipleRelatedArticlesSidebar, app, iea) {

            _.extend(multipleRelatedArticlesSidebar, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */

            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * Display the hidden elements when clicking on the load more button
             */
            loadMore: function () {

                var $load_more_btn = $('.js-load-more'),
                    $articles = $('.c-multiple-related-articles__articles'),
                    $container = $('.o-multiple-related-articles'),
                    moreLabel = this.getModelJSON().multipleRelatedArticlesSidebar.loadMoreButtonText;
                    //lessLabel = this.getModelJSON().multipleRelatedArticlesSidebar.moreLink.lessLabel;


                $load_more_btn.on('click', function (e) {
                    e.preventDefault();
                    if ($load_more_btn.hasClass('js-load-less')) {

                        TweenMax.fromTo($articles, 1, {height: '+=0'}, {height: '806'});
                        $load_more_btn.text(moreLabel).removeClass('js-load-less');
                    } else {
                        $container.addClass('is-expanded');
                        $('.c-multiple-related-articles__content-wrap', $articles).fadeIn(1000);
                        TweenMax.set($articles, {height: 'auto'});
                        TweenMax.from($articles, 1, {height: 'auto'});
                        $load_more_btn.parent().removeClass('visible-xs').hide();
                    }
                });
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();

                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                var self = this;
                
                //setTimeout(function () {
                    $('.multiple-related-articles-sidebar').clone().appendTo('.js-multiple-related-placeholder');
                    $('.js-multiple-related-placeholder').css({'opacity':1});
               //}, 200);
                this.loadMore();

                this.triggerMethod('enable'); 
            }

            /**************************PRIVATE METHODS******************************************/

            /**
             component private functions are written with '_' prefix to diffrentiate between the public methods
             example:

             _showContent: function (argument) {
                    // body...
                }

             */

        });
    });

    return MultipleRelatedArticlesSidebar;
});