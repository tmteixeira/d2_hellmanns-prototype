/*global define*/

define(['TweenMax', 'jquery.hammer', 'commonService', 'videoPlayerServiceV2'], function(TweenMax) {
    'use strict';

     var MediaGalleryV2 = IEA.module('UI.media-gallery-v2', function(mediaGalleryV2, app, iea) {

        var videoPlayerServiceV2 = null,
            commonService = null,
            videoplayerService = null;

        _.extend(mediaGalleryV2, {

            defaultSettings: {
                mediaGalleryV2Content: '.js-media-gallery-v2__content',
                mediaGalleryV2PlaylistContent: '.js-media-gallery-v2__playlist-content',
                mediaGalleryV2ChannelContent: '.js-media-gallery-v2__channel-content',
                mediaGalleryV2List: '.js-media-gallery-v2__list',
                mediaGalleryV2Item: '.js-media-gallery-v2__item',
                loadmoreWrapper: '.js-media-gallery-v2__loadmore',
                loadmore: '.js-media-gallery-v2-btn__seeall',
                quickPanelWrapper: '.js-media-gallery-v2__panel',
                thumbnail: '.js-media-gallery-v2__thumbnail',
                animateSpeed: 1000,
                itemMargin: 30,
                showOnLoad: 9,
                prevButton: '.js-media-gallery-v2__arrow-prev',
                nextButton: '.js-media-gallery-v2__arrow-next',
                closeButton: '.js-media-gallery-v2__btn-close',
                videoListTemplate: 'media-gallery-v2/partial/media-gallery-v2-videoList.hbss',
                videoWrapper: '.js-video-player',
                videoHandler: '.js-player-handler',
                isVideoOpen: 'is-video-player__open',
                videoButton: '.js-btn-video',
                multipleVideoInstances: false
            },

            events: {
                //show results on see all button click
                'click .js-media-gallery-v2-btn__seeall' : '_showAllHandler',
                // show quick panel
                'click .js-media-gallery-v2__thumbnail' : '_displayQuickPanel',
                //hide quick panel
                'click .js-media-gallery-v2__btn-close' : '_closePanel',
                //hide quick panel
                'click .js-media-gallery-v2__item.active' : '_closePanelonActiveSlide',
                // navigate quick panel through next/prev button
                'click .js-media-gallery-v2__arrow' : '_navigateToItem'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
                
                var options = {},
                    self = this;                
                
                options = {
                    selector: self.defaultSettings.videoHandler,
                    videoWrapper : self.defaultSettings.videoWrapper,
                    view: self,
                    previewImage: '.js-preview-image'
                };

                commonService = new IEA.commonService(); // creates new instance for common service
                videoPlayerServiceV2 = new IEA.videoPlayerServiceV2();
                videoPlayerServiceV2.initVideoPlayer(options);

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                var self = this,
                    videoApiOptions = {};

                self.settings = self.defaultSettings;
                self.componentJson = self.getModelJSON().mediaGalleryV2;
                self.elm = self.$el;

                self._showRequired();

                if($(self.settings.mediaGalleryV2PlaylistContent).length > 0 || $(self.settings.mediaGalleryV2ChannelContent).length > 0) {
                    videoApiOptions.apiKey = self.componentJson.apiKey;

                    if(self.$el.find(self.settings.mediaGalleryV2PlaylistContent).length > 0) {
                        videoApiOptions.id = self.componentJson.media.playlistId;
                        videoApiOptions.appendTo = self.settings.mediaGalleryV2PlaylistContent;
                        videoApiOptions.youtubeApiUrl = "https://www.googleapis.com/youtube/v3/playlistItems";

                        self._getJsonData(videoApiOptions);
                    } else if(self.$el.find(self.settings.mediaGalleryV2ChannelContent).length > 0) {
                        videoApiOptions.id = self.componentJson.media.channelId;
                        videoApiOptions.appendTo = self.settings.mediaGalleryV2ChannelContent;
                        videoApiOptions.youtubeApiUrl = "https://www.googleapis.com/youtube/v3/channels";

                        self._getChannelUploadId(videoApiOptions);
                    }
                }

                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:
            */

            // get channel upload id through youtube api v3
            _getChannelUploadId: function(videoApiOptions) {
                var self = this,
                    apiKey = videoApiOptions.apiKey,
                    channelId = videoApiOptions.id,
                    apiUrl = videoApiOptions.youtubeApiUrl,
                    channelUploadId;

                $.get(
                    apiUrl, {
                        part: 'contentDetails',
                        id: channelId,
                        key: apiKey
                    },
                    function (channelData) {
                        $.each(channelData.items, function(key, val) {
                            channelUploadId = val.contentDetails.relatedPlaylists.uploads;
                            videoApiOptions.id = channelUploadId;
                            videoApiOptions.youtubeApiUrl = "https://www.googleapis.com/youtube/v3/playlistItems";
                            self._getJsonData(videoApiOptions);
                        });
                    }
                );
            },

            // get json data using playlist id through youtube api v3
            _getJsonData: function(videoApiOptions) {
                var self = this,
                    apiKey = videoApiOptions.apiKey,
                    id = videoApiOptions.id,
                    apiUrl = videoApiOptions.youtubeApiUrl,
                    combinedJsonData = {};

                $.get(
                    apiUrl, {
                        part: 'snippet',
                        playlistId: id,
                        maxResults: self.componentJson.media.limitResultsTo,
                        key: apiKey
                    },
                    function (videoListData) {
                        combinedJsonData = {
                            'mediaGalleryV2': self.componentJson,
                            'videoListData': videoListData,
                        }
                        if(videoListData.items.length > 0) {
                            self._generatePlayListContent(combinedJsonData, videoApiOptions);
                        }
                    }
                );
            },

            // generate playlist html content using playlist partial and json data
            _generatePlayListContent: function(combinedJsonData, videoApiOptions) {
                var self = this,
                    template = self.getTemplate('media-gallery-v2-videoList', self.settings.videoListTemplate),
                    mediaHtml = template(combinedJsonData);

                $(videoApiOptions.appendTo).append(mediaHtml);
                self._showRequired();
            },

            // Set video player height inside panel
            _setPlayerHeight: function(evt) {
                var $this = $(evt.target),
                    self = this,
                    $panel = $this.parents(self.settings.quickPanelWrapper),
                    panelHeight = $panel.height();

                $panel.find('.c-media-gallery-v2__panel-videowrap').height(panelHeight - 2 * self.settings.itemMargin);
            },

            // close panel handler for active item click plus close icon click
            _closePanelHandler: function(elm, settings, obj) {
                var $this = obj,
                    self = this,
                    $currentQuickPanel = $this.parents(settings.mediaGalleryV2List).find(settings.quickPanelWrapper),
                    currentIndex = $currentQuickPanel.data('index'),
                    $currentItem = $(settings.mediaGalleryV2Item+'[data-index='+currentIndex+']');

                $currentQuickPanel.find('.c-media-gallery-v2__panel-imgwrap').show();
                $currentQuickPanel.find('.c-media-gallery-v2__panel-videowrap').hide();

                self._setActiveParams(elm, settings, currentIndex);

            },

            // close panel on close icon click
            _closePanel: function(evt) {
                evt.preventDefault();

                var $this = $(evt.target),
                    self = this;

                self._closePanelHandler(this.elm, this.settings, $this);
            },

            // close panel on active item click
            _closePanelonActiveSlide: function(evt) {
                evt.preventDefault();

                var $this = $(evt.target)
                    self = this;

                self._closePanelHandler(this.elm, this.settings, $this);
            },

            // show media items before loadmore on page load
            _showRequired: function() {
                var self = this;
                self.$el.find('.c-media-gallery-v2__item:lt(' + self.settings.showOnLoad + ')').show();
            },

            // show media items after loadmore is clicked
            _showAllHandler: function(evt) {
                var $this = $(evt.target),
                    self = this;

                $this.parents(self.settings.mediaGalleryV2Content).find(self.settings.mediaGalleryV2Item).show();
                $this.hide();
                self.settings.showOnLoad = self.settings.mediaGalleryV2Item.length;
            },

            // make panel active plus show hide prev next arrows when first or last media item is reached
            _setActiveParams: function(elm, settings, index) {
                var active = 'active',
                    nextNav = elm.find(settings.nextButton),
                    prevNav = elm.find(settings.prevButton);

                elm.find(settings.quickPanelWrapper).removeClass(active);
                elm.find(settings.mediaGalleryV2Item).removeClass(active).css('margin-bottom', settings.itemMargin);

                if (index === 0) {
                    prevNav.fadeOut(settings.animateSpeed);
                    nextNav.fadeIn(settings.animateSpeed);
                } else if (index === elm.find(settings.mediaGalleryV2Item).length - 1) {
                    prevNav.fadeIn(settings.animateSpeed);
                    nextNav.fadeOut(settings.animateSpeed);
                } else {
                    prevNav.fadeIn(settings.animateSpeed);
                    nextNav.fadeIn(settings.animateSpeed);
                }
            },

            // display panel on clicking media item
            _displayQuickPanel: function(evt) {
                evt.preventDefault();

                var $this = $(evt.target),
                    self = this,
                    $currentItem = $this.parents(self.settings.mediaGalleryV2Item),
                    currentIndex = $currentItem.data('index'),
                    $currentQuickPanel = $this.parents(self.settings.mediaGalleryV2List).find(self.settings.quickPanelWrapper+'[data-index='+currentIndex+']');

                self._setActiveParams(self.elm, self.settings, parseInt(currentIndex));

                self.elm.find(self.settings.mediaGalleryV2Item).each(function (index, el) {
                    var panelHeight = 0,
                        cols = 3,
                        row = currentIndex - currentIndex % cols;

                    if (index >= row && index <= row + (cols - 1)) {
                        panelHeight = $currentQuickPanel.height();
                    }
                    if (panelHeight !== 0) {
                        $(el).css("margin-bottom", panelHeight+ self.settings.itemMargin*2);
                    }
                });

                $currentItem.addClass('active');
                $currentQuickPanel.addClass('active').css('top', $currentItem.position().top + $currentItem.height() + self.settings.itemMargin);

                $currentQuickPanel.find('.c-media-gallery-v2__panel-imgwrap').show();
                $currentQuickPanel.find('.c-media-gallery-v2__panel-videowrap').hide();
            },

            // navigate through panel on prev next arrow clicks
            _navigateToItem: function(evt) {
                evt.preventDefault();

                var self = this,
                    $this = $(evt.target),
                    $currentItem = $this.parents(self.settings.quickPanelWrapper),
                    currentIndex = $currentItem.data('index');

                if ($this.data('role') === 'next') {
                    currentIndex = currentIndex + 1;
                } else if ($this.data('role') === 'prev') {
                    currentIndex = currentIndex - 1;
                }
                if(currentIndex < self.settings.showOnLoad) {
                    $this.parents(self.settings.mediaGalleryV2List).find(self.settings.mediaGalleryV2Item+'[data-index='+currentIndex+'] '+self.settings.thumbnail).click();
                }
            },
            _trackCompInfo: function() {
                var self = this,
                    compName = self.$el.find('[data-componentname]').data('componentname'),
                    compVar = self.$el.find('[data-component-variants]').data('component-variants'),
                    compPos = self.$el.find('[data-component-positions]').data('component-positions');

                digitalData.component = [];
                digitalData.component.push({
                    'componentInfo' :{
                        'componentID': compName,
                        'name': compName
                    },
                    'attributes': {
                        'position': compPos,
                        'variants': compVar
                    }
                });
            },
             _progressTrack:function(videoSource, videoId, progress){
                var ev = {},
                    self=this;

                    self._trackCompInfo();

                    ev.eventInfo={
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.videoProgress,
                      'eventLabel' : videoSource + ' - '+ videoId + ' - ' + 'progressed '+ progress
                    };
                    ev.attributes = {'nonInteractive':{'nonInteraction': 1}};
                    ev.category ={'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
            },
            _ctTag: function(videoSource, videoId, play) {
                var ev = {},
                    self=this,
                    eventAction = (play) ? ctConstants.videoPlays : ctConstants.videoCompletes;;

                    self._trackCompInfo();

                    digitalData.video = [];
                    digitalData.video.push({
                        'videoId':videoId
                    });

                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': eventAction,
                        'eventLabel': videoSource + ' - ' + videoId
                    };
                    ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
                    ev.category = {'primaryCategory':ctConstants.other};
                    digitalData.event.push(ev);
            },

            /**
             * Player state change callback with player states added in switch case
             * @method _playerStateChange
             * @return 
             */

            _playerStateChange: function(videoPlayer, state) {                
                var $player = $(videoPlayer.h);
                if (state) {
                    /*switch (state.data) {
                        case -1:
                            console.log('buffering');
                            break;
                        case 0:
                            console.log('video completes');
                            break;
                        case 1:
                            console.log('playing');
                            break;
                        case 2:
                            console.log('pause');
                            break;
                        case 3:
                            console.log('replay');
                            break;
                    }
                    */
                    // video analytics
                    this._ctTag($player.data('video-source'), $player.data('video-id'), state.data);
                }

                this.triggerMethod('playerStateChange');
            },

            /**
             * _onPlayerReady callback to check id youtube player is ready to use
             * @method _onPlayerReady
             * @return 
             */
            _onPlayerReady: function(videoPlayer) {
                //console.log(videoPlayer);
            },
        });
    });

    return MediaGalleryV2;
});