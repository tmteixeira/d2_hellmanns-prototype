/*global define*/

define(function() {
    var CountryLanguageSelector = IEA.module('UI.country-language-selector', function(countryLanguageSelector, app, iea) {

        _.extend(countryLanguageSelector, {

            onBeforeInit: function(options) {

            },

            onInit: function(options) {

            },

            onBeforeRender: function() {

            },

            onRender: function() {

            },

            onEnable: function() {
                
            },

            onMouseenter: function() {

            },

            onMouseleave: function() {

            },

            onBeforeExpand: function() {

            },

            onAfterExpand: function() {

            }

        });
    });

    return CountryLanguageSelector;
});
