/*global define*/

define(['eGiftingService'], function() {
    'use strict';

    var eGiftingService = null;

    var SimpleHeader = IEA.module('UI.simple-header', function(simpleHeader, app, iea) {

        _.extend(simpleHeader, {

            defaultSettings: {
                timerSelector: '#timerText',
                timerWrapper: '.c-simple-header__copy',
                giftConfig: 'div[data-role="gift-configurator"]',
                hiddenCl: 'o-hidden',
                notificationSl: '.c-simple-header__copy--notification',
                expirationSl: '.c-simple-header__copy--expiration',
                timerExtn: '.c-simple-header__copy--extn',
                formId: '#redirectForm',
                cookieName: 'cartSession',
                storageName: 'productCartData'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {                
                this.triggerMethod('beforEnable');
                var self = this,
                settings = self.defaultSettings,
                timerSelector = $(settings.timerSelector),
                timerVal = timerSelector.data('timer');

                self.componentJson = self.getModelJSON().simpleHeader;
                self.cookieName = settings.cookieName+'-'+self.componentJson.market;
                self.storageName = settings.storageName;

                eGiftingService = new IEA.EGifting();

                if (timerSelector.length) {
                    if (eGiftingService.hasCookieData(self.cookieName)) {
                        var cookieTimer = new Date(eGiftingService.readCookie(self.cookieName)),
                        timeLeft = cookieTimer - new Date,
                        min = (timeLeft/1000/60) << 0,
                        sec = (timeLeft/1000) % 60;

                        self.expireTimer(min, sec);
                        $(settings.timerWrapper).removeClass(settings.hiddenCl);
                    } else {
                        if ($(settings.giftConfig).length) {
                            $(settings.timerWrapper).addClass(settings.hiddenCl);
                            $('body').addClass('timer-inactive');
                        } else {
                            $(settings.timerWrapper).removeClass(settings.hiddenCl);
                            $(settings.notificationSl).text($(settings.expirationSl).text());
                            $(settings.timerExtn).addClass('hidden');
                        }
                        if (eGiftingService.getItem(self.storageName) !== undefined && eGiftingService.getItem(self.storageName) !== '' && eGiftingService.getItem(self.storageName) !== null) {
                            eGiftingService.removeItem(self.storageName);
                        }
                        self.clearCookieSession();                    
                    }              
                }
                this.triggerMethod('enable');
            },

            clearCookieSession: function() {
                var settings = this.defaultSettings,
                redirectForm = settings.formId,
                errorPageUrl = this.componentJson.expireTimerNotification.errorPageUrl,
                expirationMessageText = this.componentJson.expireTimerNotification.expirationMessageText

                if (redirectForm.length && !$('.wcmmode-edit').length) {
                     $(redirectForm).attr('action', errorPageUrl);
                    $('#errorMsg').val(expirationMessageText);
                    $(redirectForm).submit(); 
                }

                this.trackInputError(expirationMessageText);                          
            },

            trackInputError: function(message) {
                //Error page tracking on shopping basket               
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined' && $('.shopping-basket').length) {
                    var ev = {};
                    ev.eventInfo = {
                        'type':ctConstants.trackEvent,
                        'eventAction': ctConstants.inputerror,
                        'eventLabel' : message
                    };

                    ev.category = {'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                }
            },

            checkTwoDigits: function(n) {
                return (n <= 9 ? "0" + n : n);
            },
            
            expireTimer: function(minutes, seconds) {           
                var self = this,
                settings = this.defaultSettings,
                endTime, hours, mins, msLeft, time,
                selector = $('#timerText');

                function updateTimer() {
                    msLeft = endTime - (+new Date);
                    if ( msLeft < 1000 ) {
                        selector.text('');
                        $(settings.notificationSl).text($(settings.expirationSl).text());
                        $(settings.timerExtn).addClass('hidden');

                        eGiftingService.clearSession(self.cookieName, self.storageName);
                        self.clearCookieSession();
                    } else {
                        time = new Date(msLeft);
                        hours = time.getUTCHours();
                        mins = time.getUTCMinutes();
                        var val = (hours ? hours + ':' + self.checkTwoDigits(mins) : mins) + ':' + self.checkTwoDigits(time.getUTCSeconds());
                        selector.text(val);
                        setTimeout( updateTimer, time.getUTCMilliseconds() + 500 );
                    }
                }                

                endTime = (+new Date) + 1000 * (60*minutes + seconds) + 500;
                updateTimer();
            }

            /**************************PRIVATE METHODS******************************************/          

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */
        });
    });

    return SimpleHeader;
});
