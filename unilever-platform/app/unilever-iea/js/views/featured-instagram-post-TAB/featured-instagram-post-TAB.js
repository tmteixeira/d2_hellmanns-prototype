/*global define*/

define(['socialSharing'], function() {
    'use strict';

    var FeaturedInstagramPostTAB = IEA.module('UI.featured-instagram-post-TAB', function(featuredInstagramPostTAB, app, iea) {
		
		var socialService = null;
		
        _.extend(featuredInstagramPostTAB, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');
				
                // functions to be wrirren here
				this._socialSharing();
				
                this.triggerMethod('enable');
            },
			
			//Social Sharing
			_socialSharing: function(){
				var options = {};
                options.socialSharingMenu = $('.c-social-sharing-stack');
                options.socialSharing = $('.js-social-sharing');
                socialService = new IEA.socialsharing(); // creates new instance
                socialService.enableSocial(options);
			}


            /**************************PRIVATE METHODS******************************************/          

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */

        });
    });

    return FeaturedInstagramPostTAB;
});
