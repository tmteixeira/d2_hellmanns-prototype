/*global define*/

define(['videoplayerService', 'commonService'], function() {
    'use strict';

    var RecipeHero = IEA.module('UI.recipe-hero', function(recipeHero, app, iea) {

         var videoplayerService = null,
            commonService = null;

        _.extend(recipeHero, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */

            events: {
                'click .c-recipe-video': '_clickVideoHandler'
            },

            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');
                var self = this;               
                self.recipeJsonData = self.getModelJSON().recipeHero;
                if(self.recipeJsonData.recipeVideo && self.recipeJsonData.recipes.assets.video) {
                    this._setupPlayer();    
                }
                
                this._isEnabled = true;

                this.triggerMethod('enable');
            },

             _setupPlayer: function() {
                var options = {}, self= this,videoId,
                videojsonUrl = self.recipeJsonData.recipes.assets.video.default[0].videoUrl;
                commonService = new IEA.commonService();

                options.videoContainerId = 'recipe-video-player';
                options.view = self;
                options.videoType = 'youtube';
              
                videoId = commonService.youtubeIdExtracter(videojsonUrl);
                $('#recipe-video-player').attr('data-video-id',videoId);
               
                videoplayerService = new IEA.videoPlayerService(options);
            },

            onPlayerReady: function() {
                this.events['click .c-recipehero-video'] = '_clickVideoHandler';
                this.delegateEvents();
            },

            onVideoEnded: function() {
                if (commonService.isMobileAndTablet()) {

                    this.$el.find('.js-video-btn').removeClass('hidden');
                    this.events['click .js-video-btn'] = '_replayVideoHandler';
                    this.delegateEvents();
                }
            },

            _clickVideoHandler: function(evt) {
                var options = {};

                options.videoContainerId = 'recipe-video-player';
                options.view = this;
                options.videoType = 'youtube';

                if (evt) {
                    evt.preventDefault();
                }
                if ($('.is-hero-video-open').length) {
                    $('.c-hero-image .is-hero-video').click();
                }
            
                videoplayerService.setupVidepAPI(options);

                this.$el.find('.c-video-player__video').css({'display': 'block'});
                this.$el.addClass('js-video-player__open');

                $('body, html').addClass('is-video-player__open');

                this._isVideoOpened = !this._isVideoOpened;
            },

            _replayVideoHandler: function() {

                videoplayerService.play();

                this.$el.find('.js-video-btn').addClass('hidden');
                delete this.events['click .js-video-btn'];
                this.delegateEvents();
            },

            _ctComponentInfo: function() {
                var self = this,
                    compName = self.$el.find('[data-componentname]').data('componentname'),
                    compVar = self.$el.find('[data-component-variants]').data('component-variants'),
                    compPos = self.$el.find('[data-component-positions]').data('component-positions');
                
                digitalData.component = [];
                digitalData.component.push({
                    'componentInfo' :{
                        'componentID': compName,
                        'name': compName
                    },
                    'attributes': {
                        'position': compPos,
                        'variants': compVar
                    }
                });
            },
            
            _progressTrack: function(progress, videoId) {
                var ev = {},
                    self = this,
                    hostedUrl = window.location.href;
                
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    
                    self._ctComponentInfo();

                    digitalData.video = [];
                    digitalData.video.push({
                        'videoid' : videoId
                    });

                    ev.eventInfo={
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.videoProgress,
                      'eventLabel' : hostedUrl + ' - ' + videoId
                    };

                    ev.attributes={'nonInteraction':1};
                    ev.category ={'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);
                }
            },
            
            _ctTag: function(videoName, videoId, play) {
                var ev = {},
                    hostedUrl = window.location.href;
                
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    
                    this._ctComponentInfo();

                    digitalData.video = [];
                    digitalData.video.push({
                        'videoId': videoId
                    });

                    if (play === true) {
                        ev.eventInfo={
                            'type': ctConstants.trackEvent,
                            'eventAction': ctConstants.videoPlays,
                            'eventLabel': hostedUrl + ' - ' + videoId
                        };
                    } else {
                        ev.eventInfo={
                            'type': ctConstants.trackEvent,
                            'eventAction': ctConstants.videoCompletes,
                            'eventLabel': hostedUrl + ' - ' + videoId
                        };
                    }
                    ev.category = {'primaryCategory':ctConstants.other};
                    digitalData.event.push(ev);
                }
            }
            /**************************PRIVATE METHODS******************************************/          

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */

        });
    });

    return RecipeHero;
});
