/*global define*/

define(['ratings'], function() {
    'use strict';

    var FeaturedProduct = IEA.module('UI.featured-product', function(featuredProduct, app, iea) {
		var ratingService = null;
        _.extend(featuredProduct, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
				ratingService = new IEA.ratings(); // creates new instance

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));
                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },			
			
            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
				this.triggerMethod('beforEnable');
                var options = {},
                    featuredProductData = this.getModelJSON().featuredProduct;
                
				options.ratingReview = featuredProductData.review;

                if (featuredProductData.productList.length) {
                    options.productId = featuredProductData.productList;
                    options.brandName = featuredProductData.brandName;
                    options.ratingObj = featuredProductData.productList[0].ratings;
                    ratingService.initRatings(options); 
                }              

                this.triggerMethod('enable');
            }  
           


            /**************************PRIVATE METHODS******************************************/          

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */

        });
    });

    return FeaturedProduct;
});
