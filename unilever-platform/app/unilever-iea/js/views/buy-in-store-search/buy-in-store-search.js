/*global define*/

define(['animService', 'ratings', 'postmessage'], function() {
    'use strict';

    var productData, productIndex, varientIndex;
    var BuyInStoreSearch = IEA.module('UI.buy-in-store-search', function(buyInStoreSearch, app, iea) {
        var animService = null,
            ratingService = null;

        _.extend(buyInStoreSearch, {
            modalCl: '.js-modal-etale',
            events: {
                'click .js-btn-shareLoc' : 'shareLoc'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
                ratingService = new IEA.ratings(); // creates new instance
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                var self = this, options = {};
				self.completeData = self.getModelJSON().buyInStoreSearch;

                options.ratingReview = self.completeData.review;
				options.catData = self.completeData.categories;
				options.regExp = self.completeData.storeLocator.fields.postCode.regex;
				self.generateSubCat(options);
                self.toggleVariant();
                self.submitWtbForm(options);
                self.getProductValue();
                animService = new IEA.animService();
                ratingService.initRatings(options);

                this.triggerMethod('enable');
            },

            generateSubCat: function(options) {
				var self = this;

				var dataArray=[];
				for(var i = 0; i < options.catData.length; i++){
					dataArray.push([options.catData[i].name, options.catData[i].subCategory, options.catData[i].pagePath])
				}

				$(document).on('change', '.js-bss-selectItem', function(e){
					e.preventDefault;
					var foundFlag = false;
					$('.c-buy-in-store-search-step2').empty();
					$('.c-buy-in-store-search__form-wrapper').hide();
						$(this).nextAll().remove();

						var clickedVal = this.value,
							subCat = '';
						dataArray.filter(function(v){
							if(v[0] === clickedVal) {
								subCat = v[1];
								if(subCat) {
									for(var i = 0; i < subCat.length; i++){
										for(var j = 0; j < dataArray.length; j++){
											if(dataArray[j][0] == [subCat[i].name]){
												foundFlag = true;
											}
										}
										if(foundFlag === false){
											dataArray.push([subCat[i].name, subCat[i].subCategory, subCat[i].pagePath])
										}
										foundFlag = false;
									}
									selectSubCat(clickedVal, subCat);
								} else {
									var pagePathVal = v[2];
									self.getProductData(pagePathVal);
								}
							}
						});
				});

				function selectSubCat(currentValue, data) {
					var returnArray=[], options = '', selectStr = $('<select class="c-buy-in-store-search__select o-dropdown form-control js-bss-selectItem"><option value="' + self.completeData.selectLabel + '">' + self.completeData.selectLabel + '</option></select>');
					for(var i = 0; i < data.length; i++){
						returnArray.push([data[i].name, data[i].subCategory]);
						options += '<option value="' +data[i].name+ '">'+data[i].name+'</option>';
					}
					var dropList = selectStr.append(options);
					$('.c-buy-in-store-search-select').append(dropList);
				};
			},
            getProductData: function(pagePath) {
                var self = this, currentPageUrl = self.completeData.currentPage, dataUrl = self.completeData.productDataPath;

                $.ajax({
                    url: dataUrl,
                    type: 'GET',
                    data: {'cpt' : pagePath, 'cppt' : currentPageUrl},
                    dataType: 'json',
					contentType: "application/json; charset=ISO-8859-1",
                    success: function(data) {
                        var template, productList;
                        template = self.getTemplate('buy-in-store-search-info', 'buy-in-store-search/partial/buy-in-store-search-info.hbss');
                        productData = data.data;
                        productList = template(productData);
                        $('.c-buy-in-store-search-step2').append(productList);
                    },
                    error: function() {
                    }
                });
            },

            toggleVariant: function() {
                var self = this;
                $(document).on('click', '.js-buy-in-store-search-list li a', function() {
                    productIndex = $(this).parent('li').index();
                    $('.c-buy-in-store-search-variant__list').find(".choose-variant").attr("checked", false);
                    $('.c-buy-in-store-search-variant__list').slideUp();
                    $(this).next('.c-buy-in-store-search-variant__list').stop().slideDown("slow");
                    $('.c-buy-in-store-search__form-wrapper').hide();
                });
            },

            getProductValue: function() {
                var self = this;
                $(document).on('change', '.js-buy-in-store-search-list .choose-variant[name="variant"]', function() {
                    var productUrl = $(this).data('product-url'),
                        variantUrl = $(this).data('variant-url'),
                        productId = $(this).val();

                    $('#preUrl').val(productUrl);
                    $('#pvi').val(productId);
                    $('#c-buy-in-store-search-form').attr('action', variantUrl);
                    $('.c-buy-in-store-search__form-wrapper').show();
                    varientIndex = $(this).parent('li').index();
                    self.buyOnline();
                });
            },

            buyOnline: function() {
				var buyOnlineObj = {}, template, buyOnlineBtn;
				buyOnlineObj.shopNow = productData.shopNow;
				buyOnlineObj.product = productData.products[productIndex];
				template = this.getTemplate('bin-widget', 'partials/bin-widget.hbss');
				buyOnlineBtn = template(buyOnlineObj);

                $('.c-buyonline').html(buyOnlineBtn);
                if(productData.shopNow.serviceProviderName === 'constantcommerce' || productData.shopNow.serviceProviderName === 'click2buy') {
                    $.getScript(productData.shopNow.serviceURL);
                }

				//this.appendPostMessage(buyOnlineObj);
				this.handleBinButton(buyOnlineObj.product, buyOnlineObj.shopNow);
			},




			handleBinButton: function(productData, shopNowData) {
                var smartProductId, self = this;
                $('.js-bin-button').click(function() {
                    smartProductId = productData.productsDetail[varientIndex].smartProductId;
                    loadsWidget(smartProductId, this,'retailPopup','en');
                });

                $('.js-etale-bin-button').click(function(e) {
                    e.preventDefault();
                    self.openModal();
                    $('.js-main-wrapper').removeClass('u-blur-background');
                    $('#iframeId').attr('src', shopNowData.serviceURL+productData.productsDetail[0].smartProductId+'#'+shopNowData.pageurl);
                    self.etaleTracking(productData.productID);
                    if(productData.productsDetail.length) {
                        $('#iframeId').attr('src', shopNowData.serviceURL+productData.productsDetail[0].smartProductId+'#'+shopNowData.pageurl);
                    }

                });

                $('.js-btn-close').click(function(e) {
                    e.preventDefault();
                    self.closeModal();
                });
            },


			openModal: function () {
                animService.openOverlay(this.modalCl);
            },

            closeModal: function () {
                animService.closeOverlay(this.modalCl);
            },

            submitWtbForm: function(options) {
                var self = this;
                $('.js-btn-bss').click(function() {
                    var postalCode = $('.postalCode').val(),
                        actualURL = '',
                        validZip = self.zipCodeValidation(postalCode, options.regExp);

                    if(validZip === true) {
                        $('.help-block').addClass('hidden');
                        $('.postalCode').parents('.form-group').removeClass('has-error');
                    } else {
                        $('.postalCode').parents('.form-group').addClass('has-error');
                        $('.help-block').removeClass('hidden');
                        return false;
                    }
                });
			},

			zipCodeValidation: function(postalCode, regExp) {
				var regEx = regExp, pattern = new RegExp(regEx), isValidZip = pattern.test(postalCode);
				return isValidZip;
			},

			shareLoc: function() {
                var self=this;
				if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(function(position){
						var pos = {
							lat: position.coords.latitude,
							lng: position.coords.longitude
						}
						reverseGeoCode(pos.lat, pos.lng, function(zip) {
							$('.postalCode').val(zip);
						});
					});

					var reverseGeoCode = function(lat, lng, callback) {
						var mapServiceUrl = self.completeData.geoCodeApi,
							googleService = mapServiceUrl + lat + ',' + lng + '&sensor=false';
						$.getJSON(googleService, cbFunc);

						function cbFunc(data) {
							var addressComponents = data.results[0].address_components,
							postalCode = '', countryCode = '', type = '';
							if (addressComponents) {
								for (var i = 0, ii = addressComponents.length; i < ii; i++) {
									if (typeof addressComponents[i].types == "string") {
										type = addressComponents[i].types;
									}
									else if (typeof addressComponents[i].types == "object") {
										type = addressComponents[i].types[0];
									}

									if (type == 'postal_code') {
										postalCode = addressComponents[i].long_name;
									}
								}

								if (typeof callback === 'function') {
									callback(postalCode);
								}
							}
						}
					}
				}
			},

            etaleTracking: function(prodId){
                // Get the parent page URL as it was passed in, for browsers that don't support
                // window.postMessage (this URL could be hard-coded).
                var parent_url = decodeURIComponent( document.location.hash.replace( /^#/, '' ) ),
                    link;

                // The first param is serialized using $.param (if not a string) and passed to the
                // parent window. If window.postMessage exists, the param is passed using that,
                // otherwise it is passed in the location hash (that's why parent_url is required).
                // The second param is the targetOrigin.
                function postMessage(model, partnum, prodid, proddesc, markettext, ean, upc, etin, retailer) {
                    var message = '{ "model" : "' + model +
                                    '", "partnum" : "' + partnum +
                                    '", "prodid" : "' + prodid +
                                    '", "proddesc" : "' + proddesc +
                                    '", "markettext" : "' + markettext +
                                    '", "ean" : "' + ean +
                                    '", "upc" : "' + upc +
                                    '", "etin" :"' + etin +
                                    '", "retailer" : " ' + retailer +
                                    '", "date": "' + new Date() + '"}' ;
                    $.postMessage(message, parent_url, parent );
                }

                $.receiveMessage(
                    function (e) {
                        var json = JSON.parse(e.data),
                            retailername = json.retailer;

                        if (typeof retailername !== 'undefined') {
                            if(typeof $BV === 'object') {
                                $BV.SI.trackConversion({
                                    'type': 'BuyOnline',
                                    'label': retailername,
                                    'value': prodId
                                });
                            }
                        }
                    }
                );
            }
            /**************************PRIVATE METHODS******************************************/

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:
            */

        });
    });

    return BuyInStoreSearch;
});
