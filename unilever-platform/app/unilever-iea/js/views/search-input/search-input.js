/*global define*/

define( function() {

    'use strict';

    var HeaderSiteSearchComponent = IEA.module('UI.search-input', function (module, app, iea) {

        _.extend(module, {

            onEnable: function() {

            }

        });
    });

    return HeaderSiteSearchComponent;
});
