/*global define*/

define(['TweenMax', 'handlebars', 'slick',  'animService', 'commonService', 'ratings', 'iframetracker', 'postmessage'], function (TweenMax, handlebars) {
    'use strict';

    var ProductOverview = IEA.module('UI.product-overview', function (productOverview, app, iea) {

        var animService = null,
            commonService = null,
			ratingService = null;

        _.extend(productOverview, {

            '$carousel': '.js-thumb-carousel',
            '$carouselNav': '.js-thumb-carousel__nav',
            '$carouselProductSmall':'c-thumb-carousel__product--small',
            '$productOverviewSelect':'#js-product-overview__select',
			'$unitSelect':'.js-btn-buyStore',
            'currentSlide': 0,
            'currentData': {},
            'currentProductID': 0,
            'totalSlides': 0,
            default: {
                'carousel': {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true,
                    speed: 1250,
                    asNavFor: '.js-thumb-carousel__nav'
                },
                'carouselNav': {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    asNavFor: '.js-thumb-carousel',
                    dots: false,
                    centerMode: false,
                    speed: 1250,
                    focusOnSelect: true
                },
                modalCl: '.js-modal-etale'
            },
			
			events: {
				'click .js-btn-buyStore' : 'submitBuyStoreForm',
				'click .js-btn-show-store' : 'submitBuyStoreForm',
				'click .js-btn-shareLoc' : 'shareLoc'
			},

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
				ratingService = new IEA.ratings(); // creates new instance

                this.triggerMethod('init');
            },
            setupZoom: function () {
                var self = this;

                $('.js-c-product-overview__modal_prev').on('click', function () {
                    $(self.$carousel).slick('slickPrev');
                    animService.zoomSwap(self.getZoomImage());
                });

                //next zoom button
                $('.js-c-product-overview__modal_next').on('click', function () {
                    $(self.$carousel).slick('slickNext');
                    animService.zoomSwap(self.getZoomImage());
                });

                //open modal with the url
                $('.js-product-overview__zoom').on('click', function () {
                    if(typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                        // ZOOM analytics code
                        var label = $(this).data('label'),
                            productName = $(this).data('productname'),
                            productID = $(this).data('productid'),
                            productCategory = $(this).data('productcategory'),
                            ev = {};

                        ev.eventInfo = {
                          'type':ctConstants.trackEvent,
                          'eventAction': ctConstants.widgets,
                          'eventLabel' : label
                        };
                        digitalData.product=[];
                        digitalData.product.push(
                        {
                           'productInfo' :{
                           'productID':productID,
                           'productName': productName
                           },
                           'category':{
                           'primaryCategory':productCategory
                           }
                        });
                        ev.category ={'primaryCategory':ctConstants.other};
                        digitalData.event.push(ev);
                    }
					self.updateZoom();	
                });				
            },
            onMatchMobile: function () {
                animService.mobileCheck();
            },
            onMatchTablet: function () {
                animService.mobileCheck();
            },
            updateZoom: function () {
                animService.zoomOpenUrl(this.getZoomImage());
            },

            /**
             * returns the path to the full size product image
             * @method getZoomImage
             * @return string
             */
            getZoomImage: function () {
                var currentImg = this.currentData[this.currentProductID].images[this.currentSlide];
                //return currentImg.zoomImage;
                var currentImgLength = this.currentData[this.currentProductID].images.length;
                if(currentImgLength > 1) {
                    $(".o-modal-slider-nav").removeClass("hidden");
                } else {
                    $(".o-modal-slider-nav").addClass("hidden");
                }
                //with extension
                return currentImg.zoomImage.url;
            },

            /**
             * start carousels, enable select dropdown to swap carousel images on change
             * @method carouselInit
             * @return
             */
            carouselInit: function () {
                var self = this;

                //retrieve page JSON
                self.completeData = self.getModelJSON().productOverview;
                self.currentData = self.completeData.product.productsDetail;
                self.totalSlides = self.currentData[0].images.length;
				
				//check if isCarouselEnabled is true or false
				if(self.getModelJSON().productOverview.product.isCarouselEnabled === "false"){
					$(".js-thumb-carousel__nav").css({display: "none"});
				}

                //start carousels
                $(self.$carouselNav).slick(self.default.carouselNav);
                $(self.$carousel).slick(self.default.carousel);

                self.hasNavCarousel(self.totalSlides);

                //animate in
                TweenMax.to('.c-product-overview__thumb-carousel', 1, {alpha: 1});

                // update carousel images when the user interacts with the drop-down list
                $(self.$productOverviewSelect).on('change', function (e) {
                    e.preventDefault();
                    
                    //get the selected index
                    self.currentProductID = this.selectedIndex;
                    var data = self.currentData[self.currentProductID];
                    var cdata = self.completeData;
                        
                    //get carousel slide count
                    self.totalSlides = data.images.length;

                    //remove and update carousels
                    TweenMax.to(window,1, {scrollTo:{y:0}});

                   setTimeout( function scrollcomplete(){
                        self.updateDropdown(data, cdata);
                        self.removeCarouselImages(self, data);
                        //container adjustment according to product size
                        self.productSize(data);
                    },1000);            
                });


                $(self.$carousel).on('afterChange', function (event, slick, currentSlide, nextSlide) {
                    self.currentSlide = currentSlide;
                    //updates the navigation carousel according to the parent carousels position
                    if (self.currentData[self.currentProductID].images.length <= 3) {
                        $(self.$carouselNav).find('.slick-current').removeClass('slick-current');
                        $(self.$carouselNav).find('.slick-slide:eq(' + currentSlide + ')').addClass('slick-current');
                    }
                });

                app.on('window:resized', function (options) {
                    // gets triggered when the application start is fired.
                    if(commonService.isMobileAndTablet()){
                        TweenMax.fromTo([self.$carousel,self.$carouselNav], 0.5, {alpha:0, onComplete:function(){
                            setTimeout(function(){
                                $(self.$carousel).slick('refresh',true);
                                $(self.$carouselNav).slick('refresh',true);
                            },500);
                        }},{delay:0.5, alpha:1});
                    }
                });
            },
            handleBinButton: function(data) {
                var productId,
                    self = this;
                if(data.shopNow.serviceProviderName === 'constantcommerce' || data.shopNow.serviceProviderName === 'click2buy') {
                    $.getScript(data.shopNow.serviceURL);
                }
                $('.js-bin-button').click(function() {
                    if ($('#js-product-overview__select').length) {
                        productId = data.product.productsDetail[$('#js-product-overview__select option:selected').index()].smartProductId;
                    } else {
                        productId = data.product.productsDetail[0].smartProductId;
                    }
                    loadsWidget(productId, this,'retailPopup','en');
                });
                
                $('.js-etale-bin-button').click(function(e) {
                    e.preventDefault();
                    self.openModal();
                    $('.js-main-wrapper').removeClass('u-blur-background');
                    $('.o-modal').css('background', 'rgba(255, 255, 255, 1)');
                    self.createIframeSrcAttribute(data);
                });
                
                $('.js-btn-close').click(function(e) {
                    e.preventDefault();
                    self.closeModal();
                });
            },
            
            createIframeSrcAttribute: function(data){
                document.getElementById("iframeId").setAttribute("src", data.shopNow.serviceURL+data.product.productsDetail[0].smartProductId+"#"+data.shopNow.pageurl);
            },
            
            openModal: function () {
                animService.openOverlay(this.default.modalCl);
            },
            
            closeModal: function () {
                animService.closeOverlay(this.default.modalCl);
            },
			
            productSize: function (data) {
                var container = $('.c-product-overview');
                
                if (data.productSize === 'Short') {
                    container.addClass(this.$carouselProductSmall);
                } else {
                    container.removeClass(this.$carouselProductSmall);
                }
				setTimeout(function(){
					if(data.images.length > 0) {
						$('.c-product-overview Button').removeClass("hidden");	
					} else {
						$('.c-product-overview Button').addClass("hidden");	
					}
				}, 500);
            },
            /**
             * hides the nav carousel if there is only one product image
             * @method hasNavCarousel
             * @return null
             */

            hasNavCarousel: function (total) {
                if (total == 1) {
                    $(this.$carouselNav).addClass('u-hide');
                } else {
                    $(this.$carouselNav).removeClass('u-hide');
                }
            },

            /**
             * Select box price and unit update, used to update the label text
             * @method updateDropdown
             * @return
             */

            updateDropdown: function (data, cdata) {
                $('.js-dropdown__price').html(data.price);
                $('.js-dropdown__unit').html(data.unit);
                if(data.price) {
                    $('.js-dropdown__retail').html(data.retail);
                } else {
                    $('.js-dropdown__retail').html("");   
                }
            },
			

            /**
             * Remove current slides from Carousel
             * @method removeCarouselImages
             * @return
             */
            removeCarouselImages: function (self, data) {
                TweenMax.to([self.$carousel, self.$carouselNav],.5, {
                    autoAlpha: 0, onComplete: function () {
                        $(".js-thumb-carousel .slick-track").empty();
                        $(".js-thumb-carousel__nav .slick-track").empty();
                        /*for (var i = 0; i <= self.totalSlides; i++) {
                            $(self.$carousel).slick('slickRemove', 0);
                            $(self.$carouselNav).slick('slickRemove', 0);
                        }*/
                        self.addCarouselImages(self, data);
                    }
                });
            },
            /**
             * add new array of images to the Carousel
             * @method addCarouselImages
             * @return
             */
            addCarouselImages: function (self, data) {
                var productSize;
                for (var i = 0; i < data.images.length; i++) {
                    var obj = data.images[i];
                    productSize = (data.productSize === 'Short') ? 'c-thumb-carousel-nav__product--small' : '';
                    //console.log(obj, handlebars.helpers.adaptiveRetina( obj.path, obj.extension, obj.title, obj.altImage , "", "", "false", '490,490,490,490,640,640,460,460'));
                    //class for short or tall products
                    $(self.$carousel).slick('slickAdd', '<div class="c-thumb-carousel__img">' +
                        handlebars.helpers.adaptiveRetina( obj.path, obj.extension, obj.altImage, obj.title, obj.fileName, obj.url, "false", "490,490,490,490,640,640,460,460", "false") + '<meta itemprop="image" content="' + obj.url + '" /></div>');
                    $(self.$carouselNav).slick('slickAdd', '<div class="c-thumb-carousel__img--thumb ' + productSize + '" style="background-image: url(' + obj.thumbImage.url + ')"></div>');
                }

                TweenMax.to([self.$carousel, self.$carouselNav], 1, {
                    autoAlpha: 1, onStart: function () {
                        self.hasNavCarousel(data.images.length);
                    }
                });
                
                if(data.images.length > 0) {
                    $(".js-product-overview__zoom").removeClass("hidden");
                    if(data.productSize == "Tall") {
                        $(".c-product-overview").removeClass("c-thumb-carousel__product--small");
                    } else {
                        $(".c-product-overview").addClass("c-thumb-carousel__product--small");
                    }
                } else {
                    $(".js-product-overview__zoom").addClass("hidden")
                    if((data.productSize == "Short")) {
                        $(".c-product-overview").removeClass("c-thumb-carousel__product--small");
                    }
                }

                //lazy load images
                self.app.trigger('image:lazyload', self);
                
            },
            getSlideCount: function () {
                return $(this.$carousel).find('.slick-slide').length;
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                var productOverviewData = this.getModelJSON().productOverview;
				var self = this,
				    options = {};
				
				options.ratingReview = productOverviewData.review;
                options.ratingObj = productOverviewData.product.ratings;
                options.productId = productOverviewData.product.ratings.uniqueId;
                options.productName = productOverviewData.product.title;
				options.brandName = productOverviewData.brandName;
                options.componentName = productOverviewData.componentName;
				options.componentVariants = productOverviewData.componentVariation;
				options.componentPositions = productOverviewData.componentPosition;
                options.productCategory = productOverviewData.product.primaryCategory;
				ratingService.initRatings(options);
				
                commonService = new IEA.commonService();
				animService = new IEA.animService();	
				
                this.carouselInit();
				
				//get current page URL
				$('#preUrl').val(window.location.pathname);

				//tool tip show/hide
                 // Set Nutrition Facts tooltip
                commonService.tooltipEvent('.js-btn__form', '.c-product-overview__storewrapper', '.c-product-overview__store-locator');
             
				$('.c-buyStore__quick-close').on("click",function(){
                    self._closebutton();
                });
                
                $('.c-field-tooltip__icon').on("click",function(){
					$('.c-field-tooltip__copy').toggle();
				})

                //wait for zoom component to be added to page
                this.setupZoom();
                commonService.ios7SVGsupport(['.js-c-product-overview__modal_prev',
                            '.js-c-product-overview__modal_next',
                            '.js-product-overview__zoom__close']);			

                /*this.handleBinButton(productOverviewData.product);*/	
                this.handleBinButton(productOverviewData);
                if($("body").hasClass('pdp-page')) {   
                    var pageUrl=window.location.href;
                    setTimeout(function(){
                           var query=pageUrl.split('#');
                            if(query[1]=="reviews"){
                              $('html, body').animate({
                              scrollTop: $(".reviews").offset().top
                              }, 1000);
                            }
                    },2000);				
					
				   var productName=$(".c-product-overview__title").text(), productId=$('div[data-role="product-overview"]').find('div[data-product-id]').attr('data-product-id'), compName=$('.c-product-overview').data('componentname'), productCategory=$('.c-product-overview').data('product-category');
				   if(typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                        var ev = {}; 					
                        digitalData.product.push({
                           'productInfo' :{
							'productID':productId,
							'productName':productName
                           },
						   'category':{
							   'primaryCategory': productCategory
					   	   }
                        });					
					   	
                        digitalData.component.push({'componentInfo' :{
                           'componentID':compName
                        }});

                        ev.eventInfo={
                          'type':ctConstants.productInfo
                        };
                        ev.category ={'primaryCategory':ctConstants.custom};
                        ev.attributes={'nonInteractive':{'nonInteraction': 1}};
                        digitalData.event.push(ev);
                        digitalData.product = [];
                        digitalData.component = [];
                    }
                }
				
                self.trackIframeMethod();

                if(productOverviewData.product.awards) {
                    self._dateformat();
                }
                
				self.etaleTracking();

                this.triggerMethod('enable');
            },

            trackIframeMethod: function() {
                var self = this;
                $('.constantco-widget iframe').iframeTracker({
                    blurCallback: function() {
                        self.trackShopNow();
                    },
                    overCallback: function(element) {
                        this._overId = $(element).attr('id'); // Saving the iframe wrapper id
                    },
                    outCallback: function(element) {
                        this._overId = null; // Reset hover iframe wrapper id
                    },
                    _overId: null
                });
            },

            trackShopNow: function() {
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    var ev = {},
                        obj = $('.c-product-overview'),
                        productID = obj.data('product-id'),
                        productName = obj.data('product-name'),
                        productCategory = obj.data('product-category'),
                        compName = obj.data('componentname'),
                        productPrice = obj.find('.js-product__price').text(),
                        variant =  obj.find('.js-product__quantity').text(),
                        brandName = obj.data('brandname');

                    digitalData.product = [];
                    digitalData.component = [];

                    digitalData.component.push({'componentInfo' :{
				   		'componentID': compName,
						'name': compName
                    }});

                    digitalData.product.push({
                       'productInfo' :{
                           'productID': productID,
                           'productName': productName,
                           'price': productPrice,
                           'brand': brandName,
                           'quantity': 1,
                       },
                       'category':{
                            'primaryCategory': productCategory
                       },
                       'attributes': {
                            'productVariants':variant,
                            'listPosition': 1
                       }
                    });

                    ev.eventInfo = {
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.purchase,
                      'eventLabel' : "Online - " + productName
                    };
                    ev.category = {'primaryCategory':ctConstants.conversion};
                    digitalData.event.push(ev);
                }
            },
			
			shareLoc: function() {				
				if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(function(position){
						var pos = {
							lat: position.coords.latitude,
							lng: position.coords.longitude
						}
						reverseGeoCode(pos.lat, pos.lng, function(zip) {
							$('.postalCode').val(zip);
						});	
					});
					
					var reverseGeoCode = function(lat, lng, callback) {
						var mapServiceUrl = '//maps.googleapis.com/maps/api/geocode/json?latlng=',
							googleService = mapServiceUrl + lat + ',' + lng + '&sensor=false';
						$.getJSON(googleService, cbFunc);

						function cbFunc(data) {
							var addressComponents = data.results[0].address_components,
							postalCode = '', countryCode = '', type = '';
							if (addressComponents) {
								for (var i = 0, ii = addressComponents.length; i < ii; i++) {
									if (typeof addressComponents[i].types == "string") {
										type = addressComponents[i].types;
									}
									else if (typeof addressComponents[i].types == "object") {
										type = addressComponents[i].types[0];
									}
									
									if (type == 'postal_code') {
										postalCode = addressComponents[i].long_name;
									}
								}
								
								if (typeof callback === 'function') {
									callback(postalCode);
								}
							}
						}	
					}
				}				
			}, 
			
			submitBuyStoreForm: function(e) {
				var selectVarVal = $('.c-buyStore__select option:selected').val(), postalCode = $('.postalCode').val(),
					actionURL = $('#c-buyStore-form').attr('action'), self = this, actualURL = '';
					self.completeData = self.getModelJSON().productOverview;
					self.currentData = self.completeData.product.productsDetail;					
					self.regExp = self.completeData.product.storeLocator.fields.postCode.regex;
					
				var validZip = self.zipCodeValidation(postalCode, self.regExp);
				
				if(validZip === true) {
					$('.help-block').addClass('hidden');
					$('.postalCode').parents('.form-group').removeClass('has-error');
					$.each(self.currentData, function(i){
						if(self.currentData[i].shortIdentifierValue === selectVarVal){
							actualURL = self.currentData[i].storeLocatorResultUrl;
							return actualURL;
						}
					});					
					$('#c-buyStore-form').attr('action', actualURL);
				} else {
					$('.postalCode').parents('.form-group').addClass('has-error');
					$('.help-block').removeClass('hidden');
					return false;
				}					
			},
			
			zipCodeValidation: function(postalCode, regExp) {
				var regEx = regExp, pattern = new RegExp(regEx), isValidZip = pattern.test(postalCode);
				return isValidZip;
			},
			
			onShow:function()
            { 
            },
            /**************************PRIVATE METHODS******************************************/

            /**
             component private functions are written with '_' prefix to diffrentiate between the public methods
             example:

             _showContent: function (argument) {
                    // body...
                }

             */
            _closebutton: function(){
                    $('.c-product-overview__storewrapper').slideUp( 500, function() {
                        $('.js-btn__form').removeClass('is-active');
                        $( this ).slideUp();
                    });
                },
            _dateformat: function() {
                $('.c-award-date-range').each(function(index){
                    var daterange = $(this).html().split(' - '),
                    newdaterange = commonService.getSubstring(daterange[0],0,10) +" - "+ commonService.getSubstring(daterange[1],0,10);
                    $(this).html(newdaterange);
                });
            },
			
			// etale shop now tracking
			etaleTracking: function(){
                 // Get the parent page URL as it was passed in, for browsers that don't support
                // window.postMessage (this URL could be hard-coded).
                var parent_url = decodeURIComponent( document.location.hash.replace( /^#/, '' ) ),
                 link;

                // The first param is serialized using $.param (if not a string) and passed to the
                // parent window. If window.postMessage exists, the param is passed using that,
                // otherwise it is passed in the location hash (that's why parent_url is required).
                // The second param is the targetOrigin.              
                function postMessage(model, partnum, prodid, proddesc, markettext, ean, upc, etin, retailer) {
                    var message = '{ "model" : "' + model + 
                                    '", "partnum" : "' + partnum + 
                                    '", "prodid" : "' + prodid + 
                                    '", "proddesc" : "' + proddesc + 
                                    '", "markettext" : "' + markettext +
                                    '", "ean" : "' + ean + 
                                    '", "upc" : "' + upc + 
                                    '", "etin" :"' + etin + 
                                    '", "retailer" : " ' + retailer +
                                    '", "date": "' + new Date() + '"}' ; 
                     $.postMessage(message, parent_url, parent );
                }
				$(document).on('click', '.js-etale-bin-button', function() { 
					var ev = {},
						compName = $(this).parents('[data-componentname]').data('componentname'),
						prodName = $('.c-product-overview__title').text(),
						prodId = $('.c-product-overview').data('product-id'),
						prodCat = $('.c-product-overview').data('product-category');

					digitalData.component = [];
					digitalData.component.push({'componentInfo' :{
					   'componentID': compName,
					   'name': compName
					}});

					digitalData.product = [];
					digitalData.product.push({
					   'productInfo' :{
						   'productID': prodId,
						   'productName': prodName
						},
						'category':{
							'primaryCategory': prodCat
						}
					});

					ev.eventInfo={
						'type':ctConstants.trackEvent,
						'eventAction': ctConstants.purchase,
						'eventLabel' : 'Online-' + prodName
					};
					ev.category ={'primaryCategory':ctConstants.conversion};
					digitalData.event.push(ev);
					
					$.receiveMessage(
						function (e) {
							var json = JSON.parse(e.data);
							var retailer = json.proddesc + ' -' + json.retailer;
							var retailername = json.retailer;
							if (typeof retailername !== 'undefined') {
								var ev = {};
								ev.eventInfo={
									'type':ctConstants.trackEvent,
									'eventAction': ctConstants.retailerClick,
									'eventLabel' : 'Online - ' + prodName + ' | ' + retailername
								};
								ev.category ={'primaryCategory':ctConstants.custom};
								digitalData.event.push(ev);
								if(typeof $BV === 'object') {
									$BV.SI.trackConversion({
										'type': 'BuyOnline',
										'label': retailername,
										'value': prodId
									});
								}
							}
						}
					);
				});
			}

        });
    });

    return ProductOverview;
});
