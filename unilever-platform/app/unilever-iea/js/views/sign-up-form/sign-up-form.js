/*global define*/

define(['jquery.validate'], function () {
    'use strict';

    var SignUpForm = IEA.module('UI.sign-up-form', function (signUpForm, app, iea) {

        _.extend(signUpForm, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this._setupForm();
            },

            _setupForm: function () {
                //custom validation rule
                $.validator.addMethod("customemail", 
                function(value, element) {
                    return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
                }, 
                "Sorry, I've enabled very strict email validation"
            );

                this.$el.find('#signupform').validate({
                    rules: {
                        title: 'required',
                        fname: {
                            required: true,
                            minlength: 2
                        },
                        lname: {
                            required: true,
                            minlength: 2
                        },
                        phone:{
                            number: true
                        },
                        email: {
                            required:  {
                                depends:function(){
                                    $(this).val($.trim($(this).val()));
                                    return true;
                                }   
                            },
                            customemail: true
                        },
                        location: 'required',
                        agree: 'required'
                    }/*,
                    messages: {
                        fname: {
                            minlength: 'Please enter at least 2 characters.'
                        },
                        lname: {
                            minLength: 'Please enter at least 2 characters.'
                        }
                    }*/
                });
            }


            /**************************PRIVATE METHODS******************************************/

            /**
             component private functions are written with '_' prefix to diffrentiate between the public methods
             example:

             _showContent: function (argument) {
                    // body...
                }

             */

        });
    });

    return SignUpForm;
});
