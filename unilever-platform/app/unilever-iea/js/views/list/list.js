/*global define*/

define(function() {

    'use strict';

    var List = IEA.module('UI.list', function(module, app, iea) {

        _.extend(module, {
            // component logic goes here
        });
    });

    return List;
});
