/*global define*/

define(function() {
    'use strict';

    var ZipFileDownload = IEA.module('UI.zip-file-download', function(zipFileDownload, app, iea) {

        _.extend(zipFileDownload, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                var self = this, 
                obj = {},
                postURL,
                jsonData=this.getModelJSON().zipFileDownload;
                obj.attachments=jsonData.attachments;
                postURL=jsonData.cta.url ;  

                $('.js-download').on('click', function(){
                    $.ajax({
                    url:postURL ,
                    type: 'POST',
                    data:obj,
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function (data, status, xhr) {
                        
                    },
                    error: function (xhr, status, error) {

                    }
                    });
                })
                
            }


            /**************************PRIVATE METHODS******************************************/          

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */

        });
    });

    return ZipFileDownload;
});
