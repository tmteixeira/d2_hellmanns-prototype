/*global define*/

define(['ratings'], function() {
    'use strict';

    var Reviews = IEA.module('UI.reviews', function(reviews, app, iea) {
        
        var ratingService = null;   
        
        _.extend(reviews, {

            defaultSettings: {
                reviewsWrapper: '.rr-product-reviews'
            },

            events: {
                'click .aggregateRating' : '_scrollToReviews',
                'click .customReview' : '_scrollToReviews',
                'click .tReview' : '_scrollToReviews'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                ratingService = new IEA.ratings(); // creates new instance

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                // Inject rating service
                var self = this,
                    componentJson = self.getModelJSON().reviews,
                    options = {};

                options = {
                    ratingReview: componentJson.review,
                    ratingObj: componentJson,                    
                    brandName: componentJson.brandName,              
                    componentName: componentJson.componentName,
                    componentVariants: componentJson.componentVariation,
                    componentPositions: componentJson.componentPosition,
                    
                };
                if(componentJson.entityType === 'product') {
                    options.productId= componentJson.uniqueId;
                    options.productName= $('.rr-product-info .rr-heading-3').text();
                    options.productCategory= ''; //to do will assign the primary category from product
                }
                else if (componentJson.entityType === 'recipe') {
                    options.recipeId= componentJson.uniqueId;
                    options.recipeName= $('.rr-product-info .rr-heading-3').text();
                }

                ratingService.initRatings(options);

                //Analytics tracking
                setTimeout(function() {
                    self._ctReviews(options);
                },5000);

                this.triggerMethod('enable');
            },

            /**************************PRIVATE METHODS******************************************/          

            // scroll write a review button to review component
            _scrollToReviews: function(evt) {
                var $this = $(evt.target),
                    settings = this.defaultSettings,
                    $reviewsWrapper = $(settings.reviewsWrapper);

                if ($reviewsWrapper.length) {
                    if ($this.closest('.c-product-listing__product-item').length > 0) {
                        var pdpUrl = $this.parents('.o-rating').siblings('.c-product-listing__title').attr('href');
                        
                        if (pdpUrl !== '') {
                            window.location.href = pdpUrl+'#reviews';
                        }
                    } else {                        
                        $('html, body').animate({scrollTop: $reviewsWrapper.offset().top}, 2000);
                    }
                }
            },

            // Analytics for reviews on page load
            _ctReviews: function(options) {
                // KQ rating reviews tracking               
                if (typeof ratingReview != 'undefined') {
                    ratingReview.events = {
                        rrSubmitReview: function (data) {
                            ratingService._ctProductInfo(options, 'reviews');
                        }
                    };
                }
            }

        });
    });

    return Reviews;
});
