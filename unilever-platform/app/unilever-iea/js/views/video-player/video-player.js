/*global define*/

define(['videoplayerService', 'commonService'], function() {
    'use strict';

    var VideoPlayer = IEA.module('UI.video-player', function(videoPlayer, app, iea) {

        var videoplayerService = null,
        commonService = null;

        _.extend(videoPlayer, {

            events: {},

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();                    
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
				this.triggerMethod('beforEnable');

                var self = this;

                self.videoJsonData = self.getModelJSON().videoPlayer;

                this._setupPlayer();
                this._isEnabled = true;
                
                $('.c-video-player__wrap').click(function(e) {
                    self._clickVideoHandler(e);
                });

                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }

            */
            _setupPlayer: function() {
                var options = {};

                options.videoContainerId = 'article-video-player';
                options.view = this;
                options.videoType = this.videoJsonData.videoType;

                commonService = new IEA.commonService();
                videoplayerService = new IEA.videoPlayerService(options);
            },

            onPlayerReady: function() {
                this.events['click .c-video-player__wrap'] = '_clickVideoHandler';
                this.delegateEvents();
            },

            onVideoEnded: function() {
                if (commonService.isMobileAndTablet()) {

                    this.$el.find('.js-video-btn').removeClass('hidden');
                    this.events['click .js-video-btn'] = '_replayVideoHandler';
                    this.delegateEvents();
                }
            },

            _clickVideoHandler: function(evt) {
                var options = {};

                options.videoContainerId = 'article-video-player';
                options.view = this;
                options.videoType = this.videoJsonData.videoType;

                if (evt) {
                    evt.preventDefault();
                }
                if ($('.is-hero-video-open').length) {
                    $('.c-hero-image .is-hero-video').click();
                }
            
                videoplayerService.setupVidepAPI(options);

                this.$el.find('.c-video-player__video').css({'display': 'block'});
                this.$el.addClass('js-video-player__open');

                $('body, html').addClass('is-video-player__open');

                this._isVideoOpened = !this._isVideoOpened;
            },

            _replayVideoHandler: function() {

                videoplayerService.play();

                this.$el.find('.js-video-btn').addClass('hidden');
                delete this.events['click .js-video-btn'];
                this.delegateEvents();
            },

            _progressTrack:function(progress,videoId){
                 var ev = {},
					 compName = $('.c-video-player__wrap').data('componentname'),  compVar = $('.c-video-player__wrap').data('component-variants'), compPos = $('.c-video-player__wrap').data('component-positions');
				
				digitalData.component = [];
                digitalData.component.push({
					'componentInfo' :{
						'componentID': compName,
						'name': compName
					},
					'attributes': {
						'position': compPos,
						'variants': compVar
					}
				});
				
				ev.eventInfo={
				  'type':ctConstants.trackEvent,
				  'eventAction': ctConstants.videoProgress,
				  'eventLabel' : 'VIDEO PROGRESS '+progress
				};
				ev.attributes={'nonInteraction':1};
				ev.category ={'primaryCategory':ctConstants.custom};
				digitalData.event.push(ev);
            },

            _ctTag: function(videoName, videoId, play) {
                var ev = {},
                compName = $('.c-video-player__wrap').data('componentname'),  compVar = $('.c-video-player__wrap').data('component-variants'), compPos = $('.c-video-player__wrap').data('component-positions');
                
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    digitalData.component = [];
                    digitalData.video = [];

                    digitalData.component.push({
						'componentInfo' :{
							'componentID': compName,
							'name': compName
						},
						'attributes': {
							'position': compPos,
							'variants': compVar
						}
					});

                    digitalData.video.push({
                        'videoId':videoId
                    });

                    if (play === true) {
                        ev.eventInfo={
                            'type':ctConstants.trackEvent,
                            'eventAction':ctConstants.videoPlays,
                            'eventLabel':videoName
                        };
                    } else {
                        ev.eventInfo={
                            'type':ctConstants.trackEvent,
                            'eventAction':ctConstants.videoCompletes,
                            'eventLabel':videoName
                        };
                    }
                    ev.category = {'primaryCategory':ctConstants.other};
                    digitalData.event.push(ev);
                }
            }
        });
    });

    return VideoPlayer;
});
