/*global define*/

define(['slick', 'commonService', 'ratings', 'binWidgetService', 'socialSharing'], function() {
    'use strict';

    var DiagnosticTool = IEA.module('UI.diagnostic-tool', function(diagnosticTool, app, iea) {
        
        var commonService = null,
            ratingService = null,
            binWidgetService = null,
            socialService = null;
        

        _.extend(diagnosticTool, {
            
                defaultSettings: {
                    ansItem: '.c-res',
                    bgItem:'.c-res__bgimg-wrap',
                    currentQueNum: '.js-questionnaire-que__current',
                    emailWrap: '.c-result-sec-email',
                    emailContent: '.c-result-sec-email__content',
                    emailSuccessNotify: '.c-result-sec-email__success',
                    emailFailureNotify: '.c-result-sec-email__failure',
                    emailInput: '.c-result-sec-email__form-email',
                    introPanel: '.js-intro-panel',
                    nextBtn: '.js-questionnaire__next-btn',
                    prevBtn: '.js-questionnaire__prev-btn',
                    productItem: '.js-products-item',
                    queItem: '.js-question',
                    questionCounter: 0,
                    questionnairePanel: '.js-questionnaire-panel',
                    quesWrapper: '.js-questionnaire__content',
                    result: '.c-result',
                    resultsContent: '.c-results',
                    resultImage: '.c-result-image',
                    resultsPanel: '.js-results-panel',
                    
                    // Templates
                    emailTemplate:'diagnostic-tool-result-section/diagnostic-tool-email-view',
                    productsSummaryTemplate: 'diagnostic-tool-result-section/diagnostic-tool-products-summary.hbss',
                    socialSharingTemplate: 'diagnostic-tool-result-section/diagnostic-tool-social-sharing.hbss',
                    textArticleView: 'diagnostic-tool-result-section/diagnostic-tool-text-articles-view.hbss',
                    textProductView: 'diagnostic-tool-result-section/diagnostic-tool-text-products-view.hbss',
                    textProductArticleView: 'diagnostic-tool-result-section/diagnostic-tool-text-products-articles-view.hbss',
                    textTemplate: 'diagnostic-tool-result-section/diagnostic-tool-text.hbss',
                    
                    // Bin
                    binButtonCTA: '.js-bin-btn',
                    binOvarlay: '.bin-widget-overlay',
                    
                    //email
                    emailImgSize: '.ulenscale.180x180.jpg'
                },
            
                defaultCarosuelSettings : {
                    container: '.js-articles-container',
                    carousel: '.js-articles-carousel',
                    carouselSlide: '.js-articles-item',  
                    carouselPlaceholder: 'js-articles-carousel-placeholder', 
                    
                    firstLoad: true,
                    
                    default: {
                        carouselTabP: {
                            infinite: true,
                            speed: 1250,
                            arrows: true,
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            focusOnSelect: true,
                            dots: true
                        },
                        carouselMob: {
                            infinite: true,
                            speed: 1250,
                            slidesToShow: 1,
                            focusOnSelect: true,
                            centerMode: true,
                            centerPadding: '25%',
                            dots: true
                        }
                    }
                },
            
                events: {
                    'click .js-intro__start-btn' : '_startQuestionnaire',
                    'click .js-questionnaire__prev-btn' : '_navigateToQue',
                    'click .js-questionnaire__next-btn' : '_navigateToQue',
                    'click .js-questionnaire__start-again-btn' : '_resetQuestionnaire',
                    'click .js-dt-social-share' : '_enableSocialSharing'
                },
            
            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                
                commonService = new IEA.commonService(); // creates new instance for common service
                ratingService = new IEA.ratings(); // creates new instance for ratings
                binWidgetService = new IEA.BinWidget(); // creates new instance for bin widget service
                socialService = new IEA.socialsharing(); // creates new instance for bin widget service
                
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');
                
                var self = this;
                
                self.settings = self.defaultSettings;
                
                self.articlesCarouselSettings = self.defaultCarosuelSettings;
                
                self.productsCarouselSettings = $.extend(true, self.productsCarouselSettings, self.defaultCarosuelSettings, {
                    container: '.js-products-container',
                    carousel: '.js-products-carousel',
                    carouselSlide: '.js-products-item',  
                    carouselPlaceholder: 'js-products-carousel-placeholder'
                });
                
                self.singleProductsCarouselSettings = $.extend(true, self.singleProductsCarouselSettings, self.defaultCarosuelSettings, {
                    container: '.js-single-products-container',
                    carousel: '.js-single-products-carousel',
                    carouselSlide: '.js-single-products-item',  
                    carouselPlaceholder: 'js-single-products-carousel-placeholder',  

                    default: {
                        carouselTabP: {
                            slidesToShow: 1
                        },
                        carouselMob: {
                            slidesToShow: 1,
                            centerMode: false
                        }
                    }
                });
                
                self.componentJson = self.getModelJSON().diagnosticTool;
                self.elm = self.$el;
                self.responseArray = [];
                self.summaryProductsArray = [];
                self.totalQues = self.componentJson.questions.length;
                
                self._setProgressBarWidth(1, self.componentJson.questions.length);
                if(!self.componentJson.introductionPanel) {
                    self._ifSectionDivider(self.elm.find(self.settings.queItem).eq(0));
                }
                self.isMobile = commonService.isMobile() ? true: false;
                self._checkCookie();
                
                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/          

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:
            */
            
            // Check if question type is section divider 
            _ifSectionDivider: function($queElm) {
                var self = this;
                if($queElm.hasClass('c-section-divider') && $queElm.data('delay-interval')) {
                   self._startTimer($queElm);
                }
            },
            
            /**
             * check if cookie with questions answered exist
             * get data from cookie and post it to get result section data
             */
            _checkCookie:function() {
                var self = this,
                    options = {},
                    responseCookieObject;

                if(commonService._getCookie('questionsStore')){

                    responseCookieObject = commonService._getCookie('questionsStore');
                    var jsonObject = JSON.stringify(JSON.parse(responseCookieObject));

                    options = {
                        queryParams:jsonObject,
                        servletUrl:self.componentJson.getResponseUrl,
                        isTypePost:true,
                        // callback to display result section on questionnaire submit
                        onCompleteCallback: function(data) {
                            $(self.settings.resultsPanel).addClass('active-panel');
                            self.sectionsResultResponse = data;
                            self._createResultsSections(data);
                        }
                    };

                    commonService.getDataAjaxMethod(options);
                } else {
                    self._setProgressBarWidth(1, self.componentJson.questions.length);
                    if(self.componentJson.introductionPanel) {
                        $(self.settings.introPanel).addClass('active-panel');
                    } else {
                        self._startQuestionnaire();
                    }
                }
            },
            
            _makeSectionForEmail:function(val) {
                var self = this,
                    generalConfiguration = {},
                    sectionObj = {},
                    mergedProductsArray=[],
                    totalProductsArray=[],
                    mergedArticlesArray=[],
                    totalArticlesArray=[];
                
                generalConfiguration.headingText = val.generalConfiguration.headingText ? val.generalConfiguration.headingText : '';
                generalConfiguration.subHeadingText = val.generalConfiguration.subHeadingText ? val.generalConfiguration.subHeadingText : '';
                generalConfiguration.longSubHeadingText = val.generalConfiguration.longSubHeadingText ? val.generalConfiguration.longSubHeadingText : '';
                generalConfiguration.personalisedResponse = val.personalisedResponse ? val.personalisedResponse.text : '';

                if(!val.articlesSection && !val.productsSection) {
                    sectionObj.generalConfiguration = generalConfiguration;
                    sectionObj.products = null;
                    sectionObj.articles = null;   
                }
                
                if(val.articlesSection && !val.productsSection) {
                    mergedArticlesArray = self._mergeArticlesArray(val);
                    $.each(mergedArticlesArray, function(k,v) {
                        totalArticlesArray.push({
                            title: v.title,
                            imgSrc: v.image.path,
                            ctaSrc: v.url
                        });
                    });
                    sectionObj.generalConfiguration = generalConfiguration;
                    sectionObj.products = null;
                    sectionObj.articles = totalArticlesArray; 
                }
                
                if(val.productsSection && !val.articlesSection) {
                    mergedProductsArray = self._mergeProductsArray(val);
                    if(mergedProductsArray.length){
                        $.each(mergedProductsArray, function(k,v) {
                            totalProductsArray.push({
                                title: v.title,
                                imgSrc: v.productsDetail[0].images[0].path + self.settings.emailImgSize,
                                rating:'',
                                ctaSrc: v.url,
                                ctaText: v.productSelectCtaLabel,
                                description:v.description
                            });
                        });
                    } 
                    sectionObj.generalConfiguration = generalConfiguration;
                    sectionObj.products = totalProductsArray;
                    sectionObj.articles = null;
                    self.summaryProductsArray.push(totalProductsArray);
                }
                
                if(val.productsSection && val.articlesSection) {
                    mergedArticlesArray = self._mergeArticlesArray(val);
                    mergedProductsArray = self._mergeProductsArray(val);

                    if(mergedProductsArray.length){
                        $.each(mergedProductsArray, function(k,v) {
                            totalProductsArray.push({
                                title: v.title,
                                imgSrc: v.productsDetail[0].images[0].path + self.settings.emailImgSize,
                                rating:'',
                                ctaSrc: v.url,
                                ctaText: v.productSelectCtaLabel,
                                description:v.description
                            });
                        });
                    }
                    if(mergedArticlesArray.length){
                        $.each(mergedArticlesArray, function(k,v) {
                            totalArticlesArray.push({
                                title: v.title,
                                imgSrc: v.image.path,
                                ctaSrc: v.url
                            });
                        });
                    }  
                    sectionObj.generalConfiguration = generalConfiguration;
                    sectionObj.products = totalProductsArray;
                    sectionObj.articles = totalArticlesArray;
                    self.summaryProductsArray.push(totalProductsArray);
                }
                return sectionObj;
            },
            
            _createEmailObject:function() {
                var self = this,
                    resultData,
                    sections=[],
                    options={},
                    mergedSummaryProductsArray = [];
                
                if(self.sectionsResultResponse){
                    resultData = $.parseJSON(self.sectionsResultResponse).data;
                    
                    $.each(resultData.sections, function(key, val) {
                        sections.push(self._makeSectionForEmail(val));
                    });
                    
                    $(self.summaryProductsArray).each(function(k, v) {
                        $.merge(mergedSummaryProductsArray, v);
                    });
                    
                    $('#emailResultForm').on('submit',function(e) {
                        var $this = $(e.target),
                            emailId = $('#emailResultForm').find('input[type="email"]').val();
                        
                        if(emailId){
                            var dataToSend = {
                                brand: self.componentJson.brandName,
                                locale: self.componentJson.locale,
                                entity: self.componentJson.entity,
                                data: {
                                    emailid: emailId,
                                    readLabel: resultData.static.readLabel,
                                    sections: sections,
                                    summaryProducts: mergedSummaryProductsArray
                                }
                            };
                            
                            options = {
                                queryParams: JSON.stringify(dataToSend),
                                servletUrl: self.componentJson.emailConfig.sendEmailUrl,
                                isTypePost: true,
                                // callback to display success message after sending email
                                onCompleteCallback: function(data) {
                                    $(self.settings.emailSuccessNotify).parent().find(self.settings.emailContent).hide(0,function() {
                                        var $emailElm = $this.parents(self.settings.emailWrap),
                                            $succesElm = $emailElm.find(self.settings.emailSuccessNotify),
                                            $failureElm = $emailElm.find(self.settings.emailFailureNotify),
                                            $emailInputVal =  $emailElm.find(self.settings.emailInput).val();

                                        $failureElm.hide();
                                        $succesElm.html($succesElm.html().replace('placeholderForUserEmail', $emailInputVal));
                                        $succesElm.show();
                                        self._trackEmail($this);
                                    });                              
                                }
                            };
                                
                            commonService.getDataAjaxMethod(options);
                            e.preventDefault(); 
                        }
                    });
                }
            },
            // Reset the questionnaire back to the first question on clicking start again and remove all the selected answers.
            _resetQuestionnaire: function(evt) {
                var self = this,
                    $this = $(evt.target),
                    questionCounter,
                    $queWrapper = self.elm.find(self.settings.quesWrapper);
                
                self._trackDiagToolCta($this);
                self._setProgressBarWidth(1, self.componentJson.questions.length);
                if(self.componentJson.introductionPanel) {
                    $(self.settings.introPanel).addClass('active-panel');
                    $(self.settings.questionnairePanel).removeClass('active-panel');
                } else {
                    $queWrapper.find(self.settings.queItem).eq(0).addClass('que-active');
                }
                
                $queWrapper.find(self.settings.queItem).removeClass('que-active');
                $queWrapper.find(self.settings.nextBtn).attr('disabled');
                $queWrapper.find(self.settings.ansItem).removeClass('res-active');
                $queWrapper.find(self.settings.queItem).find('.c-res__bgimg-wrap').removeClass('img-active');

                $queWrapper.find('input:radio').removeAttr('checked');
                $queWrapper.find('input:checkbox').removeAttr('checked');
                
                $.each(self.componentJson.questions, function(i,v){
                    var $questionItem = $queWrapper.find(self.settings.queItem).eq(i);
                    if(v.displayType === 'single-choice-scale'){
                        $questionItem.find('.c-res:first').find('input').prop('checked', true);
                        $questionItem.find('.c-res__bgimg-wrap:first').addClass('img-active');
                        $questionItem.find('.c-res:first').addClass('res-active');
                    }
                    
                    if(v.displayType !== 'single-choice-scale' && v.displayType !== 'section-divider' ){
                        $questionItem.attr('data-answered','false');
                    }
                });

                self.settings.questionCounter = 0;

                questionCounter = self.settings.questionCounter;
                if($(self.settings.currentQueNum).length) {
                    $(self.settings.currentQueNum).text(self.componentJson.questions[questionCounter].stepIndicator);
                }

                self.responseArray = [];
                if(self.componentJson.introductionPanel) {
                    self._ifSectionDivider(self.elm.find(self.settings.queItem).eq(0));
                }
                clearInterval(self.timer);
            },
            
            // create the json object to be posted to get the result section data
            _createResponseArray: function($currIndex, $queId, ansArray) {
                var self = this,
                    resObj = {};
                
                resObj.questionId = $queId;
                resObj.answers = ansArray;
                self.responseArray[$currIndex] = resObj;
                
            },
            
            // create the answer array per question 
            _createAnsArray: function($currentQueElm) {
                var self = this,
                    $currIndex = $currentQueElm.index(),
                    ansElms = $currentQueElm.find('.res-active').find('input'),
                    ansArray = [],
                    $queId = $currentQueElm.data('que-id').toString(),
                    resId;
                
                ansElms.each(function(key, val) {
                    resId = $(this).parents('.c-res').data('res-id').toString();
                    ansArray.push({'id': resId});
                });
                
                self._createResponseArray($currIndex, $queId, ansArray);
            },
            
            // Set classes for questions answered and answers selected
            _setAnsweredQues: function() {
                var self = this,
                    $queElm,
                    $ansElm,
                    $imageContainer;
                    
                self.elm.find('input').off('change').on('change', function() {

                    $queElm = $(this).parents(self.settings.queItem);
                    $ansElm = $(this).parents(self.settings.ansItem);
                    $imageContainer = $queElm.find('.c-res-wrap').find('.c-res-wrap__image-container');
                    
                    if($(this).attr('type')==='checkbox') {
                        $(this).is(':checked') ? $ansElm.addClass('res-active') : $ansElm.removeClass('res-active');
                        if($queElm.find('input.checkbox:checked').length) {
                            $queElm.attr('data-answered', 'true');
                            $queElm.find(self.settings.nextBtn).removeAttr('disabled');
                        } else {
                            $queElm.find(self.settings.nextBtn).attr('disabled', true);
                        }
                        //$queElm.attr('data-answered', $queElm.find('input.checkbox:checked').length > 0);
                    } else if($(this).attr('type')==='radio'){
                        if($(this).is(':checked')) {
                            var currentIndex = $(this).parent(self.settings.ansItem).index();
                            if($imageContainer.length>0){
                                $imageContainer.find('.c-res__bgimg-wrap').removeClass('img-active');
                                $imageContainer.find('.c-res__bgimg-wrap').eq(currentIndex).addClass('img-active');
                            }

                            $queElm.attr('data-answered', 'true');
                            $queElm.find(self.settings.nextBtn).removeAttr('disabled');
                            $queElm.find(self.settings.ansItem).removeClass('res-active');
                            $ansElm.addClass('res-active');
                        }
                    }
                    
                    if(!$queElm.find(self.settings.nextBtn).length) {
                        self._nextQueHandler($queElm);
                    }
                });
            },
            
            // start questionnaire from intro panel start button
            _startQuestionnaire: function(evt) {
                var self = this,
                    $this = evt ? $(evt.target) : null,
                    $introPanel = self.elm.find(self.settings.introPanel),
                    $questionnairePanel = self.elm.find(self.settings.questionnairePanel),
                    $queWrapper = self.elm.find(self.settings.quesWrapper),
                    $firstQue = $questionnairePanel.find(self.settings.queItem).eq(0);
                $introPanel.removeClass('active-panel');
                if($questionnairePanel.find(self.settings.prevBtn).hasClass('show-prev')) {
                    $(self.settings.prevBtn).removeClass('show-prev');
                }
                $questionnairePanel.addClass('active-panel');
                $firstQue.addClass('que-active');
                self._ifSectionDivider($firstQue);
                if($(self.settings.currentQueNum).length) {
                    $(self.settings.currentQueNum).text(self.componentJson.questions[0].stepIndicator);
                }
                self._setAnsweredQues();
                evt && self._trackDiagToolCta($this);
            },
            
            // progress bar to display progress of questionnaire
            _setProgressBarWidth: function(questionCounter, totalQues) {
                var self = this,
                    progressWidth = 0,
                    $progressBar = self.elm.find('.c-questionnaire__progress-bar');
                
                progressWidth = (questionCounter/totalQues) * 100 + '%';
                $progressBar.css('width', progressWidth);
            },
            
            // check if question has been answered using data attribute set
            _ifAnswered: function(questionCounter) {
                var self = this,
                    $currentQue = self.elm.find(self.settings.queItem).eq(questionCounter);
                
                if($currentQue.attr('data-answered') === 'true') {
                    return true;
                } else {
                    return false;
                }
            },
            
            // resize carousel depending on the screen
            _setCarouselSize: function ($carousel, settings) {
                var self = this,
                    $slideItems = $(settings.carouselPlaceholder).find(settings.carouselSlide);
                //remove slick if it has been loaded
                if (settings.firstLoad) {
                    settings.firstLoad = false;
                } else {
                    $carousel.slick('unslick');
                }
                
                if(self.isMobile) {
                    $carousel.slick(settings.default.carouselMob);
                } else {
                    $carousel.slick(settings.default.carouselTabP);
                }
                
                //scale the middle selected slide and got to the middle
                $carousel.slick('slickGoTo', 1);
            },
            
            // initalise carousel
            _carouselInit: function (settings) {
                var self = this,
                    $carousel = $(settings.carousel);
                setTimeout(function () {
                    self._setCarouselSize($carousel, settings);
                    $carousel.slick('slickGoTo', 1);
                    $carousel.slick(settings.default.carouselTabP);
                }, 100);
            },
            
            init: function(settings) {
                //cloned the carousel slides for the desktop & tablet version
                var string = $(settings.carousel).html();
            },
            
            _bindCarousel: function(settings) {
                var self = this;
                setTimeout(_.bind(function () {
                    if (self.elm.find(settings.container).attr('data-type') === 'carousel') {

                        self.init(settings);
                        self._carouselInit(settings);
                    }
                }, self), 100);
            },
            
            // merge products array provided in different products section of the result section json
            _mergeProductsArray: function(val) {
                var productsArray = [];
                $(val.productsSection.productListSections).each(function(k, v) {
                    $.merge(productsArray, v.products);
                });
                
                return productsArray;
            },
            
            // merge articles array provided in different articles section of the result section json
            _mergeArticlesArray: function(val) {
                var articlesArray = [];
                $(val.articlesSection.articleListSections).each(function(k, v) {
                    $.merge(articlesArray, v.articles);
                });
                
                return articlesArray;
            },
            
            _enableSocialSharing: function(evt) {
                var self = this,
                    $this = $(evt.currentTarget),
                    mainObj = self.componentJson.socialSharing,
                    shareObj = {},
                    appId = $this.data('app-id'), //TODO: Pick it up from json
                    appUrl = '//connect.facebook.net/en_US/sdk.js';  //TODO: Pick it up from json
                    
                shareObj = {
                    postLink: window.location.href,
                    postName: mainObj.sharingTitle,
                    postImage: mainObj.sharingImageMap.path,
                    postDescriptionFacebook: mainObj.sharingPrefix + ' ' + mainObj.personalisedTextFrom + ' ' + mainObj.sharingSuffix,
                    postDescriptionTwitter: mainObj.sharingTitle + ' ' + mainObj.sharingPrefix + ' ' + window.location.href,
                    shareVia: $this.attr('title')
                };
                
                socialService._loadFacebookSdk(appId, appUrl);
                socialService._customSocialShare(appId, shareObj);
                self._trackSocialShare($this, shareObj.shareVia, shareObj.postName);
            },
            
            // Create results section from the response of the posted question ans data 
            _createResultsSections: function(data) {
                var self = this,
                    resultData = $.parseJSON(data).data,
                    textTemplate = self.getTemplate('diagnostic-tool-text', self.settings.textTemplate),
                    productsSummaryTemplate = self.getTemplate('diagnostic-tool-products-summary', self.settings.productsSummaryTemplate),
                    socialSharingTemplate = self.getTemplate('diagnostic-tool-social-sharing', self.settings.socialSharingTemplate),
                    emailTemplate = self.getTemplate('diagnostic-tool-email-view', self.settings.emailTemplate),

                    textArticleView = self.getTemplate('diagnostic-tool-text-articles-view', self.settings.textArticleView),
                    textProductView = self.getTemplate('diagnostic-tool-text-products-view', self.settings.textProductView),
                    textProductArticleView= self.getTemplate('diagnostic-tool-text-products-articles-view', self.settings.textProductArticleView),


                    $currentPanel = self.elm.find(self.settings.questionnairePanel),
                    $nextPanel = self.elm.find(self.settings.resultsPanel),
                    allproductsList = [],
                    allMergedProductsList = [],
                    articlesList = [],
                    resultSectionsHtml = '',
                    productsList = [];
                
                $currentPanel.removeClass('active-panel');
                $nextPanel.addClass('active-panel');
                
                $.each(resultData.sections, function(key, val) {
                    if(!val.articlesSection && !val.productsSection) {
                        val.enablePersonalisedTextImage = true;
                        resultSectionsHtml += textTemplate(val);
                    }
                    
                    if(val.articlesSection && !val.productsSection) {
                        val.readLabel = resultData.static.readLabel;
                        
                        articlesList = self._mergeArticlesArray(val);
                        if(articlesList.length >= 3) {
                            val.enableArticlesCarouselV1 = true;
                        }
                        
                        resultSectionsHtml += textArticleView(val);
                    }
                    
                    if(val.productsSection && !val.articlesSection) {
                        val.shopNow = resultData.shopNow;
                        val.review = resultData.review;
                        
                        productsList = self._mergeProductsArray(val);
                        allproductsList.push(productsList);
                        
                        if(productsList.length >= 3) {
                            val.enableProductsCarouselV1 = true;
                        }
                        
                        resultSectionsHtml += textProductView(val);
                    }
                    
                    if(val.productsSection && val.articlesSection) {
                        val.shopNow = resultData.shopNow;
                        val.review = resultData.review;
                        val.readLabel = resultData.static.readLabel;
                        
                        articlesList = self._mergeArticlesArray(val);
                        if(articlesList.length >= 3) {
                            val.enableArticlesCarouselV1 = true;
                        }
                        
                        productsList = self._mergeProductsArray(val);
                        allproductsList.push(productsList);
                        if(productsList.length > 1) {
                            val.enableProductsCarouselV1 = true;
                        }
                        
                        resultSectionsHtml += textProductArticleView(val);
                    }
                
                    if(self.componentJson.socialSharing.socialSharing.isEnabled) {
                        resultSectionsHtml += socialSharingTemplate(self.componentJson.socialSharing);
                    }
                });
                
                if(allproductsList.length) {
                    $(allproductsList).each(function(k, v) {
                        $.merge(allMergedProductsList, v);
                    });
                }
                
                if(allMergedProductsList.length) {
                    var allMergedProductsListObj = {};
                    allMergedProductsListObj.summaryProducts = allMergedProductsList;
                    allMergedProductsListObj.review = resultData.review;
                    allMergedProductsListObj.productSummary = self.componentJson.productSummary;
                    
                    if(allMergedProductsList.length >= 3) {
                        allMergedProductsListObj.enableProductsCarouselV1 = true;
                    }
                    resultSectionsHtml += productsSummaryTemplate(allMergedProductsListObj);
                }
                //email configuration
                if(self.componentJson.emailConfig.enableEmail) {
                    var emailContentObj = {
                        emailConfig: self.componentJson.emailConfig,
                        static: self.componentJson.static
                    }
                    resultSectionsHtml += emailTemplate(emailContentObj);
                }

                $(self.settings.resultsContent).append(resultSectionsHtml);
                
                // reload images
                commonService.imageLazyLoadOnPageScroll();
				var throttledLazyLoad = _.throttle(commonService.imageLazyLoadOnPageScroll, 150);
				$(window).on('scroll.lazyLoad', throttledLazyLoad); // Window scroll lazy load
                
                if(self.elm.find(self.singleProductsCarouselSettings.carousel).length > 0) {
                    self._bindCarousel(self.singleProductsCarouselSettings);
                }
                
                if(self.elm.find(self.articlesCarouselSettings.carousel).length > 0) {
                    self._bindCarousel(self.articlesCarouselSettings);
                }
                
                if(self.elm.find(self.productsCarouselSettings.carousel).length > 0) {
                    self._bindCarousel(self.productsCarouselSettings);
                }
                
                if(allMergedProductsList.length) {
                    var options = {};

                    options.ratingReview = resultData.review;

                    options.productId = allMergedProductsList;
                    options.brandName = resultData.brandName;
                    options.ratingObj = self.ratings;
                    ratingService.initRatings(options);
                    
                }
                
                if(self.componentJson.emailConfig.enableEmail) {
                    //bind email submit handler
                    self._createEmailObject();
                }
                
                self._binWidgetInit(resultData);
                self._handleBinButton();
                
                // Product impression tracking
                self._productImpressionTag('.c-products', '.js-products-item');
                self._productImpressionTag('.c-single-products', '.js-single-products-item');
            },
            
            // save results data and question answer data through DCS service
            _saveResponseData: function(data) {
                var self = this,
                    sectionsArray = [],
                    options = {},
                    resultData = $.parseJSON(data).data;
                    
                $.each(resultData.sections, function(key, val) {
                    var sectionObj = {};
                    
                    if(val.productsSection) {
                        var totalProductsArray = [],
                            mergedProductsArray = self._mergeProductsArray(val);
                        
                        $.each(mergedProductsArray, function(k,v) {
                            totalProductsArray.push({
                                productID: v.productID,
                                primaryCategory: v.primaryCategory,
                                description: v.description,
                                title: v.title,
                                image: v.productsDetail[0].images[0].path
                            });
                        });
                        
                        sectionObj.products = totalProductsArray;
                    }
                    
                    if(val.articlesSection) {
                        var totalArticlesArray = [],
                            mergedArticlesArray = self._mergeArticlesArray(val);
                        
                        $.each(mergedArticlesArray, function(k,v) {
                            totalArticlesArray.push({
                                title: v.title,
                                url: v.url,
                                copyText: v.copyText,
                                longCopy: v.longCopy,
                                image: v.image.path,
                                author: v.author,
                                publishDate: v.publishDate 
                            });
                        });  
                        
                        sectionObj.articles = totalArticlesArray;
                    }
                    
                    if(val.questionsToSummarise) {
                        var questionsToSummarise = val.questionsToSummarise;
                        sectionObj.questionsToSummarise = questionsToSummarise;
                    }
                    
                    if(val.personalisedResponse) {
                        var personalisedResponse = val.personalisedResponse;
                        
                        sectionObj.personalisedResponse = {
                            text: val.personalisedResponse.text,
                            image: val.personalisedResponse.image.path
                        };
                    }
                    
                    sectionsArray.push(sectionObj);
                });

                var dataToSave = {
                    brand: self.componentJson.brandName,
                    locale: self.componentJson.locale,
                    entity: self.componentJson.entity,
                    data: {
                        questions: self.responseArray,
                        sections: sectionsArray
                    }
                };
                
                options = {
                    queryParams: JSON.stringify(dataToSave),
                    servletUrl: self.componentJson.postResponseUrl,
                    isTypePost: true,
                    onCompleteCallback: function(data) {
                        console.log(data.status);
                    }
                };
                
                commonService.getDataAjaxMethod(options);
            },
            
            // Submit questionnaire and get the result section data using post request
            _submitQuestionnaire: function(elm) {
                var self = this,
                    responseArrayJson,
                    jsonObject,
                    options = {};
                
                clearInterval(self.timer);
                self._trackDiagToolCta(elm);
                responseArrayJson = {'data':self.responseArray};
                jsonObject = JSON.stringify(responseArrayJson);
                
                options = {
                    queryParams: jsonObject,
                    servletUrl: self.componentJson.getResponseUrl,
                    isTypePost:true,
                    // callback to display result section on questionnaire submit
                    onCompleteCallback: function(data) {
                        commonService._createCookie('questionsStore',jsonObject);
                        
                        $.parseJSON(data).data.sections.length ? self._trackQuestionnaireSubmission('Diag Tool Questionnaire Submitted') : self._trackQuestionnaireSubmission('Diag Tool Questionnaire Submitted - No Recommendation');
                        
                        self._createResultsSections(data);
                        self._saveResponseData(data);
                    }
                };
                
                commonService.getDataAjaxMethod(options);
            },
            
            // Timer functionality for section divider type question
            _startTimer: function($elm) {
                var self = this,
                    delayInterval = $elm.data('delay-interval'),
                    counter = delayInterval + 1,
                    $timerElm = $elm.find('.c-que__timer'),
                    queIndex = $elm.index() + 1;
                
                clearInterval(self.timer);
                $timerElm.text(delayInterval);
                $timerElm.addClass('show-timer');
                
                if(delayInterval !== 0) {
                    self.timer = setInterval(function() {
                        counter--;
                        $timerElm.text(counter);
                        if (counter === 0) {
                            if(queIndex == self.totalQues) {
                                self._createAnsArray($elm);
                                self._submitQuestionnaire($elm);
                            } else {
                                self._nextQueHandler($elm);
                            }
                        }
                    }, 1000);
                }
            },
            
            // move to next question
            _nextQueHandler: function(navElm) {
                var self = this,
                    totalQues = self.totalQues,
                    questionCounter = self.settings.questionCounter,
                    $queElm = $(self.settings.questionnairePanel).find(self.settings.queItem);
                
                clearInterval(self.timer);
                
                if(self._ifAnswered(questionCounter)) {
                    self._createAnsArray($queElm.eq(questionCounter));
                    if(questionCounter < totalQues - 1) {
                        self._trackDiagToolCta(navElm);
                        $queElm.eq(questionCounter).removeClass('que-active');
                        questionCounter = questionCounter + 1;
                        if($(self.settings.currentQueNum).length) {
                            $(self.settings.currentQueNum).text(self.componentJson.questions[questionCounter].stepIndicator);
                        }
                        $(self.settings.prevBtn).addClass('show-prev');
                        $queElm.eq(questionCounter).addClass('que-active');
                        self._setProgressBarWidth(questionCounter + 1, totalQues);
                        self._ifSectionDivider($queElm.eq(questionCounter));
                        self.settings.questionCounter = questionCounter;
                    } else {
                        self._submitQuestionnaire(navElm);
                    }
                }
            },
            
            // move to previous question
            _prevQueHandler: function(navElm) {
                var self = this,
                    totalQues = self.totalQues,
                    questionCounter = self.settings.questionCounter,
                    $queElm = $(self.settings.questionnairePanel).find(self.settings.queItem);
                
                clearInterval(self.timer);
                self._trackDiagToolCta(navElm);
                
                if(questionCounter > 0) {
                    $queElm.eq(questionCounter).removeClass('que-active');
                    if($(self.settings.currentQueNum).length) {
                        $(self.settings.currentQueNum).text(self.componentJson.questions[questionCounter-1].stepIndicator);
                    }
                    self._setProgressBarWidth(questionCounter, totalQues);
                    questionCounter = questionCounter - 1;
                    if(questionCounter === 0) {
                        $(self.settings.prevBtn).removeClass('show-prev');
                    }
                    $queElm.eq(questionCounter).addClass('que-active');
                    self.settings.questionCounter = questionCounter;
                    self._ifSectionDivider($queElm.eq(questionCounter));
                }
            },
            
            // next and previous question handler
            _navigateToQue: function(evt) {
                evt.preventDefault();
                var self = this,
                    $this = $(evt.target);
                
                if ($this.data('role') === 'next' && !$this.attr('disabled')) {
                    self._nextQueHandler($this);
                } else if ($this.data('role') === 'prev') {
                    self._prevQueHandler($this);
                }
            },
            
            /**
            @bin widget init
            @method _binWidgetInit
            @return
            **/

            _binWidgetInit: function(resultData) {
                var self = this;
                
                //intiate Bin Widget
                var options = {
                    shopNow: resultData.shopNow,
                    modalCl: '.js-modal-etale'
                };

                if (options.shopNow) {
                    binWidgetService.injectBinJS(options);
                    self._binWidgetHandler(options);
                    $('.icon-cross').attr('brand', self.componentJson.brandName);
                }               
            },
            
            /**
            @bin widget handler
            @method _binWidgetHandler
            @return 
            **/

            _binWidgetHandler: function(options) {
                var self = this,
                    productId;

                self.elm.find(self.defaultSettings.binButtonCTA).click(function(evt) {
                    evt.preventDefault();
                    var $this = $(evt.target);

                    if(options.shopNow.serviceProviderName === 'cartwire') {
                        loadsWidget(options.productId, this,'retailPopup','en');
                    
                    } else if(options.shopNow.serviceProviderName === 'etale') {
                        $this.next(self.defaultSettings.binOvarlay).show();
                    }
                });
                
                $('.js-btn-close').click(function(evt) {
                    evt.preventDefault();
                    $(self.defaultSettings.binOvarlay).hide();
                });
            },
            
            _handleBinButton: function() {
                var self = this;
                
                $('.js-bin-btn').click(function() {
                    self._trackShopNow(this);
                });    
            },
            
            _trackComponentInfo: function(obj, authorName) {
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    if (obj) {
                        var $obj = obj,
                            $this = ($obj.data('componentname') !== undefined && $obj.data('componentname') !== '') ?
                                    $obj : (($obj.parents('[data-componentname]').data('componentname') !== '') ?
                                    $obj.parents('[data-componentname]') :
                                    $obj.parents('[data-componentname]').parents('[data-componentname]')), // get the correct object which has the analytics data attributes
                            compName = $this.data('componentname'),
                            compVar = $this.data('component-variants'),
                            compPos = $this.data('component-positions');

                        digitalData.component = [];
                        digitalData.component.push({
                            'componentInfo' :{
                                'name': compName
                            },
                            'attributes': {
                                'position': compPos,
                                'variants': compVar,
                                'author': authorName ? authorName : ''
                            }
                        });
                    }
                }
            },
            
            _trackSocialShare: function(shareBtnElm, service, title) {
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    var self = this,
                        $compObj = $(shareBtnElm).parents('.c-diagnostic-tool'),
                        ev = {};

                    self._trackComponentInfo($compObj);

                    ev.eventInfo = {
                        'type':ctConstants.trackEvent,
                        'eventAction': ctConstants.share,
                        'eventLabel' : service + ' - ' + title
                    };

                    ev.category ={'primaryCategory':ctConstants.referral};
                    digitalData.event.push(ev);
                }
            },
            
            _trackShopNow: function(binBtnElm) {
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    var self = this,
                        ev = {},
                        $compObj = $(binBtnElm).parents('.c-diagnostic-tool'),
                        $prodObj = $(binBtnElm).parents(self.settings.productItem),
                        productID = $prodObj.data('product-id'),
                        productName = $prodObj.data('product-name'),
                        productCategory = $prodObj.data('product-category'),
                        brandName = self.componentJson.brandName;

                    self._trackComponentInfo($compObj);
                    
                    digitalData.product = [];

                    digitalData.product.push({
                       'productInfo': {
                           'productID': productID,
                           'productName': productName
                       },
                       'category': {
                            'primaryCategory': productCategory
                       }
                    });

                    ev.eventInfo = {
                      'type': ctConstants.trackEvent,
                      'eventAction': ctConstants.purchase,
                      'eventLabel': 'Online - ' + productName
                    };
                    ev.category = {'primaryCategory':ctConstants.conversion};
                    digitalData.event.push(ev);
                }
            },
            
            _trackDiagToolCta: function(elm) {
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    var self = this,
                        ev = {},
                        $btnLabel = elm.hasClass('o-btn') ? elm.text() : 'Next',
                        $compObj = elm.parents('.c-diagnostic-tool'),
                        $queNo = elm.hasClass('js-intro__start-btn') ? '' : $compObj.find(self.settings.currentQueNum).text();
                    
                    self._trackComponentInfo($compObj);
                    
                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.diagtoolcta,
                        'eventLabel': $btnLabel + ($queNo ? ' - ' + $queNo : '')
                    };
                    ev.attributes={'nonInteractive':{'nonInteraction': 1}};
                    ev.category = {'primaryCategory':ctConstants.custom};

                    digitalData.event.push(ev);
                }
            },
            
            _trackEmail: function(emailBtnElm) {
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    var self = this,
                        ev = {},
                        $compObj = $(emailBtnElm).parents('.c-diagnostic-tool'),
                        $pageTitle = $('meta[property="og:title"]').attr('content');
                    
                    self._trackComponentInfo($compObj);
                    
                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.diagtoolemail,
                        'eventLabel': $pageTitle + ' - Email Send'
                    };
                    ev.attributes={'nonInteractive':{'nonInteraction': 1}};
                    ev.category = {'primaryCategory':ctConstants.custom};

                    digitalData.event.push(ev);
                }
            },
            
            _trackQuestionnaireSubmission: function(msg) {
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    var self = this,
                        ev = {},
                        $compObj = $('.c-diagnostic-tool');
                    
                    self._trackComponentInfo($compObj);
                    
                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.submission,
                        'eventLabel': msg
                    };
                    ev.attributes = {'nonInteraction': 1};
                    ev.category = {'primaryCategory': ctConstants.other};
                    
                    digitalData.event.push(ev);
                }
            },
            
            _trackProductInfo: function (obj, productData, options) {
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    var productVariant = ((productData.data('product-variant') !== undefined) ? productData.data('product-variant') : obj.data('product-variant')),
                        productCategory = (productData.data('product-category') !== undefined) ?  productData.data('product-category') : obj.data('product-category'),
                        brandName = (productData.data('product-brand') !== undefined) ? productData.data('product-brand') : obj.data('product-brand');

                    digitalData.product.push({
                       'productInfo' :{
                           'productID': (options && options.productID) ? options.productID : productData.data('product-id'),
                           'productName': (options && options.productName) ? options.productName : productData.data('product-name')
                        },
                        'attributes': {
                            'productVariants': (productVariant !== undefined) ? productVariant : ''
                        },
                        'category':{
                            'primaryCategory': (productCategory !== undefined) ? productCategory : ''
                        }
                    });
                }
            },
            
            _productImpressionTag: function(wrapper, item) {
                var self = this,
                    $productListing = $(wrapper),
                    ev = {},
                    options = {},
                    $compObj = $('.c-diagnostic-tool');

                if ($productListing.length) {
                    // track component details
                    self._trackComponentInfo($compObj);

                    digitalData.product = []; //empty product data for each events so that it picks up the correct product data

                    $(wrapper+' '+item).each(function(i) {
                        var $this = $(this);

                        // track component info
                        self._trackProductInfo($productListing, $this, options);
                    });

                    ev.eventInfo = {
                      'type':ctConstants.productImpression
                    };

                    ev.category = {'primaryCategory':ctConstants.custom};
                    ev.attributes = {'nonInteractive':{'nonInteraction': 1}};
                    digitalData.event.push(ev);
                }
            }
        });
    });

    return DiagnosticTool;
});
