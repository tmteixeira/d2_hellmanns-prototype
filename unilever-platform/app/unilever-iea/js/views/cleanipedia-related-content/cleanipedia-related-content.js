/*global define*/

define(['commonService'], function() {
    'use strict';

    var CleanipediaRelatedContent = IEA.module('UI.cleanipedia-related-content', function(cleanipediaRelatedContent, app, iea) {

        var commonService = null;

        _.extend(cleanipediaRelatedContent, {

            defaultSettings: {
                pairedContentArticlesContainer: '.c-cleanipedia__paired-content-container',
                relatedContentArticlesContainer: '.c-cleanipedia__related-content-container',
                pairedContentTemplate: 'cleanipedia-related-content/partial/paired-content.hbss',
                relatedContentTemplate: 'cleanipedia-related-content/partial/related-content.hbss',
                offsetLimit: 10,
                preloader: '.o-preloader',
                offset: 0,
                isRelatedContent: false
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
                commonService = new IEA.commonService(); // creates new instance for common service
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                var self = this,
                    componentJSONData = self.getModelJSON(),
                    settings = self.defaultSettings,
                    pairedContentData = null,
                    relatedContentData = null;

                self.preloader = settings.preloader;

                pairedContentData = componentJSONData.cleanipediaRelatedContent.pairedContent;

                relatedContentData = componentJSONData.cleanipediaRelatedContent.relatedContent;

                self.isRelatedContent = settings.isRelatedContent;

                // checking paired content data in component json exist
                if(pairedContentData){
                    self._getArticlesData(componentJSONData, self.isRelatedContent);
                }

                // checking related content data in component json exist
                if(relatedContentData){
                    self._getArticlesData(componentJSONData, true);
                }
            },

            /**************************PRIVATE METHODS******************************************/

            // To get paired content and related content articles data
            _getArticlesData: function(componentJSONData, isRelatedContent) {
                var self = this;

                // getting related articles data as well as paired articles data
                if(isRelatedContent) {
                    self._getUpdatedRelatedContentData(componentJSONData);
                } else {
                    self._getUpdatedPairedContentData(componentJSONData);
                }
            },

            // To get the paired content data after ajax call
            _getUpdatedPairedContentData: function(componentJSONData){
                var self = this,
                    configSettings = componentJSONData.cleanipediaRelatedContent.cleanipediaConfiguration,
                    options = {
                        locale: configSettings.locale,
                        brand: configSettings.brand,
                        entity: configSettings.entity,
                        serviceUrl: configSettings.serviceUrl
                    },
                    articleIds = [],
                    pairedContentArticles = null,
                    updatedPairedArticlesData = [];

                $(self.preloader).removeClass('hidden');// show loader before loading data

                self.pairedContentArticleData = componentJSONData.cleanipediaRelatedContent.pairedContent;

                if (self.pairedContentArticleData.pairedContent) {
                    $(self.preloader).removeClass('hidden');

                    self.totalNumOfPairedContent = self.pairedContentArticleData.pairedContent.length;

                    for (var i = 0; i < self.totalNumOfPairedContent; i++) {
                        articleIds.push(self.pairedContentArticleData.pairedContent[i].articleId);
                        articleIds.join(',');
                        self.pairedContentArticles = articleIds.toString();
                    }

                    options.articleIds = self.pairedContentArticles;
                }

                options.servletUrl = options.serviceUrl + '/' + options.articleIds;
                options.queryParams = '?brand=' + options.brand + '&locale=' + options.locale + '&entity=' + options.entity;

                options.onCompleteCallback = function(data) {
                    data = JSON.parse(data);
                    updatedPairedArticlesData = data;
                    self._showPairedContentArticlesData(updatedPairedArticlesData, componentJSONData);
                    $(self.preloader).addClass('hidden');// hide loader after loading data
                };

                commonService.getDataAjaxMethod(options);
            },

            // To display the paired content data after ajax call
            _showPairedContentArticlesData: function(pairedContentData, componentJSONData){
                var self = this,
                    template = self.getTemplate('paired-content', self.defaultSettings.pairedContentTemplate),
                    componentPairedContentDataArrayLength = componentJSONData.cleanipediaRelatedContent.pairedContent.pairedContent.length,
                    componentPairedContentData = componentJSONData.cleanipediaRelatedContent.pairedContent,
                    componentPairedContentArticleId = null,
                    updatedPairedContentComponentJSON,
                    pairedContentJSON = {};

                if(componentPairedContentData !== "undefined" || componentPairedContentDataArrayLength > 0){
                    for (var i = 0,j=0; i<componentPairedContentDataArrayLength,j<pairedContentData.data.length; i++,j++) {
                        componentPairedContentArticleId = parseInt(componentPairedContentData.pairedContent[i].articleId);
                            if(componentPairedContentArticleId === pairedContentData.data[j].id) {

                                if(componentPairedContentData.pairedContent[i].articleHeading === "") {
                                    componentPairedContentData.pairedContent[i].articleHeading = pairedContentData.data[j].title;
                                }

                                if(componentPairedContentData.pairedContent[i].articleDescription === "") {
                                    componentPairedContentData.pairedContent[i].articleDescription = pairedContentData.data[j].meta_description;
                                }

                                if(componentPairedContentData.pairedContent[i].image.altImage === "") {
                                    componentPairedContentData.pairedContent[i].image.altImage = pairedContentData.data[j].title;
                                }

                                if(componentPairedContentData.pairedContent[i].image.url === "") {
                                   componentPairedContentData.pairedContent[i].image.url = pairedContentData.data[j].thumbnail;
                                }

                                if(self.$el.find('.paired-content-articles__read-more').parent().next().attr('href') === undefined) {
                                    componentPairedContentData.pairedContent[i].readMoreUrl = pairedContentData.data[j].canonical_url;
                                }
                            }
                        updatedPairedContentComponentJSON = componentPairedContentData;
                    }
                }

                pairedContentJSON = {
                    'pairedContentUpdatedData': updatedPairedContentComponentJSON,
                    'cleanipediaRelatedContentJSON': componentJSONData
                };

                $(self.defaultSettings.pairedContentArticlesContainer).html(template(pairedContentJSON));
            },

            // To get the related content data after ajax call
            _getUpdatedRelatedContentData: function(componentJSONData) {
                var self = this,
                    configSettings = componentJSONData.cleanipediaRelatedContent.cleanipediaConfiguration,
                    relatedContentQuery = componentJSONData.cleanipediaRelatedContent.relatedContent.contentQuery,
                    options = {},
                    finalData = [],
                    updatedRelatedArticlesData = [],
                    remainingRelatedArticles = 0;

                $(self.preloader).removeClass('hidden');

                options = {
                    brand: configSettings.brand,
                    locale: configSettings.locale,
                    entity: configSettings.entity,
                    serviceUrl: configSettings.serviceUrl,
                    maximumTxLimit: configSettings.maxRelatedArticle,
                    offset: self.defaultSettings.offset,
                    relatedContentQuery: relatedContentQuery
                },

                options.servletUrl = options.serviceUrl;
                options.queryParams = options.relatedContentQuery + '&brand=' + options.brand + '&locale=' + options.locale + '&entity=' + options.entity +'&limit='+options.maximumTxLimit+'&offset='+options.offset;

                options.onCompleteCallback = function(data) {
                    data = JSON.parse(data);
                    self.totalRecordsAfterAjaxCall = data.data.length;
                    finalData.push(data);

                    for (var i=0; i<finalData.length; i++) {
                        for(var j = 0; j<finalData[i].data.length; j++){
                            updatedRelatedArticlesData.push(finalData[i].data[j]);
                        }
                    }

                    self._showRelatedContentArticlesData(updatedRelatedArticlesData, componentJSONData);

                    $(self.preloader).addClass('hidden');
                };

                commonService.getDataAjaxMethod(options);       
            },

            // To display related content data after ajax call
            _showRelatedContentArticlesData: function(relatedContentData,cleanipediaJSONData){
                var self = this,
                template = self.getTemplate('related-content',self.defaultSettings.relatedContentTemplate),

                relatedContentJSON = {
                    'cleanipediaRelatedContentJSON': cleanipediaJSONData
                };

                relatedContentJSON['relatedContent'] = (relatedContentData.length) ? relatedContentData : relatedContentData.data;

               for(var i=0; i<relatedContentJSON.relatedContent.length; i++) {
                    for(var j=0; j<relatedContentJSON.relatedContent[i].content.length; j++) {
                        if(relatedContentJSON.relatedContent[i].content[j].type === "image"){
                            relatedContentJSON.relatedContent[i].firstRelatedContentImageAlt = relatedContentJSON.relatedContent[i].content[j].value.alt;
                            relatedContentJSON.relatedContent[i].firstRelatedContentImageSrc = relatedContentJSON.relatedContent[i].content[j].value.sizes.thumbnail;
                            break;
                        }
                    }
                }

                $(self.defaultSettings.relatedContentArticlesContainer).html(template(relatedContentJSON));
            }
        });
    });

    return CleanipediaRelatedContent;
});
