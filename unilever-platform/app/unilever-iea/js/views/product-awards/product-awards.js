/*global define*/

define(['accordion','commonService'],function() {
    'use strict';

    var ProductAwards = IEA.module('UI.product-awards', function(productAwards, app, iea) {

        var commonService = null;

        _.extend(productAwards, {

            defaultSettings: {
                headerContainer: '.c-expandcollapse__header',
                speed: 400
            },

            events: {
                'click a.c-expandcollapse__link': '_callExpandCollapse'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforeEnable');

                commonService = new IEA.commonService();
                this._dateformat();
                this.triggerMethod('enable');
            },

            _callExpandCollapse: function(evt, options) {
                var service = new IEA.accordion(); // creates new instance
                service._handleExpandCollapse(evt, options);
            },

            _dateformat: function() {
                $('.c-award-date-range').each(function(index){
                    var daterange = $(this).html().split(' - '),
                    newdaterange = commonService.getSubstring(daterange[0],0,10) +" - "+ commonService.getSubstring(daterange[1],0,10);
                    $(this).html(newdaterange);
                });
            }

            /**************************PRIVATE METHODS******************************************/          

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */

        });
    });

    return ProductAwards;
});
