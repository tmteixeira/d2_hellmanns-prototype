/*global define*/

define(function() {
    var SectionNavigation = IEA.module('UI.section-navigation', function(sectionNavigation, app, iea) {

        _.extend(sectionNavigation, {
            // Extendable hooks and API methods goes here

            onBeforeInit: function(options) {

            },

            onInit: function() {

            },

            onBeforeRender: function(options) {

            },

            onRender: function() {

            },

            onBeforeEnable: function() {

            },

            onEnable: function() {

            },
            onBeforeShowMenu: function() {

            },

            onAfterShowMenu: function() {

            },
            onBeforeHideMenu: function() {

            },

            onAfterHideMenu: function() {

            }


        });
    });

    return SectionNavigation;
});
