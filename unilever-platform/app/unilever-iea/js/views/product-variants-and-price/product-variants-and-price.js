/*global define*/

define(['TweenMax', 'slick', 'commonService'], function() {
    'use strict';

    var commonService = null;

    var ProductVariantsAndPrice = IEA.module('UI.product-variants-and-price', function(productVariantsAndPrice, app, iea) {

        _.extend(productVariantsAndPrice, {

            defaultSettings: {
                productImageWrapper: '.js-product-images__wrapper',
                productItem: '.js-product-images-thumbnail',
                productNav: '.js-product-images-nav',
                retail: '.c-dropdown-label__retail',
                unit: '.c-dropdown-label__unit',
                price: '.c-dropdown-label__price',
                productImageTemplate: 'product-variants-and-price/partial/product-image.hbss',
                slidesToScrollThumbnail : 3
            },

            events: {
                'change .js-variant-dropdown-select' : '_updateDropdown'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                // creates a new instance of the services
                commonService = new IEA.commonService();

                this.settings = this.defaultSettings;
                this.componentJson = this.getModelJSON().productVariantsAndPrice;
                this.productImageWrapper = this.settings.productImageWrapper;

                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/          

            /**
             * Select box price and unit update, used to update the label text
             * @method _updateDropdown
             * @return
             */

            _updateDropdown: function (evt) {
                evt.preventDefault();

                var $this = $(evt.currentTarget),
                    currentIndex = evt.currentTarget.selectedIndex,
                    currentData = this.componentJson.product.productsDetail[currentIndex];

                if (currentData) {
                    $(this.settings.price).html(currentData.currencySymbol + currentData.price);
                    $(this.settings.unit).html(currentData.unit);
                    
                    if (currentData.price) {
                        $(this.settings.retail).html(currentData.retail);
                    } else {
                        $(this.settings.retail).html("");   
                    }

                    //update product image
                    this._updateProductImages(currentData);
                }                
            },


            /**
             * change product image based on the variane selection
             * @method _updateProductImages
             * @return
             */

            _updateProductImages: function (currentData) {
                var self = this,
                    template = self.getTemplate('product-image', self.settings.productImageTemplate),            
                    productItem = $(self.productImageWrapper).find(self.settings.productItem),
                    productNav = $(self.productImageWrapper).find(self.settings.productNav);

                //update product image template
                $(self.productImageWrapper)
                .html(template(currentData))
                .addClass('slick-loading');
                
                TweenMax.to(window,1, {scrollTo:{y:160}});

                //slick carousel
                setTimeout(function() {
                    self._initCarousel();
                    $(self.productImageWrapper).removeClass('slick-loading');              
                },500);             
            },

            /**
             * Initiate carousel for the product image and thumbnails
             * @method _initCarousel
             * @return string
             */
            _initCarousel: function() {
                var self = this,               
                    productItem = $(self.productImageWrapper).find(self.settings.productItem),
                    productNav = $(self.productImageWrapper).find(self.settings.productNav);

                $(productItem).slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: (commonService.isMobile() === true) ? false : true,
                    asNavFor: productNav
                });

                $(productNav).slick({
                    slidesToShow: (commonService.isMobile() === true) ? 1 : self.settings.slidesToScrollThumbnail,
                    slidesToScroll: 1,
                    asNavFor: productItem,
                    dots: (commonService.isMobile() === true) ? true : false,
                    focusOnSelect: true
                });
            }
        });
    });

    return ProductVariantsAndPrice;
});
