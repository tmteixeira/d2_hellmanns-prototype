/*global define*/
define(['TweenMax', 'commonService', 'search', 'ratings', 'accordion'], function(TweenMax) {
    'use strict';

    var SearchListingV2 = IEA.module('UI.search-listing-v2', function(searchListing, app, iea) {

        var searchService = null,
            ratingService = null,
            commonService = null;

        _.extend(searchListing, {

            defaultSettings: {
                searchWrapper: '.c-inline-search .c-global-search__wrapper',
                searchInputField: '.c-inline-search .typeahead',
                clearSearch: '.c-inline-search .o-btn-clear-search',
                searchButton: '.c-inline-search .js-btn-search',
                loadmore: '.js-btn-loadmore',
                filterSl: '.c-search-listing-v2-filters__select',
                searchContainer: '.c-search-listing-v2__wrap',
                totalRecordEl: '.c-search-listing-v2__total',
                searchListingSl: '.c-search-listing-v2__list',
                searchForm: '.inlineSearch',
                noSearchResultWrapper: '.c-inline-search__no-results',
                activeclass: 'is-active',
                isLoadMore: false,
                pageNum: 1,
                contentType: '',
                contentName: '',
                queryType: '',
                loadTabs: true,
                loadFacets: true,
                pageLoad: true,
                preloader: '.c-search-listing-v2__loadmore .o-preloader',
                template: 'search-listing-v2/partial/search-result-v2-list.hbss',
                templateTabs: 'search-listing-v2/partial/search-result-v2-tabs.hbss',
                templateNoData: 'search-listing-v2/partial/no-search-result-v2.hbss',
                templateFacets: 'search-listing-v2/partial/search-result-v2-facets.hbss',
                defaultSuggestions: true,
                searchInputFont: 30,
                searchInputFontMobile: 16,
                suggestionsLimit: 6,
                searchUnder: '',
                pagination: '.js-search-listing-v2-pagination',
                paginationTemplate: 'search-listing-v2/partial/search-result-v2-pagination.hbss',
                paginationButtons: {
                    first: '.js-search-listing-v2-pagination__first-btn',
                    last: '.js-search-listing-v2-pagination__last-btn',
                    next: '.js-search-listing-v2-pagination__next-btn',
                    previous: '.js-search-listing-v2-pagination__previous-btn'
                },
                startPageNum: 1,
                loadPagination: false,
                tabWrapper: '.c-search-listing-v2__tab-wrapper'
            },

            events: {
                'change .js-search__filter-change': '_updateFilterParams', //update params and render search results on dropdown change
                'click .js-search-listing-v2-tabs__filter-tab-a': '_updateFilterParams',
                'click .js-btn-loadmore': 'showAllResultsHandler', // render results on show all button click
                'focus keypress .c-inline-search .typeahead': 'keyboardHandler', // capture keyboard events
                'click .c-inline-search .js-btn-search': 'submitForm', // render search listing on search button click
                'submit .c-inline-search__form': 'submitForm',
                'click .js-search-listing-v2-pagination-click': '_pagination',
                'click a.c-expandcollapse__link': '_callExpandCollapse'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function(options) {

                searchService = new IEA.search(); // creates new instance for search service
                ratingService = new IEA.ratings(); // creates new instance for rating service
                commonService = new IEA.commonService(); // creates new instance for common service

                this._super(options);

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function() {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
            @description: Show the component
            @method show
            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            @description: Hide the component
            @method hide
            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean
            @return {null}
            **/
            clean: function() {
                this._super();
            },

            getQueryStringVal: function(name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
                return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function() {
                this.triggerMethod('beforEnable');

                var self = this,
                    settings = self.defaultSettings;

                self.searchListingData = this.getModelJSON();
                self.searchModelData = self.searchListingData.searchListingV2;

                // required parameters for rating service
                self.ratingReview = self.searchModelData.review;
                self.brandName = self.searchModelData.brandName;

                self.isLoadMore = settings.isLoadMore;
                self.filterSelect = settings.filterSl;
                self.searchWrapper = settings.searchWrapper;
                self.searchContainer = settings.searchContainer;
                self.searchInputField = settings.searchInputField;
                self.clearSearch = settings.clearSearch;
                self.searchButton = settings.searchButton;
                self.searchForm = settings.searchForm;
                self.loadmore = settings.loadmore;
                self.searchListingSl = settings.searchListingSl;
                self.noSearchResultWrapper = settings.noSearchResultWrapper;
                self.preloader = settings.preloader;
                self.loadTabs = settings.loadTabs;
                self.loadFacets = settings.loadFacets;
                self.pagination = settings.pagination;
                self.loadPagination = settings.loadPagination;
                self.tabWrapper = settings.tabWrapper;

                // search parameters
                self.pageLoad = settings.pageLoad;
                self.pageNum = settings.pageNum;
                self.contentType = settings.contentType;
                self.contentName = settings.contentName;
                self.searchUnder = settings.searchUnder;
                self.queryType = settings.queryType;
                self.locale = self.searchModelData.market;
                self.dropDownLabel = self.searchModelData.dropDownLabel;
                self.tabContentTypes = self.searchModelData.tabContentTypes;
                self.searchKeyword = '';
                self.pageScroll = false;

                if (commonService.isMobile()) {
                    self.inputFont = settings.searchInputFontMobile;
                } else {
                    self.inputFont = settings.searchInputFont;
                }

                // show suggestions
                if ($(self.searchInputField).parent('.twitter-typeahead').length === 0) {
                    self.renderSuggestions();
                }

                /*Load first set of record*/
                self.setKeywordValOnLoad();

                $(self.tabContentTypes).each(function(key, value) {
                    if (value.contentType === self.contentType) {
                        self.contentName = value.contentName;
                        return false;
                    }
                });

                // render search listing on page load
                if (self.searchModelData.isExternal && (self.contentType === '' || self.contentName.toLowerCase() === 'everything')) {
                    self.allTabs = true;
                    self._populateDataForAllTabs();
                } else {
                    self._populateData();
                }

                // clear search on cross click
                searchService.clearSearchVal(self.clearSearch, self.searchInputField);

                // show/hide load more on page scroll
                self.handleLoadMore();

                self._triggerTabChange();

                // Ratings Integration 
                //ratingService.initRatings(options);

                this.triggerMethod('enable');
            },

            /**************************PRIVATE METHODS******************************************/

            // set the search parameter on dropdown change
            _updateFilterParams: function(ev) {
                var self = this,
                    filterTabLinkCl = ".js-search-listing-v2-tabs__filter-tab-a",
                    activeCl = "active",
                    $this = $(ev.target);

                ev.preventDefault();
                self.isLoadMore = false;
                self.pageLoad = false;
                self.loadPagination = true;
                self.pageNum = 1;
                self.animateResultsFadeout();
                //self.contentType = $this.val();

                if ($this.hasClass("js-search-listing-v2-tabs__filter-tab-a")) {
                    $(filterTabLinkCl).removeClass(activeCl);
                    $this.addClass(activeCl);
                }

                // render search listing
                self._populateData();
            },

            showAllResultsHandler: function(e) {
                var self = this,
                    $this = $(e.target),
                    ev = {},
                    compName = $this.parents('[data-componentname]').data('componentname'),
                    compVariants = $this.parents('[data-component-variants]').data('component-variants'),
                    compPositions = $this.parents('[data-component-positions]').data('component-positions');

                e.preventDefault();

                self.pageNum = self.pageNum + 1;
                $this.fadeOut(500);
                self.pageLoad = false;
                $(self.preloader).removeClass('hidden');
                self.isLoadMore = true;

                // render search listing
                self._populateData();

                // load more analytics
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    digitalData.component = [];
                    digitalData.component.push({
                        'componentInfo': {
                            'componentID': compName,
                            'name': compName
                        },
                        'attributes': {
                            'position': compPositions,
                            'variants': compVariants
                        }
                    });
                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.loadMore,
                        'eventLabel': self.contentType
                    };
                    ev.attributes = {
                        'nonInteraction': 1
                    };
                    ev.category = {
                        'primaryCategory': ctConstants.custom
                    };
                    digitalData.event.push(ev);
                }
            },

            // handle search on keyboard handler events
            keyboardHandler: function(e) {
                var self = this,
                    kCode = e.keyCode || e.charCode, //for cross browser
                    $this = $(e.target);

                if (kCode === 13) { //Capture Enter Key
                    if ($this.val() === '' || $this.val().match(/^\s*$/)) {
                        $(self.searchInputField).focus();
                        $(self.loadmore).fadeOut(500);
                    } else {
                        self.animateResultsFadeout();
                        self.isLoadMore = false;
                        self.pageLoad = true;
                        $this.typeahead('close');

                        // update query parameters
                        self.updateParams($this.val());

                        // render search listing
                        self._populateData();

                        // search analytics
                        if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                            self.searchAnalytics();
                        }
                    }
                    return false;
                }
            },

            // search Analytics
            searchAnalytics: function() {

                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    var ev = {},
                        compName = $(".c-search-listing-v2").parents('[data-componentname]').data('componentname'),
                        compVariants = $(".c-search-listing-v2").parents('[data-component-variants]').data('component-variants'),
                        compPositions = $(".c-search-listing-v2").parents('[data-component-positions]').data('component-positions');

                    digitalData.component.push({
                        'componentInfo': {
                            'componentID': compName,
                            'name': compName
                        },
                        'attributes': {
                            'position': compPositions,
                            'variants': compVariants
                        }
                    });

                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.siteSearch,
                        'eventLabel': this.searchKeyword + '-' + $(this.defaultSettings.totalRecordEl).text()
                    };
                    ev.category = {
                        'primaryCategory': ctConstants.other
                    };
                    digitalData.event.push(ev);
                    digitalData.component = [];
                }
            },

            // set value from qyery strings in search input on page load
            setKeywordValOnLoad: function() {
                var self = this;
                // page load search result
                self.searchKeyword = self.getQueryStringVal('q');
                self.searchUnder = self.getQueryStringVal('SearchUnder');
                var contentType = self.getQueryStringVal('fq');

                if (contentType === '') {
                    self.allTabs = true;
                } else {
                    self.allTabs = false;
                }

                if (contentType.indexOf('ContentType') !== -1) {
                    var queryS = contentType.split('ContentType:')[1];
                    self.contentType = queryS;
                }

                if (self.searchKeyword !== '') {
                    $(self.searchInputField).val(self.searchKeyword);
                    $(self.searchInputField).next('pre').text(self.searchKeyword);
                    $(self.clearSearch).removeClass('hidden');
                    self.resizeFont($(self.searchInputField).next('pre'), self.inputFont, $(self.searchInputField));
                }
            },

            // animate resuls
            animateResultsFadeIn: function() {
                TweenMax.to($(this.searchContainer), 0.8, {
                    autoAlpha: 1,
                    top: 0,
                    delay: 0.2
                });
                $('.c-content-loader').fadeOut(500);
            },

            // animate resuls
            animateResultsFadeout: function() {
                TweenMax.to($(this.searchContainer), 0.8, {
                    autoAlpha: 0,
                    top: '30px',
                    delay: 0.2
                });
                $('.c-content-loader').fadeIn(500);
            },

            /**
             * Auto complete
             * @method autoComplete
             * @return 
             */

            renderSuggestions: function() {
                // constructs the suggestion engine
                var self = this,
                    options = {};

                options.dropDownLabel = self.dropDownLabel;
                options.searchWrapper = self.searchWrapper;
                options.searchInputField = self.searchInputField;
                options.customKeyword = self.defaultSettings.defaultSuggestions;
                options.locale = self.locale;
                options.brandName = self.brandName;
                options.clearSearch = self.clearSearch;
                options.searchKeyword = self.searchKeyword;
                options.inputFont = self.inputFont;
                options.suggestionsLimit = self.defaultSettings.suggestionsLimit;
                options.tabContentTypes = self.tabContentTypes;
                options.hint = self.searchModelData.configurations.disableAutoComplete;

                options.selectCallback = function(options, keywordVal, obj) {
                    self.updateParams(keywordVal);
                    options.searchKeyword = self.searchKeyword;
                    self.isLoadMore = false;
                    self.animateResultsFadeout();
                    self.pageLoad = true;
                    self.loadTabs = true;
                    self.loadFacets = true;

                    $(options.tabContentTypes).each(function(key, value) {
                        if (keywordVal.indexOf(options.dropDownLabel + ' ' + value.contentType) !== -1) {
                            if (value.contentType === self.contentType) {
                                self.contentName = value.contentName;
                            }
                            self.allTabs = false;
                            return false;
                        } else {
                            self.allTabs = true;
                        }
                    });

                    if (self.contentType === '' || self.contentName.toLowerCase() === 'everything') {
                        self.allTabs = true;
                        self._populateDataForAllTabs();
                    } else {
                        self._populateData();
                    }

                    if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                        self.searchAnalytics();
                    }
                };

                searchService.showSuggestionsV2(options);
            },

            // shrink font size on type
            resizeFont: function(obj, fontsize, field) {
                searchService.shrinkToFill(obj, fontsize, field);
            },

            _triggerTabChange: function() {
                var self = this;
                $(document).on("click", ".c-search-listing-v2-results__link", function(event) {
                    self._triggerTabChangeHandler(event, self);
                });
                $(self.tabWrapper).on("click", ".js-search-listing-v2-tabs__tab-list-item", function(event) {
                    self._triggerTabChangeHandler(event, self);
                });
            },

            _triggerTabChangeHandler: function(event, self) {
                event.preventDefault();

                //get the contentType of the facet clicked
                self.contentType = $(event.target).data("content-type");
                self.pageLoad = true;
                self.loadTabs = false;
                self.loadFacets = true;
                self.queryType = $(event.target).data("query-type");
                self.pageNum = 1;
                self.isLoadMore = false;

                if (!$(event.target).hasClass("active")) {
                    $(".js-search-listing-v2-tabs__tab-list-item").removeClass("active");
                    $(".js-search-listing-v2-tabs__tab-list-item[data-content-type='" + self.contentType + "']").addClass("active");

                    if (self.contentType === '') {
                        self._populateDataForAllTabs();
                    } else {
                        self._populateData();
                    }
                }
            },

            //AJAX Call to Successful Data Fetch for all contentTypes
            _populateDataForAllTabs: function() {
                var self = this,
                    contentType,
                    deferred = [],
                    allProducts = [],
                    facets = [];

                self.searchModelData.tabContentTypes.forEach(function(item) {
                    var url, queryParams, defer, headers;
                    //handling for external calls

                    if (item.contentName.toLowerCase() !== 'everything') {
                        if (item.type !== "internal" && item.type !== "") {
                            url = item.url;
                            queryParams = {
                                "SearchKeyword": self.searchKeyword,
                                "PageSize": item.displayCount,
                                "PageIndex": self.pageNum,
                                "EnableDidYouMean": true,
                                "SU": self.searchUnder
                            };

                            var locale = self.locale,
                                beforeUn = locale.substr(0, locale.indexOf('_')),
                                afterUn = locale.substr(locale.indexOf("_") + 1).toUpperCase();
                            locale = beforeUn.concat('-', afterUn);

                            headers = {
                                'brand': self.brandName,
                                'locale': locale,
                                'Content-Type': 'application/json'
                            };

                            defer = $.ajax({
                                url: url,
                                type: "POST",
                                data: JSON.stringify(queryParams),
                                headers: headers
                            }).then(function(res) {

                                var contentTypeObj = {},
                                    facetItem = {};

                                if (res.data) {
                                    $(res.data.recipesList).each(function(key) {
                                        var listData = $(res.data.recipesList)[key],
                                            featuredTags = self.searchModelData.generalConfig.featureTags,
                                            listItemTags = '';

                                        if (listData.aemProperties.tags) {
                                            listItemTags = listData.aemProperties.tags;
                                        }

                                        if (listItemTags !== '' || listItemTags !== undefined) {
                                            listData.aemProperties.tags = [];
                                            $(featuredTags).each(function(key, value) {
                                                var tagID = value.tagID,
                                                    tagTitle = value.title;

                                                $(listItemTags).each(function(key, value) {
                                                    if (tagID === value.tagID) {
                                                        listData.aemProperties.tags.push(tagTitle);
                                                    }
                                                });
                                            });
                                        }
                                    });

                                    contentTypeObj = {
                                        ContentType: item.contentType,
                                        result: res.data.recipesList,
                                        showMore: self.searchModelData.showMore[item.contentType],
                                        type: "external",
                                        displayCount: item.displayCount
                                    };

                                    facetItem.contentType = item.contentType;
                                    facetItem.totalResults = res.data.recipeCount;

                                    if (facetItem.totalResults > 0) {
                                        facets.push(facetItem);
                                        allProducts.push(contentTypeObj);
                                    }
                                }

                            });

                        } else { // handling for internal calls
                            if (item.type !== '') {
                                contentType = (function() {
                                    var param;
                                    self.tabContentTypes.forEach(function(val) {
                                        if (val.contentType === item.contentType) {
                                            param = val.queryParams;
                                        }
                                    });

                                    return param;
                                })();
                                url = $(self.searchContainer).data('ajax-url');
                                queryParams = '?q=' + self.searchKeyword + '&' + contentType + '&Locale=' + self.locale + '&BrandName=' + self.brandName + '&PageNum=' + self.pageNum + '&ipp=' + item.displayCount + '&SU=' + self.searchUnder;
                                headers = {
                                    'featuredTags': JSON.stringify(self.searchModelData.generalConfig.featureTags),
                                    'defaultContentTypes': self.searchModelData.defaultContentTypes
                                };

                                defer = $.ajax({
                                    url: url + queryParams,
                                    type: 'GET',
                                    headers: headers
                                }).then(function(res) {
                                    var contentTypeObj = {},
                                        facetItem = {};

                                    if (res.response && !res.suggestionExist) {
                                        $(res.response).each(function(key) {
                                            var listData = $(res.response)[key],
                                                featuredTags = self.searchModelData.generalConfig.featureTags,
                                                listItemTags = '';

                                            if (listData.Tags) {
                                                listItemTags = listData.Tags;
                                            }

                                            if (listItemTags !== '' || listItemTags !== undefined) {
                                                listData.Tags = [];
                                                $(featuredTags).each(function(key, value) {
                                                    var tagID = value.tagID,
                                                        tagTitle = value.title;

                                                    $(listItemTags).each(function(key) {
                                                        if (tagID === this) {
                                                            listData.Tags.push(tagTitle);
                                                        }
                                                    });
                                                });
                                            }
                                        });

                                        contentTypeObj = {
                                            ContentType: item.contentType,
                                            result: res.response,
                                            showMore: self.searchModelData.showMore[item.contentType],
                                            type: "internal",
                                            displayCount: item.displayCount
                                        };

                                        facetItem.contentType = item.contentType;
                                        facetItem.totalResults = res.totalResults;

                                        if (res.response.length > 0) {
                                            facets.push(facetItem);
                                            allProducts.push(contentTypeObj);
                                        }
                                    }

                                });

                            } else {
                                var facetItem = {};
                                facetItem.contentType = item.contentType;
                                facets.push(facetItem);
                            }
                        }

                        deferred.push(defer);
                    }

                });

                $.when.apply(null, deferred).done(function() {

                    self.animateResultsFadeIn();
                    var isOnlyOneTag;

                    if (facets.length) {
                        isOnlyOneTag = self._createFacetTags({}, facets);
                    }

                    if (isOnlyOneTag) {
                        $(".c-search-listing-v2__tab-wrapper .js-search-listing-v2-tabs__tab-list-item[data-content-type=" + isOnlyOneTag + "]").trigger("click");
                    } else {
                        if (allProducts.length) {
                            self._handleListingForAllCategories({
                                allResultData: allProducts,
                                listing: "",
                                facets: facets
                            });
                        } else {
                            self.contentType = '';
                            self._populateData();
                        }
                    }

                });
            },

            /**
            @description: Backbone.js Bind Callback to Successful Model Fetch
            @method _searchView
            @return {null}
            **/

            _populateData: function(itemsPerPage) {
                /* Backbone.js Bind Callback to Successful Model Fetch */
                var self = this,
                    options = {},
                    contentType = '',
                    externalFilter,
                    filterQueryParameters = '',
                    queryType;

                if (itemsPerPage === '' || itemsPerPage === undefined) {
                    itemsPerPage = self.searchModelData.paginationMap.itemsPerPage;
                }

                if (self.contentType === '') {
                    contentType = '';
                } else {
                    contentType = (function() {
                        var param;
                        self.tabContentTypes.forEach(function(val) {
                            if (val.contentType === self.contentType) {
                                param = val.queryParams;
                                queryType = val.type;
                            }
                        });
                        if (param !== '' || param !== undefined) {
                            param = '&' + param;
                        } else {
                            param = '';
                        }
                        return param;
                    })();
                }

                //Generate Filter Query Parameters
                if (self.pageLoad === false) {
                    $(self.searchModelData.tabContentTypes).each(function(key, value) {
                        if (value.contentType === self.contentType && value.type !== 'internal') {
                            externalFilter = true;
                            return false;
                        } else {
                            externalFilter = false;
                        }
                    });
                    filterQueryParameters = self._generateFilterQueryParams(externalFilter);
                }

                //AJAX Handler
                if (self.searchModelData.isExternal) {

                    options.servletUrl = (function() {
                        var url = "";

                        self.searchModelData.tabContentTypes.forEach(function(item) {
                            if ((item.contentType.toLowerCase() === self.contentType.toLowerCase()) && item.url) {
                                url = item.url;
                                var queryParams = {
                                    "SearchKeyword": self.searchKeyword,
                                    "PageSize": itemsPerPage,
                                    "PageIndex": self.pageNum,
                                    "TagSearchQuery": filterQueryParameters,
                                    "EnableDidYouMean": true,
                                    "SU": self.searchUnder
                                };

                                var locale = self.locale,
                                    beforeUn = locale.substr(0, locale.indexOf('_')),
                                    afterUn = locale.substr(locale.indexOf("_") + 1).toUpperCase();
                                locale = beforeUn.concat('-', afterUn);

                                options.queryParams = JSON.stringify(queryParams);
                                options.isTypePost = true;
                                options.header = {
                                    'brand': self.brandName,
                                    'locale': locale,
                                    'Content-Type': 'application/json'
                                };
                            }
                        });

                        if (url === "") {
                            url = $(self.searchContainer).data('ajax-url');
                            options.queryParams = '?q=' + self.searchKeyword + contentType + filterQueryParameters + '&Locale=' + self.locale + '&BrandName=' + self.brandName + '&PageNum=' + self.pageNum + '&ipp=' + itemsPerPage + '&SU=' + self.searchUnder;
                            options.header = {
                                'featuredTags': JSON.stringify(self.searchModelData.generalConfig.featureTags),
                                'defaultContentTypes': self.searchModelData.defaultContentTypes
                            };
                        }

                        return url;
                    })();
                } else {
                    options.header = {
                        'featuredTags': JSON.stringify(self.searchModelData.generalConfig.featureTags),
                        'defaultContentTypes': self.searchModelData.defaultContentTypes
                    };
                    options.servletUrl = $(self.searchContainer).data('ajax-url');
                    options.queryParams = '?q=' + self.searchKeyword + contentType + filterQueryParameters + '&Locale=' + self.locale + '&BrandName=' + self.brandName + '&PageNum=' + self.pageNum + '&ipp=' + itemsPerPage + '&SU=' + self.searchUnder;
                }

                options.onCompleteCallback = function(data) {
                    self.animateResultsFadeIn();
                    self._generateSearchList(data);
                };

                commonService.getDataAjaxMethod(options);
            },

            //Update Filters w.r.t selected content type
            updateContentTypeFilters: function(responseData) {
                var self = this,
                    searchListingdataFilters = self.searchModelData.filters,
                    searchFilters = $('.c-search-listing-v2-filters'),
                    searchFiltersList = $('.c-search-listing-v2-filters__list'),
                    facets = responseData.Facet && responseData.Facet.contentTypes,
                    template = self.getTemplate('search-result-v2-facets', self.defaultSettings.templateFacets),
                    filtersAvl = 'filters-avl',
                    filters,
                    filterConfigOptions = [];

                if (!self.searchModelData.configurations.disableContentTypeFilter && !responseData.suggestionExist && !responseData.noresult) {
                    if (facets !== undefined) {
                        $(facets).each(function(key, value) {
                            if (value.totalResults > 0) {
                                var contentType = value.contentType;

                                $(searchListingdataFilters.filterOptions).each(function(key, value) {
                                    if (value.contentType === contentType) {
                                        if (value.configureOptions.length) {
                                            $(value.configureOptions).each(function(key, value) {
                                                value.brandName = self.searchModelData.brandName;
                                                if (self.allTabs === false) {
                                                    if (key > 0) {
                                                        filterConfigOptions.push(value);
                                                    }
                                                } else {
                                                    filterConfigOptions.push(value);
                                                }
                                            });

                                            if (filterConfigOptions.length > 0) {
                                                if ($(searchFilters).hasClass("hidden")) {
                                                    $(searchFilters).removeClass("hidden");
                                                    $(self.searchContainer).addClass(filtersAvl);
                                                }
                                                filters = template(filterConfigOptions);
                                                return false;
                                            } else {
                                                $(searchFilters).addClass("hidden");
                                                $(self.searchContainer).removeClass(filtersAvl);
                                            }
                                        }
                                    } else {
                                        $(searchFilters).addClass("hidden");
                                        $(self.searchContainer).removeClass(filtersAvl);
                                    }
                                });
                            }
                        });

                    } else {
                        var contentType = self.contentType;

                        $(searchListingdataFilters.filterOptions).each(function(key, value) {
                            if (value.contentType === contentType) {
                                if (value.configureOptions.length) {

                                    $(value.configureOptions).each(function(key, value) {
                                        value.brandName = self.searchModelData.brandName;
                                        if (self.allTabs || (self.allTabs === false && key > 0)) {
                                            filterConfigOptions.push(value);
                                        }
                                    });

                                    if (filterConfigOptions.length > 0) {
                                        if ($(searchFilters).hasClass("hidden")) {
                                            $(searchFilters).removeClass("hidden");
                                            $(self.searchContainer).addClass(filtersAvl);
                                        }
                                        filters = template(filterConfigOptions);
                                        return false;
                                    } else {
                                        $(searchFilters).addClass("hidden");
                                        $(self.searchContainer).removeClass(filtersAvl);
                                    }
                                }
                            }
                        });

                    }
                    //Append Filters
                    $(searchFiltersList).html(filters);
                    $(".c-search-listing-v2-filters__list li:first-child a.c-expandcollapse__link").trigger('click');
                } else {
                    $(self.tabWrapper).addClass('hidden');
                    $(searchFilters).addClass("hidden");
                    $(self.searchContainer).removeClass(filtersAvl);
                }
            },

            addPersonalizeLabel: function(objData) {
                var personalizeLabel = this.searchModelData.personalizableLabel,
                    customizable = this.searchModelData.customizableTag,
                    index = customizable.indexOf(":"),
                    textCustomizable = customizable.substr(index + 1);

                if (objData && objData.response) {
                    objData.response.forEach(function(val, indx) {
                        if (val.Tags) {
                            val.Tags.forEach(function(value, index) {
                                if (value === textCustomizable) {
                                    var span = $('.o-product__tag--customizable');
                                    span.text(personalizeLabel).removeClass('hidden');
                                }
                            });
                        }
                    });
                }
            },

            triggerMethodsOnSearch: function(objData) {
                var self = this,
                    launchTag = '.c-search-listing-v2__tag .o-tag',
                    options = {};


                options.brandName = self.brandName;
                options.productId = objData.response;

                // add ratings map to each list item

                if (objData && objData.response) {
                    objData.response.forEach(function(value, index) {
                        value.ratings = {
                            uniqueId: value.ProductSKU,
                            viewType: "inline"
                        };
                    });
                }

                options.ratingObj = objData.response[0].ratings;
                options.ratingReview = self.ratingReview;
                // Ratings Integration 
                ratingService.initRatings(options);

                if ($(launchTag).length > 0) {
                    $(launchTag).text(objData.label);
                }
                //addPersonalizeLabel
                self.addPersonalizeLabel(objData);
                self.app.trigger('image:lazyload', self);
            },

            //If noResults found
            handleNoResultCase: function(objData) {
                var self = this,
                    templateNoData = self.getTemplate('no-search-result-v2', self.defaultSettings.templateNoData),
                    suggestionText = '.js-text-suggest';

                // Show suggestions if exist in response
                if ((objData.totalResults >= 0 && (objData.suggestionExist || objData.noresult)) || (objData.data && objData.data.recipeCount === 0)) {

                    $(self.noSearchResultWrapper).html(templateNoData(self.searchListingData));

                } else {

                    $(self.noSearchResultWrapper).html('');

                }

                // Display total results 
                if ((objData.totalResults >= 0 && (objData.suggestionExist || objData.noresult)) ||
                    (objData.data && objData.data.recipeCount === 0 && objData.data.didYouMeanResultCount > 0)) {

                    $(self.defaultSettings.totalRecordEl).text('0');

                } else if (objData.totalResults >= 0 && self.pageLoad && self.loadTabs) {

                    $(self.defaultSettings.totalRecordEl).text(objData.totalResults);

                } else if ((objData.data && objData.data.recipeCount === 0 && objData.data.didYouMeanResultCount === 0 && self.pageLoad && self.loadTabs) || (objData.data && objData.data.recipeCount !== 0 && self.pageLoad && self.loadTabs)) {

                    $(self.defaultSettings.totalRecordEl).text(objData.data.recipeCount);

                }

                // show suggestionQuery keyword if suggestions exist
                if (objData.totalResults >= 0 && objData.suggestionExist) {

                    $('.matching-keyword').text(objData.suggestions + '?');
                    $('.did-you-mean, ' + suggestionText).removeClass('hidden');

                } else if (objData.totalResults >= 0 && !objData.noresult) {

                    $(suggestionText).removeClass('hidden');

                } else if (objData.data && objData.data.recipeCount === 0 && objData.data.didYouMeanResultCount > 0) {

                    $('.matching-keyword').text(objData.data.didYouMean + '?');
                    $('.did-you-mean, ' + suggestionText).removeClass('hidden');

                }
            },

            // Add/Append Results listing
            handleLoadMoreOnScroll: function(objData, listing) {
                var self = this,
                    loadmoreSl = '.c-search-listing-v2__loadmore, ' + self.loadmore,
                    itemsToShow = self.searchModelData.paginationMap.itemsPerPage,
                    totalResultsCount;

                if (self.isLoadMore) {
                    setTimeout(function() {
                        $(self.preloader).addClass('hidden');
                        $(self.searchListingSl).append(listing);

                        self.triggerMethodsOnSearch(objData);
                        // init rating 
                        if (ratingReview !== undefined && typeof ratingReview.widget !== 'undefined') {
                            ratingService._kritiqueRatings(ratingReview);
                        }
                    }, 500);
                } else {
                    setTimeout(function() {
                        $(self.searchListingSl).html(listing);
                        self.animateResultsFadeIn();

                        self.triggerMethodsOnSearch(objData);
                    }, 500);
                }

                //Get total results count
                if (objData.totalResults) {
                    totalResultsCount = objData.totalResults;
                } else if (objData.data && objData.data.recipeCount) {
                    totalResultsCount = objData.data.recipeCount;
                } else if (objData.data && objData.data.didYouMeanResultCount) {
                    totalResultsCount = objData.data.didYouMeanResultCount;
                }

                //Show loadmore on basis of total results count
                if (totalResultsCount > (itemsToShow * self.pageNum)) {
                    if (self.pageNum === 1) {
                        $(loadmoreSl).fadeIn(1000);
                    }
                    setTimeout(function() {
                        self.pageScroll = true;
                    }, 1000);
                } else {
                    $(loadmoreSl).fadeOut(500);
                    self.pageScroll = false;
                }
            },

            /**
            @description: creates search result item view
            @method _searchView
            @return {null}
            **/
            _generateSearchList: function(obj) {
                var self = this,
                    objData = $.parseJSON(obj),
                    filterData = objData.response,
                    searchListingData = self.searchModelData,
                    filterDataObj = {},
                    listing = '',
                    template = self.getTemplate('search-result-v2-list', self.defaultSettings.template),
                    recipeTemplate = self.getTemplate('search-result-v2-recipe-list', 'search-listing-v2/partial/search-result-v2-recipe-list.hbss');
                self.totalResults = objData.totalResults;

                if ($(self.searchContainer).hasClass('filters-avl') && self.contentType === '') {
                    $('.c-search-listing-v2-filters').addClass('hidden');
                    $(self.searchContainer).removeClass('filters-avl');
                }

                if (objData.response) { //Set data if Internal Content
                    for (var i = 0; i < objData.response.length; i++) {
                        $.extend(objData.response[i], objData.response[i], self.searchListingData);
                    }
                } else if (objData.data && objData.data.recipeCount) { //Set data if External Content Results
                    objData.response = [];
                    for (var i = 0; i < objData.data.recipesList.length; i++) {
                        $.extend(objData.data.recipesList[i], objData.data.recipesList[i], self.searchListingData);
                    }
                    objData.response = objData.data.recipesList;
                } else if (objData.data && objData.data.didYouMeanResultCount) { //Set data if Internal Content Suggestion
                    objData.response = [];
                    for (var i = 0; i < objData.data.didYouMeanRecipesList.length; i++) {
                        $.extend(objData.data.didYouMeanRecipesList[i], objData.data.didYouMeanRecipesList[i], self.searchListingData);
                    }
                    objData.response = objData.data.didYouMeanRecipesList;
                }

                if (self.pageLoad) {
                    if (self.loadTabs) {
                        if (objData.Facet) {
                            self._createFacetTags(filterDataObj, objData.Facet.contentTypes);
                        } else {
                            objData.externalContentType = self.contentType;
                            self._createFacetTags(filterDataObj, objData);
                        }
                    }
                    if (self.loadFacets) {
                        self.updateContentTypeFilters(objData);
                    }
                }

                if (filterData) {
                    self._handleListingForCategories({
                        filterData: filterData,
                        listing: listing,
                        searchListingData: searchListingData,
                        template: template,
                        objData: objData
                    });
                } else {
                    //recipe case
                    self._handleListingForCategories({
                        filterData: objData.response,
                        listing: listing,
                        searchListingData: searchListingData,
                        template: recipeTemplate,
                        objData: objData
                    });
                }

            },

            //Create Tabs w.r.t ContentTypes and Results
            _createFacetTags: function(filterDataObj, facets) {
                var tagData = "",
                    self = this,
                    templateTabs = self.getTemplate('search-result-v2-tabs', 'search-listing-v2/partial/search-result-v2-tabs.hbss'),
                    tabContentTypeWithPositiveRecords = [],
                    totalResultsAll = 0;

                if (self.allTabs === false) {
                    filterDataObj.contentTypeAll = false;

                    if (facets.externalContentType) {
                        var contentType = facets.externalContentType;
                        $(self.searchModelData.filters.filterOptions).each(function(key, value) {
                            if (value.contentType === contentType) {
                                self.contentType = value.contentType;
                                filterDataObj.filterTags = value.configureOptions[0].filters;
                                filterDataObj.contentType = value.contentType;
                            }
                        });
                    } else {
                        $(facets).each(function(key, value) {
                            if (value.totalResults > 0) {
                                var contentType = value.contentType;

                                $(self.searchModelData.filters.filterOptions).each(function(key, value) {
                                    if (value.contentType === contentType) {
                                        self.contentType = value.contentType;
                                        filterDataObj.filterTags = value.configureOptions[0].filters;
                                        filterDataObj.contentType = value.contentType;
                                    }
                                });
                            }
                        });
                    }

                } else {
                    filterDataObj.contentTypeAll = true;
                    filterDataObj.filterTags = (function() {
                        var tabContentTypesList = [];

                        self.searchModelData.tabContentTypes.forEach(function(item) {
                            var tabContentTypeItem = {};
                            facets.forEach(function(innerItem) {
                                if (item.contentType === innerItem.contentType) {
                                    tabContentTypeItem = {
                                        contentType: item.contentType,
                                        contentName: item.contentName,
                                        totalResults: innerItem.totalResults,
                                        queryType: item.type
                                    };

                                    tabContentTypesList.push(tabContentTypeItem);

                                    if (innerItem.totalResults > 0) {
                                        tabContentTypeWithPositiveRecords.push(item.contentType);
                                        totalResultsAll = totalResultsAll + parseInt(innerItem.totalResults);
                                    }
                                }
                            });
                        });

                        return tabContentTypesList;
                    })();

                    filterDataObj.allTabLabel = (function() {
                        var allTabLabel = {};

                        self.searchModelData.tabContentTypes.forEach(function(item) {
                            if (item.contentName.toLowerCase() === 'everything') {
                                allTabLabel = {
                                    totalResults: totalResultsAll,
                                    contentType: item.contentType,
                                    contentTypeAll: ''
                                };

                                return false;
                            }
                        });

                        return allTabLabel;
                    })();
                }

                tagData = templateTabs(filterDataObj);

                $(self.tabWrapper).removeClass('hidden').html(tagData);

                if (tabContentTypeWithPositiveRecords.length === 1) {
                    return tabContentTypeWithPositiveRecords[0];
                } else {
                    return false;
                }
            },

            //Generate Listing for One Content Type
            _handleListingForCategories: function(option) {
                var self = this,
                    filterData = option.filterData,
                    listing = option.listing,
                    searchListingData = option.searchListingData,
                    template = option.template,
                    objData = option.objData,
                    paginationTemplate = self.getTemplate('search-listing-v2-pagination', self.defaultSettings.paginationTemplate),
                    combinedJsonPagination = null;

                $(filterData).each(function(key) {
                    var listData = $(filterData)[key],
                        featuredTags = searchListingData.generalConfig.featureTags,
                        listItemTags = '';

                    if (listData.Tags) {
                        listItemTags = listData.Tags;
                    } else if (listData.aemProperties) {
                        listItemTags = listData.aemProperties.tags;
                    }

                    if (listItemTags) {
                        listData.Tags = [];
                        $(featuredTags).each(function(key, value) {
                            var tagID = value.tagID,
                                tagTitle = value.title;

                            $(listItemTags).each(function(key) {
                                if (tagID === this) {
                                    listData.Tags.push(tagTitle);
                                }
                            });
                        });
                    }

                    listing += template(listData);
                });

                $(self.pagination).removeClass('hidden');

                if (self.pageLoad || self.loadPagination) {
                    combinedJsonPagination = {
                        'componentJson': searchListingData,
                        'serviceData': objData.response,
                        'pageNum': self.pageNum
                    };

                    if (objData.totalResults) {
                        combinedJsonPagination.totalPages = Math.ceil(objData.totalResults / searchListingData.paginationMap.itemsPerPage);
                    } else if (objData.data && objData.data.recipeCount) {
                        combinedJsonPagination.totalPages = Math.ceil(objData.data.recipeCount / searchListingData.paginationMap.itemsPerPage);
                    } else if (objData.data && objData.data.didYouMeanResultCount) {
                        combinedJsonPagination.totalPages = Math.ceil(objData.data.didYouMeanResultCount / searchListingData.paginationMap.itemsPerPage);
                    }

                    $(self.pagination).html(paginationTemplate(combinedJsonPagination));
                }

                // update search results
                self.handleNoResultCase(objData);

                // check if load more enabled
                self.handleLoadMoreOnScroll(objData, listing);
                self._updatePaginationState(objData, listing);
            },

            //Generate Listing for All Content Types
            _handleListingForAllCategories: function(option) {
                var self = this,
                    listing = option.listing,
                    allResultData = option.allResultData,
                    searchFilters = $('.c-search-listing-v2-filters'),
                    filtersAvl = 'filters-avl',
                    template = self.getTemplate('search-result-v2-list-all', 'search-listing-v2/partial/search-result-v2-list-all.hbss'),
                    facets = option.facets,
                    totalResultsAll = 0,
                    allResultDataSorted = [];

                //Sort content type sections view
                self.searchModelData.tabContentTypes.forEach(function(item) {
                    allResultData.forEach(function(innerItem) {
                        if (item.contentType === innerItem.ContentType) {
                            allResultDataSorted.push(innerItem);
                        }
                    });
                });

                if ($(self.searchContainer).hasClass(filtersAvl)) {
                    $(searchFilters).addClass("hidden");
                    $(self.searchContainer).removeClass(filtersAvl);
                }

                //Get total results count
                facets.forEach(function(innerItem) {
                    if (innerItem.totalResults > 0) {
                        totalResultsAll = totalResultsAll + parseInt(innerItem.totalResults);
                    }
                });

                $('.c-search-listing-v2-pagination, .c-search-listing-v2__loadmore').addClass('hidden');

                if (allResultDataSorted !== '' || allResultDataSorted !== undefined || allResultDataSorted > 1) {
                    listing = template(allResultDataSorted);
                    $(self.searchListingSl).html(listing);
                    $(self.defaultSettings.totalRecordEl).text(totalResultsAll);
                }

                // Update search results/suggestions
                self.handleNoResultCase(allResultDataSorted);
            },

            /**
            @description: render search result item view and update DOM
            @method _renderSubView
            @return {null}
            **/

            submitForm: function(e) {
                var self = this,
                    keywordVal = $(self.searchInputField).val();

                e.preventDefault();
                self.isLoadMore = false;
                self.pageLoad = true;

                if (keywordVal === '' || keywordVal.match(/^\s*$/)) {
                    $(self.searchInputField).focus();
                    $(self.loadmore).fadeOut(500);
                } else {

                    self.animateResultsFadeout();
                    self.updateParams(keywordVal);
                    $(self.searchInputField).typeahead('close');

                    // render search listing
                    if (self.contentType === '') {
                        self._populateDataForAllTabs();
                    } else {
                        self._populateData();
                    }

                    // analytics on form submitions
                    if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                        self.searchAnalytics();
                    }
                }
                return false;
            },

            /**
            @description: Update parameter based on search
            @method updateParams
            @return {null}
            **/
            updateParams: function(keywordVal) {
                var self = this,
                    dropdownLabel = self.dropDownLabel,
                    queryLabel,
                    tabContentTypes = self.searchModelData.tabContentTypes;

                $(tabContentTypes).each(function(key, value) {
                    if (keywordVal.indexOf(dropdownLabel + ' ' + value.contentType) !== -1) {
                        self.contentType = value.contentType;
                        queryLabel = keywordVal.split(' ' + dropdownLabel + ' ' + value.contentType);
                        self.searchKeyword = queryLabel[0];
                        return false;
                    } else {
                        self.searchKeyword = keywordVal;
                        self.contentType = '';
                    }
                });

                self.pageNum = 1;
            },
            /**
            @description: handle load more click event
            @method: _handleLoadMore
            @return: {null}
            **/
            handleLoadMore: function() {
                var self = this;
                document.addEventListener('scroll', function(event) {
                    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop,
                        scrollOffset = scrollTop + window.innerHeight;

                    if ((document.body.scrollHeight <= scrollOffset) && (self.pageNum >= 2) && self.pageScroll) {
                        self.pageScroll = false;
                        $(self.loadmore).click();
                    }
                });
            },

            //Handle Pagination
            _pagination: function(e) {
                e.preventDefault();

                var clickType = $(e.currentTarget).data('pagination-action'),
                    self = this,
                    lastPageNum = Math.ceil(self.totalResults / self.searchModelData.paginationMap.itemsPerPage);

                self.pageLoad = false;

                switch (clickType) {
                    case 'showFirstPage':
                        // If we're at the first page, do nothing
                        if (self.pageNum === self.defaultSettings.startPageNum) {
                            return;
                        }

                        self.pageNum = self.defaultSettings.startPageNum;
                        break;

                    case 'showLastPage':
                        // If we're on the last page, do nothing
                        if (self.pageNum === lastPageNum) {
                            return;
                        }

                        self.pageNum = lastPageNum;
                        break;

                    case 'showPreviousPage':
                        // If we're at the first page, do nothing
                        if (self.pageNum === self.defaultSettings.startPageNum) {
                            return;
                        }

                        self.pageNum--;
                        break;

                    case 'showNextPage':
                        // If we're on the last page, do nothing
                        if (self.pageNum === lastPageNum) {
                            return;
                        }

                        self.pageNum++;
                        break;

                    case 'showPageByNumber':
                        if ($(this).hasClass('o-btn--disabled')) {
                            return false;
                        }

                        var pageNumber = $(e.currentTarget).data('pagenum');

                        self.pageNum = pageNumber;

                }

                $('.js-search-listing-v2-pagination__page').removeClass('o-btn--active o-btn--disabled');
                $('.js-search-listing-v2-pagination__page[data-pagenum="' + self.pageNum + '"]').addClass('o-btn--active o-btn--disabled');

                $(self.defaultSettings.preloader).removeClass('hidden');
                self._populateData(self.searchModelData.paginationMap.itemsPerPage);

            },

            //Generate query Parameters on Filter Change
            _generateFilterQueryParams: function(externalFilter) {
                var filterListItem = ".js-search-listing-v2-filters__list-item",
                    filterTabListItem = ".c-search-listing-v2-tabs__list-item",
                    filterTabListItemA = ".js-search-listing-v2-tabs__filter-tab-a",
                    filterParams = [],
                    filterParamsExternal = [];

                $(filterTabListItem).each(function(key, val) {
                    var $this = $(this);

                    if ($this.find(filterTabListItemA).hasClass("active")) {
                        var dataTagId = $this.find(filterTabListItemA).data("tab-id");
                        var dataTagIdExt = $this.find(filterTabListItemA).data("tab-id");
                        var tagParam = '&fq=Tags:' + dataTagId;

                        if (tagParam !== '') {
                            filterParams.push(tagParam);
                            filterParamsExternal.push('[' + parseInt(/[^/]*$/.exec(dataTagIdExt)[0]) + ']');
                        }
                    }
                });

                $(filterListItem).each(function(key, val) {
                    var $this = $(this),
                        filterType = $this.find('[data-filter-type]').data('filter-type'),
                        dataTagId = '',
                        tagParam = '',
                        tagParamExt = '';

                    switch (filterType) {
                        case 'default':
                            if ($this.find("input") && $this.find("input:checked").length) {
                                dataTagId = $this.find("input:checked").data("filter-tag-id");
                            } else if ($this.find('option:selected').length) {
                                dataTagId = $this.find('option:selected').data("filter-tag-id");
                            }

                            if (dataTagId !== undefined && dataTagId !== '') {
                                tagParam = '&fq=Tags:' + dataTagId;
                                tagParamExt = '[' + parseInt(/[^/]*$/.exec(dataTagId)[0]) + ']';
                            }
                            break;

                        case 'checkbox':
                            if ($this.find("input:checked").length) {
                                if ($this.find("input:checked").length > 1) {
                                    var dataCheckbox = [],
                                        dataCheckboxExt = [];
                                    $this.find("input:checked").each(function() {
                                        dataCheckbox.push($(this).data("filter-tag-id"));
                                        dataCheckboxExt.push(
                                            parseInt(/[^/]*$/.exec($(this).data("filter-tag-id"))[0])
                                        );
                                    });

                                    dataTagId = dataCheckbox.join(" OR ");
                                    tagParam = '&fq=Tags:' + dataTagId;
                                    tagParamExt = '[' + dataCheckboxExt + ']';
                                } else {
                                    dataTagId = $this.find("input:checked").data("filter-tag-id");
                                    tagParam = '&fq=Tags:' + dataTagId;
                                    tagParamExt = '[' + parseInt(/[^/]*$/.exec(dataTagId)[0]) + ']';
                                }
                            }
                            break;
                    }

                    if (tagParam !== '' && tagParamExt !== '') {
                        filterParams.push(tagParam);
                        filterParamsExternal.push(tagParamExt);
                    }
                });

                if (externalFilter === true) {
                    filterParamsExternal.join();
                    filterParamsExternal = '[' + filterParamsExternal + ']';
                    return filterParamsExternal;
                } else {
                    var filterParamString = filterParams.join("");
                    return filterParamString;
                }
            },

            //Update Pagination State
            _updatePaginationState: function(objData, listing) {

                var self = this,
                    lastPageNum,
                    nextButton = self.$el.find(self.defaultSettings.paginationButtons.next),
                    lastButton = self.$el.find(self.defaultSettings.paginationButtons.last),
                    prevButton = self.$el.find(self.defaultSettings.paginationButtons.previous),
                    firstButton = self.$el.find(self.defaultSettings.paginationButtons.first);

                if (objData.totalResults) {
                    lastPageNum = Math.ceil(objData.totalResults / self.searchModelData.paginationMap.itemsPerPage);
                } else if (objData.data && objData.data.recipeCount) {
                    lastPageNum = Math.ceil(objData.data.recipeCount / self.searchModelData.paginationMap.itemsPerPage);
                } else if (objData.data && objData.data.didYouMeanResultCount) {
                    lastPageNum = Math.ceil(objData.data.didYouMeanResultCount / self.searchModelData.paginationMap.itemsPerPage);
                }

                // If we're on the last page, disable next and last, else do the opposite
                if (self.pageNum === lastPageNum) {
                    nextButton.prop('disabled', 'disabled');
                    lastButton.prop('disabled', 'disabled');
                } else {
                    nextButton.prop('disabled', false);
                    lastButton.prop('disabled', false);
                }

                // If we're at the first page, disable previous and first, else do the opposite
                if (self.pageNum === self.defaultSettings.startPageNum) {
                    prevButton.prop('disabled', 'disabled');
                    firstButton.prop('disabled', 'disabled');
                } else {
                    prevButton.prop('disabled', false);
                    firstButton.prop('disabled', false);
                }

                // If the total count is less than items per page then hide
                if (objData.totalResults || objData.data) {
                    if (objData.totalResults <= self.searchModelData.paginationMap.itemsPerPage || (objData.data && objData.data.recipeCount && objData.data.recipeCount <= self.searchModelData.paginationMap.itemsPerPage) || (objData.data && objData.data.didYouMeanResultCount && objData.data.didYouMeanResultCount <= self.searchModelData.paginationMap.itemsPerPage)) {
                        self.$el.find(self.pagination).addClass('hidden');
                    } else {
                        self.$el.find(self.pagination).removeClass('hidden');
                    }
                }

            },

            //Expand Collapse Filters
            _callExpandCollapse: function(evt) {
                var service = new IEA.accordion(); // creates new instance
                service._handleExpandCollapse(evt);
            }

        });
    });

    return SearchListingV2;
});