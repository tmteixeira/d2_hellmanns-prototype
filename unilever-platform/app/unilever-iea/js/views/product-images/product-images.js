/*global define*/

define(['slick', 'commonService', 'animService'], function() {
    'use strict';

    var commonService = null,
        animService = null;

    var ProductImages = IEA.module('UI.product-images', function(productImages, app, iea) {

        _.extend(productImages, {

            defaultSettings: {
                productItem: '.js-product-images-thumbnail',
                productNav: '.js-product-images-nav',
                slidesToScrollThumbnail: 3,
                zoomModalNav: '.o-modal-nav',
                currentProductIndex : 0,
                currentImageIndex : 0
            },

            events: {
                'click .js-product-images-zoom' : '_openZoomModal',
                'click .o-modal-nav' : '_scrollZoomImage'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                // creates a new instance of the services
                commonService = new IEA.commonService();
                animService = new IEA.animService();

                this.componentJson = this.getModelJSON().productImages;
                this.settings = this.defaultSettings;
                this.currentImageIndex = this.settings.currentImageIndex;

                //init carousel method
                this._initCarousel();

                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/          

            /**
             * Initiate carousel for the product image and thumbnails
             * @method _initCarousel
             * @return string
             */
            _initCarousel: function() {
                var self = this,               
                productItem = self.$el.find(self.settings.productItem),
                productNav = self.$el.find(self.settings.productNav);

                $(productItem).slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: (commonService.isMobile() === true) ? false : true,
                    asNavFor: productNav
                });

                $(productNav).slick({
                    slidesToShow: (commonService.isMobile() === true) ? 1 : self.settings.slidesToScrollThumbnail,
                    slidesToScroll: 1,
                    asNavFor: productItem,
                    dots: (commonService.isMobile() === true) ? true : false,
                    focusOnSelect: true
                });
            },
            
            /**
             * opens the zoom modal based on the selected image
             * @method _openZoomModal
             * @return string
             */

            _openZoomModal: function () {
                this.currentImageIndex = this.$el.find(this.settings.productItem+ ' .slick-current').data('slick-index');
                animService.zoomOpenUrl(this._getZoomImage());
                $('body').addClass('open-zoom-modal');
            },

            /**
             * navigate zoomed images using next/previous buttons
             * @method _scrollZoomImage
             * @return string
             */
            _scrollZoomImage: function(evt) {
                var $this = $(evt.currentTarget),
                    $productNav = $(this.$el.find(this.settings.productNav));

                if ($this.data('type') === 'next') {
                    $productNav.slick('slickNext');
                } else {
                    $productNav.slick('slickPrev');
                }

                this._openZoomModal();
            },

            /**
             * returns the path to the full size product image
             * @method _getZoomImage
             * @return string
             */
            _getZoomImage: function () {
                var self = this,
                    currentData = self.componentJson.product.productsDetail,
                    currentImg = currentData[self.settings.currentProductIndex].images[self.currentImageIndex],
                    currentImgLength = currentData[self.settings.currentProductIndex].images.length,
                    $modalSliderNav = $(self.settings.zoomModalNav);

                if (currentImgLength > 1) {
                    $modalSliderNav.removeClass('hidden');
                } else {
                    $modalSliderNav.addClass('hidden');
                }

                //with extension
                return (currentImg.url+'.ulenscale.985x985.'+currentImg.extension);
            }
        });
    });

    return ProductImages;
});
