/*global define*/

define(['TweenMax', 'videoPlayerServiceV2', 'commonService'], function(TweenMax) {
    'use strict';

    var HeroV2 = IEA.module('UI.hero-v2', function(heroV2, app, iea) {

        var videoPlayerServiceV2 = null,
        commonService = null;

        _.extend(heroV2, {

            _isVideoOpened: false,

            /*********************PUBLIC METHODS******************************************/

            defaultSettings: {
                readMoreSelector: '.js-btn-readmore',
                toggleClass: 'is-open',
                readMoreCopy: '.c-hero-v2__longcopy',
                videoWrapper: '.js-video-player',
                videoHandler: '.js-player-handler',
                isVideoOpen: 'is-video-player__open',
                videoButton: '.js-btn-video',
                multipleVideoInstances: false
            },

            events: {
                'click .js-btn-readmore': '_toggleReadMore'
            },

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);

                // video service options
                var options = {},
                    self = this;                
                
                options = {
                    selector: self.defaultSettings.videoHandler,
                    videoWrapper : self.defaultSettings.videoWrapper,
                    view: self,
                    previewImage: '.js-preview-image',
                    multipleVideoInstances: self.defaultSettings.multipleVideoInstances
                };

                videoPlayerServiceV2 = new IEA.videoPlayerServiceV2();
                videoPlayerServiceV2.initVideoPlayer(options);

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                commonService = new IEA.commonService();
                //this._closeVideo();

                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/          

            _toggleReadMore: function(evt) {
                var self = this,
                $this = $(evt.currentTarget),
                settings = self.defaultSettings;

                evt.preventDefault();

                if ($this.is('.'+settings.toggleClass)) {
                    TweenMax.to(settings.readMoreCopy, 1, {height: 0});
                    $this.removeClass(settings.toggleClass);
                    
                    //Analytics read more button if collapsed
                    self._ctReadMore('close');
                } else {
                    TweenMax.set(settings.readMoreCopy, {height: 'auto'});
                    TweenMax.from(settings.readMoreCopy, 1, {height: 0});
                    $this.addClass(settings.toggleClass);

                    //Analytics read more button if expanded
                    self._ctReadMore('open');
                }
            },

            /**************************ANALYTICS******************************************/ 

            _ctComponentInfo: function() {
                var self = this,
                    compName = self.$el.find('[data-componentname]').data('componentname'),
                    compVar = self.$el.find('[data-component-variants]').data('component-variants'),
                    compPos = self.$el.find('[data-component-positions]').data('component-positions');
                
                digitalData.component = [];
                digitalData.component.push({
                    'componentInfo' :{
                        'componentID': compName,
                        'name': compName
                    },
                    'attributes': {
                        'position': compPos,
                        'variants': compVar
                    }
                });
            },

             /**
             * read more button tracking
             * @method _ctReadMore
             * @return 
             */

            _ctReadMore: function(openCloseStatus) {
                var ev = {};
            
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {                   
                    this._ctComponentInfo();                  
                    
                    ev.eventInfo={
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.readMore,
                        'eventLabel' : openCloseStatus
                    };
                       
                    ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
                    ev.category = {'primaryCategory': ctConstants.custom};
                    digitalData.event.push(ev);
                }
            },

            /**
             * Player state change callback with player states added in switch case
             * @method _playerStateChange
             * @return 
             */

            _playerStateChange: function(videoPlayer, state) {                
                var $player = $(videoPlayer.h);
                if (state) {
                    /*switch (state.data) {
                        case -1:
                            console.log('buffering');
                            break;
                        case 0:
                            console.log('video completes');
                            break;
                        case 1:
                            console.log('playing');
                            break;
                        case 2:
                            console.log('pause');
                            break;
                        case 3:
                            console.log('replay');
                            break;
                    }
                    */
                    // video analytics
                    this._ctTag($player.data('video-source'), $player.data('video-id'), state.data);
                }

                this.triggerMethod('playerStateChange');
            },

            /**
             * _onPlayerReady callback to check id youtube player is ready to use
             * @method _onPlayerReady
             * @return 
             */
            _onPlayerReady: function(videoPlayer) {
                //console.log(videoPlayer);
            },

            /**
             * You tube tracking to check the video progress
             * @method _progressTrack
             * @return 
             */
            _progressTrack: function(videoSource, videoId, progress) {
                var ev = {},
                    self = this;
                
                self._ctComponentInfo();

                ev.eventInfo = {
                  'type':ctConstants.trackEvent,
                  'eventAction': ctConstants.videoProgress,
                  'eventLabel' : videoSource + ' - '+ videoId + ' - ' + 'progressed '+ progress
                };

                ev.attributes = {'nonInteractive':{'nonInteraction': 1}};
                ev.category = {'primaryCategory':ctConstants.custom};
                digitalData.event.push(ev);
            },

            /**
             * Youtube play/completes tracking 
             * @method _ctTag
             * @return 
             */
            _ctTag: function(videoSource, videoId, state) {
                var ev = {};
                
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    
                    digitalData.video = [];

                    this._ctComponentInfo();

                    digitalData.video.push({
                        'videoId': videoId
                    });

                    if (state === 1) {
                        ev.eventInfo = {
                            'type': ctConstants.trackEvent,
                            'eventAction': ctConstants.videoPlays,
                            'eventLabel': videoSource + '-' + videoId
                        };
                    } else if (state === 0) {
                        ev.eventInfo={
                            'type': ctConstants.trackEvent,
                            'eventAction': ctConstants.videoCompletes,
                            'eventLabel': videoSource + '-' + videoId
                        };
                    }
                    ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
                    ev.category = {'primaryCategory':ctConstants.other};
                    digitalData.event.push(ev);
                }
            }
        });
    });

    return HeroV2;
});
