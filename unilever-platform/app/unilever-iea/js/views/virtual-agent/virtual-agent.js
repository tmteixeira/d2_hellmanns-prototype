/*global define*/

define(['TweenMax', 'typeahead', 'commonService', 'accordion', 'search'],function(TweenMax) {
    'use strict';

    var VirtualAgent = IEA.module('UI.virtual-agent', function(virtualAgent, app, iea) {
        
        var searchService = null,
            accordian=null,
            commonService = null;

        _.extend(virtualAgent, {
            
            defaultSettings: {
                searchWrapper: '.c-inline-search .c-global-search__wrapper',
                searchInputField: '.c-inline-search .typeahead',
                searchInputFieldId: '.tt-dataset-keywordSearch',
                clearSearch: '.c-inline-search .o-btn-clear-search',
                searchButton: '.c-inline-search .js-btn-search',
				searchForm : '#inlineSearch',
				preloader: '.virtual-agent-container__loadmore .o-preloader',
                virtualAgentResultContainer:'.result-container',
                template: 'virtual-agent/partial/virtual-agent-result.hbss',
                suggestionsLimit: 15,
                searchInputFont: 30,
                searchInputFontMobile: 20,
                speed: 400
            },
            
            events: {
                'click a.c-expandcollapse__link': '_callExpandCollapse',// expand and collapse for accordian
                'click .c-inline-search .js-btn-search': 'submitForm' // render result container for results on search button click
            },
            
            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                var self = this;

                searchService = new IEA.search(); // creates new instancefor common search
                commonService = new IEA.commonService(); // creates new instance for common service
                
                this._super(options);          
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }     
                this.triggerMethod('render');
                return this;
            },
            
            /**
            @description: Show the component
            @method show
            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            @description: Hide the component
            @method hide
            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean
            @return {null}
            **/
            clean: function() {
                this._super();
            },
            
            getQueryStringVal: function(name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
                return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            },
      
            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
               this.triggerMethod('beforEnable');

               var self = this,
                   options = {},
                   settings = self.defaultSettings;
                self.virtualAgentData = this.getModelJSON();
                self.searchModelData = self.virtualAgentData.virtualAgent;
                self.locale=self.searchModelData.locale;
                self.brandName = self.searchModelData.webAssistBrandName;
                self.searchWrapper = settings.searchWrapper;
                self.searchInputField = settings.searchInputField;
                self.searchInputFieldId= settings.searchInputFieldId;
                self.clearSearch = settings.clearSearch;
                self.searchButton = settings.searchButton;
                self.searchForm = settings.searchForm;
				self.virtualAgentResultContainer = settings.virtualAgentResultContainer;
                self.preloader = settings.preloader;

                // search parameters
                self.dropDownLabel = self.searchModelData.dropDownLabel;
                self.searchingKeyword = '';
                self.searchKeyword = '';
                self.searchKeywordId='';
                self.pageScroll = false;
                
                if (commonService.isMobile()) {
                    self.inputFont = settings.searchInputFontMobile;
                } else {
                    self.inputFont = settings.searchInputFont;
                }
                
                // show suggestions
                if ($(self.searchInputField).parent('.twitter-typeahead').length === 0) {
                    self.renderSuggestions();
                }
                
                // clear search on cross click
                searchService.clearSearchVal(self.clearSearch, self.searchInputField);
                
                this.triggerMethod('enable');  
            },

            /**************************PRIVATE METHODS******************************************/ 
            // animate results
            animateResultsFadeIn: function() {               
                TweenMax.to($(this.virtualAgentResultContainer), 0.8, {autoAlpha: 1, top: 0, delay: 0.2});
                $('.c-content-loader').fadeOut(1000);
            },

            // animate results
            animateResultsFadeout: function() {
                TweenMax.to($(this.virtualAgentResultContainer), 0.8, {autoAlpha: 0, top: '30px', delay: 0.2});
                $('.c-content-loader').fadeIn(1000);
            },
        
            /**
             * Auto complete
             * @method autoComplete
             * @return 
             */
            showSuggestions: function (options) {
                // constructs the suggestion engine
                var self = this,
                    dropdownLabel = options.dropDownLabel,
                    keyword = $(options.searchWrapper).attr('data-keyword'),
                    keywordSearchDefault, keywordSearch,
                    defaultSettings = {
                        selectCallback: function() {}
                    };
                options = $.extend(defaultSettings, options);
                $(options.searchInputField).typeahead({
                    hint: false,
                    highlight: true,
                    minLength: 3,
                    limit: 10,
                },
                                                 
                {
                    name: 'keywordSearch',
                    limit: self.defaultSettings.suggestionsLimit,
                    //beforeSelectedVal: this.query,
                    display: function(data) {
                       return data.title;
                    },
                    source: function (query, sync, async) { 
                        $.ajax(keyword+ '?qs='+query+'&locale='+options.locale+'&brand='+options.brandName+'&entity=webassist', {
                            dataType: 'JSONP',
                            type: 'GET',
                            success: function(data, status) { 
                                //options.suggestionsLimit = data.length;
                                $(self.preloader).removeClass('hidden');
                                if(data.length > 0){
                                    async(data);
                                    $(self.preloader).addClass('hidden');
                                }else{
                                    $(self.preloader).addClass('hidden');
                                }  
                            },
                        });   
                    },
                    templates: {
                        suggestion: function (data) {
                            return '<div id='+data.id+'><p>'+ data.title +'</p></div>';
                            /*return '<div><p>'+ data.title +'</p></div>';*/
                        }/*,
                        empty:  function (data) {
                            '<div class="noitems">',
                            'No Items Found',
                            '</div>'
                        }*/
                    }        
                }).on('keyup', function (event) {   
                    
                   if(event.which === 13){
                       self.submitForm(event);
                   } 
                    
                    var textLength = $(this).val().length;     
                    this.searchingKeyword=$(this).val();
                    var _this = $(this);
                    if (_this.typeahead('val').length > 0 && !_this.typeahead('val').match(/^\s*$/)) {
                        $(options.clearSearch).removeClass('hidden');
                    } else {
                        $(options.clearSearch).addClass('hidden');
                    }
                    if(textLength > 2){
                        $(self.preloader).removeClass('hidden');
                        searchService.shrinkToFill(_this.next('pre'), options.inputFont, _this);
                    }else{
                        $(self.preloader).addClass('hidden');
                    }
                    
                    //searchService.shrinkToFill(_this.next('pre'), options.inputFont, _this);
                    
                }).bind('typeahead:selected', function(obj, selected, name) {
                    var _this = $(this),
                        keywordVal = selected.title,
                        id = selected.id,
                        searchingKeyword = this.searchingKeyword,
                        //searchingKeyword = selected.title,
                        textLength = keywordVal.length;
            
                    _this.typeahead('close');  
                    options.selectCallback(options, keywordVal, id, searchingKeyword, _this);
                    $(self.preloader).addClass('hidden');
                    
                    _this.next('pre').text(options.searchKeyword);
                    _this.typeahead('val', options.searchKeyword);
                    _this.val(options.searchKeyword);
                    
                    //shrink font size on input keyup
                    searchService.shrinkToFill(_this.next('pre'), options.inputFont, _this); 
                });
            },
            
            renderSuggestions: function () {            
                // constructs the suggestion engine
                var self = this,
                    options = {};

                    options.dropDownLabel = self.dropDownLabel;
                    options.searchWrapper = self.searchWrapper;
                    options.searchInputField = self.searchInputField;
                    options.searchInputFieldId = self.searchInputFieldId;
                    options.customKeyword = self.defaultSettings.defaultSuggestions;
                    options.locale = self.locale;
                    options.brandName = self.brandName;
                    options.clearSearch = self.clearSearch;
                    options.searchKeyword = self.searchKeyword;
                    options.searchKeywordId = self.searchKeywordId;
                    options.searchingKeyword = self.searchingKeyword;
                    options.inputFont = self.inputFont;
                    options.suggestionsLimit = self.defaultSettings.suggestionsLimit;

                    options.selectCallback = function(options, keywordVal, id, searchingKeyword, obj) {
                        self.updateParams(keywordVal,id,searchingKeyword);
                        options.searchKeyword = self.searchKeyword;
                        options.searchKeywordId = self.searchKeywordId;
                        options.searchingKeyword = self.searchingKeyword;
                    } 
                    self.showSuggestions(options);            
            },

            // shrink font size on type
            resizeFont: function(obj, fontsize, field) {
                searchService.shrinkToFill(obj, fontsize, field);
            },
            
            /**
            @description: Backbone.js Bind Callback to Successful Model Fetch
            @method _searchView
            @return {null}
            **/

            _populateData: function() {
                /* Backbone.js Bind Callback to Successful Model Fetch */
                var self = this,
                    options = {},
                    contentType;
                
                options.servletUrl = self.searchModelData.retrivalQueryParams.requestServlet;
                options.locale=self.searchModelData.locale;
                options.brandName=self.searchModelData.webAssistBrandName;

                if (self.contentType === '') {
                    contentType = '';
                } else {                    
                    contentType = '&fq=ContentType:'+self.contentType;
                }
                
                $.ajax({
                    url: options.servletUrl+'/'+self.searchingKeyword+'/'+'?id='+self.searchKeywordId+'&title='+self.searchKeyword+'&locale='+options.locale+'&brand='+options.brandName+'&entity=webassist',
                    dataType: 'JSONP',
                    type: 'GET',
                    success: function(data, status) {
                        self.animateResultsFadeIn();
                        self._generateVirtualAgentResult(data);
                    },
                });    
            },
            
            /**
            @description: creates search result item view
            @method _searchView
            @return {null}
            **/
            _generateVirtualAgentResult: function (obj) {
                var self = this,
                    //objData = $.parseJSON(obj),
                    //listing = '', noResultsBlock = '', dataLength,
                    template = self.getTemplate('virtual-agent-result',self.defaultSettings.template), 
                    componentJSON = null,
                    finalData = null;
                componentJSON = {
                    'virtualAgentResult': obj.data,
                    'virtualAgent': self.virtualAgentData.virtualAgent
                };  
                finalData = componentJSON;
                $(self.preloader).addClass('hidden');
                $('.result-container').html(template(finalData));
            },
			
			/**
            @description: accordian calling
            @method show
            @return {null}
            **/
            _callExpandCollapse: function(evt, options) {
                accordian = new IEA.accordion(); // creates new instance
                accordian._handleExpandCollapse(evt, options);
            },
            
            /**
            @description: render search result item view and update DOM
            @method _renderSubView
            @return {null}
            **/ 

            submitForm: function(e) {                   
                var self = this,
                    keywordVal = $(self.searchInputField).val(),
                    id = $(self.searchInputFieldId).find('.tt-selectable').prop('id'),
                    searchingKeyword= this.searchingKeyword;
                    e.preventDefault();
                
                if (keywordVal === '' || keywordVal.match(/^\s*$/)) {
                    $(self.searchInputField).focus();
                } else {  
                    self.animateResultsFadeout();
                    // virtual agent get results 
                    self.updateParams(keywordVal,id,searchingKeyword);
                    $(self.preloader).removeClass('hidden');
                    //virtual agent results will display
                    self._populateData();
					self.trackFormSubmission(id, keywordVal);
                }
                return false;
            },
            
            // for updating parameters value
            updateParams: function(keywordVal,id,searchingKeyword) {
               this.searchKeyword = keywordVal;
               this.searchKeywordId = id;
               this.searchingKeyword = searchingKeyword;
            },
			
			trackFormSubmission: function(id, keywordVal){
				var ev = {},
					compName = $('.virtual-agent-container').parents('[data-componentname]').data('componentname'),
					compVariants = $('.virtual-agent-container').parents('[data-component-variants]').data('component-variants'),
					compPositions = $('.virtual-agent-container').parents('[data-component-positions]').data('component-positions');
				
				digitalData.component = [];
				digitalData.component.push({
					'componentInfo' :{
						'componentID': compName,
						'name': compName
					},
					'attributes': {
						'position': compPositions,
						'variants': compVariants
					}
				});
				
				ev.eventInfo={
				  'type':ctConstants.trackEvent,
				  'eventAction': ctConstants.virtualagent,
				  'eventLabel' : id + ' - ' + keywordVal
				};
				ev.category ={'primaryCategory':ctConstants.custom};
				digitalData.event.push(ev);
			}
			
        });
    });
    return VirtualAgent;
});
