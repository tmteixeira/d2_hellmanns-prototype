/*global define*/

define(function() {
    'use strict';

    var CallOutButton = IEA.module('UI.call-out-button', function(callOutButton, app, iea) {

        _.extend(callOutButton, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
				this.triggerMethod('beforEnable');

                var self = this,
                    $selector = $('.js-btn-checkout'),
					maximumItem = $('.c-shopping-basket__wrapper').attr('data-max'),
                    errorMessage = $('.c-shopping-basket__title').attr('data-error'),
                    $quantityWrap = $(".c-shopping-basket__quantitywrap");  
                        
                $selector.click(function(e) {
                    var totalQuantity = 0,
                        $this = $(this);
                    // check if total items is less than equal to max
                    // get total quantity of all the products.
                    $(".quantitywrap__cta-quantity").each(function(index, value) {
                        totalQuantity += parseInt($(value).text(), 10);
                    });

                    if (totalQuantity > parseInt(maximumItem) || totalQuantity < 1) {
                        if (!$quantityWrap.find(".shopping-basket-error").length) {
							var error = $('<div/>').addClass('shopping-basket-error');
							error.text(errorMessage); 
							$quantityWrap.prepend(error);	
						}
                        e.preventDefault();
						$this.addClass('disabled');	
                        self._scrollToTop();						
                    }					
                });

                this.triggerMethod('enable'); 
            },
			

            /**************************PRIVATE METHODS******************************************/          

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */
			_scrollToTop:function() {
				$('html, body').animate({
					scrollTop : $('.shopping-basket-error:first').offset().top - 200
				}, 1000);  
			}

        });
    });

    return CallOutButton;
});
