/*global define*/

define(['TweenMax', 'jquery.hammer', 'commonService', 'socialTabService'], function() {
    'use strict';

    var SocialGalleryV2 = IEA.module('UI.social-gallery-v2', function(socialGalleryV2, app, iea) {

        var commonService = null,
            socialTabService = null;

        _.extend(socialGalleryV2, {

            defaultSettings: {
                socialCarouselList: '.js-social-v2-partial-list',
                socialGalleryItem: '.js-social-v2-partial-item',
                listing: '.c-social-gallery-v2__list',
                carouselList: '.c-social-v2-carousel__list',
                quickPanelWrapper: '.js-social-gallery-v2-panel',
                pagination: '.js-social-gallery-v2-pagination',
                featuredPostList: '.js-featured-post__list',
                galleryThumbnail: '.js-social-gallery-v2__thumbnail',
                carouselThumbnail: '.js-social-v2-carousel__thumbnail',
                listingTemplate: 'social-gallery-v2/partial/gallery-listing.hbss',
                paginationTemplate: 'pagination/defaultView.hbss',
                featuredPostListingTemplate: 'social-gallery-v2/partial/featured-post-listing.hbss',
                carouselListingTemplate: 'social-gallery-v2/partial/carousel-listing.hbss',
                prevButton: '.js-social-carousel__arrow-prev',
                nextButton: '.js-social-carousel__arrow-next',
                closeButton: '.js-btn-social-carousel__close',
                animateSpeed: 1000,
                preloaderContent: '.o-preloader--content',
                pageNum: 1,
                startIndex: 1,
                pageStartIndex: 1,
                col: 3,
                isPageLoad: true,
                itemMargin: 30,
                disableHotKey: true,
                paginationBtn: '.js-btn-pagination',
                isLoadMore: false,
                loadmoreWrapper: '.js-social-gallery-v2__loadmoreWrapper',
                loadmore: '.js-social-gallery-v2__loadmore'
            },

            events: {
                'click .js-social-v2-partial-item': '_showQuickPanel', //show quick panel on item click
                'click .js-btn-social-gallery-v2-panel-close' : '_closePanel', //hide quick panel
                'click .js-social-carousel__item.active' : '_closePanelonActiveSlide', //hide quick panel
                'click .js-social-carousel__arrow' : '_navigateToItem', // navigate quick panel through next/prev button
                'click .js-btn-pagination' : '_paginationHandler',
                'click .js-social-gallery-v2__loadmore' : '_showAllHandler'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);

                commonService = new IEA.commonService(); // creates new instance for common service
                socialTabService = new IEA.SocialTabs(); // creates new instance for Social tab service

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                this.componentJson = this.getModelJSON().socialGalleryV2;
                this.listing = this.$el.find(this.defaultSettings.listing);
                this.preloaderContent = this.$el.find(this.defaultSettings.preloaderContent);
                this.pagination = this.$el.find(this.defaultSettings.pagination);
                this.quickPanelWrapper = this.$el.find(this.defaultSettings.quickPanelWrapper);
                this.featuredPostList = this.$el.find(this.defaultSettings.featuredPostList);
                this.carouselList = this.$el.find(this.defaultSettings.carouselList);
                this.socialCarouselList = this.$el.find(this.defaultSettings.socialCarouselList);
                this.navClickPrev = 1;
                this.navClickNext = 1;
                this.pageNum = this.defaultSettings.pageNum;
                this.isPageLoad = this.$el.find(this.defaultSettings.isPageLoad);
                this.startIndex = this.defaultSettings.startIndex;
                this.isLoadMore = this.defaultSettings.isLoadMore;

                if (this.componentJson.pagination.paginationType === 'infiniteScroll') {
                    // show/hide show more button on page scroll
                    this._handleScroller();
                }

                // render social gallery listing
                this._renderListing();

                this.triggerMethod('enable');
            },


            /**************************PRIVATE METHODS******************************************/

            /**
             * fetch data from backend service based on the query parameters
             * @method _renderListing
             * @return
            */
            _renderListing: function() {
                var self = this,
                    options = {},
                    isLocalEnv = $('.local-env').length; //this is for FE local build only.

                options = {
                    componentJson: self.componentJson,
                    startIndex: self.isPageLoad ? self.defaultSettings.startIndex : self.startIndex,
                    servletUrl: self.componentJson.retrivalQueryParams.requestServletUrl,
                    isTypePost: isLocalEnv ? false : true
                };

                if(self.componentJson.componentVariation === 'carousel') {
                    options.noLazyLoad = true;
                }

                if (isLocalEnv) {
                   options.queryParams = '?';
                } else {
                   options.queryParams = socialTabService.setDataParameters(options);
                }

                if (options.queryParams !== '') {
                    options.onCompleteCallback = function(data) {
                        if(data) {
                            socialTabService.animateResults(self.listing, self.preloaderContent, self.defaultSettings.animateSpeed, 'show');
                            var actualData = $.parseJSON(data);
                            if (self.componentJson.retrivalQueryParams.limitResultsTo !== 0 && actualData.responseData.totalResult > self. componentJson.retrivalQueryParams.limitResultsTo) {
                            self.totalResults = self.componentJson.retrivalQueryParams.limitResultsTo;
                            } else {
                                self.totalResults = actualData.responseData.totalResult;
                            }

                            if(self.componentJson.componentVariation === 'social-gallery') {
                                var loopEndValue = (self.totalResults >= self.componentJson.pagination.itemsPerPage*self.pageNum) ? self.componentJson.pagination.itemsPerPage : (self.totalResults % self.componentJson.pagination.itemsPerPage);

                                options.responseData = {};
                                options.responseData.results = [];
                                options.responseData.totalResult = '';

                                for (var i = 0; i < loopEndValue; i++){
                                    options.responseData.results.push(actualData.responseData.results[i]);
                                }
                                options.responseData.totalResult = actualData.responseData.totalResult;

                                self._generateListing(JSON.stringify(options));
                            } else {
                                self._generateListing(JSON.stringify(actualData));
                            }
                        }
                    };

                    commonService.getDataAjaxMethod(options);
                }
            },

            /**
             * render listing based on filtered data
             * @method _generateListing
             * @return
            */
            _generateListing: function(data) {
                var self = this,
                    serviceData = $.parseJSON(data),
                    listingTemplate = self.getTemplate('gallery-listing', self.defaultSettings.listingTemplate),
                    paginationTemplate = self.getTemplate('pagination', self.defaultSettings.paginationTemplate),
                    featuredPostListingTemplate = self.getTemplate('featured-post-listing', self.defaultSettings.featuredPostListingTemplate),
                    carouselListingTemplate = self.getTemplate('carousel-listing', self.defaultSettings.carouselListingTemplate),
                    combinedJson = null,
                    queryParamJson = self.componentJson.retrivalQueryParams,
                    hashTags = '';

                    self.itemsPerPage = self.componentJson.pagination.itemsPerPage;

                    var totalPages = Math.ceil(self.totalResults/self.itemsPerPage),
                        paginationList = [];

                    for(var i=0; i < totalPages; i++) {
                        paginationList.push(i);
                    }

                if ($(queryParamJson.hashTags).length > 0) {
                    $(queryParamJson.hashTags).each(function(key, val) {
                        if (key === 0) {
                            hashTags += val.Id;
                        } else {
                            hashTags += '|' +val.Id;
                        }
                    });
                }

                combinedJson = {
                    'componentJson': self.componentJson,
                    'serviceData': serviceData.responseData,
                    'pageNum': self.isPageLoad ? self.defaultSettings.pageNum : self.pageNum,
                    'totalPages': Math.ceil(self.totalResults/self.itemsPerPage),
                    'hashTags': hashTags,
                    'paginate': paginationList,
                    'startIndex': self.isPageLoad ? self.defaultSettings.startIndex : self.startIndex
                };

                $(self.preloaderContent).addClass('hidden');
                $(self.featuredPostList).append(featuredPostListingTemplate(combinedJson));
                $(self.carouselList).append(carouselListingTemplate(combinedJson));

                // render social gallery listing
                if (self.componentJson.pagination.paginationType === 'infiniteScroll') {
                    $(self.listing).append(listingTemplate(combinedJson));
                } else {
                    $(self.listing).html(listingTemplate(combinedJson));
                }

                // render pagination
                if (self.isPageLoad) {
                    $(self.pagination).append(paginationTemplate(combinedJson));
                }

                // carousel init
                if (self.defaultSettings.pageNum === 1 && self.$el.find(self.defaultSettings.carouselThumbnail).length) {

                    self._initCarousel();

                    if (self.totalResults > 1) {
                        self.$el.find(self.defaultSettings.nextButton).fadeIn(self.defaultSettings.animateSpeed);
                        self.$el.find(self.defaultSettings.nextButton).fadeOut(3000);
                    }
                }
            },

            /**
             * show/hide quick panel
             * @method _showQuickPanel
             * @return
            */
            _showQuickPanel: function(evt) {
                evt.preventDefault();

                var _this = $(evt.target),
                    self = this,
                    settings = self.defaultSettings,
                    active = 'active',
                    $currentItem = _this.parents(settings.socialGalleryItem),
                    currentIndex = $currentItem.data('index'),
                    $currentQuickPanel = self.$el.find(settings.quickPanelWrapper+'[data-index='+currentIndex+']');

                socialTabService.setActiveParams(self.$el, settings, parseInt(currentIndex));

                $currentItem.addClass(active);

                if(_this.parents(settings.carouselList).length) {
                    $currentQuickPanel.addClass(active);
                } else {
                    self.$el.find(settings.socialGalleryItem).each(function (index, el) {
                        var panelHeight = 0,
                            cols = 3,
                            row = currentIndex - currentIndex % cols;

                        if (index >= row && index <= row + (cols - 1)) {
                            panelHeight = $currentQuickPanel.height();
                        }
                        if (panelHeight !== 0) {
                            $(el).css("margin-bottom", panelHeight+ settings.itemMargin*2);
                        }
                    });

                    $currentQuickPanel.addClass(active).css('top', $currentItem.position().top + $currentItem.height() + settings.itemMargin);
                }

                TweenMax.to(window, 0.5, {scrollTo: {y: $currentItem.offset().top + $currentItem.height()/2}});

                settings.disableHotKey = false;

                //keyword keys handler
                this._navigateThroughKeys($currentItem, currentIndex, $(settings.socialGalleryItem).length);
            },

            /**
             * open quick panel on clicking on current item
             * @method _navigateToItem
             * @return
            */
            _navigateToItem: function(evt) {
                evt.preventDefault();

                var _this = $(evt.target),
                    self = this,
                    isCarouselClicked = false,
                    settings = self.defaultSettings,
                    $currentItem = _this.parents(settings.quickPanelWrapper),
                    currentIndex = $currentItem.data('index'),
                    itemLength = self.$el.find(settings.socialGalleryItem).length,
                    slickActiveLength = self.$el.find(settings.socialGalleryItem +'.slick-active').length;

                if (_this.parents(settings.carouselList).length) {

                    if (_this.data('role') === 'next') {
                        currentIndex = currentIndex + 1;
                        var nextItemToScroll = (itemLength >= slickActiveLength*self.navClickNext) ? (slickActiveLength*self.navClickNext) : itemLength;

                        if (currentIndex === nextItemToScroll) {
                            self.$el.find('.slick-next').click();
                            isCarouselClicked = true;
                        }
                    } else if (_this.data('role') === 'prev') {

                        if (currentIndex === slickActiveLength || currentIndex === itemLength - slickActiveLength*self.navClickPrev) {
                            self.$el.find('.slick-prev').click();
                            isCarouselClicked = true;
                        }
                        currentIndex = currentIndex - 1;
                    }

                    if (!isCarouselClicked) {
                        self.$el.find(settings.socialGalleryItem+'[data-index='+currentIndex+'] '+settings.carouselThumbnail).click();
                    }
                } else {
                    if (_this.data('role') === 'next') {
                        currentIndex = currentIndex + 1;
                    } else if (_this.data('role') === 'prev') {
                        currentIndex = currentIndex - 1;
                    }
                    self.$el.find(settings.socialGalleryItem+'[data-index='+currentIndex+'] '+settings.galleryThumbnail).click();
                }
            },

            /**
             * close quick panel
             * @method _closePanel
             * @return
            */
            _closePanel: function(evt) {
                evt.preventDefault();
                var _this = $(evt.target);

                socialTabService.closePanel(this.$el, this.defaultSettings, _this);
            },

            /**
             * navigate quick panel through next/previous button
             * @method _closePanel
             * @return
            */
            _navigateThroughKeys: function(item, index, last) {
                var self = this,
                    settings = self.defaultSettings;

                // Hotkeys for navigation
                $(document).off('keyup').on('keyup', function(e) {
                    if (e.keyCode === 37 && !settings.disableHotKey && index > 0) {
                         socialTabService.prevItem(settings, index);
                    }
                    if (e.keyCode === 39 && !settings.disableHotKey && index < last) {
                        socialTabService.nextItem(settings, index);
                    }
                    if (e.keyCode === 27) {
                        self._setParams(settings.socialGalleryItem, settings.quickPanelWrapper, index);
                        TweenMax.to(window, 0.5, {scrollTo: {y: item.offset().top - item.height()/2}});
                    }
                });

                // Touch gestures with hammerjs for navigation
                self.$el.hammer().off('swipeleft').on('swipeleft', function(e) {
                    if (!settings.disableHotKey) {
                        socialTabService.prevItem(settings, index);
                    }
                }).off('swiperight').on('swiperight', function(e) {
                    if (!settings.disableHotKey) {
                        socialTabService.nextItem(settings, index);
                    }
                });
            },

            /**
             * social gallery carousel view
             * @method _initCarousel
             * @return
            */
            _initCarousel: function() {
                var self = this,
                settings = self.defaultSettings,
                carouselList = self.$el.find(settings.socialCarouselList),

                itemToShowCarousel,
                socialGalleryCarouselItem = carouselList.find(settings.socialGalleryItem).attr('class').split(' ')[0];

                if (commonService.isMobile()) {
                    itemToShowCarousel = 3;
                }  else {
                    itemToShowCarousel = 6;
                }

                carouselList.each(function() {
                    $(this).slick({
                        infinite: false,
                        arrows: true,

                        slidesToShow: itemToShowCarousel,
                        slidesToScroll: itemToShowCarousel,
                        slide: '.'+socialGalleryCarouselItem
                    })
                    .on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                        var $activeSlide = self.$el.find(settings.socialGalleryItem+'.active'),
                            direction, currentIndex;

                        if (Math.abs(nextSlide - currentSlide) === 1) {
                            direction = (nextSlide - currentSlide > 0) ? 'right' : 'left';
                        } else {
                            direction = (nextSlide - currentSlide > 0) ? 'left' : 'right';
                        }

                        if (currentSlide === 0) {
                            self.navClickNext = 1;
                            self.navClickPrev = 1;
                        }

                        if ($activeSlide.length) {
                            if (direction === 'left') {
                                currentIndex = nextSlide;
                                self.navClickNext++;

                                if (self.navClickPrev > 1) {
                                    self.navClickPrev--;
                                }
                            } else if (direction === 'right') {
                                currentIndex = currentSlide - 1;
                                self.navClickPrev++;

                                if (self.navClickNext > 1) {
                                    self.navClickNext--;
                                }
                            }
                            self.$el.find(settings.socialGalleryItem+':eq('+currentIndex+') '+settings.carouselThumbnail).click();
                        }
                    });
                });
            },

            /**
             * _paginationHandler
             * @method _paginationHandler
             * @return
            */
            _paginationHandler: function(evt) {
                var self = this,
                    $this = $(evt.currentTarget);

                 if ($this.hasClass('o-btn--disabled') || $this.hasClass('o-btn--active')) {
                    evt.preventDefault();
                    return false;
                }

                $(self.preloaderContent).height($(self.listing).height()).removeClass('hidden').show();
                TweenMax.to(window, 0.5, {scrollTo: {y: $(self.$el).offset().top}});

                self.isPageLoad = false;

                //pagination handler common service
                commonService.pagination(self.defaultSettings.paginationBtn, $this, self);

                // render search listing
                self._renderListing();
            },

            /**
             * load more handler
             * @method _showAllHandler
             * @return
            */
            _showAllHandler: function(evt) {
                var self = this,
                itemsPerPage = self.componentJson.pagination.itemsPerPage,
                $this = $(evt.target);

                evt.preventDefault();
                self.pageNum++;

                self.startIndex = self.startIndex + itemsPerPage;
                self.isPageLoad = false;
                self.isLoadMore = true;

                $this.fadeOut(self.defaultSettings.animateSpeed);
                $(self.defaultSettings.loadmoreWrapper).find(self.defaultSettings.preloaderContent).removeClass('hidden');

                // render search listing
                self._renderListing();
            },

            /**
            @description: handle load more click event
            @method: _handleScroller
            @return: {null}
            **/

            _handleScroller: function() {
                var self = this,
                    settings = self.defaultSettings;

                document.addEventListener('scroll', function (event) {
                    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop,
                    $parentElm = (self.$el.parents('.component-wrapper').next('footer').length) ? self.$el.next('footer') : self.$el.parents('.component-wrapper'),
                    $nextElm = (self.$el.next('footer').length) ? self.$el.next('footer') : self.$el.next('div[data-role]'),
                    $scrollElm = (self.$el.parents('.component-wrapper').length) ? $parentElm : $nextElm,
                    pageOffsetBottom = document.body.scrollHeight - $scrollElm.offset().top,
                    scrollOffset = scrollTop + window.innerHeight + pageOffsetBottom - 100;

                    if ((document.body.scrollHeight <= scrollOffset) && (self.pageNum >= 2) && self.isLoadMore === true) {
                        self.isLoadMore = false;

                        if(self.totalResults >= self.componentJson.pagination.itemsPerPage*self.pageNum) {
                            $(settings.loadmore).click();
                        }
                    }
                });
            }
        });
    });

    return SocialGalleryV2;
});