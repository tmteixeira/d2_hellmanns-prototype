/*global define*/

define(function() {
    'use strict';

    var languageSelector = IEA.module('UI.language-selector', function(languageSelector, app, iea) {

        _.extend(languageSelector, {
			'$languageSelect': '.c-language-selector__select',

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },
			
			selectCountry: function() {
				var self = this;
				$(self.$languageSelect).on('change', function () {
					window.open(this.options[ this.selectedIndex ].value, '_self');
				});
			},

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {				
				this.triggerMethod('beforEnable');

                this.selectCountry();

                this.triggerMethod('enable');				
            }  


            /**************************PRIVATE METHODS******************************************/          

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */

        });
    });

    return languageSelector;
});
