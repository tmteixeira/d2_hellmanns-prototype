/*global define*/

define(['TweenMax', 'commonService', 'search', 'ratings'], function(TweenMax) {
    'use strict';

    var SearchListing = IEA.module('UI.search-listing', function(searchListing, app, iea) {

        var searchService = null,
            ratingService = null,
            commonService = null;

        _.extend(searchListing, {

            defaultSettings: {
                searchWrapper: '.c-inline-search .c-global-search__wrapper',
                searchInputField: '.c-inline-search .typeahead',
                clearSearch: '.c-inline-search .o-btn-clear-search',
                searchButton: '.c-inline-search .js-btn-search',
                loadmore: '.js-btn-loadmore',
                filterSl: '.c-search-filters__select',
                searchContainer: '.c-search-listing__wrap',
                totalRecordEl: '.c-search-listing__total',
                searchListingSl: '.c-search-listing__list',
                searchForm : '#inlineSearch',
                noSearchResultWrapper : '.c-inline-search__no-results',
                activeclass: 'is-active',
                isLoadMore: false,
                pageNum: 1,
                contentType: '',
                pageLoad: true,
                preloader: '.c-search-listing__loadmore .o-preloader',
                template: 'search-listing/partial/search-result-list.hbss',
                templateNoData: 'search-listing/partial/no-search-result.hbss',
                defaultSuggestions: true,
                searchInputFont: 30,
                searchInputFontMobile: 16,
                suggestionsLimit: 6
            },

            events: {
                'change .c-search-filters__select' : 'onDropdownChange', //update params and render search results on dropdown change
                'click .js-btn-loadmore' : 'showAllResultsHandler', // render results on show all button click
                'focus keypress .c-inline-search .typeahead' : 'keyboardHandler', // capture keyboard events
                'click .c-inline-search .js-btn-search': 'submitForm', // render search listing on search button click
                'submit #inlineSearch': 'submitForm'
            },
            
            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                
                searchService = new IEA.search(); // creates new instance for search service
                ratingService = new IEA.ratings(); // creates new instance for rating service
                commonService = new IEA.commonService(); // creates new instance for common service
                
                this._super(options);

                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
            @description: Show the component
            @method show
            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            @description: Hide the component
            @method hide
            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean
            @return {null}
            **/
            clean: function() {
                this._super();
            },

            getQueryStringVal: function(name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
                return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
				this.triggerMethod('beforEnable');

                var self = this,
                    options = {},
                    settings = self.defaultSettings;
					
				self.searchListingData = this.getModelJSON()
                self.searchModelData = self.searchListingData.searchListing;

                // required parameters for rating service
                self.ratingReview = self.searchModelData.review;
				self.brandName = self.searchModelData.brandName;
               
                self.isLoadMore = settings.isLoadMore;
                self.filterSelect = settings.filterSl;
                self.searchWrapper = settings.searchWrapper;
                self.searchContainer = settings.searchContainer;
                self.searchInputField = settings.searchInputField;
                self.clearSearch = settings.clearSearch;
                self.searchButton = settings.searchButton;
                self.searchForm = settings.searchForm;
                self.loadmore = settings.loadmore;
                self.searchListingSl = settings.searchListingSl;
                self.noSearchResultWrapper = settings.noSearchResultWrapper;
                self.preloader = settings.preloader;

                // search parameters
                self.pageLoad = settings.pageLoad;
                self.pageNum = settings.pageNum;
                self.contentType = settings.contentType;
                self.locale = self.searchModelData.market;
                self.dropDownLabel = self.searchModelData.dropDownLabel;
                self.searchKeyword = '';
                self.pageScroll = false;

                if (commonService.isMobile()) {
                    self.inputFont = settings.searchInputFontMobile;
                } else {
                    self.inputFont = settings.searchInputFont;
                }

                // show suggestions
                if ($(self.searchInputField).parent('.twitter-typeahead').length === 0) {
                    self.renderSuggestions();
                }
                
                /*Load first set of record*/
                self.setKeywordValOnLoad();
                
                // render search listing on page load
                self._populateData();
                
                // clear search on cross click
                searchService.clearSearchVal(self.clearSearch, self.searchInputField);

                // show/hide load more on page scroll
                self.handleLoadMore();

                // Ratings Integration 
                //ratingService.initRatings(options);

                this.triggerMethod('enable'); 
            },

            /**************************PRIVATE METHODS******************************************/  

            // set the search parameter on dropdown change
            onDropdownChange: function(ev) {
                var self = this,
                    $this = $(ev.target);

                ev.preventDefault();
                self.isLoadMore = false;
                self.pageLoad = false;
                self.pageNum = 1;
                self.animateResultsFadeout();
                self.contentType = $this.val();

                // render search listing
                self._populateData();
            },

            showAllResultsHandler: function(e) {
                var self = this,
                    $this = $(e.target),
                    ev = {},
					compName = $this.parents('[data-componentname]').data('componentname'),
					compVariants = $this.parents('[data-component-variants]').data('component-variants'),
					compPositions = $this.parents('[data-component-positions]').data('component-positions');

                e.preventDefault();

                self.pageNum = self.pageNum + 1;
                $this.fadeOut(500);
                self.pageLoad = false;
                $(self.preloader).removeClass('hidden');
                self.isLoadMore = true;

                // render search listing
                self._populateData();
                
                // load more analytics
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    digitalData.component = [];	
					digitalData.component.push({
						'componentInfo' :{
							'componentID': compName,
							'name': compName
						},
						'attributes': {
							'position': compPositions,
							'variants': compVariants
						}
					});
					ev.eventInfo = {
                        'type':ctConstants.trackEvent,
                        'eventAction': ctConstants.loadMore,
                        'eventLabel' : self.contentType
                    };
                    ev.attributes={'nonInteraction':1};
                    ev.category ={'primaryCategory':ctConstants.custom};
                    digitalData.event.push(ev);    
                } 
            },

            // handle search on keyboard handler events
            keyboardHandler: function(e) {
                var self = this,
                    kCode = e.keyCode || e.charCode, //for cross browser
                    $this = $(e.target);
                
                if (kCode === 13) { //Capture Enter Key
                    if ($this.val() === '' || $this.val().match(/^\s*$/)) {                           
                        $(self.searchInputField).focus();
                        $(self.loadmore).fadeOut(500);
                    } else {
                        self.animateResultsFadeout();
                        self.isLoadMore = false;
                        self.pageLoad = true;
                        $this.typeahead('close');
                        
                        // update query parameters
                        self.updateParams($this.val());
                        
                        // render search listing
                        self._populateData();
                        
                        // search analytics
                        if(typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                            self.searchAnalytics();    
                        }
                    }
                    return false;
                }
            },

            // search Analytics
            searchAnalytics: function() {
                
                if(typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    var ev = {},
						compName = $(".c-search-listing").parents('[data-componentname]').data('componentname'),
						compVariants = $(".c-search-listing").parents('[data-component-variants]').data('component-variants'),
						compPositions = $(".c-search-listing").parents('[data-component-positions]').data('component-positions');
                    
                    digitalData.component.push({
						'componentInfo' :{
							'componentID': compName,
					   		'name': compName
						},
						'attributes': {
							'position': compPositions,
							'variants': compVariants
						}
					});
					
					ev.eventInfo = {
                        'type':ctConstants.trackEvent,
                        'eventAction': ctConstants.siteSearch,
                        'eventLabel' : this.searchKeyword
                    };
                    ev.category ={'primaryCategory':ctConstants.other};
                    digitalData.event.push(ev);
                    digitalData.component = [];
                }
            },

            // set value from qyery strings in search input on page load
            setKeywordValOnLoad: function() {
                var self = this;
                // page load search result
                self.searchKeyword = self.getQueryStringVal('q');
                var contentType = self.getQueryStringVal('fq');

                if (contentType.indexOf('ContentType') !== -1) {
                    var queryS = contentType.split('ContentType:')[1];
                    self.contentType = queryS;
                }
 
                 if (self.searchKeyword !== '') {
                    $(self.searchInputField).val(self.searchKeyword);
                    $(self.searchInputField).next('pre').text(self.searchKeyword);
                    $(self.clearSearch).removeClass('hidden');
                    self.resizeFont($(self.searchInputField).next('pre'), self.inputFont, $(self.searchInputField));
                }
            },

            // animate resuls
            animateResultsFadeIn: function() {               
                TweenMax.to($(this.searchContainer), 0.8, {autoAlpha: 1, top: 0, delay: 0.2});
                $('.c-content-loader').fadeOut(500);
            },

            // animate resuls
            animateResultsFadeout: function() {
                TweenMax.to($(this.searchContainer), 0.8, {autoAlpha: 0, top: '30px', delay: 0.2});
                $('.c-content-loader').fadeIn(500);
            },
        
            /**
             * Auto complete
             * @method autoComplete
             * @return 
             */

            renderSuggestions: function () {            
                // constructs the suggestion engine
                var self = this,
                    options = {};

                    options.dropDownLabel = self.dropDownLabel;
                    options.searchWrapper = self.searchWrapper;
                    options.searchInputField = self.searchInputField;
                    options.customKeyword = self.defaultSettings.defaultSuggestions;
                    options.locale = self.locale;
                    options.brandName = self.brandName;
                    options.clearSearch = self.clearSearch;
                    options.searchKeyword = self.searchKeyword;
                    options.inputFont = self.inputFont;
                    options.suggestionsLimit = self.defaultSettings.suggestionsLimit;

                    options.selectCallback = function(options, keywordVal, obj) {
                        self.updateParams(keywordVal);
                        options.searchKeyword = self.searchKeyword;
                        self.isLoadMore = false;
                        self.animateResultsFadeout();
                        self.pageLoad = true;

                        self._populateData();

                        if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                            self.searchAnalytics();    
                        }
                    } 

                    searchService.showSuggestions(options);             
            },

            // shrink font size on type
            resizeFont: function(obj, fontsize, field) {
                searchService.shrinkToFill(obj, fontsize, field);
            },

            /**
            @description: Backbone.js Bind Callback to Successful Model Fetch
            @method _searchView
            @return {null}
            **/

            _populateData: function() {
                /* Backbone.js Bind Callback to Successful Model Fetch */
                var self = this,
                    options = {},
                    contentType;

                if (self.contentType === '') {
                    contentType = '';
                } else {                    
                    contentType = '&fq=ContentType:'+self.contentType;
                }
                
                options.queryParams = '?q='+self.searchKeyword+contentType+'&Locale='+self.locale+'&BrandName='+self.brandName+'&PageNum='+self.pageNum;

                options.servletUrl = $(self.searchContainer).data('ajax-url');

                options.onCompleteCallback = function(data) {
                    self.animateResultsFadeIn();
                    self._generateSearchList(data);
                };

                commonService.getDataAjaxMethod(options);
            },

            updateContentTypeInDropdwdown: function(data) {
                var self = this,
                    filterAllLabel = self.searchModelData.filterLabel.listing[0].value,
                    $searchFilters = $('.c-search-filters'),
                    facets, facetArray, facetCount = 0;
                    
                if (data.Facet !== undefined) {
                    facets = data.Facet.ContentType;
                    var facetData = facets.replace('[' ,'').replace(']', '');
                    facetArray = facetData.split(',');
                }

                if (self.pageLoad === true) {                    
                    $('.c-search-filters__select option:not("#all")').each(function() {
                        var $this = $(this);
                        $this.hide();

                        $(facetArray).each(function(key, value) {
                            var filterValue = value;
                            if (filterValue.indexOf($this.attr('id')) !== -1) {
                                if (filterValue.indexOf('(0') === -1) {
                                    $this.text(filterValue).show();
                                    facetCount++;
                                }                                
                            }
                        });
                    });
                    if (facetCount < 1 || data.suggestions || data.noresult || self.contentType !== "") {
                       $searchFilters.fadeOut(500);
                    } else {
                        $searchFilters.fadeIn(500);
                    }
                    $("option#all").text(filterAllLabel+' ('+data.totalResults+ ')').prop("selected", "selected");
                }
            },

            addPersonalizeLabel: function(objData) {
                var personalizeLabel = this.searchModelData.personalizableLabel,
                customizable = this.searchModelData.customizableTag, 
                index = customizable.indexOf(":"),
                textCustomizable = customizable.substr(index + 1);

                if (objData && objData.response) {
                    objData.response.forEach(function(val, indx) {
                        if (val.Tags) {
                            val.Tags.forEach(function(value, index) {
                                if (value === textCustomizable){
                                    var span = $('.o-product__tag--customizable');
                                    span.text(personalizeLabel).removeClass('hidden');   
                                }
                            }); 
                        }    
                    });
                }
            },

            triggerMethodsOnSearch: function(objData) {
                var self = this,
                launchTag = '.c-search-listing__tag .o-tag',
                options = {};
                
                
                options.brandName = self.brandName;
                options.productId = objData.response;
    
                  // add ratings map to each list item
                objData.response.forEach(function(value, index) {
                    value.ratings = {
                      uniqueId: value.ProductSKU,
                      viewType: "inline"
                    };
                });
                options.ratingObj = objData.response[0].ratings;
                // options.ratingReview = self.ratingReview;
                options.ratingReview = self.ratingReview;
                // Ratings Integration 
                ratingService.initRatings(options);

                if ($(launchTag).length > 0) {
                    $(launchTag).text(objData.label);
                }
                //addPersonalizeLabel
                self.addPersonalizeLabel(objData); 
                self.app.trigger('image:lazyload', self);
            },

            handleNoResultCase: function(objData) {
                var self = this,
                    templateNoData = self.getTemplate('no-search-result', self.defaultSettings.templateNoData),
                    hiddenCl = 'hidden',
                    suggestionText = '.js-text-suggest';

                if (objData.totalResults > 0) {
                    if (objData.suggestions) {
                        $(self.noSearchResultWrapper).html(templateNoData(self.searchListingData));
                        $('.matching-keyword').text(objData.suggestions + '?');
                        $('.did-you-mean, '+suggestionText).removeClass(hiddenCl);
                        $(self.defaultSettings.totalRecordEl).text('0');

                    } else if (objData.noresult) {
                        $(self.noSearchResultWrapper).html(templateNoData(self.searchListingData));
                        $(suggestionText).removeClass(hiddenCl);  
                        $(self.defaultSettings.totalRecordEl).text('0');

                    } else {
                        $(self.noSearchResultWrapper).html('');
                        if (self.pageLoad === true) { 
                            $(self.defaultSettings.totalRecordEl).text(objData.totalResults);
                        }
                    }
                } else {
                    $(self.noSearchResultWrapper).html(templateNoData(self.searchListingData));
                    if (self.pageLoad === true) { 
                        $(self.defaultSettings.totalRecordEl).text(objData.totalResults);
                    }
                }
            },

            handleLoadMoreOnScroll: function (objData, listing) {
               var self = this,
               loadmoreSl = '.c-search-listing__loadmore, '+self.loadmore,
               itemsToShow = self.searchModelData.resultPerPage;

               if (self.isLoadMore === true) {                  
                    setTimeout( function() {
                        $(self.preloader).addClass('hidden');
                        $(self.searchListingSl).append(listing);
                        
                        self.triggerMethodsOnSearch(objData);
                        // init rating 
                        if (ratingReview !== undefined && typeof ratingReview.widget !== 'undefined') {
                            ratingService._kritiqueRatings(ratingReview);
                        }
                    }, 500);                   
                } else {
                    setTimeout( function() {
                        $(self.searchListingSl).html(listing);
                        self.animateResultsFadeIn(); 
                        
                        self.triggerMethodsOnSearch(objData);
                    }, 500);
                }

                if ((objData.totalResults > itemsToShow*self.pageNum)) {
                    if (self.pageNum === 1) {
                        $(loadmoreSl).fadeIn(1000);
                    }
                    setTimeout( function() {
                        self.pageScroll = true; 
                    }, 1000);
                } else {                    
                    $(loadmoreSl).fadeOut(500);
                    self.pageScroll = false;
                }
            },

            /**
            @description: creates search result item view
            @method _searchView
            @return {null}
            **/
            _generateSearchList: function (obj) {
                var self = this,
                    objData = $.parseJSON(obj),
                    filterData = objData.response,
                    listing = '', noResultsBlock = '', dataLength,
                    template = self.getTemplate('search-result-list', self.defaultSettings.template);

                for(var i = 0; i < objData.response.length; i++) {
                   $.extend(objData.response[i], objData.response[i], self.searchListingData);
                }
                //$.extend(objData, objData, self.searchListingData);
                // update content type in search dropdown
                self.updateContentTypeInDropdwdown(objData);

                $(filterData).each(function(key) {
                    var listData = $(filterData)[key],
                        dataLength = $(listData).length;

                    listing += template(listData);
                });                

                // update search results
                self.handleNoResultCase(objData);

                // check if load more enabled
                self.handleLoadMoreOnScroll(objData, listing);
            },

            /**
            @description: render search result item view and update DOM
            @method _renderSubView
            @return {null}
            **/ 

            submitForm: function(e) {                   
                var self = this,
                    keywordVal = $(self.searchInputField).val();

                e.preventDefault();
                self.isLoadMore = false;
                self.pageLoad = true;

                if (keywordVal === '' || keywordVal.match(/^\s*$/)) {
                    $(self.searchInputField).focus();
                    $(self.loadmore).fadeOut(500);
                } else {
                    
                    self.animateResultsFadeout();
                    self.updateParams(keywordVal);
                    $(self.searchInputField).typeahead('close');

                    // render search listing
                    self._populateData();

                    // analytics on form submitions
                    if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                        self.searchAnalytics();    
                    }
                }
                return false;
            },

             /**
            @description: Update parameter based on search
            @method updateParams
            @return {null}
            **/ 
            updateParams: function(keywordVal) {
                var self = this,
                    dropdownLabel = self.dropDownLabel,
                    queryLabel,
                    contentType = '';

                if ((keywordVal.indexOf(dropdownLabel.inArticles) !== -1) || (keywordVal.indexOf(dropdownLabel.inProducts) !== -1) || (keywordVal.indexOf(dropdownLabel.inEverything) !== -1)) {
                    
                    if (keywordVal.indexOf(dropdownLabel.inArticles) !== -1) {
                        self.contentType = 'Article';
                        queryLabel = keywordVal.split(' '+dropdownLabel.inArticles);                   
                    } else if (keywordVal.indexOf(dropdownLabel.inProducts) !== -1) {
                        self.contentType = 'Product';
                        queryLabel = keywordVal.split(' '+dropdownLabel.inProducts);
                    } else if (keywordVal.indexOf(dropdownLabel.inEverything) !== -1) {
                        self.contentType = '';
                        queryLabel = keywordVal.split(' '+dropdownLabel.inEverything);
                    }
                    self.searchKeyword = queryLabel[0];
                } else {
                    self.searchKeyword = keywordVal;
                    self.contentType = '';
                }
                self.pageNum = 1;
            },
            /**
            @description: handle load more click event
            @method: _handleLoadMore
            @return: {null}
            **/
            handleLoadMore: function() {               
                var self = this;
                document.addEventListener('scroll', function (event) {
                    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop,
                    scrollOffset = scrollTop + window.innerHeight;

                    if ((document.body.scrollHeight <= scrollOffset) && (self.pageNum >= 2) && self.pageScroll === true) {
                        self.pageScroll = false;
                        $(self.loadmore).click();
                    }
                });
            }
        });
    });

    return SearchListing;
});