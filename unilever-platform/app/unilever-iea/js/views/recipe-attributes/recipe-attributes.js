/*global define*/

define(function() {
    'use strict';

    var RecipeAttributes = IEA.module('UI.recipe-attributes', function(recipeAttributes, app, iea) {

        _.extend(recipeAttributes, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforeEnable');
                var self = this;
                self.recipeAttributesJson = self.getModelJSON().recipeAttributes;

                self.setRecipeAttributeValue();

                this.triggerMethod('enable');
            },

           setRecipeAttributeValue:function() {
                var self = this, attributeName;
                $.each(self.$el.find('.c-recipe-attributes .c-recipe-attributes__list-item'),function(){
                    var attributeName = $(this).find('.c-recipe-attributes__value').data('attributename'), context= $(this);
                    $.each(self.recipeAttributesJson.recipes, function(key,value) {
                        if(key === attributeName) {
                            if(value === '' || value === 0) {
                                context.hide();
                            } 
                            else {
                                context.find('.c-recipe-attributes__value').html(value); 
                            }
                        }
                        if(key === attributeName && attributeName === 'difficulties') {
                            if(value.length){
                                var difficultyValue = value.categoryName;
                                context.find('.c-recipe-attributes__value').html(difficultyValue);
                            }
                            else {
                                context.hide();
                            }
                        }
                    });                  
                });                
            }

            /**************************PRIVATE METHODS******************************************/          

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */

        });
    });

    return RecipeAttributes;
});
