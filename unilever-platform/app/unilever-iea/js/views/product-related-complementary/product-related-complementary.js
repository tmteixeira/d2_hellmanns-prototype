/*global define*/

define(['slick'],function() {
    'use strict';

    var ProductRelatedComplementary = IEA.module('UI.product-related-complementary', function(productRelatedComplementary, app, iea) {

        _.extend(productRelatedComplementary, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function (options) {
                this._super(options);
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the
             * component is a server side component, skipping the call to render
             * @method enable
             * @return
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                // functions to be wrirren here

                this.triggerMethod('enable');    
            }

            /**************************PRIVATE METHODS******************************************/

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }

            */

        });
    });

    return ProductRelatedComplementary;
});
