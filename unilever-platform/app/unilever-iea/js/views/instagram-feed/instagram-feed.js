define(function() {

    'use strict';

    var InstagramFeedComponent = IEA.module('UI.instagram-feed', function(instagramFeed, app, iea) {

        _.extend(instagramFeed, {
            // instagram override logic goes here
        });
    });

    return InstagramFeedComponent;
});
