/*global define*/

define(function() {
    'use strict';

    var ProductTeaser = IEA.module('UI.product-teaser', function(productTeaser, app, iea) {

        _.extend(productTeaser, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');
                var productTeaserData = this.getModelJSON().productTeaser,
                    self = this, options = {};

                self.getKeywordValOnLoad(options);

                $('.c-product-teaser__image-wrap').attr('href', options.prevUrl);

                this.triggerMethod('enable');
            },

            /**************************PRIVATE METHODS******************************************/          

            getQueryStringVal: function(name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
                return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            },

            getKeywordValOnLoad: function(options) {
                var self = this;
                // page load search result
                options.prodId = self.getQueryStringVal('pvi');
                options.prevUrl = self.getQueryStringVal('preUrl');
            }	
        });
    });
    
    return ProductTeaser;
});

