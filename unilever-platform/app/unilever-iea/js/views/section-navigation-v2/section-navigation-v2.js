/*global define*/

define(['typeahead', 'search'], function() {
    'use strict';

    var SectionNavigationV2 = IEA.module('UI.section-navigation-v2', function(sectionNavigationV2, app, iea) {
        
        var searchService = null;
        
        _.extend(sectionNavigationV2, {
            
            defaultSettings: {
                toggleSpeed: 400
            },

            events: {
                'click a.js-expandcollapse-subnav': '_expandSubNav'
            },

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                searchService = new IEA.search(); // creates new instance for search service
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                var self = this,
                    options = {},
                    settings = self.defaultSettings;
                
                self._expandSubList();
                //retrieve page JSON
                self.completeData = self.getModelJSON().sectionNavigationV2;    
                options.jsonData = self.completeData; 
                
                self.showSuggestion(options);
                
                this.triggerMethod('enable');
            },
            
            showSuggestion: function(options) {
                var jsonObj = [], navLists = options.jsonData.navigationList, tmp, sourceArr = [], map = {}, element = this.$el;
                
                //enable and disable button
                element.find('.c-section-navigation-form__textbox').on('keyup', function() {
                    !$(this).val().length && element.find('.js-btn-navigation-search').attr('disabled', 'disabled');
                });
                
                //iteration to find label and url value from json
                _.each(navLists, function(navList){
                    _.each(navList.linkedList, function(linklist) {
                        jsonObj.push({
                            'label': linklist.link.label,
                            'url': linklist.link.url
                        });
                    });
                });
                
                //substring match function
                var substringMatcher = function(strs) {
                  return function findMatches(q, cb) {
                    var matches, substrRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function(i, str) {
                      if (substrRegex.test(str)) {
                        matches.push(str);
                      }
                    });

                    cb(matches);
                  };
                };
                
                //create an array of label field from json and map the relative url
                $.each(jsonObj, function(i, labelTxt) {
                    sourceArr.push(labelTxt.label);
                    map[labelTxt.label] = labelTxt.url;
                });
                
                //call typeahead function to show the suggestion
                element.find('input.c-section-navigation-form__textbox').typeahead({
                  hint: false,
                  highlight: true,
                  minLength: 3
                },{
                  limit: 10,  
                  name: 'sectionNavigationV2',
                  source: substringMatcher(sourceArr)
                  }).bind('typeahead:selected', function(obj, selected, name) {
                    var selUrl = map[selected];
                    element.find('.js-btn-navigation-search').removeAttr('disabled').on('click', function() {
                        window.open(map[selected], '_self');
                    });
                });
            },
            
            // Method to toggle the sub navs after level 1
            _expandSubList: function() {
                $('.c-section-navigation-v2-item-list a').off('click').on('click', function(e) {
                    $(this).next('ul').toggle();
                });
            },
            
            // Method to toggle the level 1 subnav
            _expandSubNav: function(e) {
                var $this = $(e.currentTarget); 
                $this.parent().siblings('.c-section-navigation-v2-item__body').toggle();
                e.preventDefault();
            }
            
            /**************************PRIVATE METHODS******************************************/

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */

        });
    });

    return SectionNavigationV2;
});