/*global define*/

define(['eGiftingService'], function() {
    'use strict';

    var eGiftingService = null;

    var PurchaseConfirmation = IEA.module('UI.purchase-confirmation', function(purchaseConfirmation, app, iea) {

        _.extend(purchaseConfirmation, {

            /*********************PUBLIC METHODS******************************************/

            /**
             * intialize function. the super function inside it will call the abstract initializse of the iea view.
             * @method initialize
             * @param {} options
             * @return 
             */
            initialize: function (options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
             * render logic . this gets automatically called if the component is a client side component
             * @method render
             * @return 
             */
            render: function () {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');
                return this;
            },

            /**
             * enable function to write component enable logics. this function gets automatically called if the 
             * component is a server side component, skipping the call to render
             * @method enable
             * @return 
             */
            enable: function () {
                this.triggerMethod('beforEnable');

                eGiftingService = new IEA.EGifting();               

                if (eGiftingService.getItem('orderDetails') !== undefined) {
                    var orderDetailsJson = $.parseJSON(eGiftingService.getItem('orderDetails'));

                    $('#pdfToken').val(orderDetailsJson.pdfToken);
                    $('#orderId').val(orderDetailsJson.orderId);
                    $('#emailId').val(orderDetailsJson.userEmailId);

                    $('.js__order-id').text(orderDetailsJson.orderId);
                    $('.js__user-email').text(orderDetailsJson.userEmailId);
                }             

                this.triggerMethod('enable');
            }


            /**************************PRIVATE METHODS******************************************/          

            /**
                component private functions are written with '_' prefix to diffrentiate between the public methods
                example:

                _showContent: function (argument) {
                    // body...
                }
                
            */
        });
    });

    return PurchaseConfirmation;
});