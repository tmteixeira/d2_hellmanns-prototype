/*global require*/

'use strict';

require.config({
    paths: {
        'jquery': '../js/libs/vendor/jquery/dist/jquery',
        'underscore': '../js/libs/vendor/underscore/underscore',
        'handlebars': '../js/libs/vendor/handlebars/handlebars',
        'slick': '../js/libs/vendor/slick-carousel/slick/slick',
        'TweenMax': '../js/libs/vendor/gsap/src/uncompressed/TweenMax',
        'scrollTo': '../js/libs/vendor/gsap/src/uncompressed/plugins/ScrollToPlugin',
        'jquery.highlight': '../js/libs/vendor/jquery.highlight/jquery.highlight',
        'iscroll': '../js/libs/vendor/iscroll/build/iscroll-probe',
        'typeahead': '../js/libs/vendor/typeahead.js/dist/typeahead.jquery',
        'iea': ['iea.min', '../js/libs/iea/core/js/iea'],
        'iea.components': ['iea.components.min', '../js/libs/iea/core/js/iea.components'],
        'config': '../../../unilever-iea-config',
        'unileverHelpers': '../js/module/unilever-helpers',
        'accordion': '../js/services/accordion',
        'commonService': '../js/services/common',
        'animService': '../js/services/animations',
        'videoplayerService': '../js/services/videoplayer',
        'videoPlayerServiceV2': '../js/services/videoplayerservice-v2',
        'eGiftingService': '../js/services/e-gifting',
        'socialTabService': '../js/services/social-tabs',
        'quickPanelService': '../js/services/quickpanel',
        'ratings': '../js/services/ratings',
        'search': '../js/services/search',
        'socialSharing': '../js/services/socialsharing',
        'cookies': '../js/libs/vendor/cookies-js/dist/cookies',
        'braintree' : '../js/libs/vendor/braintree-web/dist/braintree',
        'hammerjs': '../js/libs/vendor/hammerjs/hammer',
        'jquery.hammer': '../js/libs/vendor/jquery-hammerjs/jquery.hammer',
        'formService' : '../js/services/form',
        'turnToService': '../js/services/turn-to-service',
        'binWidgetService': '../js/services/bin-widget',
        'postmessage': '../../whitelabel/config/postmessage',
        'pwstrength' : '../js/libs/vendor/pwstrength-bootstrap/dist/pwstrength-bootstrap-1.2.10',
        'iframetracker' : '../js/libs/vendor/jquery.iframetracker/jquery.iframetracker',
        'xregexp': '../js/services/xregexp',
        'addthis': '../js/libs/unilever-iea/addthis/addthis-widget'
    },
    enforceDefine: true,
    shim: {
        'iea': {
            deps: ['jquery', 'underscore']
        },
        'slick': {
            deps: ['jquery']
        },
        'scrollTo': {
            deps: ['TweenMax']
        },
        'jquery.highlight': {
            deps: ['jquery']
        },
        'typeahead': {
            deps: ['jquery'],
            init: function ($) {
                return require.s.contexts._.registry['typeahead.js'].factory($);
            }
        },
        'addthis': {
            exports: 'addthis'
        },
    }
});

define(['app'], function (app) {

    app.on('before:start', function () {
        // before start logic goes here
    });

    app.on('start', function (options) {
        // on application start logic goes here
    });

    app.on('application:ready', function () {
        app.start();
    });
});