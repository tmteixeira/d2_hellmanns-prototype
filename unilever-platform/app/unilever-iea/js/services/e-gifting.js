/*global define*/

define(['braintree'], function(braintree) {
    'use strict';
    IEA.service('EGifting', function(service, app, iea) {

        /**
         * Description
         * @method
         * @return ThisExpression
         */
        var EGifting = function() {
            return this;
        };

        // extend defenition prototype
        _.extend(EGifting.prototype, {
            initialize: function() {
                return this;
            },

            createCookie: function(name, value, timer) {
                var expires = '';
                if (timer) {
                    var date = new Date();
                    date.setTime(date.getTime() + (timer * 60 * 1000));
                    expires = '; expires=' + date.toGMTString();
                } else {
                    expires = '';
                }

                document.cookie = name + '=' + value + expires + '; path=/';
            },

            readCookie: function(name) {
                var nameEQ = name + '=',
                ca = document.cookie.split(';');

                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while(c.charAt(0) === ' ') {
                        c = c.substring(1, c.length);
                    }
                    if (c.indexOf(nameEQ) === 0) {
                        return c.substring(nameEQ.length, c.length);
                    }
                }
                return null;
            },

            eraseCookie: function(name) {
                this.createCookie(name, '', -1);
            },

            hasCookieData: function(name) {
                var self = this;
                if (typeof self.readCookie(name) !== 'undefined' && self.readCookie(name) !== null) {
                    return true;
                }
            },

            localStoreSupport: function() {
                try {
                    return 'localStorage' in window;
                } catch (e) {
                    return false;
                }
            },
            setItem: function(name, value) {
                if (this.localStoreSupport() ) {
                    localStorage.setItem(name, value);
                }
            },
            getItem: function(name) {
                if( this.localStoreSupport() ) {
                    return localStorage.getItem(name);
                }
            },
            removeItem: function(name) {
                if( this.localStoreSupport() ) {
                    localStorage.removeItem(name);
                }
                else {
                    this.setItem(name,'',-1);
                }
            },

            clearSession: function(cookieName, storageName) {
                this.eraseCookie(cookieName);
                if (this.getItem(storageName) !== undefined && this.getItem(storageName) !== '' && this.getItem(storageName) !== null) {
                    this.removeItem(storageName);
                }
                if (!$('#redirectForm').length) {
                    window.location.reload();
                }
            },

            remainingChars: function(selecter) {
                var wrapperCl = '.character-left';

                $(selecter).keyup(function(ev) {
                    var char = 0,
                        self = $(this),
                        charLength = self.val().length,
                        wrapperObj = self.parent().find(wrapperCl),
                        labelId = self.attr('id'),
                        charLimit = $(this).data('rule-maxlength');
                    
                    $('.js-'+labelId).text(self.val());
                    
                    char = parseInt(charLimit) - charLength;
                    
                    if (charLimit <= charLength) {
                        wrapperObj.text(0);
                    } else {
                        char = parseInt(charLimit) - charLength;
                        wrapperObj.text(char);
                    }

                    if (char === 0) {
                        wrapperObj.addClass('disabled');
                    } else {
                        wrapperObj.removeClass('disabled');
                    }
                });
            },

            switchTabs: function(options) {
                var speed = 700,
                    self = this,
                    activeCl = 'is-active',
                    $errorCl = $('.c-payment-form__errorwrap'),
                    hiddenCl = 'o-hidden';

                $(options.selector).click(function(e) {
                    var $this = $(this),
                        selectorId = $this.attr('id');
                    
                    $errorCl.addClass('hidden');
                    $(options.paymentWrap).removeClass(hiddenCl);
                    
                    if ($this.hasClass(activeCl)) {
                        e.preventDefault();
                    } else {
                        $(options.selector).removeClass(activeCl);
                        $this.toggleClass(activeCl);
                        $(options.wrapper).slideUp(speed).addClass(hiddenCl);
                        $('.'+selectorId).slideDown(speed).removeClass(hiddenCl);
                        if ($('.braintree-hosted-field-number').length === 0 && $('#braintree-paypal-loggedin').length === 0) {
                            self.setupBrainTree(options, e);
                        }
                    }
                });
            },

            setupBrainTree: function(options, event) {
                var self = this,
                    $errorCl = $('.c-payment-form__errorwrap'),
                    defaultSettings = {
                        onCompleteCallback: function() {}
                    };

                options = $.extend(defaultSettings, options);

                $errorCl.addClass('hidden');

                if (braintree !== undefined) {
                    braintree.setup(options.clientToken, 'custom', {
                        id : options.formId,
                        paypal : {
                            container : 'paypal-container',
                            singleUse : true,
                            onSuccess: function (nonce, email) {
                                $errorCl.addClass('hidden');
                                $('input[name="transactionDetails-clientNonce"]').val(nonce);
                            }
                        },
                        hostedFields : {
                            styles: {
                                // Style all elements
                                'input': {
                                    'font-size': '14px'
                                }
                            },
                            number : {
                                selector : options.cardNumber,
                                placeholder: $(options.cardNumber).data('placeholder')
                            },
                            expirationMonth : {
                                selector : options.expirationMonth,
                                placeholder: $(options.expirationMonth).data('placeholder')
                            },
                            expirationYear : {
                                selector : options.expirationYear,
                                placeholder: $(options.expirationYear).data('placeholder')
                            },
                            cvv : {
                                selector : options.cvv,
                                placeholder: $(options.cvv).data('placeholder')
                            }
                        },
                        onError : function(obj) {
                            var message = '<p>'+obj.message+'</p>',
                                fields, errorList = '';
                            
                            if (obj.details) {
                                fields = obj.details.invalidFieldKeys;
                           
                                $(fields).each(function(key, item) {
                                    errorList += '<li class="braintree-msg-item"> - '+item+'</li>';
                                });
                            }
                            
                            $('.js-braintree-errormsg').html(message+'<ul class="braintree-msg-list has-error">'+errorList+'</ul>');
                            $errorCl.removeClass('hidden');
                        },
                        onReady: function (integration) {
                            var checkout = integration;
                            $(options.paymentWrap).find('.o-preloader').addClass('o-hidden');
                            $(options.button).removeClass('o-disabled');
                        },
                        onPaymentMethodReceived: function (obj) {
                            var data = obj;
                            $errorCl.addClass('hidden');
                            $('input[name="transactionDetails-clientNonce"]').val(data.nonce);
                            $('input[name="transactionDetails-paymenttype"]').val(data.type);
                            $('input[name="transactionDetails-cardType"]').val(data.details.cardType);
                            options.onCompleteCallback(event);
                        }
                    });
                }
            },

            calculatePrice: function(cartJsonData) {
                var orderSummaryTotal = 0,
                $amountSelector = $('.c-order-summary__total #totalAmount'),
                cartData = $.parseJSON(cartJsonData);

                $(cartData.products).each(function(index, items) {
                    var product = items;
                    orderSummaryTotal = (parseFloat(product.price) + parseFloat(product.customisedWrappingPrice))*(product.quantity) + parseFloat(orderSummaryTotal);
                });

                orderSummaryTotal = orderSummaryTotal + parseFloat(cartData.deliveryPrice);
               
                $amountSelector.text(parseFloat(orderSummaryTotal).toFixed(2));
                // update total amount in mayment from on submit button
                $('#payAmount').text(cartData.currencySymbol + parseFloat(orderSummaryTotal).toFixed(2));
            }
        });
        return EGifting;
    });

});


