/* global define */

define(function () {
    'use strict';

    var videoPlayer, videoId, videoName, playerChangeFlag = false;
    var YT_GA = YT_GA || {};

    IEA.service('youtubeService', function(service, app, iea) {

        /**
         * Description
         * @method
         * @return YouTube
         */

        var YouTubeService = function() {

            return this;
        };

        // extend definition prototype
        _.extend(YouTubeService.prototype, {

            YOUTUBE_API_URL: '//www.youtube.com/player_api',

            initialize: function(options) {

                this.view = options.view;

                return this;
            },

            setupVidepAPI: function(options) {
                var scope = this;
                var youtubeReady = function() {
                    scope._setupVideoPlayer(options.videoContainerId);
                };

                if (typeof window.onYouTubePlayerAPIReady === 'undefined') {

                    window.onYouTubePlayerAPIReady = youtubeReady;
                } else {
                    window.onYouTubePlayerAPIReady = _.compose(window.onYouTubePlayerAPIReady, youtubeReady);
                }

                if($('script[src="'+scope.YOUTUBE_API_URL+'"]').length === 0) {

                    var tag = document.createElement('script');
                    tag.src = scope.YOUTUBE_API_URL;
                    var firstScriptTag = document.getElementsByTagName('script')[0];
                    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                } else if (typeof window.YT !== 'undefined') {

                    scope._setupVideoPlayer(options.videoContainerId);
                }
            },

            play: function() {
                videoPlayer.playVideo();
            },

            stop: function(){
                videoPlayer.stopVideo();
            },

            _setupVideoPlayer: function(videoContainerId) {

                videoId = this.view.$el.find('#' + videoContainerId).attr('data-vid');
                videoName = this.view.$el.find('#' + videoContainerId).data('video-name');
                if (!videoId) {

                    return;
                }

                var vars = {
                    wmode: 'opaque',
                    enablejsapi: 1,
                    autoplay: 1,
                    modestbranding: 1,
                    showinfo: 0,
                    theme: 'light',
                    rel: 0,
                    controls: 1,
                    version: 3
                };

                var scope = this;
                videoPlayer = new window.YT.Player(videoContainerId, {
                    playerVars: vars,
                    height: 600,
                    width: 560,
                    videoId: videoId,

                    events: {
                        'onReady': scope._onPlayerReady.bind(scope),
                        'onStateChange': scope._onPlayerStateChange.bind(scope)
                    }
                });
            },

            _onPlayerReady: function(evt) {
                var scope = this;
                videoPlayer.playVideo();
                setInterval(scope._onPlayerProgressChange.bind(scope), 500);
                YT_GA.progress25 = false;
                YT_GA.progress50 = false;
                YT_GA.progress75 = false;
            },

            _onPlayerProgressChange:function() {
                var self = this.view;
                YT_GA.timePercentComplete = Math.round(videoPlayer.getCurrentTime() / videoPlayer.getDuration() * 100);

                var progress;

                if (YT_GA.timePercentComplete > 24 && !YT_GA.progress25) {
                    progress = '25%';
                    YT_GA.progress25 = true;

                }

                if (YT_GA.timePercentComplete > 49 && !YT_GA.progress50) {
                    progress = '50%';
                    YT_GA.progress50 = true;
                }

                if (YT_GA.timePercentComplete > 74 && !YT_GA.progress75) {
                    progress = '75%';
                    YT_GA.progress75 = true;
                }

                if (progress) {
                    if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                        self._progressTrack(progress, videoId);
                    }
                }
            },

			_onPlayerStateChange: function(evt) {
                var self = this.view;
                switch (evt.data) {
                    case -1:
                        self._stopPlayVideo(evt.target);
                        break;
                    case 0:
                        if (self && typeof self.onVideoEnded !== 'undefined') {
                            self.onVideoEnded();
                            if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                                self._ctTag(videoName, false);
                            }
							playerChangeFlag = false;
                        }
                        break;
                    case 1:
                        self._stopPlayVideo(evt.target);
						if(playerChangeFlag === false){
							if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                                self._ctTag(videoName, true);
                            }
							playerChangeFlag = true;
						}
                        break;
                    default:
                }
            },

            _isYoutubeScriptLoaded: function() {

                var scripts = document.getElementsByTagName('script');
                for (var i = scripts.length; i--;) {

                    if (scripts[i].src === this.YOUTUBE_API_URL) {

                        return true;
                    }
                }
                return false;
            }
        });

        return YouTubeService;
    });
});
