/*global define*/

define(function() {
    'use strict';

    IEA.service('formService', function(service, app, iea) {

        /**
         * Description
         * @method
         * @return ThisExpression
         */
        var FormService = function() {
            return this;
        };


        // extend defenition prototype
        _.extend(FormService.prototype, {
            initialize: function() {
                return this;
            },

            fieldToJson: function(objectGraph, name, value1) {
                var targetProperty, self = this;
                if (objectGraph !== null || objectGraph !== undefined) {
                    var targetObject = objectGraph;
                    $.each(name, function(i) {
                        if (self.isArray(name[i])) {
                            return;
                        }
                        var isLeaf = ((i + 1) === name.length);
                        targetObject = self.addObject(targetObject, name[i], name[i - 1], name[i + 1], isLeaf);
                        if (name.length === i + 1) {
                            self.addValue(targetObject, name[i], value1);
                        }
                    });
                }
            },

            addObject: function(parentObject, elementName, parentElementName, nextElementName, isLeaf) {
                var targetElement = parentObject,
                    self = this;

                if (parentObject.constructor === Array) {
                    var currentObject = {};
                    if (self.isArray(nextElementName)) {
                        if (parentObject[parentElementName] === undefined) {
                            currentObject[elementName] = [];
                            targetElement = currentObject[elementName];
                        } else {
                            currentObject = parentObject[parentElementName];
                            targetElement = currentObject;
                        }

                    } else {
                        if (parentObject[parentElementName] !== null && parentObject[parentElementName] !== undefined) {
                            currentObject[elementName] = parentObject[parentElementName];
                        } else {
                            currentObject[elementName] = {};
                        }

                        if (isLeaf) {
                            targetElement = currentObject;
                        } else {
                            targetElement = currentObject[elementName];
                        }
                    }
                    while (parentObject.length < parseInt(parentElementName)) {
                        parentObject.push({});
                    }
                    parentObject.splice(parseInt(parentElementName), 0, currentObject);

                } else {
                    var isNewObject = false;
                    if (self.isArray(nextElementName)) {
                        if (parentObject[elementName] !== null && parentObject[elementName] !== undefined) {
                            parentObject = parentObject[elementName];
                        } else {
                            isNewObject = true;
                            parentObject[elementName] = [];
                        }
                    } else {
                        if (parentObject[elementName] !== null && parentObject[elementName] !== undefined) {
                            parentObject = parentObject[elementName];
                        } else {
                            isNewObject = true;
                            parentObject[elementName] = {};
                        }
                    }
                    if (isLeaf || (parentObject.constructor && !isNewObject)) {
                        if (parentObject.constructor === Array && parentObject[nextElementName] !== undefined) {
                            targetElement = parentObject[nextElementName];
                        } else {
                            targetElement = parentObject;
                        }
                    } else {
                        targetElement = parentObject[elementName];
                    }
                }

                return targetElement;

            },

            addValue: function(objectGraph, name, value1) {
                if(objectGraph.constructor!==String) {
                    if((value1 instanceof String) || (typeof value1  === 'string')){
                        objectGraph[name] = $.trim(value1);
                    }
                    else{
                        objectGraph[name] = value1;
                    }
                }
            },


            isArray: function(nextElement) {
                return $.isNumeric(nextElement);
            },
            formToJson: function(form) {
                var objectGraph = {},
                    $form = $(form),
                    self = this;
                return self.convertToJSON(objectGraph, $form.find('input, textarea, select').not('[data-exclude]'), self);
            },
            removeBlankElements: function(actual) {
                var newArray = [],
                    self = this;
                for (var i = 0; i < actual.length; i++) {
                    var emptyElement = true;
                    for (var key in actual[i]) {
                        if (actual[i].hasOwnProperty(key)) {
                            emptyElement = false;
                            actual[i][key] = self.cleanArray(actual[i][key]);
                        }
                    }
                    if (!emptyElement) {
                        newArray.push(actual[i]);
                    }
                }
                return newArray;
            },
            cleanArray: function(elementObject) {
                var self = this;
                if(elementObject === null){
                    return elementObject;
                }else if (elementObject.constructor === Array) {
                    elementObject = self.removeBlankElements(elementObject);
                } else if (elementObject.constructor === Object) {
                    elementObject = self.removeEmptyArrayElements(elementObject);
                }
                return elementObject;
            },
            removeEmptyArrayElements: function(objectGraph) {
                var self = this,
                    key;
                for (key in objectGraph) {
                    if (objectGraph.hasOwnProperty(key)) {
                        objectGraph[key] = self.cleanArray(objectGraph[key]);
                    }
                }
                return objectGraph;
            },
            convertToJSON: function(objectGraph, $formElems, self) {
                objectGraph = {};
                $formElems.each(function(i) {
                    //ignore the submit button
                    if ($(this).attr('name') !== 'submit') {
                        //split the dot notated names into arrays and pass along with the value
                        if ($(this).attr('type') === 'checkbox') {
                            self.fieldToJson(objectGraph, $(this).attr('name').split('-'), this.checked);
                        } else if ($(this).attr('type') === 'radio') {
                            if (this.checked) {
								var curVal = $(this).val(),
									dataTypes = $(this).data('type');
                                if ($.isNumeric(curVal) && dataTypes === 'number') {
                                    curVal = parseInt(curVal);
                                }
                                self.fieldToJson(objectGraph, $(this).attr('name').split('-'), curVal);
                            }
                        } else if ($(this).attr('type') === 'file') {
                            var nameOne = 'campaignImage-metadata-name',
                                nameTwo = 'campaignImage-metadata-size',
                                nameFour = 'campaignImage-imageBinary';
                            self.fieldToJson(objectGraph, nameOne.split('-'), $(this).data('filename'));
                            self.fieldToJson(objectGraph, nameTwo.split('-'), $(this).data('filesize'));
                            self.fieldToJson(objectGraph, nameFour.split('-'), $(this).data('fileencoded'));

                        } else {
                            if ($(this).attr('name') !== null && $(this).attr('name') !== '') {
                                var firstSplit = $(this).attr('name').split('-'),
                                    finalString = [],
                                    dataType = $(this).data('type');

                                $.each(firstSplit, function(j) {
                                    finalString = finalString.concat(firstSplit[j].split('#'));
                                });

                                var currVal = $(this).val();
                                if ($.isNumeric(currVal) && dataType === 'number') {
                                    currVal = parseInt(currVal);
                                }
                                if (currVal !== null && currVal !== '') {
                                    FormService.prototype.fieldToJson(objectGraph, finalString, currVal);
                                } else {
                                    FormService.prototype.fieldToJson(objectGraph, finalString, null);
                                }
                            }
                        }
                    }
                });
                console.log(JSON.stringify(objectGraph));
                self.removeEmptyArrayElements(objectGraph);
                console.log(objectGraph);
                console.log(JSON.stringify(objectGraph));
                return JSON.stringify(objectGraph);
            },

            // Returns object with key value pairs where key is the id of form field to be filled by value.
            jsonToForm: function(data) {
                var allfieldMappings = {},
                    keyStringArray = [],
                    level;

                function fillData(formData, level) {
                    $.each(formData, function(key, val) {
                        keyStringArray[level] = key;
                        if (val !== null) {
                            if (typeof val === 'object') {
                                fillData(val, level + 1);
                            } else {
                                var keyString = '';
                                for (var i = 0; i < level; i++) {
                                    keyString += keyStringArray[i] + '-';
                                }
                                keyString += key;
                                allfieldMappings[keyString] = val;
                            }
                        }
                    });
                }

                fillData(data, 0);
                return allfieldMappings;
            },

            // Populate form fields with data using object returned by jsonToForm service
            populateFormData: function(mappings, form) {
                /*$.each(mappings, function(k, v) {
                    $('#' + k).val(v);
                    v.toString().indexOf('.') === -1 && $('#' + k + '-' + v).attr('checked',true);
                });b*/
                $.each(mappings, function(k, v) {
                    if(form.find('[name*=' + k + ']').length >0 ){
                        var elem = $('[name*=' + k + ']'),
                            elemType = elem.attr('type');

                        if (elemType === 'checkbox' && v === true) {
                            elem = $('[name=' + k + ']');
                            elem.attr('checked', true);
                        } else if (elemType === 'radio') {
                            $('#' + k + '-' + v).attr('checked', true);
                        } else {
                            elem = $('[name=' + k + ']');
                            elem.val(v);
                        }
                    }
                });
            },

            getCookie: function(k) {
                return (document.cookie.match('(^|; )' + k + '=([^;]*)') || 0)[2];
            },

            registrationHelpers: function() {

                var self = this;

                function getFormElements(sel) {
                    var count = 0,
                        ret = [],
                        arr = sel.sort(function(a, b) {
                            a = $(a).attr('name');
                            b = $(b).attr('name');
                            count += 2;
                            if (a > b) {
                                return 1;
                            } else if (a < b) {
                                return -1;
                            } else {
                                return 0;
                            }
                        });
                    $.each(arr, function(k, v) {
                        if (typeof arr[k + 1] !== 'undefined' && v.name !== arr[k + 1].name) {
                            ret.push(v);
                        }
                    });
                    return ret;
                }

                function setFormHeight() {
                    var height = $('form.active .c-registration-tab-content').height() + 220; // adding 50px to fit in top bar plus 170px for registration header content.
                    $('form.active').parents('.c-tab-element-wrapper-content').height(height);
                    $('.form .form-error-message').remove();
                }

                function moveNext() {
                    $('.c-tab-element-wrapper-content').find('.form-error-msg').remove();
                    var $activeForm = $('form.active');
                    $activeForm.parents('.form').next().find('form').addClass('active');
                    $activeForm.removeClass('active');
                    setFormHeight();
                }

                function registerUser(form) {
                    var objectGraph = {},
                        url = $(form).attr('action'),
                        formElems = null;

                    if ($('#userPreferencesId-submit').attr('actionType') === 'update') {
                        url = url.replace(/.create$/, '.update');
                        var detailsForm = $('#userDetailsId-submit').parents('form'),
                            preferencesForm = $('#userPreferencesId-submit').parents('form');

                        formElems = $(detailsForm).add(preferencesForm).find(':input').not('[type=submit]').not('[data-exclude]');

                    } else {
                        formElems = $('.c-tab-element-wrapper-content form :input:not([type="submit"])').not('[data-exclude]');
                    }

                    var registrationData = self.convertToJSON(objectGraph, formElems, self),
                        submitxhr = $.ajax({
                            type: 'POST',
                            url: url,
                            contentType: 'application/json',
                            dataType: 'json',
                            data: registrationData
                        });

                    submitxhr.done(function() {
                        moveNext();
                    });
                    submitxhr.fail(function(err) {
                        var $errorBlock = null;
                        if (err.error !== undefined) {
                            var errorCode = JSON.parse(err.responseText);
                            for (var i = 0; i < errorCode.errors.length; i++) {
                                $errorBlock = $('<div class="form-error-msg text-danger"><span class="glyphicon glyphicon-warning-sign"></span><span class="error-msg-text">' + errorCode.errors[i].error + '</span></div>');
                                if ($(form).prev('.form-error-msg').length > 0) {
                                    $(form).prev('.form-error-msg').remove();
                                }
                                $(form).before($errorBlock);
                            }
                        }
                    });
                }

                return {
                    moveNext: moveNext,
                    registerUser: registerUser,
                    setFormHeight: setFormHeight
                };
            }
        });
        return FormService;
    });
});
