/* global define */

define(function () {
    'use strict';

    var videoPlayer, videoId, videoName, playerChangeFlag = false;
    var YT_GA = YT_GA || {}, videoIframe;

    IEA.service('videoPlayerService', function(service, app, iea) {

        /**
         * Description
         * @method
         * @return YouTube
         */

        var VideoPlayerService = function() {
            return this;
        };

        // extend definition prototype
        _.extend(VideoPlayerService.prototype, {
            YOUTUBE_API_URL: '//www.youtube.com/player_api',

            initialize: function(options) {
                this.view = options.view;
                this.videoFlag = options.videoFlag;

                return this;
            },

            setupVidepAPI: function(options) {
                var $videoWrapper = $('#'+options.videoContainerId);
                switch(options.videoType) {
                    case 'youtube':
                        var scope = this,
                            youtubeReady = function() {
                                scope._setupVideoPlayer(options);
                            };

                        if (typeof window.onYouTubePlayerAPIReady === 'undefined') {
                            window.onYouTubePlayerAPIReady = youtubeReady;
                        } else {
                            window.onYouTubePlayerAPIReady = _.compose(window.onYouTubePlayerAPIReady, youtubeReady);
                        }

                        if ($('script[src="' + scope.YOUTUBE_API_URL + '"]').length === 0) {
                            var tag = document.createElement('script');
                            tag.src = scope.YOUTUBE_API_URL;
                            var firstScriptTag = document.getElementsByTagName('script')[0];
                            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                        } else if (typeof window.YT !== 'undefined') {
                            scope._setupVideoPlayer(options);
                        }
                        break;
                    
                    case 'youku':
                        videoId = $videoWrapper.attr('data-video-id');
                        videoIframe = '<iframe id="'+options.videoContainerId+'" src="//player.youku.com/embed/'+videoId+'&autoplay=true" width="'+options.frameWidth+'" height="'+options.frameHeight+'" frameborder="0"></iframe>';
                        $videoWrapper.html(videoIframe);
                        break;
                    
                    case 'iqiyi':
                        videoId = $videoWrapper.attr('data-video-id');
                        videoIframe = '<embed id="'+options.videoContainerId+'" src="http://player.video.qiyi.com/'+videoId+'&autoplay=true" allowFullScreen="true" quality="high" width="100%" height="600" align="middle" allowScriptAccess="always" type="application/x-shockwave-flash"></embed>';
                        $videoWrapper.html(videoIframe);
                        break;

                    case 'instagram':
                        videoId = document.getElementById(options.videoContainerId).getAttribute('data-video-id');
                        videoIframe = '<iframe width="100%" height="600" src="'+videoId+'&autoplay=true" frameborder="0"></iframe>';
                        document.getElementById(options.videoContainerId).innerHTML = videoIframe;
                        break;
                        
                }
            },
            
            play: function() {
                videoPlayer.playVideo();
            },

            stop: function() {
                videoPlayer.stopVideo();
            },

            _setupVideoPlayer: function(options) {

                videoId = $('#' + options.videoContainerId).attr('data-video-id');
                videoName = $('#' + options.videoContainerId).data('video-name');

                if (!videoId) {
                    return;
                }

                var vars = {
                    wmode: 'opaque',
                    enablejsapi: 1,
                    autoplay: 1,
                    modestbranding: 1,
                    showinfo: 0,
                    theme: 'light',
                    rel: 0,
                    controls: 1,
                    version: 3
                };

                var scope = this;
                videoPlayer = new window.YT.Player(options.videoContainerId, {
                    playerVars: vars,
                    height: (options.frameHeight !== undefined) ? options.frameHeight : 600,
                    width: (options.frameWidth !== undefined) ? options.frameWidth : 560, //options.frameWidth,
                    videoId: videoId,

                    events: {
                        'onReady': scope._onPlayerReady.bind(scope),
                        'onStateChange': scope._onPlayerStateChange.bind(scope)
                    }
                });
            },

            _onPlayerReady: function(evt) {
                var scope = this;
                videoPlayer.playVideo();
                setInterval(scope._onPlayerProgressChange.bind(scope), 500);
                YT_GA.progress25 = false;
                YT_GA.progress50 = false;
                YT_GA.progress75 = false;
            },

            _onPlayerProgressChange: function() {
                var self = this.view;
                YT_GA.timePercentComplete = Math.round(videoPlayer.getCurrentTime() / videoPlayer.getDuration() * 100);

                var progress;

                if (YT_GA.timePercentComplete > 24 && !YT_GA.progress25) {
                    progress = '25%';
                    YT_GA.progress25 = true;

                }

                if (YT_GA.timePercentComplete > 49 && !YT_GA.progress50) {
                    progress = '50%';
                    YT_GA.progress50 = true;
                }

                if (YT_GA.timePercentComplete > 74 && !YT_GA.progress75) {
                    progress = '75%';
                    YT_GA.progress75 = true;
                }

                if (progress) {
                    if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                        self._progressTrack(progress, videoId);
                    }
                }
            },

            _onPlayerStateChange: function(evt) {
                var self = this.view;
                switch (evt.data) {
                    case -1:
                        if (this.videoFlag) {
                            self._stopPlayVideo(evt.target);
                        }
                        break;
                    case 0:
                        if (self && typeof self.onVideoEnded !== 'undefined') {
                            self.onVideoEnded();
                            if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                                self._ctTag(videoName, videoId, false);
                            }
                            playerChangeFlag = false;
                        }
                        break;
                    case 1:
                        if (this.videoFlag) {
                            self._stopPlayVideo(evt.target);
                        }
                        if (playerChangeFlag === false) {
                            if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                                self._ctTag(videoName, videoId, true);
                            }
                            playerChangeFlag = true;
                        }
                        break;
                    default:
                }
            },

            _isYoutubeScriptLoaded: function() {

                var scripts = document.getElementsByTagName('script');
                for (var i = scripts.length; i--;) {

                    if (scripts[i].src === this.YOUTUBE_API_URL) {

                        return true;
                    }
                }
                return false;
            }
        });
        return VideoPlayerService;
    });
});