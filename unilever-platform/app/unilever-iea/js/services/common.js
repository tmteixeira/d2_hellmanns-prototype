/*global define*/

define(['TweenMax'], function (TweenMax) {
    'use strict';

    IEA.service('commonService', function (service, app, iea) {

        /**
         * Description
         * @method
         * @return ThisExpression
         */
        var CommonService = function () {
            return this;
        };


        // extend defenition prototype
        _.extend(CommonService.prototype, {

            initialize: function () {
                return this;
            },

            isMobile: function () {
                var check = false;

                (function (a) {
                    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {
                        check = true;
                    }
                })(navigator.userAgent || navigator.vendor || window.opera);

                return check;
            },

            isMobileAndTablet: function () {
                var check = false;
                (function (a) {
                    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))){check = true;}
                })(navigator.userAgent || navigator.vendor || window.opera);
                return check;
            },

            isScrollAnimationSupported: function() {

                if (!navigator.platform.match(/iPhone|iPod|iPad/)) {

                    return true;
                }

                var agent = navigator.userAgent.match(/; CPU.*OS (\d_\d)/);
                if (agent) {

                    var strVersion = agent[agent.length - 1].replace('_', '.');
                    var version = strVersion * 1;

                    if (version < 8 || (version <= 8.4 && navigator.userAgent.match(/CriOS/))) {

                        return false;
                    }
                }

                return true;
            },

            getAdaptiveImage: function (obj, newClassName) {
                var parsedURL = [],
                    extension = (typeof obj.extension !== 'undefined' && obj.extension !== '' && typeof obj.extension !== 'object') ? obj.extension : app.getValue('extension'),
                    imageMarkup = '',
                    imageMarkupString = '',
                    className = (typeof newClassName !== 'undefined' && newClassName !== '') ? newClassName : '',
                    selector = '.' + app.getValue('selector');

                if (typeof obj.isNotAdaptive !== 'undefined' && obj.isNotAdaptive === 'true') {
                    /*Scenario where adaptive behavior not required */
                    imageMarkupString = '<img class="non-adaptive ' + obj.title + '" src="' + obj.url + '" title="' + obj.title + '" alt="' + obj.altImage + '">';

                } else {

                    if (typeof obj.title !== 'undefined' && obj.title !== '' && typeof obj.title !== 'object') {
                        imageMarkup = '<img class="' + className + '" src="' + obj.path + selector + app.getValue('breakpoints').desktop.prefix + extension + '" title="' + obj.title + '" alt="' + obj.altImage + '">';
                    } else {
                        imageMarkup = '<img class="' + className + '" src="' + parsedURL + app.getValue('breakpoints').desktop.prefix + extension + '" alt="' + obj.altImage + '">';
                    }

                    imageMarkupString = '<picture class="lazy-load is-loading">' +
                        '<!--[if IE 9]><video style="display: none;"><![endif]-->' +
                        '<source srcset="' + obj.path + selector + app.getValue('breakpoints').desktop.prefix + extension + '" media="' + app.getValue('breakpoints').desktop.media + '">' +
                        '<source srcset="' + obj.path + selector + app.getValue('breakpoints').tabletLandscape.prefix + extension + '" media="' + app.getValue('breakpoints').tabletLandscape.media + '">' +
                        '<source srcset="' + obj.path + selector + app.getValue('breakpoints').tablet.prefix + extension + '" media="' + app.getValue('breakpoints').tablet.media + '">' +
                        '<source srcset="' + obj.path + selector + app.getValue('breakpoints').mobileLandscape.prefix + extension + '" media="' + app.getValue('breakpoints').mobileLandscape.media + '">' +
                        '<source srcset="' + obj.path + selector + app.getValue('breakpoints').mobile.prefix + extension + '" media="' + app.getValue('breakpoints').mobile.media + '">' +
                        '<!--[if IE 9]></video><![endif]-->' +
                        imageMarkup +
                        '</picture>';
                }

                return imageMarkupString;
            },
            ios7SVGsupport:function(group, delay){
                //TODO:Find alternative to using setTimeout
                delay = (typeof delay !== 'undefined') ? delay : 1000;

                setTimeout(function() {
                    _.each(group, function(ele){
                        $(ele).wrap('<div></div>');
                    });
                },delay);
            },
            debounce: function(func, wait, immediate) {
                var timeout;
                return function() {
                    var context = this, args = arguments;
                    var later = function() {
                        timeout = null;
                        if (!immediate) {
                            func.apply(context, args);
                        }
                    };
                    var callNow = immediate && !timeout;
                    clearTimeout(timeout);
                    timeout = setTimeout(later, wait);
                    if (callNow) {
                        func.apply(context, args);
                    }
                };
            },

            imageLazyLoadOnPageScroll: function() {
                var vpH = $(window).height(),
                    st = $(window).scrollTop(),
                    pictureEl = $('picture.lazy-load:not(.lazyloadcomplete)');

                if (!pictureEl.length) {
                    $(window).off('scroll.lazyLoad');
                    return;
                }

                pictureEl.each(function(i, elm) {
                    var _this = $(this),
                        srcset = _this.find('source').data('srcset'),
                        y = _this.offset().top,
                        elmHeight = _this.height(),
                        imgEl;

                    if (!srcset) {
                        return;
                    }

                    if (y < (vpH + st + elmHeight)) { // Item is visible (or about to be)
                        _this.find('source').map(function(index) {
                            var sourceEl = $(this);
                            sourceEl.attr('srcset', sourceEl.data('srcset')).attr('data-srcset', '');
                        });

                        imgEl = _this.find('img');
                        imgEl.attr('src', imgEl.data('src')).load(function() {
                            imgEl.attr('data-src', '');
                            _this.addClass('lazyloadcomplete');
                        });
                    }
                });
            },

            tooltipEvent: function(selector, wrapper, parentWrapper) {
                var animationSpeed = 800,
                    isActive = 'is-active';
                    
                $(document).on('click', selector, function(e) {
                    e.preventDefault();
                    var $this = $(this),
                    contentWrapper = $(this).parents(parentWrapper).find(wrapper);

                    if ($(selector).hasClass(isActive) && !$this.hasClass(isActive)) {
                        $(selector).removeClass(isActive);
                        $(wrapper).slideUp(animationSpeed);
                    }
                    
                    $this.toggleClass(isActive, 200);

                    if ($this.hasClass(isActive)) {
                        contentWrapper.slideDown(animationSpeed);
                    } else {
                        contentWrapper.slideUp(animationSpeed);
                    }
                });
            },

            switchContentWithTabs: function(selector, wrapper) {
                var speed = 700,
                    self = this,
                    activeCl = 'is-active',
                    hiddenCl = 'o-hidden';

                $(selector).click(function(e) {
                    e.preventDefault();
                    var $this = $(this),
                        selectorId = $this.attr('href');
                    
                    if ($this.hasClass(activeCl)) {
                        e.preventDefault();
                    } else {
                        $(selector).removeClass(activeCl);
                        $this.toggleClass(activeCl);
                        $(wrapper).slideUp(speed).addClass(hiddenCl);
                        $(selectorId).slideDown(speed).removeClass(hiddenCl);
                    }
                });
            },

            /**
            @description: Backbone.js Bind Callback to Successful Model Fetch
            @method AjaxModelView
            @return {null}
            **/

            getDataAjaxMethod: function(options) {
                var self = this,
                    defaultSettings = {
                        onCompleteCallback: function() {}
                    },
                    AjaxModal, AjaxModelView, modelJson, Model;

                options = $.extend(defaultSettings, options);

                if (options.isTypePost) {
                    Model = Backbone.Model.extend({
                        url: options.servletUrl
                    });
                } else {
                    Model = Backbone.Model.extend({
                        url: options.servletUrl+options.queryParams
                    });
                }

                var View = Backbone.View.extend({
                    initialize: function () {
                        var _thisView = this;
                        // #
                        // This works, as long as you remember to pass in the 'this' context
                        // as the last parameter.
                        this.listenTo(this.model,'change', this.render);

                        if (options.isTypePost) {
                            if(options.header) {
                                this.model.fetch({
                                    type: 'POST',
                                    data: options.queryParams,
                                    headers: options.header,
                                    contentType: 'application/json'
                                });
                            } else {
                                this.model.fetch({
                                    type: 'POST',
                                    data: options.queryParams,
                                    contentType: 'application/json'
                                });
                            }
                        } else {
                            if(options.header) {
                                this.model.fetch({headers: options.header});
                            }else {
                                this.model.fetch();
                            }
                            
                        }
                    },
                    render: function () {
                        modelJson = JSON.stringify(this.model.toJSON());
                        options.onCompleteCallback(modelJson);
                    }
                });

                // Initialize the Model
                AjaxModal = new Model();

                // Initialize the View, passing it the model instance
                AjaxModelView = new View({
                    model: AjaxModal
                });
            },
            
            getQueryParams: function(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            },

            getSubstring: function(str, startIndex, endIndex) {
                return str.substring(startIndex,endIndex);
            },

            getJsConfigUrl: function(brandname) {
                var hostname = window.location.hostname,
                    configUrl = (hostname === 'localhost' || hostname.indexOf('pro') !== -1) ? '' : '/etc/ui/' +brandname+ '/clientlibs/core/core';

                return configUrl;
            },
            
            renderScript: function(src, async, callback) {
                var el = document.createElement('script');
                
                el.type = 'text/javascript';
                el.async = async || true;
                el.src = src;
                
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(el, s);
                if(typeof callback === 'function') {
                    s.onload = function() {
                        callback();
                    };
                }
            },

            _createCookie:function(name,value,days){
                var expires;
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    expires = '; expires=' + date.toGMTString();
                }
                else {
                    expires = '';
                }
                document.cookie = name + '=' + value + expires + '; path=/';
            },

            _getCookie:function(cName){
                var cStart,cEnd;
                if (document.cookie.length > 0) {
                    cStart = document.cookie.indexOf(cName + '=');
                    if (cStart !== -1) {
                        cStart = cStart + cName.length + 1;
                        cEnd = document.cookie.indexOf(';', cStart);
                        if (cEnd === -1) {
                            cEnd = document.cookie.length;
                        }
                        return document.cookie.substring(cStart, cEnd);
                    }
                }
                return '';
            },

            youtubeIdExtracter: function(url) {
                if (/youtu\.?be/.test(url)) {

                    // Look first for known patterns
                    var i;
                    var patterns = [
                        /youtu\.be\/([^#\&\?]{11})/, // youtu.be/<id>
                        /\?v=([^#\&\?]{11})/, // ?v=<id>
                        /\&v=([^#\&\?]{11})/, // &v=<id>
                        /embed\/([^#\&\?]{11})/, // embed/<id>
                        /\/v\/([^#\&\?]{11})/ // /v/<id>
                    ];

                    // If any pattern matches, return the ID
                    for (i = 0; i < patterns.length; ++i) {
                        if (patterns[i].test(url)) {
                            return patterns[i].exec(url)[1];
                        }
                    }
                }
            },

            pagination: function(paginationBtn, obj, view) {
                var self = this,
                    $this = obj,
                    pageType = $this.data('role'),
                    disabledCl = 'o-btn--disabled',
                    $firstPage = view.$el.find(paginationBtn+'[data-role="first"]'),
                    $lastPage = view.$el.find(paginationBtn+'[data-role="last"]'),
                    $nextPage = view.$el.find(paginationBtn+'[data-role="next"]'),
                    $prevPage = view.$el.find(paginationBtn+'[data-role="prev"]');

                view.$el.find(paginationBtn).removeClass('o-btn--active');

                switch (pageType) {
                    case 'first':
                        view.pageNum = parseInt($this.data('pagenum'));
                        view.startIndex = view.pageNum;
                        break;
                        
                    case 'last':
                        view.pageNum = parseInt($this.data('pagenum'));
                        view.startIndex = ((view.componentJson.pagination.itemsPerPage) * (view.pageNum - 1)) + 1;
                        break;

                    case 'next':
                        view.pageNum++;
                        view.startIndex = view.startIndex + view.componentJson.pagination.itemsPerPage;
                        break;

                    case 'prev':
                        view.pageNum--;
                        view.startIndex = view.startIndex - view.componentJson.pagination.itemsPerPage;
                        break;

                    case 'navigation':
                        view.pageNum = parseInt($this.data('pagenum'));
                        view.startIndex = ((view.componentJson.pagination.itemsPerPage) * (view.pageNum - 1)) + 1;
                        break;
                }

                if (view.pageNum === parseInt($lastPage.data('pagenum'))) {
                    $nextPage.addClass(disabledCl);
                    $lastPage.addClass(disabledCl);
                    $firstPage.removeClass(disabledCl);
                    $prevPage.removeClass(disabledCl);
                } else if (view.pageNum === parseInt($firstPage.data('pagenum'))) {
                    $firstPage.addClass(disabledCl);
                    $prevPage.addClass(disabledCl);
                    $nextPage.removeClass(disabledCl);
                    $lastPage.removeClass(disabledCl);
                } else {
                    $firstPage.removeClass(disabledCl);
                    $prevPage.removeClass(disabledCl);
                    $nextPage.removeClass(disabledCl);
                    $lastPage.removeClass(disabledCl);
                }

                view.$el.find(paginationBtn+'[data-pagenum='+view.pageNum+']').addClass('o-btn--active');
            },

            getQueryStringVal: function(name) {
                name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
                var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
                    results = regex.exec(location.search);
                return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
            }
            
        });

        return CommonService;

    });

});


