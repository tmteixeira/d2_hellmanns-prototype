/* global define */

define(['animService'], function () {
    'use strict';

    var videoPlayer,
        videoId,
        videoSource,
        videoPlayProgressTimer,
        animService = null,
        youtubeReady,
        isVideoPlaying = false,
        videoMuteAudio;

    var YT_GA = YT_GA || {}, videoIframe;

    IEA.service('videoPlayerServiceV2', function(service, app, iea) {

        /**
         * Description
         * @method
         * @return YouTube
         */

        var VideoPlayerServiceV2 = function() {
            return this;
        };

        // extend definition prototype
        _.extend(VideoPlayerServiceV2.prototype, {
            
            YOUTUBE_API_URL: '//www.youtube.com/player_api',
            
            initialize: function(options) {
                this.overlayCl = '.js-modal-video';
                this.isVideoOpenCl = 'js-video-playing';
                this.playerButtonClick = false;
                return this;
            },

            initVideoPlayer:  function(options) {
                animService = new IEA.animService();
                this.view = options.view;
                this.closeOverlayBtn = '.js-player-close';

                this.openModal(options);
                this.closeModal(options);
            },

            /**
             * starts search methods
             * @method 
             * @return 
             */
            openModal: function(options) {
                var self = this,
                    overlayOptions = {};
                
                $(document).off('click', options.selector).on('click', options.selector, function(e) {
                    e.preventDefault();
                    var $this = $(this),
                        blurBgClass = 'u-blur-background';

                    self.resetVideoPlayer(options);
                    self.playerButtonClick = true;
                    self.setupVidepAPI(options, $this);

                    if ($this.parent().find(options.videoWrapper).data('video-display-overlay')) {
                        $('body').removeClass(self.isVideoOpenCl);
                        animService.openOverlay($this.parent().find(self.overlayCl), overlayOptions = {
                            onCompleteCallback: function() {
                                $('.'+blurBgClass).removeClass(blurBgClass);
                            }
                        });
                    } else {
                        if (!options.multipleVideoInstances) {
                            $('body').addClass(self.isVideoOpenCl);
                        }
                    }
                });
            },

            /**
             * to close an overlay
             * @method openModal
             * @return 
             */

            closeModal: function(options) {
                var self = this;
                $(document).off('click', self.closeOverlayBtn+', .'+self.isVideoOpenCl).on('click', '.js-player-close, .'+self.isVideoOpenCl, function(e) {
                    e.preventDefault();

                    var $this = $(this),
                        $playerIcon = $this.parents().find(options.selector);

                    //reset player
                    if (!self.playerButtonClick && ($this.is('.'+self.isVideoOpenCl) || $this.is(self.closeOverlayBtn))) {
                        if ($this.is('.'+self.isVideoOpenCl)) {
                            $playerIcon.fadeIn().siblings(options.previewImage).fadeIn();
                            $('body').removeClass(self.isVideoOpenCl);
                        } else {
                            animService.closeOverlay($this.parents(self.overlayCl));
                        }
                        self.resetVideoPlayer(options);
                    }
                });
            },

            setupVidepAPI: function(options, selector) {
                var $thumbnail = selector.parent(),
                    $videoWrapper = $thumbnail.find(options.videoWrapper),
                    self = this,
                    videoType = $videoWrapper.data('player'),
                    videoOptions;
                
                videoId = $videoWrapper.data('video-id');
                videoSource = $videoWrapper.data('video-source');

                videoOptions = {
                    selector: selector,
                    previewImage: selector.siblings(options.previewImage),
                    videoContainerId: $videoWrapper.attr('id'),
                    playerHeight: $thumbnail.height(),
                    videoDisplayOverlay: $videoWrapper.data('video-display-overlay') === true ? 1 : 0,
                    videoAutoplay: $videoWrapper.data('video-autoplay') === true ? 1 : 0,
                    videoHideControls: $videoWrapper.data('video-hide-controls') === true ? 0 : 1,
                    videoLoop: $videoWrapper.data('video-loop-playback') === true ? 1 : 0,
                    videoShowRelatedVideos: $videoWrapper.data('related-videos') === true ? 1 : 0
                };

                videoMuteAudio = $videoWrapper.data('video-mute-audio');

                switch(videoType) {
                    case 'youtube':
                        
                        var youtubeReady = function() {
                            self.setupVideoPlayer(videoOptions);
                        };

                        if (typeof window.onYouTubePlayerAPIReady === 'undefined') {
                            window.onYouTubePlayerAPIReady = youtubeReady;
                        } else {
                            setTimeout(function() {
                                window.onYouTubePlayerAPIReady = _.compose(window.onYouTubePlayerAPIReady, youtubeReady);
                            }, 500);
                        }

                        if ($('script[src="' + self.YOUTUBE_API_URL + '"]').length === 0) {
                            var tag = document.createElement('script'),
                                firstScriptTag = document.getElementsByTagName('script')[0];

                            tag.src = self.YOUTUBE_API_URL;
                            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                        }

                        self.setupVideoPlayer(videoOptions);

                        break;

                    case 'youku':
                        videoIframe = '<iframe id="'+videoOptions.videoContainerId+'" src="http://player.youku.com/embed/'+videoId+'?autoplay='+videoOptions.videoAutoplay+'" width="100%" height="'+videoOptions.playerHeight+'" frameborder="0"></iframe>';

                        $videoWrapper.html(videoIframe);

                        $('.js-video-player').show();
                        break;

                    case 'iqiyi':
                        videoIframe = '<embed id="'+videoOptions.videoContainerId+'" src="http://player.video.qiyi.com/'+videoId+'?autoplay='+videoOptions.videoAutoplay+'" allowFullScreen="true" quality="high" width="100%" height="'+videoOptions.playerHeight+'" align="middle" allowScriptAccess="always" type="application/x-shockwave-flash"></embed>';
                        
                        $videoWrapper.html(videoIframe);
                        $('.js-video-player').show();
                        break;

                    case 'instagram':
                        videoId = document.getElementById(videoOptions.videoContainerId).getAttribute('data-video-id');
                        videoIframe = '<iframe width="100%" height="'+videoOptions.frameHeight+'" src="'+videoId+'&autoplay=true" frameborder="0"></iframe>';

                        document.getElementById(videoOptions.videoContainerId).innerHTML = videoIframe;

                        break;
                }
                isVideoPlaying = true;
            },

            resetVideoPlayer: function(options) {
                if (videoPlayer && isVideoPlaying) {
                    var self = this,
                        $videoPlayer = $(videoPlayer.h),
                        videoContainerId = $videoPlayer.attr('id'),
                        view = self.view,
                        parentDiv = $('#'+videoContainerId).parent(),
                        playerDiv = '<div id="'+videoContainerId+'" class="'+$videoPlayer.attr('class')+'" data-video-id="'+$videoPlayer.data('video-id')+'" data-player="'+$videoPlayer.data('player')+'" data-video-name="'+$videoPlayer.data('video-name')+'" data-video-display-overlay="'+$videoPlayer.data('video-display-overlay')+'" data-video-autoplay="'+$videoPlayer.data('video-autoplay')+'" data-video-hide-controls="'+$videoPlayer.data('video-hide-controls')+'" data-video-mute-audio = "'+$videoPlayer.data('video-mute-audio')+'" data-video-loop-playback = "'+$videoPlayer.data('video-loop-playback')+'" data-related-videos = "'+$videoPlayer.data('related-videos')+'"></div>';
                    
                    if (options.multipleVideoInstances && !$videoPlayer.data('video-display-overlay')) {
                        videoPlayer.pauseVideo();
                    } else {
                        $('iframe#'+videoContainerId).remove();
                        $('embed#'+videoContainerId).remove();
                        parentDiv.append(playerDiv);
                        $videoPlayer.fadeOut();
                        parentDiv.parents(view.$el).find(options.previewImage).fadeIn();
                        parentDiv.parents(view.$el).find(options.selector).fadeIn();
                        $('body, html').removeClass(self.isVideoOpenCl);
                    }

                    clearInterval(videoPlayProgressTimer);

                    isVideoPlaying = false;
                }
            },

            setupVideoPlayer: function(videoOptions) {
                var self = this,
                    vars;
                
                if (!videoId) {
                    return;
                }
                
                vars = {
                    wmode: 'opaque',
                    enablejsapi: 1,
                    autoplay: videoOptions.videoAutoplay,
                    loop: videoOptions.videoLoop,
                    modestbranding: 1,
                    showinfo: 0,
                    theme: 'light',
                    rel: videoOptions.videoShowRelatedVideos,
                    controls: videoOptions.videoHideControls,
                    version: 3
                };
                
                if (videoOptions.videoLoop) {
                    vars.playlist = videoId;
                }

                if (window.YT) {
                    $(videoOptions.previewImage).fadeOut();
                    $(videoOptions.selector).fadeOut();
                    
                    videoPlayer = new window.YT.Player(videoOptions.videoContainerId, {
                        playerVars: vars,
                        height: videoOptions.frameHeight ? videoOptions.frameHeight : 600,
                        width: '100%',
                        videoId: videoId,
                        events: {
                            'onReady': self.onPlayerReady.bind(self),
                            'onStateChange': self.onPlayerStateChange.bind(self)
                        }
                    });

                    $('#' + videoOptions.videoContainerId).fadeIn();
                    self.playerButtonClick = false;
                }
            },

            onPlayerReady: function(options) {
                var self = this,
                    view = self.view;
                    
                if (isVideoPlaying) {
                    videoPlayProgressTimer = setInterval(self.onPlayerProgressChange.bind(self), 500);
                }
                                
                YT_GA.progress25 = false;
                YT_GA.progress50 = false;
                YT_GA.progress75 = false;
                YT_GA.progress100 = false;

                if (videoMuteAudio) {
                    videoPlayer.mute();
                } else {
                    videoPlayer.unMute();
                }
                if (view._onPlayerReady) {
                    view._onPlayerReady(videoPlayer);
                }
            },

            onPlayerProgressChange: function() {
                var view = this.view,
                    progress;
                
                YT_GA.timePercentComplete = Math.round(videoPlayer.getCurrentTime() / videoPlayer.getDuration() * 100);

                // Divides overall progress by a quarter
                // If progress hits 25, 50, 75 or 100, update analytics progress
                if (YT_GA.timePercentComplete > 24 && !YT_GA.progress25) {
                    progress = '25%';
                    YT_GA.progress25 = true;

                }

                if (YT_GA.timePercentComplete > 49 && !YT_GA.progress50) {
                    progress = '50%';
                    YT_GA.progress50 = true;
                }

                if (YT_GA.timePercentComplete > 74 && !YT_GA.progress75) {
                    progress = '75%';
                    YT_GA.progress75 = true;
                }
                
                if (YT_GA.timePercentComplete === 100 && !YT_GA.progress100) {
                    progress = '100%';
                    YT_GA.progress100 = true;
                }

                if (progress) {
                    if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                        view._progressTrack(videoSource, videoId, progress);
                    }
                }
            },

            onPlayerStateChange: function(evt) {
                var self = this,
                    view = self.view;

                if (view._playerStateChange) {
                    view._playerStateChange(videoPlayer, evt);
                }
                
                if (evt.data === -1 && self.videoFlag) {
                    self.resetVideoPlayer();
                }
            }
        });
        
        return VideoPlayerServiceV2;
    });
});
