/*global define*/

define(['commonService'], function() {
    'use strict';
    
    var productIdsObject = {},
        commonService = null;

    IEA.service('ratings', function(service, app, iea) {

        /**
         * Description
         * @method
         * @return ThisExpression
         */
        var Ratings = function() {
            return this;
        };

        // extend definition prototype
        _.extend(Ratings.prototype, {
            
            initialize: function(options) {
                commonService = new IEA.commonService(); // creates new instance for common service

                return this;
            },
            
            initRatings: function(options) {
                var self = this;
                if (typeof options.ratingReview !== 'undefined' && (options.ratingReview.enabled === 'true' || options.ratingReview.enabled)) {
                    var service = options.ratingReview.serviceProviderName;

                    if (service === 'kritique') {
                        this._loadKQScript(options);
                    } else if(service === 'bazaarvoice') {
                        this._checkScriptBazaarVoice(options);
                    }
                }

                // write a review button tracking
                $(document).on('click', '.write-review-btn', function() {
                    self._ctProductInfo(options, 'reviews');
                });
            },
            
            _loadKQScript:function(options){
                var data = options.ratingReview,
                    brandId = data.brandId,
                    brandName = options.brandName,
                    localeId = data.localeId,
                    apikey = data.apiKey,
                    serviceURI = data.serviceURI,
                    siteSource = data.sitesource,
                    widgetId = data.id;
                
                if ($('script[src*=\'RR_widget_config.js\']').length === 0) {
                    var service = document.createElement('script'),
                    headScript = document.getElementsByTagName('script')[0],
                    config = document.createElement('script');

                    service.type = 'text/javascript';
                    service.async = true;
                    service.id = widgetId;
                    service.src = serviceURI + '?brandid=' + brandId + '&localeid=' +localeId + '&apikey=' +apikey + '&siteSource=' + siteSource;
                    headScript.parentNode.insertBefore(service, headScript);

                    config.type = 'text/javascript';
                    config.async = true;
                    config.src = commonService.getJsConfigUrl(brandName)+'/config/RR_widget_config.js';
                    headScript.parentNode.insertBefore(config, headScript);
                }
            },
                        
            _loadBazaarVoiceScript: function(options, callback) {
                var data = options.ratingReview,
                    service = document.createElement('script');

                service.type = 'text/javascript';
                service.async = true;

                if (service.readyState){
                    service.onreadystatechange = function(){
                        if (service.readyState === 'loaded' || service.readyState === 'complete') {
                            service.onreadystatechange = null;
                            callback();
                        }
                    };
                } else {
                    service.onload = function() {
                        callback();
                    };
                }
                service.src = data.serviceURI;
                document.querySelector('head').appendChild(service);
            },
            
            _kritiqueRatings: function(kq) {
                if (typeof kq.widget !== 'undefined') {
                    kq.widget.rrReloadWidget();
                }
            },

            _checkScriptBazaarVoice: function(options) {
                var data = options.ratingReview,
                    serviceURI = data.serviceURI,
                    self = this;

                /* globals $BV */
                function bvConfigure(options) {
                    var productId = options.productId;
                    
                    /* globals UDM */
                    /* globals digitalData */
                    /* globals ctConstants */
                    if (typeof options.ratingObj !== 'undefined') {
                        if(options.ratingObj.viewType === 'primary') {
                            $BV.configure('global', {
                                productId : productId,
                                events: {
                                    bvRender: function (data) {
                                        self._ctProductInfo(options, 'render');
                                    },
                                    submissionLoad: function (data) {
                                        self._ctProductInfo(options, 'open');
                                    },
                                    submissionClose: function (data) {
                                        self._ctProductInfo(options, 'close');
                                    },
                                    submissionSubmitted: function (data) {
                                        self._ctProductInfo(options, 'reviews');
                                    }
                                }
                            });

                            $BV.ui('rr', 'show_reviews');

                        } else if(options.ratingObj.viewType === 'inline') {
                            var pidKey;

                            for (var i = 0; i < productId.length; i++) {
                                pidKey = productId[i].ratings.uniqueId;
                                productIdsObject[pidKey] = {
                                    containerId : 'BVRRInlineRating-' + pidKey
                                };
                            }

                            $BV.ui( 'rr', 'inline_ratings', {
                                productIds : productIdsObject
                            });
                        }
                    }
                }

                if ($('script[src*=\'bvapi.js\']').length === 0) {
                    this._loadBazaarVoiceScript(options, function() {
                        bvConfigure(options);
                    });
                } else {
                    if (typeof $BV === 'object') {
                        bvConfigure(options);
                    } else {
                        $('script[src*=\'bvapi.js\']').load(function() {
                            bvConfigure(options);
                        });
                    }
                }
            },

            // Analytics: Track Component Info
            _ctComponentInfo: function(options) {
                var compName = options.componentName,
                    compVariants = options.componentVariants,
                    compPositions = options.componentPositions;

                digitalData.component = [];
                digitalData.component.push({
                    'componentInfo' :{
                        'componentID': compName,
                        'name': compName
                    },
                    'attributes': {
                        'position': compPositions,
                        'variants': compVariants,
                    }
                });
            },

            // Analytics: Track Product Info
            _ctProductInfo: function (options, event) {
                var brandName = options.brandName,
                    service = options.ratingReview.serviceProviderName,
                    eventAction, ev = {};

                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
                    // track component info
                    this._ctComponentInfo(options);
                    
                    switch (event) {
                        case 'open':
                            eventAction = ctConstants.bvopen;
                            break;
                        case 'render':
                            eventAction = ctConstants.bvrenders;
                            break;
                        case 'close':
                            eventAction = ctConstants.bvclose;
                            break;
                        case 'reviews':
                            eventAction = ctConstants.ratingreview;
                            break;
                    }
                        
                    if(service === 'bazaarvoice'){
                        var  productId = options.productId,
                            prodname = options.productName,
                            prodCat = options.productCategory;

                        digitalData.product = [];
                        digitalData.product.push({
                            'productInfo' :{
                                'productID': productId,
                                'productName': prodname
                            },
                            'category':{
                                'primaryCategory': prodCat
                            }
                        });

                        ev.eventInfo = {
                            'type': ctConstants.trackEvent,
                            'eventAction': eventAction,
                            'eventLabel': prodname
                        };
                    }
                    else if(service === 'kritique') {
                        var  recipeId = options.recipeId,
                            recipename = options.recipeName;

                        ev.eventInfo = {
                            'type': ctConstants.trackEvent,
                            'eventAction': eventAction,
                            'eventLabel': recipename
                        };
                    }
                    ev.category ={'primaryCategory':ctConstants.custom};
                    ev.attributes={'nonInteractive':{'nonInteraction': 1}};
                    digitalData.event.push(ev);
                }
            }
        });
        return Ratings;
    });
});


