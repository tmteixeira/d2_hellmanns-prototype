/*global define*/

define(function() {
    'use strict';

    var animService = null;

    IEA.service('BinWidget', function(service, app, iea) {

        /**
         * Description
         * @method
         * @return ThisExpression
         */
        var BinWidget = function() {
            return this;
        };

        // extend definition prototype
        _.extend(BinWidget.prototype, {
            
            initialize: function(options) {
                return this;
            },

            injectBinJS: function(options) {
                if (options.shopNow.serviceProviderName !== 'shoppable' && options.shopNow.serviceProviderName !== 'etale' && $('script[src*="'+options.shopNow.serviceURL+'"]').length === 0) {
                    var service = document.createElement('script'),
                    headScript = document.getElementsByTagName('script')[0],
                    inlineScript = document.getElementsByTagName('script')[0],
                    config = document.createElement('script');

                    service.type = 'text/javascript';
                    service.async = true;
                    service.src = options.shopNow.serviceURL;
                    headScript.parentNode.insertBefore(service, headScript);

                    if (options.shopNow.serviceProviderName === 'constantcommerce') {
                        inlineScript.type = 'text/javascript';
                        inlineScript.innerHTML = 'window.constantCoAppId= "'+options.shopNow.applicationID+'"';

                        inlineScript.parentNode.insertBefore(service, inlineScript);
                    }
                }
            }
        });
        return BinWidget;
    });
});


