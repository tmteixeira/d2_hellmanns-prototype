/*global define*/

define(['TweenMax'], function(TweenMax) {
    'use strict';

    IEA.service('accordion', function(service, app, iea) {

        /**
         * Description
         * @method
         * @return ThisExpression
         */
        var Accordion = function() {
            return this;
        };

        // extend definition prototype
        _.extend(Accordion.prototype, {
            
            initialize: function(options) {
                var triggerMethod = IEA.triggerMethod,
                defaultSettings = {
                    headerContainer: '.c-expandcollapse__header',
                    speed: 700
                };
                this.options = _.extend(defaultSettings, options);
                
                return this;
            },

            expand: function(elem, options) {
                this.triggerMethod('BeforeExpand', elem);

                var wrapper = (options && options.wrapper !== undefined) ? options.wrapper : this.options.headerContainer,
                    elemsExpanded = $(wrapper).siblings('.expanded'),
                    self = this;

                if (elemsExpanded.length) {
                    elemsExpanded.each(function (i, element) {
                        var $element = $(element);
                        self.collapse($element);
                    });
                }

                elem.removeClass('collapsed')
                .addClass('expanded')
                .next()
                .slideDown(this.options.speed)
                .toggleClass('o-hidden');

                this.triggerMethod('AfterExpand', elem);
                TweenMax.to(elem.find('.c-svg.plus'), 0.6, {rotation:45});
            },

            collapse: function(elem) {
                this.triggerMethod('BeforeCollapse', elem);
                elem.addClass('collapsed')
                .removeClass('expanded')
                .next()
                .slideUp(function() {
                    elem.next().toggleClass('o-hidden');
                });

                this.triggerMethod('AfterCollapse', elem);
                TweenMax.to(elem.find('.c-svg.plus'), 0.6, {rotation:0});
            },

            _handleExpandCollapse: function(evt, options) {
                var self = this,
                    target = $(evt.currentTarget).parent();
                
                evt.preventDefault();
                target.hasClass('expanded') ? self.collapse(target) : self.expand(target, options);
            }
        });

        return Accordion;

    });

});