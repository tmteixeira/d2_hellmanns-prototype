/*global define*/

define(['animService', 'typeahead'], function () {
    'use strict';

    IEA.service('search', function(service, app, iea) {

        /**
         * Description
         * @method
         * @return ThisExpression
         */
        var Search = function() {
            return this;
        };

        // extend definition prototype
        _.extend(Search.prototype, {
            
            initialize: function(options) {
                var triggerMethod = IEA.triggerMethod,
                defaultSettings = {
                    
                };
                this.options = _.extend(defaultSettings, options);
                
                return this;
            },

            /**
             * Auto complete
             * @method autoComplete
             * @return 
             */
            showSuggestions: function (options) {
                // constructs the suggestion engine
                var self = this,
                    dropdownLabel = options.dropDownLabel,
                    keyword = $(options.searchWrapper).attr('data-keyword'),
                    keywordSearchDefault, keywordSearch,
                    defaultSettings = {
                        selectCallback: function() {}
                    };

                options = $.extend(defaultSettings, options);

                $(options.searchInputField).typeahead({
                    hint: (options.hint) ? options.hint : false,
                    highlight: true,
                    minLength: (options.minLengthToTriggerAutoSuggest) ? options.minLengthToTriggerAutoSuggest : 3
                },
                {
                    name: 'keywordSearchDefault',
                    source: function (query, sync, async) {
                        if (options.customKeyword) {
                            var suggestions = [],
                                article = '',
                                product = '',
                                everything = '',
                                queryVal;

                            if ((query.indexOf(dropdownLabel.inArticles) !== -1) || (query.indexOf(dropdownLabel.inProducts) !== -1) || (query.indexOf(dropdownLabel.inEverything) !== -1)) {
                                queryVal = query.split(' ')[0];
                            } else {
                                queryVal = query;
                            }
                            if (dropdownLabel.length) {
                                $(dropdownLabel).each(function(key, value) {
                                    var defaultValue = options.inLabel + ' ' + value.contentType;
                                    if (query.indexOf(defaultValue) === -1) {
                                        suggestions.push(query + ' ' + options.inLabel + ' ' + value.contentType);
                                    }
                                });
                            } else {
                                article = queryVal+ ' ' +dropdownLabel.inArticles;
                                product = queryVal+ ' ' +dropdownLabel.inProducts;
                                everything = queryVal+ ' ' +dropdownLabel.inEverything;

                                suggestions.push(product);
                                suggestions.push(article);
                                suggestions.push(everything);
                            }
                            sync(suggestions);
                        }
                    }
                },
                {
                    name: 'keywordSearch',
                    limit: options.suggestionsLimit,
                    beforeSelectedVal: this.query,
                    source: function (query, sync, async) {
                        $.ajax(keyword+ '?q='+query+'&Locale='+options.locale+'&BrandName='+options.brandName, {
                            success: function(data, status) {
                                async(data);
                            }
                        });
                    }
                }).on('keyup', function (event) {
                    var _this = $(this),
                    textLength = _this.val().length,
                    kCode = event.keyCode || event.charCode; //for cross browser
                    
                    if (_this.typeahead('val').length > 0 && !_this.typeahead('val').match(/^\s*$/)) {
                        $(options.clearSearch).removeClass('hidden');
                    } else {
                        $(options.clearSearch).addClass('hidden');
                    }

                    //callback method
                    if (options.keyupCallback && kCode === 13) {
                        options.keyupCallback(options, event, _this.val());
                    }

                    self.shrinkToFill(_this.next('pre'), options.inputFont, _this);

                }).bind('typeahead:selected', function(obj, selected, name) {
                    var _this = $(this),
                        keywordVal = _this.val(),
                        textLength = keywordVal.length;
                    
                    _this.typeahead('close');
                    
                    //callback method
                    options.selectCallback(options, keywordVal, _this);

                    _this.next('pre').text(options.searchKeyword);
                    _this.typeahead('val', options.searchKeyword);
                    _this.val(options.searchKeyword);
                    
                    //shrink font size on input keyup
                    self.shrinkToFill(_this.next('pre'), options.inputFont, _this);

                }).bind('typeahead:render', function(ev, suggestion) {
                    var _this = $(this),
                        keywordVal = _this.val(),
                        typeheadWrapper = _this.parent(),
                        suggestions = typeheadWrapper.find('.tt-dataset-keywordSearch .tt-suggestion'),
                        hintField = typeheadWrapper.find('.tt-hint');

                    if (suggestions && options.hint) {
                        suggestions.each(function(key, value) {
                            var defaultValue = value.innerText.toLowerCase();
                            if (defaultValue.indexOf(keywordVal.toLowerCase()) === 0) {
                                hintField.val(defaultValue);
                                return false;
                            }
                        });
                    }
                });
            },

            showSuggestionsV2: function (options) {
                // constructs the suggestion engine
                var self = this,
                    dropdownLabel = options.dropDownLabel,
                    keyword = $(options.searchWrapper).attr('data-keyword'),
                    keywordSearchDefault, keywordSearch,
                    defaultSettings = {
                        selectCallback: function() {}
                    };

                options = $.extend(defaultSettings, options);

                $(options.searchInputField).typeahead({
                    hint: (options.hint) ? options.hint : false,
                    highlight: true,
                    minLength: (options.minLengthToTriggerAutoSuggest) ? options.minLengthToTriggerAutoSuggest : 3
                },
                {
                    name: 'keywordSearchDefault',
                    source: function (query, sync, async) {
                        if (options.customKeyword) {
                            var suggestions = [],
                                queryVal,
                                searchTextContainsTab;

                            searchTextContainsTab = (function(){
                                var flag = false;

                                if(options.tabContentTypes && options.tabContentTypes.length) {
                                    options.tabContentTypes.forEach(function(val){
                                        flag = flag || (query.indexOf(val.contentType) > -1);
                                    });
                                }

                                return flag;
                            })();

                            if (searchTextContainsTab) {
                                queryVal = query.split(' ')[0];
                            } else {
                                queryVal = query;
                            }
                            if (options.tabContentTypes.length) {
                                options.tabContentTypes.forEach(function(val){
                                    var defaultValue = options.dropDownLabel + ' ' + val.contentType;

                                    if (query.indexOf(defaultValue) === -1) {
                                        suggestions.push(query + ' ' + defaultValue);
                                    }
                                });
                            }
                            sync(suggestions);
                        }
                    }
                },
                {
                    name: 'keywordSearch',
                    limit: options.suggestionsLimit,
                    beforeSelectedVal: this.query,
                    source: function (query, sync, async) {
                        $.ajax(keyword+ '?q='+query+'&Locale='+options.locale+'&BrandName='+options.brandName, {
                            success: function(data, status) {
                                async(data);
                            }
                        });
                    }
                }).on('keyup', function (event) {
                    var _this = $(this),
                    textLength = _this.val().length,
                    kCode = event.keyCode || event.charCode; //for cross browser
                    
                    if (_this.typeahead('val').length > 0 && !_this.typeahead('val').match(/^\s*$/)) {
                        $(options.clearSearch).removeClass('hidden');
                    } else {
                        $(options.clearSearch).addClass('hidden');
                    }

                    //callback method
                    if (options.keyupCallback && kCode === 13) {
                        options.keyupCallback(options, event, _this.val());
                    }

                    self.shrinkToFill(_this.next('pre'), options.inputFont, _this);

                }).bind('typeahead:selected', function(obj, selected, name) {
                    var _this = $(this),
                        keywordVal = _this.val(),
                        textLength = keywordVal.length;
                    
                    _this.typeahead('close');
                    
                    //callback method
                    options.selectCallback(options, keywordVal, _this);

                    _this.next('pre').text(options.searchKeyword);
                    _this.typeahead('val', options.searchKeyword);
                    _this.val(options.searchKeyword);
                    
                    //shrink font size on input keyup
                    self.shrinkToFill(_this.next('pre'), options.inputFont, _this);

                }).bind('typeahead:render', function(ev, suggestion) {
                    var _this = $(this),
                        keywordVal = _this.val(),
                        typeheadWrapper = _this.parent(),
                        suggestions = typeheadWrapper.find('.tt-dataset-keywordSearch .tt-suggestion'),
                        hintField = typeheadWrapper.find('.tt-hint');

                    if (suggestions && options.hint) {
                        suggestions.each(function(key, value) {
                            var defaultValue = value.innerText.toLowerCase();
                            if (defaultValue.indexOf(keywordVal.toLowerCase()) === 0) {
                                hintField.val(defaultValue);
                                return false;
                            }
                        });
                    }
                });
            },

            measureText: function(field, fontsize) {
                var $tag = $(field);
                if ($tag.length) {
                    $tag.css('font-size', fontsize);
                }
                return {
                    width: $tag.width(),
                    height: $tag.height()
                };
            },

            shrinkToFill: function(field, fontsize, input) {
                var $input = $(input),
                    maxWidth = $input.width() + 5, // add some padding
                    fontSize = fontsize + 'px';
                // see how big the text is at the default size
                
                var textWidth = this.measureText(field, fontsize).width;
                if (textWidth > maxWidth) {
                    // if it's too big, calculate a new font size
                    // the extra .9 here makes up for some over-measures
                    fontSize = fontsize * maxWidth / textWidth * '.95';
                    fontSize = fontSize + 'px';
                    // and set the style on the input
                    $input.css('font-size', fontSize);
                } else {
                    // in case the font size has been set small and 
                    // the text was then deleted
                    $input.css('font-size', fontsize);
                }
            },
            clearSearchVal: function(selector, field) {
                var self = this,
                    $field = $(field);

                $(document).on('click', selector, function (e) {
                    e.preventDefault();
                    $field.typeahead('close');
                    $field.val('').typeahead('val', '').focus();
                    $field.next('pre').text('');
                    $field.css('font-size', '30px');
                    $(this).addClass('hidden');
                });
            }
        });
        return Search;
    });
});
