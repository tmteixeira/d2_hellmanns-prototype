/*global define*/

define(function() {
    'use strict';
    var turnToConfig;
    IEA.service('turnToService', function(service, app, iea) {

        /**
         * Description
         * @method
         * @return ThisExpression
         */
        var TurnToService = function() {
            return this;
        };


        // extend defenition prototype
        _.extend(TurnToService.prototype, {
            initialize: function() {
                return this;
            },
            
            initTurnTo: function(options) {
                // check if siteKey, setupType and skuId is defined
                if (options.siteKey && options.skuId && options.setupType && options.src && options.itemJs) {
                    turnToConfig = {
                        siteKey: options.siteKey,
                        skipCssLoad: true,
                        setupType: options.setupType
                    };
                    (function() {
                        var tt = document.createElement('script');
                        tt.type = 'text/javascript';
                        tt.async = true;
                        tt.src = options.src;
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(tt, s);
                        var TurnToItemSku = options.skuId;
                        var src = options.itemJs;
                        $('head').append($('<script />').attr('src', src));
                    })();
                }
            },
            
            initCommunityProductQna: function(options, loadScript) {
                // check if siteKey, setupType and skuId is defined
                if (options.siteKey && options.skuId && options.setupType && options.src && options.itemJS) {
                    if(!window.TurnToItemSku) {
                        window.TurnToItemSku = options.productID;
                    }
                    if(!window.turnToConfig) {
                        window.turnToConfig = {
                            siteKey: options.siteKey,
                            skipCssLoad: options.skipCssLoad,
                            setupType: options.setupType
                        };
                    }
                    
                    if($('link[href*=\'inputteasers.css\']').length === 0) {
                        $('head').append('<link rel="stylesheet" type="text/css" href="' + options.teaserCss + '"/>');
                    }
                    if($('link[href*=\'tra-en_US.css\']').length === 0) {
                        $('head').append('<link rel="stylesheet" type="text/css" href="' + options.traCss +'"/>');
                    }
                    loadScript(options);
                }
            }
        });

        return TurnToService;

    });
});