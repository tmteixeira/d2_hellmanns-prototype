/*global define*/

define(['TweenMax', 'commonService'], function(TweenMax) {
    'use strict';

    IEA.service('QuickPanel', function(service, app, iea) {

        /**
         * Description
         * @method
         * @return ThisExpression
         */
        var QuickPanel = function() {
            return this;
        };


        // extend defenition prototype
        _.extend(QuickPanel.prototype, {
            
            initialize: function() {
                return this;
            },

            /**
             * hide quick panel
             * @method closePanel
             * @return 
            */
            closePanel: function(options) {
                var $currentQuickPanel = $(options.quickPanelWrapper),
                    active = 'active',
                    currentIndex = $currentQuickPanel.data('index'),
                    $currentItem = $(options.listingItem+'[data-index='+currentIndex+']');

                this.setActiveParams(options, currentIndex);

                TweenMax.to(window, 0.5, {scrollTo: {y: $currentItem.offset().top - $currentItem.height()/2}});
            },

            /**
             * set parameters for quick panel carousel
             * @method closePanel
             * @return 
            */
            setActiveParams: function(options, currentIndex) {
                var active = 'active',
                    nextNav = options.nextButton,
                    prevNav = options.prevButton,
                    $listingItem = $(options.listingItem);

                $(options.quickPanelWrapper).removeClass(active);
                $listingItem.removeClass(active).css('margin-bottom', options.itemMargin);

                if (currentIndex === 0) {
                    prevNav.fadeOut(options.animateSpeed);
                    if ($listingItem.length === 1) {
                        nextNav.fadeOut(options.animateSpeed);
                    } else {
                        nextNav.fadeIn(options.animateSpeed);
                    }
                    
                } else if (currentIndex === $listingItem.length - 1) {
                    prevNav.fadeIn(options.animateSpeed);
                    nextNav.fadeOut(options.animateSpeed);
                } else {
                    prevNav.fadeIn(options.animateSpeed);
                    nextNav.fadeIn(options.animateSpeed);
                }
            },

            /**
             * scroll to next panel
             * @method nextItem
             * @return 
            */
            nextItem: function(options, index) {
                var currentIndex = index + 1;

                $(options.listingItem+'[data-index='+currentIndex+'] '+options.quickPanelCta).click();
            },

            /**
             * scroll to prev panel
             * @method prevItem
             * @return 
            */
            prevItem: function(options, index) {
                var currentIndex = index - 1;
                
                $(options.listingItem+'[data-index='+currentIndex+'] '+options.quickPanelCta).click();
            },

            /**
             * reset quick panel parameters on window resize
             * @method resetPanelOnResize
             * @return 
            */
            resetPanelOnResize: function(options) {
                // Can't use app.on('window:resized') because it doesn't get triggered immediately
                var self = this,
                    commonService = new IEA.commonService(),
                    windowResized,
                    screenWidth = $(window).width(),
                    $currentQuickPanel = $(options.quickPanelWrapper+'.active'),
                    currentIndex = $currentQuickPanel.data('index');

                windowResized = commonService.debounce(function() {
                    if(screenWidth !== $(window).width()) {
                        self.setActiveParams(options, currentIndex);

                        screenWidth = $(window).width();
                    }

                }, 1000, true);

                window.addEventListener('resize', windowResized);
            },

            /**
             * animate resuls with fadeIn / fadeOut effect
             * @method animateResults
             * @return 
            */
            animateResults: function(item, preloader, speed, display) {
                if (display === 'show') {
                    TweenMax.to($(item), 0.8, {autoAlpha: 1, top: 0, delay: 0.2});
                    $(preloader).fadeOut(speed);
                } else {
                    TweenMax.to($(item), 0.8, {autoAlpha: 0, top: '30px', delay: 0.2});
                    $(preloader).fadeIn(speed);
                }
            }
        });

        return QuickPanel;

    });

});


