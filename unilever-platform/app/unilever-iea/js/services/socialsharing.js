/*global define*/
/*global FB*/

define(['TweenMax'], function(TweenMax) {
    'use strict';

    IEA.service('socialsharing', function(service, app, iea) {
        /**
         * Description
         * @method
         * @return ThisExpression
         */
        var Socialsharing = function() {
            return this;
        };

        // extend definition prototype
        _.extend(Socialsharing.prototype, {
            
            initialize: function(options) {
                return this;
            },

            enableSocial: function(options) {
                var self = this,
                    isOpen = false,
                    $socialSharingMenu = options.socialSharingMenu;

                $socialSharingMenu.on({
                    'mouseover': function() {
                        self._toggleSocialStack(this, true, options);
                    },
                    'mouseleave': function() {
                        self._toggleSocialStack(this, false, options);
                    },
                    'click':function(e){
                        e.stopImmediatePropagation();
                        if (!isOpen){
                            self._toggleSocialStack(this, true, options);
                            isOpen = true;
                        } else{
                            self._toggleSocialStack(this, false, options);
                            isOpen = false;
                        }
                    }
                });
            },

            _toggleSocialStack: function(element, val, options) {
                if(val){
                    TweenMax.to($(element).find(options.socialSharing), 0.3, {autoAlpha: 1, display: 'block'}, 0.2);
                } else{
                    TweenMax.to($(element).find(options.socialSharing), 0.3, {autoAlpha: 0, display: 'none'});
                }
            },
            
            _loadFacebookSdk: function(appId, appUrl) {
                $.ajaxSetup({
                    cache: true
                });
                
                $.getScript(appUrl, function () { // TODO: Pick url from json
                    FB.init({
                        appId: appId,
                        xfbml: true,
                        version: 'v2.7'
                    });
                });
                
                (function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) {
                        return;
                    }
                    js = d.createElement(s);
                    js.id = id;
                    js.src = appUrl;
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
            },
            
            _customSocialShare: function(appId, shareObj) {
                var url = '';
                
                switch(shareObj.shareVia) {
                    case 'twitter':
                        url = 'https://twitter.com/share/?url=' + shareObj.postLink +'&text=' + shareObj.postDescriptionTwitter;
                        break;
                    case 'facebook':
                        url = 'https://www.facebook.com/dialog/feed?app_id=' + appId +
                                '&display=popup&caption=' + shareObj.postDescriptionFacebook +
                                '&link=' + shareObj.postLink +
                                '&name=' + shareObj.postName +
                                '&redirect_uri=' + shareObj.postLink +
                                '&picture=' + shareObj.postImage;
                        break;
                    default:
                        return null;
                }
                
                window.open(url, 'name', 'width=600,height=400');
            }
        });

        return Socialsharing;

    });

});


