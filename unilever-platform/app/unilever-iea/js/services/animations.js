/*global define*/

define(['TweenMax', 'scrollTo', 'jquery.highlight', 'commonService'], function (TweenMax) {
    'use strict';

    IEA.service('animService', function (service, app, iea) {
        // To-DO: Unless we can retrieve the image height from the CMS, we should hardcode it for the zoom feature
        var DEFAULT_HEIGHT = 1400,
            DEFAULT_IMAGE_HEIGHT = DEFAULT_HEIGHT,
            DEFAULT_SPEED = 1,
            DEFAULT_MOBILE = 985,
            isMobile = false,
            $modal = null,
            $modalImage = null,
            $modalSwap = null,
            commonService = null;


        /**
         * Description
         * @method
         * @return ThisExpression
         */
        var AnimService = function () {
            return this;
        };


        // extend defenition prototype
        _.extend(AnimService.prototype, {
            initialize: function () {

                var self = this;
                commonService = new IEA.commonService();

                // Scroll to anchors (common functionality. Initialised by default)
                $('[data-anim="scroll"]').click(function (e) {
                    e.preventDefault();
                    self.scrollTo($($(this).attr('href')).offset().top);
                });

                self.mobileCheck();

                return this;
            },
            mobileCheck: function () {
                var val = null;
                if (commonService.isMobile() && $(window).width() <= DEFAULT_MOBILE || commonService.isMobileAndTablet()) {
                    DEFAULT_IMAGE_HEIGHT = DEFAULT_MOBILE;
                    isMobile = true;

                    val = DEFAULT_MOBILE / 3;
                    TweenMax.to('.o-modal-content', 1, {scrollTo: {x: val, y: val}});

                }else {
                    DEFAULT_IMAGE_HEIGHT = DEFAULT_HEIGHT;
                    isMobile = false;
                }

                return isMobile;
            },

            initSyntax: function (target) {

                // Syntax highlight (only in dev mode)
                $('pre.code').highlight({
                    source: true,
                    zebra: true,
                    indent: 'tabs',
                    list: 'ol',
                    attribute: 'lang'
                });
            },

            blurBackground:function(arr, val){

                for(var i=0; i< arr.length; i++){
                    var obj = arr[i];
                    if(val){
                        $('.'+ obj).addClass('u-blur-background');
                    }else{
                        $('.'+ obj).removeClass('u-blur-background');
                    }
                }

            },
            initProducts: function () {

                // PLP animations
                var $products = $('[data-anim="product"]');
                if ($products.length > 0) {
                    // On page load
                    // $products.each(function (i, element) {
                    //     $(element).delay(i * 300).queue(function (next) {
                    //         $(this).addClass('float');
                    //         next();
                    //     });
                    // });

                    // On scroll reaching the products' list
                    var productsOffset = $products.first().offset().top,
                        windowHeight = $(window).height();

                    $(document).on('scroll', function (e) {

                        var currentOffset = $(window).scrollTop();

                        if (currentOffset > productsOffset - (windowHeight / 1.5)) {
                            $products.each(function (i, element) {
                                $(element).delay(i * 300).queue(function (next) {
                                    TweenMax.to($(this), 0.8, {y: -8, opacity: 1});
                                    next();
                                });
                            });
                        }
                    });
                }
            },

            initParallax: function (speed) {

                // Parallax init
                var $parallaxLayers = $('[data-anim="parallax"]');

                if ($parallaxLayers.length > 0) {

                    var windowHeight = $(window).height();

                    // Iterate over each object in collection
                    $parallaxLayers.each(function () {

                        var $this = $(this);

                        // Set up Scroll Handler
                        $(document).scroll(function () {

                            var scrollTop = $(window).scrollTop(),
                                offset = $this.offset().top,
                                height = $this.outerHeight();

                            // Check if above or below viewport
                            if (offset + height <= scrollTop || offset >= scrollTop + windowHeight) {
                                return;
                            }

                            var yBgPosition = Math.round((offset - scrollTop) * speed);

                            // Apply the Y Background Position to Set the Parallax Effect
                            $this.css('background-position', 'center ' + yBgPosition + 'px');

                        });
                    });
                }
            },

            initParallaxEffect: function(view, isAnimationOutsideOfViewport) {

                var $parallaxLayers = view.$el.find('[data-anim="parallax"]');
                var eventToListen = 'scroll';
                var $window = $(window);

                var setBgImageTopPosition = function($image) {

                    var windowHeight = $window.height();
                    var scrollTop = $window.scrollTop();

                    var topOffset = $image.offset().top;
                    var height = $image.outerHeight();

                    if ((topOffset + height <= scrollTop || topOffset >= scrollTop + windowHeight) && !isAnimationOutsideOfViewport) {

                        return;
                    }

                    var speed = Number($image.attr('data-anim-speed'));
                    var topPosition = Math.round((topOffset - scrollTop) * speed);
                    var style = {
                        'webkitTransform': 'translate3d(0, ' + topPosition + 'px, 0)',
                        'MozTransform': 'translate3d(0, ' + topPosition + 'px, 0)',
                        'msTransform': 'translate3d(0, ' + topPosition + 'px, 0)',
                        'OTransform': 'translate3d(0, ' + topPosition + 'px, 0)',
                        'transform': 'translate3d(0, ' + topPosition + 'px, 0)'
                    };

                    $image.css(style);
                };

                var scrollHandler = function() {

                    $parallaxLayers.each(function(index, item) {

                        setBgImageTopPosition($(item), index);
                    });

                    if (typeof view.parallaxScrollingHandler === 'function') {

                        view.parallaxScrollingHandler();
                    }
                };

                if ($parallaxLayers.length > 0) {

                    $window.on(eventToListen, scrollHandler);
                    scrollHandler();
                }
            },

            initZoom: function () {

                var self = this;

                // Product image zoom
                var $images = $('[data-zoom-image]');

                if ($modal === null) {

                    if ($images.length > 0) {

                        $modal = $('.js-modal'),
                            $modalImage = $modal.find('.js-modal-image'),
                            $modalSwap = $modal.find('.js-modal-image-swap');

                        // Create a click event to all elements with zoom
                        $images.each(function () {
                            self.zoomEvents($(this), self);
                        });

                        // Zoom image movement
                        $modalImage.mousemove(function (e) {
                            self.zoomMove($(this), e.clientY);
                        });

                        // Zoom image close
                        $modalImage.on('click', function () {
                            TweenMax.to($modal, DEFAULT_SPEED, {autoAlpha: 0});
                            $('body').removeClass('open-zoom-modal').removeAttr('style');
                        });
                    }
                }
                else {
                    $images.each(function () {
                        $(this).off('click').unbind();
                        self.zoomEvents($(this), self);
                    });
                }
            },
            zoomOpenUrl: function (url) {
                var self = this;

                //setup on first load

                if ($modal === null) {
                    $modal = $('.js-modal');
                    $modalImage = $modal.find('.js-modal-image');
                    $modalSwap = $modal.find('.js-modal-image-swap');

                    // Zoom image movement
                    $modalImage.mousemove(function (e) {
                        self.zoomMove($(this), e.clientY);
                    });

                    // Zoom image close
                    $modalImage.on('click', function () {
                        $('body').removeClass('open-zoom-modal').removeAttr('style');
                        TweenMax.to($modal, DEFAULT_SPEED, {autoAlpha: 0, display: 'none'});
						self.blurBackground(['js-blur-bg'], false);
                    });

                    // Close on press ESC
                    $(document).keyup(function(e) {
                        if (e.keyCode === 27) { // escape key maps to keycode `27`
                            $('.js-modal-image').trigger('click');
                        }
                    });
                }
                self.mobileCheck();
                //load image
                this.zoom(url, window.innerHeight / 2);
                self.blurBackground(['js-blur-bg'], true);
            },
            zoomEvents: function ($el, self) {

                $el.on('click', function (e) {
                    self.zoom($el.data('zoom-image'), e.clientY);
                });
            },

            zoom: function (imageURL, pos, imageHeight) {

                if ($modal !== null) {

                    // Create image
                    $modalImage.css('backgroundImage', 'url("' + imageURL + '")');

                    // Prepare the image
                    var h = typeof imageHeight !== 'undefined' ? imageHeight : DEFAULT_IMAGE_HEIGHT;

                    if (commonService.isMobile()) {
                        $modalImage.width(DEFAULT_MOBILE);
                    } else {
                        //$modalImage.width(h);
                        $modalImage.width($(window).width());

                    }
                    $modalImage.height(h);

                    // Prepare page and animate
                    $('body').css('overflow', 'hidden');
                    TweenMax.to($modal, DEFAULT_SPEED, {autoAlpha: 1, display: 'block'});
                }
            },

            zoomMove: function ($el, pos, imageHeight) {

                var h = typeof imageHeight !== 'undefined' ? imageHeight : DEFAULT_IMAGE_HEIGHT,
                    windowHeight = $(window).height(),
                    screenOffset = h - windowHeight, // $(this).height()
                    normalizedPos = 1 - (pos / windowHeight), // 0 to 1
                    newPos = -screenOffset + (normalizedPos * screenOffset);

                if(!commonService.isMobileAndTablet()){
                    $el.css('top', newPos + 'px');
                }

            },

            zoomSwap: function (imageURL, imageHeight) {

                $modalImage.css('backgroundImage', 'url("' + imageURL + '")');
                $modalSwap = $modalImage;

                // Prepare the image
                var h = typeof imageHeight !== 'undefined' ? imageHeight : DEFAULT_IMAGE_HEIGHT;
                //$modalImage.width($(window).width()).height(h);

                TweenMax.to($modalImage,0.5, {alpha: 0, onComplete: function () {
                        $modalImage.css('backgroundImage', 'url(' + imageURL + ')');
                        TweenMax.to($modalImage, 0.5, {alpha: 1});
                    }
                });
            },

            openOverlay: function (id, options) {
                var $modal = $(id),
                    defaultSettings = {
                        onCompleteCallback: function() {}
                    };

                options = $.extend(defaultSettings, options);

                if ($modal !== null) {
                    // Prepare page and animate
                    $('body').addClass('o-modal-open');

                    TweenMax.to($modal, DEFAULT_SPEED, {autoAlpha: 1, display: 'block', onComplete: function() {
                        options.onCompleteCallback();
                    }
                    });
                    this.blurBackground(['js-blur-bg'], true);
                }
            },

            closeOverlay: function (id) {
                var $modal = $(id);
                TweenMax.to($modal, DEFAULT_SPEED, {autoAlpha: 0});
                $('body').removeClass('o-modal-open');
                this.blurBackground(['js-blur-bg'], false);
            },

            scrollTo: function (offset) {
                TweenMax.to(window, DEFAULT_SPEED, {scrollTo: {y: offset, autoKill: true}});
            }

        });

        return AnimService;

    });

});


