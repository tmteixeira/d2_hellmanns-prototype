/*global define*/

define(['TweenMax', 'commonService'], function(TweenMax) {
    'use strict';

    IEA.service('SocialTabs', function(service, app, iea) {

        /**
         * Description
         * @method
         * @return ThisExpression
         */
        var SocialTabs = function() {
            return this;
        };


        // extend defenition prototype
        _.extend(SocialTabs.prototype, {
            
            initialize: function() {
                return this;
            },

            /**
             * set query parametrs for social tab service
             * @method setQueryParameters
             * @return 
            */
            setQueryParameters: function(componentJson, startIndex, isScroll) {
                var queryParamJson = componentJson.retrivalQueryParams,
                    hashTags = '', channelList = '',
                    queryParams, numberOfResults;

                if ($(queryParamJson.hashTags).length > 0) {
                    $(queryParamJson.hashTags).each(function(key, val) {
                        hashTags += 'h='+val.Id+'&';
                    });
                }

                if ($(queryParamJson.socialChannelNames).length > 0) {
                    $(queryParamJson.socialChannelNames).each(function(key, val) {
                        channelList += 'chnl='+val.name+'&';
                    });
                }

                if (isScroll) {
                    numberOfResults = queryParamJson.maximumImages;
                } else {
                    numberOfResults = queryParamJson.numberOfPosts;
                }

                if (hashTags === '' && channelList === '') {
                    queryParams = '';
                } else {
                    queryParams = '?'+hashTags+channelList+'si='+startIndex+'&rsno='+numberOfResults+'&br='+queryParamJson.brandNameParameter;
                }
                
                return queryParams;
            },

            /**
             * set query parametrs for social aggregator service
             * @method setDataParameters
             * @return 
            */
            setDataParameters: function (options) {
                var queryParamJson = options.componentJson.retrivalQueryParams,
                    dataParams,
                    numberOfResults,
                    hashTags = '',
                    youtube = null,
                    twitter = null,
                    instagram = null,
                    playlistId = null,
                    channelId = null,
                    secure;

                if (options.noLazyLoad) {
                    if(queryParamJson.maximumImages !== undefined || queryParamJson.maximumImages === 0) {
                        numberOfResults = queryParamJson.maximumImages;
                    } else {
                        numberOfResults = queryParamJson.limitResultsTo;
                    }
                } else {
                    if(queryParamJson.numberOfPosts !== undefined || queryParamJson.numberOfPosts === 0) {
                        numberOfResults = queryParamJson.numberOfPosts;
                    } else {
                        numberOfResults = options.componentJson.pagination.itemsPerPage;
                    }
                }

                if ($(queryParamJson.hashTags).length > 0) {
                    $(queryParamJson.hashTags).each(function(key, val) {
                        if (key === 0) {
                            hashTags += val.Id;
                        } else {
                            hashTags += ',' +val.Id;
                        }
                    });
                } else {
                    hashTags = null;
                }

                if (queryParamJson.channelOrPlayListType) {
                    if (queryParamJson.channelOrPlayListType === 'playListId') {
                        playlistId = queryParamJson.channelOrPlayListId;
                    } else {
                        channelId = queryParamJson.channelOrPlayListId;
                    }
                }
                
                if(options.componentJson.static !== undefined && options.componentJson.static.aggregationMode === 'manual') {
                    if(options.componentJson.socialConfiguration.length && $(queryParamJson.socialChannelNames).length) {
                        var socialChannelData;
                                                
                        if ($(queryParamJson.socialChannelNames).length) {
                            $(queryParamJson.socialChannelNames).each(function(key, val) {
                                var socialChannel = val.name.toLowerCase();
                                switch (socialChannel) {
                                    case 'twitter':
                                        socialChannelData =  filteredSocialChannelData(options.componentJson.socialConfiguration,'socialChannels','twitter');
                                        val.postId = socialChannelCollection(socialChannelData);
                                        
                                        twitter = {
                                            'accountId' : (val.accountId !== undefined) ? val.accountId : null,
                                            'hashtags' : hashTags,
                                            'postId' : (options.postId !== undefined) ? options.postId : val.postId
                                        };
                                        break;

                                    case 'instagram':
                                        socialChannelData =  filteredSocialChannelData(options.componentJson.socialConfiguration,'socialChannels','instagram');
                                        val.postId = socialChannelCollection(socialChannelData);
                                        
                                        instagram = {
                                            'accountId' : (val.accountId !== undefined) ? val.accountId : null,
                                            'hashtags' : hashTags,
                                            'postId' : (val.postId !== undefined) ? val.postId : null
                                        };
                                        break;

                                    case 'youtube':
                                        youtube = {
                                            'playlistId' : playlistId,
                                            'channelId' : channelId,
                                            'videoIds' : ''
                                        };
                                        break;
                                    default:
                                }
                            });
                        }
                    }
                } else {
                    if ($(queryParamJson.socialChannelNames).length) {
                        $(queryParamJson.socialChannelNames).each(function(key, val) {
                            var socialChannel = val.name.toLowerCase();
                            switch (socialChannel) {
                                case 'twitter':
                                    twitter = {
                                        'accountId' : (val.accountId !== undefined) ? val.accountId : null,
                                        'hashtags' : hashTags,
                                        'postId' : (options.postId !== undefined) ? options.postId : val.postId
                                    };
                                    break;

                                case 'instagram':
                                    instagram = {
                                        'accountId' : (val.accountId !== undefined) ? val.accountId : null,
                                        'hashtags' : hashTags,
                                        'postId' : (val.postId !== undefined) ? val.postId : null
                                    };
                                    break;

                                case 'youtube':
                                    youtube = {
                                        'playlistId' : playlistId,
                                        'channelId' : channelId,
                                        'videoIds' : ''
                                    };
                                    break;
                                default:
                            }
                        });
                    }
                }
                
                function filteredSocialChannelData(jsonObj, filterKey, filterQuery){
                    if(jsonObj){
                        return jsonObj.filter(function(row){
                            if(row[filterKey].toLowerCase() === filterQuery) {
                                return true;
                            }
                        });
                    } else {
                        return [];
                    }
                }
                
                function socialChannelCollection(obj){
                    var collection = '';
                    $.each(obj, function(key, val){
                        if(collection.length === 0){
                            collection = val.postId;
                        } else {
                            collection = collection + ',' + val.postId;
                        }
                    });
                    return collection;
                }
                
                dataParams = {
                    'locale': options.componentJson.locale,
                    'brand': options.componentJson.brandName,
                    'recordCount': numberOfResults,
                    'startIndex': options.startIndex,
                    'secure': (location.protocol === 'https:') ? true : false,
                    'mediaTypes': (queryParamJson.mediaType !== undefined) ? queryParamJson.mediaType : ['image'],
                    'youtube': youtube,
                    'twitter': twitter,
                    'instagram': instagram
                };
                
                return JSON.stringify(dataParams);
            },

            /**
             * hide quick panel
             * @method closePanel
             * @return 
            */
            closePanel: function(elm, settings, obj) {
                var _this = obj,
                    $currentQuickPanel = _this.parents(settings.quickPanelWrapper),
                    currentIndex = $currentQuickPanel.data('index'),
                    $currentItem = $(settings.socialGalleryItem+'[data-index='+currentIndex+']');

                this.setActiveParams(elm, settings, currentIndex);

                TweenMax.to(window, 0.5, {scrollTo: {y: $currentItem.offset().top - $currentItem.height()/2}});
            },

            /**
             * set parameters for quick panel carousel
             * @method closePanel
             * @return 
            */
            setActiveParams: function(elm, settings, index) {
                var active = 'active',
                    nextNav = elm.find(settings.nextButton),
                    prevNav = elm.find(settings.prevButton);

                elm.find(settings.quickPanelWrapper).removeClass(active);
                elm.find(settings.socialGalleryItem).removeClass(active).css('margin-bottom', settings.itemMargin);
                
                if (index === 0) {
                    prevNav.fadeOut(settings.animateSpeed);
                    nextNav.fadeIn(settings.animateSpeed);
                } else if (index === elm.find(settings.socialGalleryItem).length - 1) {
                    prevNav.fadeIn(settings.animateSpeed);
                    nextNav.fadeOut(settings.animateSpeed);
                } else {
                    prevNav.fadeIn(settings.animateSpeed);
                    nextNav.fadeIn(settings.animateSpeed);
                }

                this.resetVideoThumbnail(settings);
            },

            /**
             * reset video player on show.hide quick panel
             * @method resetVideoThumbnail
             * @return 
            */
            resetVideoThumbnail: function(settings) {
                if (settings.videoThumbnail) {
                    $(settings.videoThumbnailLink).fadeIn(200);
                    
                    if ($('iframe'+settings.videoThumbnail).length) {
                        var videoIframe = $('iframe'+settings.videoThumbnail),
                        videoWrapper = '<div id="'+videoIframe.attr('id')+'" class="'+videoIframe.attr('class')+'" data-vid="'+videoIframe.data('vid')+'">'+ '</div>';
                        videoIframe.before(videoWrapper);
                        videoIframe.remove();
                    }
                    
                }
            },

            /**
             * scroll to next panel
             * @method nextItem
             * @return 
            */
            nextItem: function(settings, index) {
                var currentIndex = index + 1;

                $(settings.socialGalleryItem+'[data-index='+currentIndex+'] '+settings.thumbnail).click();
            },

            /**
             * scroll to prev panel
             * @method prevItem
             * @return 
            */
            prevItem: function(settings, index) {
                var currentIndex = index - 1;
                
                $(settings.socialGalleryItem+'[data-index='+currentIndex+'] '+settings.thumbnail).click();
            },

            /**
             * reset quick panel parameters on window resize
             * @method resetPanelOnResize
             * @return 
            */
            resetPanelOnResize: function(elm, settings) {
                // Can't use app.on('window:resized') because it doesn't get triggered immediately
                var self = this,
                    commonService = new IEA.commonService(),
                    windowResized,
                    screenWidth = $(window).width(),
                    $currentQuickPanel = elm.find(settings.quickPanelWrapper+'.active'),
                    currentIndex = $currentQuickPanel.data('index');

                windowResized = commonService.debounce(function() {
                    if(screenWidth !== $(window).width()) {
                        self.setActiveParams(elm, settings, currentIndex);

                        screenWidth = $(window).width();
                    }

                }, 1000, true);

                window.addEventListener('resize', windowResized);
            },

            /**
             * animate resuls with fadeIn / fadeOut effect
             * @method animateResults
             * @return 
            */
            animateResults: function(item, preloader, speed, display) {
                if (display === 'show') {
                    TweenMax.to($(item), 0.8, {autoAlpha: 1, top: 0, delay: 0.2});
                    $(preloader).fadeOut(speed);
                } else {
                    TweenMax.to($(item), 0.8, {autoAlpha: 0, top: '30px', delay: 0.2});
                    $(preloader).fadeIn(speed);
                }
            }
        });

        return SocialTabs;

    });

});


