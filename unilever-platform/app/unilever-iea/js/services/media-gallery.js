/*global define*/

define(['TweenMax', 'commonService'], function(TweenMax) {
    'use strict';

    IEA.service('MediaGallery', function(service, app, iea) {

        /**
         * Description
         * @method
         * @return ThisExpression
         */
        var MediaGallery = function() {
            return this;
        };


        // extend defenition prototype
        _.extend(MediaGallery.prototype, {
            
            initialize: function() {
                return this;
            },

            closePanel: function(elm, settings, obj) {
                var _this = obj,
                    $currentQuickPanel = _this.parents(settings.mediaGalleryV2List).find(settings.quickPanelWrapper),
                    active = 'active',
                    currentIndex = $currentQuickPanel.data('index'),
                    $currentItem = $(settings.mediaGalleryV2Item+'[data-index='+currentIndex+']');

                this.setActiveParams(elm, settings, currentIndex);

                $currentQuickPanel.find('.c-media-gallery-v2__panel-imgwrap').show();
                $currentQuickPanel.find('.c-media-gallery-v2__panel-videowrap').hide();

                TweenMax.to(window, 0.5, {scrollTo: {y: $currentItem.offset().top - $currentItem.height()/2}});
            },

            setActiveParams: function(elm, settings, index) {
                var nextNav = elm.find(settings.nextButton),
                    prevNav = elm.find(settings.prevButton);

                elm.find(settings.quickPanelWrapper).removeClass('active');
                elm.find(settings.mediaGalleryV2Item).removeClass('active').css('margin-bottom', settings.itemMargin);
                
                if (index === 0) {
                    prevNav.fadeOut(settings.animateSpeed);
                    nextNav.fadeIn(settings.animateSpeed);
                } else if (index === elm.find(settings.mediaGalleryV2Item).length - 1) {
                    prevNav.fadeIn(settings.animateSpeed);
                    nextNav.fadeOut(settings.animateSpeed);
                } else {
                    prevNav.fadeIn(settings.animateSpeed);
                    nextNav.fadeIn(settings.animateSpeed);
                }
            }
        });

        return MediaGallery;

    });

});


