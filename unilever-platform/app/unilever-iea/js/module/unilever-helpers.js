/* global define */

define([
    'handlebars'
], function (Handlebars) {
    'use strict';
    var UnileverHelpers = IEA.module('UnileverHelpers', function (unileverHelpers, app, iea) {

        var helpers = {},
            self = this;

        // add handlebars helpers here
        _.extend(helpers, {

            EvenOdd: function (index) {
                return (index % 2 === 0 ? 'even' : 'odd');
            },

            /**
             * Adds a new Button instance according to type
             * View styleguide for usage examples
             * @param  {[type]} string [description]
             * @return {[type]}        [description]
             */

            svg: function(options) {
                var data = options.hash,
                    iconName = data.icon,
                    cssClass = data.class,
                    appName = data.brand,
                    hostname = window.location.hostname,
                    path = (hostname === 'localhost' || hostname.indexOf('pro') !== -1) ? '': '/etc/ui/' +appName+ '/clientlibs/core/core',
                    svgMarkupString = '<svg class="c-svg ' +cssClass+ '" xmlns:xlink="http://www.w3.org/1999/xlink">'+
                    '<use xlink:href="'+path+ '/svgs/symbol/svg/sprite.symbol.svg#'+iconName+'">'+
                    '</use>'+
                    '</svg>';

                return new Handlebars.SafeString(svgMarkupString);
            },

            getDataMap: function (type, options) {
                var data = options.hash,
                    path = 'partials/' + type;
                return {path: path, data: data};
            },

            renderTemplate: function (path, data, subData) {
                var template, args = [];
                if(subData && !subData.hash) {
                    args[0] = subData;
                    data.args = args;
                }
                template = app.getTemplate('include', path)(data);
                return new Handlebars.SafeString(template);
            },

            toLowerCase: function (str) {
                return str.toLowerCase();
            },

            getPattern: function (itemLenght, variant, idx) {
                var pattern = null,
                    count = 0;
                pattern = {
                    'A': [1, 2, 2, 2, 3, 2, 2, 2],
                    'B': [1, 2, 2, 2, 2, 4, 2, 2, 2, 3, 2, 2, 2],
                    'C': [2, 2, 2, 3, 2, 2, 2, 1],
                    'key': ['a', 'b', 'c', 'd']
                };
                for (var i = 0; i < itemLenght; i++) {

                    if (i === idx) {
                        return pattern.key[parseInt(pattern[variant][count]) - 1];
                    }
                    count++;
                    if (count === pattern[variant].length) {
                        count = 0;
                    }
                }
            },

            customEach: function(context, options, max) {
                var fn, maxVal, ret = '', inverse;
                
                if (max !== '' && typeof(max) !== 'undefined') {
                    fn = max.fn, inverse = max.inverse;
                    maxVal = options;
                } else {
                    fn = options.fn, inverse = options.inverse;
                }
                
                if(context && context.length > 0) {
                    if (max !== '' && typeof(max) !== 'undefined') {
                        for(var i = 0; i < maxVal && i < context.length; ++i) {
                            var lastIndex = i+1;
                            ret = ret + fn(_.extend({}, context[i], { i: i, index: i, first:0, last:lastIndex}));
                        }
                    } else {
                        for (var j=0; j<context.length; j++) {
                            var lastIdx = j+1;
                            ret = ret + fn(_.extend({}, context[j], { j: j, index: j, first:0, last:lastIdx}));
                        }
                    }
                } else {
                    ret = inverse(this);
                }
                return ret;
            },

            dateComparison: function(launchdate, options) {
                var oneDay = 24*60*60*1000, // hours*minutes*seconds*milliseconds
                currentDate = new Date(),
                launchDate = new Date(launchdate),
                newLaunch = $('.c-search-listing__wrap').data('launch'),
                diffDays = Math.round(Math.abs((currentDate.getTime() - launchDate.getTime())/(oneDay)));
             
                if (diffDays < newLaunch) {
                    return options.fn(this);
                }
                return options.inverse(this);
            },

            isPageNonSecure: function(){
                return window.location.protocol.indexOf('https') === -1;
            },

            pageReferrer: function(){
                return encodeURIComponent(window.location.href);
            },

            math: function(value1, operator, value2, options, isInt) {
                value1 = parseFloat(value1);
                value2 = parseFloat(value2);
                var returnVal =  {
                    '+': value1 + value2,
                    '-': value1 - value2,
                    '*': value1 * value2,
                    '/': value1 / value2,
                    '%': value1 % value2
                }[operator];

                if (isInt) {
                    return Math.ceil(returnVal);
                } else {
                    return returnVal.toFixed(2);
                }
            },

            /**
             * Description
             * @method adaptive
             * @param {} url
             * @param {} ext
             * @param {} alt
             * @param {} title
             * @param {} filename
             * @param {} imageURL, isNotAdaptive, dimensions, isLazyLoad, imageID
             * @return NewExpression
            */
            adaptiveRetina: function(url, ext, alt, title, filename, imageURL, isNotAdaptive, dimensions, isLazyLoad, imageID) {
            
               // var sharedConfig = window.IEASharedConfig || '',
                var parsedURL = [],
                    imageExtension = (typeof ext !== 'undefined' && ext !== '' && typeof ext !== 'object') ? ext : app.getValue('extension'),
                    imageMarkupString,
                    highImg ='2x',
                    sizes = dimensions.split(','),
                    dataPrefix = '',
                    pictureCl = '',
                    w = window;

                if (url !== undefined && url !== ''){
                    url = url.split(' ').join('%20');
                }
                
                if (imageURL !== undefined && imageURL !== '') {
                    imageURL = imageURL.split(' ').join('%20');
                }

                // Check if imageID is present, for backward compatibility
                if (typeof imageID === 'string') {
                    var componentData = w.adaptiveRetina[imageID.split('.')[0]];
                    if(componentData) {
                        var imageData = componentData[imageID.split('.')[1]];

                        // Override image dimensions, if specified in `config/breakpoints.js`
                        if (imageData && imageData.dimensions) {
                            sizes = imageData.dimensions.split(',');
                        }

                        // Override lazyload, if specified in `config/breakpoints.js`
                        if (imageData && typeof imageData.isLazyLoad === 'boolean'){
                            isLazyLoad = imageData.isLazyLoad;
                        }
                    }
                }

                if (sizes.length > 0) {

                    // Generate non-retina and retina image sizes strings
                    // The sizes array contains non-retina dimensions for desktop, tablet landscape, table portrait and mobile
                    // The retina strings (2x) are generated here in the helper
                    var desktop = '.' + sizes[0] + 'x' + sizes[1] + '.',
                        tabL = '.' + sizes[2] + 'x' + sizes[3] + '.',
                        tabP = '.' + sizes[4] + 'x' + sizes[5] + '.',
                        mob = '.' + sizes[6] + 'x' + sizes[7] + '.',
                        tabL2x = '.' + (sizes[0]) + 'x' + (sizes[1]) + '.',
                        tabP2x = '.' + (sizes[4]*2) + 'x' + (sizes[5]*2) + '.',
                        mob2x = '.' + (sizes[6]*2) + 'x' + (sizes[7]*2) + '.';
                        
                    var mobMin = w.BREAKPOINTS.mobMin,
                        tabPMin = w.BREAKPOINTS.tabPMin,
                        tabLMin = w.BREAKPOINTS.tabLMin,
                        desktopMin = w.BREAKPOINTS.desktopMin,
                        mobMax = w.BREAKPOINTS.mobMax,
                        tabPMax = w.BREAKPOINTS.tabPMax,
                        tabLMax = w.BREAKPOINTS.tabLMax;
                    
                    if (typeof isNotAdaptive !== 'undefined' && isNotAdaptive === 'true') {

                        // Scenario where adaptive behavior not required
                        imageMarkupString = '<img class="non-adaptive ' + title + '" src="' + imageURL + '" title="' + alt + '" alt="' + alt + '">';

                    } else {

                        parsedURL.push(url);
                        parsedURL.push('ulenscale');

                        parsedURL = parsedURL.join('.');

                        // If image is lazy loaded, add `data-` prefix and css classes
                        if (typeof isLazyLoad !== 'undefined' && isLazyLoad === 'true') {
                            dataPrefix = 'data-';
                            pictureCl = ' class="lazy-load is-loading is-lazyload"';
                        }

                        // If `title` is a string, and populated, assign `imageCl` with class attribute for <img>
                        var imageCl = (typeof title !== 'undefined' && title !== '' && typeof title !== 'object') ? 'class="' + title + '" ' : '';

                        var imageMarkup = '<img ' + imageCl + dataPrefix + 'src="' + parsedURL + mob + imageExtension + '" title="' + alt + '" alt="' + alt + '">';
                        
                        imageMarkupString = '<picture' + pictureCl + '>' +
                            '<!--[if IE 9]><video style="display: none;"><![endif]-->' +
                            '<source '+ dataPrefix + 'srcset="' + parsedURL + desktop + imageExtension + '" media="screen and (min-width: '+desktopMin+'px)">' +
                            '<source ' + dataPrefix + 'srcset="' + parsedURL + tabL + imageExtension + ', ' +
                            parsedURL + tabL2x + imageExtension + ' ' + highImg + '" media="screen and (min-width: '+tabLMin+'px) and (max-width: '+ tabLMax +'px)">' +
                            '<source ' + dataPrefix +'srcset="' + parsedURL + tabP + imageExtension + ', ' +
                            parsedURL + tabP2x + imageExtension + ' ' + highImg + '" media="screen and (min-width: '+tabPMin+'px) and (max-width: '+ tabPMax +'px)">' +
                            '<source ' + dataPrefix +'srcset="' + parsedURL + mob + imageExtension + ', ' +
                            parsedURL + mob2x + imageExtension + ' ' + highImg + '" media="screen and (min-width: '+mobMin+'px) and (max-width: '+ mobMax +'px)">' +
                            '<!--[if IE 9]></video><![endif]-->' + imageMarkup + '</picture>';
                    }
                }

                return new Handlebars.SafeString(imageMarkupString);
            },
            
            /**
             * Description
             * @method adaptive RMS
             * @param {} alt
             * @param {} title
             * @param {} versionPath
             * @return NewExpression
            */
            adaptiveRetinaRMS: function(alt, title, versions, dimensions, isLazyLoad, imageID) {
                var parsedURL = [],
                    imageMarkup = '',
                    imageMarkupString = '',
                    highImg ='2x',
                    sizes = dimensions.split(','),
                    imageSrc = '',
                    sourceSrc = '',
                    pictureCl = '',
                    w = window;
                   // sharedConfig = w.IEASharedConfig || '';

                // Overwrite dimension if specified in config/BREAKPOINTS.js
                // If we are passing an imageID via helper, check for overwrites
                if (typeof imageID === 'string') {

                    var imageData = w.adaptiveRetina[imageID.split('.')[0]][imageID.split('.')[1]],
                        componentData = w.adaptiveRetina[imageID.split('.')[0]];
                    
                    // If image sizes are declared in config/breakpoints.js,
                    // Override image dimensions, if specified in `config/breakpoints.js`
                    if (imageData && imageData.dimensions) {
                        sizes = imageData.dimensions.split(',');
                    }

                    // Override lazyload, if specified in `config/breakpoints.js`
                    if (imageData && typeof imageData.isLazyLoad === 'boolean'){
                        isLazyLoad = imageData.isLazyLoad;
                    }

                }

                if (sizes.length > 0) {

                    // Generate non-retina and retina image sizes strings
                    // The sizes array contains non-retina dimensions for desktop, tablet landscape, table portrait and mobile
                    // The retina strings (2x) are generated here in the helper
                    var mobMin = w.BREAKPOINTS.mobMin,
                        tabPMin = w.BREAKPOINTS.tabPMin,
                        tabLMin = w.BREAKPOINTS.tabLMin,
                        desktopMin = w.BREAKPOINTS.desktopMin,
                        mobMax = w.BREAKPOINTS.mobMax,
                        tabPMax = w.BREAKPOINTS.tabPMax,
                        tabLMax = w.BREAKPOINTS.tabLMax;

                    var getImageUrl = function(width, versions) {
                        var url = '';

                        width = width.toString();

                        if(versions.original[0].width === width) {
                            url = versions.original[0].url;

                        } else {
                            versions.default.forEach(function(item) {
                                if(item.width === width) {
                                    url = item.url;
                                }
                            });
                        }

                        return url;
                    };

                    var getNextImage = function(widthS, versions) {
                        var widthArr = [], indexOfWidthS, url;

                        widthS = parseInt(widthS, 10);
                        widthArr.push(widthS);
                        widthArr.push(parseInt(versions.original[0].width, 10));

                        versions.default.forEach(function(item, index) {
                            var width;

                            if(typeof item.width === 'string') {
                                width = parseInt(item.width, 10);
                            }else {
                                width = item.width;
                            }

                            widthArr.push(width);
                        });

                        //sort the widthArr in ascending order
                        widthArr.sort(function(a, b){
                            return a - b;
                        });

                        //get the indexOf widthS
                        indexOfWidthS = widthArr.indexOf(widthS);

                        //check if next item is present
                        if(indexOfWidthS > -1 && indexOfWidthS < (widthArr.length - 1)) {
                            //get the url of next index
                            url = getImageUrl(widthArr[indexOfWidthS + 1], versions);
                        }else {
                            //get the url of previous index
                            url = getImageUrl(widthArr[indexOfWidthS - 1], versions);
                        }

                        return url;

                    };
                    
                    var getImage = function (widthS, heightS, versions){
                        var toReturn, isFound = false;
                        
                        $(versions.default).each(function(){
                            if(widthS === this.width && heightS === this.height){
                                toReturn = this.url;
                                isFound = true;
                                return false;
                            }
                        });

                        if(!isFound) {
                            // toReturn = versions.original[0].url;
                            toReturn = getNextImage(widthS, versions);
                        }

                        return toReturn;
                    };
                    
                    var desktopImage = getImage( sizes[0], sizes[1], versions),
                        tabLImage = getImage( sizes[2], sizes[3], versions),
                        tabPImage = getImage( sizes[4], sizes[5], versions),
                        mobImage = getImage( sizes[6], sizes[7], versions),
                        tabL2xImage = getImage( sizes[0], sizes[1], versions),
                        tabP2xImage = getImage((sizes[4]*2).toString(), (sizes[5]*2).toString(), versions),
                        mob2xImage = getImage((sizes[6]*2).toString(), (sizes[7]*2).toString(), versions);
                    
                    if (typeof isLazyLoad !== 'undefined' && isLazyLoad === 'false') {
                        imageSrc = 'src';
                        sourceSrc = 'srcset';
                    } else {
                        imageSrc = 'data-src';
                        sourceSrc = 'data-srcset';
                        pictureCl = 'lazy-load is-loading is-lazyload';
                    }

                    if (typeof title !== 'undefined' && title !== '' && typeof title !== 'object') {
                        imageMarkup = '<img class="' + title + '" '+imageSrc+'="' + mobImage + '" title="' + title + '" alt="' + alt + '">';
                    } else {
                        imageMarkup = '<img '+imageSrc+'="' + mobImage + '" alt="' + alt + '">';
                    }

                    imageMarkupString = '<picture class="'+pictureCl+'">' +
                        '<!--[if IE 9]><video style="display: none;"><![endif]-->' +
                       '<source '+sourceSrc+'="' + desktopImage + '" media="screen and (min-width: '+desktopMin+'px)">' +

                        '<source '+sourceSrc+'="' + tabLImage + ', ' +
                        tabL2xImage + ' ' + highImg + '" media="screen and (min-width: '+tabLMin+'px) and (max-width: '+tabLMax+'px)">' +

                        '<source '+sourceSrc+'="' + tabPImage + ', ' +
                        tabP2xImage + ' ' + highImg + '" media="screen and (min-width: '+tabPMin+'px) and (max-width: '+tabPMax+'px)">' +
                        '<source '+sourceSrc+'="' + mobImage + ', ' +
                        mob2xImage + ' ' + highImg + '" media="screen and (min-width: '+mobMin+'px) and (max-width: '+mobMax+'px)">' +

                        '<!--[if IE 9]></video><![endif]-->' + imageMarkup + '</picture>';
                    
                }

                return new Handlebars.SafeString(imageMarkupString);
            }
        });
        // register all helpers from the helper object into handlebars
        /**
         * Description
         * @method registerHelpers
         * @param {} options
         * @return
         */
        this.registerHelpers = function (options) {
            options = options || {};
            for (var helper in helpers) {
                if (helpers.hasOwnProperty(helper)) {
                    Handlebars.registerHelper(helper, helpers[helper]);
                }
            }
            this.triggerMethod('helper:registered');
            window.testHandlebars = Handlebars;
        };

        // add self init code to fire register helper when module loaded.
        this.addInitializer(this.registerHelpers);

    });

    return UnileverHelpers;

});
