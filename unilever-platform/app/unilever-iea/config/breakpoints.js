// This config is shared amongst front-end (IEA) and back-end (AEM)

// Image dimensions overwrites for the adaptive retina helper
var BREAKPOINTS = {
   desktopMin : 1200,
   tabLMin : 992,
   tabLMax : 1199,
   tabPMin : 768,
   tabPMax : 991,
   mobMin : 480,
   mobMax : 767
}, 

adaptiveRetina = {};