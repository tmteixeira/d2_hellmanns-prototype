(function (window, document, $) {
	if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
		$(document).off('click', '[data-ct-action]').on('click', '[data-ct-action]', function(){
			var clickType = $(this).attr('data-ct-action'), title,
			compName = $(this).parents('[data-componentName]').attr('data-componentName');
			if ($(this).attr('data-title')) {
				title = $(this).data('title');
			} else {
				title = $(this).attr('title');
			}
			
			digitalData.component = [];
			digitalData.component.push({'componentInfo' :{
			   'componentID':compName,
			   'name':compName
			}});

			switch (clickType) {			
				case 'productClick':
					var _this = $(this),
						productID = _this.data('product-id'),
						productName = _this.data('product-name'),
						productCategory = _this.data('product-category'),     
						listPosition = _this.parents('.c-product-listing__product-wrap').index(),
						productPrice = _this.data('product-price'),
						quantity = _this.data('product-quantity'),
						brand = _this.data('product-brand'),
						variant = _this.data('product-variant');
					if (listPosition < 0) {
						listPosition = 1;
					} else {
						listPosition = listPosition/2 + 1;
					}
					var ev = {};
					digitalData.product=[];
					digitalData.product.push({
					   'productInfo' :{
					   'productID':productID,
					   'productName':productName,
					   'price':productPrice,
					   'brand':brand,
					   'quantity': quantity,
					   'componentName':compName
					   },
					   'category':{
					   'primaryCategory': productCategory
					   },
					   'attributes':
					   {
					   'productVariants':variant,
					   'listPosition':listPosition
					   }			   
					});

					ev.eventInfo = {
					  'type':ctConstants.productclick,
					};
					if (_this.attr('data-info') === 'true') {
						ev.category ={'primaryCategory':ctConstants.other};
					} else {
						ev.category ={'primaryCategory':ctConstants.custom};
					}
					
					ev.attributes={'nonInteraction':1};
					digitalData.event.push(ev);

					break;

				case 'articleClick':
					var ev = {},
					label = $(this).attr('href');
					ev.eventInfo = {
					  'type':ctConstants.trackEvent,
					  'eventAction': ctConstants.articleClick,
					  'eventLabel' : label,				  
					  'componentName':compName
					};
					ev.category = {'primaryCategory':ctConstants.other};
					digitalData.event.push(ev);
					break; 
				case 'relatedArticle':
					var ev = {},
					label = $(this).attr('href');
					ev.eventInfo={
						'type':ctConstants.trackEvent,
						'eventAction': ctConstants.relatedArticle,
						'eventLabel' : label,			
						'componentName':compName
					};
					ev.category = {'primaryCategory':ctConstants.custom};
					digitalData.event.push(ev);
					break;
				case 'tags':
					var ev = {},
					label = $(this).data('ct-label');
					ev.eventInfo = {
						'type':ctConstants.trackEvent,
						'eventAction': ctConstants.tags,
						'eventLabel' : label,					
						'componentName':compName
					};
					ev.category ={'primaryCategory':ctConstants.custom};
					digitalData.event.push(ev);
					break;
				case 'loadMore':
					var ev = {};
					ev.eventInfo = {
					  'type':ctConstants.trackEvent,
					  'eventAction': ctConstants.loadMore,
					  'eventLabel' : title,                  
					  'componentName':compName
					};
					ev.attributes={'nonInteraction':1};
					ev.category ={'primaryCategory':ctConstants.custom};
					digitalData.event.push(ev);
					break;
				default: 
			}		
		});	

		$(document).on('change', "#js-product-overview__select", function () {
			var filterItemSelected= $(this).find("option:selected").text(),
				compName = $(this).parents('[data-componentName]').attr('data-componentName'),
				ev = {};
			digitalData.component = [];	
			digitalData.component.push({'componentInfo' :{
			   'componentID':compName,
			   'name':compName
			}});

			ev.eventInfo ={
				'type':ctConstants.trackEvent,
				'eventAction': ctConstants.dropDown,
				'eventLabel' : filterItemSelected,
				'componentName':compName
			};
			ev.category ={'primaryCategory':ctConstants.custom};
			digitalData.event.push(ev);
		});


		$(document).on('change', ".c-product-listing__filter-input, .c-search-filters__select", function () {
			var filterItemSelected= $(this).find("option:selected").text(),
			compName = $(this).parents('[data-componentName]').attr('data-componentName');
			var ev = {};
			
			digitalData.component = [];
			digitalData.component.push({'componentInfo' :{
			   'componentID':compName,
			   'name':compName
			}});

			ev.eventInfo={
			  'type':ctConstants.trackEvent,
			  'eventAction': ctConstants.filter,
			  'eventLabel' : filterItemSelected
			};
			ev.attributes={'nonInteraction':1};
			ev.category ={'primaryCategory':ctConstants.custom};
			digitalData.event.push(ev);

		});
		
		$(document).on('click', '.js-thumb-carousel__nav .slick-arrow', function () {
			var direction=$(this).attr('aria-label'),
			compName = $(this).parents('[data-componentName]').attr('data-componentName');
			
			digitalData.component = [];
			digitalData.component.push({'componentInfo' :{
			   'componentID':compName,
			   'name':compName
			}});

			digitalData.component.push({'componentInfo' :{
			   'componentID':compName,
			   'name':compName
			}});
			
			digitalData.direction = direction;

			 var ev = {};
			 ev.eventInfo={
			  'type':ctConstants.trackEvent,
			  'eventAction': ctConstants.widgets,
			  'eventLabel' : 'Scroll'
			};
			ev.category ={'primaryCategory':ctConstants.custom};
			digitalData.event.push(ev);
		});
		
		//read more
		$(document).on('click', '.c-hero-button-readmore', function(){ 
			var openCloseStatus='',
			compName = $(this).parents('[data-componentName]').attr('data-componentName');
			
			digitalData.component = [];
			digitalData.component.push({'componentInfo' :{
			   'componentID':compName,
			   'name':compName
			}});

			if(!$('.js-show-more').is(':visible'))
			{
				openCloseStatus='open';
				var ev = {};
				ev.eventInfo={
					'type':ctConstants.trackEvent,
					'eventAction': ctConstants.readMore,
					'eventLabel' : openCloseStatus
				};
			}
			else{
				openCloseStatus='close';
			}		
			ev.attributes={'nonInteraction':1};
			ev.category ={'primaryCategory':ctConstants.custom};
			digitalData.event.push(ev);
		});

		 // analytics code for determining the percentage of page read by user
		var scroll24 = 0; var scroll49 = 0; var scroll74 = 0; var scroll99 = 0; var scroll100 = 0;
		$(window).scroll(function () {
				var h = $(document).height() - $(window).height();
				var h25 = Math.floor(h * 0.25);  // 25% 
				var h50 = Math.floor(h * 0.50);  // 50%
				var h75 = Math.floor(h * 0.75);  // 75%
				if ($(window).scrollTop() > 0 && $(window).scrollTop() <h25 && scroll24 == 0) {
						scroll24 = 1;
						var ev = {};
						ev.eventInfo={
						  'type':ctConstants.trackEvent,
						  'eventAction': ctConstants.pageScroll,
						  'eventLabel' : 'Scroll Depth - 0%'
						};
						ev.attributes={'nonInteractive':{'nonInteraction':1}};
						ev.category ={'primaryCategory':ctConstants.custom};
						digitalData.event.push(ev);
				}else if($(window).scrollTop() >= h25 && $(window).scrollTop() < h50 && scroll49 == 0) {
						scroll49 = 1;
						 var ev = {};
						ev.eventInfo={
						  'type':ctConstants.trackEvent,
						  'eventAction': ctConstants.pageScroll,
						  'eventLabel' : 'Scroll Depth - 25%'
						};
						ev.attributes={'nonInteractive':{'nonInteraction':1}};
						ev.category ={'primaryCategory':ctConstants.custom};
						digitalData.event.push(ev);
				}else if($(window).scrollTop() >= h50 && $(window).scrollTop() < h75 && scroll74 == 0) {
						scroll74 = 1;
						var ev = {};
						ev.eventInfo={
						  'type':ctConstants.trackEvent,
						  'eventAction': ctConstants.pageScroll,
						  'eventLabel' : 'Scroll Depth - 50%'
						};
						ev.attributes={'nonInteractive':{'nonInteraction':1}};
						ev.category ={'primaryCategory':ctConstants.custom};
						digitalData.event.push(ev);
				}else if($(window).scrollTop() >= h75 && $(window).scrollTop() < h && scroll99 == 0) {
						scroll99 = 1; 
						var ev = {};
						ev.eventInfo={
						  'type':ctConstants.trackEvent,
						  'eventAction': ctConstants.pageScroll,
						  'eventLabel' : 'Scroll Depth - 75%'
						};
						ev.attributes={'nonInteractive':{'nonInteraction':1}};
						ev.category ={'primaryCategory':ctConstants.custom};
						digitalData.event.push(ev);
				}else if($(window).scrollTop() == h && scroll100 == 0){
						scroll100 = 1;
						var ev = {};
						ev.eventInfo={
						  'type':ctConstants.trackEvent,
						  'eventAction': ctConstants.pageScroll,
						  'eventLabel' : 'Scroll Depth - 100%'
						};
						ev.attributes={'nonInteractive':{'nonInteraction':1}};
						ev.category ={'primaryCategory':ctConstants.custom};
						digitalData.event.push(ev);
				}else{

				}
		});	

		if(typeof ratingReview != 'undefined') {
			ratingReview.events = {
				rrSubmitReview: function (data) {
					var ev = {},
					productName = $('.rr-product-info .rr-heading-3').text();
					ev.eventInfo = {
					  'type':ctConstants.trackEvent,
					  'eventAction': ctConstants.ratingreview,
					  'eventLabel' : productName
					};
					ev.category ={'primaryCategory':ctConstants.other};
					digitalData.event.push(ev);
				}
			};
			
			//read more
			$(document).on('click', '.write-review-btn', function() { 
				var ev = {};
				var productName = $('.c-product-overview__title').text();
				ev.eventInfo={
				  'type':ctConstants.trackEvent,
				  'eventAction': ctConstants.ratingreview,
				  'eventLabel' : productName
				};
				ev.category ={'primaryCategory':ctConstants.other};
				digitalData.event.push(ev);
			});
		};
		
		/*AddThis Share Widget Services Track*/
		var trackShareThis = function (evt) {
			if (evt.type == 'addthis.menu.share') {
				var ev = {};
				ev.eventInfo = {
					'type':ctConstants.trackEvent,
					'eventAction': ctConstants.share,
					'eventLabel' : evt.data.service
				};
				ev.category ={'primaryCategory':ctConstants.referral};
				digitalData.event.push(ev);
			}
		};

		if ($('.o-social-sharing').length > 0 && (typeof addthis) !== undefined) {
			addthis.addEventListener("addthis.menu.share", trackShareThis);
		}
	}	
})(window, document, jQuery);