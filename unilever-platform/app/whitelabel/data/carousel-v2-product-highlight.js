{
   "data":{
      "path":"/content/whitelabel/en_gb/home/wl_orderedlist_harsh/_jcr_content/flexi_hero_par/carouselv2",
      "componentType":"carouselV2",
      "ajaxURL":"/content/whitelabel/en_gb/home/wl_orderedlist_harsh/_jcr_content/flexi_hero_par/carouselv2.data.ajax.json",
      "clientSideRendering":false,
      "viewType":"defaultView",
      "randomNumber": "808",
      "isContainer":false,
      "carouselV2":{
        "slides":[
            {
               "heroV2":{
                  "generalConfig":{
                     "secondary":{
                        "ctaUrl":"http://localhost:4502/content/whitelabel/en_gb/kuch-karo.html",
                        "ctaLabel":"Secondary CTA",
                        "openInNewWindow":true
                     },
                     "headingText":"heading",
                     "longSubheadingText":"<p>Long Sub Heading</p>\r\n",
                     "shortSubheadingText":"Short Heading",
                     "heroType":"image",
                     "primary":{
                        "ctaUrl":"http://localhost:4502/content/whitelabel/en_gb/harsh_whitelabel.html",
                        "ctaLabel":"Primary CTA",
                        "openInNewWindow":false
                     }
                  },
                  "image":{
                     "url":"http://localhost:4502/content/dam/unilever/dove/uk/products/Dove_Women_Pure_Compressed_125ML_FO_96080870.png",
                     "fileName":"",
                     "isNotAdaptiveImage":false,
                     "extension":"jpg",
                     "altImage":"Dove_Women_Pure_Compressed",
                     "title":"",
                     "path":"/images-cms/carousel-v2/img-placeholder",
                     "carousalPanelName":"Carousel Panel Name"
                  },
                  "readMore":{
                     "isReadMoreEnable":"",
                     "readLess":"Show less",
                     "readMore":"Read more"
                  }
               },
               "componentParentType":"carousel",
               "resourceType":"/apps/unilever-iea/components/heroV2"
            },
            {
               "heroV2": {
                  "generalConfig":{
                     "secondary":{
                        "ctaUrl":"http://localhost:4502/content/whitelabel/en_gb/kuch-karo.html",
                        "ctaLabel":"Secondary CTA",
                        "openInNewWindow":true
                     },
                     "headingText":"A white label website",
                     "longSubheadingText":"<p>An introduction to our components and how you can use them...</p>\r\n",
                     "shortSubheadingText":"new and improved",
                     "heroType":"video",
                     "primary":{
                        "ctaUrl":"http://localhost:4502/content/whitelabel/en_gb/harsh_whitelabel.html",
                        "ctaLabel":"Primary CTA",
                        "openInNewWindow":false
                     }
                     },
                     "video":{
                        "carousalPanelName":"Carousel Panel",
                        "videoSource":"youtube",
                        "videoId":"DU6IdS2gVog",
                        "videoControls":{
                           "muteVideoAudio":false,
                           "displayInOverlay": false,
                           "hideVideoControls":false,
                           "relatedVideoAtEnd":false,
                           "autoplay": true,
                           "loopVideoPlayback":false
                        },
                        "previewImage":{
                           "url":"http://localhost:4502/content/dam/unilever/dove/uk/products/Dove_Women_Pure_Compressed_125ML_FO_96080870.png",
                           "fileName":"",
                           "isNotAdaptiveImage":false,
                           "extension":"jpg",
                           "altImage":"Dove_Women_Pure_Compressed",
                           "title":"",
                           "path":"/images-cms/carousel-v2/img-placeholder",
                           "carousalPanelName":"Carousel Panel Name"
                        }
                  },
                  "readMore":{
                      "isReadMoreEnable": false,
                      "readLess":"Show less",
                      "readMore":"Read more"
                  }
               }
            },
            {
               "heroV2":{
                  "generalConfig":{
                     "secondary":{
                        "ctaUrl":"http://localhost:4502/content/whitelabel/en_gb/kuch-karo.html",
                        "ctaLabel":"Secondary CTA",
                        "openInNewWindow":true
                     },
                     "headingText":"heading",
                     "longSubheadingText":"<p>Long Sub Heading</p>\r\n",
                     "shortSubheadingText":"Short Heading",
                     "heroType":"image",
                     "primary":{
                        "ctaUrl":"http://localhost:4502/content/whitelabel/en_gb/harsh_whitelabel.html",
                        "ctaLabel":"Primary CTA",
                        "openInNewWindow":false
                     }
                  },
                  "readMore":{
                     "isReadMoreEnable":"",
                     "readLess":"Show less",
                     "readMore":"Read more"
                  }
               },
               "componentParentType":"carousel",
               "resourceType":"/apps/unilever-iea/components/heroV2"
            },
            {
               "heroV2":{
                  "generalConfig":{
                     "secondary":{
                        "ctaUrl":"http://localhost:4502/content/whitelabel/en_gb/kuch-karo.html",
                        "ctaLabel":"Secondary CTA",
                        "openInNewWindow":true
                     },
                     "headingText":"heading",
                     "longSubheadingText":"<p>Long Sub Heading</p>\r\n",
                     "shortSubheadingText":"Short Heading",
                     "heroType":"image",
                     "primary":{
                        "ctaUrl":"http://localhost:4502/content/whitelabel/en_gb/harsh_whitelabel.html",
                        "ctaLabel":"Primary CTA",
                        "openInNewWindow":false
                     }
                  },
                  "image":{
                     "url":"http://localhost:4502/content/dam/unilever/dove/uk/products/Dove_Women_Pure_Compressed_125ML_FO_96080870.png",
                     "fileName":"",
                     "isNotAdaptiveImage":false,
                     "extension":"jpg",
                     "altImage":"Dove_Women_Pure_Compressed",
                     "title":"",
                     "path":"/images-cms/carousel-v2/img-placeholder",
                     "carousalPanelName":"Carousel Panel Name"
                  },
                  "readMore":{
                     "isReadMoreEnable":"",
                     "readLess":"Show less",
                     "readMore":"Read more"
                  }
               },
               "componentParentType":"carousel",
               "resourceType":"/apps/unilever-iea/components/heroV2"
            },
            {
               "heroV2":{
                  "generalConfig":{
                     "secondary":{
                        "ctaUrl":"http://localhost:4502/content/whitelabel/en_gb/kuch-karo.html",
                        "ctaLabel":"Secondary CTA",
                        "openInNewWindow":true
                     },
                     "headingText":"heading",
                     "longSubheadingText":"<p>Long Sub Heading</p>\r\n",
                     "shortSubheadingText":"Short Heading",
                     "heroType":"image",
                     "primary":{
                        "ctaUrl":"http://localhost:4502/content/whitelabel/en_gb/harsh_whitelabel.html",
                        "ctaLabel":"Primary CTA",
                        "openInNewWindow":false
                     }
                  },
                  "image":{
                     "url":"http://localhost:4502/content/dam/unilever/dove/uk/products/Dove_Women_Pure_Compressed_125ML_FO_96080870.png",
                     "fileName":"",
                     "isNotAdaptiveImage":false,
                     "extension":"jpg",
                     "altImage":"Dove_Women_Pure_Compressed",
                     "title":"",
                     "path":"/images-cms/carousel-v2/img-placeholder",
                     "carousalPanelName":"Carousel Panel Name"
                  },
                  "readMore":{
                     "isReadMoreEnable":"",
                     "readLess":"Show less",
                     "readMore":"Read more"
                  }
               },
               "componentParentType":"carousel",
               "resourceType":"/apps/unilever-iea/components/heroV2"
            },
            {
               "heroV2":{
                  "generalConfig":{
                     "secondary":{
                        "ctaUrl":"http://localhost:4502/content/whitelabel/en_gb/kuch-karo.html",
                        "ctaLabel":"Secondary CTA",
                        "openInNewWindow":true
                     },
                     "headingText":"heading",
                     "longSubheadingText":"<p>Long Sub Heading</p>\r\n",
                     "shortSubheadingText":"Short Heading",
                     "heroType":"image",
                     "primary":{
                        "ctaUrl":"http://localhost:4502/content/whitelabel/en_gb/harsh_whitelabel.html",
                        "ctaLabel":"Primary CTA",
                        "openInNewWindow":false
                     }
                  },
                  "image":{
                     "url":"http://localhost:4502/content/dam/unilever/dove/uk/products/Dove_Women_Pure_Compressed_125ML_FO_96080870.png",
                     "fileName":"",
                     "isNotAdaptiveImage":false,
                     "extension":"jpg",
                     "altImage":"Dove_Women_Pure_Compressed",
                     "title":"",
                     "path":"/images-cms/carousel-v2/img-placeholder",
                     "carousalPanelName":"Carousel Panel Name"
                  },
                  "readMore":{
                     "isReadMoreEnable":"",
                     "readLess":"Show less",
                     "readMore":"Read more"
                  }
               },
               "componentParentType":"carousel",
               "resourceType":"/apps/unilever-iea/components/heroV2"
            }
         ],
         "intervalsOfRotation":6,
         "brandName":"whitelabel",
         "market":"en_gb",
         "locale":"en-GB",
         "componentVariation":"defaultView",
         "componentPosition":6,
         "componentName":"carouselV2",
         "style":{
            "type":"type-b",
            "class":"class a"
         },
         "analytics":{
            "usageContext":"",
            "eventType":"Engagement"
         }
      },
      "resourceType":"unilever-iea/components/carouselV2"
   },
   "responseHeader":{
      "messages":[

      ],
      "status":"200"
   }
}