/*global define, enquire*/

define([
    'iea',
    'svg4everybody',
    'config'
], function(
    IEA,
    svg4everybody,
    Config) {

    'use strict';

    _.extend(Config, {
        'custom': 'value'
    });

    var app = new IEA.Application(Config);

    app.addInitializer(function() {
        //console.log('app intialize');
    });

    app.on('start', function (options) {

        svg4everybody({
            fallback: function (src, svg, use) {
                // src: current xlink:href String
                // svg: current SVG Element
                // use: current USE Element

                return '../svgs/sprites/img/symbol-sprite.png'; // ok, always return fallback.png
            }
        });
    });

    /* ----------------------------------------------------------------------------- *\
       Private Methods
    \* ----------------------------------------------------------------------------- */


    /**
     * intialize router
     * @method _initRouter
     * @return
     */
    var _initRouter = function() {
        console.log('router initialized');
        //Router.initialize();
    };


    return app;
});
