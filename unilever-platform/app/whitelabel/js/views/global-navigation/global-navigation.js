/*define(function() {
    'use strict';

    var Navigation = IEA.module('UI.global-navigation', function(globalNavigation, app, iea) {
        // Extend
        // _.extend(globalNavigation, {
        //     overlay: false,
        //     onInit: function() {
        //         this.addEvent({'mouseenter .o-navbar-tier1-item.submenu > .o-navbar-label': '_bindSubmenu'});
        //         this.addEvent({'click .js-navbar-overlay-close': '_bindClose'});
        //     },
        //     _bindClose: function() {
        //         var menus = $('.o-navbar-tier1-item.submenu > .o-navbar-label');
        //         var self = this;
        //         $.each(menus, function() {
        //             var parent = $(this).parent();
        //             if(parent.hasClass('opened')) {
        //                 self._toggleSubmenu($(this));
        //             }
        //         });
        //         this._toggleOverlay(false);
        //         this.overlay = false;
        //     },
        //     _bindSubmenu: function(e) {
        //         var target = $(e.currentTarget);
        //         if(!this.overlay) {
        //             this._toggleOverlay(true);
        //             this._toggleSubmenu(target);
        //             this.overlay = true;
        //         }
        //     }
        // });

        //With THIS
        this.overlay = false;
        this.onInit = function() {
            this.addEvent({'mouseenter .o-navbar-tier1-item.submenu > .o-navbar-label': '_bindSubmenu' });
            this.addEvent({'click .js-navbar-overlay-close': '_bindClose'})
        };
        this._bindClose = function() {
            var menus = $('.o-navbar-tier1-item.submenu > .o-navbar-label');
            var self = this;
            $.each(menus, function() {
                var parent = $(this).parent();
                if(parent.hasClass('opened')) {
                    self._toggleSubmenu($(this));
                }
            });
            this._toggleOverlay(false);
            this.overlay = false;
        };
        this._bindSubmenu = function(e) {
            var target = $(e.currentTarget);
            if(!this.overlay) {
                this._toggleOverlay(true);
                this._toggleSubmenu(target);
                this.overlay = true;
            }
        }
    });
    return Navigation;
});*/
