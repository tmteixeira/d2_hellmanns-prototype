(function (window, document, $) {
    if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined') {
        /*
            Common methods
            *******************
        */
        var trackComponentInfo = function(obj, authorName) {
            if (obj) {
                var $obj = obj,
                    $this = ($obj.data('componentname') !== undefined && $obj.data('componentname') !== '') ?
                            $obj : (($obj.parents('[data-componentname]').data('componentname') !== '') ?
                            $obj.parents('[data-componentname]') :
                            $obj.parents('[data-componentname]').parents('[data-componentname]')), // get the correct object which has the analytics data attributes
                    compName = $this.data('componentname'),
                    compVar = $this.data('component-variants'),
                    compPos = $this.data('component-positions');

                digitalData.component = [];
                digitalData.component.push({
                    'componentInfo' :{
                        'name': compName
                    },
                    'attributes': {
                        'position': compPos,
                        'variants': compVar,
                        'author': authorName ? authorName : ''
                    }
                });
            }
        },

        trackShareThis = function (evt) {
            if (evt.type == 'addthis.menu.share') {
                var ev = {},
                    obj = '.c-social-sharing-component',
                    $obj = $(obj),
                    compName = ($obj.parents('[data-componentname]').data('componentname') !== '') ?
                                $obj.parents('[data-componentname]').data('componentname') :
                                $obj.parents('[data-componentname]').parents('[data-componentname]').data('componentname'),

                    compVariants = ($obj.parents('[data-component-variants]').data('component-variants') !== '') ?
                                    $obj.parents('[data-component-variants]').data('component-variants') :
                                    $obj.parents('[data-component-variants]').parents('[data-component-variants]').data('component-variants'),

                    compPositions = ($obj.parents('[data-component-positions]').data('component-positions') !== '') ?
                                    $obj.parents('[data-component-positions]').data('component-positions') :
                                    $obj.parents('[data-component-positions]').parents('[data-component-positions]').data('component-positions');

                // track component details
                trackComponentInfo($obj);
                   digitalData.component = [];
                    digitalData.component.push({
                    'componentInfo' :{
                        'componentID': compName,
                        'name': compName
                    },
                    'attributes': {
                        'position': compPositions,
                        'variants': compVariants
                    }
                });

                ev.eventInfo = {
                    'type':ctConstants.trackEvent,
                    'eventAction': ctConstants.share,
                    'eventLabel' : compName + ' - ' + evt.data.service + ' - ' + document.title
                };
                ev.category ={'primaryCategory':ctConstants.referral};
                ev.attributes={'nonInteractive':{'nonInteraction': 0}};
                digitalData.event.push(ev);
            }
        },

        trackProductInfo = function (obj, productData, options) {
            var productVariant = ((productData.data('product-variant') !== undefined) ? productData.data('product-variant') : obj.data('product-variant')),
                productCategory = (productData.data('product-category') !== undefined) ?  productData.data('product-category') : obj.data('product-category'),
                brandName = (productData.data('product-brand') !== undefined) ? productData.data('product-brand') : obj.data('product-brand');

            digitalData.product.push({
               'productInfo' :{
                   'productID': (options && options.productID) ? options.productID : productData.data('product-id'),
                   'productName': (options && options.productName) ? options.productName : productData.data('product-name'),
                   'price': (options && options.price) ? options.price : ((productData.data('product-price')) ? productData.data('product-price') :''),
                   'brand': (brandName !== undefined) ? brandName : '',
                   'quantity': (options && options.quantity) ? options.quantity : ((productData.data('product-quantity')) ? productData.data('product-quantity') :'')
                },
                'attributes': {
                    'productVariants': (productVariant !== undefined) ? productVariant : '',
                    'listPosition': (options && options.listPosition) ? options.listPosition : 1
                },
                'category':{
                    'primaryCategory': (productCategory !== undefined) ? productCategory : ''
                }
            });
        },

        errorTracking = function(obj, productData) {
            var ev = {},
                errorlog = [],
                errorvalue;

            $('.has-error').each(function(i, value) {
               errorlog.push($(this).find('.help-block').text());
            });

            errorvalue = errorlog.join(" | ");

            ev.eventInfo = {
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.inputerror,
              'eventLabel' : errorvalue
            };
            ev.category ={'primaryCategory':ctConstants.conversion};
            digitalData.event.push(ev);
        },

        productImpressionTag = function(wrapper, item) {
            var $productListing = $(wrapper),
                ev = {},
                options = {};

            if ($productListing.length) {
                // track component details
                trackComponentInfo($productListing);

                digitalData.product = []; //empty product data for each events so that it picks up the correct product data

                $(wrapper+' '+item).each(function(i) {
                    var $this = $(this);

                    options.listPosition = i + 1;

                    // track component info
                    trackProductInfo($productListing, $this, options);
                });

                ev.eventInfo = {
                  'type':ctConstants.productImpression
                };

                ev.category = {'primaryCategory':ctConstants.custom};
                ev.attributes = {'nonInteractive':{'nonInteraction': 1}};
                digitalData.event.push(ev);
            }
        },

        trackSocialAttributes = function(obj, activePanel) {
            var authorName = activePanel.find('.o-author-name').text();
                contentType = activePanel.data('contenttype');
                hashTags = activePanel.data('hashtags');

            // track component info
            trackComponentInfo(obj, authorName);

            digitalData.page = [];
            digitalData.page.push({'category': {
                    'contentType': contentType,
                    'contentTopic': hashTags
                }
            });
        },

        trackSocialCarouselLeftRight = function(obj, selector) {
            var ev = {},
                $activePanel = $(selector+ ' .active'),
                contentName = $activePanel.find('.o-text__quote').text(),
                shortContentName = contentName.trim().substr(0, 20);

            shortContentName = shortContentName.substr(0, Math.min(shortContentName.length, shortContentName.lastIndexOf(' ')));

            //track social attributes
            trackSocialAttributes(obj, $activePanel);

            ev.eventInfo = {
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.leftrightquickview,
              'eventLabel' : shortContentName
            };

            ev.category ={'primaryCategory':ctConstants.custom};
            ev.attributes={'nonInteractive':{'nonInteraction': 1}};
            digitalData.event.push(ev);
            digitalData.page ='';
        },

        trackSocialCarouselClose = function(obj, selector, panel) {
            var ev = {},
                listPosition = obj.parents('[data-index]').data('index'),
                $activePanel = $(panel+ '[data-index="' + listPosition +'"]'),
                contentName = $activePanel.find('.o-text__quote').text(),
                shortContentName = contentName.trim().substr(0, 30);

            shortContentName = shortContentName.substr(0, Math.min(shortContentName.length, shortContentName.lastIndexOf(" ")));

            //track social attributes
            trackSocialAttributes(obj, $activePanel);

            ev.eventInfo={
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.closecontentfocus,
              'eventLabel' : shortContentName
            };
            ev.category ={'primaryCategory':ctConstants.custom};
            ev.attributes={'nonInteractive':{'nonInteraction': 1}};
            digitalData.event.push(ev);
            digitalData.page ='';
        },

        trackSocialCarouselChange = function(obj, selector, panel) {
            var ev = {},
                listPosition = obj.parents('[data-index]').data('index'),
                $activePanel = $(panel+ '[data-index="' + listPosition +'"]'),
                contentName = $(panel+ '.active .o-text__quote').text(),
                shortContentName = contentName.trim().substr(0, 30);

            shortContentName = shortContentName.substr(0, Math.min(shortContentName.length, shortContentName.lastIndexOf(" ")));

            //track social attributes
            trackSocialAttributes(obj, $activePanel);

            ev.eventInfo={
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.changecontentfocus,
              'eventLabel' : shortContentName
            };
            ev.category ={'primaryCategory':ctConstants.custom};
            ev.attributes={'nonInteractive':{'nonInteraction': 1}};
            digitalData.event.push(ev);
            digitalData.page ='';
        },

        //Social PromotionView Analytics
        trackPromotionTags = function(obj, options) {
            var ev = {};

            digitalData.promotion = [];

            digitalData.promotion.push({
                'promotionInfo' :{
                   'promotionId': options.promotionId,
                   'promotionName': options.promotionName
                }
            });

            // track component info
            trackComponentInfo(obj);

            ev.eventInfo = {
                'type': options.eventType
            };

            ev.attributes={'nonInteractive':{'nonInteraction': 1}};
            ev.category ={'primaryCategory':ctConstants.custom};

            digitalData.event.push(ev);

            digitalData.promotion = [];
            digitalData.component = [];
            digitalData.promotion.push({
                'promotionInfo' :{
                   'promotionId': '',
                   'promotionName': ''
                }
            });
        },

        trackVirtualAgentResults = function(obj, label) {
            var ev = {};

            // track component info
            trackComponentInfo(obj);

            ev.eventInfo={
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.virtualagent,
              'eventLabel' : label
            };
            ev.category ={'primaryCategory':ctConstants.custom};
             digitalData.event.push(ev);
        },

        trackAwards = function (obj, eventLabel) {
            var ev = {};

            // track component details
            trackComponentInfo(obj);

            // track product info
            digitalData.product = []; //empty product data for each events so that it picks up the correct product data
            trackProductInfo(obj, $('.c-product-overview'));

            ev.eventInfo = {
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.productawards,
              'eventLabel' : eventLabel
            };
            ev.category ={'primaryCategory':ctConstants.custom};
            digitalData.event.push(ev);
        },

        trackFilters = function (obj, selector) {
            var $this = obj,
                clickType = $this.data('ct-action'),
                title = $this.attr('title'),
                $selector = $(selector);

            // track component info
            trackComponentInfo($this);

            if (clickType === 'filter') {
                var ev = {},
                    label;
                    if(typeof $selector !== undefined) {
                        $selector.each(function(key, val) {
                            var $this = $(this);
                            if ($this.data('filter-type') === 'dropdown') {
                                label = $this.find('option:selected').text() + "|";
                            } else {
                                label = ($this.is(':checked')) ? $this.parent.text() : '' + "|";
                            }
                        });
                    }
                    else {
                        if ($this.data('filter-type') === 'dropdown') {
                            label = $this.find('option:selected').text();
                        } else {
                            label = ($this.is(':checked')) ? $this.parent.text() : '';
                        }
                    }                

                ev.eventInfo = {
                    'type': ctConstants.trackEvent,
                    'eventAction': ctConstants.filter,
                    'eventLabel' : label
                };
                ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
                ev.category = {'primaryCategory':ctConstants.custom};
                digitalData.event.push(ev);
            }
        },

        trackProductOverviewInfo = function(obj, productData) {
            var ev = {};
            // track component details
            trackComponentInfo(obj);

            // track product info
            digitalData.product = []; //empty product data for each events so that it picks up the correct product data
            trackProductInfo(obj, productData);

            ev.eventInfo = {
               'type':ctConstants.productInfo
            };
            ev.category = {'primaryCategory':ctConstants.other};
            ev.attributes = {'nonInteractive':{'nonInteraction': 1}};
            digitalData.event.push(ev);
        },

        // Carousel click tracking
        trackCarouselClick = function(carouselItem, videoWrapper) {
            var ev = {},
                label;

            $(document).off('click', '.slick-arrow, .slick-dots li, .js-ct-carousel-nav.slick-slide').on('click', '.slick-arrow, .slick-dots li, .js-ct-carousel-nav.slick-slide', function(evt) {
                var $this = $(this),
                    $carouselItem = $this.parents('[data-componentname]').find(carouselItem),
                    $videoWrapper = $carouselItem.find(videoWrapper),
                    activeVideo = (videoWrapper) ? $('.slick-current').find($videoWrapper) : null,
                    ctaUrl = (activeVideo !== null && activeVideo.length) ? activeVideo.data('video-id') : $carouselItem.find('.slick-slide.slick-current .o-btn').attr('href'),
                    hostedUrl = (ctaUrl !== undefined) ? (' - ' +ctaUrl) : '';

                // track component info
                trackComponentInfo($this);

                if ($this.is('.slick-arrow')) {
                    label = $this.text();
                } else if ($this.is('.js-ct-carousel-nav')) {
                    label = 'thumbnail#' + (parseInt($this.data('slick-index')) + 1);
                } else {
                    label = 'button#' + (parseInt($this.find('button').data('slick-index')) + 1);
                }

                ev.eventInfo = {
                  'type': ctConstants.trackEvent,
                  'eventAction': ctConstants.carouselClick,
                  'eventLabel' : label + hostedUrl
                };

                ev.category = {'primaryCategory':ctConstants.custom};
                digitalData.event.push(ev);
            });
        };

        /*
            Page load events
            *******************
        */

        setTimeout(function() {
            // product impression tag
            productImpressionTag('.c-related-products-carousel', '.c-related-products-item');
            productImpressionTag('.c-product-listing__container', '.c-product-listing__title');
            productImpressionTag('.c-product-listing-v2', '.js-product-listing-v2__list-item');
            productImpressionTag('.c-featured-product-v2', '.c-featured-product-v2');

            // product overview info
            if ($('.product-images').length) {
                trackProductOverviewInfo($('.c-product-images'), $('.js-product-images-thumbnail'));
            }

            /*AddThis Share Widget Services Track*/
            if ($('.o-social-sharing').length > 0 && (typeof addthis) !== undefined) {
                addthis.addEventListener("addthis.menu.share", trackShareThis);
            }

            //carousel v2 click events
            trackCarouselClick('.js-carousel-v2-item', '.js-video-player');

            //product image carousel click events
            trackCarouselClick('.js-product-images__wrapper');
        }, 1000);

        // basic Tracking for CTA Click
        $(document).off('click', 'a[data-ct-action], .o-btn[data-ct-action], .js-ct-linkclick a').on('click', 'a[data-ct-action], .o-btn[data-ct-action], .js-ct-linkclick a', function() {

            var $this = $(this),
                clickType = ($this.parents('.js-ct-linkclick').length) ? 'linkClick' : $this.data('ct-action'),
                title = ($this.attr('title')) ? $this.attr('title') : $this.data('title'),
                parentWrapper = $this.parents('[data-componentname]'),
                compContext = (parentWrapper !== undefined && parentWrapper.data('componentname') !== '') ? parentWrapper : parentWrapper.parents('[data-componentname]'),
                compName = compContext.data('componentname'),
                compCtaContext = $this.parents('[data-cta-context]'),
                context = (compCtaContext.data('cta-context')) ? compCtaContext.data('cta-context') : compName,
                label,
                ev = {},
                options = {};
            
            // track component info
            trackComponentInfo($this);

            switch (clickType) {
                case 'productClick':
                    options.price = $this.data('product-price');
                    options.quantity = $this.data('product-quantity');

                    //track product info
                    digitalData.product = []; //empty product data for each events so that it picks up the correct product data
                    trackProductInfo($this, $this.parents('[data-product-id]'), options);

                    ev.eventInfo = {
                      'type': ctConstants.productclick
                    };

                    if ($this.attr('data-info') === 'true') {
                        ev.category = {'primaryCategory': ctConstants.other};
                    } else {
                        ev.category = {'primaryCategory':ctConstants.custom};
                    }

                    break;

                case 'articleClick':
                    label = $this.attr('href');

                    ev.eventInfo = {
                      'type': ctConstants.trackEvent,
                      'eventAction': ctConstants.articleClick,
                      'eventLabel' : label
                    };
                    ev.category = {'primaryCategory': ctConstants.other};

                    break;

                case 'recipeClick':
                    label = $this.data('ct-recipeName');

                    ev.eventInfo = {
                      'type': ctConstants.trackEvent,
                      'eventAction': ctConstants.recipeClick,
                      'eventLabel' : label
                    };
                    ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
                    ev.category = {'primaryCategory': ctConstants.other};
                    digitalData.event.push(ev);

                    break;

                case 'recipeApplyFilter':
                    
                    trackFilters($this, '.c-recipe-listing-filter__filter');
                    break;    

                case 'relatedArticle':
                    label = $this.attr('href');

                    ev.eventInfo={
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.relatedArticle,
                        'eventLabel' : label
                    };

                    ev.category = {'primaryCategory': ctConstants.custom};

                    break;

                case 'tags':
                    label = $this.attr('title');

                    ev.eventInfo = {
                        'type':ctConstants.trackEvent,
                        'eventAction': ctConstants.tags,
                        'eventLabel' : label
                    };

                    ev.category = {'primaryCategory': ctConstants.custom};

                    break;

                case 'loadMore':
                    // product impression tracking
                    var productListWrapper = '.c-product-listing__container, .c-product-listing-v2';

                    // product listing
                    if ($(productListWrapper).length) {
                        productImpressionTag(productListWrapper, '.c-product-listing__title, .js-product-listing-v2__list-item');
                    }

                    ev.eventInfo = {
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.loadMore,
                      'eventLabel' : title
                    };

                    ev.category = {'primaryCategory':ctConstants.custom};
                    break;

                case 'pagination':
                    // product impression tracking
                    var productListWrapper = '.c-product-listing-v2';
                    
                    label = 'Pagination #'+ (($this.data('role') === 'navigation') ? $this.data('pagenum') : $this.data('role'));

                    // product listing
                    if ($(productListWrapper).length) {
                        productImpressionTag(productListWrapper, '.js-product-listing-v2__list-item');
                    }

                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.pagination,
                        'eventLabel' : label
                    };

                    ev.category = {'primaryCategory':ctConstants.custom};

                    break;

                case 'linkClick':
                    label = ($this.attr('href')) ? $this.attr('href') : $this.data('href');

                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.linkClick,
                        'eventLabel' : context + ' - ' + title  + ' - ' + label
                    };
                    
                    ev.category = {'primaryCategory': ctConstants.custom};

                    break;

                case 'anchorLink':
                    label = $this.data('ct-label');

                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.anchorLinkClicked,
                        'eventLabel' : label
                    };

                    ev.category = {'primaryCategory': ctConstants.custom};

                    break;

                case 'clicktoaction':
                    var linkHref = ($this.attr('href')) ? $this.attr('href') : $this.data('href'),
                        productData = $this.parents('[data-componentname]').find('[data-product-name]');

                    if (productData !== undefined) {
                        // tracking product info
                        trackProductInfo($this, productData);
                    }

                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.clicktoaction,
                        'eventLabel' : context + ' - ' + title  + ' - ' + linkHref
                    };

                    ev.category = {'primaryCategory': ctConstants.custom};

                    break;

                case 'clickstosocialplatforms':
                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.clickstosocialplatforms,
                        'eventLabel' : $this.attr('href')
                    };

                    ev.category = {'primaryCategory':ctConstants.custom};

                    break;

                case 'languageSelector':
                    
                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.languageSelector,
                        'eventLabel' : title + ' - ' + $this.attr('href')
                    };

                    ev.category = {'primaryCategory':ctConstants.custom};

                    break;

                case 'widgets':
                    var productData = $this.parents('[data-componentname]').find('[data-product-name]');

                    if (productData !== undefined) {
                         //track product info
                        digitalData.product = []; //empty product data for each events so that it picks up the correct product data
                        trackProductInfo($this, productData);
                    }

                    ev.eventInfo = {
                        'type':ctConstants.trackEvent,
                        'eventAction': ctConstants.widgets,
                        'eventLabel' : title
                    };

                    ev.category = {'primaryCategory':ctConstants.other};

                    break;

                case 'chat':
                    
                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.chat,
                        'eventLabel' : 'ChatOpen'
                    };

                    ev.category = {'primaryCategory':ctConstants.custom};

                    break;

                case 'download':
                    label = digitalData.page.category.contentType;
                    
                    ev.eventInfo={
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.share,
                      'eventLabel' : compName + ' - ' + title  + ' - ' + label
                    };

                    ev.category = {'primaryCategory':ctConstants.referral};

                    break;

                case 'accordionClick':
                    
                    ev.eventInfo = {
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.accordianClicked,
                      'eventLabel' : title
                    };

                    ev.category = {'primaryCategory':ctConstants.custom};

                    break;

                case 'quickView':
                    var $productObj = $this.parents('[data-product-name]');

                    label = $productObj.data('product-name');
                    options.listPosition = $productObj.data('index') + 1;

                    //track component info
                    trackComponentInfo($this.parents('[data-componentname]'));

                    //track product info
                    digitalData.product = []; //empty product data for each events so that it picks up the correct product data
                    trackProductInfo($this, $productObj, options);

                    ev.eventInfo = {
                      'type': ctConstants.trackEvent,
                      'eventAction': ctConstants.productQuickView,
                      'eventLabel' : label
                    };
                    ev.category = {'primaryCategory':ctConstants.custom};
                    
                    break;
            }
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);
        });

        // dropdown events

         $(document).off('change', 'select[data-ct-action], input').on('change', 'select[data-ct-action], input', function() {
            trackFilters($(this));            
        });

        // product overview dropdown tracking
        $(document).on('change', "#js-product-overview__select, .js-variant-dropdown-select", function () {
            var ev = {},
                $this = $(this),
                filterItemSelected = $this.find("option:selected").text();

            // track component info
            trackComponentInfo($this);

            ev.eventInfo ={
                'type':ctConstants.trackEvent,
                'eventAction': ctConstants.dropDown,
                'eventLabel' : filterItemSelected
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category ={'primaryCategory':ctConstants.custom};
            digitalData.event.push(ev);
        });

        // product impression tracking on dropdown change
        $(document).on("change", ".c-product-listing__form select", function () {
            productImpressionTag('.c-product-listing__container', '.c-product-listing__title');
        });

        // filters tracking

        $(document).on('change', ".c-product-listing__filter-input, .c-search-filters__select", function () {
            var filterItemSelected= $(this).find("option:selected").text(),
                compName = $(this).parents('[data-componentname]').data('componentname'),
                compVariants = $(this).parents('[data-component-variants]').data('component-variants'),
                compPositions = $(this).parents('[data-component-positions]').data('component-positions'),
                ev = {};
            // track component info
            trackComponentInfo($this);

            digitalData.product= []; //empty product data for each events so that it picks up the correct product data

            ev.eventInfo={
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.filter,
              'eventLabel' : filterItemSelected
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category ={'primaryCategory':ctConstants.custom};
            digitalData.event.push(ev);

        });

        // Carousel tracking
        $(document).on('click', '.js-thumb-carousel__nav .slick-arrow', function () {
            var ev = {},
                $this = $(this),
                direction = $this.attr('aria-label');

            // track component info
            trackComponentInfo($this);

            digitalData.direction = direction;
            ev.eventInfo={
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.widgets,
              'eventLabel' : 'Scroll'
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category ={'primaryCategory':ctConstants.other};
            digitalData.event.push(ev);
        });

        // Hero component read more tracking
        $(document).on('click', '.c-hero-button-readmore', function(){
            var ev = {},
                $this = $(this),
                openCloseStatus = '';

            // track component info
            trackComponentInfo($this);

            if (!$('.js-show-more').is(':visible')) {
                openCloseStatus='open';

                ev.eventInfo={
                    'type':ctConstants.trackEvent,
                    'eventAction': ctConstants.readMore,
                    'eventLabel' : openCloseStatus
                };
            } else {
                openCloseStatus='close';
            }
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category ={'primaryCategory':ctConstants.custom};
            digitalData.event.push(ev);
        });

        /*
            Page scroll tracking
            *******************
            analytics code for determining the percentage of page read by user
        */

        var scroll24 = 0; var scroll49 = 0; var scroll74 = 0; var scroll99 = 0; var scroll100 = 0;
        $(window).scroll(function () {
            var h = $(document).height() - $(window).height(),
                h25 = Math.floor(h * 0.25),  // 25%
                h50 = Math.floor(h * 0.50),  // 50%
                h75 = Math.floor(h * 0.75);  // 75%

            if ($(window).scrollTop() > 0 && $(window).scrollTop() <h25 && scroll24 == 0) {
                scroll24 = 1;
                var ev = {};
                ev.eventInfo={
                  'type':ctConstants.trackEvent,
                  'eventAction': ctConstants.pageScroll,
                  'eventLabel' : 'Scroll Depth - 0%'
                };
                ev.attributes={'nonInteractive':{'nonInteraction':1}};
                ev.category ={'primaryCategory':ctConstants.custom};
                digitalData.event.push(ev);

            } else if ($(window).scrollTop() >= h25 && $(window).scrollTop() < h50 && scroll49 == 0) {
                scroll49 = 1;
                var ev = {};
                ev.eventInfo={
                  'type':ctConstants.trackEvent,
                  'eventAction': ctConstants.pageScroll,
                  'eventLabel' : 'Scroll Depth - 25%'
                };
                ev.attributes={'nonInteractive':{'nonInteraction':1}};
                ev.category ={'primaryCategory':ctConstants.custom};
                digitalData.event.push(ev);

            } else if($(window).scrollTop() >= h50 && $(window).scrollTop() < h75 && scroll74 == 0) {
                scroll74 = 1;
                var ev = {};
                ev.eventInfo={
                  'type':ctConstants.trackEvent,
                  'eventAction': ctConstants.pageScroll,
                  'eventLabel' : 'Scroll Depth - 50%'
                };
                ev.attributes={'nonInteractive':{'nonInteraction':1}};
                ev.category ={'primaryCategory':ctConstants.custom};
                digitalData.event.push(ev);

            } else if($(window).scrollTop() >= h75 && $(window).scrollTop() < h && scroll99 == 0) {
                scroll99 = 1;
                var ev = {};
                ev.eventInfo={
                  'type':ctConstants.trackEvent,
                  'eventAction': ctConstants.pageScroll,
                  'eventLabel' : 'Scroll Depth - 75%'
                };
                ev.attributes={'nonInteractive':{'nonInteraction':1}};
                ev.category ={'primaryCategory':ctConstants.custom};
                digitalData.event.push(ev);

            } else if($(window).scrollTop() == h && scroll100 == 0){
                scroll100 = 1;
                var ev = {};
                ev.eventInfo={
                  'type':ctConstants.trackEvent,
                  'eventAction': ctConstants.pageScroll,
                  'eventLabel' : 'Scroll Depth - 100%'
                };
                ev.attributes={'nonInteractive':{'nonInteraction':1}};
                ev.category ={'primaryCategory':ctConstants.custom};
                digitalData.event.push(ev);
            }
        });

        /*
            E-gifting Tracking
            *******************
        */

        //Personalise Gift button track starts*/
        $(document).on('click', '.c-product-overview__customisable-wrap .o-btn', function() {
            var $this = $(this),
                productName = $('.c-product-overview__title').text(),
                productId = $('.c-product-overview').data('product-id'),
                ev = {};

            // track component info
            trackComponentInfo($this);

            digitalData.product = []; //empty product data for each events so that it picks up the correct product data

            digitalData.product.push({
               'productInfo' :{
                   'productID': productId,
                   'productName': productName
                }
            });

            ev.eventInfo = {
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.personalizegift,
              'eventLabel' : "PersonalizedGift -" + productName
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category ={'primaryCategory':ctConstants.custom};
            digitalData.event.push(ev);
        });

        // Egifting Add to Basket button tracking
        $(document).on('click', '.js-btn__addtocart', function() {
            var ev = {},
                options = {},
                $this = $(this),
                compWrapper = $this.parents('.c-gift-configurator');

            options.price = $('.o-product-total .o-product-price').text();
            options.quantity = 1;

            if ($('.has-error').length > 0) {
                errorTracking($this, compWrapper);
            }
            if (!$(this).hasClass("disabled")) {
                // track product info
                digitalData.product = []; //empty product data for each events so that it picks up the correct product data
                trackProductInfo($this, compWrapper, options);

                ev.eventInfo = {
                  'type': ctConstants.addtoCart
                };
                ev.category = {'primaryCategory': ctConstants.custom};
                ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
                digitalData.event.push(ev);
            }
        });

        // Shopping basket +- tracking

        $(document).on('click', '.o-btn--add', function() {
            var ev = {},
                options = {},
                $this = $(this),
                listWrapper = $this.parents('.c-shopping-basket__item'),
                indivisualPrice = $('.js-currency-symbol').text() + parseInt(listWrapper.find('#quantityWrapPrice').text()) / parseInt(listWrapper.find('.quantitywrap__cta-quantity').text());

            options.price = indivisualPrice;
            options.quantity = 1;

            // track product info
            digitalData.product = []; //empty product data for each events so that it picks up the correct product data
            trackProductInfo($this, listWrapper, options);

            ev.eventInfo = {
              'type': ctConstants.addtoCart
            };
            ev.category = {'primaryCategory': ctConstants.custom};
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);
        });

        // Shopping basket Remove Items tracking

        $(document).on('click', '.js-btn--cancel, .o-btn--remove', function() {
            var ev = {},
                options = {},
                $this = $(this),
                listWrapper = $this.parents('.c-shopping-basket__item'),
                isEnabled = false,
                indivisualPrice = $('.js-currency-symbol').text() + parseInt(listWrapper.find('#quantityWrapPrice').text()) / parseInt(listWrapper.find('.quantitywrap__cta-quantity').text());

            options.price = ($this.is('.js-btn--cancel')) ? listWrapper.find('.quantitywrap__price').text() :indivisualPrice;
            options.quantity = ($this.is('.js-btn--cancel')) ? listWrapper.find('.quantitywrap__cta-quantity').text() : 1;

            if ($(this).is('.o-btn--remove__disabled')) {
                isEnabled = false;
            } else {
                isEnabled = true;
            }

            if (isEnabled) {
                // track product info
                digitalData.product = []; //empty product data for each events so that it picks up the correct product data
                trackProductInfo($this, listWrapper, options);

                ev.eventInfo = {
                  'type': ctConstants.removecart
                };
                ev.category = {'primaryCategory': ctConstants.custom};
                ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
                digitalData.event.push(ev);
            }

        });

        //Egifting Secure Checkout button tracking
        $(document).on('click', '.js-btn-checkout', function() {
            var ev = {},
                compWrapper = $('.c-shopping-basket'),
                listWrapper = $('.c-shopping-basket__item'),
                compName = compWrapper.data('componentname');

            // track Component Info
            trackComponentInfo(compWrapper);

            digitalData.product = []; //empty product data for each events so that it picks up the correct product data

            listWrapper.each(function() {
                var $this = $(this),
                    options = {};

                options.quantity = $this.find('.quantitywrap__cta-quantity').text();
                option.price = $this.find('.quantitywrap__price').text();
                // track product info
                trackProductInfo(compWrapper, $this, options);
            });

            ev.eventInfo = {
              'type': ctConstants.checkoutenhanced
            };
            ev.category = {'primaryCategory': ctConstants.custom};
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);
        });

        //Egifting Secure Checkout Form error tracking

        $(document).on('click', '.js-egifting-submit-btn', function() {
            var productData = localStorage.getItem('productCartData');

            if (productData !== undefined && productData !== '' && productData !== null) {

                var ev = {},
                    options = {},
                    compWrapper = $('.c-order-summary'),
                    parsedProductData = JSON.parse(productData).products,
                    errorlog = [],
                    errorvalue = '';

                // track component info
                trackComponentInfo(compWrapper);

                digitalData.product = []; //empty product data for each events so that it picks up the correct product data

                $.each(parsedProductData, function(key, val) {
                    var $this = val;
                    options.productID = val.productId;
                    options.productName = val.title;
                    options.price = val.price;
                    options.quantity = val.quantity;

                    trackProductInfo(compWrapper, $this, options);
                });

                setTimeout(function() {
                    if ($('.has-error .help-block').length > 0) {
                        $('.has-error .help-block').each(function(i, value) {
                           errorlog.push($(this).text());
                        });
                    }

                    if($('.braintree-msg-list').hasClass('has-error') && $('.braintree-msg-list li').length > 0) {
                        $('.braintree-msg-list li').each(function(key, val) {
                            var errMsg = $(this).text();
                            errorlog.push(errMsg.slice(3));
                        });
                    }

                    errorvalue += errorlog.join(" | ");

                    ev.eventInfo = {
                      'type': ctConstants.trackEvent,
                      'eventAction': ctConstants.inputerror,
                      'eventLabel' : errorvalue
                    };
                    ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
                    ev.category = {'primaryCategory':ctConstants.conversion};
                    digitalData.event.push(ev);
                }, 2500);
            }
        });

        // E Gifting Product Cross Sell tracking
        $(document).on('click', '.js-btn__personalize', function() {
            var ev = {},
                $this = $(this),
                productName = $this.data('product-name');

            //track component info
            trackComponentInfo($this);

            //track product info
            digitalData.product = []; //empty product data for each events so that it picks up the correct product data
            trackProductInfo($this, $this);

            ev.eventInfo={
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.personalizegift,
              'eventLabel': "PersonalizedGift - " + productName
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category = {'primaryCategory':ctConstants.custom};
            digitalData.event.push(ev);
        });

        // Purchase Confirmation Tracking
        if ($('.c-purchase-confirmation').length > 0) {
            var ev = {};

            ev.eventInfo = {
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.ordersummary,
              'eventLabel' : $('#orderId').val()
            };
            ev.category ={'primaryCategory':ctConstants.custom};
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);
        }

        // Download PDF Tracking
        $(document).on('click', '.c-purchase-confirmation__downloadlink', function() {
            var ev = {};
            ev.eventInfo = {
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.downloads,
              'eventLabel' : $(this).text()
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category ={'primaryCategory':ctConstants.other};
            digitalData.event.push(ev);
        });

        //Error page tracking
        if ($('.c-dynamic-error-message').length) {
            var ev = {};
            ev.eventInfo = {
                'type': ctConstants.trackEvent,
                'eventAction': ctConstants.inputerror,
                'eventLabel' : $('.c-dynamic-error-message .o-text__body').text()
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category = {'primaryCategory':ctConstants.custom};
            digitalData.event.push(ev);
        }

        //promo products tagging start

        var $promotionTag = $('.c-promotion-tag');

        if ($promotionTag.length) {
            $promotionTag.each(function() {
                var obj = $(this),
                    options = {};

                options.promotionName = obj.find('a').data('title');
                options.promotionId = obj.find('a').data('product-id');
                options.eventType = ctConstants.promotionView;

                trackPromotionTags(obj, options);
            });

            $promotionTag.find('a').on('click', function() {
                var obj = $(this),
                    options = {};

                options.promotionName = obj.data('title');
                options.promotionId = obj.data('product-id');
                options.eventType = ctConstants.promotionClick;

                trackPromotionTags(obj, options);
            });
        }
        //promo products tagging end

        //quick view
        $(document).on('click', '.c-product-listing__quick-cta', function() {
            var ev = {},
                $this = $(this);

            //track component info
            trackComponentInfo($this.parents('[data-componentname]'));

            //track product info
            digitalData.product = []; //empty product data for each events so that it picks up the correct product data
            trackProductInfo($this, $this);

            ev.eventInfo = {
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.productQuickView,
              'eventLabel' : $this.data('product-name')
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category = {'primaryCategory':ctConstants.custom};
            digitalData.event.push(ev);
        });

        //Left Right Quickview - CP-42
        $(document).on('click', '.js-social-gallery-TAB__panel .js-social-gallery__arrow', function() {
            trackSocialCarouselLeftRight($(this), '.c-social-gallery__list');
        });

        //Left Right Quickview - CP-68
        $(document).on('click', '.c-social-gallery-carousel__panel .js-social-gallery-carousel_arrow', function() {
            trackSocialCarouselLeftRight($(this), '.c-social-gallery-carousel-expanded__list');
        });

        //Close Content Focus - CP-42
        $(document).on('click', '.js-btn-social-gallery__close', function() {
            trackSocialCarouselClose($(this), '.c-social-gallery__list', '.c-social-gallery__panel');
        });

        //Close Content Focus - CP-68
        $(document).on('click', '.js-btn-social-gallery-carousel__close', function() {
            trackSocialCarouselClose($(this), '.c-social-gallery-carousel-expanded__list', '.c-social-gallery-carousel__panel');
        });

        //Change Content Focus - CP-42
        $(document).on('click', '.js-social-gallery-TAB__thumbnail', function() {
            if ($('.c-social-gallery__item').is('.active')){
                trackSocialCarouselChange($(this), '.c-social-gallery__list', '.c-social-gallery__panel');
            } else {
                trackSocialCarouselClose($(this), '.c-social-gallery__panel', '.c-social-gallery__panel');
            }
        });

        //Change Content Focus - CP-68
        $(document).on('click', '.js-social-gallery-carousel__thumbnail', function() {
            if ($('.c-social-gallery-carousel-expanded__list .c-social-gallery-carousel__item').is('.active')) {
                trackSocialCarouselChange($(this), '.c-social-gallery-carousel-expanded__list', '.c-social-gallery-carousel__panel');
            } else {
                trackSocialCarouselClose($(this), '.c-social-gallery-carousel__panel', '.c-social-gallery-carousel__panel');
            }
        });

        //Spin Carousel - CP-54
        $(document).on('click', '.js-multiple-related-tweet__carousel .slick-dots li', function() {
            var ev = {},
                $this = $(this),
                compName = $this.parents('[data-componentname]').data('componentname');

            //track component info
            trackComponentInfo($this);

            ev.eventInfo = {
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.spincarousel,
              'eventLabel' : compName
            };
            ev.category = {'primaryCategory':ctConstants.custom};
            ev.attributes = {'nonInteractive':{'nonInteraction': 1}};
            digitalData.event.push(ev);
        });

        //Clicks to Social Platforms - CP-40, CP-54, CP-43
        $(document).on('click', '.c-featured-tweet__copy .o-btn, .c-multiple-related-tweet__copy .o-btn, .c-brand-social-channels__a', function() {
            var ev = {},
                $this = $(this),
                destinationUrl = $this.attr('href');

            //track component info
            trackComponentInfo($this);

            ev.eventInfo={
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.clickstosocialplatforms,
              'eventLabel' : destinationUrl
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category ={'primaryCategory':ctConstants.referral};
            digitalData.event.push(ev);
        });

        //Social Aggregator Analytics Start//

        //Left Right Quickview - CP-66
        $(document).on('click', '.js-social-gallery__video-list .js-social-gallery__arrow', function() {
            trackSocialCarouselLeftRight($(this), '.js-social-gallery__video-list');
        });

        //Left Right Quickview - CP-69
        $(document).on('click', '.js-social-carousel__panel .js-social-carousel__arrow', function() {
            trackSocialCarouselLeftRight($(this), '.c-social-carousel__list');
        });

        //Close Content Focus - CP-66
        $(document).on('click', '.js-btn-social-gallery__close', function() {
            trackSocialCarouselClose($(this), '.js-social-gallery__panel', '.js-social-gallery__panel');
        });

        //Close Content Focus - CP-69
        $(document).on('click', '.js-btn-social-carousel__close', function() {
            trackSocialCarouselClose($(this), '.js-social-carousel__panel', '.js-social-carousel__panel');
        });

        //Change Content Focus - CP-66
        $(document).on('click', '.js-social-gallery__thumbnail', function() {
            if ($('.c-social-gallery__item').is('.active')){
                trackSocialCarouselChange($(this), '.js-social-gallery__video-list', '.js-social-gallery__panel');
            } else {
                trackSocialCarouselClose($(this), '.js-social-gallery__panel', '.js-social-gallery__panel');
            }
        });

        //Change Content Focus - CP-69
        $(document).on('click', '.js-social-carousel__thumbnail', function() {
            if ($('.js-social-carousel__list .js-social-carousel__item').is('.active')) {
                trackSocialCarouselChange($(this), '.js-social-carousel__list', '.js-social-carousel__panel');
            } else {
                trackSocialCarouselClose($(this), '.js-social-carousel__panel', '.js-social-carousel__panel');
            }
        });

        //Follow Account - CP-43
        $(document).on('click', '.c-brand-social-channels__a', function() {
            var ev = {},
                $this = $(this),
                socialNetwork = $this.attr('title');

            //track component info
            trackComponentInfo($this);

            ev.eventInfo={
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.followaccount,
              'eventLabel' : socialNetwork
            };
            ev.category = {'primaryCategory':ctConstants.referral};
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);

        });

        //Social Promotion Analytics
        if ($('.js-social-promotion-tag').length) {
            $('.js-social-promotion-tag').each(function() {
                var obj = $(this),
                    options = {};

                options.promotionName = obj.find('.o-btn').attr('title');
                options.promotionId = obj.find('.o-btn').attr('href');
                options.eventType = ctConstants.promotionView;

                trackPromotionTags(obj, options);
            });

            $(document).on('click', '.js-social-promotion-tag .o-btn', function() {
                var obj = $(this),
                    options = {};

                options.promotionName = obj.attr('title');
                options.promotionId = obj.attr('href');
                options.eventType = ctConstants.promotionClick;

                trackPromotionTags(obj, options);
            });
        }

        //PromotionView/Click Brand Social Channels
        if ($('.js-social-promotion-tag__brand-social-channels').length) {
            $('.js-social-promotion-tag__brand-social-channels').each(function() {
                var obj = $(this).find('.c-brand-social-channels__a'),
                    options = {};

                options.promotionName = obj.attr('title');
                options.promotionId = obj.attr('href');
                options.eventType = ctConstants.promotionView;

                trackPromotionTags(obj, options);

            });

            $(document).on('click', '.js-social-promotion-tag__brand-social-channels .c-brand-social-channels__a', function() {
                var obj = $(this),
                    options = {};

                options.promotionName = obj.attr('title');
                options.promotionId = obj.attr('href');
                options.eventType = ctConstants.promotionClick;

                trackPromotionTags(obj, options);
            });
        }

        //Social Component CTA - CP-42, CP-66, CP-68, CP-69
        $(document).on('click', '.js-social-component-cta .o-btn', function() {
            var ev = {},
                $this = $(this),
                ctaLabel = $this.attr('title');

            //track component info
            trackComponentInfo($this);

            ev.eventInfo = {
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.socialcomponentcta,
              'eventLabel' : ctaLabel
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category = {'primaryCategory':ctConstants.custom};
            digitalData.event.push(ev);
        });

        //Footer social link tracking
        $(document).on('click', '.c-footer-social a.c-social-icons-list', function() {
            var ev = {},
                $this = $(this),
                destURL = $this.attr('href');

            //track component info
            trackComponentInfo($this);

            ev.eventInfo = {
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.clickstosocialplatforms,
              'eventLabel' : destURL
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category = {'primaryCategory':ctConstants.referral};
            digitalData.event.push(ev);

            digitalData.component = [];
        });

        //Call Out Button CTA
        $(document).on('click', '.c-call-out-button__copy .o-btn', function() {
            var ev = {},
                $this = $(this),
                destinationUrl = $this.attr('href');

            //track component info
            trackComponentInfo($this);

            ev.eventInfo = {
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.buttonclicked,
              'eventLabel' : destinationUrl
            };
            ev.category = {'primaryCategory':ctConstants.other};
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);
        });

        //Quick Poll Tracking
        if ($('.c-quick-poll-wrap').length) {
            var ev = {},
                $this = $('.c-quick-poll-wrap'),
                pollId = $this.data('quick-poll-id'),
                pageTitle = digitalData.page.category.contentType;

            //track component info
            trackComponentInfo($this);

            ev.eventInfo = {
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.quickPoll,
              'eventLabel' : 'QP Launch - ' + pollId + ' - ' + pageTitle
            };

            ev.category = {'primaryCategory':ctConstants.custom};
            ev.attributes = {'nonInteractive':{'nonInteraction': 1}};
            digitalData.event.push(ev);

            digitalData.component = [];
        }

        //Quick Poll Submit
        $(document).on('click', '.js-btn-quick-poll', function() {
            var ev = {},
                $this = $(this),
                answerChosen = $('.c-qusAns-answer__container .selected').text(),
                pollId = $this.parents('[data-quick-poll-id]').data('quick-poll-id');

            //track component info
            trackComponentInfo($this);

            ev.eventInfo = {
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.quickPoll,
              'eventLabel' : 'QP - ' + pollId + '- Submit - ' + answerChosen
            };
            ev.category = {'primaryCategory':ctConstants.custom};
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);

            digitalData.component = [];
        });

        //Quick Poll Response Tracking
        $(document).on('click', '.js-quick-poll-res-link', function() {
            var ev = {},
                $this = $(this),
                pollId = $this.parents('[data-quick-poll-id]').data('quick-poll-id'),
                headline = $this.attr('title'),
                pageTitle = digitalData.page.category.contentType;

            // track component info
            trackComponentInfo($('.c-quick-poll-wrap'));

            ev.eventInfo = {
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.quickPoll,
              'eventLabel' : 'QP - ' + pollId + ' - Response - ' + pageTitle
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category = {'primaryCategory':ctConstants.custom};
            digitalData.event.push(ev);

            digitalData.component = [];
        });

        //Smartlabel Image Click
        $(document).on('click', 'header .image-link', function(){
            var ev = {},
                eventLabel = $(this).find('img').attr('alt');

            digitalData.component = [];
            digitalData.product = []; //empty product data for each events so that it picks up the correct product data

            ev.eventInfo = {
              'type': onstants.trackEvent,
              'eventAction': ctConstants.imageclick,
              'eventLabel' : eventLabel
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category = {'primaryCategory':ctConstants.custom};
            digitalData.event.push(ev);
        });

        //Smartlabel Click
        $(document).on('click', '.o-btn-smart-label, #js-smart-label__product', function() {
            var ev = {},
                $this = $(this),
                pageType = digitalData.page.category.pageType,
                productId = $('.c-product-overview').data('product-id'),
                productvariant,
                productName;

            if ($("#js-smart-label__select").length > 0) {
                productvariant = $("#js-smart-label__select option:selected").data('unit');
                productName = $("#js-smart-label__select option:selected").data('product-name');
            } else {
                productvariant = $this.data('unit');
                productName = $this.text();
            }

            //track component info
            trackComponentInfo($this);

            digitalData.product = []; //empty product data for each events so that it picks up the correct product data
            digitalData.product.push({
               'productInfo' :{
                   'productID': productId,
                   'productName': productName
               },
               'attributes':{
                   'productVariants': productvariant
               }
            });

            ev.eventInfo = {
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.smartlabelClick,
              'eventLabel' : pageType + ' | ' + productvariant
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category ={'primaryCategory':ctConstants.custom};
            digitalData.event.push(ev);
        });



        //Contact us click to call
        $(document).on('click', '.call-our-careline a', function() {
            var ev = {},
                callVal = $(this).parents('li').find('span').text(),
                $this = $(this);

            //track component info
            trackComponentInfo($this);

            ev.eventInfo = {
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.contactus,
              'eventLabel' : 'Contact-Us - Click To Call - ' + callVal
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category = {'primaryCategory':ctConstants.other};
            digitalData.event.push(ev);

            digitalData.component = [];
        });

        // Shoppable tracking
        $(document).on('click', '.addtobag-btn', function() {
            var ev = {},
                $this = $(this),
                productData = ($this.data('productname')) ? $this : $this.parents('[data-product-name]'),
                productName = (productData.data('productname')) ? productData.data('productname') : productData.data('product-name'); 

            trackProductInfo($this, productData);

            ev.eventInfo = {
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.purchase,
              'eventLabel' : 'Online - '+ productName
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category = {'primaryCategory':ctConstants.conversion};
            digitalData.event.push(ev);

        });

        $('.viewMyBag').on('click', '.js-viewBag-btn', function() {
            var ev = {};
            ev.eventInfo = {
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.viewBag,
              'eventLabel' : $(this).text()
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category = {'primaryCategory':ctConstants.custom};

            digitalData.event.push(ev);
        });

        // shoppable - continue shopping btn click
        $('.js-shopping-bag').on('click','.js-shopping-btn', function() {
            var ev = {};
            ev.eventInfo={
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.continueShopping,
              'eventLabel' : 'Continue Shopping'
            };
            ev.category ={'primaryCategory':ctConstants.custom};
            digitalData.event.push(ev);
        });

        // shoppable - go back btn click
        $('.c-shopping-checkout').on('click','.js-shopping-btn', function() {
            var ev = {};
            ev.eventInfo = {
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.viewBag,
              'eventLabel' : $(this).text()
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category = {'primaryCategory':ctConstants.custom};

            digitalData.event.push(ev);
        });

        // Add to bag btn click - capture product info
        $(document).on('click','#shoppable_magic_add_to_cart_btn', function() {
            var ev = {};
            digitalData.product = []; //empty product data for each events so that it picks up the correct product data
            digitalData.product.push({
               'productInfo' :{
                   'productID': $('.addtobag-btn').data('upc'),
                   'productName': $('.addtobag-btn').data('productname'),
                   'price': $('#price').text(),
                   'brand': $('.addtobag-btn').data('brandname'),
                   'quantity': $('#shoppable_magic_select_qty').val()
               },
               'category':{
                'primaryCategory':''
               },
               'attributes':
               {
                   'productVariants': $('#shoppable_magic_select_color_size').val(),
                   'listPosition': ''
               }
            });
            ev.eventInfo = {
              'type':ctConstants.addtoCart
            };
            ev.category = {
                'primaryCategory':ctConstants.custom
            };
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);
        });

        //PDP page buy in store tracking
        $('.o-product-overview__store-locator a.js-btn__form').on('click', function() {
            if(!$(this).hasClass('is-active')) {
                var ev = {},
                $this = $(this),
                prodData = $this.parents('.product-overview').find('.c-product-overview');

                //track component info
                trackComponentInfo($this);

                //track product info
                digitalData.product = []; //empty product data for each events so that it picks up the correct product data
                trackProductInfo($this, prodData);

                ev.eventInfo = {
                  'type':ctConstants.trackEvent,
                  'eventAction': ctConstants.storeLocator,
                  'eventLabel' : 'Storelocator - Launch Open'
                };
                ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
                ev.category ={'primaryCategory':ctConstants.custom};
                digitalData.event.push(ev);

                digitalData.product = [];
                digitalData.component = [];
            }
        });

        $(document).on('click', '.js-btn-buyStore, .js-btn-show-store', function() {
            var ev = {},
                compName = $(this).parents('.product-overview').find('.c-product-overview').data('componentname'),
                prodId = $(this).parents('.product-overview').find('.c-product-overview').data('product-id'),
                prodVariation = $('.c-buyStore__form .c-buyStore-pvi option:selected').text().trim(),
                radiusSel = $('.c-buyStore__form .c-buyStore-dw option:selected').text().trim(),
                zipCode = $('.c-buyStore__form .postalCode').val();

            if (!$(this).parents('.c-buyStore__form').find('.has-error').length) {
                digitalData.component.push({
                    'componentInfo' :{
                        'name': compName
                    }
                });

                ev.eventInfo = {
                  'type':ctConstants.trackEvent,
                  'eventAction': ctConstants.storeLocator,
                  'eventLabel' :'Store Locator Options - ' + prodVariation + ' | Entered postcode | ' + radiusSel
                };
                ev.category ={'primaryCategory':ctConstants.custom};
                ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
                digitalData.event.push(ev);

                digitalData.component = [];

                if (typeof $BV === 'object') {
                    $BV.SI.trackConversion({
                        'type': 'StoreLocator',
                        'label': zipCode,
                        'value': prodId
                    });
                }
            }
        });

        //Where to Buy Tracking
        $(document).on('click', '.js-btn-bss', function() {
            var ev = {},
                $this = $(this),
                prodVariation = $('.js-btn-bss').parents('.c-buy-in-store-search').find('.js-buy-in-store-search-list .choose-variant:checked').next().text(),
                radiusSel = $('.c-wtb__select option:selected').text().trim(),
                prodId = $('.js-btn-bss').parents('.c-buy-in-store-search').find('.js-buy-in-store-search-list .choose-variant:checked').data("product-id"),
                zipCode = $('.c-buy-in-store-search__form .postalCode').val();

            if (!$(this).parents('.c-buy-in-store-search__form').find('.has-error').length) {

                //track component info
                trackComponentInfo($this);

                ev.eventInfo = {
                  'type':ctConstants.trackEvent,
                  'eventAction': ctConstants.storeLocator,
                  'eventLabel' :'Store Locator Options - ' + prodVariation + ' | Entered postcode | ' + radiusSel
                };
                ev.category = {'primaryCategory':ctConstants.custom};
                ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
                digitalData.event.push(ev);

                digitalData.component = [];

                //ROI Beacon Tracking
                if (typeof $BV === 'object') {
                    $BV.SI.trackConversion({
                        'type': 'StoreLocator',
                        'label': zipCode,
                        'value': prodId
                    });
                }
            }
        });

        /*
               Coupons/Spotlight Analytics
            --------------------------------_
        */

        $(document).on('click', '.c-spotlight-view__wrapper .c-spotlight-btn', function(){
            var ev = {},
                $this = $(this),
                destURL = $this.attr('href');

            //track component info
            trackComponentInfo($this);

            ev.eventInfo = {
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.coupon,
              'eventLabel' : destURL
            };
            ev.category = {'primaryCategory':ctConstants.custom};
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);

            digitalData.component = [];
        });

        /*
              Live Chat Analytics
            -----------------------
        */

        $(document).on('click', '.c-live-chat__wrapper img[id^="liveagent_button_online"]', function() {
            var ev = {},
                $this = $(this);

            //track component info
            trackComponentInfo($this);

            ev.eventInfo = {
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.chat,
              'eventLabel' : "Open"
            };
            ev.category = {'primaryCategory':ctConstants.custom};
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);

            digitalData.component = [];
        });

        //Awards analytics
        $(document).on('click', '.js-award-img, .js-awardBtn', function() {
            var eventLabel = 'AwardView - ' + $(this).parents('.c-award-img').find('.c-award-name').text();
            trackAwards($(this), eventLabel);
        });

        $(document).on('click', '.js-awards-btn', function() {
            var eventLabel = 'Main - ' + $(this).attr('href');
            trackAwards($(this), eventLabel);
        });

        //Product Detail Read More
        if ($("body").hasClass('pdp-page')) {
            $(document).on('click', '.c-expandcollapse__link', function() {
                if($(this).parents('.c-expandcollapse').find('.c-expandcollapse__header').hasClass('expanded')){
                    var ev = {},
                        $this = $(this),
                        productData = $('.c-product-overview'),
                        productName = productData.data('product-name'),
                        sectionName = $this.find('.c-expandcollapse__title').text();

                    //track product info
                    digitalData.product = []; //empty product data for each events so that it picks up the correct product data
                    trackProductInfo($this, productData);

                    ev.eventInfo = {
                      'type':ctConstants.trackEvent,
                      'eventAction': ctConstants.accordianClicked,
                      'eventLabel' : productName + ' - ' + sectionName
                    };
                    ev.category = {'primaryCategory':ctConstants.custom};
                    ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
                    digitalData.event.push(ev);
                }
            });
        }

        //Anchor Link tracking
        $(document).on('click', '.js-navigation-sticky__item', function() {
            var ev = {},
                $this = $(this),
                sectionName = $this.find('.c-navigation-sticky__button').text();

            //track component info
            trackComponentInfo($this);

            ev.eventInfo = {
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.anchorLinkClicked,
              'eventLabel' : sectionName
            };
            ev.category = {'primaryCategory':ctConstants.custom};
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);
        });

        //Store locator direction tracking
        $(document).on('click', '.map-direction', function(e) {
            var ev = {},
                $this = $(this),
                linkAttr = $this.attr('href'),
                titleTxt = $this.attr('title');

                //track component info
                trackComponentInfo($this);

                ev.eventInfo = {
                  'type':ctConstants.trackEvent,
                  'eventAction': ctConstants.storeLocator,
                  'eventLabel' : 'Store Locator - ' + titleTxt + ' - ' + linkAttr
                };
                ev.category = {'primaryCategory':ctConstants.custom};
                ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
                digitalData.event.push(ev);

                digitalData.component = [];
        });

        // TurnTo analytics
        // click on ask a question link
        $(document).on('click', '.c-product-overview__turntowrapper .TurntoItemTeaserClick', function() {
            var ev = {};
            ev.eventInfo = {
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.turnto,
              'eventLabel' : 'Top Module - Ask A Question'
            };
            ev.category = {'primaryCategory':ctConstants.custom};
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);
        });

        // click on 'see question' link
        $(document).on('click', '.c-product-overview__turntowrapper .TurnToIteaSee', function() {
            var ev = {};
            ev.eventInfo = {
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.turnto,
              'eventLabel' : 'Top Module - See Questions'
            };
            ev.category = {'primaryCategory':ctConstants.custom};
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);
        });

        // click on 'submit a question'
        $(document).on('click', '#TT2askOwnersBtn', function() {
            var ev = {};
            ev.eventInfo = {
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.turnto,
              'eventLabel' : 'Question Submit'
            };
            ev.category = {'primaryCategory':ctConstants.custom};
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);
        });

        // click on 'Post an answer' link
        $(document).on('click', '#TurnToContent .TT3answerBtn', function() {
            var ev = {};
            ev.eventInfo = {
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.turnto,
              'eventLabel' : 'Answer Submit'
            };
            ev.category = {'primaryCategory':ctConstants.custom};
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);
        });

        // click on 'Inaccurate' link
        $(document).on('click', '#TurnToContent .TTflagAnswer a', function() {
            var ev = {};
            ev.eventInfo = {
              'type': ctConstants.trackEvent,
              'eventAction': ctConstants.turnto,
              'eventLabel' : 'Rated Inaccurate'
            };
            ev.category = {'primaryCategory':ctConstants.custom};
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);
        });

        //Zip File Download Tracking
        $(document).on('click', '.c-zip-file-download .o-btn', function() {
            var ev = {},
                fileName = $(this).data('zip-file-name');

            ev.eventInfo = {
                'type': ctConstants.trackEvent,
                'eventAction': ctConstants.downloads,
                'eventLabel' : fileName
            };
            ev.category ={'primaryCategory':ctConstants.other};
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);
        });

        //Virtual Agent launch
        if ($('.virtual-agent-container').length) {
            var ev = {},
                $this = $('.virtual-agent-container');

            //track component info
            trackComponentInfo($this);

            ev.eventInfo = {
                'type': ctConstants.trackEvent,
                'eventAction': ctConstants.virtualagent,
                'eventLabel': 'Virtual Agent - Launch'
            };
            ev.category = {'primaryCategory':ctConstants.custom};
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);
        }

        //Virtual Agent Results Page
        $(document).on('click', '.virtual-agent-result a', function() {
            var ev = {},
                label = $(this).text();

            if ($(this).is('.c-expandcollapse__link')) {
                var label = $(this).find('.c-expandcollapse__title').text();
                if($(this).parents('.c-expandcollapse__header').is('.expanded')){
                    trackVirtualAgentResults($(this), label);
                }
            } else {
                trackVirtualAgentResults($(this), label);
            }
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            ev.category = {'primaryCategory':ctConstants.custom};
        });

        //Input error Tracking
        $(document).on('click', 'input[type="submit"], button[type="submit"]', function() {
            if ($('body').hasClass('profile-page') || $('body').hasClass('registration-page') || $('body').hasClass('login-page')) {
                var $hasError = $('.has-error');

                setTimeout(function() {
                    if ($hasError.length) {
                        var ev = {},
                            errorlog = [],
                            $obj = $hasError.parents('form'),
                            errorvalue;

                        $('.has-error').each(function(i, value) {
                           errorlog.push($(this).find('.help-block').text());
                        });

                        errorvalue = errorlog.join(" | ");

                        //track component info
                        trackComponentInfo($obj);

                        ev.eventInfo = {
                          'type': ctConstants.trackEvent,
                          'eventAction': ctConstants.inputerror,
                          'eventLabel' : errorvalue
                        };
                        ev.category ={'primaryCategory':ctConstants.custom};
                        ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
                        digitalData.event.push(ev);
                    }
                }, 500);
            }
        });

        //Iframe Tracking
        var $cIframe = $('.c-iframe');

        if ($cIframe.length) {
            var ev = {},
                $iframeContainer = $iframe.find('iframe').attr('name');

           //track component info
            trackComponentInfo($cIframe);

            ev.eventInfo = {
              'type':ctConstants.trackEvent,
              'eventAction': ctConstants.iFrame,
              'eventLabel' :  $iframeContainer  + ' - Loaded'
            };
            ev.category = {'primaryCategory':ctConstants.custom};
            ev.attributes = {'nonInteractive':{'nonInteraction': 0}};
            digitalData.event.push(ev);
        }

        /* Recipe listing analytics tracking */
        /*Filters tracking*/
        $(document).on('change', ".c-recipe-listing-filter__radio, .c-recipe-listing-filter__checkbox,.c-recipe-listing-filter__dropdown", function () {
             var ev = {};
            if(!($('.c-recipe-listing-filter').find('.c-recipe-listing-filter__expand').is(':visible'))) {
                var $this = $(this),
                            filterType = $this.data('filter-type'),
                            selectedVal = '';

                        switch (filterType) {
                            case 'dropdown':
                                selectedVal = $this.find('option:selected').val();
                                break;

                            case 'radioButton':
                            case 'checkBox':
                                selectedVal = ($this.is(':checked')) ? $this.val() : '';
                                break;
                            }

                var compName = $(this).parents('[data-componentname]').data('componentname'),
                    compVariants = $(this).parents('[data-component-variants]').data('component-variants'),
                    compPositions = $(this).parents('[data-component-positions]').data('component-positions'),
                    ev = {};
                // track component info
                trackComponentInfo($this);

                ev.eventInfo={
                  'type':ctConstants.trackEvent,
                  'eventAction': ctConstants.filter,
                  'eventLabel' : selectedVal
                };
                ev.category ={'primaryCategory':ctConstants.custom};
                digitalData.event.push(ev);
            }
        });


    }
})(window, document, jQuery);