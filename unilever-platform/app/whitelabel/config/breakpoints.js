// This config is shared amongst front-end (IEA) and back-end (AEM)


// Image dimensions overwrites for the adaptive retina helper

var adaptiveRetina = {

        'heroV2': {
            'img01' : {
                dimensions: '1330,752,670,381,630,350,385,221',
                isLazyLoad: false
            },

            'img02' : {
                dimensions: '2058,696,1209,572,778,475,385,385',
                isLazyLoad: false
            },

            'videoPreviewImage01' : {
                dimensions: '1330,752,670,381,630,350,385,221',
                isLazyLoad: false
            },

            'videoPreviewImage02' : {
                dimensions: '2058,696,1209,572,778,475,385,385',
                isLazyLoad: false
            }
        },

        'homeStory': {
            'centreImage' : {
                dimensions: '1546,835,1546,835,1497,808,580,316',
                isLazyLoad: true
            },

            'leftImage' : {
                dimensions: '410,410,410,410,310,310,210,210',
                isLazyLoad: false
            },

            'rightImage' : {
                dimensions: '410,410,410,410,310,310,210,210',
                isLazyLoad: true
            }
        },

        'recipeHero' : {
            'recipeImage': {
                dimensions: '410,410,410,410,310,310,210,210',
                isLazyLoad: true
            }
        }
    },

    BREAKPOINTS = {
        desktopMin : 1200,
        tabLMin : 992,
        tabLMax : 1199,
        tabPMin : 768,
        tabPMax : 991,
        mobMin : 480,
        mobMax : 767   
    };