module.exports = function(grunt, options) {
    return {
        files: {
        	dest: '<%= config.dist %>/css/main-core.css',
        	src: ['<%= config.dist %>/css/iea.css', '<%= config.dist %>/css/main-<%= config.parentAppName %>.css', '.tmp/css/main.css']
        }
    };
}
