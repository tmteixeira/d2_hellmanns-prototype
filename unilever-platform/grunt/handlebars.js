module.exports = function(grunt, options) {
    var parentHelpers = options.config.template.parentHelpers,
        tenantHelpers = options.config.template.helpers,
        appAmd,tenantAmd;
    // ['handlebars', '<helpers1>', '<helpers2>']
    function handlebarsAmd(helpersObj) {
        var amdArray = ['handlebars'];
        for (var key in helpersObj) {
            if (helpersObj.hasOwnProperty(key)) {
                amdArray.push(helpersObj[key]);
            }
        }
        return amdArray;
    }

    function getComponentList(task) {
        var clientSideComponents = (options.config.template.clientSideComponents === undefined) ? options.config.template.path : options.config.template.clientSideComponents,
            confComponent = (task === 'appBuild' || task === 'tenantBuild') ? clientSideComponents : options.config.template.path,
            componentArray = [], templatePath = '',
            ns='', dependency = options.config.componentDependency;

        var loadPath = function(confComponent) {
            for (var key in confComponent) {
                if (confComponent.hasOwnProperty(key)) {
                    templatePath = confComponent[key];
                    if (typeof templatePath === 'object') {
                        loadPath(templatePath);
                    }
                    ns = (templatePath.split('/')[0] === 'lib') ? templatePath.split('/')[0] : templatePath.split('/')[1];
                    if (task === 'app' && ns === options.config.parentAppName) {
                        componentArray.push(options.config.parentDir+'/js/templates/'+key+'/**/*.hbss');
                    } else if (task === 'tenant' && ns === options.config.name) {
                        componentArray.push(options.config.appBasePath+'/js/templates/'+key+'/**/*.hbss');
                    } else if (task === 'iea' && ns === 'lib') {
                        componentArray.push(options.config.lib+'/js/templates/'+key+'/**/*.hbss');
                    } else if (task === 'appBuild' && ns === options.config.parentAppName) {
                        componentArray.push(templatePath + key + '/**/*.hbss');
                    } else if (task === 'tenantBuild' && ns === options.config.name) {
                        componentArray.push(options.config.appBasePath+'/js/templates/'+key+'/**/*.hbss');
                    }


                }
            }
        };
        loadPath(confComponent);

        /*Fallback hbss file for scenario where no component is selected. Grunt was not generating empty template file*/
        if(componentArray.length === 0) {
            var filepath = options.config.appBasePath + '/js/libs/iea/placeholder.hbss';
            grunt.file.write(filepath, '/*placeholder*/');
            componentArray.push(filepath);
        }
        return componentArray;
    }

    appAmd = handlebarsAmd(parentHelpers);
    tenantAmd = handlebarsAmd(tenantHelpers);

    return {
        app: {
            options: {
                templateSettings: {
                    variable: 'data'
                },
                namespace: function(filePath) {
                    //var pieces = filePath.split('/');
                    //return pieces[0];
                    return (options.config.template && options.config.template.parentNamespace) ? options.config.template.parentNamespace : options.config.parentAppName;
                },
                amd: appAmd
            },
            files: {
                '<%= config.appBasePath %>/js/libs/iea/core/js/<%= config.parentAppName %>.templates.js' : getComponentList('app')
            }
        },

        appBuild: {
            options: {
                templateSettings: {
                    variable: 'data'
                },
                namespace: function(filePath) {
                    //var pieces = filePath.split('/');
                    //return pieces[0];
                    return (options.config.template && options.config.template.parentNamespace) ? options.config.template.parentNamespace : options.config.parentAppName;
                },
                amd: appAmd
            },
            files: {
                '<%= config.appBasePath %>/js/libs/iea/core/js/<%= config.parentAppName %>.templates.js' : getComponentList('appBuild')
            }
        },

        tenantBuild: {
            options: {
                templateSettings: {
                    variable: 'data'
                },
                namespace: function(filePath) {
                    return (options.config.template && options.config.template.namespace) ? options.config.template.namespace : options.config.name;
                },
                amd: tenantAmd
            },
            files: {
                '.tmp/js/<%= config.name %>.templates.js': getComponentList('tenantBuild')
            }
        },

        tenant: {
            options: {
                templateSettings: {
                    variable: 'data'
                },
                namespace: function(filePath) {
                    //var pieces = filePath.split('/');
                    //return pieces[0];
                    return (options.config.template && options.config.template.namespace) ? options.config.template.namespace : options.config.name;
                },
                amd: tenantAmd
            },
            files: {
                '.tmp/js/<%= config.name %>.templates.js': getComponentList('tenant')
            }
        },

        iea: {
            options: {
                templateSettings: {
                    variable: 'data'
                },
                namespace: function(filePath) {
                    var pieces = filePath.split('/');
                    return pieces[0];
                },
                amd: ['handlebars','helpers']
            },
            files: {
                '<%= config.appBasePath %>/js/libs/iea/core/js/iea.templates.js': getComponentList('iea')
            }
        }
    };
}
