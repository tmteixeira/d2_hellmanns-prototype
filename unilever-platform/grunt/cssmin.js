module.exports = function(grunt, options) {
    return {
        cssmin: {
            files: {
                '<%= config.dist %>/css/main-min.css': '<%= config.dist %>/css/main-core.css'
            }
        }
    };
}