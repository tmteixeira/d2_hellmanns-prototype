module.exports = function(grunt, options) {
    return {
        iea: {
            files: [{
                expand: true,
                cwd: '<%= config.lib %>/images',
                src: '{,*/}*.{png,jpg,jpeg,gif}',
                dest: '<%= config.appBasePath %>/js/libs/iea/core/images'
            }]
        },
        dist: {
            files: [{
                expand: true,
                cwd: '<%= config.parentDir %>/images',
                src: '{,*/}*.{png,jpg,jpeg,gif}',
                dest: '<%= config.dist %>/images'
            },
            {
                expand: true,
                cwd: '<%= config.appBasePath %>/images',
                src: '{,*/}*.{png,jpg,jpeg,gif}',
                dest: '<%= config.dist %>/images'
            }]
        }
    };
}
