module.exports = function(grunt, options) {
    return {
        bless: {
            files: {
                '<%= config.dist %>/css/main.css': '<%= config.dist %>/css/main-min.css'
            }
        }
    };
}
