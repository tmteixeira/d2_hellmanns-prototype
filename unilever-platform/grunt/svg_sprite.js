module.exports = function(grunt, options) {
    return {

            symbol: {
                // Target basics
                expand: true,
                cwd: '<%= config.appBasePath %>/svgs/sprites',
                src: ['*.svg'],
                dest: '<%= config.appBasePath %>/svgs',
                // Target options
                options: {
                    shape: {
                        dimension: { // Set maximum dimensions
                            maxWidth: 8,
                            maxHeight: 8
                        }
                    },
                    mode: {
                        inline: true,
                        symbol: true // Activate the «symbol» mode
                    }
                }
            }

    };
}
