module.exports = function(grunt, options) {
    var watchApp, watchTenant;

    // All lib and teanant related changes goes here
    watchApp = {
        options: {
            nospawn: true,
            livereload: true
        },
        //gruntchange: {
        //    files: ['Gruntfile.js','grunt/**/*.js'],
        //    tasks: ['jshint', 'template:app', 'compass:app']
        //},
/**************************************IEA WATCH*******************************************************/
        ieacore: {
            files: ['<%= config.lib %>/core/{,*/}*.js'],
            tasks: ['jshint','preprocess:iea', 'requirejs:iea']
        },

        ieacomp: {
            files: ['<%= config.lib %>/js/**/*.js'],
            tasks: ['jshint', 'loadIEAComponents', 'requirejs:ieac']
        },

        compassLib: {
            files: ['<%= config.lib %>/sass/**/*.{scss,sass}'],
            tasks: ['template:iea', 'compass:iea']
        },

        ieahbss: {
            files: ['<%= config.lib %>/js/templates/**/*.hbss'],
            tasks: ['handlebars:iea', 'loadIEAComponents', 'requirejs:ieac']
        },

        livereloadlib: {
            options: {
                livereload: grunt.option('LIVERELOAD_PORT')
            },
            files: [
                '<%= config.lib %>/css/{,*/}*.css',
                '<%= config.lib %>/js/{,*/}*.js',
                '<%= config.lib %>/images/{,*/}*.{png,jpg,jpeg,gif,webp}',
                '<%= config.lib %>/js/templates/*.{ejs,mustache,hbs,hbss}'
            ]
        },

        /*****************************************************************************************/

        livereloadten: {
            options: {
                livereload: grunt.option('LIVERELOAD_PORT')
            },
            files: [
                '<%= config.appBasePath %>/*.html',
                '<%= config.appBasePath %>/css/{,*/}*.css',
                '<%= config.appBasePath %>/js/{,*/}*.js',
                '<%= config.appBasePath %>/images/{,*/}*.{png,jpg,jpeg,gif,webp}',
                '<%= config.appBasePath %>/js/templates/*.{ejs,mustache,hbs,hbss}',
            ]
        },

        tenjs: {
            files: ['<%= config.appBasePath %>/js/**/*.js','config/*.js'],
            tasks: ['jshint', 'loadTenantComponents', 'requirejs:tenc']
        },

        tenApp: {
            files: ['<%= config.appBasePath %>/sass/**/*.{scss,sass}'],
            tasks: ['template:tenant', 'compass:tenant']
        },

        tenhbss: {
            files: ['<%= config.appBasePath %>/js/templates/**/*.hbss'],
            tasks: ['handlebars:tenant', 'loadTenantComponents', 'requirejs:tenc']
        }
    };

    // App level changes goes here
    watchTenant = {
        /********************************APP WATCH*************************************************************/

        livereloadapp: {
            options: {
                livereload: grunt.option('LIVERELOAD_PORT')
            },
            files: [
                '<%= config.parentDir %>/*.html',
                '<%= config.parentDir %>/css/{,*/}*.css',
                '<%= config.parentDir %>/js/{,*/}*.js',
                '<%= config.parentDir %>/images/{,*/}*.{png,jpg,jpeg,gif,webp}',
                '<%= config.parentDir %>/js/templates/*.{ejs,mustache,hbs,hbss}',
            ]
        },

        appjs: {
            files: ['<%= config.parentDir %>/js/**/*.js','config/*.js'],
            tasks: ['jshint', 'loadAPPComponents', 'requirejs:appc']
        },

        compassApp: {
            files: ['<%= config.parentDir %>/sass/**/*.{scss,sass}'],
            tasks: ['template:app', 'compass:app']
        },

        apphbss: {
            files: ['<%= config.parentDir %>/js/templates/**/*.hbss'],
            tasks: ['handlebars:app', 'loadAPPComponents', 'requirejs:appc']
        },
    };

    // Get all the set of commands from watchApp to watchTenant
    for (var key in watchApp) {
        if (watchApp.hasOwnProperty(key)) {
            watchTenant[key] = watchApp[key];
        }
    }

    if(options.config.name === options.config.parentAppName) {
        return watchApp;
    } else {
        return watchTenant;
    }
};
