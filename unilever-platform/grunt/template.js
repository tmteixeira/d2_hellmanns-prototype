module.exports = function(grunt, options) {
    return {
        options: {
            data: {
                version: grunt.option('version'),
                name: '<%= config.name %>',
                parentName: '<%= config.parentAppName %>',
                theme: '<%= config.theme %>',
                parentTheme: '<%= config.parentTheme %>',
                appBasePath: '<%= config.appBasePath %>',
                fontPath: '<%= config.dev.fontPath%>',
                imagePath: '<%= config.dev.imagePath%>',
                templatePath: '<%= config.template.path %>',
                breakpointSmall: '<%= config.breakpoints.deviceSmall%>',
                breakpointMedium: '<%= config.breakpoints.deviceMedium%>',
                breakpointLarge: '<%= config.breakpoints.deviceLarge%>',
                breakpointXlarge: '<%= config.breakpoints.deviceXlarge%>',

                containerSmall: '<%= config.breakpoints.containerSmall%>',
                containerMedium: '<%= config.breakpoints.containerMedium%>',
                containerLarge: '<%= config.breakpoints.containerLarge%>',
                containerXLarge: '<%= config.breakpoints.containerXLarge%>',
            }
        },
        iea: {
            'files': {
                '<%= preprocess.iea.dest %>': ['<%= preprocess.iea.dest %>'],
                '<%= config.lib %>/sass/iea.scss': ['<%= config.lib %>/sass/_base.scss']
            }
        },
        app: {
            'files': {
                '<%= config.parentDir %>/sass/_<%= config.parentTheme%>.scss': ['<%= config.parentDir %>/sass/_base.scss'],
                '<%= config.parentDir %>/sass/main-<%= config.parentAppName %>.scss': ['<%= config.parentDir %>/sass/_main.scss']
            }
        },
        tenant: {
            'files': {
                '<%= config.appBasePath %>/sass/_<%= config.theme%>.scss': ['<%= config.appBasePath %>/sass/_base.scss'],
                '<%= config.appBasePath %>/sass/main.scss': ['<%= config.appBasePath %>/sass/_main.scss']
            }
        },
        source: {
            options: {
                data: {
                    version: grunt.option('version'),
                    name: '<%= config.name %>',
                    parentName: '<%= config.parentAppName %>',
                    theme: '<%= config.theme%>',
                    parentTheme: '<%= config.parentTheme %>',
                    fontPath: '<%= config.source.fontPath%>',
                    imagePath: '<%= config.source.imagePath%>',
                    templatePath: '<%= config.template.path %>',
                    breakpointSmall: '<%= config.breakpoints.deviceSmall%>',
                    breakpointMedium: '<%= config.breakpoints.deviceMedium%>',
                    breakpointLarge: '<%= config.breakpoints.deviceLarge%>',
                    breakpointXlarge: '<%= config.breakpoints.deviceXlarge%>',

                    containerSmall: '<%= config.breakpoints.containerSmall%>',
                    containerMedium: '<%= config.breakpoints.containerMedium%>',
                    containerLarge: '<%= config.breakpoints.containerLarge%>',
                    containerXLarge: '<%= config.breakpoints.containerXLarge%>',
                }
            },
            'files': {
                '<%= preprocess.iea.dest %>': ['<%= preprocess.iea.dest %>'],
                '<%= config.parentDir %>/sass/_<%= config.parentTheme%>.scss': ['<%= config.parentDir %>/sass/_base.scss'],
                '<%= config.parentDir %>/sass/<%= config.parentAppName %>.scss': ['<%= config.parentDir %>/sass/_main.scss'],
                '<%= config.appBasePath %>/sass/_<%= config.theme%>.scss': ['<%= config.appBasePath %>/sass/_base.scss'],
                '<%= config.appBasePath %>/sass/main.scss': ['<%= config.appBasePath %>/sass/_main.scss'],
                '<%= config.lib %>/sass/iea.scss': ['<%= config.lib %>/sass/_base.scss']
            }
        },
        cq: {
            options: {
                data: {
                    version: grunt.option('version'),
                    name: '<%= config.name %>',
                    parentName: '<%= config.parentAppName %>',
                    theme: '<%= config.theme%>',
                    parentTheme: '<%= config.parentTheme %>',
                    fontPath: '<%= config.cq.fontPath%>',
                    imagePath: '<%= config.cq.imagePath%>',
                    templatePath: '<%= config.template.path %>',
                    breakpointSmall: '<%= config.breakpoints.deviceSmall%>',
                    breakpointMedium: '<%= config.breakpoints.deviceMedium%>',
                    breakpointLarge: '<%= config.breakpoints.deviceLarge%>',
                    breakpointXlarge: '<%= config.breakpoints.deviceXlarge%>',

                    containerSmall: '<%= config.breakpoints.containerSmall%>',
                    containerMedium: '<%= config.breakpoints.containerMedium%>',
                    containerLarge: '<%= config.breakpoints.containerLarge%>',
                    containerXLarge: '<%= config.breakpoints.containerXLarge%>',
                }
            },
            'files': {
                '<%= preprocess.iea.dest %>': ['<%= preprocess.iea.dest %>'],
                '<%= config.parentDir %>/sass/_<%= config.parentTheme%>.scss': ['<%= config.parentDir %>/sass/_base.scss'],
                '<%= config.parentDir %>/sass/<%= config.parentAppName %>.scss': ['<%= config.parentDir %>/sass/_main.scss'],
                '<%= config.appBasePath %>/sass/_<%= config.theme%>.scss': ['<%= config.appBasePath %>/sass/_base.scss'],
                '<%= config.appBasePath %>/sass/main.scss': ['<%= config.appBasePath %>/sass/_main.scss'],
                '<%= config.lib %>/sass/iea.scss': ['<%= config.lib %>/sass/_base.scss']
            }
        }
    };
}
