module.exports = function(grunt, options) {
	return {

		symbol: {
			options: {
				spriteElementPath: "<%= config.appBasePath %>/svgs/sprites",
				spritePath: "<%= config.appBasePath %>/svgs/sprites/img",
				cssPath: "<%= config.appBasePath %>/svgs/sprites/css/svg-sprite.css"

			},
			shapes: {
				options: {
					// more options
				}
			}

		}

	};
}