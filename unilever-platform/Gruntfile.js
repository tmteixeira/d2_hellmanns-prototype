/*jshint -W079 */

'use strict';

var LIVERELOAD_PORT = 35729,
    SERVER_PORT = 9000,
    fs = require('fs'),
    _ = require('underscore'),
    IEAComponents = [],
    APPComponents = [],
    TenantComponents= [],
    pkg,
    config = [],
    libModules,
    appModules,
    tenantModules,
    appName,
    regex = /(^|.\/)\.+[^\/\.]/g;

module.exports = function(grunt) {
    appName = grunt.option('appName');

    config = require('./config/'+appName+'-config.js');

    // package specific configuration
    pkg = grunt.file.readJSON('package.json');

    // in case of tenant application config fallback scenarios
    config.parentDir = config.parentDir || config.appBasePath;
    config.parentAppName = config.parentAppName || config.name;
    config.parentTheme = config.parentTheme || config.theme;
    config.template.parentNamespace = config.template.parentNamespace || config.template.namespace;

    //read all the view files from iea and app
    libModules = fs.readdirSync(config.lib + '/js/views/');
    appModules = fs.readdirSync(config.parentDir + '/js/views/');
    tenantModules = fs.readdirSync(config.appBasePath + '/js/views');

    // create array of files which needs to be loaded
    var buildComp = config.template.path;

    _.each(libModules, function(model) {
        var str = model.replace('.js', '');

        if(!regex.test(str) && buildComp.hasOwnProperty(str)) {
            IEAComponents.push('views/' + str + '/_' + str);
        }
    });
    IEAComponents.push('iea.templates');

    // create array of files which needs to be loaded
    _.each(appModules, function(model) {
        var str = model.replace('.js', '');

        if(!regex.test(str) && buildComp.hasOwnProperty(str)) {
            APPComponents.push('../../../'+config.parentDir + '/js/views/' + str + '/' + str);
        }
    });
    APPComponents.push(config.parentAppName+'.templates');

    // create array of files which needs to be loaded
    _.each(tenantModules, function(model) {
        var str = model.replace('.js', '');

        if(!regex.test(str) && buildComp.hasOwnProperty(str)) {
            TenantComponents.push('../../../'+config.appBasePath + '/js/views/' + str + '/' + str);
        }

    });
    TenantComponents.push(config.name+'.templates');

    // grunt basic config values
    grunt.option('LIVERELOAD_PORT', LIVERELOAD_PORT);
    grunt.option('port', SERVER_PORT);
    grunt.option('host', '0.0.0.0'); // change this to '0.0.0.0' to access the server from outside
    grunt.option('config', config);
    grunt.option('version', pkg.version);

    require('load-grunt-config')(grunt, {
        //data passed into config.  Can use with <%= test %>
        data: {
            LIVERELOAD_PORT: LIVERELOAD_PORT,
            port: SERVER_PORT,
            config: config,
            pkg: pkg,
            host: '0.0.0.0'
        }
    });

    // create AMD list of all the iea components
    grunt.registerTask('loadIEAComponents', function() {
        grunt.file.write(config.appBasePath + '/js/libs/iea/core/js/iea.components.js', 'define(["' + IEAComponents.join('","') + '"],function(){return Array.prototype.slice.call(arguments);});');
    });

    // create AMD list of all the app components
    grunt.registerTask('loadAPPComponents', function() {
        grunt.file.write(config.appBasePath + '/js/libs/iea/core/js/'+config.parentAppName+'.components.js', 'define(["' + APPComponents.join('","') + '"],function(){return Array.prototype.slice.call(arguments);});');
    });

    // create AMD list of all the app components
    grunt.registerTask('loadTenantComponents', function() {
        grunt.file.write('.tmp/js/'+ config.name+ '.components.js', 'define(["' + TenantComponents.join('","') + '"],function(){return Array.prototype.slice.call(arguments);});');
    });

    // 'grunt server' used to simply call `grunt serve` but now we just stop and let the user know why.
    grunt.registerTask('server', function(target) {
        grunt.fail.fatal('The `server` task has been deprecated. Use `grunt serve` instead of `grunt server`.');
    });

    /*******************************************************************************************************/
    /******************************************IEA AND APP START********************************************/
    /*******************************************************************************************************/

    grunt.registerTask('serve', function(target) {

        // 'grunt serve' by default will run 'grunt serve:all' which kickstarts the application
        if (!target) {
            target = fs.existsSync(config.appBasePath + '/js/libs/iea/core') ? 'tenant' : 'all';
        }

        /*******************************IEA SPECIFIC TASKS**********************************************/

        if (target === 'all') {
            if(config.name === config.parentAppName) {
                return grunt.task.run([
                    'serve:iea',
                    'serve:tenant'
                ]);
            } else {
                return grunt.task.run([
                    'serve:iea',
                    'serve:app',
                    'serve:tenant'
                ]);
            }
        }

        if (target === 'iea') {
            return grunt.task.run([
                'env:dev',
                'jshint',
                'clean:iea',
                'loadIEAComponents',
                'handlebars:iea',
                'preprocess:iea',
                'template:iea',
                'requirejs:iea',
                'requirejs:ieac',
                'copy:iea',
                'compass:iea',
                'imagemin:iea',
                'clean:iearesidue'
            ]);
        }

        /***********************************APPLICATION SPECIFIC GRUNT TASK********************************/

        if (target === 'app') {
            return grunt.task.run([
                'jshint',
                'clean:app',
                'loadAPPComponents',
                'handlebars:app',
                'template:app',
                'requirejs:appc',
                'compass:app',
                'clean:appresidue',
                'svg_sprite:symbol',
                'dr-svg-sprites'
            ]);
        }

        if (target === 'appBuild') {
            return grunt.task.run([
                'jshint',
                'clean:app',
                'loadAPPComponents',
                'handlebars:appBuild',
                'template:app',
                'requirejs:appc',
                'compass:app',
                'clean:appresidue'
            ]);
        }

        if (target === 'tenantBuild') {
            return grunt.task.run([
                'jshint',
                'clean:tenant',
                'loadTenantComponents',
                'handlebars:tenantBuild',
                'template:tenant',
                'requirejs:tenc',
                'compass:tenant',
                'server:start'
            ]);
        }

        if (target === 'tenant') {
            return grunt.task.run([
                'jshint',
                'clean:tenant',
                'loadTenantComponents',
                'handlebars:tenant',
                'template:tenant',
                'requirejs:tenc',
                'compass:tenant',
                'serve:start'
            ]);
        }

        if (target === 'start') {
            return grunt.task.run([
                'connect:livereload',
                'open:server',
                'watch'
            ]);
        }

        if (target === 'server') {
            return grunt.task.run([
                'serve:start'
            ]);
        }
    });

    /*******************************************************************************************************/
    /******************************************BUILD RELATED GRUNT TASKS************************************/
    /*******************************************************************************************************/

    grunt.registerTask('build', function(type) {
        if (type === 'cq') {
            if(config.name === config.parentAppName) {
                return grunt.task.run([
                    'serve:iea',
                    'defaultBuild',
                    'copy:cq',
                    'clean:cq'
                ]);
            } else {
                return grunt.task.run([
                    'serve:iea',
                    'serve:appBuild',
                    'defaultBuild',
                    'copy:cq',
                    'clean:cq'
                ]);
            }
        }

        grunt.task.run([
            'default'
        ]);
    });

    grunt.registerTask('defaultBuild', [
        'env:build',
        'clean:dist',
        'clean:tenant',
        'loadTenantComponents',
        'svg_sprite',
        'handlebars:tenantBuild',
        'template:tenant',
        'requirejs:tenc',
        'requirejs:dist',
        'compass:tenant',
        'imagemin:dist',
        'useminPrepare',
        'htmlmin',
        'copy:def',
        'usemin',
        'concat',
        'concat_css',
        'cssmin:cssmin',
        //'uglify',
        'bless:bless',
        'clean:distresidue'
    ]);

    grunt.registerTask('default', [
        'env:build',
        'clean:dist',
        'clean:tenant',
        'loadTenantComponents',
        'svg_sprite',
        'handlebars:tenant',
        'template:tenant',
        'requirejs:tenc',
        'requirejs:dist',
        'compass:tenant',
        'imagemin:dist',
        'useminPrepare',
        'htmlmin',
        'copy:def',
        'usemin',
        'concat',
        'concat_css',
        'cssmin:cssmin',
        //'uglify',
        'bless:bless',
        'clean:distresidue'
    ]);

};
