!function (e, t) {
    "function" == typeof define && define.amd ? define(t) : "object" == typeof exports ? module.exports = t : e.configObj = t
}(this, {
    'name': 'whitelabel', // IEA tenant/sub-tenant application namespace
    'version' : '2.4.1', // App version number
    'theme': 'whitelabel', // Application theme
    'lib': 'lib', //IEA; should not be modified
    'parentDir': 'app/unilever-iea', //IEA tenant folder for sub-tenant; same in case of tenant application
    'parentAppName': 'unilever-iea', //IEA tenant appName for sub-tenant; same in case of tenant application
    'parentTheme': 'whitelabel', //IEA tenant theme for sub-tenant; same in case of tenant application
    'dist': 'dist', //IEA; should not be modified
    'appBasePath': 'app/whitelabel', //IEA tenant/sub-tenant base path

    // debug settings(true | false | {}) (app.config.getDebugSetting())
    // environment specific setting will override default setting on load.
    'debug': true,

    'dependencies': ['unilever-iea.components', 'whitelabel.components'], //component dependency
    'defaultTemplateName': ['landingView', 'defaultView'],
    'templatePath': 'app/whitelabel/js/templates/',
    'dev': {
        'css': 'css/',
        'fonts': 'css/fonts/',
        'js': 'js/',
        'images': 'images/',
        'svgs': 'svgs/',
        'config': 'config/',
        'templates': 'js/templates',
        'fontPath': 'fonts/',
        'imagePath': '../images/'
    },
    'cq': {
        'css': 'dist/etc/ui/whitelabel/clientlibs/core/core/css',
        'fonts': 'dist/etc/ui/whitelabel/clientlibs/core/core/css/fonts',
        'js': 'dist/etc/ui/whitelabel/clientlibs/core/require/js',
        'modjs': 'dist/etc/ui/whitelabel/clientlibs/core/core/js',
        'images': 'dist/etc/ui/whitelabel/clientlibs/core/core/images',
        'svgs': 'dist/etc/ui/whitelabel/clientlibs/core/core/svgs',
        'config': 'dist/etc/ui/whitelabel/clientlibs/core/core/config',
        'templates': 'dist/etc/ui/whitelabel/templates/components',
        'fontPath': '../fonts/',
        'imagePath': '../images/'
    },

    // breakpoints configuration
    'breakpoints': {
        'deviceSmall': '320px',
        'deviceMedium': '768px',
        'deviceLarge': '992px',
        'deviceXlarge': '1200px',

        'containerSmall': 'auto',
        'containerMedium': 'auto',
        'containerLarge': '1024px',
        'containerXLarge': '1200px'
    },

    'build': {
        'minified': true,
        'combined': true
    },

    // application template and engine settings (app.config.getTemplateSetting())
    'template': {
        'namespace': 'whitelabel',
        'parentNamespace': 'unilever-iea',
        'parentHelpers': ['unileverHelpers'],
        //'clientSideComponents': {}, //It should be enabled by brand for selective ".hbss" pre-compile
        'path': {
            'accordion': 'app/unilever-iea/js/templates/',
            'accordion-v2': 'app/unilever-iea/js/templates/',
            'ad-choices': 'app/unilever-iea/js/templates/',
            'adaptive-image': 'app/unilever-iea/js/templates/',
            'adaptive-image-v2': 'app/unilever-iea/js/templates/',
            'anchor-link-navigation': 'app/unilever-iea/js/templates/',
            'anchor-link-navigation-v2': 'app/unilever-iea/js/templates/',
            'animations': 'app/unilever-iea/js/templates/',
            'article-listing': 'app/unilever-iea/js/templates/',
            'back-to-top': 'app/unilever-iea/js/templates/',
            'back-to-top-cta': 'app/unilever-iea/js/templates/',
            'body-copy': 'app/unilever-iea/js/templates/',
            'body-copy-headline': 'app/unilever-iea/js/templates/',
            'body-copy-steps': 'app/unilever-iea/js/templates/',
            'brand-social-channels': 'app/unilever-iea/js/templates/',
            'breadcrumb': 'app/unilever-iea/js/templates/',
            'buy-in-store-search': {
                'buy-in-store-search': 'app/unilever-iea/js/templates/',
                'buy-in-store-search-info': 'app/unilever-iea/js/templates/'
            },
            'buy-in-store-search-result': {
                'buy-in-store-search-list': 'app/unilever-iea/js/templates/',
                'buy-in-store-search-result': 'app/unilever-iea/js/templates/'
            },
            'buy-it-now': 'app/unilever-iea/js/templates/',
            'call-out-button': 'app/unilever-iea/js/templates/',
            'call-to-action': 'app/unilever-iea/js/templates/',
            'campaign-offer-flow-widget': 'app/unilever-iea/js/templates/',
            'carousel': 'app/unilever-iea/js/templates/',
            'carousel-v2': 'app/unilever-iea/js/templates/',
            'category-overview': 'app/unilever-iea/js/templates/',
            'category-reviews': 'app/unilever-iea/js/templates/',
            'checkout-form':' app/unilever-iea/js/templates/',
            'checkout-shoppable': 'app/unilever-iea/js/templates/',
            'cleanipedia-related-content': {
                'cleanipedia-related-content':'app/unilever-iea/js/templates/',
                'paired-content':'app/unilever-iea/js/templates/',
                'related-content': 'app/unilever-iea/js/templates/'
            },
            'community-product-qna-overview': 'app/unilever-iea/js/templates/',
            'content-results-grid': {
                'content-result-grid-article': 'app/unilever-iea/js/templates/',
                'content-results-grid': 'app/unilever-iea/js/templates/'
            },
            'country-language-selector': 'app/unilever-iea/js/templates/',
            'country-selector': 'app/unilever-iea/js/templates/',
            'diagnostic-tool': {
                'diagnostic-tool': 'app/unilever-iea/js/templates/'
            },
            'diagnostic-tool-question': {
                'diagnostic-tool-question': 'app/unilever-iea/js/templates/',
                'multiple-choice-multiple-answer': 'app/unilever-iea/js/templates/',
                'multiple-choice-single-answer': 'app/unilever-iea/js/templates/',
                'section-divider': 'app/unilever-iea/js/templates/',
                'single-choice-scale': 'app/unilever-iea/js/templates/',
                'single-choice-yes-no': 'app/unilever-iea/js/templates/'
            },
            'diagnostic-tool-result-section': {
                'diagnostic-tool-article': 'app/unilever-iea/js/templates/',
                'diagnostic-tool-email-view' : 'app/unilever-iea/js/templates/',
                'diagnostic-tool-image': 'app/unilever-iea/js/templates/',
                'diagnostic-tool-product': 'app/unilever-iea/js/templates/',
                'diagnostic-tool-products-summary': 'app/unilever-iea/js/templates/',
                'diagnostic-tool-single-product': 'app/unilever-iea/js/templates/',
                'diagnostic-tool-social-sharing': 'app/unilever-iea/js/templates/',
                'diagnostic-tool-text': 'app/unilever-iea/js/templates/',
                'diagnostic-tool-text-articles-view' : 'app/unilever-iea/js/templates/',
                'diagnostic-tool-text-image-view' : 'app/unilever-iea/js/templates/',
                'diagnostic-tool-text-products-articles-view' : 'app/unilever-iea/js/templates/',
                'diagnostic-tool-text-products-view' : 'app/unilever-iea/js/templates/'
            },
            'dynamic-error-message': 'app/unilever-iea/js/templates/',
            'dynamic-page-properties': 'app/unilever-iea/js/templates/',
            'e-gifting-configurator': 'app/unilever-iea/js/templates/',
            'end': 'app/unilever-iea/js/templates/',
            'end-section': 'app/unilever-iea/js/templates/',
            'expand-collapse': 'app/unilever-iea/js/templates/',
            'featured-article': {
                'featured-article': 'app/unilever-iea/js/templates/',
                'featured-article-image-small': 'app/unilever-iea/js/templates/',
                'featured-article-image-wide': 'app/unilever-iea/js/templates/'
            },
            'featured-content': 'app/unilever-iea/js/templates/',
            'featured-instagram-post': 'app/unilever-iea/js/templates/',
            'featured-instagram-post-TAB': 'app/unilever-iea/js/templates/',
            'featured-product': 'app/unilever-iea/js/templates/',
            'featured-product-range': {
                'featured-product-range': 'app/unilever-iea/js/templates/',
                'featured-product-range-banner': 'app/unilever-iea/js/templates/',
                'featured-product-range-banner-fluid': 'app/unilever-iea/js/templates/',
                'featured-product-range-lifestyle': 'app/unilever-iea/js/templates/'
            },
            'featured-product-v2': 'app/unilever-iea/js/templates/',
            'featured-quote': {
                'featured-quote': 'app/unilever-iea/js/templates/',
                'featured-quote-center': 'app/unilever-iea/js/templates/',
                'featured-quote-left': 'app/unilever-iea/js/templates/'
            },
            'featured-social-topic': 'app/unilever-iea/js/templates/',
            'featured-tweet-TAB': {
                'featured-tweet-TAB':'app/unilever-iea/js/templates/',
                'featured-tweet-single':'app/unilever-iea/js/templates/'
            },
            'featured-twitter-post': 'app/unilever-iea/js/templates/',
            'flyOutMenu': 'app/unilever-iea/js/templates/',
            'footer': 'app/unilever-iea/js/templates/',
            'form': 'app/unilever-iea/js/templates/',
            'form-element':{
                'button':'app/unilever-iea/js/templates/',
                'calender':'app/unilever-iea/js/templates/',
                'captcha':'app/unilever-iea/js/templates/',
                'dropdown':'app/unilever-iea/js/templates/',
                'link':'app/unilever-iea/js/templates/',
                'logintext':'app/unilever-iea/js/templates/',
                'radio':'app/unilever-iea/js/templates/',
                'text':'app/unilever-iea/js/templates/',
                'textarea':'app/unilever-iea/js/templates/'
            },
            'form-element-v2': 'app/unilever-iea/js/templates/',
            'form-v2': 'app/unilever-iea/js/templates/',
            'formcheckbox': 'app/unilever-iea/js/templates/',
            'formcopy': 'app/unilever-iea/js/templates/',
            'formelements': {
                'captcha':'app/unilever-iea/js/templates/',
                'cardnumber':'app/unilever-iea/js/templates/',
                'checkbox':'app/unilever-iea/js/templates/',
                'date':'app/unilever-iea/js/templates/',
                'dropdown':'app/unilever-iea/js/templates/',
                'emailaddress':'app/unilever-iea/js/templates/',
                'fileupload':'app/unilever-iea/js/templates/',
                'formelements':'app/unilever-iea/js/templates/',
                'formreset':'app/unilever-iea/js/templates/',
                'formsubmit':'app/unilever-iea/js/templates/',
                'hiddenfield':'app/unilever-iea/js/templates/',
                'number':'app/unilever-iea/js/templates/',
                'password':'app/unilever-iea/js/templates/',
                'radio':'app/unilever-iea/js/templates/',
                'telephonenumber':'app/unilever-iea/js/templates/',
                'textarea':'app/unilever-iea/js/templates/',
                'textfield':'app/unilever-iea/js/templates/'
            },
            'formfileupload': 'app/unilever-iea/js/templates/',
            'gift-configurator': 'app/unilever-iea/js/templates/',
            'global-footer-v2': 'app/unilever-iea/js/templates/',
            'global-navigation': {
				'global-navigation': 'app/unilever-iea/js/templates/',
				'global-navigation-without-image': 'app/unilever-iea/js/templates/',
				'global-navigation-image-rollover': 'app/unilever-iea/js/templates/'
			},
            'global-search': 'app/unilever-iea/js/templates/',
            'hero': {
                'hero': 'app/unilever-iea/js/templates/',
                'hero-small': 'app/unilever-iea/js/templates/'
            },
            'hero-launch-video': 'app/unilever-iea/js/templates/',
            'hero-v2': {
                'hero-v2': 'app/unilever-iea/js/templates/',
                'image': 'app/unilever-iea/js/templates/',
                'textonly': 'app/unilever-iea/js/templates/',
                'video': 'app/unilever-iea/js/templates/'
            },
            'home-story': 'app/unilever-iea/js/templates/',
            'hotspot': 'app/unilever-iea/js/templates/',
            'html-injector': 'app/unilever-iea/js/templates/',
            'ifaq': {
                'ifaq': 'app/unilever-iea/js/templates/',
                'ifaq-item': 'app/unilever-iea/js/templates/'
            },
            'iframe': 'app/unilever-iea/js/templates/',
            'instagram-feed': 'app/unilever-iea/js/templates/',
            'language-selector': 'app/unilever-iea/js/templates/',
            'list': 'app/unilever-iea/js/templates/',
            'live-chat': 'app/unilever-iea/js/templates/',
            'live-chat-v2': 'app/unilever-iea/js/templates/',
            'login-register': 'app/unilever-iea/js/templates/',
            'media-gallery': 'app/unilever-iea/js/templates/',
            'media-gallery-v2': {
                'media-gallery-v2': 'app/unilever-iea/js/templates/',
                'media-gallery-v2-channel': 'app/unilever-iea/js/templates/',
                'media-gallery-v2-videoList': 'app/unilever-iea/js/templates/'
            },
            'multiple-related-articles': {
                'carousel': 'app/unilever-iea/js/templates/',
                'multiple-related-articles': 'app/unilever-iea/js/templates/',
                'vertical': 'app/unilever-iea/js/templates/'
            },
            'multiple-related-articles-sidebar': 'app/unilever-iea/js/templates/',
            'multiple-related-products-alternate': 'app/unilever-iea/js/templates/',
            'multiple-related-products-article': 'app/unilever-iea/js/templates/',
            'multiple-related-products-complimentary': 'app/unilever-iea/js/templates/',
            'multiple-related-tweets': {
                'multiple-related-tweets': 'app/unilever-iea/js/templates/',
                'social-feed-post-top': 'app/unilever-iea/js/templates/'
            },
            'multiple-related-tweets-TAB': {
                'multiple-related-tweets-TAB': 'app/unilever-iea/js/templates/',
                'multiple-related-tweet': 'app/unilever-iea/js/templates/'
            },
            'notFound': 'app/unilever-iea/js/templates/',
            'notfound': 'app/unilever-iea/js/templates/',
            'olapic': 'app/unilever-iea/js/templates/',
            'order-summary': 'app/unilever-iea/js/templates/',
            'ordered-list': 'app/unilever-iea/js/templates/',
            'page-divider': {
                'page-divider': 'app/unilever-iea/js/templates/',
                'page-divider-variant_a': 'app/unilever-iea/js/templates/',
                'page-divider-variant_b': 'app/unilever-iea/js/templates/'
            },
            'page-divider-v2': 'app/unilever-iea/js/templates/',
            'page-listing-v2': 'app/unilever-iea/js/templates/',
            'page-property': 'app/unilever-iea/js/templates/',
            'pagination': 'app/unilever-iea/js/templates/',
            'panel': {
                'imagePanel': 'app/unilever-iea/js/templates/',
                'panel': 'app/unilever-iea/js/templates/',
                'videoPanel': 'app/unilever-iea/js/templates/'
            },
            'payment-form': 'app/unilever-iea/js/templates/',
            'pinterest': 'app/unilever-iea/js/templates/',
            'plain-text-image': 'app/unilever-iea/js/templates/',
            'product': 'app/unilever-iea/js/templates/',
            'product-article': 'app/unilever-iea/js/templates/',
            'product-awards': 'app/unilever-iea/js/templates/',
            'product-awards-overview': 'app/unilever-iea/js/templates/',
            'product-copy': 'app/unilever-iea/js/templates/',
            'product-copy-v2': {
                'copy': 'app/unilever-iea/js/templates/',
                'product-copy-v2': 'app/unilever-iea/js/templates/',
                'product-name': 'app/unilever-iea/js/templates/',
                'quote': 'app/unilever-iea/js/templates/'
            },
            'product-crossell': 'app/unilever-iea/js/templates/',
            'product-images': 'app/unilever-iea/js/templates/',
            'product-listing': 'app/unilever-iea/js/templates/',
            'product-listing-v2': {
                'product-listing-v2': 'app/unilever-iea/js/templates/',
                'variant-product': 'app/unilever-iea/js/templates/',
                'filters': 'app/unilever-iea/js/templates/'
            },
            'product-overview': 'app/unilever-iea/js/templates/',
            'product-range': 'app/unilever-iea/js/templates/',
            'product-rating-overview': 'app/unilever-iea/js/templates/',
            'product-related-complementary': 'app/unilever-iea/js/templates/',
            'product-tags': 'app/unilever-iea/js/templates/',
            'product-teaser': 'app/unilever-iea/js/templates/',
            'product-variants-and-price': {
                'product-image': 'app/unilever-iea/js/templates/',
                'product-variants-and-price': 'app/unilever-iea/js/templates/'
            },
            'purchase-confirmation': 'app/unilever-iea/js/templates/',
            'purchase-order-confirmation': 'app/unilever-iea/js/templates/',
            'question': 'app/unilever-iea/js/templates/',
            'quick-poll': {
                'bar-chart': 'app/unilever-iea/js/templates/',
                'quick-poll': 'app/unilever-iea/js/templates/'
            },
            'quote': 'app/unilever-iea/js/templates/',
            'ratings': 'app/unilever-iea/js/templates/',
            'recipe-attributes':'app/unilever-iea/js/templates/',
            'recipe-cooking-method':'app/unilever-iea/js/templates/',
            'recipe-copy':'app/unilever-iea/js/templates/',
            'recipe-dietary-attributes':'app/unilever-iea/js/templates/',
            'recipe-hero':'app/unilever-iea/js/templates/',
            'recipe-listing': {
                'carousel-view': 'app/unilever-iea/js/templates/',
                'featured-view': 'app/unilever-iea/js/templates/',
                'list-view': 'app/unilever-iea/js/templates/',
                'recipe-listing': 'app/unilever-iea/js/templates/'
            },
            'recipe-nutrients':'app/unilever-iea/js/templates/',
            'recipe-rating-overview':'app/unilever-iea/js/templates/',
            'recipe-related-products':'app/unilever-iea/js/templates/',
            'related-articles': 'app/unilever-iea/js/templates/',
            'related-content-list': 'app/unilever-iea/js/templates/',
            'related-products': 'app/unilever-iea/js/templates/',
            'reviews': 'app/unilever-iea/js/templates/',
            'rich-text-image': 'app/unilever-iea/js/templates/',
            'rich-text-v2': 'app/unilever-iea/js/templates/',
            'search-input': 'app/unilever-iea/js/templates/',
            'search-input-v2': 'app/unilever-iea/js/templates/',
            'search-listing': {
                'no-search-result': 'app/unilever-iea/js/templates/',
                'search-listing': 'app/unilever-iea/js/templates/',
                'search-result-list': 'app/unilever-iea/js/templates/'
            },
            'search-listing-v2': {
                'no-search-result-v2': 'app/unilever-iea/js/templates/',
                'search-listing-v2': 'app/unilever-iea/js/templates/',
                'search-result-v2-facets': 'app/unilever-iea/js/templates/',
                'search-result-v2-list': 'app/unilever-iea/js/templates/',
                'search-result-v2-list-all': 'app/unilever-iea/js/templates/',
                'search-result-v2-pagination':'app/unilever-iea/js/templates/',
                'search-result-v2-recipe-list':'app/unilever-iea/js/templates/',
                'search-result-v2-tabs':'app/unilever-iea/js/templates/'
            },
            'search-result': 'app/unilever-iea/js/templates/',
            'section-navigation': 'app/unilever-iea/js/templates/',
            'section-navigation-v2':{
                'dropdown':'app/unilever-iea/js/templates/',
                'list':'app/unilever-iea/js/templates/',
                'section-navigation-v2':'app/unilever-iea/js/templates/'
            },
            'shoppable-view-bag': {
                'shoppable-cart':'app/unilever-iea/js/templates/',
                'shoppable-view-bag':'app/unilever-iea/js/templates/'
            },
            'shopping-basket': {
                'shopping-basket': 'app/unilever-iea/js/templates/',
                'shopping-cart-list': 'app/unilever-iea/js/templates/'
            },
            'simple-footer': 'app/unilever-iea/js/templates/',
            'simple-header': 'app/unilever-iea/js/templates/',
            'sitemap': 'app/unilever-iea/js/templates/',
            'smart-label': 'app/unilever-iea/js/templates/',
            'smart-label-v2': 'app/unilever-iea/js/templates/',
            'social-carousel': {
                'social-carousel': 'app/unilever-iea/js/templates/',
                'social-carousel-list': 'app/unilever-iea/js/templates/'
            },
            'social-gallery': {
                'social-gallery': 'app/unilever-iea/js/templates/',
                'social-gallery-video-list': 'app/unilever-iea/js/templates/'
            },
            'social-gallery-carousel-expanded-TAB': 'app/unilever-iea/js/templates/',
            'social-gallery-carousel-thumbnails-TAB': 'app/unilever-iea/js/templates/',
            'social-gallery-TAB': {
                'social-gallery-TAB': 'app/unilever-iea/js/templates/',
                'social-gallery-list': 'app/unilever-iea/js/templates/'
            },
            'social-gallery-tab-v2': {
                'social-gallery-tab-v2': 'app/unilever-iea/js/templates/',
                'social-gallery-list': 'app/unilever-iea/js/templates/'
            },
            'social-gallery-v2': {
                'carousel': 'app/unilever-iea/js/templates/',
                'featured-panel': 'app/unilever-iea/js/templates/',
                'featured-post': 'app/unilever-iea/js/templates/',
                'gallery-listing': 'app/unilever-iea/js/templates/',
                'social-gallery': 'app/unilever-iea/js/templates/',
                'social-gallery-v2': 'app/unilever-iea/js/templates/',
                'featured-post-listing': 'app/unilever-iea/js/templates/',
                'carousel-listing': 'app/unilever-iea/js/templates/'
            },
            'social-sharing': {
                'social-sharing': 'app/unilever-iea/js/templates/',
                'social-sharing-inline': 'app/unilever-iea/js/templates/',
                'social-sharing-stack': 'app/unilever-iea/js/templates/'
            },
            'spotlight': 'app/unilever-iea/js/templates/',
            'start-section': 'app/unilever-iea/js/templates/',
            'store-search': 'app/unilever-iea/js/templates/',
            'store-search-pdp': 'app/unilever-iea/js/templates/',
            'store-search-results': 'app/unilever-iea/js/templates/',
            'tabbed-content': 'app/unilever-iea/js/templates/',
            'tags': 'app/unilever-iea/js/templates/',
            'thank-you': 'app/unilever-iea/js/templates/',
            'thumb-carousel': 'app/unilever-iea/js/templates/',
            'turn-to': 'app/unilever-iea/js/templates/',
            'twitter-feed':'app/unilever-iea/js/templates/',
            'video': {
                'brightcove': 'app/unilever-iea/js/templates/',
                'dam': 'app/unilever-iea/js/templates/',
                'video': 'app/unilever-iea/js/templates/',
                'vimeo': 'app/unilever-iea/js/templates/',
                'youtube': 'app/unilever-iea/js/templates/'
            },
            'video-gallery': 'app/unilever-iea/js/templates/',
            'video-player': 'app/unilever-iea/js/templates/',
            'video-player-v2': 'app/unilever-iea/js/templates/',
            'video-v2': 'app/unilever-iea/js/templates/',
            'virtual-agent': {
                'virtual-agent':'app/unilever-iea/js/templates/',
                'virtual-agent-result': 'app/unilever-iea/js/templates/'
            },
            'whats-new': 'app/unilever-iea/js/templates/',
            'zip-file-download': 'app/unilever-iea/js/templates/',
            'zoom-modal': 'app/unilever-iea/js/templates/',
            'partials': {
                'awards': 'app/unilever-iea/js/templates/',
                'bin-widget': 'app/unilever-iea/js/templates/',
                'bin-widget-listing': 'app/unilever-iea/js/templates/',
                'breadcrumb-list': 'app/unilever-iea/js/templates/',
                'buy-in-store': 'app/unilever-iea/js/templates/',
                'egifting-tooltip': 'app/unilever-iea/js/templates/',
                'global-search-input': 'app/unilever-iea/js/templates/',
                'hero-copy': 'app/unilever-iea/js/templates/',
                'multiple-related-products': 'app/unilever-iea/js/templates/',
                'order-summary-list': 'app/unilever-iea/js/templates/',
                'page-listing-v2-filter' : 'app/unilever-iea/js/templates/',
                'page-listing-v2-page' : 'app/unilever-iea/js/templates/',
                'product-info': 'app/unilever-iea/js/templates/',
                'product-listing-filter' : 'app/unilever-iea/js/templates/',
                'product-listing-filter-option' : 'app/unilever-iea/js/templates/',
                'product-listing-products': 'app/unilever-iea/js/templates/',
                'product-listing-v2-product' : 'app/unilever-iea/js/templates/',
                'product-listing-v2-quick-view' : 'app/unilever-iea/js/templates/',
                'question-answer': 'app/unilever-iea/js/templates/',
                'recipe-listing' : 'app/unilever-iea/js/templates/',
                'recipe-ratings' : 'app/unilever-iea/js/templates/',
                'search-input': 'app/unilever-iea/js/templates/',
                'section-navigation-list': 'app/unilever-iea/js/templates/',
                'shoppable-cart': 'app/unilever-iea/js/templates/',
                'social-feed-post': 'app/unilever-iea/js/templates/',
                'social-gallery-thumbnail': 'app/unilever-iea/js/templates/',
                'social-gallery-thumbnail-manual': 'app/unilever-iea/js/templates/',
                'tooltip': 'app/unilever-iea/js/templates/',
                'turn-to': 'app/unilever-iea/js/templates/',
                'whats-new-products': 'app/unilever-iea/js/templates/',
                'whats-new-products-range': 'app/unilever-iea/js/templates/'
            }
        }
    },

    // internationalization settings (app.config.geti18NSetting())
    'i18n': {},

    // layout settings (app.config.getLayoutSetting())
    'layout': {},

    // application level default messages (app.config.getMessageSetting())
    'messages': {
        /*'invalidJsonErrName' : ''*/
        'loadErrDefaultTitle': "Component could not be loaded",
        'loadErrDefaultMsg': "Could not load the requested component",
        'invalidJSONDefaultMsg': "Invalid JSON contract received",
        'connectivityErrorMsg': 'Component not loaded due to network connectivity!!'
    },

    // application folder and file naming conventions (app.config.getConventionsSetting())
    'conventions': {
        'notFoundClass': 'not-found',
        'errorClass': 'error',
        'imagePath': 'app/images/',
        'sassPath': 'app/sass/',
        'jsPath': 'app/js/',
        'cssPath': 'app/css/',
        'fontPath': 'app/css/fonts'
    },

    // other application settings (app.config.getSetting())
    'settings': {
        'errorHandler': 'jsonParseError',
        'missingTemplateHandler': 'missingTemplate'
    },

    // environment detection and settings (app.config.getEvnironmentSetting())
    'environment': {
        'development': ['^localhost', '^127.0.0.1', '^10.202.11', '^demo.'],
        'stage': ['^stage', '^beta'],
        'production': []
    },

    // development settings (app.config.getDevelopmentSetting())
    'development': {
        // debug settings(true | false | {}) (app.config.getDebugSetting())
        'debug': true
    },

    // stageserver settings (app.config.getStageSetting())
    'stage': {
        // debug settings(true | false | {}) (app.config.getDebugSetting())
        'debug': true
    },

    // production settings (app.config.getProductionSetting())
    'production': {
        // debug settings(true | false | {}) (app.config.getDebugSetting())
        'debug': false
    }
});
