!function (e, t) {
    "function" == typeof define && define.amd ? define(t) : "object" == typeof exports ? module.exports = t : e.configObj = t
}(this, {
    'name': 'dig',               // IEA tenant/sub-tenant application namespace
    'theme': 'whitelabel',                  // Application theme
    'lib': 'lib',                           //IEA; should not be modified
    'parentDir': 'app/unilever-iea',   //IEA tenant folder for sub-tenant; same in case of tenant application
    'parentAppName': 'unilever-iea',   //IEA tenant appName for sub-tenant; same in case of tenant application
    'parentTheme': 'whitelabel',            //IEA tenant theme for sub-tenant; same in case of tenant application
    'dist': 'dist',                         //IEA; should not be modified
    'appBasePath': 'app/dig',    //IEA tenant/sub-tenant base path
    'selector': 'ulenscale',

    // debug settings(true | false | {}) (app.config.getDebugSetting())
    // environment specific setting will override default setting on load.
    'debug': true,

    'dependencies': ['unilever-iea.components', 'dig.components'], //component dependency
    'defaultTemplateName': ['defaultView', 'landingView'],
    'templatePath': 'app/dig/js/templates/',
    'dev': {
        'css': 'css/',
        'fonts': 'css/fonts/',
        'js': 'js/',
        'images': 'images/',
        'svgs':'svgs/',
        'config':'config/',
        'templates': 'js/templates',
        'fontPath': 'fonts/',
        'imagePath': '../images/'
    },
    'cq': {
        'css': 'dist/etc/ui/dig/clientlibs/core/core/css',
        'fonts': 'dist/etc/ui/dig/clientlibs/core/core/css/fonts',
        'js': 'dist/etc/ui/dig/clientlibs/core/require/js',
        'modjs': 'dist/etc/ui/dig/clientlibs/core/core/js',
        'images': 'dist/etc/ui/dig/clientlibs/core/core/images',
        'svgs': 'dist/etc/ui/dig/clientlibs/core/core/svgs',
        'config': 'dist/etc/ui/dig/clientlibs/core/core/config',
        'templates': 'dist/etc/ui/dig/templates/components',
        'fontPath': '../fonts/',
        'imagePath': '../images/'
    },
    // breakpoint configuration
    'breakpoints': {
        'deviceSmall': '320px',
        'deviceMedium': '768px',
        'deviceLarge': '992px',
        'deviceXlarge': '1200px',

        'containerSmall': 'auto',
        'containerMedium': 'auto',
        'containerLarge': '1024px',
        'containerXLarge': '1200px'
    },
    'build': {
        'minified': true,
        'combined': true
    },
    // application template and engine settings (app.config.getTemplateSetting())
    'template': {
        'namespace': 'dig',
        'parentNamespace': 'unilever-iea',
        'parentHelpers': ['unileverHelpers'],
        'path': {
            'notFound': 'app/unilever-iea/js/templates/',
			'adaptive-image': 'app/unilever-iea/js/templates/',
            'global-navigation': 'app/dig/js/templates/',
            'flyOutMenu': 'app/unilever-iea/js/templates/',
            'anchor-link-navigation': 'app/unilever-iea/js/templates/',
            'breadcrumb': 'app/unilever-iea/js/templates/',
            'footer': 'app/dig/js/templates/',
            'hero': {
                'hero': 'app/dig/js/templates/',
                'hero-small': 'app/dig/js/templates/'
            },
            'hero-launch-video': 'app/dig/js/templates/',
            'category-overview': 'app/unilever-iea/js/templates/',
            'featured-product': 'app/unilever-iea/js/templates/',
            'featured-product-range': 'app/dig/js/templates/',
			'language-selector': 'app/unilever-iea/js/templates/',
            'product-article': 'app/unilever-iea/js/templates/',
            'body-copy': 'app/unilever-iea/js/templates/',
            'body-copy-steps': 'app/unilever-iea/js/templates/',
            'multiple-related-articles': {
                'multiple-related-articles': 'app/unilever-iea/js/templates/',
                'carousel': 'app/unilever-iea/js/templates/',
                'vertical': 'app/unilever-iea/js/templates/'
            },
            'multiple-related-articles-sidebar': 'app/unilever-iea/js/templates/',
            'product': 'app/unilever-iea/js/templates/',
            'ratings': 'app/unilever-iea/js/templates/',
            'product-listing':'app/unilever-iea/js/templates/',

            'multiple-related-products-alternate': 'app/unilever-iea/js/templates/',
            'multiple-related-products-complimentary': 'app/unilever-iea/js/templates/',
            'multiple-related-products-article': 'app/unilever-iea/js/templates/',

            'product-related-complementary': 'app/unilever-iea/js/templates/',
            'featured-article': {
                'featured-article': 'app/unilever-iea/js/templates/',
                'featured-article-image-small': 'app/unilever-iea/js/templates/',
                'featured-article-image-wide': 'app/unilever-iea/js/templates/'
            },
            'article-listing': 'app/dig/js/templates/',
            'product-overview': 'app/unilever-iea/js/templates/',
            'accordion': 'app/unilever-iea/js/templates/',
            'product-copy': 'app/unilever-iea/js/templates/',
            'product-tags': 'app//js/templates/',
            'home-story': 'app/dig/js/templates/',
            'featured-quote': {
                'featured-quote': 'app/unilever-iea/js/templates/',
                'featured-quote-center': 'app/unilever-iea/js/templates/',
                'featured-quote-left': 'app/unilever-iea/js/templates/'
            },
            'video-gallery': 'app/unilever-iea/js/templates/',
            'reviews': 'app/unilever-iea/js/templates/',
            'thumb-carousel': 'app/unilever-iea/js/templates/',
            'whats-new': 'app/unilever-iea/js/templates/',
            'partials': {
                'breadcrumb-list': 'app/unilever-iea/js/templates/',
                'global-search-input': 'app/unilever-iea/js/templates/',
                'multiple-related-products': 'app/unilever-iea/js/templates/',
                'no-search-result': 'app/unilever-iea/js/templates/',
                'product-listing-filter' : 'app/unilever-iea/js/templates/',
                'product-listing-filter-option' : 'app/unilever-iea/js/templates/',
                'product-listing-products': 'app/unilever-iea/js/templates/',
                'search-input': 'app/unilever-iea/js/templates/',
                'search-result-list': 'app/unilever-iea/js/templates/',
                'svg': 'app/unilever-iea/js/templates/',
                'whats-new-products': 'app/unilever-iea/js/templates/',
                'whats-new-products-range': 'app/unilever-iea/js/templates/',
                'bin-widget': 'app/unilever-iea/js/templates/',
                'shopping-cart-list': 'app/unilever-iea/js/templates/',
                'order-summary-list': 'app/unilever-iea/js/templates/',
                'egifting-tooltip': 'app/unilever-iea/js/templates/',
                'product-info': 'app/unilever-iea/js/templates/'
            },
            'animations': 'app/unilever-iea/js/templates/',
            'social-sharing': {
                'social-sharing': 'app/unilever-iea/js/templates/',
                'social-sharing-inline': 'app/unilever-iea/js/templates/',
                'social-sharing-stack': 'app/unilever-iea/js/templates/'
            },
            'zoom-modal': 'app/unilever-iea/js/templates/',
            'global-search': 'app/unilever-iea/js/templates/',
            'search-listing': 'app/unilever-iea/js/templates/',
            'video-player': 'app/unilever-iea/js/templates/',
            'body-copy-headline':'app/dig/js/templates/',
            'sitemap': 'app/unilever-iea/js/templates/',
            'form': 'app/dig/js/templates/',
            'formcopy': 'app/unilever-iea/js/templates/',
            'formcheckbox': 'app/unilever-iea/js/templates/',
            'form-element':{
                'calender':'app/unilever-iea/js/templates/',
                'dropdown':'app/unilever-iea/js/templates/',
                'radio':'app/unilever-iea/js/templates/',
                'text':'app/unilever-iea/js/templates/',
                'textarea':'app/unilever-iea/js/templates/',
            },

            'formfileupload': 'app/unilever-iea/js/templates/',
            'country-selector': 'app/dig/js/templates/',
            'product-range': 'app/unilever-iea/js/templates/',
            'page-divider': 'app/dig/js/templates/',
            'olapic': 'app/unilever-iea/js/templates/',
			'content-results-grid': {
                'content-results-grid': 'app/unilever-iea/js/templates/',
                'content-result-grid-article': 'app/unilever-iea/js/templates/'
            },
            'country-language-selector': 'app/unilever-iea/js/templates/',
            'dynamic-page-properties':'app/unilever-iea/js/templates/',
            'expand-collapse':'app/unilever-iea/js/templates/',
            'flyOutMenu':'app/unilever-iea/js/templates/',
            'hotspot':'app/unilever-iea/js/templates/',
            'instagram-feed':'app/unilever-iea/js/templates/',
            'list':'app/unilever-iea/js/templates/',
            'media-gallery':'app/unilever-iea/js/templates/',
            'navigation':'app/dig/js/templates/',
            'notfound':'app/unilever-iea/js/templates/',
			'carousel': 'app/unilever-iea/js/templates/',
            'panel':{
                'panel':'app/unilever-iea/js/templates/',
                'imagePanel':'app/unilever-iea/js/templates/',
                'videoPanel':'app/unilever-iea/js/templates/'
            },
			'video': {
                'video': 'app/unilever-iea/js/templates/',
                'vimeo': 'app/unilever-iea/js/templates/',
                'youtube': 'app/unilever-iea/js/templates/',
                'brightcove': 'app/unilever-iea/js/templates/',
                'dam': 'app/unilever-iea/js/templates/'
            },
            'pinterest':'app/unilever-iea/js/templates/',
            'plain-text-image':'app/dig/js/templates/',
            'related-content-list':'app/unilever-iea/js/templates/',
            'rich-text-image':'app/dig/js/templates/',
            'search-input':'app/unilever-iea/js/templates/',
            'search-result':'app/unilever-iea/js/templates/',
            'section-navigation':'app/unilever-iea/js/templates/',
            'social-share-print':'app/unilever-iea/js/templates/',
            'tabbed-content':'app/unilever-iea/js/templates/',
            'twitter-feed':'app/unilever-iea/js/templates/',

            'featured-category': 'app/unilever-iea/js/templates/',
            'call-out-button': 'app/unilever-iea/js/templates/',
            'gift-configurator': 'app/unilever-iea/js/templates/',
            'shopping-basket': 'app/unilever-iea/js/templates/',
            'featured-social-topic': 'app/unilever-iea/js/templates/',
			'simple-footer': 'app/unilever-iea/js/templates/',
			'simple-header': 'app/unilever-iea/js/templates/',
            'product-crossell': 'app/unilever-iea/js/templates/',
			'checkout-form':'app/unilever-iea/js/templates/',
            'payment-form':'app/unilever-iea/js/templates/',
            'order-summary':'app/unilever-iea/js/templates/',
			'purchase-confirmation':'app/unilever-iea/js/templates/',
            'dynamic-error-message':'app/unilever-iea/js/templates/'
        }
    },

    // internationalization settings (app.config.geti18NSetting())
    'i18n': {},

    // layout settings (app.config.getLayoutSetting())
    'layout': {},

    // application level default messages (app.config.getMessageSetting())
    'messages': {
        /*'invalidJsonErrName' : ''*/
        'loadErrDefaultTitle': "Component could not be loaded",
        'loadErrDefaultMsg': "Could not load the requested component",
        'invalidJSONDefaultMsg': "Invalid JSON contract received",
        'connectivityErrorMsg': 'Component not loaded due to network connectivity!!'
    },

    // application folder and file naming conventions (app.config.getConventionsSetting())
    'conventions': {
        'notFoundClass': 'not-found',
        'errorClass': 'error',
        'imagePath': 'app/images/',
        'sassPath': 'app/sass/',
        'jsPath': 'app/js/',
        'cssPath': 'app/css/',
        'fontPath': 'app/css/fonts'
    },

    // other application settings (app.config.getSetting())
    'settings': {
        'errorHandler': 'jsonParseError',
        'missingTemplateHandler': 'missingTemplate'
    },

    // environment detection and settings (app.config.getEvnironmentSetting())
    'environment': {
        'development': ['^localhost', '^127.0.0.1', '^10.202.11', '^demo.'],
        'stage': ['^stage', '^beta'],
        'production': [],
    },

    // development settings (app.config.getDevelopmentSetting())
    'development': {
        // debug settings(true | false | {}) (app.config.getDebugSetting())
        'debug': true
    },

    // stageserver settings (app.config.getStageSetting())
    'stage': {
        // debug settings(true | false | {}) (app.config.getDebugSetting())
        'debug': true
    },

    // production settings (app.config.getProductionSetting())
    'production': {
        // debug settings(true | false | {}) (app.config.getDebugSetting())
        'debug': false
    }
});
