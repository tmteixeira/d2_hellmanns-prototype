!function(e,t){"function"==typeof define&&define.amd?define(t):"object"==typeof exports?module.exports=t:e.configObj=t}(this,{
    'name': 'hellmanns',               // IEA tenant/sub-tenant application namespace
    'theme': 'whitelabel',                  // Application theme
    'lib': 'lib',                           //IEA; should not be modified
    'parentDir': 'app/unilever-iea',   //IEA tenant folder for sub-tenant; same in case of tenant application
    'parentAppName': 'unilever-iea',   //IEA tenant appName for sub-tenant; same in case of tenant application
    'parentTheme': 'whitelabel',            //IEA tenant theme for sub-tenant; same in case of tenant application
    'dist': 'dist',                         //IEA; should not be modified
    'appBasePath': 'app/hellmanns',    //IEA tenant/sub-tenant base path

    // debug settings(true | false | {}) (app.config.getDebugSetting())
    // environment specific setting will override default setting on load.
    'debug': true,

    'dependencies': ['unilever-iea.components', 'hellmanns.components'], //component dependency
    'defaultTemplateName' : ['defaultView', 'landingView'],
    'templatePath': 'app/hellmanns/js/templates/',
    'dev': {
        'css': 'css/',
        'fonts': 'css/fonts/',
        'js': 'js/',
        'images': 'images/',
        'templates': 'js/templates',
        'fontPath': 'fonts/',
        'imagePath': '../images/'
    },
    'cq': {
        'css': 'dist/etc/ui/hellmanns/clientlibs/core/core/css',
        'fonts': 'dist/etc/ui/hellmanns/clientlibs/core/core/css/fonts',
        'js': 'dist/etc/ui/hellmanns/clientlibs/core/require/js',
        'modjs': 'dist/etc/ui/hellmanns/clientlibs/core/core/js',
        'images': 'dist/etc/ui/hellmanns/clientlibs/core/core/images',
        'templates': 'dist/etc/ui/hellmanns/templates/components',
        'fontPath': '../fonts/',
        'imagePath': '../images/'
    },
    // breakpoint configuration
    'breakpoints': {
        'deviceSmall': '320px',
        'deviceMedium': '480px',
        'deviceLarge': '768px',
        'deviceXlarge': '1025px',
        'containerSmall': 'auto',
        'containerMedium': 'auto',
        'containerLarge': '750px',
        'containerXLarge': '960px'
    },
    'build': {
        'minified' : true,
        'combined' : true
    },
     // application template and engine settings (app.config.getTemplateSetting())
    'template':{
        'namespace': 'hellmanns',
        'helpers': [],
        'parentHelpers': [],
        'parentNamespace': 'unilever-iea',
        'path': {
            'adaptive-image': 'app/hellmanns/js/templates/',
            'breadcrumb': 'app/hellmanns/js/templates/',
            'carousel': 'app/hellmanns/js/templates/',
            'content-results-grid': {
                'content-results-grid': 'app/hellmanns/js/templates/',
                'content-result-grid-article': 'app/hellmanns/js/templates/'
            },
            'country-language-selector': 'app/hellmanns/js/templates/',
            'dynamic-page-properties':'app/hellmanns/js/templates/',
            'expand-collapse':'app/hellmanns/js/templates/',
            'flyOutMenu':'app/hellmanns/js/templates/',
            'form':'app/hellmanns/js/templates/',
            'form-element':{
                'calender':'app/hellmanns/js/templates/',
                'dropdown':'app/hellmanns/js/templates/',
                'radio':'app/hellmanns/js/templates/',
                'text':'app/hellmanns/js/templates/'
            },
            'hotspot':'app/hellmanns/js/templates/',
            'instagram-feed':'app/hellmanns/js/templates/',
            'list':'app/hellmanns/js/templates/',
            'media-gallery':'app/hellmanns/js/templates/',
            'navigation':'app/hellmanns/js/templates/',
            'notfound':'app/hellmanns/js/templates/',
            'panel':{
                'panel':'app/hellmanns/js/templates/',
                'imagePanel':'app/hellmanns/js/templates/',
                'videoPanel':'app/hellmanns/js/templates/'
            },
            'pinterest':'app/hellmanns/js/templates/',
            'plain-text-image':'app/hellmanns/js/templates/',
            'related-content-list':'app/hellmanns/js/templates/',
            'rich-text-image':'app/hellmanns/js/templates/',
            'search-input':'app/hellmanns/js/templates/',
            'search-result':'app/hellmanns/js/templates/',
            'section-navigation':'app/hellmanns/js/templates/',
            'social-share-print':'app/hellmanns/js/templates/',
            'tabbed-content':'app/hellmanns/js/templates/',
            'twitter-feed':'app/hellmanns/js/templates/',
            'video':{
                'video': 'app/hellmanns/js/templates/',
                'vimeo': 'app/hellmanns/js/templates/',
                'youtube': 'app/hellmanns/js/templates/',
                'brightcove': 'app/hellmanns/js/templates/',
                'dam': 'app/hellmanns/js/templates/'
            }
        }
    },

    // internationalization settings (app.config.geti18NSetting())
    'i18n':{},

    // layout settings (app.config.getLayoutSetting())
    'layout':{},

    // application level default messages (app.config.getMessageSetting())
    'messages':{
        /*'invalidJsonErrName' : ''*/
        'loadErrDefaultTitle' : "Component could not be loaded",
        'loadErrDefaultMsg': "Could not load the requested component",
        'invalidJSONDefaultMsg': "Invalid JSON contract recieved",
        'connectivityErrorMsg': 'Component not loaded due to network connectivity!!'
    },

    // application folder and file nameing convensions (app.config.getConventionsSetting())
    'conventions':{
        'notFoundClass': 'not-found',
        'errorClass': 'error',
        'imagePath': 'app/images/',
        'sassPath': 'app/sass/',
        'jsPath': 'app/js/',
        'cssPath': 'app/css/',
        'fontPath': 'app/css/fonts'
    },

    // other application settings (app.config.getSetting())
    'settings':{
        'errorHandler': 'jsonParseError',
        'missingTemplateHandler': 'missingTemplate'
    },

    // environment detection and settings (app.config.getEvnironmentSetting())
    'environment': {
        'development': ['^localhost','^127.0.0.1', '^10.202.11', '^demo.'],
        'stage': ['^stage','^beta'],
        'production': [],
    },

    // development settings (app.config.getDevelopmentSetting())
    'development': {
        // debug settings(true | false | {}) (app.config.getDebugSetting())
        'debug': true
    },

    // stageserver settings (app.config.getStageSetting())
    'stage': {
        // debug settings(true | false | {}) (app.config.getDebugSetting())
        'debug': true
    },

    // production settings (app.config.getProductionSetting())
    'production': {
        // debug settings(true | false | {}) (app.config.getDebugSetting())
        'debug': false
    }
});
