'use strict';
var util = require('util'),
    path = require('path'),
    fs = require('fs'),
    yeoman = require('yeoman-generator'),
    scriptBase = require('../script-base'),
    backboneUtils = require('../util.js');

var IEAGenerator = module.exports = function IEAGenerator() {
  yeoman.generators.Base.apply(this, arguments);
  this.sourceRoot(path.join(__dirname, '../templates'));

  this.log(this.yeoman);
}

util.inherits(IEAGenerator, yeoman.generators.Base);

IEAGenerator.prototype.createFiles = function createFiles() {

  var appFolder = 'app',
      othersPath = 'others',
      self = this;

  this.argument('appname', { type: String, required: false });
  this.argument('parentName', { type: String, required: false });

  this.parentName = this.parentName || this.appname;

  var appPath = appFolder + '/' + this.appname;

  this.mkdir(appPath);

  backboneUtils.copyFolderRecursiveSync(this.sourceRoot() + '/'+othersPath+'/app/css', appPath);
  backboneUtils.copyFolderRecursiveSync(this.sourceRoot() + '/'+othersPath+'/app/data', appPath);
  backboneUtils.copyFolderRecursiveSync(this.sourceRoot() + '/'+othersPath+'/app/images', appPath);
  backboneUtils.copyFolderRecursiveSync(this.sourceRoot() + '/'+othersPath+'/app/js', appPath);
  backboneUtils.copyFolderRecursiveSync(this.sourceRoot() + '/'+othersPath+'/app/sass', appPath);

  if(this.appname === this.parentName) {
    this.indexFile = this.readFileAsString(path.join(this.sourceRoot()+ '/'+othersPath+'/', 'index-tenant.html'));
    this.copy('others/app-config-parent.js', 'config/'+this.appname+'-config.js');
    this.copy('js/main-parent.js', appPath + '/js/main.js');
    this.copy('js/_base-parent.scss', appPath + '/sass/_base.scss');
    this.copy('js/_main-tenant.scss', appPath + '/sass/_main.scss');
  } else {
    this.indexFile = this.readFileAsString(path.join(this.sourceRoot()+ '/'+othersPath+'/', 'index-sub-tenant.html'));
    this.copy('others/app-config.js', 'config/'+this.appname+'-config.js');
    this.copy('js/main-tenant.js', appPath + '/js/main.js');
    this.copy('js/_base.scss', appPath + '/sass/_base.scss');
    this.copy('js/_main-sub-tenant.scss', appPath + '/sass/_main.scss');
  }

  this.indexFile = this.engine(this.indexFile, this);

  this.copy(othersPath+'/app/404.html', appPath + '/404.html');
  this.copy(othersPath+'/app/favicon.ico', appPath + '/favicon.ico');
  this.copy(othersPath+'/app/robots.txt', appPath + '/robots.txt');
  this.copy(othersPath+'/app/.htaccess', appPath + '/.htaccess');
  this.copy(othersPath+'/_bower.json', appPath + '/bower.json');
  this.copy(othersPath+'/bowerrc', appPath + '/.bowerrc');
  this.write(appPath + '/index.html', this.indexFile);

  fs.readFile('.gitignore', 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    data = data + '\napp/'+self.appname+'/js/libs/iea/core' + '\napp/'+self.appname+'/js/libs/vendor';

    var stream = fs.createWriteStream('.gitignore');
    stream.once('open', function(fd) {
      stream.write(data);
      stream.end();
    });
  });

};
