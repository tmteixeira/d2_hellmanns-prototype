/*jshint -W079 */

'use strict';

var LIVERELOAD_PORT = 35729;
var SERVER_PORT = 9000;

var fs = require('fs');
var _ = require('underscore');
var OOBComponents = [],
    appComponents = [];

var config = {
    app: 'app',
    dist: 'dist',
    dev: {
        theme: 'whitelabel',
        css: 'css/',
        fonts: 'css/fonts/',
        js: 'js/',
        images: 'images/',
        templates: 'js/templates',
        fontPath: 'fonts/',
        imagePath: '../images/'
    },
    cq: {
        theme: 'whitelabel',
        css: 'dist/etc/ui/iea/clientlibs/base/css',
        fonts: 'dist/etc/ui/iea/clientlibs/base/fonts',
        js: 'dist/etc/ui/iea/clientlibs/base/js',
        images: 'dist/etc/ui/iea/clientlibs/images',
        templates: 'dist/etc/ui/iea/templates/components',
        fontPath: '../fonts/',
        imagePath: '../images/'
    }
};

var appModules = fs.readdirSync(config.app + '/js/views/');

_.each(appModules, function(model) {
    var str = model.replace('.js', '');
    appComponents.push('views/' + str + '/' + str);
});

module.exports = function(grunt) {
    grunt.option('LIVERELOAD_PORT', LIVERELOAD_PORT);
    grunt.option('port', SERVER_PORT);
    grunt.option('host', '0.0.0.0'); // change this to '0.0.0.0' to access the server from outside
    grunt.option('theme', 'whitelabel');
    grunt.option('version', '1.0.0');
    grunt.option('config', config);

    require('load-grunt-config')(grunt, {
        data: {
            port: grunt.option('port'),
            config: grunt.option('config'),
            appComponents: appComponents
        }
    });

    grunt.registerTask('createDefaultTemplate', function() {
        grunt.file.write('.tmp/js/templates.js', 'this.JST = this.JST || {};');
    });

    grunt.registerTask('loadComponents', function() {
        grunt.file.write('.tmp/js/components.js', 'define(["' + appComponents.join('","') + '"],function(){return Array.prototype.slice.call(arguments);});');
    });

    grunt.registerTask('server', function(target) {
        grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
        grunt.task.run(['serve' + (target ? ':' + target : '')]);
    });

    grunt.registerTask('server', function(target) {
        if (target === 'dist') {
            return grunt.task.run([
                'build',
                'open:server',
                'connect:dist:keepalive'
            ]);
        }

        grunt.task.run([
            'jshint',
            'clean:server',
            'createDefaultTemplate',
            'loadComponents',
            'copy:iea',
            'handlebars',
            'compass:server',
            'connect:livereload',
            'open:server',
            'watch'
        ]);
    });

    grunt.registerTask('build', function(type) {
        if (type === 'cq') {
            return grunt.task.run([
                'clean:dist',
                'jshint',
                'createDefaultTemplate',
                'loadOOBComponents',
                'loadComponents',
                'handlebars',
                'preprocess',
                'template:cq',
                'useminPrepare',
                'requirejs:iea',
                'requirejs:dist',
                'compass:iea',
                'compass:server',
                'imagemin',
                'htmlmin',
                'concat',
                'cssmin',
                'uglify',
                'copy:def',
                'usemin',
                'copy:cq',
                /*'clean:cq'*/
            ]);
        }

        if(type === 'boilerplate') {
            return grunt.task.run([
                'default',
                'copy:boilerplate',
            ]);
        }

        grunt.task.run([
            'default',
            'copy:webserver'
        ]);
    });

    grunt.registerTask('c2s',['copy:cssAsScss']);

    grunt.registerTask('default', [
        'clean:dist',
        'jshint',
        'createDefaultTemplate',
        'loadComponents',
        'copy:iea',
        'handlebars',
        'useminPrepare',
        'requirejs:dist',
        'compass:server',
        'imagemin',
        'htmlmin',
        'concat',
        'cssmin',
        'uglify',
        'copy:def',
        'usemin'
    ]);

};
