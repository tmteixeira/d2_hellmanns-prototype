!function(e,t){"function"==typeof define&&define.amd?define(t):"object"==typeof exports?module.exports=t:e.configObj=t}(this,{
    'name': '<%= appname %>',               // IEA tenant/sub-tenant application namespace
    'theme': 'whitelabel',                  // Application theme
    'lib': 'lib',                           //IEA; should not be modified
    'dist': 'dist',                         //IEA; should not be modified
    'appBasePath': 'app/<%= appname %>',    //IEA tenant/sub-tenant base path

    // debug settings(true | false | {}) (app.config.getDebugSetting())
    // environment specific setting will override default setting on load.
    'debug': true,

    'dependencies': ['<%= appname %>.components'], //component dependency
    'defaultTemplateName' : ['defaultView', 'landingView'],
    'templatePath': 'app/<%= appname %>/js/templates/',
    'dev': {
        'css': 'css/',
        'fonts': 'css/fonts/',
        'js': 'js/',
        'images': 'images/',
        'templates': 'js/templates',
        'fontPath': 'fonts/',
        'imagePath': '../images/'
    },
    'cq': {
        'css': 'dist/etc/ui/<%= appname %>/clientlibs/core/core/css',
        'fonts': 'dist/etc/ui/<%= appname %>/clientlibs/core/core/css/fonts',
        'js': 'dist/etc/ui/<%= appname %>/clientlibs/core/require/js',
        'modjs': 'dist/etc/ui/<%= appname %>/clientlibs/core/core/js',
        'images': 'dist/etc/ui/<%= appname %>/clientlibs/core/core/images',
        'templates': 'dist/etc/ui/<%= appname %>/templates/components',
        'fontPath': '../fonts/',
        'imagePath': '../images/'
    },
    // breakpoint configuration
    'breakpoints': {
        'deviceSmall': '320px',
        'deviceMedium': '480px',
        'deviceLarge': '768px',
        'deviceXlarge': '1025px',
        'containerSmall': 'auto',
        'containerMedium': 'auto',
        'containerLarge': '750px',
        'containerXLarge': '960px'
    },
    'build': {
        'minified' : true,
        'combined' : true
    },
     // application template and engine settings (app.config.getTemplateSetting())
    'template':{
        'namespace': '<%= appname %>',
        'helpers': [],
        'path': {
            'adaptive-image': 'app/<%= appname %>/js/templates/',
            'breadcrumb': 'app/<%= appname %>/js/templates/',
            'carousel': 'app/<%= appname %>/js/templates/',
            'content-results-grid': {
                'content-results-grid': 'app/<%= appname %>/js/templates/',
                'content-result-grid-article': 'app/<%= appname %>/js/templates/'
            },
            'country-language-selector': 'app/<%= appname %>/js/templates/',
            'dynamic-page-properties':'app/<%= appname %>/js/templates/',
            'expand-collapse':'app/<%= appname %>/js/templates/',
            'flyOutMenu':'app/<%= appname %>/js/templates/',
            'form':'app/<%= appname %>/js/templates/',
            'form-element':{
                'calender':'app/<%= appname %>/js/templates/',
                'dropdown':'app/<%= appname %>/js/templates/',
                'radio':'app/<%= appname %>/js/templates/',
                'text':'app/<%= appname %>/js/templates/'
            },
            'hotspot':'app/<%= appname %>/js/templates/',
            'instagram-feed':'app/<%= appname %>/js/templates/',
            'list':'app/<%= appname %>/js/templates/',
            'media-gallery':'app/<%= appname %>/js/templates/',
            'navigation':'app/<%= appname %>/js/templates/',
            'notfound':'app/<%= appname %>/js/templates/',
            'panel':{
                'panel':'app/<%= appname %>/js/templates/',
                'imagePanel':'app/<%= appname %>/js/templates/',
                'videoPanel':'app/<%= appname %>/js/templates/'
            },
            'pinterest':'app/<%= appname %>/js/templates/',
            'plain-text-image':'app/<%= appname %>/js/templates/',
            'related-content-list':'app/<%= appname %>/js/templates/',
            'rich-text-image':'app/<%= appname %>/js/templates/',
            'search-input':'app/<%= appname %>/js/templates/',
            'search-result':'app/<%= appname %>/js/templates/',
            'section-navigation':'app/<%= appname %>/js/templates/',
            'social-share-print':'app/<%= appname %>/js/templates/',
            'tabbed-content':'app/<%= appname %>/js/templates/',
            'twitter-feed':'app/<%= appname %>/js/templates/',
            'video':{
                'video': 'app/<%= appname %>/js/templates/',
                'vimeo': 'app/<%= appname %>/js/templates/',
                'youtube': 'app/<%= appname %>/js/templates/',
                'brightcove': 'app/<%= appname %>/js/templates/',
                'dam': 'app/<%= appname %>/js/templates/'
            }
        }
    },

    // internationalization settings (app.config.geti18NSetting())
    'i18n':{},

    // layout settings (app.config.getLayoutSetting())
    'layout':{},

    // application level default messages (app.config.getMessageSetting())
    'messages':{
        /*'invalidJsonErrName' : ''*/
        'loadErrDefaultTitle' : "Component could not be loaded",
        'loadErrDefaultMsg': "Could not load the requested component",
        'invalidJSONDefaultMsg': "Invalid JSON contract recieved",
        'connectivityErrorMsg': 'Component not loaded due to network connectivity!!'
    },

    // application folder and file nameing convensions (app.config.getConventionsSetting())
    'conventions':{
        'notFoundClass': 'not-found',
        'errorClass': 'error',
        'imagePath': 'app/images/',
        'sassPath': 'app/sass/',
        'jsPath': 'app/js/',
        'cssPath': 'app/css/',
        'fontPath': 'app/css/fonts'
    },

    // other application settings (app.config.getSetting())
    'settings':{
        'errorHandler': 'jsonParseError',
        'missingTemplateHandler': 'missingTemplate'
    },

    // environment detection and settings (app.config.getEvnironmentSetting())
    'environment': {
        'development': ['^localhost','^127.0.0.1', '^10.202.11', '^demo.'],
        'stage': ['^stage','^beta'],
        'production': [],
    },

    // development settings (app.config.getDevelopmentSetting())
    'development': {
        // debug settings(true | false | {}) (app.config.getDebugSetting())
        'debug': true
    },

    // stageserver settings (app.config.getStageSetting())
    'stage': {
        // debug settings(true | false | {}) (app.config.getDebugSetting())
        'debug': true
    },

    // production settings (app.config.getProductionSetting())
    'production': {
        // debug settings(true | false | {}) (app.config.getDebugSetting())
        'debug': false
    }
});
