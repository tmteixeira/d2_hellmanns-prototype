/*global define*/

define(function() {

    'use strict';

    var PlainTextImage = IEA.module('UI.plain-text-image', function(module, app, iea) {

        _.extend(module, {
            // component override logic goes here
        });
    });

    return PlainTextImage;
});
