define(function() {

    'use strict';

    var PinterestFeed = IEA.module('UI.pinterest', function(module, app, iea) {

        _.extend(module, {
            // component override logic goes here
        });
    });

    return PinterestFeed;
});
