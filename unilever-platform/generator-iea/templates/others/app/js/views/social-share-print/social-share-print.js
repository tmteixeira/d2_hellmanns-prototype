/*global define*/
define(function() {

    'use strict';

    var SocialSharePrint = IEA.module('UI.social-share-print', function(module, app, iea) {

        _.extend(module, {
            // component override logic goes here
        });

    });

    return SocialSharePrint;
});
