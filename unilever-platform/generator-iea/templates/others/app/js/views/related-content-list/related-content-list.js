/*global define*/

define(function() {

    'use strict';

    var RelatedContentList = IEA.module('UI.related-content-list', function(module, app, iea) {

        _.extend(module, {
            // component override logic goes here
        });
    });

    return RelatedContentList;
});
