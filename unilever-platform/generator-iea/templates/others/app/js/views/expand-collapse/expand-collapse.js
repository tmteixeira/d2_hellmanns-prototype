/*global define*/

define(function() {

    'use strict';

    var ExpandCollapse = IEA.module('UI.expand-collapse', function(expandCollapse, app, iea) {

        _.extend(expandCollapse, {

            onBeforeInit: function(options) {

            },

            onInit: function(options) {

            },

            onBeforeRender: function(element) {

            },

            onRender: function(element) {

            },

            onEnable: function(element) {
                
            },

            onBeforeExpand: function(element) {

            },

            onAfterExpand: function(element) {

            },

            onBeforeCollapse: function(element) {

            },

            onAfterCollapse: function(element) {

            }

        });
    });

    return ExpandCollapse;
});
