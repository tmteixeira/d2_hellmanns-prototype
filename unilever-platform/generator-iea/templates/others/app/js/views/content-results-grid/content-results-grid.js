/*global define*/

define(function() {

    'use strict';

    var ContentResultGrid = IEA.module('UI.content-results-grid', function(module, app, iea) {

        _.extend(module, {
            onEnable: function() {

            }

        });
    });

    return ContentResultGrid;
});
