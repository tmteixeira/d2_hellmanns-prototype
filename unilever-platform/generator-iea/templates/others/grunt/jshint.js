module.exports = function(grunt, options) {
    return {
        options: {
            jshintrc: '.jshintrc',
            reporter: require('jshint-stylish')
        },
        all: [
            'Gruntfile.js',
            '<%= config.lib %>/js/{,*/}*.js',
            '<%= config.parentDir %>/js/{,*/}*.js',
            '!<%= config.parentDir %>/js/libs/vendor/*',
            '<%= config.appBasePath %>/js/{,*/}*.js',
            '!<%= config.appBasePath %>/js/libs/vendor/*'
        ]
    };
}
