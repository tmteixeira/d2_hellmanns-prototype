module.exports = function (grunt, options) {
    return {
            /********************************************************************************************************/
            iea: ['<%= config.appBasePath %>/js/libs/iea/core'],
            iearesidue: [],
            iearesidue: ['<%= config.appBasePath %>/js/libs/iea/core/js/iea.core.js'],
            /********************************************************************************************************/
            app: ['.tmp'],
            tenant: ['.tmp'],
            dist: ['<%= config.dist %>/*'],
            distresidue: ['<%= config.dist %>/css/<%= config.parentAppName %>.css', '<%= config.dist %>/css/<%= config.parentAppName %>.min.css', '<%= config.dist %>/css/iea.css', '<%= config.dist %>/css/iea.min.css'],
            appresidue: ['<%= config.appBasePath %>/js/libs/iea/core/js/<%= config.name %>.templates.js'],

            /********************************************************************************************************/
            cq: ['<%= config.dist %>/*','!<%= config.dist %>/etc']
        };
}
