module.exports = function(grunt, options) {
    return {
        app: {
            options: {
                templateSettings: {
                    variable: 'data'
                },
                namespace: function(filePath) {
                    //var pieces = filePath.split('/');
                    //return pieces[0];
                    return (options.config.template && options.config.template.parentNamespace) ? options.config.template.parentNamespace : options.config.parentAppName;
                },
                amd: ['handlebars']
            },
            files: {
                '<%= config.appBasePath %>/js/libs/iea/core/js/<%= config.parentAppName %>.templates.js' : ['<%= config.parentDir %>/js/templates/**/*.hbss']
            }
        },

        tenant: {
            options: {
                templateSettings: {
                    variable: 'data'
                },
                namespace: function(filePath) {
                    //var pieces = filePath.split('/');
                    //return pieces[0];
                    return (options.config.template && options.config.template.namespace) ? options.config.template.namespace : options.config.name;
                },
                amd: ['handlebars']
            },
            files: {
                '.tmp/js/<%= config.name %>.templates.js': ['<%= config.appBasePath %>/js/templates/**/*.hbss']
            }
        },

        iea: {
            options: {
                templateSettings: {
                    variable: 'data'
                },
                namespace: function(filePath) {
                    var pieces = filePath.split('/');
                    return pieces[0];
                },
                amd: ['handlebars','helpers']
            },
            files: {
                '<%= config.appBasePath %>/js/libs/iea/core/js/iea.templates.js': ['<%= config.lib %>/js/templates/**/*.hbss']
            }
        }
    };
}
