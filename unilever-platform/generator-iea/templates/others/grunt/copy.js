module.exports = function(grunt, options) {

    options.config.buildTemplatePath = options.config.appBasePath;

    if(options.config.name === 'whitelabel') {
        options.config.buildTemplatePath = 'lib';
    }

    return {
        iea: {
            files: [{
                expand: true,
                dot: true,
                cwd: '<%= config.lib %>',
                dest: '<%= config.appBasePath %>/js/libs/iea/core',
                src: [
                    'js/templates/{,*/}*.*',
                    'css/fonts/{,*/}*.*'
                ]
            }]
        },
        def: {
            files: [{
                expand: true,
                dot: true,
                cwd: '<%= config.appBasePath %>/js/libs/iea/core/',
                dest: '<%= config.dist %>',
                src: [
                    'images/{,*/}*.*',
                    // 'js/{,*/}*.*',
                    'css/{,*/}*.*',
                    'css/fonts/{,*/}*.*'
                ]
            },
            {
                expand: true,
                dot: true,
                cwd: '<%= config.appBasePath %>',
                dest: '<%= config.dist %>',
                src: [
                    '*.{ico,txt}',
                    'images/{,*/}*.{png,jpg,jpeg,gif}',
                    'css/fonts/{,*/}*.*',
                    'data/{,*/}*.*'
                ]
            },
            {
                expand: true,
                dot: true,
                cwd: '.tmp',
                dest: '<%= config.dist %>',
                src: [
                    // 'js/<%= config.name %>.components.js',
                    'css/main.css'
                ]
            }]
        },
        source: {
            files: [{
                expand: true,
                dot: true,
                cwd: '.tmp',
                dest: '<%= config.dist %>',
                src: [
                    'css/iea.css'
                ]
            }]
        },
        cq: {
            files: [{
                expand: true,
                dot: true,
                cwd: '<%= config.dist %>/images',
                dest: '<%= config.cq.images%>',
                src: '{,*/}*.*'
            }, {
                expand: true,
                dot: true,
                cwd: '<%= config.dist %>/css/',
                dest: '<%= config.cq.css%>',
                src: '*.css'
            }, {
                expand: true,
                dot: true,
                cwd: '<%= config.dist %>/css/fonts/',
                dest: '<%= config.cq.fonts%>',
                src: '{,*/}*.*'
            }, {
                expand: true,
                dot: true,
                cwd: '<%= config.dist %>/js/',
                dest: '<%= config.cq.js%>',
                src: '**/*.*'
            }, {
                expand: true,
                dot: true,
                cwd: '<%= config.buildTemplatePath %>/js/templates',
                dest: '<%= config.cq.templates%>',
                src: '**/*'
            }, { // temprory patch for cq build
                expand: true,
                dot: true,
                cwd: '<%= config.dist %>/js/libs/vendor/requirejs',
                dest: '<%= config.cq.js %>',
                src: '**/*'
            }]
        },
        webserver: {
            files: [{
                expand: true,
                dot: true,
                cwd: '<%= config.dist %>',
                dest: 'C:/wamp/www',
                src: [
                    '*.{ico,txt}',
                    'images/{,*/}*.{png,jpg,jpeg,gif}',
                    'css/{,*/}*.*',
                    'js/{,*/}*.*',
                    'data/{,*/}*.*'
                ]
            }]
        },
        boilerplate: {
            files: [{
                expand: true,
                dot: true,
                cwd: '<%= config.parentDir %>',
                dest: 'IEAApp',
                src: [
                    'generator/templates/{,*/}*.*',
                    'app/{,*/}*.*'
                ]
            }, {
                expand: true,
                dot: true,
                cwd: '<%= config.parentDir %>',
                dest: 'IEAApp/app',
                src: [
                    'app/{,*/}*.*'
                ]
            }]
        }
    };
}
