module.exports = function(grunt, options) {
    return {
        files: {
        	dest: '<%= config.dist %>/css/main.css',
        	src: ['<%= config.dist %>/css/iea.css', '<%= config.dist %>/css/<%= config.parentAppName %>.css', '.tmp/css/main.css']
        }
    };
}
