module.exports = function(grunt, options) {
    return {
        iea: {
            files: {}
        },
        dist: {
            files: {
                '.tmp/css/main.css': ['<%= config.appBasePath %>/css/{,*/}*.css']
            }
        }
    };
}
