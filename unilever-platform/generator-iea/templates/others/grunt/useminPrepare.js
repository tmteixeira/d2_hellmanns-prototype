module.exports = function(grunt, options) {
    return {
        html: '<%= config.appBasePath %>/index.html',
        options: {
            dest: '<%= config.dist %>'
        }
    };
}
