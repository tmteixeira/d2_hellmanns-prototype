module.exports = function(grunt, options) {
    var requireConfig = {
        iea: {
            // Options: https://github.com/jrburke/r.js/blob/master/build/example.build.js
            options: {
                baseUrl: '<%= config.lib %>/js',
                optimize: 'none',
                paths: {
                    'iea': '../../<%= config.appBasePath %>/js/libs/iea/core/js/iea.core',
                    'jquery': '../../<%= config.lib %>/js/libs/vendor/jquery/dist/jquery',
                    'underscore': '../../<%= config.lib %>/js/libs/vendor/underscore/underscore',
                    'handlebars': '../../<%= config.lib %>/js/libs/vendor/handlebars/handlebars',
                },
                preserveLicenseComments: false,
                useStrict: true,
                name: 'iea',
                out: '<%= config.appBasePath %>/js/libs/iea/core/js/iea.js',
                mainConfigFile: '<%= config.lib %>/js/iea.js',
                include:['handlebars','helpers','slider','video','popup','brightcoveVideo','validator','tooltip'],
                exclude: ['jquery', 'underscore']
            }
        },
        ieac: {
            options: {
                baseUrl: '<%= config.lib %>/js',
                optimize: 'none',
                paths: {
                    'iea': '../../<%= config.appBasePath %>/js/libs/iea/core/js/iea',
                    'jquery': 'empty:',
                    'underscore': 'empty:',
                    'iea.components': '../../<%= config.appBasePath %>/js/libs/iea/core/js/iea.components',
                    'iea.templates': '../../<%= config.appBasePath %>/js/libs/iea/core/js/iea.templates',
                    'handlebars': '../../<%= config.lib %>/js/libs/vendor/handlebars/handlebars',
                },
                preserveLicenseComments: false,
                useStrict: true,
                name: 'iea.components',
                out: '<%= config.appBasePath %>/js/libs/iea/core/js/iea.components.js',
                mainConfigFile: '<%= config.lib %>/js/iea.js',
                include: [],
                exclude: ['iea','handlebars']
            }
        },
        appc: {
            options: {
                baseUrl: '<%= config.parentDir %>/js',
                optimize: 'none',
                paths: {
                    'iea': '../../../<%= config.appBasePath %>/js/libs/iea/core/js/iea',
                    'iea.components': '../../../<%= config.appBasePath %>/js/libs/iea/core/js/iea.components',
                    'jquery': 'empty:',
                    'underscore': 'empty:',
                    'handlebars': '../../../<%= config.lib %>/js/libs/vendor/handlebars/handlebars'
                },
                preserveLicenseComments: false,
                // findNestedDependencies: true,
                useStrict: true,
                name: '<%= config.parentAppName %>.components',
                out: '<%= config.appBasePath %>/js/libs/iea/core/js/<%= config.parentAppName %>.components.js',
                exclude: ['iea','handlebars'],
                include: []
            }
        },
        tenc: {
            options: {
                baseUrl: '<%= config.appBasePath %>/js',
                optimize: 'none',
                paths: {
                    'iea': '../../../<%= config.appBasePath %>/js/libs/iea/core/js/iea',
                    'iea.components': '../../../<%= config.appBasePath %>/js/libs/iea/core/js/iea.components',
                    'jquery': 'empty:',
                    'underscore': 'empty:',
                    'handlebars': '../../../<%= config.lib %>/js/libs/vendor/handlebars/handlebars',
                },
                preserveLicenseComments: false,
                findNestedDependencies: true,
                useStrict: true,
                name: '<%= config.name %>.components',
                out: '.tmp/js/<%= config.name %>.components.js',
                exclude: ['iea','handlebars']
            }
        },
        dist: {
            // Options: https://github.com/jrburke/r.js/blob/master/build/example.build.js
            options: {
                baseUrl: '<%= config.appBasePath %>/js',
                optimize: 'none',
                paths: {
                    'iea': '../../../<%= config.appBasePath %>/js/libs/iea/core/js/iea',
                    'iea.components': '../../../<%= config.appBasePath %>/js/libs/iea/core/js/iea.components',
                    'jquery': '../../../<%= config.appBasePath %>/js/libs/vendor/jquery/dist/jquery',
                    'underscore': '../../../<%= config.appBasePath %>/js/libs/vendor/underscore/underscore',
                    'handlebars': '../../../<%= config.lib %>/js/libs/vendor/handlebars/handlebars',
                    'config': '../../../config/<%= config.name %>-config'
                },
                preserveLicenseComments: false,
                useStrict: true,
                name: 'main',
                out: '<%= config.dist %>/js/main.js',
                mainConfigFile: ['<%= config.appBasePath %>/js/main.js'],
                include:['iea.components','<%= config.parentAppName %>.components','<%= config.name %>.components']
            }
        }
    };

    // temporary hack to add dyamic asset names to require config.
    requireConfig.appc.options.paths[options.config.parentAppName + '.components'] = '../../../<%= config.appBasePath %>/js/libs/iea/core/js/<%= config.parentAppName %>.components';
    requireConfig.appc.options.paths[options.config.parentAppName + '.templates'] = '../../../<%= config.appBasePath %>/js/libs/iea/core/js/<%= config.parentAppName %>.templates';

    requireConfig.tenc.options.paths[options.config.name + '.components'] = '../../../.tmp/js/<%= config.name %>.components';
    requireConfig.tenc.options.paths[options.config.name + '.templates'] = '../../../.tmp/js/<%= config.name %>.templates';

    requireConfig.dist.options.paths[options.config.parentAppName + '.components'] = '../../../<%= config.appBasePath %>/js/libs/iea/core/js/<%= config.parentAppName %>.components';
    requireConfig.dist.options.paths[options.config.name + '.components'] = '../../../.tmp/js/<%= config.name %>.components';

    return requireConfig;
}
