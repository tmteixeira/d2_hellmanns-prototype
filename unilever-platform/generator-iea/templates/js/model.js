define([
    'underscore',
    'backbone'
], function(_,
    Backbone) {

    'use strict';

    var <%= _.classify(name) %>Model = IEA.Model.extend({
        defaults: {}
    });

    return <%= _.classify(name) %>Model;
});