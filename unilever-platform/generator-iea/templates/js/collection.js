
define([
    'underscore',
    'backbone'
], function(_,
    Backbone) {

	'use strict';

	var <%= _.classify(name) %>Collection = IEA.Collection.extend({

	});

	return <%= _.classify(name) %>Collection;
});