/*global define*/

define([
    'handlebars'
], function(
    Handlebars
) {

	'use strict';

	var <%= _.classify(name) %> = IEA.module('<%= name %>', function(<%= name %>, app, iea) {
        var helpers = (<%= name %>) ? <%= name %> : {};
		_.extend(helpers, {

		});

        this.registerHelpers = function(options) {
            options = options || {};
            for (var helper in helpers) {
                if (helpers.hasOwnProperty(helper)) {
                    Handlebars.registerHelper(helper, helpers[helper]);
                }
            }
            this.triggerMethod('helper:registered');
        };

        // add self init code to fire register helper when module loaded.
        this.addInitializer(this.registerHelpers);
	});

	return <%= _.classify(name) %>;
});
