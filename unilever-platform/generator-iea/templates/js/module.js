/*global define*/

define(function() {

	'use strict';

	var <%= _.classify(name) %> = IEA.module('<%= name %>', function(<%= name %>, app, iea) {
		_.extend(<%= name %>, {

		});
	});

	return <%= _.classify(name) %>;
});
