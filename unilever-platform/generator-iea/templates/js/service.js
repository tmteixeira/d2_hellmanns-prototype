/*global define*/

define(function() {
    'use strict';

    IEA.service('<%= _.classify(name) %>', function(service, app, iea) {

        /**
         * Description
         * @method
         * @return ThisExpression
         */
        var <%= _.classify(name) %> = function() {
            return this;
        };


        // extend defenition prototype
        _.extend(<%= _.classify(name) %>.prototype, {
            initialize: function() {
                return this;
            }

        });

        return <%= _.classify(name) %>;

    });

});


