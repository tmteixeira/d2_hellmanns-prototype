'use strict';

var path = require('path'),
    util = require('util'),
    yeoman = require('yeoman-generator'),
    scriptBase = require('../script-base');
    var backboneUtils = require('../util.js'),
    config = require('../config/config');


var ThemeGenerator = module.exports = function ThemeGenerator() {
  yeoman.generators.NamedBase.apply(this, arguments);
  scriptBase.apply(this, arguments);
  this.sourceRoot(path.join(__dirname, '../templates'));
};

util.inherits(ThemeGenerator, yeoman.generators.NamedBase, scriptBase);

ThemeGenerator.prototype.createViewFiles = function createViewFiles() {
  this.argument('applicationName', { type: String, required: false });

  var themePath = config.app+ '/' + this.applicationName + '/sass/themes/' + this.name;
  this.mkdir(themePath);
  backboneUtils.copyFolderRecursiveSync(this.sourceRoot() + '/sass/theme/fonts', themePath);
  this.copy('sass/theme/_colors.scss', themePath + '/_colors.scss');
  this.copy('sass/theme/_variables.scss', themePath + '/_variables.scss');
  this.copy('sass/theme/_bootstrap-variables.scss', themePath + '/_bootstrap-variables.scss');
};
