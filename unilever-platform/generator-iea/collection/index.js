'use strict';

var path = require('path'),
    util = require('util'),
    yeoman = require('yeoman-generator'),
    scriptBase = require('../script-base'),
    config = require('../config/config');


var CollectionGenerator = module.exports = function CollectionGenerator() {
  yeoman.generators.NamedBase.apply(this, arguments);
  scriptBase.apply(this, arguments);
  this.sourceRoot(path.join(__dirname, '../templates'));
};

util.inherits(CollectionGenerator, yeoman.generators.NamedBase, scriptBase);

CollectionGenerator.prototype.createViewFiles = function createViewFiles() {
  this.argument('applicationName', { type: String, required: false });

  this.copy('js/collection.js', config.app+ '/'+ this.applicationName +'/js/collection/' + this.name + '.js');
};