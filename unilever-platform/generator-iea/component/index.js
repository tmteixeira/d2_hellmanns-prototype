'use strict';

var path = require('path'),
    util = require('util'),
    yeoman = require('yeoman-generator'),
    scriptBase = require('../script-base'),
    config = require('../config/config');


var Generator = module.exports = function Generator() {
  yeoman.generators.NamedBase.apply(this, arguments);
  scriptBase.apply(this, arguments);
  this.sourceRoot(path.join(__dirname, '../templates'));
};

util.inherits(Generator, yeoman.generators.NamedBase, scriptBase);

Generator.prototype.createViewFiles = function createViewFiles() {
  this.argument('applicationName', { type: String, required: false });

  this.dirPath = (typeof this.arguments[1] !== 'undefined') ? '/' + this.arguments[1] : '';
  this.templatePath = config.app+ '/'+ this.applicationName +'/js/templates/' + this.name + '/' + 'defaultView' + '.hbss';
  this.JSONPath = config.app+ '/'+ this.applicationName +'/data/' + this.name + '.json';
  this.sassPath = config.app+ '/'+ this.applicationName +'/sass/views/' + this.name + '/_' + this.name + '.scss';

  this.template('js/view.hbss', this.templatePath);
  this.template('js/view.json', this.JSONPath);
  this.template('js/view.scss', this.sassPath);
  this.copy('js/view.js', config.app+ '/'+ this.applicationName +'/js/views/' + this.name + '/' + this.name + '.js');
};