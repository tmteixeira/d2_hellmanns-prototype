'use strict';

var path = require('path'),
    util = require('util'),
    yeoman = require('yeoman-generator'),
    scriptBase = require('../script-base'),
    config = require('../config/config');


var HtmlTemplateGenerator = module.exports = function HtmlTemplateGenerator() {
  yeoman.generators.NamedBase.apply(this, arguments);
  scriptBase.apply(this, arguments);
  this.sourceRoot(path.join(__dirname, '../templates'));
};

util.inherits(HtmlTemplateGenerator, yeoman.generators.NamedBase, scriptBase);

HtmlTemplateGenerator.prototype.createViewFiles = function createViewFiles() {
  this.argument('applicationName', { type: String, required: false });

  this.copy('js/htmlteamplate.html', config.app+ '/' +this.applicationName + '/' + this.name + '.html');
};