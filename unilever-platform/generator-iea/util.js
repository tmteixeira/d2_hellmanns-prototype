'use strict';
var path = require('path');
var fs = require('fs');


module.exports = {
    rewrite: rewrite,
    rewriteFile: rewriteFile,
    copyFolderRecursiveSync: copyFolderRecursiveSync,
    copyFileSync: copyFileSync
};

function rewriteFile(args) {
    args.path = args.path || process.cwd();
    var fullPath = path.join(args.path, args.file);

    args.haystack = fs.readFileSync(fullPath, 'utf8');
    var body = rewrite(args);

    fs.writeFileSync(fullPath, body);
}

function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
}

function rewrite(args) {
    // check if splicable is already in the body text
    var re = new RegExp(args.splicable.map(function(line) {
        return '\s*' + escapeRegExp(line);
    })
        .join('\n'));

    if (re.test(args.haystack)) {
        return args.haystack;
    }

    var lines = args.haystack.split('\n');

    var otherwiseLineIndex = 0;
    lines.forEach(function(line, i) {
        if (line.indexOf(args.needle) !== -1) {
            otherwiseLineIndex = i;
        }
    });

    var spaces = 0;
    while (lines[otherwiseLineIndex].charAt(spaces) === ' ') {
        spaces += 1;
    }

    var spaceStr = '';
    while ((spaces -= 1) >= 0) {
        spaceStr += ' ';
    }

    lines.splice(otherwiseLineIndex, 0, args.splicable.map(function(line) {
        return spaceStr + line;
    })
        .join('\n'));

    return lines.join('\n');
}

function copyFileSync( source, target ) {

    var targetFile = target;
    if ( fs.existsSync( target ) ) {
        if ( fs.lstatSync( target ).isDirectory() ) {
            targetFile = path.join( target, path.basename( source ) );
        }
    }

    fs.createReadStream( source ).pipe( fs.createWriteStream( targetFile ) );
}

function copyFolderRecursiveSync(source, target) {
    var files = [];

    var targetFolder = path.join( target, path.basename( source ) );
    if ( !fs.existsSync(targetFolder) ) {
        fs.mkdirSync( targetFolder );
    }

    if (fs.lstatSync(source).isDirectory() ) {
        files = fs.readdirSync(source);

        files.forEach( function ( file, index ) {

            var curSource = path.join( source, file );
            var curTarget = path.join( targetFolder);

            if ( fs.lstatSync( curSource ).isDirectory() ) {
                copyFolderRecursiveSync( curSource, curTarget );
            } else {
                copyFileSync( curSource, curTarget );
            }
        } );
    }
}
