'use strict';

var path = require('path'),
    util = require('util'),
    yeoman = require('yeoman-generator'),
    scriptBase = require('../script-base'),
    config = require('../config/config');


var ModuleGenerator = module.exports = function ModuleGenerator() {
  yeoman.generators.NamedBase.apply(this, arguments);
  scriptBase.apply(this, arguments);
  this.sourceRoot(path.join(__dirname, '../templates'));
};

util.inherits(ModuleGenerator, yeoman.generators.NamedBase, scriptBase);

ModuleGenerator.prototype.createViewFiles = function createViewFiles() {
  this.argument('applicationName', { type: String, required: false });

  this.copy('js/module.js', config.app+ '/'+this.applicationName+'/js/module/' + this.name + '.js');
};