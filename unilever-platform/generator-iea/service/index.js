'use strict';

var path = require('path'),
    util = require('util'),
    yeoman = require('yeoman-generator'),
    scriptBase = require('../script-base'),
    config = require('../config/config');


var ServiceGenerator = module.exports = function ServiceGenerator() {
  yeoman.generators.NamedBase.apply(this, arguments);
  scriptBase.apply(this, arguments);
  this.sourceRoot(path.join(__dirname, '../templates'));
};

util.inherits(ServiceGenerator, yeoman.generators.NamedBase, scriptBase);

ServiceGenerator.prototype.createViewFiles = function createViewFiles() {
  this.argument('applicationName', { type: String, required: false });

  this.copy('js/service.js', config.app+ '/'+this.applicationName+'/js/services/' + this.name + '.js');
};