/*global define*/

define(function() {

    'use strict';

    var CountryLanguageSelector = IEA.module('UI.country-language-selector', function (countryLanguageSelector, app, iea) {

        _.extend(countryLanguageSelector, {

            defaultSettings: {
                selectorBody: '.selector-body',
                desktopClass: 'desktop',
                mobileClass: 'mobile',
                selectorDisplayName: '.display-name',
                selectorDisplayFlag: '.display-flag',
                items: '.language-item'
            },

            events: {
                'click .region-title': '_handleAccordionToggle',
                'click .dropdown-menu': '_stopEvent',
                'mouseenter .dropdown-menu': '_stopEvent',
                'mouseenter .dropdown': '_handleToggleDropdown',
                'mouseleave .dropdown': '_handleToggleDropdown'
            },

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
            @method initialize

            @return {null}
            **/
            initialize: function(options) {
                this.triggerMethod('beforeInit');
                this._super(options);
                this.triggerMethod('init');
            },

            /**
            @method render

            @return BlogView
            **/
            render: function() {
                this.triggerMethod('beforeRender');
                this.$el.html(this.template(this.getModelJSON()));
                this.triggerMethod('render', this);
                
                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled=true;
                }

                return this;
            },

            /**
            enable function for the component

            @method enable

            @return {null}
            **/
            enable: function() {
                /*enable logic goes here*/
                this.$el.on('show.bs.dropdown', $.proxy(this.triggerMethod, this, 'beforeOpen'));
                this.$el.on('shown.bs.dropdown', $.proxy(this.triggerMethod, this, 'afterOpen'));
                this.$el.on('hide.bs.dropdown', $.proxy(this.triggerMethod, this, 'beforeClose'));
                this.$el.on('hidden.bs.dropdown', $.proxy(this.triggerMethod, this, 'afterClose'));

                this.$selectDisplayName = $(this.defaultSettings.selectorDisplayName, this.$el);
                this.$selectDisplayFlag = $(this.defaultSettings.selectorDisplayFlag, this.$el);

                this._setPosition();

                this.triggerMethod('enable');
                //this._handleDefaultSelected();
            },

            /**
            show the component

            @method show

            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            hide the component

            @method hide

            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean

            @return {null}
            **/
            clean: function() {
                this._super();
                this.$el.off('show.bs.dropdown');
                this.$el.off('shown.bs.dropdown');
                this.$el.off('hide.bs.dropdown');
                this.$el.off('hidden.bs.dropdown');
            },

            /**
            @method onMatchDesktop

            @return {null}
            **/
            onMatchDesktop: function() {
                this._setCompForDesktopAndTablet();
            },

            /**
            @method onMatchTablet

            @return {null}
            **/
            onMatchTablet: function() {
                this._setCompForDesktopAndTablet();
            },

            /**
            @method onMatchMobile

            @return {null}
            **/
            onMatchMobile: function() {
                $(this.defaultSettings.selectorBody, this.$el).removeClass(this.defaultSettings.desktopClass).addClass(this.defaultSettings.mobileClass);
                this.$selectDisplayName.hide();
            },


            /* ----------------------------------------------------------------------------- *\
               Private Methods
            \* ----------------------------------------------------------------------------- */

            /**
            This method is used to set component for desktop and tablet viewport..

            @method setDisplayNameAndFlag

            @return {null}
            **/
            _setCompForDesktopAndTablet: function() {
                $(this.defaultSettings.selectorBody, this.$el).addClass(this.defaultSettings.desktopClass).removeClass(this.defaultSettings.mobileClass);
                this.$el.find('.country-list').show();

                this.$selectDisplayName.show();
            },

            /**
            This method handles click for Expand collapse of region heading

            @method _handleAccordionToggle

            @return {null}
            **/
            _handleAccordionToggle: function(evt) {
                this.triggerMethod('beforeExpand');

                if(app.getValue('isMobileBreakpoint')){
                    $(evt.currentTarget).toggleClass('expanded').next('.country-list').slideToggle();
                }

                this.triggerMethod('afterExpand');
            },

            /**
            Stop click event on dropdown-menu

            @method _stopEvent

            @return {null}
            **/
            _stopEvent: function (evt) {
                evt.stopPropagation();
            },

            /**
            Stop click event on dropdown-menu

            @method _showDropdown

            @return {null}
            **/
            _setPosition: function() {
                this.$el.each(function () {
                    if($(this).offset().left < $(window).width()/2) {
                        $(this).find('.selector-body').addClass('left-align');
                    }
                    else {
                        $(this).find('.selector-body').addClass('right-align');
                    }
                });
            },

            /**
            Stop click event on dropdown-menu

            @method _showDropdown

            @return {null}
            **/
            _handleToggleDropdown: function (evt) {
                if(app.getValue('isDesktopBreakpoint')) {
                    if (evt.type === 'mouseenter') {
                        $(evt.currentTarget).addClass('open');

                        // Hook for handling mouse enter event
                        this.triggerMethod('mouseenter');
                    } 
                    else {
                        $(evt.currentTarget).removeClass('open');

                        // Hook for handling mouse leave event
                        this.triggerMethod('mouseleave');
                    }
                }
            }

        });
    });

    return CountryLanguageSelector;
});
