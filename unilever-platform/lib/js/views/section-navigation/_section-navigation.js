/*global define*/

define(function() {

    'use strict';

    var SectionNavigation = IEA.module('UI.section-navigation', function (sectionNavigation, app, iea) {

        _.extend(sectionNavigation, {

            defaultSettings: {
                expandEl: '.expandable',
                navCollapse: '.navbar-collapse'
            },

            events: {
                'click .sub-menu > a': '_slideUpDownMenu'
            },

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
            @method initialize

            @return {null}
            **/
            initialize: function(options) {
                this.triggerMethod('beforeInit',options);
                this._super(options);
                this.triggerMethod('init',options);
            },

            /**
            @method render

            @return BlogView
            **/
            render: function() {
                this.triggerMethod('beforeRender', this);
                this.$el.html(this.template(this.getModelJSON()));
                this.triggerMethod('render', this);
                
                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled=true;
                }

                return this;
            },

            /**
            enable function for the component

            @method enable

            @return {null}
            **/
            enable: function() {
                /*enable logic goes here*/
                this.triggerMethod('beforeEnable');
                var self = this;
                $(this.defaultSettings.expandEl, this.$el).find('li').each(function() {
                    if($(this).hasClass('active')) {
                        $(this).parents('.sub-menu').addClass('active');
                        self.showMenu($(this).parents('.sub-menu'));
                    }
                });
                this.triggerMethod('enable');
            },

            /**
            show the component

            @method show

            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            hide the component

            @method hide

            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean

            @return {null}
            **/
            clean: function() {
                this._super();
                // view cleanup code here, anything that needs to be temporarily paused when view is not currently displayed
            },

            /**
            @method gets fired when hits Tablet breakpoint

            @return {null}
            **/
            onMatchTablet: function ()  {
                $(this.defaultSettings.navCollapse, this.$el).addClass('in');
                $(this.defaultSettings.navCollapse, this.$el).height('auto');
            },

            /**
            @method gets fired when moves out from Tablet breakpoint

            @return {null}
            **/
            onUnmatchTablet: function () {
                $(this.defaultSettings.navCollapse, this.$el).removeClass('in');
                $(this.defaultSettings.navCollapse, this.$el).height(0);
            },

            showMenu: function($elem) {
                this.triggerMethod('beforeShowMenu', $elem);

                $elem.addClass('expanded').children(this.defaultSettings.expandEl).slideDown();

                this.triggerMethod('afterShowMenu', $elem);
            },

            hideMenu: function($elem) {
                this.triggerMethod('beforeHideMenu', $elem);

                $elem.removeClass('expanded').children(this.defaultSettings.expandEl).slideUp();

                this.triggerMethod('afterHideMenu', $elem);
            },
             /**
            }
            expand submenu on click

            @method _expandSubMenu

            @return {null}
            **/
            _slideUpDownMenu: function(evt) {
                var $currSubMenu = $(evt.currentTarget).parent();

                $currSubMenu.hasClass('expanded') ? this.hideMenu($currSubMenu) : this.showMenu($currSubMenu);
            }
        });
    });

    return SectionNavigation;
});
