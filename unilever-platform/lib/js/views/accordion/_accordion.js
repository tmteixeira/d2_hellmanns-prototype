/*global define*/

define(function() {

    'use strict';

    var Accordion = IEA.module('UI.accordion', function(module, app, IEA) {

        _.extend(module,{

            events: {
                'click a': '_handleClick'
            },

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
            @method initialize

            @return {null}
            **/
            initialize: function(options) {
                this.triggerMethod('beforeInit');
                this._super(arguments);
                this.triggerMethod('init');
            },

            /**
            @method render

            @return this
            **/
            render: function() {
                this.triggerMethod('beforeRender');
                this.$el.html(this.template(this.model.toJSON()));
                this.triggerMethod('render', this);

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                return this;
            },

            onMatchDesktop: function () {
                // fires on matching desktop
            },

            /**
            enable function for the component

            @method enable

            @return {null}
            **/
            enable: function() {
                this.triggerMethod('beforeEnable');

                /*this.$el.find('.panel-heading').css({
                    'background-color': 'blue'
                });*/

                this.triggerMethod('enable');
            },

            _handleClick: function() {
                this.triggerMethod('click');
            }
        });

    });

    return Accordion;
});
