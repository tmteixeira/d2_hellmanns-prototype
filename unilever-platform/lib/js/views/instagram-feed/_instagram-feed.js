define([
    'imagesloaded'
], function(
    Imagesloaded) {

    'use strict';

    var InstagramFeedComponent = IEA.module('UI.instagram-feed', function (module, app, iea) {

        _.extend(module, {

            events: {},

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
            @method initialize

            @return {null}
            **/
            initialize: function(options) {
                this._super(options);
                this.feedsTemplate = this.getTemplate('partial','instagram-feed/partial/instagram-feeds.hbss');
                this.triggerMethod('init');
            },

            /**
            @method render

            @return BlogView
            **/
            render: function() {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled=true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
            enable function for the component

            @method enable

            @return {null}
            **/
            enable: function() {
                var moduleName = this.moduleName.hyphenToCamelCase(),
                    instagramFeed = this.getModelJSON()[moduleName];

                this._handleStyle();
                this.$instagramFeedWrapper = $('.instagram-feed-wrapper', this.$el);

                if (instagramFeed.maxImageNum) {
                    if (parseInt(instagramFeed.maxImageNum) > 0) {
                        if ((instagramFeed.instagramUserName !== "") && (instagramFeed.instagramUserName !== undefined)) {
                            this._getInstagramUserId();
                        }
                    }
                } else {
                    if ((instagramFeed.instagramUserName !== "") && (instagramFeed.instagramUserName !== undefined)) {
                        this._getInstagramUserId();
                    }
                }

                this.triggerMethod('enable');
            },

            /**
            show the component

            @method show

            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            hide the component

            @method hide

            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean

            @return {null}
            **/
            clean: function() {
                this._super();
            },

            /**
            @method onWindowResized

            @return {null}
            **/
            onWindowResized: function() {
                var self = this;
                this._handleHideCaption();
                $('.feeds', this.$el).removeAttr("style");
                setTimeout(function() {
                    self._matchHeight();
                }, 700);
            },

            /* ----------------------------------------------------------------------------- *\
               Private Methods
            \* ----------------------------------------------------------------------------- */

            /**
            gets the instragram user id

            @method _getInstagramUserId

            @return {null}
            **/
            _getInstagramUserId: function() {
                var self = this,
                    moduleName = this.moduleName.hyphenToCamelCase(),
                    instagramUserName = this.getModelJSON()[moduleName].instagramUserName,
                    requestUrl = "https://api.instagram.com/v1/users/search?q=" + instagramUserName + "&access_token=" + app.getValue('instagramAccessToken'),
                    options = {
                        compEl: this.$el,
                        errorMsg: app.config.getMessageSetting().connectivityErrorMsg,
                        successCallback: function(response) {
                            var instagramUserId = response.data[0].id;
                                self._getInstagramFeed(instagramUserId);
                        }
                    };

                IEA.getJSON(requestUrl, options);
            },

            /**
            creates the subview for feeds

            @method _getInstagramFeed

            @return {null}
            **/
            _getInstagramFeed: function(instagramUserId) {
                var self = this,
                    moduleName = this.moduleName.hyphenToCamelCase(),
                    instagramFeed = this.getModelJSON()[moduleName],
                    hideCaption = instagramFeed.hideCaption,
                    instagramFeedURL = "https://api.instagram.com/v1/users/" + instagramUserId + "/media/recent/?access_token=" + app.getValue('instagramAccessToken') + "&count=" + instagramFeed.maxImageNum,
                    options = {
                        successCallback: function(feeds) {
                            var modalFeeds = new Backbone.Model(feeds),
                                $feedsView = '';

                            if (hideCaption) {
                                modalFeeds.set('isCaption', false);
                            } else {
                                modalFeeds.set('isCaption', true);
                            }
                            $feedsView = self.feedsTemplate(modalFeeds.toJSON());
                            self.$instagramFeedWrapper.append($feedsView);
                        },
                        completeCallback: function(){
                            self._handleHideCaption();
                            self.$instagramFeedWrapper.imagesLoaded( function() {
                                self._matchHeight();
                            });
                        }
                    };

                 IEA.getJSON(instagramFeedURL, options);
            },

            /**
            makes the gird to equal height if any un evnen heights

            @method _articleView

            @return {null}
            **/
            _matchHeight: function () {
                var $grid = $('.feeds', this.$el),
                    elementHeights = $grid.map(function () {
                        return $(this).height();
                    }).get(),
                maxHeight = Math.max.apply(null, elementHeights);
                $grid.height(maxHeight);
            },

            /**
            Hiding the caption if the container is less than 140px

            @method _handleHideCaption

            @return {null}
            **/
            _handleHideCaption: function () {
                var $grid = $('.feeds', this.$el),
                    $gridContainer = $('.instagram-feed-wrapper', this.$el),
                    feedWrapperWidth = $gridContainer.width(),
                    $gridCaption = $('.feeds .thumbnail', this.$el),
                    thumbnailWidth = $gridCaption.width();

                if (!app.getValue('isMobileBreakpoint')) {
                    if (feedWrapperWidth < 340) {
                        $grid.attr('class', 'feeds col-xs-12');
                    } else if (feedWrapperWidth < 540) {
                        $grid.attr('class', 'feeds col-xs-6');
                    } else if (feedWrapperWidth < 740) {
                        $grid.attr('class', 'feeds col-xs-4');
                    }
                }

                
                if (thumbnailWidth < 180) {
                    if(!$gridCaption.hasClass('no-caption')) {
                        $gridCaption.addClass('no-caption');
                    }
                } else {
                    if($gridCaption.hasClass('no-caption')) {
                        $gridCaption.removeClass('no-caption');
                    }
                }
            },

            _handleStyle: function () {
                var $gridContainer = $('.instagram-feed-wrapper', this.$el),
                    feedWrapperWidth = $gridContainer.width();

                if (!app.getValue('isMobileBreakpoint')) {
                    if (feedWrapperWidth < 340) {

                        if(!this.$el.hasClass('x-small-container')) {
                            this.$el.addClass('x-small-container');
                        }

                    } else if (feedWrapperWidth < 540) {

                        if(!this.$el.hasClass('small-container')) {
                            this.$el.addClass('small-container');
                        }

                    } else if (feedWrapperWidth < 740) {

                        if(!this.$el.hasClass('medium-container')) {
                            this.$el.addClass('medium-container');
                        }
                    }
                }
            }

        });
    });

    return InstagramFeedComponent;
});
