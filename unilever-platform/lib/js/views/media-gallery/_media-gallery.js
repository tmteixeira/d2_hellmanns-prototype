/*global define*/

define([
    'popup',
    'slider',
    'video',
], function(
    Popup,
    Slider,
    Video) {

    'use strict';

    var MediaGallery = IEA.module('UI.media-gallery', function(module, app, iea) {

        _.extend(module, {

            defaultSettings: {
                mediaLink:'.media-link',
                mediaSlides: '.media-slides',
                mediaThumbnails: '.media-thumbnails',
                caption: '.caption',
                thumbnail: 'thumbnail'


            },

            events: {
                'click .media-link': '_handleSlideClick',
                'mouseenter .media-link': '_handleSlideHover',
                'mouseleave .media-link': '_handleSlideHover'
            },

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
             * Description
             * @return
             * @method initialize
             * @param {} opts
             * @return
             */
            initialize: function(options) {
                this._super(options);
                this.popupItems = [];
                this.isPopupEnabled = false;
                this.isSliderEnabled = false;
                this.componentVideos = {};

                this.triggerMethod('init');
            },

            /**
             * Description
             * @method render
             * @return ThisExpression
             */
            render: function() {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');

                return this;
            },

            /**
             * Description
             * @method onMatchMobile
             * @return
             */
            onMatchMobile: function() {
                this._disablePopup();
                this._enableSlider();
            },

            /**
             * Description
             * @method onMatchTablet
             * @return
             */
            onMatchTablet: function() {
                this._enablePopup();
                this._disableSlider();
            },

            /**
             * Description
             * @method onMatchDesktop
             * @return
             */
            onMatchDesktop: function() {
                this._enablePopup();
                this._disableSlider();
            },

            /**
             * enable function for the component
             * @return
             * @method enable
             * @return
             */
            enable: function() {
                this.mediaItems = $('.media-link', this.$el);
                this.sliderContainer = $('.media-slides', this.$el);
                this.slidePagination = $('.media-thumbnails', this.$el);
                this.$captions = $('.caption', this.$el);
                this.slides = $('.thumbnail', this.$el);
                this.paginationThumbnails = $('.media-thumbnail', this.$el);

                this.triggerMethod('enable', this);

            },

            /**
             * show the component
             * @return
             * @method show
             * @return
             */
            show: function() {
                this._super();
            },

            /**
             * hide the component
             * @return
             * @method hide
             * @param {} cb
             * @param {} scope
             * @param {} params
             * @return
             */
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
             * Description
             * @return
             * @method clean
             * @return
             */
            clean: function() {
                this._super();
            },

            /**
             * ----------------------------------------------------------------------------- *\
             * Private Methods
             * -----------------------------------------------------------------------------
             * @method _enablePopup
             * @return
             */
            _enablePopup: function() {
                var self = this,
                    $el;

                if (this.isPopupEnabled) {
                    return;
                }

                // kill all videos if its coming from a mobile breakpoint.
                this._killAllVideos();

                // create the item object from the DOM so that the approach can be used
                // for server side compnents
                this.mediaItems.each(function(index, value) {
                    $el = $(this);
                    self.popupItems.push({
                        src: $el.attr('href'),
                        type: $el.attr('type')
                    });
                });

                this.popup = IEA.popup({
                    items: this.popupItems,
                    type: (this.popupItems.length > 1) ? 'gallery' : 'inline'
                });

                this.popup.on('popup:open', function(item) {
                    var $video = $('.video', item.content);
                    self._enableVideo($video, item.index);
                });
                this.popup.on('popup:beforeClose', $.proxy(this._killVideo, this));
                this.popup.on('popup:change', function(item, instance) {
                    // when the slide changes, make sure to kill the previous video , if exists
                    // and enable new item video.
                    setTimeout(function() {
                        var $video = $('.video', instance.content);
                        self._killAllVideos.apply(self, instance);
                        self._enableVideo.apply(self, [$video, instance.index]);
                    }, 300);
                });

                this.triggerMethod('popupEnable', this);

            },

            /**
             * Description
             * @method _enableSlider
             * @return
             */
            _enableSlider: function() {
                var self = this;

                this.$captions.each(function() {
                    $(this).attr('style', '');
                });

                if (this.isSliderEnabled) {
                    this.slider.reload();
                    return;
                }

                this.slider = IEA.slider(this.sliderContainer, {
                    auto: false,
                    pager: false,
                    infiniteLoop: true
                });

                this.slider.enable();

                this.paginationSlider = IEA.slider(this.slidePagination, {
                    auto: false,
                    minSlides: (this.slider.getSlideCount() > 6) ? 6 : this.slider.getSlideCount(),
                    maxSlides: (this.slider.getSlideCount() > 6) ? 6 : this.slider.getSlideCount(),
                    slideWidth: 100,
                    slideMargin: 10,
                    moveSlides: 1,
                    pager: false,
                    infiniteLoop: false,
                    hideControlOnEnd: true
                });

                this.paginationSlider.enable();

                this._bindSliderEvents(this.slider, this.paginationSlider);

                this.mediaItems.each(function(index, elem) {
                    var $video = $('.video', elem);

                    if ($video.length) {
                        self._enableVideo($video, index);
                    }
                });

                this.isSliderEnabled = true;
                this.triggerMethod('sliderEnable', this);
            },

            /**
             * Description
             * @method _disableSlider
             * @return
             */
            _disableSlider: function() {
                if (this.isSliderEnabled) {
                    this.slider.destroy();
                    this.paginationSlider.destroy();

                    this.isSliderEnabled = false;
                }
            },

            /**
             * Description
             * @method _bindSliderEvents
             * @param {} slider
             * @param {} pagination
             * @return
             */
            _bindSliderEvents: function(slider, pagination) {
                var self = this,
                    thumbnails = $('.media-thumbnail', pagination.$el);

                this._slideThumbnail(pagination, thumbnails, slider.getCurrentSlide());

                slider.on('slide:before', function(item, oldIndex, newIndex) {
                    self._slideThumbnail(pagination, thumbnails, newIndex);
                });

                // on thumbnail click the index is used to goto the corresponing slide in the carousel
                pagination.$el.on('click', 'li', function(evt) {
                    evt.preventDefault();
                    slider.goToSlide(_.indexOf(self.paginationThumbnails, evt.currentTarget));
                });

            },

            /**
             * Description
             * @method _slideThumbnail
             * @param {} pagination
             * @param {} thumbnails
             * @param {} newIndex
             * @return
             */
            _slideThumbnail: function(pagination, thumbnails, newIndex) {
                var slideCount = (this.slider.getSlideCount() > 6) ? 6 : this.slider.getSlideCount();
                // remove the existing active and add active class to current slide
                thumbnails.removeClass('active');
                thumbnails.eq(newIndex).addClass('active');

                // only move the slide if the active state has reached the end and there is more
                // thumbs to be shown
                if (pagination.getSlideCount() - newIndex >= slideCount) {
                    pagination.goToSlide(newIndex);
                } else {
                    pagination.goToSlide(pagination.getSlideCount() - slideCount);
                }
            },

            /**
             * Description
             * @method _disablePopup
             * @return
             */
            _disablePopup: function() {
                if ($.magnificPopup.instance.isOpen) {
                    $.magnificPopup.instance.close();
                }

                if (this.isPopupEnabled) {
                    $.magnificPopup.instance.popupsCache = {};
                }
            },

            /**
             * Description
             * @method _enableVideo
             * @param {} $video
             * @param {} index
             * @return
             */
            _enableVideo: function($video, index) {
                var self=this;
                if ($video.length) {
                    var videoURL = $video.data('url'),
                        videoType = $video.data('type');

                    if(videoType === "html5") {
                          this.componentVideos[index] = IEA.video($video, {
                            viewType: videoType,
                            randomNumber: 'video-' + Math.floor(Math.random() * 100),
                            videoFormats: {
                                mp4Video: $video.data('mp4'),
                                oggVideo: $video.data('ogg'),
                                flvVideo: $video.data('flv')
                            },
                            autoplay: true,
                            showControls: true,
                            loopPlayback: false
                        });
                        if(index === 0) {
                            self.slider.redraw();
                        }  
                    }
                    else { 
                        this.componentVideos[index] = IEA.video($video, {
                            viewType: videoType,
                            randomNumber: 'video-' + Math.floor(Math.random() * 100),
                            videoId: videoURL,
                            autoplay: true,
                            showControls: true,
                            loopPlayback: false,
                            audio: true
                        });
                        if(index === 0) {
                            this.componentVideos[index].on('video:loaded', function() {
                                self.slider.redraw();
                            });
                        }
                    }

                    $video.data('video', this.componentVideos[index]);
                }
            },
            /**
             * Description
             * @method _killVideo
             * @param {} item
             * @param {} instance
             * @return
             */
            _killVideo: function(item, instance) {
                if (typeof instance.index !== 'undefined' && typeof this.componentVideos[instance.index] !== 'undefined') {
                    this.componentVideos[instance.index].destroy();
                    delete this.componentVideos[instance.index];
                }
            },

            /**
             * Description
             * @method _stopVideo
             * @param {} content
             * @param {} oldIndex
             * @return
             */
            _stopVideo: function(content, oldIndex) {
                if (typeof this.componentVideos !== 'undefined' && typeof this.componentVideos[oldIndex] !== 'undefined') {
                    this.componentVideos[oldIndex].stop();
                }
            },

            /**
             * Description
             * @method _playVideo
             * @param {} content
             * @param {} oldIndex
             * @param {} newIndex
             * @return
             */
            _playVideo: function(content, oldIndex, newIndex) {
                if (typeof this.componentVideos !== 'undefined' && typeof this.componentVideos[newIndex] !== 'undefined') {
                    this.componentVideos[newIndex].play();
                }
            },

            /**
             * Description
             * @method _killAllVideos
             * @return
             */
            _killAllVideos: function() {
                var index;

                for (index in this.componentVideos) {
                    this.componentVideos[index].destroy();
                    delete this.componentVideos[index];
                }
            },

            /**
             * Description
             * @return
             * @method _handleSlideHover
             * @param {} evt
             * @return
             */
            _handleSlideHover: function(evt) {
                if (!app.getValue('isMobileBreakpoint')) {
                    var $el = $(evt.currentTarget),
                        $caption = $el.find('.caption');

                    if (!$caption.length) {
                        return;
                    }

                    if (evt.type === 'mouseenter') {
                        // since its starting from bottom , call it slideDown
                        $caption.slideDown("fast");
                        $el.parent('.thumbnail').addClass('active');
                    } else {
                        $caption.slideUp("fast");
                        $el.parent('.thumbnail').removeClass('active');
                    }
                }
            },

            /**
             * Description
             * @return
             * @method _handleSlideClick
             * @param {} evt
             * @return
             */
            _handleSlideClick: function(evt) {
                evt.preventDefault();

                if (!app.getValue('isMobileBreakpoint')) {
                    this.popup.open(_.indexOf(this.mediaItems, evt.currentTarget));
                }
            }

        });
    });

    return MediaGallery;
});
