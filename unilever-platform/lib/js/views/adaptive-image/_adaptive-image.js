/*global define*/

define(function() {

    'use strict';

    var AdaptiveImage = IEA.module('UI.adaptive-image', function (module, app, iea) {

        _.extend(module, {

            events: {

            },

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
            @method initialize

            @return {null}
            **/
            initialize: function(options) {
				this.triggerMethod('beforeInit');
                this._super(options);
				this.triggerMethod('init');
            },

            /**
            @method render

            @return BlogView
            **/
            render: function() {
				this.triggerMethod('beforeRender');
                this.$el.html(this.template(this.getModelJSON()));
				this.triggerMethod('render', this);
				
                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled=true;
                }

                return this;
            },

            /**
            enable function for the component

            @method enable

            @return {null}
            **/
            enable: function() {
                /*enable logic goes here*/
				this.triggerMethod('enable');
            },

            /**
            show the component

            @method show

            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            hide the component

            @method hide

            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean

            @return {null}
            **/
            clean: function() {
                this._super();
                // view cleanup code here, anything that needs to be temporarily paused when view is not currently displayed
            }

        });
    });

    return AdaptiveImage;
});
