/*global define*/

define([
    'slider',
    'video',
    'popup',
    'brightcoveVideo'
], function(
    Slider,
    Video,
    Popup,
    BrightcoveVideo) {

    'use strict';

    var Carousel = IEA.module('UI.carousel', function(module, app, iea) {

        _.extend(module, {
            slider: null,
            playerObj: {},
            isYoutubeVideoReadyforFirstTime: false,
            isVimeoVideoReadyforFirstTime: false,
            isBrightcoveVideoReadyforFirstTime: false,
            isVideoLoaded: false,
            isSliderLoaded: false,
            defaultSettings: {
                controls: true,
                auto: false,
                pager: false,
                infiniteLoop: false,
                autoControls: true,
                adaptiveHeight: false,
                video: false
            },
            events: {
                'click li': 'stopRotation'
            },

            videoInstances: {},

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

                /**
            @method initialize

            @return {null}
            **/
            initialize: function(options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
            @method render

            @return Carousel
            **/
            render: function() {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }
                this.triggerMethod('render');

                return this;

            },

            /**
            enable function for the component
            @method enable
            @return {null}
            **/
            enable: function() {
                var $carouselContainer = this.$el.find('.iea-carousel'),
                    modelData = this.getModelJSON(),
                    config = this._carouselInitialization(modelData),
                    moduleName = this.moduleName.hyphenToCamelCase();

                this.carouselId = modelData.randomNumber;

                this.slideCollection = new IEA.Collection(modelData[moduleName].slides);

                this.$el.css('overflow', 'hidden');
                $carouselContainer.css('width', 99999+'px');
                this.$el.find('li').css('float', 'left');

                this.slider = IEA.slider($carouselContainer, config);

                if(this.slideCollection.length > 1 && !this.isSliderLoaded) {
                    this.slider.on('slide:after', $.proxy(this._onSliderAfter, this));
                    this.slider.on('slider:load', $.proxy(this.loadCarouselVideos, this));
                    this.slider.enable();
                    this.isSliderLoaded = true;
                } else {
                    this.loadCarouselVideos();
                }

                this.triggerMethod('enable');
            },

            destroy: function() {
                if(this.slider) {
                    this.slider.destroy();
                    this.isSliderLoaded = false;
                    this.isVideoLoaded = false;
                }
            },

            reload: function() {
                this.slider.reload();
            },

            getCurrentSlide: function() {
                if(this.isSliderLoaded) {
                    return this.slider.getCurrentSlide();
                }

                return false;
            },

            getSlideCount: function() {
                if(this.isSliderLoaded) {
                    return this.slider.getSlideCount();
                }

                return false;
            },

            play: function() {
                if(this.isSliderLoaded) {
                    this.slider.play();
                }
            },

            pause: function() {
                if(this.isSliderLoaded) {
                    this.slider.pause();
                }
            },

            goToSlide: function(index) {
                if(this.isSliderLoaded) {
                    this.slider.goToSlide(index);
                }
            },

            loadCarouselVideos: function(){
                var slideCount = this.slideCollection.length,
                    videoConfig, videoNode,
                    self = this;

                //timeout added just to minimize conflict between inline video and carousel video
                setTimeout(function(){
                    if(!self.isVideoLoaded) {
                        for (var i = 0; i < slideCount; i++) {
                            videoConfig = self.slideCollection[i].panel.video;
                            if(videoConfig){
                                // TODO: Get a better selector
                                videoNode = self.slider.$el.children().not('.bx-clone').filter(':eq('+i+')');
                                self.renderVideo(videoNode, videoConfig, i);
                            }
                        }

                        self.isVideoLoaded = true;
                        self.reload();
                    }
                }, 100);
            },

            renderVideo: function(node, config, slideIndex) {
                var self = this,
                    customConfig = config,
                    container = $(node).find('.video-frame'),
                    videoObj = this.slideCollection[slideIndex].panel.video;

                videoObj.baseConfig = {
                    autoPlay: config.autoPlay
                };

                // For embedded video passing autoplay false as default
                if (customConfig.displayOption !== 'Overlay') {
                    // setting the embedded carousel videos as autoplay as false.
                    customConfig.autoPlay = 'false';
                }
                // append carouselId in randomNumber to avoid panel conflict
                customConfig.randomNumber = 'video_' + this.carouselId + '_' + customConfig.randomNumber;

                //Add video instance in slide collection
                if(customConfig.displayOption === 'Overlay') {

                    videoObj.$overlayLink = $(node).find('a.video-overlay'),
                    videoObj.$close = $(node).find('button.mfp-close'),
                    videoObj.container = $(container).parents('.video-content');

                    videoObj.popup = new IEA.popup({
                        type: 'inline',
                        preloader: false,
                        callbacks: {
                            beforeOpen: function() {
                                self._initializeVideo(slideIndex, videoObj.container, customConfig);
                                self.slider.pause();
                            },
                            open: function() {
                                var $close = $('button.mfp-close', '.video-content');

                                $close.on('click', function(e) {
                                    e.preventDefault();
                                    e.stopPropagation();
                                    videoObj.popup.close();
                                });
                            },
                            close: function(){
                                videoObj.instance.destroy();
                            }
                        }
                    });

                    videoObj.$overlayLink.off('click.popup').on('click.popup', function(){
                        videoObj.popup.open(videoObj.container);
                    });

                    videoObj.$close.on('click', function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                        videoObj.popup.close();
                    });
                } else {
                    this._initializeVideo(slideIndex, container, customConfig);
                }
            },

            /**
            show the component

            @method show

            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            hide the component

            @method hide

            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean

            @return {null}
            **/
            clean: function() {
                this._super();
                this.isYoutubeVideoReadyforFirstTime = false;
                this.isVimeoVideoReadyforFirstTime = false;
                this.isBrightcoveVideoReadyforFirstTime = false;
            },

            /**
            handle vcartousel click

            @method stopRotation

            @return {null}
            **/
            stopRotation: function(evt) {
                if(this.isSliderLoaded) {
                    this.slider.pause();
                }
            },

            startRotation: function(evt) {
                if(this.isSliderLoaded) {
                    this.slider.play();
                }
            },

            /**
            @method _carouselInitialization

            @return {null}
            **/
            _carouselInitialization: function(data) {
                var configuration = this.defaultSettings,
                    slideCount = this.$el.find('.iea-carousel li').length,
                    moduleName = this.moduleName.hyphenToCamelCase(),
                    intervalsOfRotation = parseInt(data[moduleName].intervalsOfRotation) * 1000;

                if (intervalsOfRotation >= 1000) {
                    if (slideCount > 1) {
                        configuration = $.extend(configuration, {
                            pause: intervalsOfRotation,
                            auto: true,
                            pager: true,
                            infiniteLoop: true
                        });
                    }
                } else {
                    if (slideCount > 1) {
                        configuration = $.extend(configuration, {
                            auto: false,
                            pager: true,
                            infiniteLoop: true
                        });
                    }
                }

                return configuration;
            },

            _onSliderAfter: function(elem, previousIndex, currentIndex){

                var slideCollection = this.slideCollection,
                currentSlide = slideCollection[currentIndex].panel,
                previousSlide = slideCollection[previousIndex].panel;

                if(currentSlide.video && currentSlide.video.displayOption !== 'Overlay' && currentSlide.video.baseConfig.autoPlay === 'true'){
                    this.slider.pause();
                    //this.videoInstances[currentIndex].instance.play();
                    currentSlide.video.instance.play();
                }

                if(previousSlide.video && previousSlide.video.displayOption !== 'Overlay'){

                    previousSlide.video.instance.pause();
                }

                // also publish an event from here which can be overridden for specific functionality
            },

                        /**
            * This method is used to intialize video using Video Service and handling its events
            */
            _initializeVideo: function(slideIndex, container, customConfig) {
                var self = this,
                    videoObj = this.slideCollection[slideIndex].panel.video;

                videoObj.instance = IEA.video(container, customConfig);

                // Subscribe to Youtube event and store player events
                videoObj.instance.on('youtube:onReady', function(evt, playerId){
                    self.playerObj[playerId] = evt;
                    if(!self.isYoutubeVideoReadyforFirstTime) {
                        //Reload the slider after video is ready
                        self.slider.reload();
                        self.isYoutubeVideoReadyforFirstTime = true;
                    }
                });

                // Subscribe to Vimeo event and store
                videoObj.instance.on('vimeo:ready', function(evt, vimeoObject, playerSource){
                    self.playerObj[slideIndex] = {
                        event: evt,
                        obj: vimeoObject,
                        src: playerSource
                    };
                    if(!self.isVimeoVideoReadyforFirstTime) {
                        //Reload the slider after video is ready
                        self.slider.reload();
                        self.isVimeoVideoReadyforFirstTime = true;
                    }
                });

                videoObj.instance.on('brightcove:templateReadyHandler', function(player, evt){
                    self.playerObj[slideIndex] = player;
                    if(!self.isBrightcoveVideoReadyforFirstTime) {
                        //Reload the slider after video is ready
                        self.slider.reload();
                        self.isBrightcoveVideoReadyforFirstTime = true;
                    }
                });
            }
        });

    });

    return Carousel;
});
