define(function() {

    'use strict';

    var TwitterFeed = IEA.module('UI.twitter-feed', function (module, app, iea) {

        _.extend(module, {

            events: {},

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
            @method initialize

            @return {null}
            **/
            initialize: function(options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
            @method render

            @return BlogView
            **/
            render: function() {
                var self = this,
                    componentData = self.getModelJSON(),
                    widgetId = app.getValue('twitterWidgetId');

                if (widgetId === 'undefined' || widgetId === '') {
                    console.info('%c Twitter Widget ID Not Available : ', 'background: #ffb400; color: #fff', widgetId);
                }

                // add twitterWidgetId from tenant config to component data object
                componentData.tenant = {
                    twitterWidgetId: widgetId
                };

                self.$el.html(self.template(componentData));

                if(self._isEnabled === false) {
                    self.enable();
                    self._isEnabled = true;
                }

                self.triggerMethod('render');

                return this;
            },

            /**
            enable function for the component

            @method enable

            @return {null}
            **/
            enable: function() {
                var self = this,
                    options = {
                        compEl: this.$el,
                        errorMsg: app.config.getMessageSetting().connectivityErrorMsg,
                        successCallback: function(script, textStatus) {
                            twttr.ready(function (twttr) {
                                self.triggerMethod('beforeEnable', self);
                                twttr.widgets.load();
                                self.triggerMethod('enable', self);

                                twttr.events.bind('rendered', function (event) {
                                    //adjusting the tweet height not to overflow
                                    setTimeout(function() {
                                        var twitterHeight = $('.twitter-timeline', self.el).height() + 60;
                                        $('.twitter-timeline', self.el).css('height', twitterHeight + "px");
                                    }, 400);

                                    $('iframe#twitter-widget-0', self.el).contents().find('head').append('<style> .var-chromeless .tweet {padding-bottom:9px !important; border-bottom: 1px solid #ccc; margin-bottom:5px;}</style>');
                                });
                            })
                        },
                        failuerCallBack: function() {
                            self.$el.find('.twitter-timeline').hide();
                        }
                    }

                IEA.getScript('https://platform.twitter.com/widgets.js', options);
            },

            /**
            show the component

            @method show

            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            hide the component

            @method hide

            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean

            @return {null}
            **/
            clean: function() {
                this._super();
            },

            /* ----------------------------------------------------------------------------- *\
               Private Methods
            \* ----------------------------------------------------------------------------- */

            /**
            Handle the scaling of typography based on width.

            @method _handleStyle

            @return {null}
            **/
            _handleStyle: function () {
                var $gridContainer = $('.twitter-feed-wrapper', this.$el),
                    feedWrapperWidth = $gridContainer.width();

                if (!app.getValue('isMobileBreakpoint')) {
                    if (feedWrapperWidth < 340) {

                        if(!this.$el.hasClass('x-small-container')) {
                            this.$el.addClass('x-small-container');
                        }

                    } else if (feedWrapperWidth < 540) {

                        if(!this.$el.hasClass('small-container')) {
                            this.$el.addClass('small-container');
                        }

                    } else if (feedWrapperWidth < 740) {

                        if(!this.$el.hasClass('medium-container')) {
                            this.$el.addClass('medium-container');
                        }
                    }
                }
            }

        });
    });

    return TwitterFeed;
});
