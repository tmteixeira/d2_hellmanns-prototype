/*global define*/

define([
    'validator',
    'datepicker',
    'popup'
], function(
    Validator,
    Datepicker,
    Popup) {

    'use strict';

    var Form = IEA.module('UI.form', function(module, app, iea) {

        _.extend(module, {
            defaultSettings: {
                calenderInput: '.input-calendar',
                calenderTodayBtn: 'linked',
                calenderAutoclose: true,
                calenderTodayHighlight: true,
                popupClass: 'white-popup-block',
                popupOpenType: 'inline',
                multivalueField: '.multivalue-field'
            },
            events: {
                'submit form': '_handleFormSubmit',
                'reset form': '_handleFormReset',
                'click .add-input': '_addMultiValueField',
                'click .remove-input': '_removeInput',
                'click .calendar-input': '_openCalendar'
            },

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
             * intialize form component
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function(options) {
                this._super(options);
                this.validator = app.form.validator; // need to include validation DI to define.
                this.formCalendarLoaded = false;
                this.changedSet = {};
                this.isvalidationRequired = false;
                this.isFormValid = false;
                this.isSubmitted = false;
                this.multiValueFields = {};

                this.validator.addValidation(this, {
                    model: this.model
                });

                this.triggerMethod('init');
            },

            /**
             * render form view
             * @method render
             * @return ThisExpression
             */
            render: function() {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');

                return this;
            },

            /**
             * enable form
             * @method enable
             * @return
             */
            enable: function() {
                this.triggerMethod('beforEnable');

                var moduleName = this.moduleName.hyphenToCamelCase(),
                    modelData = this.getModelJSON()[moduleName],
                    allElementsConfig = modelData.formElementsConfig,
                    idx = 0,
                    item, $elem;

                this.$form = $('form', this.$el);
                this.$formElements = this.$form.find(':input');
                this.popup = IEA.popup();
                this.form = {
                    isAjax: this.$form.data('ajax') || (modelData.isAjax) ? modelData.isAjax : false,
                    postURL: this.$form.attr('action') || (modelData.servletPath) ? modelData.servletPath : '',
                    failureMessage: this.$form.data('fail') || (modelData.failureMessage) ? modelData.failureMessage : 'Submission Failed',
                    successMessage: this.$form.data('success') || (modelData.successMessage) ? modelData.successMessage : 'Thank you for your submission',
                    validation: this.$form.data('validation') || (modelData.validation) ? modelData.validation : false,
                    successRedirectURL: this.$form.data('redirect') || (modelData.redirect) ? modelData.redirect : false,
                    errorRedirectURL: this.$form.data('redirect') || (modelData.failureRedirect) ? modelData.failureRedirect : false
                };

                this.setElementActions();

                // add validation rules from the model into the validator engine.
                this.validator.addRules(this, {
                    rules: this._createValidationRules()
                });

                // Getting all the multivalue field together
                for(idx=0; idx < allElementsConfig.length; idx++) {
                    item = allElementsConfig[idx].formElement;
                    if(item && item.multivalue) {
                        // adding rule to the first elemnet of multivalue
                        this.multiValueFields[item.name] = {
                            'name': item.name,
                            rules: this.model.validation[item.name]
                        };

                        $elem = $('input[name^='+item.name+']');
                        this._setUpMultiValueField($elem, item.name, item.name+'_1' );

                        delete this.model.validation[item.name];
                    }
                }

                this.triggerMethod('enable');
            },

            /**
             * set element behaviours
             * @method setElementActions
             * @return
             */
            setElementActions: function() {
                var settings = this.defaultSettings,
                    $dateInput = $(settings.calenderInput, this.$el);

                if (this.formCalendarLoaded === false) {
                    $dateInput.datepicker({
                        format: $dateInput.data('format'),
                        todayBtn: settings.calenderTodayBtn,
                        autoclose: settings.calenderAutoclose,
                        todayHighlight: settings.calenderTodayHighlight,
                        orientation: app.getValue('isMobileBreakpoint') ? 'top' : 'auto'
                    }).on('changeDate', function(ev){
                        $dateInput.datepicker('hide');
                    });

                    this.formCalendarLoaded = true;

                    $dateInput.on('focus', function(evt) {
                        if(!app.getValue('isDesktopBreakpoint')) {
                            $dateInput.blur();
                        }
                    });
                }

            },

            /**
             * unset all the element behaviours, used when switching viewport
             * @method unsetElementActions
             * @return
             */
            unsetElementActions: function() {
                $dateInput.datepicker('remove');
                this.formCalendarLoaded = false;
            },

            /**
             * on match desktop do some action
             * @method onMatchDesktop
             * @return
             */
            onMatchDesktop: function() {
                this.setElementActions();
            },

            /**
             * on unmatch desktop do some action
             * @method onUnmatchDesktop
             * @return
             */
            onUnmatchDesktop: function() {
                this.setElementActions();
            },

            /**
             * show copmnent
             * @method show
             * @return
             */
            show: function() {
                this._super();
            },

            /**
             * hide component
             * @method hide
             * @param {} cb
             * @param {} scope
             * @param {} params
             * @return
             */
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
             * clean compnent on exist from the component
             * @method clean
             * @return
             */
            clean: function() {
                this._super();
                this.unsetElementActions();
                this.validator.removeValidation(this, {
                    model: this.model
                });
            },

            /* ----------------------------------------------------------------------------- *\
               Private Methods
            \* ----------------------------------------------------------------------------- */

            /**
             * create validation rules from the model data and then create a the validation rules which
             * then is set to the form validation engine
             * @method _createValidationRules
             * @return validationRules
             */
            _createValidationRules: function() {
                var self = this,
                    moduleName = this.moduleName.hyphenToCamelCase(),
                    formElements = this.getModelJSON()[moduleName].formElementsConfig,
                    element, elementRule = {},
                    validationRules = {},
                    idx = 0,
                    jdx = 0,
                    pattern;

                for(idx = 0; idx<formElements.length; idx++) {
                    element = formElements[idx];
                    if(!_.isFunction(element.formElement)) {
                        validationRules[element.formElement.name] = [];
                        if (typeof element.formElement.rules !== 'undefined' && _.isArray(element.formElement.rules)) {

                            for (jdx = 0; jdx<element.formElement.rules.length; jdx++) {
                                self.isvalidationRequired = true;
                                elementRule = element.formElement.rules[jdx];
                                validationRules[element.formElement.name].push({
                                    xss: true
                                });
                                if (_.has(elementRule, 'pattern')) {
                                    if(elementRule.pattern === 'creditcard') {
                                        validationRules[element.formElement.name].push({
                                            creditcard: true,
                                            msg: elementRule.msg
                                        });
                                    } else {
                                        pattern = decodeURIComponent(elementRule.pattern).replace(/%24/g, '$');
                                        validationRules[element.formElement.name].push({
                                            pattern: this.validator.patterns[pattern] || new RegExp(pattern),
                                            msg: elementRule.msg
                                        });
                                    }
                                    
                                } else {
                                    validationRules[element.formElement.name].push(elementRule);
                                }
                            }

                        }
                    }
                }

                return validationRules;
            },

            /**
             * handle submit
             * @method _handleFormSubmit
             * @param {} evt
             * @return
             */
            _handleFormSubmit: function(evt) {
                var formData = IEA.serializeFormObject(this.$form),
                    submitxhr, idx =0, key, item;

                evt.preventDefault();

                this.model.unset(this.changedSet);
                this.model.set(formData);
                this.changedSet = this.model.changed;

                // Check if the model is valid before saving
                // See: http://thedersen.com/projects/backbone-validation/#methods/isvalid
                if (!this.isvalidationRequired || (this.isvalidationRequired && this.model.isValid(true))) {
                    this.isFormValid = true;
                } else {
                    this.isFormValid = false;
                }

                for(idx in this.multiValueFields) {
                    var multiValueArray = [],
                        match, elem;
                    item = this.multiValueFields[idx];

                    // Get the value from input field and put it back to its hidden field
                    for(key in formData) {
                        match = key.match(item.name);
                        if(match && match.length > 0) {
                            elem = $('input[name='+key+']');
                            elem.siblings('input[type=hidden]').val(elem.val());
                        }
                    }

                    formData[item] = multiValueArray;
                }

                if(this.$errorBlock) {
                    this.$errorBlock.hide();
                }

                if (this.isFormValid && this.form.postURL !== '' && this.isSubmitted === false) {
                    if (this.form.isAjax) {

                        submitxhr = $.post(this.form.postURL, this.$form.serialize());
                        submitxhr.done($.proxy(this._successHandler, this));
                        submitxhr.fail($.proxy(this._errorHandler, this));

                    } else {
                        this.$form[0].submit();
                    }
                    this.triggerMethod('formSubmit', this);
                    this.isSubmitted = true;
                }
            },

            /**
             * handle form reset. all the errors and fields should be reset
             * @method _handleFormReset
             * @return
             */
            _handleFormReset: function() {
                var self = this,
                    formData = IEA.serializeFormObject(this.$form);

                // remove all the data from the model
                this.model.unset(formData);

                this.isFormValid = false;
                this.isSubmitted = false;

                // loop throught each element and remove the error classes
                this.$formElements.each(function() {
                    self.validator.callbacks.valid(self, $(this).attr('name'));
                });

                // removing multivalue field other than first one
                $('.form-group .multivalue').each(function(index) {
                    $(this).find(self.defaultSettings.multivalueField).each(function(index) {
                        if(index === 0) {
                            $(this).find('.remove-input').removeClass('remove-input').addClass('add-input')
                                   .find('.glyphicon-minus').removeClass('glyphicon-minus').addClass('glyphicon-plus');
                        } else {
                            $(this).remove();
                        }
                    });
                });

                this.triggerMethod('formReset', this);
            },

            /**
             * on success submission of the form
             * @method _successHandler
             * @param {} data
             * @return
             */
            _successHandler: function(data) {

                if(data.responseHeaders.status === 200) {
                    if (typeof this.form.successRedirectURL !== 'undefined' && this.form.successRedirectURL !== '' && this.form.successRedirectURL !== false) {
                        // if the router is running, then make sure its using hash url change
                        // otherwise change the complete URL
                        if (IEA.History.started) {
                            Router.navigate(this.form.successRedirectURL, {
                                trigger: true
                            });
                        } else {
                            window.location.href = this.form.successRedirectURL;
                        }
                    } else {
                        this.$sucessMsgBlock = $('<div class="form-success-msg"><span class="glyphicon glyphicon-ok"></span><span class="success-msg-text">' + this.form.successMessage + '</span></div>');
                        this.$form.before(this.$sucessMsgBlock);
                        this.$form.hide();
                    }
                } else {
                    this._errorHandler();
                }
                this.triggerMethod('success', this);
                this.isSubmitted = false;
            },

            /**
             * on form submission error
             * @method _errorHandler
             * @param {} err
             * @return
             */
            _errorHandler: function(err) {

                if (typeof this.form.errorRedirectURL !== 'undefined' && this.form.errorRedirectURL !== '' && this.form.errorRedirectURL !== false) {
                    // if the router is running, then make sure its using hash url change
                    // otherwise change the complete URL
                    if (IEA.History.started) {
                        Router.navigate(this.form.errorRedirectURL, {
                            trigger: true
                        });
                    } else {
                        window.location.href = this.form.errorRedirectURL;
                    }
                } else {
                    this.$errorBlock = $('<div class="form-error-msg text-danger"><span class="glyphicon glyphicon-warning-sign"></span><span class="error-msg-text">' + this.form.failureMessage + '</span></div>');
                    this.$form.before(this.$errorBlock);
                }

                this.triggerMethod('error', this.form.failureMessage);
                this.isSubmitted = false;
            },

            /**
             * Description
             * @method _addMultiValueField
             * @return elemObj
             */
            _addMultiValueField: function (evt) {

                var addInput = $(evt.currentTarget),
                    parentField = addInput.parents(this.defaultSettings.multivalueField),
                    multiInputFields = addInput.parents('.multivalue ').find(this.defaultSettings.multivalueField),
                    inputLimit = addInput.parent().attr('data-input-limit');

                // If limit is not provided put some bigger value
                inputLimit = inputLimit ? inputLimit : 100000;

                if(inputLimit && inputLimit > multiInputFields.length) {

                    var toBeAdded = $(parentField[0]).clone(true),
                        attrName = toBeAdded.find('input[type=text]').attr('name').slice(0, -2),
                        newAttrName = attrName + '_' + (multiInputFields.length +1),
                        inputElem =  toBeAdded.find('input').val('');


                    $(parentField[0]).find('.add-input')
                                          .removeClass('add-input')
                                          .addClass('remove-input');

                    $(parentField[0]).find('.glyphicon-plus')
                                          .removeClass('glyphicon-plus')
                                          .addClass('glyphicon-minus');

                    $(parentField[0]).after(toBeAdded);
                    this._setUpMultiValueField(inputElem, attrName, newAttrName);
                } else {
                    alert('Limit reached');
                }

            },

            /**
             * Description
             * @method _setUpMultiValueField
             * @return
             */
            _setUpMultiValueField: function(elem, attrName, newAttrName) {
                var newRule = {};

                elem.attr('name', newAttrName);
                elem.siblings('input[type=hidden]').attr('name', attrName);

                newRule[newAttrName] = this.multiValueFields[attrName].rules;

                this.validator.addRules(this, {
                    rules: newRule
                });
            },

            /**
             * Description
             * @method _removeInput
             * @return
             */
            _removeInput: function(evt) {

                var minusIcon = $(evt.currentTarget),
                    inputField = minusIcon.parents(this.defaultSettings.multivalueField);

                inputField.remove();
            },

            /**
             * Description
             * @method _openCalendar
             * @return
             */
            _openCalendar: function(evt) {
                var calIcon = $(evt.currentTarget),
                    inputField = calIcon.siblings('input');
                inputField.focus();
            },

            /**
             * Description
             * @method _populateElems
             * @return elemObj
             */
            _populateElems: function() {
                var elems = this.form.elements,
                    errorNode = null,
                    elemObj = {};
                for (var elem in elems) {
                    if (typeof elems[elem] === 'object' && ['submit', 'reset'].indexOf(elems[elem].type) === -1) {
                        errorNode = $(elems[elem]).parents('.row:first').find('.error')[0];
                        elemObj[elems[elem].name] = {
                            node: elems[elem],
                            type: elems[elem].type,
                            required: elems[elem].dataset.required || 'undefined',
                            constraint: unescape(elems[elem].dataset.constraint),
                            errorRequired: errorNode.dataset['errorRequired'],
                            errorConstraint: errorNode.dataset['errorConstraint'],
                            errorNode: errorNode
                        };
                    }
                }
                return elemObj;
            }
        });
    });

    return Form;
});
