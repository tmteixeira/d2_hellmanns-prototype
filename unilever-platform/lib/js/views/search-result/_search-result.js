/*global define*/
define([
    'paginator'
], function(
    Paginator) {

    'use strict';

    var SearchResult = IEA.module('UI.search-result', function (module, app, iea) {

        _.extend(module, {
            defaultSettings: {
                loadmore: '.load-more',
                searchContainer: '.search-result-container',
                currentRecordEl: '.start',
                totalRecordEl: '.total',
                activeclass: 'active',
                mode: 'server'
            },
            events: {
                'click .load-more': '_handleLoadMore'
            },

            /**
            @method initialize
            @return {null}
            **/
            initialize: function(options) {
                this._super(options);
                var searchResultData = this.getModelJSON(),
                    searchModel = Backbone.Model.extend({}),
                    SearchCollection = null,
                    moduleName = this.moduleName.hyphenToCamelCase(),
                    searchResults = searchResultData[moduleName];

                if(typeof searchResultData === 'undefined' || typeof searchResults === 'undefined') {
                    return;
                }

                this.loadUrl = (searchResults.loadMoreConfig) ? searchResults.loadMoreConfig.loadMoreBaseUrl : '';
                this.loadUrlParam = (searchResults.loadMoreConfig) ? searchResults.loadMoreConfig.loadMoreAjaxUrl : '';
                this.searchArticleTemplate = this.getTemplate('articles', 'search-result/partial/search-result-article.hbss');
                this.pageViewLimit = (searchResults.resultSize > 0) ? searchResults.resultSize : 1;
                this.isFirstPage = true;

                SearchCollection = Backbone.PageableCollection.extend({
                        model: searchModel,
                        url: this.loadUrl,
                        queryParams: {
                            searchParams: this.loadUrlParam
                        }
                    });

                this.searchArticles = new SearchCollection(searchResults.results, {
                    mode: this.defaultSettings.mode,
                    state: {
                        pageSize: this.pageViewLimit,
                        totalRecords: searchResults.totalResults
                    }
                });
                this.triggerMethod('init');
            },

            /**
            @method render
            @return SearchResult
            **/
            render: function() {
                var settings = this.defaultSettings;
                this.$el.html(this.template(this.getModelJSON()));
                this.$searchList = $(settings.searchContainer, this.$el);
                this.$loadmore = $(settings.loadmore, this.$el);

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled=true;
                }

                this.triggerMethod('render');

                return this;
            },

            /**
            @description:enable function for the component , load first set of record
            @method enable
            @return {null}
            **/
            enable: function() {
                /*Load first set of record*/
                this._searchView();
                this.triggerMethod('enable', this);
            },

            /**
            @description: Show the component
            @method show
            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            @description: Hide the component
            @method hide
            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean
            @return {null}
            **/
            clean: function() {
                this._super();
            },

            /**
            @description: creates search result item view
            @method _searchView
            @return {null}
            **/
            _searchView: function() {
                var self = this;

                if (this.isFirstPage) {
                    var resultModel = this._pagination();
                    this._renderSubView(resultModel,self, true);

                    this.triggerMethod('firstPage', resultModel);

                } else {
                    self._pagination().done(function(response){
                        self.$loadmore.removeClass(self.defaultSettings.activeclass);
                        self._renderSubView(response, self, false);

                        self.triggerMethod('nextPage', response);
                    })
                    .error(function(err){
                        self.triggerMethod('error', err);
                        console.log('%c Error : ', 'background: #ff0000; color: #fff',  err.responseText);
                    });
                }
            },

            /**
            @description: render search result item view and update DOM
            @method _renderSubView
            @return {null}
            **/
            _renderSubView: function(response, self, isFirstPage) {
                var $currentRecordEl = this.$el.find(self.defaultSettings.currentRecordEl),
                    $totalRecordEl = this.$el.find(self.defaultSettings.totalRecordEl),
                    moduleName = this.moduleName.hyphenToCamelCase(),
                    resultModel = (isFirstPage) ? response : response.data[moduleName].results,
                    resultView = (isFirstPage) ? self.searchArticleTemplate(resultModel.toJSON()) : self.searchArticleTemplate(resultModel),
                    totalPagesShowing = (isFirstPage) ? resultModel.length : self.searchArticles.state.currentPage * self.pageViewLimit,
                    totalRecords = (isFirstPage) ? self.searchArticles.state.totalRecords: response.data[moduleName].totalResults,
                    showingCount = 0;

                self.loadUrl = (isFirstPage) ? response.url : response.data[moduleName].loadMoreConfig.loadMoreBaseUrl;
                self.loadUrlParam = (isFirstPage) ? resultModel.queryParams.searchParams : response.data[moduleName].loadMoreConfig.loadMoreAjaxUrl;

                if(totalPagesShowing >= totalRecords) {
                    showingCount = totalRecords;
                    self.$loadmore.hide();

                    self.triggerMethod('lastPage', resultModel);

                } else {
                    showingCount = totalPagesShowing;
                }
                self.$searchList.append(resultView);
                $currentRecordEl.html(showingCount);
                $totalRecordEl.html(totalRecords);
                app.trigger('image:lazyload',self);
            },

            /**
            @description: handle pagination
            @method _pagination
            @return list of next page search result
            @return searchArticles.getFirstPage()
            @return searchArticles.getNextPage()
            **/
            _pagination: function() {
                if (this.isFirstPage) {
                    this.isFirstPage = false;
                    return this.searchArticles;
                } else {
                    this.searchArticles.queryParams.searchParams = this.loadUrlParam;
                    this.searchArticles.url = this.loadUrl;
                    return this.searchArticles.getNextPage();
                }
            },

            /**
            @description: handle load more click event
            @method: _handleclick
            @return: {null}
            **/
            _handleLoadMore: function() {
                this.$loadmore.addClass(this.defaultSettings.activeclass);
                this._searchView();
            }
        });
    });
    return SearchResult;
});
