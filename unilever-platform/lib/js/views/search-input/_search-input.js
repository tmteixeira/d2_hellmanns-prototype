/*global define*/

define(function() {

    'use strict';

    var HeaderSiteSearchComponent = IEA.module('UI.search-input', function (module, app, iea) {

        _.extend(module, {

            defaultSettings: {
                searchBox: '.search-textbox',
                searchButton: '.btn-default',
                inputGroup: '.input-group',
                headerInputGroup: 'header .input-group',
                searchIcon: '.search-icon'
            },

            events: {
                'click .btn-default': '_handleclick',
                'blur .search-textbox': '_handleSearchBoxBlur',
                'keyup .search-textbox': '_handleButtonEnable',
                'click .search-icon': '_handleToggleSearchBox'
            },

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
            @method initialize

            @return {null}
            **/
            initialize: function(options) {
                var moduleName = this.moduleName.hyphenToCamelCase();

                this._super(options);
                this.searchConfig = this.getModelJSON()[moduleName];

                this.triggerMethod('init');
            },

            /**
            @method render

            @return BlogView
            **/
            render: function() {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled=true;
                }

                this.triggerMethod('render');

                return this;
            },

            /**
            enable function for the component

            @method enable

            @return {null}
            **/
            enable: function() {
                /*enable logic goes here*/
                this.$searchTextBox = $(this.defaultSettings.searchBox, this.$el);
                this.$searchButton = $(this.defaultSettings.searchButton, this.$el);
                this.$searchButton.attr('disabled','disabled');
                this.$searchTextBox.val('');

                this.triggerMethod('enable', this);
            },

            /**
            show the component

            @method show

            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            hide the component

            @method hide

            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean

            @return {null}
            **/
            clean: function() {
                this._super();
            },

            /**
            @method onMatchMobile

            @return {null}
            **/
            onMatchMobile: function() {
                var inputGroup = $(this.defaultSettings.inputGroup, this.$el);

                inputGroup.hide();
                $(this.defaultSettings.searchIcon, this.$el).show();
                inputGroup.css('marginTop', 31+'px');
            },


            /**
            @method onMatchTablet

            @return {null}
            **/
            onMatchTablet: function() {
                 var inputGroup = $(this.defaultSettings.inputGroup, this.$el);

                inputGroup.hide();
                $(this.defaultSettings.searchIcon, this.$el).show();
                inputGroup.css('marginTop', 31+'px');
            },

            /**
            @method onMatchDesktop

            @return {null}
            **/
            onMatchDesktop: function() {
                var inputGroup = $(this.defaultSettings.inputGroup, this.$el);

                inputGroup.show();
                $(this.defaultSettings.searchIcon, this.$el).hide();
                inputGroup.css('marginTop', -9+'px');
            },

            /**
            @method onWindowResized

            @return {null}
            **/
            onWindowResized: function() {
                if (app.getValue('isDesktopBreakpoint')) {
                    $(this.defaultSettings.inputGroup, this.$el).removeAttr("style");
                } else {
                    this._handleToggleSearchBox();
                }
            },

            /* ----------------------------------------------------------------------------- *\
               Private Methods
            \* ----------------------------------------------------------------------------- */

            /**
            handle button click

            @method _handleclick

            @return {null}
            **/
            _handleclick: function(evt) {
                var url = this._formURL();
                window.location.href = url;
            },

            /**
            form the result base url

            @method _formURL

            @return url
            **/
            _formURL: function() {
                var url = '',
                    queryString = '',
                    basePath = this.searchConfig.basePath.replace(/\//g, '|'),
                    SearchValue = this.$searchTextBox.val();
                queryString = 'q=' + SearchValue + '.basePath=' + basePath + '.limit=' + this.searchConfig.limit + '.noOfResults=' + this.searchConfig.noOfResults + '.doLazyLoad=' + this.searchConfig.doLazyLoad + '.isAjax=' + this.searchConfig.isAjax;
                url = this.searchConfig.resultPagePath + '.' + 'html?searchParams=' + queryString;

                return url;
            },

            /**
            handle search box blur

            @method _handleSearchBoxBlur

            @return {null}
            **/
            _handleSearchBoxBlur: function(evt) {
                var textbox = evt.currentTarget,
                    searchValue = $(textbox).val();

                if(searchValue === '') {
                    this.$searchButton.attr('disabled','disabled');
                }
            },


            /**
            toggle the search box

            @method _handleToggleSearchBox

            @return {null}
            **/
            _handleToggleSearchBox: function(evt) {
                if(evt) {
                    evt.preventDefault();
                }

                var inputGroup = $(this.defaultSettings.inputGroup, this.$el),
                    windowWidth = $(window).width(),
                    offset;

                inputGroup.toggle();

                offset = inputGroup.offset();

                if(this.$el.parents('header').length > 0) {
                    inputGroup.css({
                        'width': windowWidth+'px',
                        'marginTop': 4+'px',
                        'marginLeft': -1*offset.left
                    });
                }
            },

            /**
            enable or disable the search button

            @method _handleButtonEnable

            @return {null}
            **/
            _handleButtonEnable: function(evt) {
                var textbox = evt.currentTarget,
                    searchValue = $(textbox).val();

                if (evt.keyCode === 13) {
                    this._handleclick();
                }

                if(searchValue === '') {
                    this.$searchButton.attr('disabled','disabled');
                } else {
                    this.$searchButton.removeAttr('disabled');
                }
            }
        });
    });

    return HeaderSiteSearchComponent;
});
