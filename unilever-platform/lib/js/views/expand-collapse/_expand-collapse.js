/*global define*/

define(function() {

    'use strict';

    var ExpandCollapse = IEA.module('UI.expand-collapse', function (module, app, iea) {

        _.extend(module, {

            defaultSettings: {
                headerContainer: '.expandcollapse-header',
                speed: 400
            },

            events: {
                'click a.expandcollapse-toggle': '_handleExpandCollapse'
            },

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
            @method initialize

            @return {null}
            **/
            initialize: function(options) {
                this.triggerMethod('beforeInit', options);
                this._super(options);
                this.triggerMethod('init', options);
            },

            /**
            @method render

            @return BlogView
            **/
            render: function() {
                this.triggerMethod('beforeRender', this);
                this.$el.html(this.template(this.getModelJSON()));
                this.triggerMethod('render', this);
                
                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled=true;
                }

                return this;
            },

            /**
            enable function for the component

            @method enable

            @return {null}
            **/
            enable: function() {
                /*enable logic goes here*/
                this.triggerMethod('enable', this);
            },

            /**
            show the component

            @method show

            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            hide the component

            @method hide

            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean

            @return {null}
            **/
            clean: function() {
                this._super();
                // view cleanup code here, anything that needs to be temporarily paused when view is not currently displayed
            },

            expand: function(elem) {
                this.triggerMethod('BeforeExpand', elem);

                var elemsExpanded = $(this.defaultSettings.headerContainer).siblings('.expanded'),
                    self = this;

                if (elemsExpanded.length) {
                    elemsExpanded.each(function (i, element) {
                        var $element = $(element);
                        self.collapse($element);
                    });
                }

                elem.removeClass('collapsed').addClass('expanded').next().slideDown(this.defaultSettings.speed).toggleClass('hiding');
                this.triggerMethod('AfterExpand', elem);
            },

            collapse: function(elem) {
                this.triggerMethod('BeforeCollapse', elem);
                elem.addClass('collapsed').removeClass('expanded').next().slideUp(function() {elem.next().toggleClass('hiding')});
                this.triggerMethod('AfterCollapse', elem);
            },

            /* ----------------------------------------------------------------------------- *\
               Private Methods
            \* ----------------------------------------------------------------------------- */

            /**
            handle expand and collapse

            @method _handleExpandCollapse

            @return {null}
            **/

            _handleExpandCollapse: function(evt) {
                evt.preventDefault();
                var target = $(evt.currentTarget).parent();
                target.hasClass('expanded') ? this.collapse(target) : this.expand(target);
            }

        });
    });

    return ExpandCollapse;
});
