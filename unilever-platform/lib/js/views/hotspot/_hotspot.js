/*global define*/

define([
    'tooltip'
], function(
    toolTip) {

    'use strict';

    var Hotspot = IEA.module('UI.hotspot', function (module, app, iea) {

        _.extend(module, {

            defaultSettings: {
                baseImage: '.base-image',
                pointerContainer: '.pointer',
                pointer: '.hotspot-pointer',
                pointerContent: '.hotspot-content',
                pointerEvent: 'click',
                pointerPosition: 'bottom',
                onlyOne: true
            },
            events: {
            },

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
            @method initialize

            @return {null}
            **/
            initialize: function(options) {
                this._super(options);

                this.triggerMethod('init');
            },

            /**
            @method render

            @return HotspotView
            **/
            render: function() {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
            enable function for the component

            @method enable

            @return {null}
            **/
            enable: function() {
                /*enable logic goes here*/

                this.$el.find(this.defaultSettings.baseImage + ' picture img').on('load', $.proxy(this._plotPointers, this));
                this.$el.find(this.defaultSettings.baseImage + ' picture img').on('fail', this.triggerMethod('error', 'Error while loading base image'));
                this.on('window:resized', this._plotPointers);

                this.triggerMethod('enable', this);
            },

            /**
            show the component

            @method show

            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            hide the component

            @method hide

            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean

            @return {null}
            **/
            clean: function() {
                this._super();
                // view cleanup code here, anything that needs to be temporarily paused when view is not currently displayed
            },

            /* ----------------------------------------------------------------------------- *\
               Private Methods
            \* ----------------------------------------------------------------------------- */

            /**
            handle pointer plot

            @method _plotPointers

            @return {null}
            **/
            _plotPointers: function(evt){
                var config = this.defaultSettings,
                    $baseImage = $(config.baseImage),
                    $hotspotPointers = $(config.pointerContainer).not('.hidden'),
                    baseImageWidth = $baseImage.width(),
                    baseImageHeight = $baseImage.height(),
                    pointerHeight = $(config.pointer + ':first').height(),
                    pointerWidth = $(config.pointer + ':first').width(),
                    tooltipArray = [],
                    tooltipInstance = null;

                $.each($hotspotPointers, function(){
                    var $pointer = $(this).children(config.pointer),
                        $toolTipContent = $(this).children(config.pointerContent);
                    $pointer.css({
                        left: ($pointer.data('left')*baseImageWidth)/100 - pointerWidth/2,
                        top: ($pointer.data('top')*baseImageHeight)/100 - pointerHeight
                    });

                    tooltipInstance = IEA.tooltip($pointer, {content: $toolTipContent,
                                                                contentAsMarkup: true,
                                                                trigger: config.pointerEvent,
                                                                position: config.pointerPosition,
                                                                onlyOne: config.onlyOne });
                    tooltipArray.push(tooltipInstance);

                    tooltipInstance.on('tooltip:ready', function(origin, tooltip, object) {
                        var idx = 0,
                            length = tooltipArray.length;

                        for(idx = 0; idx<=length; idx++) {
                            // hide all toolt ip instances
                            if(tooltipArray[idx]) {
                                tooltipArray[idx].hide();
                            }
                        }
                        // show the current tooltip
                        object.show();
                    });
                });

            }
        });
    });

    return Hotspot;
});
