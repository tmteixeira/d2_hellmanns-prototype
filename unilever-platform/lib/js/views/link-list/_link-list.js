/*global define*/

define(function() {

    'use strict';

    var LinkList = IEA.module('UI.link-list', function(module, app, iea) {

        _.extend(module, {

            events: {

            },

            /**
             * Description
             * @method initialize
             * @param {} options
             * @return
             */
            initialize: function(options) {
                this._super(options);
                this.triggerMethod('init');
            },


            /**
             * Description
             * @method render
             * @return ThisExpression
             */
            render: function() {
                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },


            /**
             * Description
             * @method enable
             * @return
             */
            enable: function() {
                this.triggerMethod('enable');
            },

            /**
             * Description
             * @method show
             * @return
             */
            show: function() {
                this._super();
            },


            /**
             * Description
             * @method hide
             * @param {} cb
             * @param {} scope
             * @param {} params
             * @return
             */
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },


            /**
             * Description
             * @method clean
             * @return
             */
            clean: function() {
                this._super();
            },
        });
    });

    return LinkList;
});
