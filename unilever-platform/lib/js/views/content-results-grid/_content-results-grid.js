/*global define*/

define([
    'waypoints',
    'paginator'
], function(
    Waypoints,
    Paginator) {

    'use strict';

    var ContentResultGrid = IEA.module('UI.content-results-grid', function (module, app, iea) {

        _.extend(module, {
            defaultSettings: {
                articleContainer: '.article-container',
                sortingEl: '.selected-sort-key',
                pageIndexEl: '.page-index',
                pageSortKey: 'default',
                waypointOffset: 'bottom-in-view',
                mode: 'client',
                sortingField: 'titleDetail',
                articleInnerEl: 'div.thumbnail'
            },
            events: {
                'click .sorting-list': '_handleclick',
                'click .btn-group': '_handleDropdownOverflow'
            },

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
            @method initialize

            @return {null}
            **/
            initialize: function(options) {
                this._super(options);

                this.ArticleCollection = Backbone.PageableCollection.extend({
                    model: IEA.Model.extend({})
                });

                this.articleTemplate = this.getTemplate('articles', 'content-results-grid/partial/content-result-grid-article.hbss');

                //app.on('window:scrolled', $.proxy(_.debounce(this._startInfiniteLoading, 300), this));
                this.isFirstPage = true;
                this.isSeverSide = this.isServerComponent;

                this.triggerMethod('init');
            },

            /**
            @method render

            @return BlogView
            **/
            render: function() {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled=true;
                }

                this.triggerMethod('render');

                return this;
            },

            /**
            enable function for the component

            @method enable

            @return {null}
            **/
            enable: function() {
                /*enable logic goes here*/
                var settings = this.defaultSettings,
                    isDefaultMatched = false,
                    idx = 0,
                    moduleName = this.moduleName.hyphenToCamelCase(),
                    configuration = this.getModelJSON()[moduleName].configurations;

                this.$articleList = $(settings.articleContainer, this.$el);
                this.$sortKeySelected = $(settings.sortingEl, this.$el);
                this.$pageIndex = $(settings.pageIndexEl, this.$el);
                this.pageSortKey = settings.pageSortKey;

                if(!this.isServerComponent) {
                    var defaultSortOption = configuration.defaultSortOption,
                        sortOptions = configuration.sortOptions;

                    //selecting default sort option.
                    if(typeof sortOptions !== "undefined" && sortOptions && sortOptions.length > 0) {
                        for(idx=0; idx<sortOptions.length; idx++) {
                            if(sortOptions[idx].value === defaultSortOption) {
                                this.pageSortKey = sortOptions[idx].key;
                                this.$sortKeySelected.html(defaultSortOption);
                                isDefaultMatched = true;
                                break;
                            }
                        }
                    }

                    this.model.set({'dataLoaded':true});
                    this._createObjects();
                    this._articleView();

                } else {
                    var self = this;
                    setTimeout(function() {
                        self._matchHeight();
                    }, 700);
                }

                var listItems = this.$el.find('ul.dropdown-menu li'),
                    defaultVal = $(listItems[0]).find('a').html();

                for(idx=1; idx<listItems.length; idx++) {
                    if($(listItems[idx]).find('a').html() === defaultVal) {
                        this.$el.find('ul.dropdown-menu li:first-child').remove();
                        isDefaultMatched = true;
                        break;
                    }
                }

                this.$sortKeySelected.html(defaultVal);

                this._startInfiniteLoading();
                //this.$el.find('.thumbnail-arrow').hide();
                this.triggerMethod('enable', this);
            },

            /**
            show the component

            @method show

            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            hide the component

            @method hide

            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean

            @return {null}
            **/
            clean: function() {
                this._super();
            },

            /**
            @method onWindowResized

            @return {null}
            **/
            onWindowResized: function() {
                var self = this;
                $(this.defaultSettings.articleInnerEl, this.$el).removeAttr("style");
                setTimeout(function() {
                    self._matchHeight();
                }, 700);
            },

            /* ----------------------------------------------------------------------------- *\
               Private Methods
            \* ----------------------------------------------------------------------------- */

            /**
            handle dropdown list select / click

            @method _handleclick

            @return {null}
            **/
            _handleclick: function(evt) {
                var a = evt.currentTarget,
                    dataKey = $(a).attr('data-key'),
                    dataField = $(a).attr('data-field'),
                    dataValue = $(a).html();
                evt.preventDefault();
                this.$sortKeySelected.html(dataValue);
                this.pageSortKey = dataKey;
                this.pageSortField = dataField;
                this.isFirstPage = true;
                this.isSeverSide = false;
                this.$articleList.html('');
                this._articleView();
            },

            /**
            handle infinite scrolling

            @method _startInfiniteLoading

            @return {null}
            **/
            _startInfiniteLoading: function(evt) {
                var self = this,
                    waypoint = $(self.defaultSettings.articleContainer, self.$el).waypoint({
                        handler: function(direction) {
                            if(direction === 'down') {
                                self._articleView();
                            }
                        },
                        offset: 'bottom-in-view'
                    });
                    /*waypoint = new Waypoint({
                        element: $(self.defaultSettings.articleContainer, self.$el),
                        handler: function(direction) {
                            if(direction === 'down') {
                                self._articleView();
                            }
                        },
                        offset: self.defaultSettings.waypointOffset
                    });*/
            },

            _getModel: function(cb) {
                var self = this,
                    moduleName = this.moduleName.hyphenToCamelCase();

                $.getJSON(this.getModelJSON()[moduleName].configurations.jsonUrl, function( data ) {
                    self.model.set({"data": data.data});
                    self.model.set({'dataLoaded':true});
                    self._createObjects();

                    if(typeof cb === 'function') {
                        cb.apply(self,[self.model]);
                    }
                });
            },

            _createObjects : function() {
                var moduleName = this.moduleName.hyphenToCamelCase(),
                    contentResultData = this.getModelJSON()[moduleName],
                    contentResult = contentResultData.contentResults,
                    configurations = contentResultData.configurations;

                this.pageViewLimit = configurations.lazyload ? parseInt(configurations.pageSize) : parseInt(contentResultData.resultCount);
                this.articles = new this.ArticleCollection(contentResult, {mode: this.defaultSettings.mode, state: {pageSize: this.pageViewLimit}});
            },

            /**
            creates subviews

            @method _articleView

            @return {null}
            **/
            _articleView: function() {
                if(this.model.get('dataLoaded')) {
                    this._showView();
                } else {
                    this._getModel($.proxy(this._showView, this));
                }
            },

            _showView: function() {
                var moduleName = this.moduleName.hyphenToCamelCase(),
                    modelData = this.getModelJSON()[moduleName];

                if(modelData.resultCount > 0) {
                    if ((this.isFirstPage) || (this.articles.hasNextPage())) {
                        var resultModel = this._pagination(),
                            _self = this,
                            $resultView = this.articleTemplate({'data': resultModel.toJSON(), 'column': modelData.configurations.viewFormat}),
                            showingCount = ((this.articles.state.currentPage * this.articles.state.pageSize) > this.articles.state.totalRecords)
                                            ? this.articles.state.totalRecords
                                            : (this.articles.state.currentPage * this.articles.state.pageSize);

                        this.$articleList.append($resultView);
                        this.$pageIndex.html(modelData.showPrefixText + ' ' + showingCount + ' ' + modelData.showPostfixText + ' ' + this.articles.state.totalRecords);

                        setTimeout(function () {
                            _self.app.trigger('image:lazyload', _self);
                            Waypoint.refreshAll();
                        }, 300);

                        setTimeout(function() {
                            _self._matchHeight();
                            Waypoint.refreshAll();
                        }, 700);

                        if(this.isFirstPage) {
                            this.triggerMethod('firstPage', resultModel);
                        } else {
                            this.triggerMethod('nextPage', resultModel);
                        }

                    }
                } else {
                    var noResultCopy = modelData.configurations.noResultCopy;

                    if(noResultCopy) {
                       this.$articleList.html('<h4>' +  noResultCopy+ '</h4>');
                    }

                }
            },

            /**
            handle pagination

            @method _pagination

            return list of next page articles as per sort key

            @return articles.getNextPage()
            **/
            _pagination: function() {
                switch(this.pageSortKey) {
                case 'asc':
                    this.articles.setSorting(this.pageSortField, -1);
                    this.articles.fullCollection.sort();
                    break;

                case 'desc':
                    this.articles.setSorting(this.pageSortField, 1);
                    this.articles.fullCollection.sort();
                    break;

                case 'default':
                    this.articles.setSorting(null);
                    break;
                }

                if (this.isFirstPage) {
                    this.isFirstPage = false;
                    //if (this.isSeverSide) { this.articles.getFirstPage(); }
                    return this.isSeverSide ? this.articles.getPage(2) : this.articles.getFirstPage();
                } else {
                    return this.articles.getNextPage();
                }
            },

            _handleDropdownOverflow: function () {
                var self = this;

                setTimeout(function () {
                    var documentWidth = $(document).width(),
                        windowWidth = $(window).width(),
                        $dropdown = $('.dropdown-menu', self.$el),
                        offsetWidth = 0;

                    if(documentWidth > windowWidth) {
                        offsetWidth = (documentWidth - windowWidth) + 10;
                        $dropdown.css({'margin-left':'-'+offsetWidth+'px'});
                    }
                }, 100);
            },

            /**
            makes the gird to equal height if any un evnen heights

            @method _articleView

            @return {null}
            **/
            _matchHeight: function () {
                var $articles = $(this.defaultSettings.articleInnerEl, this.$el),
                    elementHeights = $articles.map(function ()
                        {
                            return $(this).height();
                        }).get(),
                maxHeight = Math.max.apply(null, elementHeights);
                $articles.height(maxHeight);
            }

        });
    });

    return ContentResultGrid;
});
