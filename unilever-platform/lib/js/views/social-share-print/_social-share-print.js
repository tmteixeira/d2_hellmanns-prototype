/*global define*/
define([
    'addthis'
], function(
    AddThis) {

    'use strict';

    var SocialSharePrint = IEA.module('UI.social-share-print', function (module, app, iea) {

        _.extend(module, {
            events: {
                'click .addthis_button_print': '_printTrigger'
            },

            /**
            @method initialize
            @return {null}
            **/
            initialize: function(options) {
                this._super(options);
                this.triggerMethod('init');
            },

            /**
            @method render
            @return SearchResult
            **/
            render: function() {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled=true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
            @description:enable function for the component , initialize addthis button instance with configurations. addthis_config available at http://support.addthis.com/customer/portal/articles/1337994-the-addthis_config-variable and addthis_share variables at http://support.addthis.com/customer/portal/articles/1337996-the-addthis_share-variable
            @method enable
            @return {null}
            **/
            enable: function() {
                // incase of tab it will not show dialog and required services_compact
                var moduleName = this.moduleName.hyphenToCamelCase(),
                    addThisOptions = String(this.getModelJSON()[moduleName].addThisOptions),
                    compact = (app.getValue('isDesktopBreakpoint')) ? 'none' : addThisOptions,
                    addthisConfig = {
                        services_expanded: (addThisOptions !== 'undefined') ? addThisOptions : 'default',
                        services_compact: compact,
                        ui_click: false,
                        ui_delay: 500
                    },
                    sharingObject={};
                //Creating wondow level object for addthis configuration
                window.addthis_config = addthisConfig;
                addthis.button('.addthis_button_compact', addthisConfig, sharingObject);
                this.triggerMethod('enable');
            },

            /**
            @description: Show the component
            @method show
            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            @description: Hide the component
            @method hide
            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean
            @return {null}
            **/
            clean: function() {
                this._super();
            },
            _printTrigger: function() {
                window.print();
            }
        });
    });
    return SocialSharePrint;
});
