/*global define*/

define(function() {

    'use strict';

    var Breadcrumb = IEA.module('UI.breadcrumb', function (module, app, iea) {

        _.extend(module, {

            events: {
                'click li>a': '_goToPageLink'
            },

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
            @method initialize

            @return {null}
            **/
            initialize: function(options) {
                this.triggerMethod('beforeInit',options);
                this._super(options);
                this.triggerMethod('init', options);
                this._isEnabled= false;
            },

            /**
            @method render

            @return BlogView
            **/
            render: function() {
                this.triggerMethod('beforeRender', this);
                this.$el.html(this.template(this.getModelJSON()));
                this.triggerMethod('render', this);

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled= true;
                }

                return this;
            },

            /**
            enable function for the component

            @method enable

            @return {null}
            **/
            enable: function() {
                /*enable logic goes here*/
                // Hook for handling load event
                this.triggerMethod('enable');
            },

            /**
            show the component

            @method show

            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            hide the component

            @method hide

            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean

            @return {null}
            **/
            clean: function() {
                this._super();
                // view cleanup code here, anything that needs to be temporarily paused when view is not currently displayed
            },

             /**
            navigates to another page on click

            @method _goToPageLink

            @return {null}
            **/
            _goToPageLink: function (evt) {
                this.triggerMethod('beforeLinkClick');
                window.location.href = $(evt.target).attr('href');
            }
        });
    });

    return Breadcrumb;
});
