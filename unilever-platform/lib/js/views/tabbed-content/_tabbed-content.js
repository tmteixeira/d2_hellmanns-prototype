/*global define*/

define(function() {

    'use strict';

    var TabbedContent = IEA.module('UI.tabbed-content', function (module, app, iea) {

        _.extend(module, {

            events: {
                'click .tabbed-content__link': '_handleClick'
            },

            render: function() {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled=true;
                }
                this.triggerMethod('render');
                return this;
            },

            initialize: function(options) {
                this._super(options);
                this.triggerMethod('init');

                var additionalOptions = {
                    selectors: {
                        tabbedContent: '.tabbed-content',
                        tabContainer: {
                            tab: {
                                item: '.tabbed-content__tab',
                                link: '.tabbed-content__link'
                            },
                            panel: {
                                item: '.tabbed-content__panel',
                                body: '.tabbed-content-panel__body'
                            }
                        }
                    },
                    cssClasses: {
                        tabs: 'tabbed-content-tabs',
                        tabContainer: {
                            tab: {
                                isActive: 'tabbed-content__tab--is-active'
                            },
                            panel: {
                                isActive: 'tabbed-content-panel__body--is-active'
                            }
                        }
                    }
                };

                this.options = _.extend(additionalOptions, options);

                return this;
            },

            _handleClick: function (evt) {
                evt.preventDefault();

                var self = this,
                    $this = $(evt.currentTarget),
                    compName = $this.parents('[data-componentname]').data('componentname'),
                    compVar = $this.parents('[data-component-variants]').data('component-variants'),
                    compPos = $this.parents('[data-component-positions]').data('component-positions'),
                    ev = {},
                    eventLabel;


                var $tab = $(evt.target).parents(self.options.selectors.tabContainer.tab.item);
                self.setActiveTab($tab);

                // Analytics script: Please do not change or remove
                if (typeof digitalData !== 'undefined' && typeof ctConstants !== 'undefined' && jTitle) {

                    eventLabel = $this.attr('title') + '-'
                                + (parseInt($this.parent('.tabbed-content__tab--is-active').index()) + 1) + '#'
                                + location.href;

                    // Analytics script: Please do not change or remove
                    digitalData.component = [];
                    digitalData.component.push({
                        'componentInfo' :{
                            'componentID': compName,
                            'name': compName
                        },
                        'attributes': {
                            'position': compPos,
                            'variants': compVar
                        }
                    });

                    ev.eventInfo = {
                        'type': ctConstants.trackEvent,
                        'eventAction': ctConstants.tabClick,
                        'eventLabel' : eventLabel
                    };

                    ev.attributes = {'nonInteractive':{'nonInteraction': 0}};

                    ev.category = {
                        'primaryCategory' : ctConstants.custom
                    };

                    digitalData.event.push(ev)
                }
            },

            setActiveTab: function ($tab) {

                // Tabs:
                // Find current tab and tabbed control container
                var $tabbedContentContainer = $tab.parents(this.options.selectors.tabbedContent);

                // De-activate currently active tab
                $tabbedContentContainer.find(this.options.selectors.tabContainer.tab.item)
                    .removeClass(this.options.cssClasses.tabContainer.tab.isActive);

                // Toggle class on selected item
                $tab.toggleClass(this.options.cssClasses.tabContainer.tab.isActive);

                // Panels:
                // Hide currently visible
                $tabbedContentContainer.find(this.options.selectors.tabContainer.panel.body)
                    .removeClass(this.options.cssClasses.tabContainer.panel.isActive);

                // Get related panel and show it
                var panelId = $tab.find(this.options.selectors.tabContainer.tab.link).attr('aria-controls');
                $('#' + panelId).toggleClass(this.options.cssClasses.tabContainer.panel.isActive);
            },

            enable: function () {

                // Get this control's tabs and how many there are
                var tabs = this.$el.find(this.options.selectors.tabContainer.tab.item),
                    tabLength = tabs.length;
                
                // Try and get default index
                var defaultIndex = this.$el.find('.' + this.options.cssClasses.tabs).data('default-index'),
                    isValidTabIndex = ((defaultIndex >= 0) && (defaultIndex < tabLength));

                // If the default index isn't available or is invalid - set the first tab as selected/active
                if (!isNaN(parseInt(defaultIndex, 10)) && !isValidTabIndex) {
                    this.setActiveTab(tabs.first());
                }

                this.triggerMethod('enable');
            }
        });

    });
    return TabbedContent;
});