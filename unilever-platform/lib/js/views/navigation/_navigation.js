/*global define*/

define(function() {

    'use strict';

    var Navigation = IEA.module('UI.navigation', function(module, app, iea) {

        _.extend(module, {

            defaultSettings: {
                dropdownEl: '.dropdown',
                multiColumnEl: '.multi-column div',
                mainMenuIcon: '> a span',
                enableHover: true,
                flyoutOpenIcon: 'glyphicon-chevron-up',
                flyoutCloseIcon: 'glyphicon-chevron-down',
                transitionTime: 200
            },
            events: {
                'mouseenter .navbar-nav .dropdown': '_handleNavToggle',
                'mouseleave .navbar-nav .dropdown': '_handleNavToggle',
                'touchend .navbar-nav .dropdown': '_handleTouch',
                'click .navbar-nav .dropdown > a' : '_handleNavClick',
                'click .dropdown' : '_handleTopNavClick'
            },

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
            @method initialize

            @return {null}
            **/
            initialize: function(options) {
                this.triggerMethod('beforeInit');
                IEA.View.prototype.initialize.apply(this, arguments);

                this.triggerMethod('init');
            },

            /**
            @method render

            @return Navigation
            **/
            render: function() {
                this.$el.html(this.template(this.getModelJSON()));
                this.$navElements = $(this.defaultSettings.dropdownEl, this.$el);

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');

                return this;
            },

            /**
            enable function for the component

            @method enable

            @return {null}
            **/
            enable: function() {
                // Hook for handling load event
                this.triggerMethod('enable');
            },

            /**
            @method onMatchDesktop

            @return {null}
            **/
            onMatchDesktop: function () {
                $('.navbar-collapse', this.$el).removeClass('in');
                this._disableDropdown();
            },

            /**
            @method onMatchTablet

            @return {null}
            **/
            onMatchTablet: function() {
                this._enableDropdown();
            },

            /**
            @method onMatchMobile

            @return {null}
            **/
            onMatchMobile: function() {
                this._enableDropdown();
            },

            /**
            show the component

            @method show

            @return {null}
            **/
            show: function() {
                IEA.View.prototype.show.apply(this, arguments);
            },

            /**
            hide the component

            @method hide

            @return {null}
            **/
            hide: function(cb, scope, params) {
                IEA.View.prototype.hide.apply(this, arguments);
            },

            /**
            @method clean

            @return {null}
            **/
            clean: function() {
                IEA.View.prototype.clean.apply(this, arguments);
                // view cleanup code here, anything that needs to be temporarily paused when view is not currently displayed
            },

            _equalHeight: function(groupEl) {
                var tallest = 0;
                groupEl.each(function() {
                    var thisHeight = $(this).height();
                    if (thisHeight > tallest) {
                        tallest = thisHeight;
                    }
                });
                groupEl.height(tallest);
            },

            /* ----------------------------------------------------------------------------- *\
               private Methods
            \* ----------------------------------------------------------------------------- */

            /**
            @method _enableDropdown

            @return {null}
            **/
            _enableDropdown: function() {
                if(this.$el.hasClass('top-navigation')) {
                    $('.navbar-toggle', this.$el).html('').addClass('icon-top-gear');
                    $('.container-fluid', this.$el).addClass('dropdown');
                    $('.navbar-header', this.$el).addClass('dropdown-toggle');
                    $('.collapse.navbar-collapse', this.$el).addClass('dropdown-menu');
                }
            },

            /**
            @method _disableDropdown

            @return {null}
            **/
            _disableDropdown: function() {
                if(this.$el.hasClass('top-navigation')) {
                    $('.navbar-toggle', this.$el).html('').removeClass('icon-top-gear');
                    $('.container-fluid', this.$el).removeClass('dropdown');
                    $('.navbar-header', this.$el).removeClass('dropdown-toggle');
                    $('.collapse.navbar-collapse', this.$el).removeClass('dropdown-menu');
                }
            },

            /**
            handle vcartousel click

            @method _handleclick

            @return {null}
            **/
            _handleNavToggle: function(evt) {
                var config = this.defaultSettings,
                    groupEl = $(config.multiColumnEl, $(evt.currentTarget)),
                    $mainMenuIcon = $(config.mainMenuIcon, $(evt.currentTarget)),
                    component = this;

                groupEl.attr('style', '');
                if(config.enableHover) {
                    if (evt.type === 'mouseenter') {
                        $(evt.currentTarget).addClass('open');
                        $mainMenuIcon.addClass(config.flyoutOpenIcon).removeClass(config.flyoutCloseIcon);
                        this._equalHeight(groupEl);

                        // Adjusting the overflowing dropdown
                        this._handleNavOverflow(evt.currentTarget);

                        // Hook for handling mouse enter event
                        component.triggerMethod('mouseenter');
                    } 
                    else {
                        $(evt.currentTarget).removeClass('open');
                        $mainMenuIcon.addClass(config.flyoutCloseIcon).removeClass(config.flyoutOpenIcon);

                        // Hook for handling mouse leave event
                        component.triggerMethod('mouseleave');
                    }
                }

            },
            _handleTouch: function(evt) {
                var config = this.defaultSettings,
                $mainMenuIcon = $(config.mainMenuIcon, $(evt.currentTarget));
                if($mainMenuIcon.hasClass(config.flyoutOpenIcon)) {
                    $mainMenuIcon.addClass(config.flyoutCloseIcon).removeClass(config.flyoutOpenIcon);
                }
                else {
                    $mainMenuIcon.addClass(config.flyoutOpenIcon).removeClass(config.flyoutCloseIcon);
                }
            },

            /**
            handle navigation click

            @method _handleNavClick

            @return {null}
            **/
            _handleNavClick: function(evt) {
                if(app.getValue('isDesktopBreakpoint')) {
                    window.location.href= $(evt.target).attr('href');
                }

                if(app.getValue('isTabletBreakpoint')) {
                    if ($(evt.currentTarget).hasClass("dropdown-toggle")) {
                        evt.preventDefault();
                    } else {
                        window.location.href= $(evt.target).attr('href');
                    }
                }

                this.triggerMethod('mouseclick');
            },

            /**
            @method _handleTopNavClick

            @return {null}
            **/
            _handleTopNavClick: function(evt) {
                // debugger;
                if(this.$el.hasClass('top-navigation')) {
                    evt.preventDefault();
                    var dropdown = $('.navbar-default .navbar-collapse.dropdown-menu', this.$el);
                    dropdown.hasClass('slide-open') ? this.closeDropdown(dropdown) : this.openDropdown(dropdown);
                }
            },

            /**
            opens the dropdown

            @method openDropdown

            @return {null}
            **/
            openDropdown: function(elem) {
                this.triggerMethod('onBeforeOpen');
                elem.slideToggle(this.defaultSettings.transitionTime);
                elem.toggleClass('slide-open');
                this.triggerMethod('onAfterOpen');
            },

            /**
            closes the dropdown

            @method closeDropdown

            @return {null}
            **/
            closeDropdown: function(elem) {
                this.triggerMethod('onBeforeClose');
                elem.slideToggle(this.defaultSettings.transitionTime);
                elem.toggleClass('slide-open');
                this.triggerMethod('onAfterClose');
            },

            /**
            handle navigation overflow

            @method _handleNavOverflow

            @return {null}
            **/
            _handleNavOverflow: function(dropdown) {
                var documentWidth = $(document).width(),
                    windowWidth = $(window).width(),
                    $dropdown = $(dropdown).find('.dropdown-menu'),
                    offsetWidth = 0;

                if(documentWidth > windowWidth) {
                    offsetWidth = (documentWidth - windowWidth) + 20;
                    $dropdown.css({'margin-left':'-'+offsetWidth+'px'});
                }
            }
        });
    });

    return Navigation;
});
