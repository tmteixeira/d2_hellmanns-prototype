/*global define*/

define([
    'video',
    'popup'
], function(
    Video,
    Popup) {

    'use strict';


    var Video = IEA.module('UI.video', function(module, app, iea) {

        _.extend(module, {
            instance: (this.instance) ? this.instance: {},
            events: {},

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
            @method initialize

            @return {null}
            **/
            initialize: function(options) {
                IEA.View.prototype.initialize.apply(this, arguments);
                this.triggerMethod('init');
            },

            /**
            @method render

            @return BlogView
            **/
            render: function() {
                //this.$el.html(this.template({video: this.getModelJSON().video}));

                this.$el.html(this.template(this.getModelJSON()));

                if (this._isEnabled === false) {
                    this.enable();
                    this._isEnabled = true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
            enable function for the component

            @method enable

            @return {null}
            **/
            enable: function() {
                this.triggerMethod('beforeEnable');
                /*enable logic goes here*/
                var moduleName = this.moduleName.hyphenToCamelCase(),
                    customConfig = this.getModelJSON()[moduleName],
                    container = this.$el.find('.video-frame'),
                    self = this;

                // var instance = new VideoFacade({
                //     config: customConfig,
                //     container: container
                // });

                this.key =  customConfig.randomNumber;

                if (customConfig.displayOption === 'Overlay') {

                    var $overlayLink = this.$el.find('a.video-overlay'),
                        $close = this.$el.find('button.mfp-close');

                    var popup = new IEA.popup({
                        type: 'inline',
                        preloader: false
                    });

                    this.popup = popup;

                    popup.on('popup:beforeOpen', function() {
                        self.instance[self.key] = IEA.video(container, customConfig);
                    });

                    popup.on('popup:open', function() {
                        if(app.getValue('isDesktopBreakpoint')) {
                            $('.mfp-content').addClass('video');
                         }

                        var videoPlayer = self.instance[self.key];
                        if(videoPlayer.options.viewType === 'html5') {
                            if(customConfig.autoplay) {
                                videoPlayer.play();
                            }
                        }
                    });

                    popup.on('popup:close', function() {
                        self.instance[self.key].destroy();
                    });

                    $overlayLink.on('click', function() {
                        popup.open($(container).parents('.video-content'));
                    });

                    $close.on('click', function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                        popup.close();
                    });

                } else {

                    this.instance[this.key] = IEA.video(container, customConfig);
                }

                this.triggerMethod('enable', this.instance[customConfig.randomNumber]);
            },

            play: function() {
                this.instance[this.key].play();
                this.triggerMethod('play', this.instance[this.key]);
            },

            pause: function() {
                this.instance[this.key].pause();
                this.triggerMethod('pause', this.instance[this.key]);
            },

            stop: function() {
                this.instance[this.key].stop();
                this.triggerMethod('stop', this.instance[this.key]);
            },

            destroy: function() {
                this.instance[this.key].destroy();
                this.triggerMethod('destroy', this.instance[this.key]);
            },

            restart: function() {
                this.instance[this.key].restart();
                this.triggerMethod('restart', this.instance[this.key]);
            },

            seek: function(seconds) {
                this.instance[this.key].seek(seconds);
                this.triggerMethod('seek', this.instance[this.key], seconds);
            },

            resize: function(width, height) {
                this.instance[this.key].resize(width, height);
                this.triggerMethod('resize', this.instance[this.key], width, height);
            },


            /**
            show the component

            @method show

            @return {null}
            **/
            show: function() {
                IEA.View.prototype.show.apply(this, arguments);
            },

            /**
            hide the component

            @method hide

            @return {null}
            **/
            hide: function(cb, scope, params) {
                IEA.View.prototype.hide.apply(this, arguments);
            },

            /**
            @method clean

            @return {null}
            **/
            clean: function() {
                IEA.View.prototype.clean.apply(this, arguments);
                // view cleanup code here, anything that needs to be temporarily paused when view is not currently displayed
            },

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
            handle vcartousel click

            @method _handleclick

            @return {null}
            **/
            _handleclick: function(evt) {}
        });
    });

    return Video;
});
