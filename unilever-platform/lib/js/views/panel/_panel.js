/*global define*/

define([
    'video',
    'popup'
], function(
    Video,
    Popup) {

    'use strict';


    var Panel = IEA.module('UI.panel', function (module, app, iea) {

        _.extend(module, {

            events: {
            },

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
            @method initialize

            @return {null}
            **/
            initialize: function(options) {
				this.triggerMethod('beforeInit');
                this._super(options);
				this.triggerMethod('init');
            },

            /**
            @method render

            @return BlogView
            **/
            render: function() {
                var data = {};

				this.triggerMethod('beforeRender');
                this.$el.html(this.template(this.getModelJSON()));
				this.triggerMethod('render', this);

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled=true;
                }

                return this;
            },

            /**
            enable function for the component

            @method enable

            @return {null}
            **/
            enable: function() {
                /*enable logic goes here*/
                this.triggerMethod('beforeEnable');
                // Call the video init using video facade
                var moduleName = this.moduleName.hyphenToCamelCase(),
                    modelData = this.getModelJSON()[moduleName];

                if(typeof modelData.video != 'undefined') {
                    var customConfig = modelData.video,
                        container = this.$el.find('.video-frame');

                    this.renderVideo(customConfig, container);
                }

				this.triggerMethod('enable');
            },

            renderVideo: function(customConfig, container) {
                var instance;

                if(customConfig.displayOption === 'Overlay'){
                    var $overlayLink = this.$el.find('a.video-overlay'),
                    $close = this.$el.find('button.mfp-close');

                    var popup = new IEA.popup({
                        type: 'inline',
                        preloader: false
                    });

                    popup.on('popup:beforeOpen', function () {
                        instance = IEA.video(container, customConfig);
                    });

                    popup.on('popup:open', function() {
                        if(app.getValue('isDesktopBreakpoint')) {
                            $('.mfp-content').addClass('video');
                         }

                        if(instance.options.viewType === 'html5') {
                            instance.play();
                        }
                    });

                    popup.on('popup:close', function () {
                        instance.destroy();
                    });

                    $overlayLink.on('click', function(){
                        popup.open($(container).parents('.video-content'));
                    });

                    $close.on('click', function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                        popup.close();
                    });

                } else {
                    instance = IEA.video(container, customConfig);
                }
            },

            /**
            show the component

            @method show

            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            hide the component

            @method hide

            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean

            @return {null}
            **/
            clean: function() {
                this._super();
                // view cleanup code here, anything that needs to be temporarily paused when view is not currently displayed
            }

        });
    });

    return Panel;
});
