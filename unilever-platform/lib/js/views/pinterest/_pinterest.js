define([
    'masonry',
    'bridget',
    'imagesloaded'
], function(
    Masonry,
    Bridget,
    Imagesloaded) {

    'use strict';

    var PinterestFeed = IEA.module('UI.pinterest', function (module, app, iea) {

        _.extend(module, {

            events: {},

            /* ----------------------------------------------------------------------------- *\
               Public Methods
            \* ----------------------------------------------------------------------------- */

            /**
            @method initialize

            @return {null}
            **/
            initialize: function(options) {
                this._super(options);
                $.bridget('masonry', Masonry);
                this.triggerMethod('init');
            },

            /**
            @method render

            @return BlogView
            **/
            render: function() {
                this.$el.html(this.template(this.getModelJSON()));

                if(this._isEnabled === false) {
                    this.enable();
                    this._isEnabled=true;
                }

                this.triggerMethod('render');
                return this;
            },

            /**
            enable function for the component

            @method enable

            @return {null}
            **/
            enable: function() {
                //this._renderPinterest();
                var moduleName = this.moduleName.hyphenToCamelCase(),
                    pinterestData = this.getModelJSON()[moduleName],
                    boardUrl = pinterestData.pinterestUserName + "/" + pinterestData.pinterestBoardURL,
                    feedCount = pinterestData.maxPinNum ? pinterestData.maxPinNum : 20,
                    $feedContainer = $('.pinitfeeds', this.el);

                if (((pinterestData.pinterestUserName !== "") && (pinterestData.pinterestUserName !== undefined)) && ((pinterestData.pinterestBoardURL !== "") && (pinterestData.pinterestBoardURL !== undefined))) {
                    this._createFeeds($feedContainer, boardUrl, feedCount);
                }

                this.triggerMethod('enable');
            },

            /**
            show the component

            @method show

            @return {null}
            **/
            show: function() {
                this._super();
            },

            /**
            hide the component

            @method hide

            @return {null}
            **/
            hide: function(cb, scope, params) {
                this._super(cb, scope, params);
            },

            /**
            @method clean

            @return {null}
            **/
            clean: function() {
                this._super();
            },

            /* ----------------------------------------------------------------------------- *\
               Private Methods
            \* ----------------------------------------------------------------------------- */

            /**
            render pinsterest feeds

            @method _createFeeds

            @return {null}
            **/

            _createFeeds: function(el, feedUrl, limit) {

                // Add ul tag to target element
                $(el).append('<ul class="stream"></ul>');

                // Set Pinterest RSS url using Google Feed API
                var href = 'http://www.pinterest.com/' + feedUrl + '.rss',
                    url = 'http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num='+limit+'&callback=?&q=' + encodeURIComponent(href),
                    $container = $('.stream', el),
                    options = {
                        compEl: this.$el,
                        errorMsg: app.config.getMessageSetting().connectivityErrorMsg,
                        successCallback: function(a) {

                            a = a.responseData.feed.entries;
                            $.each(a, function(i,item) {
                                if(i < limit) {
                                    var img = '<a href="'+item.link+'" target="_blank"><img src="'+$('img',item.content).attr('src')+'" alt="" /></a>',
                                        html = '<li class="item">' + img + '<p>' + item.contentSnippet + '</p></li>';

                                    // Add pinterest feed items to stream
                                    $container.append(html);
                                }
                            });

                        },
                        completeCallback: function() {
                            $container.imagesLoaded( function() {
                                $container.masonry({
                                    itemSelector: '.item',
                                    'gutter': 20
                                });
                            });
                        }
                    };

                IEA.getJSON(url, options);
            }

        });
    });

    return PinterestFeed;
});
