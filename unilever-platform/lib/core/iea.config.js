'use strict';

/**
 * application configuration class which will hold all the application level information and can be accessed anywhere from the application.
 * @method Config
 * @param {} options
 * @param {} app
 * @return
 */
IEA.Config= function (options, app) {
    Backbone.Model.apply(this, [options]);
    this.app = app;
};

IEA.Config.extend = Backbone.Model.extend;

_.extend(IEA.Config.prototype, Backbone.Model.prototype, {

    defaults: {
        '$htmlAndBody': $('html, body'),
        '$body': $('body'),
        '$window': $(window),
        '$document': $(document),
        'isIE8': $('body').hasClass('lt-ie9'),
        'pageTitle': document.title,
        'isMobileBreakpoint': false,
        'isTabletBreakpoint': false,
        'isDesktopBreakpoint': false,

        'FLCN_TEMPLATE': {},
        'FLCN_BASEPATH': 'lib',
        'FLCN_TEMPLATEPATH': 'lib/js/templates/',
        'FLCN_IMAGEPATH': 'lib/images/',
        'FLCN_VIEWPATH': 'lib/js/templates/',
        'FLCN_SASSPATH': 'lib/js/sass/',
        'FLCN_THEMEPATH': 'lib/js/sass/theme/',
        'FLCN_DEPENDENCIES': ['iea.components'],
        'JSONDataKey': 'data',

        'templateFramework':'Handlebars',
        'defaultTemplateName' : '',
        'dependencies': ['app.components'],
        'theme': 'whitelabel',
        'selector': 'enscale',
        'extension': 'jpg',
        'breakpoints': {},


        // application template and engine settings
        'template':{},

        // internationalization settings
        'i18n':{},

        // layout settings
        'layout':{},

        // debug settings
        'debug': true,

        // application level default messages (app.config.getMessageSetting())
        'messages':{
            'loadErrDefaultTitle' : "Component could not be loaded",
            'loadErrDefaultMsg': "Could not load the requested component",
        },

        // application folder and file nameing convensions
        'conventions':{
            'notFoundClass': 'not-found',
            'errorClass': 'error',
            'componentLoadErrDefTitle' : "Component could not be loaded",
            'componentLoadErrMsg': "Could not load the requested component",
        },

        // other application settings
        'settings':{
            'errorHandler': 'error',
            'missingTemplateHandler': 'missingTemplate'
        },

        // environment detection and settings
        'environment': {
            'development': [],
            'stage': [],
            'production': [],
        },

        // development settings
        'development': {},

        // stageserver settings
        'stage': {},

        // production settings
        'production': {}
    },

    /**
     * configuration object intialize and add the configuration at application level as well as server side json.
     * @method initialize
     * @return
     */
    initialize: function (options) {
        var self = this,
            breakpoints = options.breakpoints,
            deviceSmall = +breakpoints.deviceSmall.slice(0, -2),
            deviceMedium = +breakpoints.deviceMedium.slice(0, -2),
            deviceLarge = +breakpoints.deviceLarge.slice(0, -2),
            deviceXlarge = +breakpoints.deviceXlarge.slice(0, -2);

        if (options && typeof options.url !== 'undefined') {
            this.urlRoot = options.url;
        }

        this.set({
            template: {
                namespace: options.template.namespace || options.name,
                parentNamespace: options.template.parentNamespace || options.name,
                path: options.template.path || {}
            },

            breakpoints: {
                'mobile': {
                    'media': 'screen and (max-width: ' + (deviceLarge - 1) + 'px)',
                    'prefix': '.mobP.high.'
                },
                'mobileLandscape': {
                    'media': 'screen and (max-width: ' + (deviceLarge - 1) + 'px) and (orientation : landscape)',
                    'prefix':'.mobL.high.'
                },
                'tablet': {
                    'media': 'screen and (min-width: ' + deviceLarge + 'px) and (max-width: ' + (deviceXlarge - 1) + 'px)',
                    'prefix': '.tabP.high.'
                },
                'tabletLandscape': {
                    'media': 'screen and (min-width: ' + deviceLarge + 'px) and (max-width: ' + (deviceXlarge - 1) + 'px) and (orientation : landscape)',
                    'prefix': '.tabL.high.'
                },
                'desktop': {
                    'media': 'screen and (min-width: ' + deviceXlarge + 'px)',
                    'prefix': '.full.high.'
                }
            }
        });

        this.set(this.get(this.getEnvironment()));

        // add change event to configuration whic will trigger an event at application level
        this.on('change', function (config) {
            if(this.get('debug')) {
                console.info('%c '+ this.getEnvironment() +' configuration changed! ', 'background: #222; color: #fff',  config.changed);
            }
            self.app.triggerMethod('configuration:changed', config.changed);
        });
    },

    getTemplateSetting: function (prop) {
        var template = this.get('template');

        if(typeof prop !== 'undefined') {
            return template[prop];
        }

        return template;
    },

    geti18NSetting: function (prop) {
        var i18n = this.get('i18n');

        if(typeof prop !== 'undefined') {
            return i18n[prop];
        }

        return i18n;
    },

    getLayoutSetting: function (prop) {
        var layout = this.get('layout');

        if(typeof prop !== 'undefined') {
            return layout[prop];
        }

        return layout;
    },

    getSetting: function(prop) {
        var settings = this.get('settings');

        if(typeof prop !== 'undefined') {
            return settings[prop];
        }

        return settings;
    }, 

    getMessageSetting: function (prop) {
        var messages = this.get('messages');

        if(typeof prop !== 'undefined') {
            return messages[prop];
        }

        return messages;
    },

    getDebugSetting : function (prop) {
        var debug = this.get('debug');

        if(typeof prop !== 'undefined') {
            return debug[prop];
        }

        return debug;
    },

    getConventionSetting: function (prop) {
        var conventions = this.get('conventions');

        if(typeof prop !== 'undefined') {
            return conventions[prop];
        }

        return conventions;
    },

    getEnvironment: function (argument) {
        var env, envs = this.get('environment'), ptrn,debugPtrn = new RegExp('debug=iea');
        for (env in envs) {
            for (var i = 0; i < envs[env].length; i++) {
                ptrn = new RegExp(envs[env][i]);
                if(ptrn.test(location.hostname)) {
                    return env;
                }
            };
        }

        if(debugPtrn.test(window.location.href)) {
            return 'development';
        }

        return 'production';
    },

    getEnvironmentSetting: function (prop) {
        var environment = this.get('environment');

        if(typeof prop !== 'undefined') {
            return environment[prop];
        }

        return environment;
    },

    getDevelopmentSetting: function (prop) {
        var development = this.get('development');

        if(typeof prop !== 'undefined') {
            return development[prop];
        }

        return development;
    },

    getStageSetting: function (prop) {
        var stage = this.get('stage');

        if(typeof prop !== 'undefined') {
            return stage[prop];
        }

        return stage;
    },

    getProductionSetting: function (prop) {
        var production = this.get('production');

        if(typeof prop !== 'undefined') {
            return production[prop];
        }

        return production;
    }
});

