/*jshint nocap: true*/

'use strict';

// Application

/**
 * IEA Application class
 * @return
 * @method Application
 * @param {object} options
 * @return
 */
IEA.Application = function(options) {
    this._initCallbacks = new IEA.Callbacks();
    this.submodules = {};
    this.components = {};
    this.dependencies = [];
    this.initialize.apply(this, arguments);
};

// Copy the `extend` function used by Backbone's classes
IEA.Application.extend = IEA.extend;

_.extend(IEA.Application.prototype, Backbone.Events, {

    /**
     * Initialize is an empty function by default. Override it with your own
     * @return
     * @method initialize
     * @param {} options
     * @return
     */
    initialize: function(options) {
        var self = this,
            configURL = $('body').data('config');

        // add the current application instance to IEA scope for future use
        IEA.currentInstance = this;

        // create a new backbone configuration model and add it to application scope
        if (typeof configURL !== 'undefined' && configURL !== '') {
            options = _.extend(options, {
                url: configURL
            });
        }

        // create a config model with basic configuration
        this.config = new IEA.Config(options, this);

        try {
            // fetch the configuration json and update the option model.
            this.config.fetch({

                success: function(data) {
                    self.triggerMethod('config:loaded', self);
                },

                error: function() {
                    console.info('Configuration load error');
                }
            });
        } catch (err) {
            // put a very minor delay to make sure that the listeners are registered before its fired.
            setTimeout($.proxy(this.triggerMethod, this, 'config:loaded', this), 100);
        }

        this.on('config:loaded', function(app) {
            // run the consolify funtion to make sure that console is either diabled or gracefully killed.
            IEA.consolify(self.config.getDebugSetting());

            // add dependecies to be loaded before the application starts
            self.addDependencies(app.getValue('FLCN_DEPENDENCIES'));
            self.addDependencies(app.getValue('dependencies'));

            // add all the application start stuffs
            self.addInitializer($.proxy(self._registerModules, self, self));
            self.addInitializer($.proxy(self._listenToWindowEvents, self));
            self.addInitializer($.proxy(self._setUserAgent, self));
            self.addInitializer($.proxy(self._listenToViewportEvents, self));
            self.addInitializer($.proxy(self._loadComponentModules, self));
            self.addInitializer(function () {
                if(self.config.getEnvironment() !== 'production') {
                    self.config.get('$body').append('<p style="padding:1px;position:absolute;z-index:999;background-color:yellow;font-size:8px"> IEA Version : '+ IEA.getVersion() +' | '+ self.config.get('name') +' | '+self.config.getEnvironment()+'</p>');
                }
            });

            // $.when(self.loadDependencies(app.getValue('dependencies'))).done($.proxy(self._loadComponentModules, self));

            setTimeout($.proxy(self.triggerMethod, self, 'application:ready', self), 100);
        });

        this.on('image:lazyload', $.proxy(this._handleLazyLoad, this));
    },

    /**
     * Add an initializer that is either run at when the `start` method is called, or run immediately if added after `start` has already been called.
     * @return
     * @method addInitializer
     * @param {function} initializer
     * @return
     */
    addInitializer: function(initializer) {
        this._initCallbacks.add(initializer);
    },

    /**
     * kick off all of the application's processes. initializes all of the initializer functions
     * @return
     * @method start
     * @param {} options
     * @return
     */
    start: function(options) {
        this.triggerMethod('before:start', options);
        this._initCallbacks.run(options, this);
        this.triggerMethod('start', options);
    },

    /**
     * Create a module, attached to the application
     * @method module
     * @param {string} moduleNames
     * @param {function} moduleDefinition
     * @return CallExpression
     */
    addModule: function(moduleNames, moduleDefinition) {

        // Overwrite the module class if the user specifies one
        var ModuleClass = IEA.Module.getClass(moduleDefinition);

        // slice the args, and add this application object as the
        // first argument of the array
        var args = Array.prototype.slice.call(arguments);
        args.unshift(this);

        // see the IEA.Module object for more information
        return ModuleClass.create.apply(ModuleClass, args);
    },

    /**
     * Retrieve an object, function or other value from a target object or its `options`, with `options` taking precedence.
     * @method getValue
     * @param {} optionName
     * @return value
     */
    getValue: function(optionName) {
        if (!optionName) {
            return;
        }
        var value;

        if (typeof this.config !== undefined) {
            value = this.config.get(optionName);
        }

        return value;
    },

    /**
     * set value to the application level options
     * @method setValue
     * @param {string} optionName
     * @param {any} value
     * @return Literal
     */
    setValue: function(optionName, value) {
        if (!optionName) {
            return false;
        }

        if (typeof this.config !== undefined) {
            this.config.set(optionName, value);
        }

        return true;
    },

    /**
     * Description
     * @method getTemplateFramework
     * @return CallExpression
     */
    getTemplateFramework: function() {
        return this.getValue('templateFramework');
    },

    /**
     * Description
     * @method getTemplateExtension
     * @return xtn
     */
    getTemplateExtension: function() {
        var xtn = '';

        switch (this.getValue('templateFramework')) {
            case 'Handlebars':
                xtn = 'hbss';
                break;
        }

        return xtn;
    },

    /**
     * Description
     * @method getTemplate
     * @param {} templateName
     * @return MemberExpression
     */
    getTemplate: function(templateName, templatePath) {

        // if there is a default template settingused, the rather than folder name it used defaultTemplatenname
        // templatefilename can also be an array of possible names
        var templateFileName = (typeof this.getValue('defaultTemplateName') !== 'undefined' && this.getValue('defaultTemplateName') !== '') ? this.getValue('defaultTemplateName') : templateName,
            templateSettings = this.config.getTemplateSetting(),
            templateKey, appTemplate, ieaTemplate, confTemplatePath, confTemplateNS, componentName;

        // if templateName is empty means the use has send a custom templatePath to explicity load
        // moslty used in case of sub view load.
        if (!templateName) {
            return false;
        }

        // if application specific template exists then use that rather than looking into iea templates.
        if (this.getValue('appBasePath')) {
            //if user has send templatPath specifically , then use that rather than deriving the url based on templateName
            // for include and partials
            if (typeof templatePath !== 'undefined' && _.isString(templatePath) && templatePath.indexOf(this.getValue('templatePath')) < 0) {
                componentName = templatePath.split('/')[0],
                //confTemplatePath = this.getValue('tenantTemplatePath')[componentName];
                confTemplatePath = templateSettings.path[componentName];

                if(_.isString(confTemplatePath)){
                    confTemplateNS = (confTemplatePath.split('/')[0] === 'lib') ? confTemplatePath.split('/')[0] : confTemplatePath.split('/')[1];
                } else {
                    var partialFile = templatePath.split(/[\/ ]+/).pop().split('.')[0];
                    confTemplatePath = confTemplatePath[partialFile];
                    confTemplateNS = (confTemplatePath.split('/')[0] === 'lib') ? confTemplatePath.split('/')[0] : confTemplatePath.split('/')[1];
                }
                //confTemplateNS = (confTemplateNS === 'common') ? this.getValue('parentAppName') : confTemplateNS;
                // if the templatPath is relative then prefix the path with templatePath
                //appTemplate = this.getValue('templatePath') + templatePath;
                appTemplate = confTemplatePath + templatePath;

                // if the path does not have the extendion then append the extension
                if (appTemplate.indexOf(this.getTemplateExtension()) < 0) {
                    appTemplate = appTemplate + '.' + this.getTemplateExtension();
                }

            } else {

                //For scenario like Video which is container component using partials inside own folder
                //confTemplatePath = this.getValue('tenantTemplatePath')[templateName];
                confTemplatePath = templateSettings.path[templateName];

                if(confTemplatePath) {
                    if(_.isString(confTemplatePath)){
                        confTemplateNS = (confTemplatePath.split('/')[0] === 'lib') ? confTemplatePath.split('/')[0] : confTemplatePath.split('/')[1];
                    } else {
                        confTemplatePath = confTemplatePath[templateName];
                        confTemplateNS = (confTemplatePath.split('/')[0] === 'lib') ? confTemplatePath.split('/')[0] :confTemplatePath.split('/')[1];
                    }

                    //confTemplateNS = (confTemplateNS === 'common') ? this.getValue('parentAppName') : confTemplateNS;
                }
            }

            // if user has send custom path, then use it else derive the path from templateName and templateFileNames
            if (appTemplate) {
                templateKey = appTemplate;
            } else {
                // if the defaultTemplates is an array then we have to loop it and see which one exist
                if (_.isArray(templateFileName)) {
                    for (var i = 0; i < templateFileName.length; i++) {
                        //templateKey = this.getValue('templatePath') + templateName + '/' + templateFileName[i] + '.' + this.getTemplateExtension();
                        templateKey = confTemplatePath + templateName + '/' + templateFileName[i] + '.' + this.getTemplateExtension();
                        // if the template exist in the application namespace, then use it. else if will
                        // exit the condition and go to iea template fetching logic
                        if (typeof window[confTemplateNS] !== 'undefined' && _.has(window[confTemplateNS], templateKey)) {
                            console.info('%c Component template exists : ', 'background: #00A61E; color: #fff', templateKey);
                            return window[confTemplateNS][templateKey];
                        } else {
                            console.info('%c Component template does not exists : ', 'background: #00A61E; color: #fff', templateKey);
                        }
                    }
                } else {
                    templateKey = confTemplatePath + templateName + '/' + templateFileName + '.' + this.getTemplateExtension();
                    //templateKey = this.getValue('templatePath') + templateName + '/' + templateFileName + '.' + this.getTemplateExtension();
                }
            }

            // if the template exist in the application namespace, then use it. else if will
            // exit the condition and go to iea template fetching logic
            if (typeof window[confTemplateNS] !== 'undefined' && _.has(window[confTemplateNS], templateKey)) {
                console.info('%c Component template exists : ', 'background: #00A61E; color: #fff', templateKey);
                return window[confTemplateNS][templateKey];
            } else {
                throw new Error('Template does not exists for ' +templateName);
            }
        }


        /*if (typeof templatePath !== 'undefined' && _.isString(templatePath) && templatePath.indexOf(this.getValue('FLCN_BASEPATH')) < 0) {
            ieaTemplate = this.getValue('FLCN_TEMPLATEPATH') + templatePath;

            if (ieaTemplate.indexOf(this.getTemplateExtension()) < 0) {
                ieaTemplate = ieaTemplate + '.' + this.getTemplateExtension();
            }
        }

        if (ieaTemplate) {
            templateKey = ieaTemplate;
        } else {
            if (_.isArray(templateFileName)) {
                for (var i = 0; i < templateFileName.length; i++) {
                    templateKey = this.getValue('FLCN_TEMPLATEPATH') + templateName + '/' + templateFileName[i] + '.' + this.getTemplateExtension();
                    console.info('%c IEA component template exists : ', 'background: #00A61E; color: #fff', templateKey);
                    return window[this.getValue('FLCN_BASEPATH')][templateKey];
                };
            } else {
                templateKey = this.getValue('FLCN_TEMPLATEPATH') + templateName + '/' + templateFileName + '.' + this.getTemplateExtension();
            }
        }

        console.info('%c IEA component template exists : ', 'background: #00A61E; color: #fff', templateKey);
        return window[this.getValue('FLCN_BASEPATH')][templateKey];*/


        // if client template is available pick it else pick application template
        /*if (this.getValue('appBasePath')) {
            templateAvailable = (typeof window[this.getValue('appBasePath')][this.getValue('templatePath') + this.getValue('FLCN_TEMPLATE')[templateName]] !== 'undefined') ? window[this.getValue('appBasePath')][this.getValue('templatePath') + this.getValue('FLCN_TEMPLATE')[templateName]] : window.app[this.getValue('FLCN_TEMPLATEPATH') + this.getValue('FLCN_TEMPLATE')[templateName]];
        } else {
            templateAvailable = window.app[this.getValue('FLCN_TEMPLATEPATH') + this.getValue('FLCN_TEMPLATE')[templateName]];
        }

        return templateAvailable;*/
    },

    /**
     * Description
     * @method registerTemplate
     * @param {} templateName
     * @param {} templateName
     * @return MemberExpression
     */
    registerTemplate: function(templateName, partialPath) {

        var templateFileName = (typeof this.getValue('defaultTemplateName') !== 'undefined' && this.getValue('defaultTemplateName') !== '') ? this.getValue('defaultTemplateName') : templateName,
            path = '';

        if (!templateName) {
            return false;
        }

        if (partialPath) {
            //path = this.getValue('templatePath') + partialPath + '.' + this.getTemplateExtension();
            path = partialPath + '.' + this.getTemplateExtension();
        } else {
            //path = this.getValue('templatePath') + templateName + '/' + templateFileName + '.' + this.getTemplateExtension();
            path = templateName + '/' + templateFileName + '.' + this.getTemplateExtension();
        }
        if (typeof this.config.attributes.FLCN_TEMPLATE !== undefined) {
            this.getValue('FLCN_TEMPLATE')[templateName] = path;
        }
        //this.setValue(templateName, path);

        return true;
    },

    /**
     * returns currently set application breakpoint
     * @method getCurrentBreakpoint
     * @return currentBreakpoint
     */
    getCurrentBreakpoint: function() {
        var currentBreakpoint = 'matchDesktop';

        if (this.getValue('isMobileBreakpoint')) {
            currentBreakpoint = 'matchMobile';
        } else if (this.getValue('isTabletBreakpoint')) {
            currentBreakpoint = 'matchTablet';
        } else if (this.getValue('isDesktopBreakpoint')) {
            currentBreakpoint = 'matchDesktop'
        }

        return currentBreakpoint;
    },
    /**
     * Description
     * @method addDependencies
     * @param {} dep
     * @return MemberExpression
     */
    addDependencies: function(dep) {
        if (_.isArray(dep)) {
            this.dependencies = this.dependencies.concat(dep)
        } else {
            this.dependencies.push(dep);
        }
        return this.dependencies;
    },
    /**
     * Description
     * @method loadDependencies
     * @param {} callback
     * @return
     */
    loadDependencies: function(deps, callback) {
        if (_.isFunction(deps)) {
            callback = deps;
            deps = this.dependencies;
        }

        function load(queue, results) {
            if (queue.length) {
                require([queue.shift()], function(result) {
                    results.push(result);
                    load(queue, results);
                });
            } else {
                callback.apply(null, results);
            }
        }

        load(deps, []);
    },

    /**
     * Description
     * @method renderComponent
     * @param jQueryObject el
     * @return
     */
    renderComponent: function(el, id, isLastComponent) {

        var self = this,
            $el = $(el),
            moduleName = $el.data('role'),
            parentModuleName= 'notfound',
            extend = $el.data('extend') || {},
            isServerComponent = $el.data('server')|| false,
            compConfig = $('#' + $el.data('config')),
            compTemplatePath = $el.data('template'),
            compName= $el.data('name'),
            compDataURL = $el.data('path'),
            moduleView = '',
            template = '',
            errorClass = '',
            componentHasError = false,
            conventionSetting = this.config.getConventionSetting(),
            ErrDefaultTitle = this.config.getMessageSetting('loadErrDefaultTitle'),
            ErrDefaultMsg = this.config.getMessageSetting('loadErrDefaultMsg'),
            invalidJsonMsg = this.config.getMessageSetting('invalidJSONDefaultMsg');
        
        /*
            naming convention for component Id
            1. data.randomNumber
            2. data.name
            3. modulename + seqid
            4. modulename + randomNo

            naming convention
            carousel-1
            media-gallery-2
        */
        if(typeof compName === 'undefined') {
            compName = id ? id : (moduleName + '-' + Math.floor((Math.random() * 1000) + 1));
        }

        if (typeof this.UI !== 'undefined' && typeof this.UI[moduleName] !== 'undefined') {

            //TODO : need to write this code much more elegantly
            var i,extendableModule = [this.UI[moduleName]],mod = this.UI[moduleName],modtemplate='notfound';

            while(_.has(mod,'extend')) {
                extendableModule.push(mod.extend);
                mod = mod.extend;
            }

            extendableModule.push(IEA.View);

            for (i = extendableModule.length; i > 0; i--) {
                if(extendableModule[i-2]) {

                    //Get the template only incase of client side rendering; fix for no precompile template
                    if (typeof compDataURL !== 'undefined' && typeof compDataURL === 'string') {
                        // get the parent template ready in case the child does not have a template
                        modtemplate = this.getTemplate(extendableModule[i-2].moduleName);
                    } else {
                        modtemplate = 'notfound';
                    }
                    if(typeof modtemplate !== 'undefined' & modtemplate !== '') {
                        template = modtemplate
                    }
                    // b.extend(a), the parent is extended into the child using backbone extend principle
                    extendableModule[i-2] = extendableModule[i-1].extend(extendableModule[i-2]);
                }
            };

            //this.UI[moduleName].extends.extends = IEA.View.extend(this.UI[moduleName].extends.extends); // d.extend(c)
            //this.UI[moduleName].extends = this.UI[moduleName].extends.extends.extend(this.UI[moduleName].extends); // c.extend(b)
            //moduleView = this.UI[moduleName].extends.extend(this.UI[moduleName]); //b.extend(a)
            // if the base template is not existing in app and iea, then fallback to its super component and pick its template
            if(typeof template === 'undefined' || template === '') {
                componentHasError = true;
            }

            moduleView = extendableModule[0];

            // this is a default view binding with server rendered component.
            var viewOptions = {
                el : $el,
                className: moduleName,
                parent: self.UI[moduleName],
                app: self,
                isServerComponent: isServerComponent,
                template: template
            };

            if (typeof compConfig !== 'undefined' && typeof compDataURL === 'undefined' && compConfig.length) {
                console.info('%c SSR component enabled : ', 'background: #428bca; color: #fff', moduleName);

                try {
                    compConfig = JSON.parse(compConfig.html());
                } catch (err) {
                    componentHasError = true;
                    compConfig = {
                        status: err.message || ErrDefaultTitle,
                        details: err.stack || invalidJsonMsg
                    };
                }

                if(_.isEmpty(compConfig) || componentHasError) {
                    errorClass = conventionSetting.errorClass;
                }


                self._showComponent.apply(self, [moduleView,compName, compConfig, viewOptions, 'enable', isServerComponent, isLastComponent, errorClass]);

            } else if (typeof compDataURL !== 'undefined' && typeof compDataURL === 'string') {
                console.info('%c CSR component rendered : ', 'background: #428bca; color: #fff', moduleName, compDataURL);

                var compxhr,errorMessage,
                    componentData = new IEA.Model({
                        url: compDataURL
                    });

                compxhr = componentData.fetch();

                compxhr.done(function(response) {

                    if (_.isEmpty(response) || (typeof response.responseHeader !== 'undefined' && typeof response.responseHeader.status !== 'undefined' && response.responseHeader.status !== "200")) {

                        try{
                            errorMessage = response.responseHeader.messages[0];
                        }
                        catch(err){
                            errorMessage = invalidJsonMsg;
                        }

                        response = {
                            status: ErrDefaultTitle,
                            details: errorMessage
                        };

                        viewOptions.template = self.getTemplate(parentModuleName);
                        viewOptions._isEnabled = true;
                        componentHasError = true;

                        errorClass = conventionSetting.errorClass;

                    } else if(componentHasError) {
                
                        viewOptions.template = self.getTemplate(parentModuleName);
                        
                        errorClass = conventionSetting.notFoundClass;

                        response = {
                            status: ErrDefaultTitle,
                            details: ErrDefaultMsg
                        };
                    }

                    self._showComponent.apply(self, [moduleView, compName, response, viewOptions, 'render', isServerComponent, isLastComponent, errorClass]);
                });

                compxhr.fail(function(object, status, details) {
                    errorClass = conventionSetting.errorClass;
                    viewOptions.template = self.getTemplate(parentModuleName);
                    viewOptions._isEnabled = true;
                    componentHasError = true;

                    self._showComponent.apply(self, [moduleView, compName, {
                        status: status || ErrDefaultTitle,
                        details: details.stack || ErrDefaultMsg
                    }, viewOptions, 'render', isServerComponent, isLastComponent, errorClass]);
                });

            } else {
                console.info('%c SSR component enabled : ', 'background: #428bca; color: #fff', moduleName);

                self._showComponent.apply(self, [moduleView, compName, {}, viewOptions, 'enable', isServerComponent, isLastComponent, errorClass]);
            }

        }
    },

    /**
     * render all component in the page
     * @return
     */
    renderAllComponent: function() {
        var self = this,
            comps = $('[data-role]', $(document)),
            isLastComponent,
            id;

        comps.each(function (index, component) {
            isLastComponent = index === comps.length - 1;
            id = $(component).data('role') + '-' + (index + 1);
            self.renderComponent.apply(self,[component, id, isLastComponent]);
        });

        return this;
    },

    // import the `triggerMethod` to trigger events with corresponding
    // methods if the method exists
    triggerMethod: IEA.triggerMethod,
    triggerMethodOn: IEA.triggerMethodOn,

    /* -----------------------------------------------------------------------------
     Private Methods
     ----------------------------------------------------------------------------- */


    /**
     * load all component modules when the application is loaded.
     * @return
     * @method _loadComponentViews
     * @return
     */
    _loadComponentModules: function() {
        var self = this;

        this.loadDependencies(function () {
            // assumed that all dependencies are loaded when render all function is called
            self.triggerMethod('dependencies:loaded', self);

            self.renderAllComponent();
        });
    },

    /**
     * Description
     * @method _showComponent
     * @param {} module
     * @param {} el
     * @param {} data
     * @param {} viewOptions
     * @param {} method
     * @return
     */
    _showComponent: function(moduleView, compName, response, viewOptions, method, isServerComponent, isLastComponent, errorClass) {
        
        var notFoundClass = this.config.getConventionSetting('notFoundClass'),
            errMissingTemplate = this.config.getSetting('missingTemplateHandler'),
            errJsonParse = this.config.getSetting('errorHandler');

        //if randomNumber exists then modify the compName based on randomNumber
        if(response.data && response.data.randomNumber) {
            compName = viewOptions.className + '-' +response.data.randomNumber;
        }

        // create new instance of the module view and add the data as model
        viewOptions.model = new IEA.Model(response);
        this.components[compName] = new moduleView(viewOptions);

        // this.components[compName].template = this.getTemplate(this.components[compName].moduleName);
        // this.components[compName].className = this.components[compName].moduleName;

        this.triggerMethodOn(this.components[compName], 'before:show');
        this.components[compName][method]();

        if(errorClass) {

            if(errorClass === notFoundClass) {
                this.triggerMethod(errMissingTemplate, {
                    type: errMissingTemplate,
                    response: response,
                    compId: compName,
                    options: viewOptions
                });

            }
            else {
                this.triggerMethod(errJsonParse, {
                    type: errJsonParse,
                    response: response,
                    compId: compName,
                    options: viewOptions
                });

            }

            this.components[compName].$el.addClass(errorClass);
        }


        this.triggerMethodOn(this.components[compName], 'show');
        this.triggerMethod('component:show', this.components[compName]);

        if(isLastComponent) {
            this.triggerMethod('components:loaded', this.components[compName]);
        }

        this.triggerMethod('image:lazyload', this.components[compName]);

        // add instance object to DOM for future reference
        this.components[compName].$el
            .data('view', this.components[compName])
            .prop('id',compName);
    },

    /**
     * Initialize enquire setup.
     * @return
     * @method _listenToViewportEvents
     * @return
     */
    _listenToViewportEvents: function() {
        var self = this;

        if (this.getValue('isIE8')) {
            this.setValue('isMobileBreakpoint', false);
            this.setValue('isTabletBreakpoint', false);
            this.setValue('isDesktopBreakpoint', true);
            self.triggerMethod('matchDesktop');
        } else {
            enquire.register(this.getValue('breakpoints').mobile.media, {
                /**
                 * Description
                 * @return
                 * @method setup
                 * @return
                 */
                setup: function() {
                    // body...
                },
                /**
                 * Description
                 * @return
                 * @method match
                 * @return
                 */
                match: function() {
                    self.setValue('isMobileBreakpoint', true);
                    self.setValue('isTabletBreakpoint', false);
                    self.setValue('isDesktopBreakpoint', false);
                    self.triggerMethod('matchMobile');
                },
                /**
                 * Description
                 * @return
                 * @method unmatch
                 * @return
                 */
                unmatch: function() {
                    self.setValue('isMobileBreakpoint', false);
                    self.triggerMethod('unMatchMobile');
                }

            }).register(this.getValue('breakpoints').tablet.media, {
                /**
                 * Description
                 * @return
                 * @method setup
                 * @return
                 */
                setup: function() {
                    // body...
                },
                /**
                 * Description
                 * @return
                 * @method match
                 * @return
                 */
                match: function() {
                    self.setValue('isMobileBreakpoint', false);
                    self.setValue('isTabletBreakpoint', true);
                    self.setValue('isDesktopBreakpoint', false);
                    self.triggerMethod('matchTablet');
                },
                /**
                 * Description
                 * @return
                 * @method unmatch
                 * @return
                 */
                unmatch: function() {
                    self.setValue('isTabletBreakpoint', false);
                    self.triggerMethod('unmatchTablet');
                }
            }).register(this.getValue('breakpoints').desktop.media, {
                /**
                 * Description
                 * @return
                 * @method setup
                 * @return
                 */
                setup: function() {
                    // body...
                },
                /**
                 * Description
                 * @return
                 * @method match
                 * @return
                 */
                match: function() {
                    self.setValue('isMobileBreakpoint', false);
                    self.setValue('isTabletBreakpoint', false);
                    self.setValue('isDesktopBreakpoint', true);
                    self.triggerMethod('matchDesktop');
                },
                /**
                 * Description
                 * @return
                 * @method unmatch
                 * @return
                 */
                unmatch: function() {
                    self.setValue('isDesktopBreakpoint', false);
                    self.triggerMethod('unmatchDesktop');
                }
            }, true);
        }
    },


    //_.debounce: http://rifatnabi.com/post/detect-end-of-jquery-resize-event-using-underscore-debounce
    //resize: http://stackoverflow.com/a/1921259

    /**
     * initialize applicatin level window event listeners and event emitters
     * @method _listenToWindowEvents
     * @return
     */
    _listenToWindowEvents: function() {
        var self = this,
            $window = $(window),
            width = $window.width(),
            height = $window.height(),
            newWidth = 0,
            newHeight = 0;

        $window.on('resize orientationchange', _.debounce(function(evt) {
            newWidth = $window.width();
            newHeight = $window.height();

            if (newWidth !== width || newHeight !== height) {
                width = newWidth;
                height = newHeight;

                //TODO: this needs to be standardized
                self._handleLazyLoad(self);
                self.triggerMethod('window:resized', evt);
            }
        }, 500));

        $window.on('scroll', _.debounce(function(evt) {
            self.triggerMethod('window:scrolled', evt);
            self.triggerMethod('image:lazyload', self);
        }, 100));

        self.triggerMethod('window:resized');
    },

    /**
     * Description
     * @method _handleLazyLoad
     * @return
     */
    _handleLazyLoad: function(view) {
        Picturefill();
        picturefill({ reevaluate: true });

        if (view instanceof IEA.View) {
            view.loadedImageCount = 0;
            
            if($.fn.imagesLoaded) {
                view.$el.imagesLoaded()
                .progress(function(imgLoad, image) {
                    var $image = $(image.img),
                        $item = $image.parent();

                    $item.removeClass('is-loading').addClass('loaded');

                    IEA.triggerMethodOn($image, 'load');

                    if (!image.isLoaded) {
                        $item.addClass('is-broken');
                        IEA.triggerMethodOn($image, 'fail');
                    } else {
                        $item.removeClass('is-loading').addClass('loaded');
                    }

                    view.loadedImageCount++;
                })
                .always(function(instance) {
                    view.triggerMethod('image:loaded');
                    $('picture.loaded', view.$el).removeClass('is-broken');
                });
            }
        }
    },

    /**
     * application level UA sniffer
     * @method _setUserAgent
     * @return
     */
    _setUserAgent: function() {
        var ieInfo = IEA.checkNonConditionalIEVersion(),
            isiPad = navigator.userAgent.match(/iPad/i) !== null,
            $body = $('body');

        this.setValue('isIE', ieInfo.isIE);
        this.setValue('IEVersion', ieInfo.version);

        if (ieInfo.isIE && ieInfo.version >= 10) {
            $body.addClass('ie ie' + ieInfo.version);
        }

        if (isiPad) {
            $body.addClass('iPad');
        }
    },

    _registerModules: IEA.registerModules,
});
